
#include "Drivers/board.h"
#include "Drivers/lpc_pinsel.h"
#include "Drivers/lpc_gpio.h"
#include "Drivers/lpc_rtc.h"
#include "EasyGui/GuiLib.h"//eliminare solo per debug
#include "math.h"

/******************************************************************************
 * Defines and typedefs
 *****************************************************************************/

//#define LOGIC_ABS(x) (  ((int32_t)(x)) < 0 ? (-(x)) : (x))


struct digital_input DigIn;
struct strele StRele[MAXRELE];
struct adox Adox1500;
struct e2prom E2prom[E2_MAX_PAGE];
struct analog_input AnalogInput[11];

signed int sonde [NUMERO_SONDE];
//unsigned int O2ZeroADC, O2SpanADC;
unsigned int O2in;
//unsigned int O2Span;


unsigned char AlarmCounter=0;  //contatore allarmi collegati al rel� 10
unsigned char Allarme[25];
unsigned char AllarmIndex = 0;
unsigned char AL4Allarme = 0;
unsigned char AL4AllarmeB = 0;
unsigned char AL10Allarme = 0;
unsigned char AL10AllarmeB = 0;


// external struct / variables
extern RTC_TIME_Type RTCFullTime;
extern struct modbus Modbus;
extern unsigned char AbilitaTest;
extern unsigned int CANcount[],CANcountSlave;
extern unsigned char RS485_Slave_Address, RS485_Master_Address;
extern uint8_t LED_output;
extern unsigned char RS485_Port_Sel;


void InitRele(void)
{
  unsigned char rele;
  
  for (rele=0; rele<MAXRELE; rele++)     
  {
    // all'inizio tutti i rel� sono OFF
    StRele[rele].Stato = RELEOFF;
    StRele[rele].SecondiToOn = 0;
    StRele[rele].SecondiOn = 0;
  }
  StRele[0].Port = Port_5;
  StRele[1].Port = Port_4;
  StRele[2].Port = Port_4;
  StRele[3].Port = Port_4;
  StRele[4].Port = Port_4;
  StRele[5].Port = Port_4;
  StRele[0].Pin = RELE_1;
  StRele[1].Pin = RELE_2;
  StRele[2].Pin = RELE_3;
  StRele[3].Pin = RELE_4;
  StRele[4].Pin = RELE_5;
  StRele[5].Pin = RELE_6;
}



void SetMioRele(unsigned char comando, unsigned int sectowork, 
                unsigned long secwork, unsigned char rele)
{
	//uso indice "rele-1" perch� ho definito i rel� a partire da 1
  StRele[rele].SecondiToOn = sectowork;
  StRele[rele].SecondiOn = secwork;
  if (comando==ON)
  { 
    StRele[rele].Stato |= RELESETON;
    StRele[rele].Stato &= ~RELESETOFF;
  }
  else
  {
    StRele[rele].Stato |= RELESETOFF;
    StRele[rele].Stato &= ~RELESETON;
  }
}


//controlla il lavoro dei rel�: apre/chiude/conteggia
void Rele_Work(void)
{
  unsigned char rele, index;          

  // Cicla sui rel�
  for (rele=0; rele<MAXRELE; rele++)     
  {
    switch (StRele[rele].SecondiToOn)   
    {    
      case 0:
        if (StRele[rele].Stato & RELESETON)
        {
          StRele[rele].Stato &= ~RELESETON;
          StRele[rele].Stato |= RELEON;
          MySetPin(StRele[rele].Port, StRele[rele].Pin, 1);
        }
        else if (StRele[rele].Stato & RELESETOFF)
        {
          StRele[rele].Stato &= ~RELESETOFF;
          StRele[rele].Stato = RELEOFF;
          StRele[rele].SecondiOn = 0;
          MySetPin(StRele[rele].Port, StRele[rele].Pin, 0);
        }
        else if (StRele[rele].SecondiOn)
        {
          if (--StRele[rele].SecondiOn==0)
          {
            StRele[rele].Stato = RELEOFF;
            MySetPin(StRele[rele].Port, StRele[rele].Pin, 0);
          }   
        }
        break;

      default:       
        --StRele[rele].SecondiToOn;
        break;
    }
  }  
}


///***********************************************************************
// * @brief		get_DigInput 
// *          legge lo stato degli ingressi digitali
// *          N.B. restituisce 1 se l'ingresso sente un contatto chiuso, 0 se aperto
// **********************************************************************/
unsigned char get_DigInput(void){
 unsigned char result=0;
	
  result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_1 & 1) << 0;
  result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_2 & 1) << 1;
 	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_3 & 1) << 2;
	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_4 & 1) << 3;
	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_5 & 1) << 4;
	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_6 & 1) << 5;
	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_7 & 1) << 6;
	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_8 & 1) << 7;
	
	return ~result;//faccio il complemento a 1 per invertire
}



///***********************************************************************
// * @brief		CheckIngressi 
// *          legge lo stato degli ingressi digitali
// *          restituisce lo stato degli ingressi digitali in Adox1500.Ingressi
// **********************************************************************/
//restituisce lo stato degli ingressi digitali in Adox1500.Ingressi
void CheckIngressi (void)
{ 
  unsigned char index, temp;
  
  if (CANcount[0]>=40)
    Modbus.Array[MBVECTOR_ALLARMI_2] |= MB_ALLARMI_2_CAN_VALVE;
  else
    Modbus.Array[MBVECTOR_ALLARMI_2] &= ~MB_ALLARMI_2_CAN_VALVE;

  // Ingresso 7: errore sequenza fasi alimentazione (AL1) - allarme attivo basso - riarmo manuale 
  if (!(DigIn.Status & DIG_IN_7)){
    Adox1500.Allarme |= 0x8000;//Allarme che necessita di riarmo manuale
    Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN7;
    Adox1500.Ingressi &= ~(0x02);
    if (!(Adox1500.Allarme & 0x0002)){
      if (AlarmCounter<250)
        ++AlarmCounter;
      Adox1500.Allarme |= 0x0002;
      Allarme[AllarmIndex] = AL1;
      AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
    }
  }
  
  // Ingresso 3: guasto macchina Slave - allarme attivo basso - riarmo automatico
  if (!(DigIn.Status & DIG_IN_3)){
    if (!(Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_SLAVE)){
      if (AlarmCounter<250)
        ++AlarmCounter;
      Allarme[AllarmIndex] = AL26;
      AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      Modbus.Array[MBVECTOR_ALLARMI_2] |= MB_ALLARMI_2_SLAVE;
    }
  }
  else{
    //se era in allarme e ora non lo � pi� resetta l'allarme
    if (Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_SLAVE){
      Modbus.Array[MBVECTOR_ALLARMI_2] &= ~MB_ALLARMI_2_SLAVE;
      //cerca l'elemento corrispondente nell'array allarmi e lo elimina
      for (index=0; index<AllarmIndex; index++){
        if (Allarme[index]==AL26){
          Allarme[index] = Allarme[AllarmIndex-1];
          index = 50;
          --AllarmIndex;
          if (AlarmCounter)
            --AlarmCounter;
        }
      }
    }
  }
  // Ingresso 6: check se la macchina � in ON o OFF - consenso macchina in ON se ingresso alto
  if ( ( (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_REMOTE) && ((DigIn.Status & DIG_IN_6) || (Modbus.Array[MBVECTOR_COMANDO]) || (Adox1500.CAN_cmd)) ) ||
       ( (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_LOCAL_ON) && (Modbus.Array[MBVECTOR_COMANDO]) ) ){
    if (Adox1500.TmrFirstRun)
      --Adox1500.TmrFirstRun;
      
    if ( (Adox1500.TmrFirstRun==0) && (Adox1500.Tmr_Start) )
      --Adox1500.Tmr_Start;
      
//    if ( (Adox1500.Tmr_Start==0) && (Adox1500.TmrFirstRun==0) && (!(Adox1500.Allarme & 0x8000)) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN4)) ){
    if ( (Adox1500.Tmr_Start==0) && (Adox1500.TmrFirstRun==0) && (CANcount[0]<30) && (!(Adox1500.Allarme & 0x8000)) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN4)) ){
      Adox1500.Mode |= ADOX_START;
LED_output |= LED_8; // eliminare - prova segnalo con il LED 8 che la macchina � in funzione
	  }
    Adox1500.Ingressi |= 0x04;
    Adox1500.Allarme &= ~(0x0004);
  }
  else{
LED_output &= ~LED_8; //eliminare - arresto la macchina
		Adox1500.Ingressi &= ~(0x04);
    Adox1500.Allarme |= 0x0004;
    if (!(Adox1500.Mode & ADOX_PRE_STOP))
      Adox1500.Tmr_Stop = TMR_PRE_STOP;
    Adox1500.Mode |= ADOX_PRE_STOP;
  }

  // solo se la macchina � ON controllo gli altri ingressi
  if (Adox1500.Ingressi & 0x04){
    
    // Ingresso 1: scaricatore - allarme attivo basso - solo segnalazione
    if (DigIn.Status & DIG_IN_1){
      if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN6){
        //cerca l'elemento corrispondente nell'array allarmi e lo elimina
        for (index=0; index<AllarmIndex; index++){
          if (Allarme[index]==AL24){
            Allarme[index] = Allarme[AllarmIndex-1];
            index = 25;
            --AllarmIndex;
          }
        }
      }
      Modbus.Array[MBVECTOR_ALLARMI] &= ~MB_ALLARMI_IN6;
    }
    else{
      if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN6)){
        Allarme[AllarmIndex] = AL24;
        AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      }
      Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN6;
    }

		// Ingresso 2: alta temperatura pompa (AL4) - allarme attivo basso - riarmo manuale - se non � ADOX_200_TYCO
    if ( (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==0) || (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==2) || (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==3) ){
      // Ingresso 2: alta temperatura pompa (AL4) - allarme attivo basso - riarmo manuale
      if (!(DigIn.Status & DIG_IN_2)){
        //ritardo di 30 secondi
        if (AL4AllarmeB==0){
          AL4AllarmeB = RITALARME+1;
        }
        if (AL4AllarmeB==1){
          Adox1500.Ingressi |= 0x01;
          Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN9;
          if (!(Adox1500.Allarme & 0x0001))
          {
            if (AlarmCounter<250)
              ++AlarmCounter;
            Adox1500.Allarme |= 0x0001;
            Adox1500.Allarme |= 0x8000;//Allarme che necessita di riarmo manuale
            Allarme[AllarmIndex] = AL4;
            AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
          }
        }
      }
      else{
        AL4AllarmeB = 0;
      }
    }
    
		// Ingresso 3: guasto macchina Slave - controllato sempre anche a macchina ferma
    
    // Ingresso 4 Adox: mancanza aria compressa (AL2) - allarme attivo basso - riarmo automatico
    if (DigIn.Status & DIG_IN_4){
      Adox1500.TmrRitAL2 = RIT_AL2; //ricarica tmr antirimbalzo 06/02/07
      Adox1500.Ingressi |= 0x10;
      // resetta allarme
      if (Adox1500.Allarme & 0x0010){
				if (CANcount[0]<35){
          Adox1500.Mode &= ~ADOX_PRE_STOP;
          Adox1500.Mode &= ~ADOX_STOP;
				}
        Modbus.Array[MBVECTOR_ALLARMI] &= ~MB_ALLARMI_IN4;
        Adox1500.Allarme &= ~(0x0010);
        Adox1500.TmrFirstSincro = (E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1]*30);//sfasa di 3*ID secondi
        if (AlarmCounter)
          --AlarmCounter;
        //cerca l'elemento corrispondente nell'array allarmi e lo elimina
        for (index=0; index<AllarmIndex; index++){
          if (Allarme[index]==AL2){
            Allarme[index] = Allarme[AllarmIndex-1];
            index = 25;
            --AllarmIndex;
          }
        }
      }
    }
    else{
      if (Adox1500.TmrRitAL2)
        --Adox1500.TmrRitAL2;
      //se l'allarme perdura 10 secondi lo segnala
      if (Adox1500.TmrRitAL2==0){
        Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN4;
        Adox1500.Ingressi &= ~(0x10);
        if (!(Adox1500.Allarme & 0x0010)){
          if (AlarmCounter<250)
            ++AlarmCounter;
          Adox1500.Allarme |= 0x0010;
          Allarme[AllarmIndex] = AL2;
          AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
          if (Adox1500.AL2_Memo[6]==0){  
            Adox1500.AL2_Memo[0] = RTCFullTime.DOM;
            Adox1500.AL2_Memo[1] = RTCFullTime.MONTH;
            Adox1500.AL2_Memo[2] = RTCFullTime.YEAR-2000;
            Adox1500.AL2_Memo[3] = RTCFullTime.HOUR;
            Adox1500.AL2_Memo[4] = RTCFullTime.MIN;
            Adox1500.AL2_Memo[5] = RTCFullTime.SEC;
            Adox1500.AL2_Memo[6] = 1;
          }
          else if (Adox1500.AL2_Memo[6]<255)
            ++Adox1500.AL2_Memo[6];
        }
      }
    }
			
    // Ingresso 5: rele' termico (motore) (AL3) - allarme attivo basso - riarmo manuale
//    if (!(DigIn.Status & DIG_IN_5)){
    if (DigIn.Status & DIG_IN_5){
      Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN5;
      Adox1500.Ingressi |= 0x20;
      if (!(Adox1500.Allarme & 0x0020)){
        if (AlarmCounter<250)
          ++AlarmCounter;
        Adox1500.Allarme |= 0x0020;
        Adox1500.Allarme |= 0x8000;
        Allarme[AllarmIndex] = AL3;
        AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      }
    }

    // Ingresso 6: consenso macchina in ON  - controllato sempre anche a macchina ferma
		
    // Ingresso 7: errore sequenza fasi alimentazione - controllato sempre anche a macchina ferma
    
    // Ingresso 8: allarme inverter (AL8) - allarme attivo basso - riarmo manuale
    if (!(DigIn.Status & DIG_IN_8)){
      if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN8)){
        if (AlarmCounter<250)
          ++AlarmCounter;
      }
      Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN8;
      Adox1500.Allarme |= 0x8000;//Allarme che necessita di riarmo manuale
      Allarme[AllarmIndex] = AL8;//visualizza AL8
      AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
    }	
  }
  
  if (AL4Allarme)
    AL4Allarme--;
  if (AL4AllarmeB)
    AL4AllarmeB--;
  if (AL10Allarme)
    AL10Allarme--;
  if (AL10AllarmeB)
    AL10AllarmeB--;
}



// Aggiorna le uscite PWM (4-20mA) e i tempi di assorbimento - penso che questa funzione non sia pi� necessaria, lasciare solo impostazione uscite analogiche. Da verificare
void UpdateAdoxSettings (void)
{

  unsigned char index;
#ifdef SHOW_O2_PPM
  unsigned int setPSALow, setPSAHi;
#else
  unsigned char setPSALow, setPSAHi;
#endif
  float tmpset;
  float fattconv;
  
  if ( (Adox1500.Stato & ADOX_TARATURA) || (Adox1500.Stato & ADOX_PWD) || (Adox1500.Standby) )
    return;
  // controllo su tipo macchina (Adox/CO2 Adsorber/N2 Generator)
  fattconv = 6.3;//(2000-400)/254; //fattore di conversione

  // lavora come ADOX N2 (VSA) o come ADOX PSA
  if (Adox1500.Tipo & TYPE_2)
  {
    Adox1500.Assorbimento = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA];
    Adox1500.Compensazione = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA];
    tmpset = 0;
    //se non � in allarme confronta il set con il valore di O2
    if ( (Adox1500.Ingressi==RUNADOX) && (!(Adox1500.Allarme & 0x8000)) )
    {
      Adox1500.Set_Pause = (Adox1500.Set_Pause>0)? --Adox1500.Set_Pause: 0;
      if (Adox1500.Set_Pause==0)
      {
        //ver 1.02 check segno
#ifdef SHOW_O2_PPM
        setPSALow = (((int)Adox1500.SetO2-(int)Adox1500.Isteresi)>=1)? (unsigned int)(Adox1500.SetO2-Adox1500.Isteresi): 0;
        setPSAHi = (((int)Adox1500.SetO2+(int)Adox1500.Isteresi)<=500)? (unsigned int)(Adox1500.SetO2+Adox1500.Isteresi): 500;
#else
        setPSALow = (((int)Adox1500.SetO2-(int)Adox1500.Isteresi)>=1)? (unsigned char)(Adox1500.SetO2-Adox1500.Isteresi): 0;
        setPSAHi = (((int)Adox1500.SetO2+(int)Adox1500.Isteresi)<=255)? (unsigned char)(Adox1500.SetO2+Adox1500.Isteresi): 254;
#endif
        if (O2in <= setPSALow)
        {
          if ( (!(StRele[OUT_CC_ADOX].Stato & RELEON)) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN1)) )
          {
            Adox1500.Stato |= SET_RAGGIUNTO;
            Adox1500.Set_Pause = 6; //ricarica attesa a 1 minuto
            SetMioRele(ON, 0, 0, OUT_CC_ADOX);
          }
        }  
        else if (O2in >= setPSAHi)
        {
          if ( (StRele[OUT_CC_ADOX].Stato & RELEON) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN1)) )
          {
            Adox1500.Stato &= (~SET_RAGGIUNTO);
            Adox1500.Set_Pause = 6;
            SetMioRele(OFF, 0, 0, OUT_CC_ADOX);
          }
        }
        //controllo allarme purezza generata
        if (O2in <= E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM])
        {
          Adox1500.O2_Tmr_Err = RIT_O2_ALARM;
          //se era in allarme e ora non lo � pi� resetta l'allarme
          if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN11)
          {
            Modbus.Array[MBVECTOR_ALLARMI] &= ~MB_ALLARMI_IN11;
            //cerca l'elemento corrispondente nell'array allarmi e lo elimina
            for (index=0; index<AllarmIndex; index++)
            {
              if (Allarme[index]==AL20)
              {
                Allarme[index] = Allarme[AllarmIndex-1];
                index = 25;
                --AllarmIndex;
                if (AlarmCounter)
                  --AlarmCounter;
              }
            }
          }
        }
        else
        {
          if (Adox1500.O2_Tmr_Err==0)
          {
            if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN11))
            {
              if (AlarmCounter<250)
                ++AlarmCounter;
              Allarme[AllarmIndex] = AL20;
              AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
              Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN11;//allarme O2 impuro
            }
          }
          if ( (Adox1500.O2_Tmr_Err) && (Adox1500.Mode & ADOX_RUNNING) )
            --Adox1500.O2_Tmr_Err;
          if (!(Adox1500.Mode & ADOX_RUNNING))
            Adox1500.O2_Tmr_Err = RIT_O2_ALARM;
        }
      }
    }
  }

  //solo se non sto testando la macchina
  if (AbilitaTest==NO)
  {
//    //imposta le uscite analogiche
//    setpwm1 = (float)(400 + (float)(tmpset*fattconv));//26+,27-
//    // salva su RAM tamponata K di funzionamento Adox canale 1
//    rtc_wrram((uchar)(tmpset),REG_KPWM1);
//    //analogo per uscita 2
//    //setpwm2 = (float)(400 + (float)(tmpset*fattconv));//24+,25-
  }
}


void InitQueue (void)
{
  Adox1500.O2_FifoPointer = 1;
  Adox1500.O2_Queue = 0;
}


void O2_Media (void)
{
  unsigned int index;
  unsigned int max_sample;
  double mediamobile=0;
  
  Adox1500.O2_Fifo[Adox1500.O2_FifoPointer] = O2in;
  
  max_sample = ((Adox1500.Assorbimento + Adox1500.Compensazione)<DIM_O2_QUEUE)? (Adox1500.Assorbimento + Adox1500.Compensazione): DIM_O2_QUEUE-1;
  
  if (Adox1500.O2_FifoPointer<max_sample)
     ++Adox1500.O2_FifoPointer;
  else
  {
     Adox1500.O2_FifoPointer = 1;
     Adox1500.O2_Queue = 1;
  }
  
  if (Adox1500.O2_Queue==0)
  {
    for (index=1; index<Adox1500.O2_FifoPointer; index++)
      mediamobile += Adox1500.O2_Fifo[index];
    Adox1500.O2_Fifo[0] = (unsigned char) (mediamobile/(Adox1500.O2_FifoPointer-1));
  }
  else
  {
    for (index=1; index<max_sample+1; index++)
      mediamobile += Adox1500.O2_Fifo[index];
    Adox1500.O2_Fifo[0] = (unsigned char) (mediamobile/(max_sample));
  }
}


void CheckPress(void)
{
  unsigned char index;
  
  if (E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2])
  {
    if ( (Modbus.Array[MBVECTOR_P1]<2500) && (Modbus.Array[MBVECTOR_P1] > E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2]) )
    {
      if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN2))
      {
        Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN2;
        Allarme[AllarmIndex] = AL21;
        AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      }
    }
    else if ( (Modbus.Array[MBVECTOR_P1] < E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2]) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN3)) )
    {
      // resetta allarme
      if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN2)
      {
        Modbus.Array[MBVECTOR_ALLARMI] &= ~MB_ALLARMI_IN2;
        //cerca l'elemento corrispondente nell'array allarmi e lo elimina
        for (index=0; index<AllarmIndex; index++)
        {
          if (Allarme[index]==AL21)
          {
            Allarme[index] = Allarme[AllarmIndex-1];
            index = 20;
            --AllarmIndex;
          }
        }
      }
    }
  }
  if (E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2])
  {
    if ( (Modbus.Array[MBVECTOR_P1]<2500) && (Modbus.Array[MBVECTOR_P1] > E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2]) )
    {
      Adox1500.Allarme |= 0x8000;//Allarme che necessita di riarmo manuale
      if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN3))
      {
        if (AlarmCounter<250)
          ++AlarmCounter;
        Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN3;
        Allarme[AllarmIndex] = AL22;
        AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      }
    }
  }
}


//controllo temperatura lavoro macchina
void CheckTemp(void)
{
  unsigned char index;
  
  // se la temperatura sale troppo o errore sonda (ed � abilitato il controllo) arresta la macchina
  if ( (E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]) && (Modbus.Array[MBVECTOR_T1] > E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]) )
  {
    if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN16))
    {
      Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN16;
      Allarme[AllarmIndex] = AL17; //visualizza AL 11
      AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      if (AlarmCounter<250)
        ++AlarmCounter;
    }
    Adox1500.Allarme |= 0x8000;//Allarme che necessita di riarmo manuale
  }
  // controllo temperatura CABINET
  // esegue il controllo solo se il set � impostato a un valore positivo
  if (E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP])
  {
    // se la temperatura � troppo elevata
    // se errore sonda segnala ma non controlla
    if ( (Modbus.Array[MBVECTOR_T2]<1000) && (Modbus.Array[MBVECTOR_T2] > E2prom[E2_PAGE_2].E2_Int[E2_2_TOO_HIGH_TEMP]) )
    {
      //in caso di superamento di questa soglia arresta anche la macchina - bloccante con riarmo manuale
      Adox1500.Allarme |= 0x8000;
      if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN12))
      {
        Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN12;
        if (AlarmCounter<250)
          ++AlarmCounter;
      }
    }
    if ( (Modbus.Array[MBVECTOR_T2]<1000) && (Modbus.Array[MBVECTOR_T2] > E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP]) )
    {
      if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN13))
      {
        Modbus.Array[MBVECTOR_ALLARMI] |= MB_ALLARMI_IN13;
        Allarme[AllarmIndex] = AL11;
        AllarmIndex = (AllarmIndex<20)? ++AllarmIndex: 0;
      }
    }
    else if ( (Modbus.Array[MBVECTOR_T2] < E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP]) && (!(Adox1500.Allarme & 0x8000)) )
    {
      if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN13)
      {
        Modbus.Array[MBVECTOR_ALLARMI] &= ~MB_ALLARMI_IN13;
        for (index=0; index<AllarmIndex; index++)
        {
          if (Allarme[index]==AL11)
          {
            Allarme[index] = Allarme[AllarmIndex-1];
            index = 20;
            --AllarmIndex;
          }
        }
      }
    }
  }
  //controlla solo se non c'� STOP emergenza
  //if (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN13))
  {
    //se E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]=0 --> OFF - il rel� non viene comandato
    if (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]==0)
    {
      if (StRele[OUT_VENTIL_TEMP].Stato & RELEON)
        SetMioRele(OFF, 0, 0, OUT_VENTIL_TEMP);
    }
    //se E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]=1 --> R1  - il rel� viene associato e comandato come R1
    else if (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]==1)
    {
      if ( (!(StRele[OUT_VENTIL_TEMP].Stato & RELEON)) && (StRele[OUT_CC_ADOX].Stato & RELEON) )
        SetMioRele(ON, 0, 0, OUT_VENTIL_TEMP);
      else if ( (StRele[OUT_VENTIL_TEMP].Stato & RELEON) && (!(StRele[OUT_CC_ADOX].Stato & RELEON)) )
        SetMioRele(OFF, 0, 0, OUT_VENTIL_TEMP);
    }
    //se E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]>1 --> SET - il rel� viene comandato dalla temperatura e da R1
    else
    {
      //parte la ventilazione se R1 � ON o se la temperatura � maggiore o  uguale al set
      if ( (StRele[OUT_CC_ADOX].Stato & RELEON) || (Modbus.Array[MBVECTOR_T2] >= E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]) )
      {
        if (!(StRele[OUT_VENTIL_TEMP].Stato & RELEON))
          SetMioRele(ON, 0, 0, OUT_VENTIL_TEMP);
      }
      //ferma la ventilazione se R1 � OFF e se la temperatura � minore o uguale al set - 3�C
      else if ( (!(StRele[OUT_CC_ADOX].Stato & RELEON)) && (Modbus.Array[MBVECTOR_T2] <= (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]-30)) )
      {
        if (StRele[OUT_VENTIL_TEMP].Stato & RELEON)
          SetMioRele(OFF, 0, 0, OUT_VENTIL_TEMP);
      }
    }
    //se E2prom[E2_PAGE_2].E2_In[tE2_2_T3_R5_TEMP]=0 --> OFF - il rel� non viene comandato
    if (E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]==0)
    {
      if (StRele[OUT_R5_LIKE_R1].Stato & RELEON)
        SetMioRele(OFF, 0, 0, OUT_R5_LIKE_R1);
    }
    //se E2prom[E2_PAGE_3].E2_Int[E2_T3_R5_TEMP]=1 --> R1  - il rel� viene associato e comandato come R1
    else if (E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]==1)
    {
      if ( (!(StRele[OUT_R5_LIKE_R1].Stato & RELEON)) && (StRele[OUT_CC_ADOX].Stato & RELEON) )
        SetMioRele(ON, 0, 0, OUT_R5_LIKE_R1);
      else if ( (StRele[OUT_R5_LIKE_R1].Stato & RELEON) && (!(StRele[OUT_CC_ADOX].Stato & RELEON)) )
        SetMioRele(OFF, 0, 0, OUT_R5_LIKE_R1);
    }
    //se E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]>1 --> SET - il rel� viene comandato dalla temperatura e da R1
    else
    {
      //parte la ventilazione se R1 � ON o se la temperatura � maggiore o  uguale al set
      if ( (StRele[OUT_CC_ADOX].Stato & RELEON) || (Modbus.Array[MBVECTOR_T2] >= E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]) )
      {
        if (!(StRele[OUT_R5_LIKE_R1].Stato & RELEON))
          SetMioRele(ON, 0, 0, OUT_R5_LIKE_R1);
      }
      //ferma la ventilazione se R1 � OFF e se la temperatura � minore o uguale al set - 3�C
      else if ( (!(StRele[OUT_CC_ADOX].Stato & RELEON)) && (Modbus.Array[MBVECTOR_T2] <= (E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]-30)) )
      {
        if (StRele[OUT_R5_LIKE_R1].Stato & RELEON)
          SetMioRele(OFF, 0, 0, OUT_R5_LIKE_R1);
      }
    }
  }
}



void Adox1500work (void)
{
  unsigned char i, j;
  unsigned int time;

  //segnalazione allarme generico
  if ( (AlarmCounter) || (CANcount[0]>=35) || (CANcountSlave>=SLAVE_WDOG_TMOUT) )
  {
    if (StRele[OUT_ALARM].Stato & RELEON)
    {
      SetMioRele(OFF, 0, 0, OUT_ALARM);
//      SetMioRele(OFF, 0, 0, OUT_R3_LIKE_R10);
    }
  }
  else
  {
    if (!(StRele[OUT_ALARM].Stato & RELEON))
    {
      SetMioRele(ON, 0, 0, OUT_ALARM);
//      SetMioRele(ON, 0, 0, OUT_R3_LIKE_R10);
    }
  }
  
  // lavora solo se non ci sono allarmi
//  if ( (Adox1500.Ingressi==RUNADOX) &&
//       (!(Adox1500.Allarme & 0x8000)) && (CANcount[0]<35) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN4)) && 
//       (((E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_REMOTE) && ((DigIn.Status & DIG_IN_6) || (Modbus.Array[MBVECTOR_COMANDO]) || (Adox1500.CAN_cmd))) ||
//         (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_LOCAL_ON)) )
  if ( (Adox1500.Ingressi==RUNADOX) &&
       (!(Adox1500.Allarme & 0x8000)) && (CANcount[0]<35) && (!(Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN4)) && 
       (((E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_REMOTE) && ((DigIn.Status & DIG_IN_6) || (Modbus.Array[MBVECTOR_COMANDO]) || (Adox1500.CAN_cmd))) ||
         ((E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_LOCAL_ON) && (Modbus.Array[MBVECTOR_COMANDO])) ) )
  {
    Adox1500.Mode &= ~ADOX_PRE_STOP;
    // conteggio ore funzionamento Adox ------------------->
    if (Adox1500.CounterSec<HALFDAY-1)
      ++Adox1500.CounterSec;
    // memorizza su E2 e azzera contatore secondi
    else{
      Adox1500.CounterSec = 0;
      if (Adox1500.Ore_Run<8333){
        ++Adox1500.Ore_Run;
        E2prom[E2_PAGE_4].E2_Int[E2_4_HALFDAY] = Adox1500.Ore_Run;
				eeprom_write_word(E2_PAGE_4,E2_4_HALFDAY*2,Adox1500.Ore_Run);
      }  
    }
    // salva secondi su RAM tamponata
    RTC_WriteGPREG(LPC_RTC,RTC_REG_0,Adox1500.CounterSec);
  }
  else
  {
    if (!(Adox1500.Mode & ADOX_PRE_STOP))
      Adox1500.Tmr_Stop = TMR_PRE_STOP;
    Adox1500.Mode |= ADOX_PRE_STOP;
    if (Adox1500.Tmr_Stop)
      --Adox1500.Tmr_Stop;
    else
    {
      Adox1500.Mode |= ADOX_STOP;
      Adox1500.Mode &= ~ADOX_PRE_STOP;
    }
  }
  if ( (!(Adox1500.Mode & ADOX_RUNNING)) && (!(Adox1500.Mode & ADOX_STOP)) )
  {
    if (Adox1500.Mode & ADOX_START)
    {
      Adox1500.Mode |= ADOX_RUNNING;
      Adox1500.Ciclo[0] = 0;
      Adox1500.Ciclo[1] = 0;
      Adox1500.Timer[0] = 0;
      Adox1500.Timer[1] = Adox1500.Sfasamento/10;
    }
  }
  if (Adox1500.Mode & ADOX_RUNNING)
  {
    if ( (!(StRele[OUT_CC_ADOX].Stato & RELEON)) || (Adox1500.RitVacuum) )
    {
      Adox1500.RitVacuum = 0; //aggiunto in ver 3.05b
      SetMioRele(ON, 0, 0, OUT_CC_ADOX); //cc macchina
      SetMioRele(ON, 0, 0, OUT_VTARATURA); //04/11/2014 Marco Cavalli
      Adox1500.O2_Tmr_Err = RIT_O2_ALARM;
    }
    if (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==0) // valvole FESTO DA - Adox N2 2016
      j = 12;
    else if (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==1)// valvole FESTO NC - Adox 200 Tyco
      j = 6;
    else if (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==2)// valvole FESTO doppio effetto - Adox 1300 Univergomma
      j = 12;
    else if (E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]==3)// valvole PNEUMAX doppio effetto o FESTO NC - Adox 1300 Hypoxic
      j = 6;
    
//GuiVar_integer_4 = Adox1500.Timer[0];
//GuiVar_integer_5 = Adox1500.Ciclo[0];
		
    for (i=0; i<2; i++) //solo 2 moduli
    {
      //if (Adox1500.MyModule & (unsigned char)pow(2,i)) //se il modulo � abilitato
      {
        if (Adox1500.Timer[i])
          --Adox1500.Timer[i];
        if (Adox1500.Timer[i]==0)
        {
          Adox1500.ModuleValve[i] = 0;//mette a zero per poi ricostruire il valore da spedire
                                      //relativo alle uscite (valvole) solo del modulo che
                                      //dovr� imporre nuove uscite
          if (Adox1500.Ciclo[i]==0)//se Scarico 2
          {
            Adox1500.Ciclo[i] = 1;//Assorbimento 1
            Adox1500.Timer[i] = Adox1500.Assorbimento/10;
            Adox1500.ModuleValve[i] = Adox1500.phaseA1[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
            SetMioRele(ON, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 ON, se i=1 VP2 ON
//            SetMioRele(ON, 0, 0, OUT_R8_V1A_V2V);
//            SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
          }
          else if (Adox1500.Ciclo[i]==1)//se Assorbimento 1
          {
            if (Adox1500.Compensazione)//esegue solo se c'� un tempo di compensazione > 0
            {
              Adox1500.Ciclo[i] = 2;//Compensazione 1
              Adox1500.Timer[i] = Adox1500.Compensazione/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseC1[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
//              SetMioRele(ON, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
            }
            else if (Adox1500.Scarico)//esegue solo se c'� un tempo di scarico > 0
            {
              Adox1500.Ciclo[i] = 3;//Scarico 1
              Adox1500.Timer[i] = Adox1500.Scarico/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseS1[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
//              SetMioRele(ON, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
              //non spegne pompe SetMioRele(OFF, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 OFF, se i=1 VP2 OFF
            }
            else //altrimenti riparte con il ciclo di assorbimento successivo
            {
              Adox1500.Ciclo[i] = 4;//Assorbimento 2
              Adox1500.Timer[i] = Adox1500.Assorbimento/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseA2[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
              SetMioRele(ON, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 ON, se i=1 VP2 ON
//              SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(ON, 0, 0, OUT_R9_V2A_V1V);
            }
          }
          else if (Adox1500.Ciclo[i]==2)//se Compensazione 1
          {
            if (Adox1500.Scarico)//esegue solo se c'� un tempo di scarico > 0
            {
              Adox1500.Ciclo[i] = 3;//Scarico 1
              Adox1500.Timer[i] = Adox1500.Scarico/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseS1[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
//              SetMioRele(ON, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
              //non spegne pompe SetMioRele(OFF, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 OFF, se i=1 VP2 OFF
            }
            else //altrimenti riparte con il ciclo di assorbimento successivo
            {
              Adox1500.Ciclo[i] = 4;//Assorbimento 2
              Adox1500.Timer[i] = Adox1500.Assorbimento/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseA2[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
              SetMioRele(ON, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 ON, se i=1 VP2 ON
//              SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(ON, 0, 0, OUT_R9_V2A_V1V);
            }
          }
          else if (Adox1500.Ciclo[i]==3)//se Scarico 1
          {
            Adox1500.Ciclo[i] = 4;//Assorbimento 2
            Adox1500.Timer[i] = Adox1500.Assorbimento/10;
            Adox1500.ModuleValve[i] = Adox1500.phaseA2[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
            SetMioRele(ON, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 ON, se i=1 VP2 ON
//            SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//            SetMioRele(ON, 0, 0, OUT_R9_V2A_V1V);
          }
          else if (Adox1500.Ciclo[i]==4)//se Assorbimento 2
          {
            if (Adox1500.Compensazione)//esegue solo se c'� un tempo di compensazione > 0
            {
              Adox1500.Ciclo[i] = 5;//Compensazione 2
              Adox1500.Timer[i] = Adox1500.Compensazione/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseC2[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
//              SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(ON, 0, 0, OUT_R9_V2A_V1V);
            }
            else if (Adox1500.Scarico)//esegue solo se c'� un tempo di scarico > 0
            {
              Adox1500.Ciclo[i] = 0;//Scarico 2
              Adox1500.Timer[i] = Adox1500.Scarico/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseS2[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j); //BUG Andrea V. era 0x0033
//              SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(ON, 0, 0, OUT_R9_V2A_V1V);
              //non spegne pompe SetMioRele(OFF, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 OFF, se i=1 VP2 OFF
            }
            else //altrimenti riparte con il ciclo di assorbimento successivo
            {
              Adox1500.Ciclo[i] = 1;//Assorbimento 1
              Adox1500.Timer[i] = Adox1500.Assorbimento/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseA1[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
              SetMioRele(ON, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 ON, se i=1 VP2 ON
//              SetMioRele(ON, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
            }
          }
          else if (Adox1500.Ciclo[i]==5)//se Compensazione 2
          {
            if (Adox1500.Scarico)//esegue solo se c'� un tempo di scarico > 0
            {
              Adox1500.Ciclo[i] = 0;//Scarico 2
              Adox1500.Timer[i] = Adox1500.Scarico/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseS2[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j); //BUG Andrea V. era 0x0033
//              SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(ON, 0, 0, OUT_R9_V2A_V1V);
              //non spegne pompe SetMioRele(OFF, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 OFF, se i=1 VP2 OFF
            }
            else //altrimenti riparte con il ciclo di assorbimento successivo
            {
              Adox1500.Ciclo[i] = 1;//Assorbimento 1
              Adox1500.Timer[i] = Adox1500.Assorbimento/10;
              Adox1500.ModuleValve[i] = Adox1500.phaseA1[E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE]]<<(i*j);
              SetMioRele(ON, 0, 0, OUT_1_ADOX+i); //se i=0 VP1 ON, se i=1 VP2 ON
//              SetMioRele(ON, 0, 0, OUT_R8_V1A_V2V);
//              SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
            }
          }
        }
      }
    }
    //ricostruisce lo stato delle uscite (primo modulo + secondo modulo)
    Adox1500.Output = Adox1500.ModuleValve[0] | Adox1500.ModuleValve[1];
    
    //sistemare aggiungendo t scarico, serve solo come visualizzazione stato funzionamento
    Adox1500.SecondiOn = Adox1500.Timer[0];
    
    if ( (Adox1500.Mode & ADOX_PRE_STOP) || (Adox1500.Mode & ADOX_STOP) )
    {
      SetMioRele(OFF, 0, 0, OUT_CC_ADOX); //tensione OFF
      
      if (E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM])
      {
        if (Adox1500.RitVacuum==0)
        {
//          time = 10*(unsigned int)E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM]; //NewAdso
          time = (unsigned int)E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM];      //SBM
          SetMioRele(ON, 0, time, OUT_VTARATURA); //20/04/2017 Marco Cavalli
          SetMioRele(ON, 0, time, OUT_1_ADOX);  //VP1 ON
        }
        Adox1500.RitVacuum = 1;
      }
      else
      {
        SetMioRele(OFF, 0, 0, OUT_VTARATURA); //04/11/2014 Marco Cavalli //20/04/2017 Marco Cavalli - commenta
        SetMioRele(OFF, 0, 0, OUT_1_ADOX);  //VP1 OFF //20/04/2017 Marco Cavalli - commenta
      }
    }
  }
  if (Adox1500.Mode & ADOX_STOP)
  {
    Adox1500.Mode = 0;
//    SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//    SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
    Adox1500.Tmr_Start = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START];
    if ( (StRele[OUT_CC_ADOX].Stato & RELEON) && (Adox1500.RitVacuum==0) )
    {
      if (E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM])
      {
        Adox1500.RitVacuum = 1; //aggiunto in ver 3.05b
//        time = 10*(unsigned int)E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM]; //NewAdso
        time = (unsigned int)E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM];      //SBM
        SetMioRele(ON, 0, time, OUT_CC_ADOX); //tensione OFF
        SetMioRele(ON, 0, time, OUT_VTARATURA); //04/11/2014 Marco Cavalli
        SetMioRele(ON, 0, time, OUT_1_ADOX);  //VP1 ON
      }
      else
      {
        Adox1500.RitVacuum = 0; //aggiunto in ver 3.05b
        SetMioRele(OFF, 0, 0, OUT_CC_ADOX); //tensione OFF
        SetMioRele(OFF, 0, 0, OUT_VTARATURA); //04/11/2014 Marco Cavalli
        SetMioRele(OFF, 0, 0, OUT_1_ADOX);  //VP1 OFF
//        SetMioRele(OFF, 0, 0, OUT_R8_V1A_V2V);
//        SetMioRele(OFF, 0, 0, OUT_R9_V2A_V1V);
      }
    }
    for (i=0; i<2; i++)
      Adox1500.ModuleValve[i] = 0;
    //ricostruisce lo stato delle uscite (primo modulo + secondo modulo)
    Adox1500.Output = Adox1500.ModuleValve[0] | Adox1500.ModuleValve[1];
  }
	//gestione variabili grafiche
	
	GuiVar_time_left_sec = Adox1500.SecondiOn;

	if (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_REMOTE){
    if ( (DigIn.Status & DIG_IN_6) || (Modbus.Array[MBVECTOR_COMANDO]) || (Adox1500.CAN_cmd) ){
			if (Adox1500.Mode & ADOX_RUNNING){
				GuiVar_indice_fase = Adox1500.Ciclo[0];
  			GuiVar_indice_time = 1;
			}
			else{
				GuiVar_indice_fase = 7;
		    GuiVar_indice_time = 0;
			}
		}
		else{
			GuiVar_indice_fase = 7;
			GuiVar_indice_time = 0;
		}

	}
	else if (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_LOCAL_OFF){
		GuiVar_indice_fase = 6;
		GuiVar_indice_time = 0;
	}
	else{
		if (Adox1500.Mode & ADOX_RUNNING)
			GuiVar_indice_fase = Adox1500.Ciclo[0];
		else
			GuiVar_indice_fase = 6;
		if (Adox1500.Mode & ADOX_RUNNING)
			GuiVar_indice_time = 1;
		else
			GuiVar_indice_time = 0;
	}
}


//calcola valore medio flusso nel tempo (un campione al secondo)
void FlowAverage (void)
{
  double tmpAvg;
  unsigned char i, max_index;
  
  tmpAvg = 0;
  
  if ( (Modbus.Array[MBVECTOR_Q_AIR]>0) && (Modbus.Array[MBVECTOR_Q_AIR] < 10000) )//se non c'� errore sonda
  {
    Adox1500.FlowAvg[Adox1500.AvgIndex] = Modbus.Array[MBVECTOR_Q_AIR];
    if (Adox1500.AvgBufferFull==0)
      max_index = Adox1500.AvgIndex;
    else
      max_index = E2prom[E2_PAGE_4].E2_Int[E2_4_AVGINDEXMAX];
      
    for (i=1; i<max_index+1; i++)
      tmpAvg += Adox1500.FlowAvg[i];
    Adox1500.FlowAvg[0] = (unsigned int)(tmpAvg/max_index);
    Modbus.Array[MBVECTOR_FLOW_AVG] = Adox1500.FlowAvg[0];
    if (Adox1500.AvgIndex<E2prom[E2_PAGE_4].E2_Int[E2_4_AVGINDEXMAX])
      ++Adox1500.AvgIndex;
    else
    {
      Adox1500.AvgIndex = 1;
      Adox1500.AvgBufferFull = 1;
    }
  }
  else
  {
    Adox1500.AvgIndex = 1;
    Adox1500.AvgBufferFull = 0;
    Adox1500.FlowAvg[0] = Modbus.Array[MBVECTOR_Q_AIR];
    Modbus.Array[MBVECTOR_FLOW_AVG] = Adox1500.FlowAvg[0];
  }
}




// valvola di ricircolo per evitare che la cella vada in depressione
void Ricircolo (void)
{
//  if (Adox1500.Tmr_Comp)
//    --Adox1500.Tmr_Comp;
//  
//  //se la macchina � in funzione
//  if (StRele[OUT_CC_ADOX].Stato & RELEON)
//  {
//    if (Adox1500.Tmr_Comp==0)
//    {
//      if (StRele[OUT_RICIRCOLO].Stato & RELEON)
//      {
//        Adox1500.Tmr_Comp = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_COMP_OFF];
//        SetMioRele(OFF, 0, 0, OUT_RICIRCOLO);
//      }
//      else
//      {
//        Adox1500.Tmr_Comp = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_COMP_ON];
//        SetMioRele(ON, 0, 0, OUT_RICIRCOLO);
//      }
//    }
//  }
//  else
//  {
//    Adox1500.Tmr_Comp = 0;
//    if (StRele[OUT_RICIRCOLO].Stato & RELEON)
//      SetMioRele(OFF, 0, 0, OUT_RICIRCOLO);
//  }
}




// all'accensione della scheda carica dalla RAM tamponata
// lo stato di funzionamento dell'Adox
// reg definito in realtime.h
// valutare cosa sia meglio fare in caso di RAM con valori strani
void InitAdox (void)
{
  unsigned char offset, page;
  unsigned int tmp_maxtime, tmp_rtc, ore;
  float tmppwm1;
  unsigned long ore_e2;
	int16_t tmp_int;
	 
//  eeprom_write_word(E2_PAGE_4,E2_4_HALFDAY*2,430);//per inizializzare le ore del nostro Adox
	
	//per l'inizializzazione carico da EEPROM le 4 pagine con cicli "for"
  for (page=0; page<E2_MAX_PAGE; page++){
    for (offset=0; offset<30; offset++){
	     E2prom[page].E2_Int[offset] = eeprom_read_word(page,offset*2);
		}
	}

  E2prom[E2PROM_PAGE_OF_OTHER].E2_Int[REG_BOARD_NUMBER] = eeprom_read_word(E2PROM_PAGE_OF_OTHER,REG_BOARD_NUMBER*2);
  E2prom[E2PROM_PAGE_OF_OTHER].E2_Int[REG_NEW_CONFIGURATION] = eeprom_read_word(E2PROM_PAGE_OF_OTHER,REG_NEW_CONFIGURATION*2);
  E2prom[E2PROM_PAGE_OF_OTHER].E2_Int[REG_LANGUAGE] = eeprom_read_word(E2PROM_PAGE_OF_OTHER,REG_LANGUAGE*2);
  E2prom[E2PROM_PAGE_OF_OTHER].E2_Int[REG_BACKLIGHT] = eeprom_read_word(E2PROM_PAGE_OF_OTHER,REG_BACKLIGHT*2);
	
	
	Adox1500.TmrErrB1 = 30;//30 secondi
	Adox1500.TmrErrB2 = 30;//30 secondi
	Adox1500.TmrErrB3 = 30;//30 secondi
	
  Adox1500.FlowAvg[0] = 0;
  Adox1500.AvgIndex = 1;
  Adox1500.AvgBufferFull = 0;
  
  Adox1500.Assorbimento = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA];
  Adox1500.Compensazione = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA];
  Adox1500.Scarico = E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO];
  Adox1500.Sfasamento = E2prom[E2_PAGE_2].E2_Int[E2_2_SFASAMENTO];
  Adox1500.Mode = 0;
  
  Adox1500.StatoSlave = 0;
  
  // resetta allarmi e setta ingressi per ripartire
  Adox1500.Allarme = 0;
  Adox1500.Ingressi = RUNADOX;
  AllarmIndex =  0;
  //RitardoAllarme = 0;
  AL4Allarme = 0;
  AL10Allarme = 0;
  Adox1500.TmrRitAL5 = 0;
  
  //carica da E2 i valori di span e zero per O2
//  O2ZeroADC = (unsigned int)(E2prom[E2_PAGE_1].E2_Int[E2_1_O2_ZEROADC]);
//  O2SpanADC = (unsigned int)(E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPANADC]);
#ifdef SHOW_O2_PPM
//  O2Span = E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPAN];
  Adox1500.SetO2 = E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET];
  Adox1500.Isteresi = E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_ISTERESI];
#else
//  O2Span = (unsigned char)(E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPAN]);
  Adox1500.SetO2 = (unsigned char)E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET];
  Adox1500.Isteresi = (unsigned char)E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_ISTERESI];
#endif

  //controlla se deve lavorare come CO2 Adsorber o come N2 Generator
  Adox1500.Tipo = 0;
  switch (E2prom[E2_PAGE_2].E2_Int[E2_2_MACHINE])
  {
    case 0:
      Adox1500.Tipo = TYPE_1;    // CO2 ADSORBER
      break;
    case 1:
      Adox1500.Tipo = 0;         // N2 GENERATOR
      break;
    case 2:
      Adox1500.Tipo = TYPE_2;    // Adox N2 (F)
      Adox1500.Set_Pause = 6;
      break;
    case 3:
      Adox1500.Tipo = TYPE_3;    // Adox PSA NT
      Adox1500.Set_Pause = 6;
      break;
    case 4:
      Adox1500.Tipo = TYPE_4;    // Adox PSA 1/2
      Adox1500.Set_Pause = 6;
      break;
    case 5:
      Adox1500.Tipo = TYPE_5;    // Adox PSA NTF
      Adox1500.Set_Pause = 6;
      break;
    case 6:
      Adox1500.Tipo = TYPE_6;    // Adox PSA TSF
      Adox1500.Set_Pause = 6;
      break;
  }
  
  tmp_maxtime = 200;
  
  //l'Adox risulta spento
  Adox1500.Stato = ADOX_OFF;
  //legge lo stato Adox allo spegnimento/reset della scheda
//  Adox1500.Stato |= rtc_rdram(REG_ADOXSTATO);
//  Adox1500.Stato &= (~SET_RAGGIUNTO);
//  Adox1500.Stato &= (~ADOX_TARATURA);
//  Adox1500.Stato &= (~ADOX_PWD);
//  //legge i secondi rimanenti di funzionamento Adox
//  Adox1500.SecondiOn = rtc_rdram(REG_ADOXTIME);
//  // carica l'ultimo valore utilizzato per l'uscita analogica
//  tmppwm1 = rtc_rdram(REG_KPWM1);
//  tmppwm1 = (tmppwm1>254)? 0: tmppwm1; //controllo integrit� valore
//  setpwm1 = (float)(400 + (float)(tmppwm1*6.3));
	
  // legge secondi e ore funzionamento totali Adox
	Adox1500.CounterSec = RTC_ReadGPREG (LPC_RTC,RTC_REG_0);
  Adox1500.Ore_Run = E2prom[E2_PAGE_4].E2_Int[E2_4_HALFDAY];
  //ricostruisco ore funzionamento Adox
  ore_e2 = (unsigned long)Adox1500.Ore_Run * 12;
  ore = Adox1500.CounterSec/3600;                            // ore
  Adox1500.TmrService = E2prom[E2_PAGE_4].E2_Int[E2_4_NEXT_SERVICE] - (ore_e2+ore);
  
  Adox1500.Reintegro = E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO];//ver 1.06: 18/07/06
  Adox1500.Standby = 0;
//  Adox1500.DoppioReintegro = E2prom[E2_PAGE_2].E2_Int[E2_2_REINTEGRO2];
  
  Adox1500.TmrRitAL2 = RIT_AL2; //ricarica tmr antirimbalzo mancanza pressione //ver 1.08
  Adox1500.TmrFirstRun = RIT_AL2; //ricarica tmr prima partenza //ver 1.08
  
//  if ( (E2prom[E2_PAGE_2].E2_Int[E2_2_FILTRO_O2]>90) || (E2prom[E2_PAGE_2].E2_Int[E2_2_FILTRO_O2]<5) )
//    E2prom[E2_PAGE_2].E2_Int[E2_2_FILTRO_O2] = 27;
  
  //Adox.TmrFirstSincro = AVVISO_START+E2prom[E2_PAGE_1].E2_Int[E2_SINCRO_PSA]+(E2prom[E2_PAGE_1].E2_Int[E2_ID_CAN]*10);//sfasa di un secondo a seconda dell'id
  //Adox.TmrFirstSincro = AVVISO_START+(E2prom[E2_PAGE_1].E2_Int[E2_ID_CAN]*10);//sfasa di un secondo a seconda dell'id
  Adox1500.TmrFirstSincro = (E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1]*30);//sfasa di 3*ID secondi

  //check congruenza valori RAM se i valori sono sballati parte con
  // il primo ciclo di assorbimento per 200
  if ( (Adox1500.SecondiOn>tmp_maxtime) || (Adox1500.SecondiOn<1) )
    Adox1500.SecondiOn = tmp_maxtime;
  if ( (!(Adox1500.Stato==ADOX_OFF+ASS_CICLO1)) ||
       (!(Adox1500.Stato==ADOX_OFF+ASS_CICLO2)) ||
       (!(Adox1500.Stato==ADOX_OFF+CMP_CICLO1)) ||
       (!(Adox1500.Stato==ADOX_OFF+CMP_CICLO2)) ||
       (!(Adox1500.Stato==ASS_CICLO1)) ||
       (!(Adox1500.Stato==ASS_CICLO2)) ||
       (!(Adox1500.Stato==CMP_CICLO1)) ||
       (!(Adox1500.Stato==CMP_CICLO2)) )
  {
    Adox1500.Stato = ADOX_OFF+ASS_CICLO1;
  }     
  else
  {  
    //funzionamento come Adox VSA o PSA
    Adox1500.Stato = ADOX_OFF + CMP_CICLO1;
  }
  //ripartenza PSA dal primo ciclo
  //ver 1.05:07/07/06 ----------------------------->
  if (Adox1500.Tipo > TYPE_2)
  {
    Adox1500.Stato = ADOX_OFF;
    Adox1500.SecondiOn = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA];
    //check congruenza valori RAM se i valori sono sballati parte con
    // il primo ciclo di assorbimento per 50
    if ( (Adox1500.SecondiOn>1000) || (Adox1500.SecondiOn<10) )
      Adox1500.SecondiOn = tmp_maxtime;
    //SetMioRele(ON, 0, E2prom[E2_PAGE_1].E2_Int[E2_SECRESTART], OUT_RESTART);
    //alla ripartenza chiamo R1 (cc essicatore, compressore e valvola analisi per 1 ora)
    //SetMioRele(ON, 0, RIT_OFF_R1, OUT_CC_PSA); //ver 1.08
    //<-----------------------------ver 1.05:07/07/06
  }
	
//  RS485_Slave_Address = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1];
//  RS485_Master_Address = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_2];
//	RS485_Port_Sel = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_PORT_SEL];
//  RS485_Slave_Address = 1;
//  RS485_Master_Address = 1;
//	RS485_Port_Sel = SLAVE_1_SLAVE_2;
	
  Modbus.Array[MBVECTOR_ALLARMI] = 0;
  Modbus.Array[MBVECTOR_AL_RESET] = 0;//cancella flag per reset allarmi
  //Adox.Set_TIN = E2prom[E2_PAGE_1].E2_Int[E2_SET_TIN];
  
  AlarmCounter = 0;
  SetMioRele(ON, 0, 0, OUT_ALARM);
	
	
//  SetMioRele(ON, 0, 0, OUT_R3_LIKE_R10);
  InitQueue();
  Adox1500.AL2_Memo[6] = 0;
  Adox1500.RitVacuum = 0; //aggiunto in ver 3.05b

// inizio e fondo scala flussimetro aria (1 canale in mA)
  AnalogInput[1].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT1_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT1_IS]);
// inizio e fondo scala flussimetro N2 (2 canale in mA)
  AnalogInput[2].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT2_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT2_IS]);
// inizio e fondo scala pressione aria (3 canale in mA)
  AnalogInput[3].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT3_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT3_IS]);
// inizio e fondo scala pressione N2 (4 canale in mA) 
//  AnalogInput[4].Span = LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_FS]) + LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_IS]); //con segno
	//controllo il segno
	tmp_int = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_FS];
	E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_FS] = tmp_int;
  AnalogInput[4].Span = 200;//LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_FS]) + LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_IS]); //con segno
// inizio e fondo scala flussimetro aria (5 canale in mA)
  AnalogInput[5].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT5_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT5_IS]);
// inizio e fondo scala pressione (6 canale in mA)
  AnalogInput[6].Span = (int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT6_FS] - (int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT6_IS]; //con segno
// inizio e fondo scala (7 canale)
//  AnalogInput[7].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS]);
	//controllo il segno
	tmp_int = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS];
	E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS] = tmp_int;
  AnalogInput[7].Span = LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_FS]) + LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS]); //con segno
// inizio e fondo scala (8 canale)
//  AnalogInput[8].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS]);
	//controllo il segno
	tmp_int = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS];
	E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS] = tmp_int;
  AnalogInput[8].Span = LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_FS]) + LOGIC_ABS((int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS]); //con segno
// inizio e fondo scala (9 canale)
  AnalogInput[9].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS]);
// inizio e fondo scala (10 canale)
  AnalogInput[10].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT10_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT10_IS]);

  Adox1500.Mode=0;
  Adox1500.O2_Tmr_Err = RIT_O2_ALARM;
  
  Adox1500.phaseA1[0] = ADOX_N2_2016_PHASE_A1;
  Adox1500.phaseC1[0] = ADOX_N2_2016_PHASE_C1;
  Adox1500.phaseS1[0] = ADOX_N2_2016_PHASE_S1;
  Adox1500.phaseA2[0] = ADOX_N2_2016_PHASE_A2;
  Adox1500.phaseC2[0] = ADOX_N2_2016_PHASE_C2;
  Adox1500.phaseS2[0] = ADOX_N2_2016_PHASE_S2;
  Adox1500.phaseA1[1] = ADOX_200_TYCO_PHASE_A1;
  Adox1500.phaseC1[1] = ADOX_200_TYCO_PHASE_C1;
  Adox1500.phaseS1[1] = ADOX_200_TYCO_PHASE_S1;
  Adox1500.phaseA2[1] = ADOX_200_TYCO_PHASE_A2;
  Adox1500.phaseC2[1] = ADOX_200_TYCO_PHASE_C2;
  Adox1500.phaseS2[1] = ADOX_200_TYCO_PHASE_S2;
  Adox1500.phaseA1[2] = ADOX_1300_UNIVERGOMMA_PHASE_A1;
  Adox1500.phaseC1[2] = ADOX_1300_UNIVERGOMMA_PHASE_C1;
  Adox1500.phaseS1[2] = ADOX_1300_UNIVERGOMMA_PHASE_S1;
  Adox1500.phaseA2[2] = ADOX_1300_UNIVERGOMMA_PHASE_A2;
  Adox1500.phaseC2[2] = ADOX_1300_UNIVERGOMMA_PHASE_C2;
  Adox1500.phaseS2[2] = ADOX_1300_UNIVERGOMMA_PHASE_S2;
  Adox1500.phaseA1[3] = ADOX_1300_HYPOXIC_PHASE_A1;
  Adox1500.phaseC1[3] = ADOX_1300_HYPOXIC_PHASE_C1;
  Adox1500.phaseS1[3] = ADOX_1300_HYPOXIC_PHASE_S1;
  Adox1500.phaseA2[3] = ADOX_1300_HYPOXIC_PHASE_A2;
  Adox1500.phaseC2[3] = ADOX_1300_HYPOXIC_PHASE_C2;
  Adox1500.phaseS2[3] = ADOX_1300_HYPOXIC_PHASE_S2;
  
  Adox1500.Tmr_Start = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START];
	
	GuiVar_Adox_Cmd = E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS];
  
	//converte indice dell'impostazione del configuratore in baudrate reale
	if (E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE]<10){
		switch (E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE])
		{
			case 0:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 10;
				break;
			case 1:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 20;
				break;
			case 2:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 50;
				break;
			case 3:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 100;
				break;
			case 4:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 125;
				break;
			case 5:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 250;
				break;
			case 6:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 500;
				break;
			case 7:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 800;
				break;
			case 8:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 1000;
				break;
		}
    eeprom_write_word(E2_PAGE_3,E2_3_CAN_1_BAUDRATE*2,E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE]);
	}
	if (E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE]<10){
		switch (E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE])
		{
			case 0:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 10;
				break;
			case 1:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 20;
				break;
			case 2:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 50;
				break;
			case 3:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 100;
				break;
			case 4:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 125;
				break;
			case 5:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 250;
				break;
			case 6:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 500;
				break;
			case 7:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 800;
				break;
			case 8:
        E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 1000;
				break;
		}
    eeprom_write_word(E2_PAGE_3,E2_3_CAN_2_BAUDRATE*2,E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE]);
	}
//	if (E2prom[E2_PAGE_3].E2_Int[E2_3_UART1_BAUDRATE]<10){
    E2prom[E2_PAGE_3].E2_Int[E2_3_UART1_BAUDRATE] = 0x80; // SCR1 = 0x13 + E2prom[E2_PAGE_1].E2_Int[E2_SET_UART1] default "even"
//    eeprom_write_word(E2_PAGE_3,E2_3_UART1_BAUDRATE*2,E2prom[E2_PAGE_3].E2_Int[E2_3_UART1_BAUDRATE]);
//  }
//	if (E2prom[E2_PAGE_3].E2_Int[E2_3_UART2_BAUDRATE]<10){
    E2prom[E2_PAGE_3].E2_Int[E2_3_UART2_BAUDRATE] = 0x80; // SCR1 = 0x13 + E2prom[E2_PAGE_1].E2_Int[E2_SET_UART1] default "even"
//    eeprom_write_word(E2_PAGE_3,E2_3_UART2_BAUDRATE*2,E2prom[E2_PAGE_3].E2_Int[E2_3_UART2_BAUDRATE]);
//  }
	E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_PORT_SEL] = SLAVE_1_MASTER_2;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1] = 1;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_2] = 1;
	RS485_Slave_Address = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1];
  RS485_Master_Address = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_2];
	RS485_Port_Sel = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_PORT_SEL];
	
	if (GuiVar_Adox_Cmd==2)
    GuiVar_ON_OFF = 1;
	
	if ( (E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]==0) || (E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]>1) ){
	  E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP] = 1; //devo ancora inserire la schermata per impostare questo parametro (ex T quadro: OFF, LIKE-R1, temperatura da 0-50�C)
    eeprom_write_word(E2_PAGE_2,E2_2_T3_R5_TEMP*2,E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP]);
	}
}


//procedura per inizializzazione con i parametri di fabbrica
void InitSpecial (void)
{
  unsigned char index;
  unsigned char offset, page;
    
  E2prom[E2_PAGE_4].E2_Int[E2_4_AVGINDEXMAX] = 180;

  Adox1500.Tipo |= TYPE_2;
  E2prom[E2_PAGE_2].E2_Int[E2_2_MACHINE] = 2;
  
  //l'Adox risulta spento
  Adox1500.Stato = ADOX_OFF + CMP_CICLO1;
   
//  E2prom[E2_PAGE_1].E2_Int[E2_1_O2_ZEROADC] = 0;
//  E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPANADC] = 480;
//  E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPAN] = 208;
  E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET] = 5;
  E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_ISTERESI] = 1;
  E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA] = 600;
  E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA] = 0;
//  O2ZeroADC = E2prom[E2_PAGE_1].E2_Int[E2_1_O2_ZEROADC];
//  O2SpanADC = E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPANADC];
//  O2Span = E2prom[E2_PAGE_1].E2_Int[E2_1_O2_SPAN];
  Adox1500.SetO2 = E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET];
  Adox1500.Isteresi = E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_ISTERESI];
  Adox1500.Assorbimento = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA];
  Adox1500.Compensazione = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA];

//  E2prom[E2_PAGE_2].E2_Int[E2_2_RITARDOAL11] = 10;

  E2prom[E2_PAGE_2].E2_Int[E2_2_MEDIAMOBILE] = 0;
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO] = 0;
//  E2prom[E2_PAGE_2].E2_Int[E2_2_REINTEGRO2] = 0;
  E2prom[E2_PAGE_2].E2_Int[E2_2_SFASAMENTO] = 250;
  
//  Adox1500.Al11enabled = E2prom[E2_PAGE_2].E2_Int[E2_2_RITARDOAL11];
//  Lingua = 0;
//  Backlight = 10;
//  E2prom[E2_PAGE_1].E2_Int[E2LINGUA] = Lingua;
//  E2prom[E2_PAGE_1].E2_Int[E2_LUCE] = Backlight;
  Adox1500.CounterSec = 0;
	// salva secondi su RAM tamponata
	RTC_WriteGPREG(LPC_RTC,RTC_REG_0,Adox1500.CounterSec);

//  rtc_wrram(0,REG_ADOXTIME);  // byte basso
//  Adox1500.SecondiOn = 2;
//  Adox1500.Allarme &= (~0x0100);
//  Adox1500.Allarme &= (~0x0400);
//  Adox1500.Allarme &= (~0x0200);
//  Adox1500.Allarme &= (~0x0800);
//  Adox1500.Ore_Run = 0;
//  Adox1500.TmrService = 500;

  E2prom[E2_PAGE_4].E2_Int[E2_4_HALFDAY] = 0;
  E2prom[E2_PAGE_4].E2_Int[E2_4_NEXT_SERVICE] = 5000;
  E2prom[E2_PAGE_4].E2_Int[E2_4_SERVICE] = 5000;
   
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1] = 0;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_2] = 0;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_PORT_SEL] = MASTER_1_MASTER_2;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1] = 1;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_2] = 1;
  E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_PORT_SEL] = SLAVE_1_MASTER_2;
  E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE] = 125;
	E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE] = 125;
  	

//  E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP1] = 0;
//  E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP2] = 0;
//  E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP3] = 0;
//  E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP4] = 0;
//  E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP5] = 0;
//  E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP6] = 0;
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_1_ZEROADC] = 12519;   // ADC corrispondente a 4 mA da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_1_SPANADC] = 62625;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_2_ZEROADC] = 12529;   // ADC corrispondente a 4 mA da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_2_SPANADC] = 62655;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_3_ZEROADC] = 12510;   // ADC corrispondente a 4 mA da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_3_SPANADC] = 62615;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_ZEROADC] = 12520;   // ADC corrispondente a 4 mA da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_SPANADC] = 62640;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_5_ZEROADC] = 12509;   // ADC corrispondente a 4 mA da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_5_SPANADC] = 62610;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_6_ZEROADC] = 12529;   // ADC corrispondente a 4 mA da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_6_SPANADC] = 62655;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
//  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC] = 12529;   // ADC corrispondente a 4 mA da ricalcolare in taratura
//  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_SPANADC] = 62655;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
//  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_ZEROADC] = 12529;   // ADC corrispondente a 4 mA da ricalcolare in taratura
//  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_SPANADC] = 62655;   // ADC corrispondente a 20 mA  da ricalcolare in taratura
	E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC] = 48000;   // ADC corrispondente a -20�c da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_SPANADC] = 65000;   // ADC corrispondente a  75�c  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_ZEROADC] = 48000;   // ADC corrispondente a -10�c da ricalcolare in taratura 95.8 ohm
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_SPANADC] = 65000;   // ADC corrispondente a  70�c  da ricalcolare in taratura 127,3 ohm
	
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC] = 0;       // ADC corrispondente a 0 mV da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_SPANADC] = 8095;   // ADC corrispondente a 4 mV  da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_10_ZEROADC] = 0;      // ADC corrispondente a 0 mV da ricalcolare in taratura
  E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_10_SPANADC] = 8095;  // ADC corrispondente a 4 mV  da ricalcolare in taratura
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT1_IS] = 0;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT1_FS] = 700;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT2_IS] = 0;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT2_FS] = 740;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT3_IS] = 0;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT3_FS] = 250;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_IS] = -100;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_FS] = 100;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT5_IS] = 0;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT5_FS] = 250;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT6_IS] = -100;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT6_FS] = 100;
//  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS] = 0;
//  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_FS] = 1000;
//  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS] = 0;
//  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_FS] = 1000;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS] = -5;   //PT100
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_FS] = 75;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS] = -5;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_FS] = 75;
	
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS] = 0;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS] = 400;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT10_IS] = 0;
  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT10_FS] = 400;
//  E2prom[E2_PAGE_1].E2_Int[E2_SET_TIN] = 0; // se zero non viene preso in considerazione
  //------------------------------------------
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START] = 0;
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_SOLO_MEDIA] = 1; //visualizza solo media O2
  
//  E2prom[E2_PAGE_2].E2_Int[E2_2_LOCK] = NO;
  
  E2prom[E2_PAGE_3].E2_Int[E2_3_UART1_BAUDRATE] = 0x80; // SCR1 = 0x13 + E2prom[E2_PAGE_1].E2_Int[E2_SET_UART1] default "even"
  E2prom[E2_PAGE_3].E2_Int[E2_3_UART2_BAUDRATE] = 0x80; // SCR1 = 0x13 + E2prom[E2_PAGE_1].E2_Int[E2_SET_UART1] default "even"
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM] = 120;
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM] = 5;
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_B2_ERROR] = 0;
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2] = 140; //Press. stop
  E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2] = 120;  //Press. start

  
  E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_TYPE] = 0; // Default Adox N2 2016 - setta il tipo di macchina da cui dipende anche il modello di valvole utilizzato
//	0 = Adox N2 2016 (valvole FESTO DA)
//	1 = Adox TYCO (valvole FESTO NC)
//	2 = Adox UNIVERGOMMA (valvole FESTO doppio effetto)
//	3 = Adox HYPOXIC (valvole PNEUMAX doppio effetto o FESTO NC)
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM] = 550; //temperatura aria compressa - allarme T2 aria
  E2prom[E2_PAGE_2].E2_Int[E2_2_TOO_HIGH_TEMP] = 500; //temperatura blocco macchina - T1 blocco/stop
  E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP] = 450; //alert temperatura cabinet - T1 avviso/alert
  E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP] = 350; //temperatura avvio ventilazione cabinet R4
  E2prom[E2_PAGE_2].E2_Int[E2_2_T3_R5_TEMP] = 1;
  
  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_COMP_ON] = 20;
  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_COMP_OFF] = 40;
  
  E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS] = CMD_REMOTE;
  E2prom[E2_PAGE_2].E2_Int[E2_2_POWER_USER] = 0;
	
  //dopo ogni assegnazione deve seguire il comando di scrittura in eeprom, ma in questa funziona salvo integralmente le 4 pagine con dei cicli "for"
  for (page=0; page<E2_MAX_PAGE; page++){
    for (offset=0; offset<30; offset++){
    	eeprom_write_word(page,offset*2,E2prom[page].E2_Int[offset]);
		}
	}

  InitAdox();
}

