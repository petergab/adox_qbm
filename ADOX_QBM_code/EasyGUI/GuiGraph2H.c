/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*           2 bit (4 shade grayscale) graphics primitives library          */
/*                      Horizontal display bytes layout                     */
/*                               v6.0.8.004                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];

#ifdef GuiConst_BIT_TOPLEFT
const GuiConst_INT8U LinePattern2Left[4] = {0xFF, 0xFC, 0xF0, 0xC0};
const GuiConst_INT8U LinePattern2Right[4] = {0x03, 0x0F, 0x3F, 0xFF};
const GuiConst_INT8U LinePattern2Pixel[4] = {0x03, 0x0C, 0x30, 0xC0};
#else
const GuiConst_INT8U LinePattern2Left[4] = {0xFF, 0x3F, 0x0F, 0x03};
const GuiConst_INT8U LinePattern2Right[4] = {0xC0, 0xF0, 0xFC, 0xFF};
const GuiConst_INT8U LinePattern2Pixel[4] = {0xC0, 0x30, 0x0C, 0x03};
#endif

GuiConst_INT8U Color2BitPattern[4] = {0x00, 0x55, 0xAA, 0xFF};

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S C;

  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > sgl.CurLayerWidth - 1)
    X2 = sgl.CurLayerWidth - 1;
  if (Y2 > sgl.CurLayerHeight - 1)
    Y2 = sgl.CurLayerHeight - 1;

  sgl.ClippingX1 = X1;
  sgl.ClippingY1 = Y1;
  sgl.ClippingX2 = X2;
  sgl.ClippingY2 = Y2;
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

  X1 = X1 / 4;
  X2 = X2 / 4;

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(sgl.CurLayerBufPtr, Color2BitPattern[GuiConst_PIXEL_OFF], sgl.CurLayerBytes);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
    PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 4);
    *PixelPtr =
       (*PixelPtr & ~LinePattern2Pixel[X % 4]) |
       (Color2BitPattern[Color] & LinePattern2Pixel[X % 4]);

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  if ((X < 0) || (X >= sgl.CurLayerWidth) ||
      (Y < 0) || (Y >= sgl.CurLayerHeight))
    return (0);
  else
#ifdef GuiConst_BIT_TOPLEFT
    return ((*(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 4)) &
             LinePattern2Pixel[X % 4]) >> (2 * (X % 4)));
#else
    return ((*(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 4)) &
             LinePattern2Pixel[X % 4]) >> (2 * (1 - (X % 4))));
#endif
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, X1b, X2b;
  GuiConst_INT8U *PixelPtr;

  X1b = X1 / 4;
  X2b = X2 / 4;

  PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X1b;
  if (X1b == X2b)
    *PixelPtr = (*PixelPtr &
       ~(LinePattern2Left[X1 % 4] & LinePattern2Right[X2 % 4])) |
       (Color2BitPattern[Color] &
       (LinePattern2Left[X1 % 4] & LinePattern2Right[X2 % 4]));
  else
  {
    *PixelPtr =
       (*PixelPtr & ~LinePattern2Left[X1 % 4]) |
       (Color2BitPattern[Color] & LinePattern2Left[X1 % 4]);
    PixelPtr++;

    for (X = X1b + 1; X < X2b; X++)
    {
      *PixelPtr = Color2BitPattern[Color];
      PixelPtr++;
    }

    *PixelPtr =
       (*PixelPtr & ~LinePattern2Right[X2 % 4]) |
       (Color2BitPattern[Color] & LinePattern2Right[X2 % 4]);
  }
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Xb;
  GuiConst_INT8U Pattern;
  GuiConst_INT8U *PixelPtr;

  Xb = X / 4;
  PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + Xb;
  Pattern = Color2BitPattern[Color] & LinePattern2Pixel[X % 4];
  while (Y1 <= Y2)
  {
    *PixelPtr = (*PixelPtr & ~LinePattern2Pixel[X % 4]) | Pattern;
    PixelPtr += sgl.CurLayerLineSize;
    Y1++;
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT8U PixelPattern;
  GuiConst_INT16S N;
  GuiConst_INT8U YHeight;
  GuiConst_INT16S Bx;
  GuiConst_INT16S PY, Y2;
  GuiConst_INT8U PixN;
  GuiConst_INT16S Xb;
  GuiConst_INT8U Pattern;
  GuiConst_INT8U PixelLineSize;
  GuiConst_INT8U *PixelPtr;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (sgl.ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != sgl.CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       sgl.GuiLib_RemoteFontBuffer);
    sgl.CurRemoteFont = CharNdx;
  }
  CharPtr = &sgl.GuiLib_RemoteFontBuffer[0];
#endif

  if ((*(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

  gl.Dummy1_8U = Font->LineSize;   // To avoid compiler warning
#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  YHeight = Font->XSize;
  #else
  YHeight = Font->YSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
  PixelLineSize = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #else
  PixelLineSize = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = (YHeight + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
  PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif

#else

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif

#endif

  PY = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = *LineCtrl;
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PY < YHeight)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PY >= YHeight - 1));

      PY++;
      if (PY % 8 == 7)
      {
        LineCtrlByte = *LineCtrl;
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

    Bx = X;

    for (N = 0; N < PixelLineSize; N++)
    {
      PixelPattern = *PixelData;

      if (PixelPattern != 0)
      {
        for (PixN = 0; PixN < 8; PixN++)
        {
          if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
              (Bx + PixN >= sgl.ClippingX1) && (Bx + PixN <= sgl.ClippingX2) &&
#endif
#ifdef GuiConst_BIT_TOPLEFT
             ((PixelPattern >> PixN) & 0x01))
#else
             ((PixelPattern >> (7-PixN)) & 0x01))
#endif
          {
            Xb = (Bx + PixN) / 4;
            Y2 = Y;
            PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Xb;

            Pattern =
               Color2BitPattern[Color] & LinePattern2Pixel[(Bx + PixN) % 4];
#ifndef GuiConst_FONT_UNCOMPRESSED
            for (M = 0; M < LineRepeat; M++)
#endif
            {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
              if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                *PixelPtr = (*PixelPtr &
                   ~LinePattern2Pixel[(Bx + PixN) % 4]) | Pattern;
              Y2++;
              PixelPtr += sgl.CurLayerLineSize;
            }
          }
        }
      }

      PixelData++;
      Bx+=8;
    }

#ifdef GuiConst_FONT_UNCOMPRESSED
    PY++;
    Y++;
#else
    Y += LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT16S ByteSizeX;
  GuiConst_INT8U PrefixRom *PixelDataPtr2;
  GuiConst_INT8U B;
  GuiConst_INT16S X1b, X2b, Xb, Xbyte;
  GuiConst_INT8S BitOffset1a, BitOffset1b;
  GuiConst_INT8U BitOffsetX1, BitOffsetX2;
  GuiConst_INT8U *PixelPtr;

  SizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  ByteSizeX = (SizeX + 3) / 4;

#ifdef GuiConst_ROTATED_90DEGREE
  sgl.BitmapWriteX2 = X + SizeY - 1;
  sgl.BitmapWriteY2 = Y + SizeX - 1;
#else
  sgl.BitmapWriteX2 = X + SizeX - 1;
  sgl.BitmapWriteY2 = Y + SizeY - 1;
#endif

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST_TRANSP(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
      return;
    if (X1 < X)
      X1 = X;
    if (X2 > X + SizeX - 1)
      X2 = X + SizeX - 1;
    if (Y1 < Y)
      Y1 = Y;
    if (Y2 > Y + SizeY - 1)
      Y2 = Y + SizeY - 1;
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

    if (X < 0)
      Xb = -((3 - X) / 4);
    else
      Xb = X / 4;
    X1b = X1 / 4;
    X2b = X2 / 4;

    PixelDataPtr += ByteSizeX * (Y1 - Y) + X1b - Xb;

    BitOffset1a = 2 * (X % 4);
    if (BitOffset1a < 0)
      BitOffset1a += 8;
    BitOffset1b = 8 - BitOffset1a;
    BitOffsetX1 = X1 % 4;
    BitOffsetX2 = X2 % 4;

    while (Y1 <= Y2)
    {
      PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
      if (BitOffset1a == 0)
      {
        if (X1b == X2b)
          *PixelPtr =
             (*PixelPtr &
             ~(LinePattern2Left[BitOffsetX1] &
             LinePattern2Right[BitOffsetX2])) |
             (*PixelDataPtr &
             (LinePattern2Left[BitOffsetX1] & LinePattern2Right[BitOffsetX2]));
        else
        {
          PixelDataPtr2 = PixelDataPtr;

          *PixelPtr =
             (*PixelPtr & ~LinePattern2Left[BitOffsetX1]) |
             (*PixelDataPtr & LinePattern2Left[BitOffsetX1]);
          PixelPtr++;
          PixelDataPtr2++;

          for (Xbyte = X1b + 1; Xbyte < X2b; Xbyte++)
          {
            *PixelPtr = *PixelDataPtr2;
            PixelPtr++;
            PixelDataPtr2++;
          }

          *PixelPtr =
             (*PixelPtr & ~LinePattern2Right[BitOffsetX2]) |
             (*PixelDataPtr2 & LinePattern2Right[BitOffsetX2]);
        }
      }
      else
      {
        PixelDataPtr2 = PixelDataPtr;

        if (X1b == Xb)
#ifdef GuiConst_BIT_TOPLEFT
          B = *PixelDataPtr2 << BitOffset1a;
#else
          B = *PixelDataPtr2 >> BitOffset1a;
#endif
        else
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 1)) >> BitOffset1b) +
              (*PixelDataPtr2 << BitOffset1a);
#else
          B = ((*(PixelDataPtr2 - 1)) << BitOffset1b) +
              (*PixelDataPtr2 >> BitOffset1a);
#endif
        *PixelPtr =
           (*PixelPtr & ~LinePattern2Left[BitOffsetX1]) |
           (B & LinePattern2Left[BitOffsetX1]);
        PixelPtr++;
        PixelDataPtr2++;

        if (X2b > X1b)
        {
          for (Xbyte = X1b + 1; Xbyte < X2b; Xbyte++)
          {
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 1)) >> BitOffset1b) +
                (*PixelDataPtr2 << BitOffset1a);
#else
            B = ((*(PixelDataPtr2 - 1)) << BitOffset1b) +
                (*PixelDataPtr2 >> BitOffset1a);
#endif
            *PixelPtr = B;
            PixelPtr++;
            PixelDataPtr2++;
          }

#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 1)) >> BitOffset1b) +
              (*PixelDataPtr2 << BitOffset1a);
#else
          B = ((*(PixelDataPtr2 - 1)) << BitOffset1b) +
              (*PixelDataPtr2 >> BitOffset1a);
#endif
          *PixelPtr =
             (*PixelPtr & ~LinePattern2Right[BitOffsetX2]) |
             (B & LinePattern2Right[BitOffsetX2]);
        }
      }

      PixelDataPtr += ByteSizeX;
      Y1++;
    }
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S X, X1b, X2b;
  GuiConst_INT8U *PixelPtr;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
    while (Y1 <= Y2)
    {
      X1b = X1 / 4;
      X2b = X2 / 4;

      PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
      if (X1b == X2b)
        *PixelPtr ^= LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 4];
      else
      {
        *PixelPtr ^= LinePattern2Left[X1 % 4];
        PixelPtr++;

        for (X = X1b + 1; X < X2b; X++)
        {
          *PixelPtr = ~*PixelPtr;
          PixelPtr++;
        }

        *PixelPtr ^= LinePattern2Right[X2 % 4];
      }

      Y1++;
    }
  }
}

