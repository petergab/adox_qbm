/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2012 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                         Graphics include library                         */
/*                               v6.0.4.003                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//==============================================================================
#define GuiLib_DEG              10
#define GuiLib_DEG360           360 * GuiLib_DEG

#define GuiLib_RAD              4096
#define GuiLib_RAD_QUARTER_PI   3217
#define GuiLib_RAD_HALF_PI      6434
#define GuiLib_RAD_PI           12868
#define GuiLib_RAD_2_PI         25736
#define GuiLib_RAD_TO_DEG       573

//==============================================================================

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_DegToRad(
   GuiConst_INT32S Angle)
{
  return ((GuiLib_RAD * Angle) / GuiLib_RAD_TO_DEG);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_RadToDeg(
   GuiConst_INT32S Angle)
{
  return ((GuiLib_RAD_TO_DEG * Angle) / GuiLib_RAD);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_SinRad(
   GuiConst_INT32S Angle)
{
  GuiConst_INT32S A, B, X2;

  Angle %= GuiLib_RAD_2_PI;

  if (Angle > GuiLib_RAD_PI)
    X2 = GuiLib_RAD_2_PI - Angle;
  else
    X2 = Angle;
  if (X2 > GuiLib_RAD_HALF_PI)
    X2 = GuiLib_RAD_PI - X2;

  A = ((((X2 * X2) >> 12) * X2) / 6) >> 12;
  B = ((((A * X2) >> 12) * X2) / 20) >> 12;
  B = X2 - A + B - ((((B * X2) / 42) * X2) >> 24);

  if (Angle > GuiLib_RAD_PI)
    return (-B);
  else
    return (B);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_SinDeg(
   GuiConst_INT32S Angle)
{
  return (GuiLib_SinRad(GuiLib_DegToRad(Angle)));
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_CosRad(
   GuiConst_INT32S Angle)
{
  GuiConst_INT32S A, B, C, X2;

  Angle %= GuiLib_RAD_2_PI;

  if (Angle > GuiLib_RAD_PI)
    Angle = GuiLib_RAD_2_PI - Angle;
  if (Angle > GuiLib_RAD_HALF_PI)
    X2 = GuiLib_RAD_PI - Angle;
  else
    X2 = Angle;

  A = (X2 * X2) >> 13;
  B = ((((A * X2) >> 12) * X2) / 12) >> 12;
  C = (((B * X2) / 30) * X2) >> 24;
  C = GuiLib_RAD - A + B - C + (((((C * X2) / 8) * X2) / 7) >> 24);
  if (Angle > GuiLib_RAD_HALF_PI)
    return (-C);
  else
    return (C);
}

//------------------------------------------------------------------------------
GuiConst_INT32S GuiLib_CosDeg(
   GuiConst_INT32S Angle)
{
  return (GuiLib_CosRad(GuiLib_DegToRad(Angle)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_Sqrt(
   GuiConst_INT32U X)
{
  GuiConst_INT32U X1, X2;

  if (X == 0)
    return (0);

  X1 = (X / 2) + 1;
  X2 = (X1 + (X / X1)) / 2;
  while (X2 < X1)
  {
    X1 = X2;
    X2 = (X1 + (X / X1)) / 2;
  }
  return (X1);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_GetRedRgbColor(
   GuiConst_INT32U RgbColor)
{
  return (RgbColor & 0x000000FF);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_SetRedRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U RedColor)
{
  return ((RgbColor & 0x00FFFF00) | RedColor);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_GetGreenRgbColor(
   GuiConst_INT32U RgbColor)
{
  return (GuiConst_INT8U)((RgbColor & 0x0000FF00) >> 8);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_SetGreenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U GreenColor)
{
  return ((RgbColor & 0x00FF00FF) | ((GuiConst_INT32U)GreenColor << 8));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_GetBlueRgbColor(
   GuiConst_INT32U RgbColor)
{
  return (GuiConst_INT8U)((RgbColor & 0x00FF0000) >> 16);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_SetBlueRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U BlueColor)
{
  return ((RgbColor & 0xFF00FFFF) | ((GuiConst_INT32U)BlueColor << 16));
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_RgbToPixelColor(
   GuiConst_INT32U RgbColor)
{
#ifdef GuiConst_COLOR_MODE_GRAY
  return ((GuiConst_COLOR_MAX *
     (GuiConst_INT16U)GuiLib_RgbColorToGrayScale(RgbColor)) / 255);
#else
#ifdef GuiConst_COLOR_MODE_PALETTE
  GuiConst_INT16U ColorR, ColorG, ColorB;
  GuiConst_INT16U I, I2;
  GuiConst_INT16U Rating, NewRating;
  GuiConst_INT32U PColor;

  ColorR = RgbColor & 0x000000FF;
  ColorG = (RgbColor & 0x0000FF00) >> 8;
  ColorB = (RgbColor & 0x00FF0000) >> 16;
#ifndef GuiConst_COLOR_RGB_STANDARD
  ColorR = (GuiConst_COLORCODING_R_MAX * ColorR) / 255;
  ColorG = (GuiConst_COLORCODING_G_MAX * ColorG) / 255;
  ColorB = (GuiConst_COLORCODING_B_MAX * ColorB) / 255;
#endif
  Rating = 0xFFFF;
  for (I = 0; I < GuiConst_PALETTE_SIZE; I++)
  {
    PColor = (GuiConst_INT32U)GuiStruct_Palette[I][0] |
            ((GuiConst_INT32U)GuiStruct_Palette[I][1] << 8) |
            ((GuiConst_INT32U)GuiStruct_Palette[I][2] << 16);
    NewRating =
       abs(ColorR - ((PColor & GuiConst_COLORCODING_R_MASK) >>
       GuiConst_COLORCODING_R_START)) +
       abs(ColorG - ((PColor & GuiConst_COLORCODING_G_MASK) >>
       GuiConst_COLORCODING_G_START)) +
       abs(ColorB - ((PColor & GuiConst_COLORCODING_B_MASK) >>
       GuiConst_COLORCODING_B_START));
    if (NewRating < Rating)
    {
      Rating = NewRating;
      I2 = I;
    }
  }
  return (I2);
#else
#ifdef GuiConst_COLOR_RGB_STANDARD
  return (RgbColor);
#else
  return ((((GuiConst_COLORCODING_R_MAX *
     (RgbColor & 0x000000FF)) / 255) << GuiConst_COLORCODING_R_START) |
     (((GuiConst_COLORCODING_G_MAX *
     ((RgbColor & 0x0000FF00) >> 8)) / 255) << GuiConst_COLORCODING_G_START) |
     (((GuiConst_COLORCODING_B_MAX *
     ((RgbColor & 0x00FF0000) >> 16)) / 255) << GuiConst_COLORCODING_B_START));
#endif
#endif
#endif
}
//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_Rgb888ToPixelColor(
   GuiConst_INT8U Red, GuiConst_INT8U Green, GuiConst_INT8U Blue)
{
#ifdef GuiConst_COLOR_MODE_GRAY
  return ((GuiConst_COLOR_MAX *
     (GuiConst_INT16U)GuiLib_Rgb888ColorToGrayScale(Red, Green, Blue)) / 255);
#else
#ifdef GuiConst_COLOR_MODE_PALETTE
  GuiConst_INT16U ColorR, ColorG, ColorB;
  GuiConst_INT16U I, I2;
  GuiConst_INT16U Rating, NewRating;
  GuiConst_INT32U PColor;

  ColorR = Red;
  ColorG = Green;
  ColorB = Blue;
#ifndef GuiConst_COLOR_RGB_STANDARD
  ColorR = (GuiConst_COLORCODING_R_MAX * ColorR) / 255;
  ColorG = (GuiConst_COLORCODING_G_MAX * ColorG) / 255;
  ColorB = (GuiConst_COLORCODING_B_MAX * ColorB) / 255;
#endif
  Rating = 0xFFFF;
  for (I = 0; I < GuiConst_PALETTE_SIZE; I++)
  {
    PColor = (GuiConst_INT32U)GuiStruct_Palette[I][0] |
            ((GuiConst_INT32U)GuiStruct_Palette[I][1] << 8) |
            ((GuiConst_INT32U)GuiStruct_Palette[I][2] << 16);
    NewRating =
       abs(ColorR - ((PColor & GuiConst_COLORCODING_R_MASK) >>
       GuiConst_COLORCODING_R_START)) +
       abs(ColorG - ((PColor & GuiConst_COLORCODING_G_MASK) >>
       GuiConst_COLORCODING_G_START)) +
       abs(ColorB - ((PColor & GuiConst_COLORCODING_B_MASK) >>
       GuiConst_COLORCODING_B_START));
    if (NewRating < Rating)
    {
      Rating = NewRating;
      I2 = I;
    }
  }
  return (I2);
#else
#ifdef GuiConst_COLOR_RGB_STANDARD
  return (Red | (Green << 8) | (Blue << 16));
#else
  return ((((GuiConst_COLORCODING_R_MAX * Red) / 255)
              << GuiConst_COLORCODING_R_START) |
          (((GuiConst_COLORCODING_G_MAX * Green) / 255)
              << GuiConst_COLORCODING_G_START) |
          (((GuiConst_COLORCODING_B_MAX * Blue) / 255)
              << GuiConst_COLORCODING_B_START));
#endif
#endif
#endif
}
//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_PixelToRgbColor(
   GuiConst_INTCOLOR PixelColor)
{
#ifdef GuiConst_COLOR_MODE_GRAY
  GuiConst_INT16U GrayValue;

  GrayValue = (255 * PixelColor) / GuiConst_COLOR_MAX;
  return (GuiLib_GrayScaleToRgbColor((GuiConst_INT8U)GrayValue));
#else
  GuiConst_INT32U PColor;
#ifndef GuiConst_COLOR_RGB_STANDARD
  GuiConst_INT32U ColorR, ColorG, ColorB;
#endif

#ifdef GuiConst_COLOR_MODE_PALETTE
  PColor = (GuiConst_INT32U)GuiStruct_Palette[PixelColor][0] |
          ((GuiConst_INT32U)GuiStruct_Palette[PixelColor][1] << 8) |
          ((GuiConst_INT32U)GuiStruct_Palette[PixelColor][2] << 16);
#else
  PColor = PixelColor;
#endif

#ifdef GuiConst_COLOR_RGB_STANDARD
  return (PColor);
#else
  ColorR =
     (PColor & GuiConst_COLORCODING_R_MASK) >> GuiConst_COLORCODING_R_START;
  ColorG =
     (PColor & GuiConst_COLORCODING_G_MASK) >> GuiConst_COLORCODING_G_START;
  ColorB =
     (PColor & GuiConst_COLORCODING_B_MASK) >> GuiConst_COLORCODING_B_START;
  ColorR = (255 * ColorR) / GuiConst_COLORCODING_R_MAX;
  ColorG = (255 * ColorG) / GuiConst_COLORCODING_G_MAX;
  ColorB = (255 * ColorB) / GuiConst_COLORCODING_B_MAX;
  return (ColorR | (ColorG << 8) | (ColorB << 16));
#endif
#endif
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_RgbColorToGrayScale(
   GuiConst_INT32U RgbColor)
{
  return (((RgbColor & 0x000000FF) +
     ((RgbColor & 0x0000FF00) >> 8) +
     ((RgbColor & 0x00FF0000) >> 16)) / 3);
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_Rgb888ColorToGrayScale(
   GuiConst_INT8U Red, GuiConst_INT8U Green, GuiConst_INT8U Blue)
{
  return ((Red + Green + Blue) / 3);
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_GrayScaleToRgbColor(
   GuiConst_INT8U GrayValue)
{
  return ((GuiConst_INT32U)GrayValue |
         ((GuiConst_INT32U)GrayValue << 8) |
         ((GuiConst_INT32U)GrayValue << 16));
}

//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_PixelColorToGrayScale(
   GuiConst_INTCOLOR PixelColor)
{
  return (GuiLib_RgbColorToGrayScale(GuiLib_PixelToRgbColor(PixelColor)));
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_GrayScaleToPixelColor(
   GuiConst_INT8U GrayValue)
{
  return (GuiLib_RgbToPixelColor(GuiLib_GrayScaleToRgbColor(GrayValue)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_BrightenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  GuiConst_INT32U ColorR, ColorG, ColorB;

  if (Amount == 0)
    return (RgbColor);
  else
  {
    ColorR = 255 - ((GuiConst_INT32U)(255 - GuiLib_GetRedRgbColor(RgbColor)) *
       (1000 - Amount) / 1000);
    ColorG = 255 - ((GuiConst_INT32U)(255 - GuiLib_GetGreenRgbColor(RgbColor)) *
       (1000 - Amount) / 1000);
    ColorB = 255 - ((GuiConst_INT32U)(255 - GuiLib_GetBlueRgbColor(RgbColor)) *
       (1000 - Amount) / 1000);
    return (ColorR | (ColorG << 8) | (ColorB << 16));
  }
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_BrightenPixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_BrightenRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_DarkenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  GuiConst_INT32U ColorR, ColorG, ColorB;

  if (Amount == 0)
    return (RgbColor);
  else
  {
    ColorR = (GuiConst_INT32U)GuiLib_GetRedRgbColor(RgbColor) *
       (1000 - Amount) / 1000;
    ColorG = (GuiConst_INT32U)GuiLib_GetGreenRgbColor(RgbColor) *
       (1000 - Amount) / 1000;
    ColorB = (GuiConst_INT32U)GuiLib_GetBlueRgbColor(RgbColor) *
       (1000 - Amount) / 1000;
    return (ColorR | (ColorG << 8) | (ColorB << 16));
  }
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_DarkenPixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_DarkenRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_MiddleRgbColor(
   GuiConst_INT32U RgbColor1,
   GuiConst_INT32U RgbColor2,
   GuiConst_INT16U Amount)
{
  GuiConst_INT32S ColorR, ColorG, ColorB;

  if (Amount == 0)
    return (RgbColor1);
  else if (Amount >= 1000)
    return (RgbColor2);
  else
  {
    ColorR = (GuiConst_INT32U)GuiLib_GetRedRgbColor(RgbColor1);
    ColorR = ColorR + (((GuiConst_INT32S)Amount *
       ((GuiConst_INT32S)GuiLib_GetRedRgbColor(RgbColor2) - ColorR)) / 1000);
    ColorG = (GuiConst_INT32U)GuiLib_GetGreenRgbColor(RgbColor1);
    ColorG = ColorG + (((GuiConst_INT32S)Amount *
       ((GuiConst_INT32S)GuiLib_GetGreenRgbColor(RgbColor2) - ColorG)) / 1000);
    ColorB = (GuiConst_INT32U)GuiLib_GetBlueRgbColor(RgbColor1);
    ColorB = ColorB + (((GuiConst_INT32S)Amount *
       ((GuiConst_INT32S)GuiLib_GetBlueRgbColor(RgbColor2) - ColorB)) / 1000);
    return (ColorR | (ColorG << 8) | (ColorB << 16));
  }
}

// -----------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_MiddlePixelColor(
   GuiConst_INTCOLOR PixelColor1,
   GuiConst_INTCOLOR PixelColor2,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_MiddleRgbColor(GuiLib_PixelToRgbColor(PixelColor1),
                           GuiLib_PixelToRgbColor(PixelColor2),
                           Amount)));
}

// -----------------------------------------------------------------------------
GuiConst_INT32U GuiLib_DesaturateRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  GuiConst_INT32U GrayValue;
  GuiConst_INTCOLOR ColorDesat;

  GrayValue = (GuiConst_INT32U)GuiLib_RgbColorToGrayScale(RgbColor);
  ColorDesat = GrayValue | (GrayValue << 8) | (GrayValue << 16);
  return (GuiLib_MiddleRgbColor(RgbColor, ColorDesat, Amount));
}

// -----------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_DesaturatePixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_DesaturateRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
GuiConst_INT32U GuiLib_AccentuateRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount)
{
  if (Amount == 0)
    return (RgbColor);
  else if (GuiLib_RgbColorToGrayScale(RgbColor) <= 127)
    return (GuiLib_BrightenRgbColor(RgbColor, Amount));
  else
    return (GuiLib_DarkenRgbColor(RgbColor, Amount));
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_AccentuatePixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount)
{
  return (GuiLib_RgbToPixelColor(
     GuiLib_AccentuateRgbColor(GuiLib_PixelToRgbColor(PixelColor), Amount)));
}

//------------------------------------------------------------------------------
void Ellipse(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius1,
   GuiConst_INT16U Radius2,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor,
   GuiConst_INT8U Q1,
   GuiConst_INT8U Q2,
   GuiConst_INT8U Q3,
   GuiConst_INT8U Q4)
{
  GuiConst_INT16S PX, PY;
  GuiConst_INT16S L1, L2, LP1, LP2, LX, LY;
  GuiConst_INT16S PX2, PY2;
  GuiConst_INTCOLOR BColor;
  GuiConst_INT8U Filling, LFilling, First;
  GuiConst_INT16S R1, R2;
  GuiConst_INT32S R22;
  GuiConst_INT8U Q12, Q34, Q14, Q23, Qfull;

  Filling = (FillColor != GuiLib_NO_COLOR);
  if (BorderColor == GuiLib_NO_COLOR)
  {
    if (!Filling)
      return;
    else
      BColor = (GuiConst_INTCOLOR)FillColor;
  }
  else
    BColor = (GuiConst_INTCOLOR)BorderColor;

  Q12 = (Q1 || Q2);
  Q34 = (Q3 || Q4);
  Q14 = (Q1 || Q4);
  Q23 = (Q2 || Q3);
  Qfull = Q1 & Q2 & Q3 & Q4;
  if (!(Q12 || Q34))
    return;

  if (Radius1 == 0)
  {
    if (Qfull)
      GuiLib_VLine(X, Y - Radius2, Y + Radius2, BColor);
    else if (Q12)
      GuiLib_VLine(X, Y, Y + Radius2, BColor);
    else
      GuiLib_VLine(X, Y - Radius2, Y, BColor);
  }
  else if (Radius2 == 0)
  {
    if (Qfull)
      GuiLib_HLine(X - Radius1, X + Radius1, Y, BColor);
    else if (Q14)
      GuiLib_HLine(X, X + Radius1, Y, BColor);
    else
      GuiLib_HLine(X - Radius1, X, Y, BColor);
  }
  else if (Radius1 == 1)
  {
    PY = Radius2 / 2;
    if (Q23)
    {
      if (Q2 && Q3)
        GuiLib_VLine(X - 1, Y - PY, Y + PY, BColor);
      else if (Q2)
        GuiLib_VLine(X - 1, Y, Y + PY, BColor);
      else
        GuiLib_VLine(X - 1, Y - PY, Y, BColor);
    }
    if (Q34)
      GuiLib_VLine(X, Y - Radius2, Y - PY - 1, BColor);
    if (Filling)
    {
      if (Q12 && Q34)
        GuiLib_VLine(X, Y - PY, Y + PY, (GuiConst_INTCOLOR)FillColor);
      else if (Q12)
        GuiLib_VLine(X, Y, Y + PY, (GuiConst_INTCOLOR)FillColor);
      else
        GuiLib_VLine(X, Y - PY, Y, (GuiConst_INTCOLOR)FillColor);
    }
    if (Q12)
      GuiLib_VLine(X, Y + PY + 1, Y + Radius2, BColor);
    if (Q14)
    {
      if (Q1 && Q4)
        GuiLib_VLine(X + 1, Y - PY, Y + PY, BColor);
      else if (Q1)
        GuiLib_VLine(X + 1, Y, Y + PY, BColor);
      else
        GuiLib_VLine(X + 1, Y - PY, Y, BColor);
    }
  }
  else if (Radius2 == 1)
  {
    PX = Radius1 / 2;
    if (Q34)
    {
      if (Q3 && Q4)
        GuiLib_HLine(X - PX, X + PX, Y - 1, BColor);
      else if (Q4)
        GuiLib_HLine(X, X + PX, Y - 1, BColor);
      else
        GuiLib_HLine(X - PX, X, Y - 1, BColor);
    }
    if (Q23)
      GuiLib_HLine(X - Radius1, X - PX - 1, Y, BColor);
    if (Filling)
    {
      if (Q14 && Q23)
        GuiLib_HLine(X - PX, X + PX, Y, (GuiConst_INTCOLOR)FillColor);
      else if (Q14)
        GuiLib_HLine(X, X + PX, Y, (GuiConst_INTCOLOR)FillColor);
      else
        GuiLib_HLine(X - PX, X, Y, (GuiConst_INTCOLOR)FillColor);
    }
    if (Q14)
      GuiLib_HLine(X + PX + 1, X + Radius1, Y, BColor);
    if (Q12)
    {
      if (Q1 && Q2)
        GuiLib_HLine(X - PX, X + PX, Y + 1, BColor);
      else if (Q1)
        GuiLib_HLine(X, X + PX, Y + 1, BColor);
      else
        GuiLib_HLine(X - PX, X, Y + 1, BColor);
    }
  }
  else
  {
    R1 = Radius1 << 3;
    R2 = Radius2 << 3;

    if (Radius1 >= Radius2)
    {
      R22 = (GuiConst_INT32S)R2 * R2;

      PX = 0;
      First = 1;
      for (PY = R2 - 4; PY >= 4; PY -= 8)
      {
        PX2 = PX + 8;
        PX = (GuiConst_INT16S)((R1 * GuiLib_Sqrt((0x00000400 -
           ((((GuiConst_INT32S)PY * (PY + 3)) << 10) / R22)) << 8)) >> 9) + 1;
        LY = (PY + 4) >> 3;
        if (First)
        {
          L1 = X - (PX >> 3);
          L2 = X + (PX >> 3);
          if (Q12)
          {
            if (Q1 && Q2)
              GuiLib_HLine(L1, L2, Y + LY, BColor);
            else if (Q1)
              GuiLib_HLine(X, L2, Y + LY, BColor);
            else
              GuiLib_HLine(L1, X, Y + LY, BColor);
          }
          if (Q34)
          {
            if (Q3 && Q4)
              GuiLib_HLine(L1, L2, Y - LY, BColor);
            else if (Q4)
              GuiLib_HLine(X, L2, Y - LY, BColor);
            else
              GuiLib_HLine(L1, X, Y - LY, BColor);
          }
          First = 0;
        }
        else
        {
          LP1 = X - (PX2 >> 3);
          LP2 = X + (PX2 >> 3);
          L1 = X - (PX >> 3);
          L2 = X + (PX >> 3);
          if (LP1 < L1)
          {
            LP1 = L1;
            LP2 = L2;
          }
          LFilling = Filling & (LP2 - LP1 >= 2);

          if (Q12)
          {
            if (Q2)
              GuiLib_HLine(L1, LP1, Y + LY, BColor);
            if (LFilling)
            {
              if (Q1 && Q2)
                GuiLib_HLine(LP1 + 1, LP2 - 1, Y + LY,
                   (GuiConst_INTCOLOR)FillColor);
              else if (Q1)
                GuiLib_HLine(X, LP2 - 1, Y + LY, (GuiConst_INTCOLOR)FillColor);
              else
                GuiLib_HLine(LP1 + 1, X, Y + LY, (GuiConst_INTCOLOR)FillColor);
            }
            if (Q1)
              GuiLib_HLine(LP2, L2, Y + LY, BColor);
          }
          if (Q34)
          {
            if (Q3)
              GuiLib_HLine(L1, LP1, Y - LY, BColor);
            if (LFilling)
            {
              if (Q3 && Q4)
                GuiLib_HLine(LP1 + 1, LP2 - 1, Y - LY,
                   (GuiConst_INTCOLOR)FillColor);
              else if (Q4)
                GuiLib_HLine(X, LP2 - 1, Y - LY, (GuiConst_INTCOLOR)FillColor);
              else
                GuiLib_HLine(LP1 + 1, X, Y - LY, (GuiConst_INTCOLOR)FillColor);
            }
            if (Q4)
              GuiLib_HLine(LP2, L2, Y - LY, BColor);
          }
        }
      }
      L1 = (PX + 8) >> 3;
      if (L1 > Radius1)
        L1 = Radius1;

      if (Q23)
        GuiLib_HLine(X - Radius1, X - L1, Y, BColor);
      if (Filling & (L1 >= 1))
      {
        if (Q14 && Q23)
          GuiLib_HLine(X - L1 + 1, X + L1 - 1, Y, (GuiConst_INTCOLOR)FillColor);
        else if (Q14)
          GuiLib_HLine(X, X + L1 - 1, Y, (GuiConst_INTCOLOR)FillColor);
        else
          GuiLib_HLine(X - L1 + 1, X, Y, (GuiConst_INTCOLOR)FillColor);
      }
      if (Q14)
        GuiLib_HLine(X + L1, X + Radius1, Y, BColor);
    }
    else
    {
      R22 = (GuiConst_INT32S)R1 * R1;

      PY = 0;
      First = 1;
      for (PX = R1 - 4; PX >= 4; PX -= 8)
      {
        PY2 = PY + 8;
        PY = (GuiConst_INT16S)((R2 * GuiLib_Sqrt((0x00000400 -
           ((((GuiConst_INT32S)PX * (PX + 3)) << 10) / R22)) << 8)) >> 9) + 1;
        LX = (PX + 4) >> 3;
        if (First)
        {
          L1 = Y - (PY >> 3);
          L2 = Y + (PY >> 3);
          if (Q14)
          {
            if (Q1 && Q4)
              GuiLib_VLine(X + LX, L1, L2, BColor);
            else if (Q1)
              GuiLib_VLine(X + LX, Y, L2, BColor);
            else
              GuiLib_VLine(X + LX, L1, Y, BColor);
          }
          if (Q23)
          {
            if (Q2 && Q3)
              GuiLib_VLine(X - LX, L1, L2, BColor);
            else if (Q2)
              GuiLib_VLine(X - LX, Y, L2, BColor);
            else
              GuiLib_VLine(X - LX, L1, Y, BColor);
          }
          First = 0;
        }
        else
        {
          LP1 = Y - (PY2 >> 3);
          LP2 = Y + (PY2 >> 3);
          L1 = Y - (PY >> 3);
          L2 = Y + (PY >> 3);
          if (LP1 < L1)
          {
            LP1 = L1;
            LP2 = L2;
          }
          LFilling = Filling & (LP2 - LP1 >= 2);

          if (Q14)
          {
            if (Q4)
              GuiLib_VLine(X + LX, L1, LP1, BColor);
            if (LFilling)
            {
              if (Q1 && Q4)
                GuiLib_VLine(X + LX, LP1 + 1, LP2 - 1,
                   (GuiConst_INTCOLOR)FillColor);
              else if (Q1)
                GuiLib_VLine(X + LX, Y, LP2 - 1, (GuiConst_INTCOLOR)FillColor);
              else
                GuiLib_VLine(X + LX, LP1 + 1, Y, (GuiConst_INTCOLOR)FillColor);
            }
            if (Q1)
              GuiLib_VLine(X + LX, LP2, L2, BColor);
          }
          if (Q23)
          {
            if (Q3)
              GuiLib_VLine(X - LX, L1, LP1, BColor);
            if (LFilling)
            {
              if (Q2 && Q3)
                GuiLib_VLine(X - LX, LP1 + 1, LP2 - 1,
                   (GuiConst_INTCOLOR)FillColor);
              else if (Q2)
                GuiLib_VLine(X - LX, Y, LP2 - 1, (GuiConst_INTCOLOR)FillColor);
              else
                GuiLib_VLine(X - LX, LP1 + 1, Y, (GuiConst_INTCOLOR)FillColor);
            }
            if (Q2)
              GuiLib_VLine(X - LX, LP2, L2, BColor);
          }
        }
      }
      L1 = (PY + 8) >> 3;
      if (L1 > Radius2)
        L1 = Radius2;

      if (Q34)
        GuiLib_VLine(X, Y - Radius2, Y - L1, BColor);
      if (Filling & (L1 >= 1))
      {
        if (Q12 && Q34)
          GuiLib_VLine(X, Y - L1 + 1, Y + L1 - 1, (GuiConst_INTCOLOR)FillColor);
        else if (Q12)
          GuiLib_VLine(X, Y, Y + L1 - 1, (GuiConst_INTCOLOR)FillColor);
        else
          GuiLib_VLine(X, Y - L1 + 1, Y, (GuiConst_INTCOLOR)FillColor);
      }
      if (Q12)
        GuiLib_VLine(X, Y + L1, Y + Radius2, BColor);
    }
  }
}

//------------------------------------------------------------------------------
void Circle(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius,
   GuiConst_INTCOLOR Color,
   GuiConst_INT8U S0,
   GuiConst_INT8U S1,
   GuiConst_INT8U S2,
   GuiConst_INT8U S3,
   GuiConst_INT8U S4,
   GuiConst_INT8U S5,
   GuiConst_INT8U S6,
   GuiConst_INT8U S7,
   GuiConst_INT8U IF)
{
  GuiConst_INT16S PX, PY, PX2;
  GuiConst_INT16S L1, L2, LP1, LP2, LX, LY, LX2;
  GuiConst_INT8U First;
  GuiConst_INT16S R;
  GuiConst_INT32S R2;

  if (!(S0 || S1 || S2 || S3 || S4 || S5 || S6 || S7))
    return;
  if (Radius == 0)
    return;

  if (Radius == 1)
  {
    if (S7 || S0)
      GuiLib_Dot(X + 1, Y, Color);
    if (S1 || S2)
      GuiLib_Dot(X, Y + 1, Color);
    if (S3 || S4)
      GuiLib_Dot(X - 1, Y, Color);
    if (S5 || S6)
      GuiLib_Dot(X, Y - 1, Color);
  }
  else
  {
    R = Radius << 3;
    R2 = (GuiConst_INT32S)R * R;
    First = 1;
    PX = 0;

    for (PY = R - 4; PY >= R / 2; PY -= 8)
    {
      PX2 = PX + 8;
      PX = (GuiConst_INT16S)((R * GuiLib_Sqrt((0x00000400 -
         ((((GuiConst_INT32S)PY * (PY + 3)) << 10) / R2)) << 8)) >> 9) + 1;
      LX2 = (PX2 + 4) >> 3;
      LX = (PX + 4) >> 3;
      LY = (PY + 4) >> 3;
      if (First)
      {
        if (S1 || S2)
        {
          if (S1)
            L2 = X + LX;
          else
            L2 = X;
          if (S2)
            L1 = X - LX;
          else
            L1 = X;
          GuiLib_HLine(L1, L2, Y + LY, Color);
          if (IF)
            GuiLib_HLine(L1, L2, Y + LY - 1, Color);
        }
        if (S5 || S6)
        {
          if (S5)
            L1 = X - LX;
          else
            L1 = X;
          if (S6)
            L2 = X + LX;
          else
            L2 = X;
          GuiLib_HLine(L1, L2, Y - LY, Color);
          if (IF)
            GuiLib_HLine(L1, L2, Y - LY +1, Color);
        }

        if (S3 || S4)
        {
          if (S3)
            L2 = Y + LX;
          else
            L2 = Y;
          if (S4)
            L1 = Y - LX;
          else
            L1 = Y;
          GuiLib_VLine(X - LY, L1, L2, Color);
          if (IF)
            GuiLib_VLine(X - LY + 1, L1, L2, Color);
        }
        if (S7 || S0)
        {
          if (S7)
            L1 = Y - LX;
          else
            L1 = Y;
          if (S0)
            L2 = Y + LX;
          else
            L2 = Y;
          GuiLib_VLine(X + LY, L1, L2, Color);
          if (IF)
            GuiLib_VLine(X + LY - 1, L1, L2, Color);
        }

        First = 0;
      }
      else if (LX < LY)
      {
        LP1 = X - LX2;
        LP2 = X + LX2;
        L1 = X - LX;
        L2 = X + LX;

        if (S1)
        {
          GuiLib_HLine(LP2, L2, Y + LY, Color);
          if (IF)
            GuiLib_HLine(LP2, L2, Y + LY - 1, Color);
        }
        if (S2)
        {
          GuiLib_HLine(L1, LP1, Y + LY, Color);
          if (IF)
            GuiLib_HLine(L1, LP1, Y + LY - 1, Color);
        }

        if (S5)
        {
          GuiLib_HLine(L1, LP1, Y - LY, Color);
          if (IF)
            GuiLib_HLine(L1, LP1, Y - LY + 1, Color);
        }
        if (S6)
        {
          GuiLib_HLine(LP2, L2, Y - LY, Color);
          if (IF)
            GuiLib_HLine(LP2, L2, Y - LY + 1, Color);
        }

        LP1 = Y - LX2;
        LP2 = Y + LX2;
        L1 = Y - LX;
        L2 = Y + LX;

        if (S7)
        {
          GuiLib_VLine(X + LY, L1, LP1, Color);
          if (IF)
            GuiLib_VLine(X + LY - 1, L1, LP1, Color);
        }
        if (S0)
        {
          GuiLib_VLine(X + LY, LP2, L2, Color);
          if (IF)
            GuiLib_VLine(X + LY - 1, LP2, L2, Color);
        }

        if (S3)
        {
          GuiLib_VLine(X - LY, LP2, L2, Color);
          if (IF)
            GuiLib_VLine(X - LY + 1, LP2, L2, Color);
        }
        if (S4)
        {
          GuiLib_VLine(X - LY, L1, LP1, Color);
          if (IF)
            GuiLib_VLine(X - LY + 1, L1, LP1, Color);
        }
      }
      else
      {
        if (S0)
        {
          GuiLib_Dot(X + LY, Y + LX2, Color);
          if (IF)
            GuiLib_Dot(X + LY - 1, Y + LX2, Color);
        }
        if (S1)
        {
          GuiLib_Dot(X + LX2, Y + LY, Color);
          if (IF)
            GuiLib_Dot(X + LX2, Y + LY - 1, Color);
        }
        if (S2)
        {
          GuiLib_Dot(X - LX2, Y + LY, Color);
          if (IF)
            GuiLib_Dot(X - LX2, Y + LY - 1, Color);
        }
        if (S3)
        {
          GuiLib_Dot(X - LY, Y + LX2, Color);
          if (IF)
            GuiLib_Dot(X - LY + 1, Y + LX2, Color);
        }
        if (S4)
        {
          GuiLib_Dot(X - LY, Y - LX2, Color);
          if (IF)
            GuiLib_Dot(X - LY + 1, Y - LX2, Color);
        }
        if (S5)
        {
          GuiLib_Dot(X - LX2, Y - LY, Color);
          if (IF)
            GuiLib_Dot(X - LX2, Y - LY + 1, Color);
        }
        if (S6)
        {
          GuiLib_Dot(X + LX2, Y - LY, Color);
          if (IF)
            GuiLib_Dot(X + LX2, Y - LY + 1, Color);
        }
        if (S7)
        {
          GuiLib_Dot(X + LY, Y - LX2, Color);
          if (IF)
            GuiLib_Dot(X + LY - 1, Y - LX2, Color);
        }

        break;
      }
    }

    if (S7 || S0)
      GuiLib_Dot(X + Radius, Y, Color);
    if (S3 || S4)
      GuiLib_Dot(X - Radius, Y, Color);
  }
}

//------------------------------------------------------------------------------
void FilledCircle(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S PX, PY, PX2;
  GuiConst_INT16S LX, LY, LXY, LX2;
  GuiConst_INT16S R;
  GuiConst_INT32S R2;

  if (Radius == 0)
    return;

  if (Radius == 1)
  {
    GuiLib_Dot(X, Y - 1, Color);
    GuiLib_HLine(X - 1, X + 1, Y, Color);
    GuiLib_Dot(X, Y + 1, Color);
  }
  else
  {
    R = Radius << 3;
    R2 = (GuiConst_INT32S)R * R;
    LXY = (707 * (GuiConst_INT32S)Radius) / 1000;
    PX = 0;

    for (PY = R - 4; PY >= R / 2; PY -= 8)
    {
      PX2 = PX + 8;
      PX = (GuiConst_INT16S)((R * GuiLib_Sqrt((0x00000400 -
         ((((GuiConst_INT32S)PY * (PY + 3)) << 10) / R2)) << 8)) >> 9) + 1;
      LX2 = (PX2 + 4) >> 3;
      LX = (PX + 4) >> 3;
      LY = (PY + 4) >> 3;

      if (LX < LY)
      {
        GuiLib_HLine(X - LX, X + LX, Y + LY, Color);
        GuiLib_HLine(X - LX, X + LX, Y - LY, Color);

        if (LX > LXY)
          LX = LXY;
        GuiLib_VLine(X - LY, Y - LX, Y + LX, Color);
        GuiLib_VLine(X + LY, Y - LX, Y + LX, Color);
      }
      else
      {
        GuiLib_HLine(X - LX2, X + LX2, Y - LY, Color);
        GuiLib_HLine(X - LX2, X + LX2, Y + LY, Color);
        GuiLib_VLine(X - LY, Y - LX2, Y + LX2, Color);
        GuiLib_VLine(X + LY, Y - LX2, Y + LX2, Color);

        break;
      }
    }
    GuiLib_FillBox(X - LY + 1, Y - LY + 1, X + LY - 1, Y + LY - 1, Color);
  }
}

//------------------------------------------------------------------------------
void GuiLib_Circle(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor)
{
  Ellipse(X, Y, Radius, Radius, BorderColor, FillColor, 1, 1, 1, 1);
}

//------------------------------------------------------------------------------
void GuiLib_Ellipse(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius1,
   GuiConst_INT16U Radius2,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor)
{
  Ellipse(X, Y, Radius1, Radius2, BorderColor, FillColor, 1, 1, 1, 1);
}

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
void GuiLib_ResetClipping(void)
{
  sgl.ClippingTotal = 0;
  SetClipping(
     0, 0, GuiConst_DISPLAY_WIDTH_HW - 1, GuiConst_DISPLAY_HEIGHT_HW - 1);
}

//------------------------------------------------------------------------------
void GuiLib_SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  sgl.ClippingTotal = ((X1 > X2) || (Y1 > Y2));

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

  SetClipping(X1, Y1, X2, Y2);
}
#endif

//------------------------------------------------------------------------------
void GuiLib_ResetDisplayRepaint(void)
{
  GuiConst_INT16S LineNo;

  if (!sgl.BaseLayerDrawing)
    return;

  for (LineNo = 0; LineNo < GuiConst_BYTE_LINES; LineNo++)
  {
    GuiLib_DisplayRepaint[LineNo].ByteEnd = -1;
    #ifdef GuiConst_VNC_REMOTE_SUPPORT_ON
    GuiLib_VncRepaint[LineNo].ByteEnd = -1;
    #endif
  }
}

//------------------------------------------------------------------------------
void GuiLib_MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
}

//------------------------------------------------------------------------------
void GuiLib_ClearDisplay(void)
{
  ClearDisplay();
  MarkDisplayBoxRepaint(
     0, 0, GuiConst_DISPLAY_WIDTH_HW - 1, GuiConst_DISPLAY_HEIGHT_HW - 1);
}

//------------------------------------------------------------------------------
void GuiLib_Dot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

  MakeDot(X, Y, Color);
}

//------------------------------------------------------------------------------
GuiConst_INTCOLOR GuiLib_GetDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  GuiLib_COORD_ADJUST(X, Y);

  return (ReadDot(X, Y));
}

//------------------------------------------------------------------------------
void GuiLib_Line(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, Y;
  GuiConst_INT16S PX1, PY1, PX2, PY2;
  GuiConst_INT16S RemCoord;
  GuiConst_INT32S Slope;
  GuiConst_INT16S Offset, NewOffset;
  GuiConst_INT16U OffsetCnt;
  GuiConst_INT16S OffsetDir;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  if (X1 == X2)
  {
    OrderCoord(&Y1, &Y2);
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
    {
      VertLine(X1, Y1, Y2, Color);
      MarkDisplayBoxRepaint(X1, Y1, X1, Y2);
    }
  }
  else if (Y1 == Y2)
  {
    OrderCoord(&X1, &X2);
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
    {
      HorzLine(X1, X2, Y1, Color);
      MarkDisplayBoxRepaint(X1, Y1, X2, Y1);
    }
  }
  else
  {
    Slope = labs ((10000 * (GuiConst_INT32S) (Y2 - Y1))) /
       labs ((GuiConst_INT32S) (X2 - X1));

    if (Slope <= 10000)
    {
      if (OrderCoord(&X1, &X2))
        SwapCoord(&Y1, &Y2);

      X = 0;
      RemCoord = 0;
      OffsetCnt = 0;
      if (Y1 < Y2)
        OffsetDir = 1;
      else
        OffsetDir = -1;
      do
      {
        NewOffset = (((GuiConst_INT32S) X * Slope) + 5000) / 10000;
        if (((X > 0) && (NewOffset != Offset)) || (X > X2 - X1))
        {
          PX1 = X1 + RemCoord;
          PY1 = Y1 + OffsetDir * Offset;
          PX2 = PX1 + OffsetCnt - 1;
          if (OffsetCnt == 1)
            MakeDot(PX1, PY1, Color);
          else
#ifdef GuiConst_CLIPPING_SUPPORT_ON
               if (CheckRect (&PX1, &PY1, &PX2, &PY1))
#endif
          {
            HorzLine(PX1, PX2, PY1, Color);
            MarkDisplayBoxRepaint(PX1, PY1, PX2, PY1);
          }
          OffsetCnt = 1;
          RemCoord = X;

          if (X > X2 - X1)
            return;
        }
        else
          OffsetCnt++;

        Offset = NewOffset;
        X++;
      }
      while (1);
    }
    else
    {
      if (OrderCoord(&Y1, &Y2))
        SwapCoord(&X1, &X2);

      Y = 0;
      RemCoord = 0;
      OffsetCnt = 0;
      if (X1 < X2)
        OffsetDir = 1;
      else
        OffsetDir = -1;
      do
      {
        NewOffset = (((GuiConst_INT32S) Y * 10000) + (Slope / 2)) / Slope;
        if (((Y > 0) && (NewOffset != Offset)) || (Y > Y2 - Y1))
        {
          PX1 = X1 + OffsetDir * Offset;
          PY1 = Y1 + RemCoord;
          PY2 = PY1 + OffsetCnt - 1;
          if (OffsetCnt == 1)
            MakeDot(PX1, PY1, Color);
          else
#ifdef GuiConst_CLIPPING_SUPPORT_ON
               if (CheckRect (&PX1, &PY1, &PX1, &PY2))
#endif
          {
            VertLine(PX1, PY1, PY2, Color);
            MarkDisplayBoxRepaint(PX1, PY1, PX1, PY2);
          }
          OffsetCnt = 1;
          RemCoord = Y;

          if (Y > Y2 - Y1)
            return;
        }
        else
          OffsetCnt++;

        Offset = NewOffset;
        Y++;
      }
      while (1);
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_LinePattern(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT8U LinePattern,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X,Y;
  GuiConst_INT32S Slope;
  GuiConst_INT16S Offset;
  GuiConst_INT8U MaskTemp;
  GuiConst_INT8U MaskCtr = 0;
  GuiConst_INT8U Vertical;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  Vertical = (X1 == X2);
  if (Vertical)
    Offset = 0;
  else
    Slope = labs ((10000 * (GuiConst_INT32S)(Y2 - Y1))) /
            labs ((GuiConst_INT32S)(X2 - X1));

  if (Vertical || (Slope > 10000))
  {
    if (OrderCoord(&Y1, &Y2))
      SwapCoord(&X1, &X2);

    for (Y = 0; Y <= Y2 - Y1; Y++)
    {
      if (MaskCtr == 0 )
      {
        MaskCtr = 8;
        MaskTemp = LinePattern;
      }
      if (MaskTemp & 0x01)
      {
        if (!Vertical)
          Offset = (((GuiConst_INT32S) Y * 10000) + (Slope / 2)) / Slope;
        if (X1 < X2)
          MakeDot(X1 + Offset, Y1 + Y, Color);
        else
          MakeDot(X1 - Offset, Y1 + Y, Color);
      }
      MaskTemp >>= 1;
      MaskCtr--;
    }
  }
  else
  {
    if (OrderCoord(&X1, &X2))
      SwapCoord(&Y1, &Y2);

    for (X = 0; X <= X2 - X1; X++)
    {
      if (MaskCtr == 0 )
      {
        MaskCtr = 8;
        MaskTemp = LinePattern;
      }

      if (MaskTemp & 0x01)
      {
        Offset = (((GuiConst_INT32S) X * Slope) + 5000) / 10000;
        if (Y1 < Y2)
          MakeDot(X1 + X, Y1 + Offset, Color);
        else
          MakeDot(X1 + X, Y1 - Offset, Color);
      }
      MaskTemp >>= 1;
      MaskCtr--;
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_HLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiLib_Line(X1, Y, X2, Y, Color);
}

//------------------------------------------------------------------------------
void GuiLib_VLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiLib_Line(X, Y1, X, Y2, Color);
}

//------------------------------------------------------------------------------
void GuiLib_Box(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X1C;
  GuiConst_INT16S Y1C;
  GuiConst_INT16S X2C;
  GuiConst_INT16S Y2C;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

  X1C = X1;
  Y1C = Y1;
  X2C = X2;
  Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect(&X1C, &Y1C, &X2C, &Y2C))
#endif
  {
    MarkDisplayBoxRepaint(X1C, Y1C, X2C, Y2C);
    X1C = X1;
    Y1C = Y1;
    X2C = X1;
    Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect(&X1C, &Y1C, &X1C, &Y2C))
#endif
      VertLine(X1C, Y1C, Y2C, Color);
    X1C = X2;
    Y1C = Y1;
    X2C = X2;
    Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (CheckRect(&X2C, &Y1C, &X2C, &Y2C))
#endif
      VertLine(X2C, Y1C, Y2C, Color);
    if (X2 - X1 > 1)
    {
      X1C = X1 + 1;
      Y1C = Y1;
      X2C = X2 - 1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      if (CheckRect(&X1C, &Y1C, &X2C, &Y1C))
#endif
        HorzLine(X1C, X2C, Y1C, Color);
      X1C = X1 + 1;
      Y1C = Y2;
      X2C = X2 - 1;
      Y2C = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      if (CheckRect(&X1C, &Y2C, &X2C, &Y2C))
#endif
        HorzLine(X1C, X2C, Y2C, Color);
    }
  }
}

//------------------------------------------------------------------------------
void GuiLib_FillBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);
  GuiLib_COLOR_ADJUST(Color);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
#ifdef GuiConst_BYTE_HORIZONTAL
    while (Y1 <= Y2)
    {
      HorzLine(X1, X2, Y1, Color);
      Y1++;
    }
#else
    while (X1 <= X2)
    {
      VertLine(X1, Y1, Y2, Color);
      X1++;
    }
#endif
  }
}

//------------------------------------------------------------------------------
void GuiLib_BorderBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR BorderColor,
   GuiConst_INTCOLOR FillColor)
{
  GuiLib_Box(X1, Y1, X2, Y2, BorderColor);
  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);
  if (((X2 - X1) >= 2) && ((Y2 - Y1) >= 2))
    GuiLib_FillBox(X1+1, Y1+1, X2-1, Y2-1, FillColor);
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ReadBitmapSizes(
   GuiConst_INT16U BitmapIndex)
{
#ifndef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U PrefixRom * PixelDataPtr;
#endif

#ifdef GuiConst_REMOTE_BITMAP_DATA
  if (BitmapIndex != sgl.CurRemoteBitmap)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
       (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex + 1] -
       (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
       sgl.GuiLib_RemoteBitmapBuffer);
    sgl.CurRemoteBitmap = BitmapIndex;
  }
  sgl.BitmapSizeX = (GuiConst_INT16S)sgl.GuiLib_RemoteBitmapBuffer[0] +
                    256*(GuiConst_INT16S)sgl.GuiLib_RemoteBitmapBuffer[1];
  sgl.BitmapSizeY = (GuiConst_INT16S)sgl.GuiLib_RemoteBitmapBuffer[2] +
                    256*(GuiConst_INT16S)sgl.GuiLib_RemoteBitmapBuffer[3];
#else
  PixelDataPtr =
     (GuiConst_INT8U PrefixRom *)ReadWord(GuiStruct_BitmapPtrList[BitmapIndex]);

  sgl.BitmapSizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  sgl.BitmapSizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  sgl.BitmapSizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  sgl.BitmapSizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmap(
   GuiConst_INT16U BitmapIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT32S TranspColor)
{
#ifdef GuiConst_REMOTE_BITMAP_DATA
  if (BitmapIndex != sgl.CurRemoteBitmap)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
       (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex + 1] -
       (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
       sgl.GuiLib_RemoteBitmapBuffer);
    sgl.CurRemoteBitmap = BitmapIndex;
  }
  ShowBitmapArea(&sgl.GuiLib_RemoteBitmapBuffer[0],
     X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
#else
  ShowBitmapArea(
     (GuiConst_INT8U PrefixRom *)ReadWord(GuiStruct_BitmapPtrList[BitmapIndex]),
      X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
#endif
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmapAt(
   GuiConst_INT8U * BitmapPtr,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT32S TranspColor)
{
  ShowBitmapArea(
     BitmapPtr, X, Y, 0, 0, 0, 0, TranspColor, GuiLib_FULL_BITMAP);
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmapArea(
   GuiConst_INT16U BitmapIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S AX1,
   GuiConst_INT16S AY1,
   GuiConst_INT16S AX2,
   GuiConst_INT16S AY2,
   GuiConst_INT32S TranspColor)
{
#ifdef GuiConst_REMOTE_BITMAP_DATA
  if (BitmapIndex != sgl.CurRemoteBitmap)
  {
    GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex + 1] -
     (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[BitmapIndex],
     sgl.GuiLib_RemoteBitmapBuffer);
  sgl.CurRemoteBitmap = BitmapIndex;
  }
  ShowBitmapArea(&sgl.GuiLib_RemoteBitmapBuffer[0],
   X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
#else
  ShowBitmapArea(
     (GuiConst_INT8U PrefixRom *)ReadWord(GuiStruct_BitmapPtrList[BitmapIndex]),
      X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
#endif
}

//------------------------------------------------------------------------------
void GuiLib_ShowBitmapAreaAt(
   GuiConst_INT8U * BitmapPtr,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S AX1,
   GuiConst_INT16S AY1,
   GuiConst_INT16S AX2,
   GuiConst_INT16S AY2,
   GuiConst_INT32S TranspColor)
{
  ShowBitmapArea(
     BitmapPtr, X, Y, AX1, AY1, AX2, AY2, TranspColor, GuiLib_AREA_BITMAP);
}
#endif

