/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                               v6.0.8.004                                 */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------

#include "GuiConst.h"
#include "GuiLib.h"
#include "GuiLibStruct.h"
#ifdef GuiConst_VNC_REMOTE_SUPPORT_ON
#include "GuiVnc.h"
#endif
#include <string.h>
#include <stdlib.h>

#ifndef GuiConst_PC_V6_0_8
If your compiler sees this text you are using a wrong version of the easyGUI
library. Version numbers of the easyGUI PC application and c library must match.
Only exception is GuiDisplay.c/h (your display driver), which can be kept from
version to version.
#endif

#define GuiLib_CHR_PSLEFT_OFS              0
#define GuiLib_CHR_PSRIGHT_OFS             5
#define GuiLib_CHR_XLEFT_OFS               10
#define GuiLib_CHR_XWIDTH_OFS              11
#define GuiLib_CHR_YTOP_OFS                12
#define GuiLib_CHR_YHEIGHT_OFS             13
#define GuiLib_CHR_LINECTRL_OFS            14
#define GuiLib_CHR_PS_TOP_OFS              0
#define GuiLib_CHR_PS_MID_OFS              1
#define GuiLib_CHR_PS_MIDBASE_OFS          2
#define GuiLib_CHR_PS_BASE_OFS             3
#define GuiLib_CHR_PS_BOTTOM_OFS           4



#ifdef GuiConst_ARAB_CHARS_INUSE
#define GuiLib_ARAB_LIGATURES_CNT          4
const GuiConst_INT16U GuiLib_ARAB_LIGATURES[GuiLib_ARAB_LIGATURES_CNT][3] =
   {{0x0644, 0x0622, 0xFEF5},
    {0x0644, 0x0623, 0xFEF7},
    {0x0644, 0x0625, 0xFEF9},
    {0x0644, 0x0627, 0xFEFB}};

#define GuiLib_ARAB_CHAR_PRI_MIN           0x0622
#define GuiLib_ARAB_CHAR_PRI_MAX           0x06D6
#define GuiLib_ARAB_CHAR_SEC_MIN           0xFB50
#define GuiLib_ARAB_CHAR_SEC_MAX           0xFEF4
#define GuiLib_ARAB_CHAR_TYPE_ISO          0
#define GuiLib_ARAB_CHAR_TYPE_FIN          1
#define GuiLib_ARAB_CHAR_TYPE_INI          2
#define GuiLib_ARAB_CHAR_TYPE_MED          3
#define GuiLib_ARAB_CHAR_ISOFIN            0x02
#define GuiLib_ARAB_CHAR_ISOFININIMED      0x04
#define GuiLib_ARAB_CHAR_DIACRITIC         0x0A
#define GuiLib_ARAB_CHAR_CONVERT_CNT       83

const GuiConst_INT16U GuiLib_ARAB_CHAR_CONVERT[GuiLib_ARAB_CHAR_CONVERT_CNT][3] =
   {{0x0622, 0xFE81, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0623, 0xFE83, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0624, 0xFE85, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0625, 0xFE87, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0626, 0xFE89, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0627, 0xFE8D, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0628, 0xFE8F, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0629, 0xFE93, GuiLib_ARAB_CHAR_ISOFIN},
    {0x062A, 0xFE95, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062B, 0xFE99, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062C, 0xFE9D, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062D, 0xFEA1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062E, 0xFEA5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x062F, 0xFEA9, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0630, 0xFEAB, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0631, 0xFEAD, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0632, 0xFEAF, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0633, 0xFEB1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0634, 0xFEB5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0635, 0xFEB9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0636, 0xFEBD, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0637, 0xFEC1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0638, 0xFEC5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0639, 0xFEC9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x063A, 0xFECD, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0641, 0xFED1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0642, 0xFED5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0643, 0xFED9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0644, 0xFEDD, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0645, 0xFEE1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0646, 0xFEE5, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0647, 0xFEE9, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0648, 0xFEED, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0649, 0xFEEF, GuiLib_ARAB_CHAR_ISOFIN},
    {0x064A, 0xFEF1, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x064E, 0xFE76, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x064F, 0xFE78, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0650, 0xFE7A, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0651, 0xFE7C, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0652, 0xFE7E, GuiLib_ARAB_CHAR_DIACRITIC},
    {0x0671, 0xFB50, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0679, 0xFB66, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067A, 0xFB5E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067B, 0xFB52, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067E, 0xFB56, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x067F, 0xFB62, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0680, 0xFB5A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0683, 0xFB76, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0684, 0xFB72, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0686, 0xFB7A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0687, 0xFB7E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x0688, 0xFB88, GuiLib_ARAB_CHAR_ISOFIN},
    {0x068C, 0xFB84, GuiLib_ARAB_CHAR_ISOFIN},
    {0x068D, 0xFB82, GuiLib_ARAB_CHAR_ISOFIN},
    {0x068E, 0xFB86, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0691, 0xFB8C, GuiLib_ARAB_CHAR_ISOFIN},
    {0x0698, 0xFB8A, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06A4, 0xFB6A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06A6, 0xFB6E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06A9, 0xFB8E, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06AD, 0xFBD3, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06AF, 0xFB92, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06B1, 0xFB9A, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06B3, 0xFB96, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06BA, 0xFB9E, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06BB, 0xFBA0, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06BE, 0xFBAA, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06C0, 0xFBA4, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C1, 0xFBA6, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06C5, 0xFBE0, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C6, 0xFBD9, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C7, 0xFBD7, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C8, 0xFBDB, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06C9, 0xFBE2, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06CB, 0xFBDE, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06CC, 0xFBFC, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06D0, 0xFBE4, GuiLib_ARAB_CHAR_ISOFININIMED},
    {0x06D2, 0xFBAE, GuiLib_ARAB_CHAR_ISOFIN},
    {0x06D3, 0xFBB0, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEF5, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEF7, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEF9, GuiLib_ARAB_CHAR_ISOFIN},
    {     0, 0xFEFB, GuiLib_ARAB_CHAR_ISOFIN}};
#endif

//------------------------------------------------------------------------------
//----------------------X-----------------------

GuiLib_DisplayLineRec GuiLib_DisplayRepaint[GuiConst_BYTE_LINES];
#ifdef GuiConst_VNC_REMOTE_SUPPORT_ON
GuiLib_DisplayLineRec GuiLib_VncRepaint[GuiConst_BYTE_LINES];
#endif // GuiConst_VNC_REMOTE_SUPPORT_ON

#ifdef GuiConst_REMOTE_DATA
void (*GuiLib_RemoteDataReadBlock) (
      GuiConst_INT32U SourceOffset,
      GuiConst_INT32U SourceSize,
      GuiConst_INT8U * TargetAddr);
#endif // GuiConst_REMOTE_DATA
#ifdef GuiConst_REMOTE_TEXT_DATA
void (*GuiLib_RemoteTextReadBlock) (
      GuiConst_INT32U SourceOffset,
      GuiConst_INT32U SourceSize,
      void * TargetAddr);
#endif // GuiConst_REMOTE_TEXT_DATA

#ifdef GuiConst_CURSOR_SUPPORT_ON
GuiConst_INT16S GuiLib_ActiveCursorFieldNo;
#endif
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
GuiConst_INT8U GuiLib_DisplayUpsideDown;
#endif

GuiConst_INT16S GuiLib_CurStructureNdx;
GuiConst_INT16S GuiLib_LanguageIndex;

GuiLib_GLOBAL gl;
static GuiLib_STATIC sgl;


//==============================================================================
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  #define GuiLib_COORD_ADJUST(X, Y)                                            \
  {                                                                            \
    if (GuiLib_DisplayUpsideDown)                                              \
    {                                                                          \
      X = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoX - X;                 \
      Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoY - Y;                \
    }                                                                          \
    else                                                                       \
    {                                                                          \
      X = sgl.CoordOrigoX + X;                                                 \
      Y = sgl.CoordOrigoY + Y;                                                 \
    }                                                                          \
  }

  #define GuiLib_MIRROR_BITS(B)                                                \
  {                                                                            \
    B = (((B & 0x80) >> 7) | ((B & 0x40) >> 5) |                               \
         ((B & 0x20) >> 3) | ((B & 0x10) >> 1) |                               \
         ((B & 0x08) << 1) | ((B & 0x04) << 3) |                               \
         ((B & 0x02) << 5) | ((B & 0x01) << 7));                               \
  }
#else
  #ifdef GuiConst_ROTATED_OFF
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoX - X;             \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoY - Y;            \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoX - X;             \
          Y = sgl.CoordOrigoY + Y;                                             \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoY - Y;            \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = sgl.CoordOrigoY + Y;                                             \
        }
      #endif
    #endif
  #endif
  #ifdef GuiConst_ROTATED_90DEGREE_RIGHT
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoY - Y;             \
          SwapCoord(&X, &Y);                                                   \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = sgl.CoordOrigoY + Y;                                             \
          SwapCoord(&X, &Y);                                                   \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoX - X;            \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoY - Y;             \
          SwapCoord(&X, &Y);                                                   \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoX - X;            \
          Y = sgl.CoordOrigoY + Y;                                             \
          SwapCoord(&X, &Y);                                                   \
        }
      #endif
    #endif
  #endif
  #ifdef GuiConst_ROTATED_UPSIDEDOWN
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = sgl.CoordOrigoY + Y;                                             \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoY - Y;            \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoX - X;             \
          Y = sgl.CoordOrigoY + Y;                                             \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoX - X;             \
          Y = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoY - Y;            \
        }
      #endif
    #endif
  #endif
  #ifdef GuiConst_ROTATED_90DEGREE_LEFT
    #ifdef GuiConst_MIRRORED_HORIZONTALLY
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoX - X;            \
          Y = sgl.CoordOrigoY + Y;                                             \
          SwapCoord(&X, &Y);                                                   \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = GuiConst_DISPLAY_HEIGHT_HW - 1 - sgl.CoordOrigoX - X;            \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoY - Y;             \
          SwapCoord(&X, &Y);                                                   \
        }
      #endif
    #else
      #ifdef GuiConst_MIRRORED_VERTICALLY
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = sgl.CoordOrigoY + Y;                                             \
          SwapCoord(&X, &Y);                                                   \
        }
      #else
        #define GuiLib_COORD_ADJUST(X, Y)                                      \
        {                                                                      \
          X = sgl.CoordOrigoX + X;                                             \
          Y = GuiConst_DISPLAY_WIDTH_HW - 1 - sgl.CoordOrigoY - Y;             \
          SwapCoord(&X, &Y);                                                   \
        }
      #endif
    #endif
  #endif
#endif

#define GuiLib_FONT_MID_Y(BaseLine, TopLine) ((BaseLine - TopLine + 1) / 2)

#ifdef GuiConst_COLOR_DEPTH_1
#define GuiLib_COLOR_ADJUST(C)  C &= 0x01;
#endif
#ifdef GuiConst_COLOR_DEPTH_2
#define GuiLib_COLOR_ADJUST(C)  C &= 0x03;
#endif
#ifdef GuiConst_COLOR_DEPTH_4
#define GuiLib_COLOR_ADJUST(C)  C &= 0x0F;
#endif
#ifdef GuiConst_COLOR_DEPTH_5
#define GuiLib_COLOR_ADJUST(C)  C = (C & 0x1F) << 3;
#endif
#ifndef GuiLib_COLOR_ADJUST
#define GuiLib_COLOR_ADJUST_TRANSP(C)
#define GuiLib_COLOR_ADJUST(C)
#else
#define GuiLib_COLOR_ADJUST_TRANSP(C) if (C != -1)  GuiLib_COLOR_ADJUST(C)
#endif
//----------------------X-----------------------
#define GuiLib_GET_MIN(A, B) ((A) > (B) ? (B) : (A))
#define GuiLib_GET_MAX(A, B) ((A) > (B) ? (A) : (B))
#define GuiLib_GET_MINMAX(X, A, B) ((X) > (A) ? (GuiLib_GET_MIN(X,B)) : (A))
#define GuiLib_LIMIT_MIN(X, A) if (X < A) X = A
#define GuiLib_LIMIT_MAX(X, B) if (X > B) X = B
#define GuiLib_LIMIT_MINMAX(X, A, B)                                           \
{                                                                              \
  if (X < A)                                                                   \
    X = A;                                                                     \
  else if ((B < A) && (X > A))                                                 \
    X = A;                                                                     \
  else if ((B >= A) && (X > B))                                                \
    X = B;                                                                     \
}

//==============================================================================

//------------------------------------------------------------------------------
static GuiConst_INT16S CopyBytes(GuiConst_INT8U *dst, GuiConst_INT8U *src, GuiConst_INT32S size)
{
  GuiConst_INT32S i;
  GuiConst_INT8U *d, *s;

  if (size < 0)
    return -1;

  if (size > GuiConst_DISPLAY_BYTES)
    return -1;

  d = (GuiConst_INT8U *)dst;
  s = (GuiConst_INT8U *)src;

  for (i=0;i<size;i++)
    *d++ = *s++;

  return 0;
}
//------------------------------------------------------------------------------
static void SwapCoord(
   GuiConst_INT16S * X1,
   GuiConst_INT16S * X2)
{
  GuiConst_INT16S Tmp;

  Tmp = *X1;
  *X1 = *X2;
  *X2 = Tmp;
}

//------------------------------------------------------------------------------
static GuiConst_INT8U OrderCoord(
   GuiConst_INT16S * X1,
   GuiConst_INT16S * X2)
{
  if (*X1 > *X2)
  {
    SwapCoord (X1, X2);
    return (1);
  }
  else
    return (0);
}

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static GuiConst_INT8U CheckRect(
   GuiConst_INT16S * X1,
   GuiConst_INT16S * Y1,
   GuiConst_INT16S * X2,
   GuiConst_INT16S * Y2)
{
  if (sgl.ClippingTotal ||
     (*X1 > sgl.ClippingX2) || (*X2 < sgl.ClippingX1) ||
     (*Y1 > sgl.ClippingY2) || (*Y2 < sgl.ClippingY1))
    return (0);
  else
  {
    if (*X1 < sgl.ClippingX1)
      *X1 = sgl.ClippingX1;
    if (*X2 > sgl.ClippingX2)
      *X2 = sgl.ClippingX2;
    if (*Y1 < sgl.ClippingY1)
      *Y1 = sgl.ClippingY1;
    if (*Y2 > sgl.ClippingY2)
      *Y2 = sgl.ClippingY2;
    return (1);
  }
}
#endif

//==============================================================================

#ifdef GuiConst_COLOR_DEPTH_1
  #ifdef GuiConst_BYTE_HORIZONTAL
    #include "GuiGraph1H.c"
  #else
    #include "GuiGraph1V.c"
  #endif
#endif
#ifdef GuiConst_COLOR_DEPTH_2
  #ifdef GuiConst_BYTE_HORIZONTAL
    #ifdef GuiConst_COLOR_PLANES_2
      #include "GuiGraph2H2P.c"
    #else
      #include "GuiGraph2H.c"
    #endif
  #else
    #ifdef GuiConst_COLOR_PLANES_2
      #include "GuiGraph2V2P.c"
    #else
      #include "GuiGraph2V.c"
    #endif
  #endif
#endif
#ifdef GuiConst_COLOR_DEPTH_4
  #ifdef GuiConst_BYTE_HORIZONTAL
    #include "GuiGraph4H.c"
  #else
    #include "GuiGraph4V.c"
  #endif
#endif
#ifdef GuiConst_COLOR_DEPTH_5
  #include "GuiGraph5.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_8
  #include "GuiGraph8.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_12
  #include "GuiGraph16.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_15
  #include "GuiGraph16.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_16
  #include "GuiGraph16.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_18
  #include "GuiGraph24.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_24
  #include "GuiGraph24.c"
#endif
#ifdef GuiConst_COLOR_DEPTH_32
  #include "GuiGraph32.c"
#endif

#include "GuiGraph.c"
#ifdef GuiConst_ADV_GRAPHICS_ON
//#include "GuiGraphAdv.c"
#endif

//==============================================================================
static void DrawStructure(GuiLib_StructPtr Structure, GuiConst_INT8U ColorInvert) PrefixReentrant;

#ifdef GuiConst_CURSOR_SUPPORT_ON
static void DrawCursorItem(GuiConst_INT8U CursorVisible);
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
static void ScrollBox_DrawScrollLine(GuiConst_INT8U ScrollBoxIndex, GuiConst_INT16S LineNdx);
#endif

#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
static GuiConst_INT16S IndexOfGraphicsLayer(GuiConst_INT16S GraphicsLayerIndex);
static GuiConst_INT8U GraphicsLayer_Push(GuiConst_INT8U GraphicsLayerIndex);
static GuiConst_INT8U GraphicsLayer_Pop(GuiConst_INT16S GraphicsLayerIndex);
static void GraphicsLayer_Copy(
   GuiConst_INT8U *DestAddress,
   GuiConst_INT16U DestLineSize,
   GuiConst_INT16S DestX,
   GuiConst_INT16S DestY,
   GuiConst_INT8U *SourceAddress,
   GuiConst_INT16U SourceLineSize,
   GuiConst_INT16S SourceX,
   GuiConst_INT16S SourceY,
   GuiConst_INT16U Width,
   GuiConst_INT16U Height);
#endif

//==============================================================================

//------------------------------------------------------------------------------
static void ResetLayerBufPtr(void)
{
#ifdef GuiConst_DISPLAY_BUFFER_EASYGUI
#ifdef GuiLib_COLOR_UNIT_16
  sgl.CurLayerBufPtr = &(GuiLib_DisplayBuf.Bytes[0][0]);
#else
  #ifdef GuiConst_COLOR_DEPTH_2
    #ifdef GuiConst_BYTE_HORIZONTAL
      #ifdef GuiConst_COLOR_PLANES_2
  sgl.CurLayerBufPtr = &(GuiLib_DisplayBuf[0][0][0]);
      #else
  sgl.CurLayerBufPtr = &(GuiLib_DisplayBuf[0][0]);
      #endif
    #else // GuiConst_BYTE_HORIZONTAL
      #ifdef GuiConst_COLOR_PLANES_2
  sgl.CurLayerBufPtr = &(GuiLib_DisplayBuf[0][0][0]);
      #else
  sgl.CurLayerBufPtr = &(GuiLib_DisplayBuf[0][0]);
      #endif
    #endif // GuiConst_BYTE_HORIZONTAL
  #else // GuiConst_COLOR_DEPTH_2
  sgl.CurLayerBufPtr = &(GuiLib_DisplayBuf[0][0]);
  #endif // GuiConst_COLOR_DEPTH_2
#endif // GuiLib_COLOR_UNIT_16
#else // GuiConst_DISPLAY_BUFFER_EASYGUI
  sgl.CurLayerBufPtr = 0;
#endif // GuiConst_DISPLAY_BUFFER_EASYGUI

  sgl.CurLayerLineSize = GuiConst_BYTES_PR_LINE;
  sgl.CurLayerWidth = GuiConst_DISPLAY_WIDTH_HW;
  sgl.CurLayerHeight = GuiConst_DISPLAY_HEIGHT_HW;
  sgl.CurLayerBytes = GuiConst_DISPLAY_BYTES;
  sgl.BaseLayerDrawing = 1;
}

//------------------------------------------------------------------------------
void GuiLib_Init(void)
{
  sgl.RefreshClock = 0;

  ResetLayerBufPtr();

  AutoRedraw_Init();

#ifdef GuiConst_DISPLAY_ACTIVE_AREA
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_COO_REL
  sgl.DisplayOrigoX = GuiConst_DISPLAY_ACTIVE_AREA_X1;
  sgl.DisplayOrigoY = GuiConst_DISPLAY_ACTIVE_AREA_Y1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  sgl.DisplayActiveAreaX1 = 0;
  sgl.DisplayActiveAreaY1 = 0;
#endif // GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
  sgl.DisplayActiveAreaX2 =
     GuiConst_DISPLAY_ACTIVE_AREA_X2 - GuiConst_DISPLAY_ACTIVE_AREA_X1;
  sgl.DisplayActiveAreaY2 =
     GuiConst_DISPLAY_ACTIVE_AREA_Y2 - GuiConst_DISPLAY_ACTIVE_AREA_Y1;
#else // GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
  sgl.DisplayActiveAreaX2 =
     GuiConst_DISPLAY_WIDTH - GuiConst_DISPLAY_ACTIVE_AREA_X1 - 1;
  sgl.DisplayActiveAreaY2 =
     GuiConst_DISPLAY_HEIGHT - GuiConst_DISPLAY_ACTIVE_AREA_Y1 - 1;
#endif // GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
#else // GuiConst_DISPLAY_ACTIVE_AREA_COO_REL
  sgl.DisplayOrigoX = 0;
  sgl.DisplayOrigoY = 0;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
  sgl.DisplayActiveAreaX1 = GuiConst_DISPLAY_ACTIVE_AREA_X1;
  sgl.DisplayActiveAreaY1 = GuiConst_DISPLAY_ACTIVE_AREA_Y1;
  sgl.DisplayActiveAreaX2 = GuiConst_DISPLAY_ACTIVE_AREA_X2;
  sgl.DisplayActiveAreaY2 = GuiConst_DISPLAY_ACTIVE_AREA_Y2;
#else // GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
  sgl.DisplayActiveAreaX1 = 0;
  sgl.DisplayActiveAreaY1 = 0;
  sgl.DisplayActiveAreaX2 = GuiConst_DISPLAY_WIDTH - 1;
  sgl.DisplayActiveAreaY2 = GuiConst_DISPLAY_HEIGHT - 1;
#endif // GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
#endif // GuiConst_CLIPPING_SUPPORT_ON
#endif // GuiConst_DISPLAY_ACTIVE_AREA_COO_REL
#else // GuiConst_DISPLAY_ACTIVE_AREA
  sgl.DisplayOrigoX = 0;
  sgl.DisplayOrigoY = 0;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  sgl.DisplayActiveAreaX1 = 0;
  sgl.DisplayActiveAreaY1 = 0;
  sgl.DisplayActiveAreaX2 = GuiConst_DISPLAY_WIDTH - 1;
  sgl.DisplayActiveAreaY2 = GuiConst_DISPLAY_HEIGHT - 1;
#endif // GuiConst_CLIPPING_SUPPORT_ON
#endif // GuiConst_DISPLAY_ACTIVE_AREA

#ifdef GuiConst_ADV_GRAPHICS_ON
//  GuiLib_AG_Init();
#endif

  GuiDisplay_Init();
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiLib_ResetClipping();
#endif
  GuiLib_ResetDisplayRepaint();
  GuiLib_Clear();

  sgl.DisplayWriting = 1;
  sgl.InitialDrawing = 0;
  sgl.TopLevelStructure = 0;
  sgl.SwapColors = 0;
  GuiLib_SetLanguage(0);
#ifdef GuiConst_BLINK_SUPPORT_ON
  sgl.BlinkBoxRate = 0;
#endif
  sgl.InvertBoxOn = 0;
  sgl.DrawingLevel = 0;
  GuiLib_CurStructureNdx = -1;
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  GuiLib_TouchAdjustReset();
#endif
#ifdef GuiConst_ITEM_BUTTON_INUSE
  sgl.ButtonColorOverride = GuiLib_FALSE;
  sgl.DisabledButtonColor = 0;
#endif
#ifdef GuiConst_REMOTE_DATA
  GuiLib_RemoteDataReadBlock = 0;
  #ifdef GuiConst_REMOTE_TEXT_DATA
  GuiLib_RemoteTextReadBlock = 0;
  sgl.CurRemoteText = -1;
  sgl.RemoteTextTableOfs = -1;
  #endif
  #ifdef GuiConst_REMOTE_FONT_DATA
  sgl.CurRemoteFont = -1;
  #endif
  #ifdef GuiConst_REMOTE_BITMAP_DATA
  sgl.CurRemoteBitmap = -1;
  #endif
#endif

#ifdef GuiConst_VNC_REMOTE_SUPPORT_ON
  GuiVnc_Init();
#endif
}

//------------------------------------------------------------------------------
void GuiLib_Clear(void)
{
  GuiConst_INT16S I;
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  GuiConst_INT16S N;
#endif

  GuiLib_ClearDisplay();

  GuiLib_CurStructureNdx = -1;

  AutoRedraw_Destroy();

#ifdef GuiConst_CURSOR_SUPPORT_ON
  sgl.CursorInUse = 0;
  GuiLib_ActiveCursorFieldNo = -1;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
  for (I = 0; I < GuiConst_BLINK_FIELDS_MAX; I++)
  {
    sgl.BlinkTextItems[I].InUse = 0;
    sgl.BlinkTextItems[I].Active = 0;
  }
#endif
#endif
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  sgl.TouchAreaCnt = 0;
#endif

#ifdef GuiConst_TEXTBOX_FIELDS_ON
  for (I = 0; I < GuiConst_TEXTBOX_FIELDS_MAX; I++)
    sgl.TextBoxScrollPositions[I].index = -1;
#endif

  sgl.LayerOrigoX = 0;
  sgl.LayerOrigoY = 0;

  sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
  sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  sgl.NextScrollLineReading = 0;
  sgl.GlobalScrollBoxIndex = 0;
  for (I = 0; I < GuiConst_SCROLLITEM_BOXES_MAX; I++)
  {
    sgl.ScrollBoxesAry[I].X1 = 0;
    sgl.ScrollBoxesAry[I].Y1 = 0;
    sgl.ScrollBoxesAry[I].InUse = GuiLib_SCROLL_STRUCTURE_UNDEF;
    sgl.ScrollBoxesAry[I].ScrollTopLine = 0;
    sgl.ScrollBoxesAry[I].LastScrollTopLine = 0;
    sgl.ScrollBoxesAry[I].LastMarkerLine = 0;
    sgl.ScrollBoxesAry[I].ScrollActiveLine = 0;
    sgl.ScrollBoxesAry[I].NumberOfLines = 0;
  }
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
  sgl.GlobalGraphIndex = 0;
  for (I = 0; I < GuiConst_GRAPH_MAX; I++)
  {
    sgl.GraphAry[I].InUse = GuiLib_GRAPH_STRUCTURE_UNDEF;
    sgl.GraphAry[I].GraphAxesCnt[GuiLib_GRAPHAXIS_X] = 0;
    sgl.GraphAry[I].GraphAxesCnt[GuiLib_GRAPHAXIS_Y] = 0;
    sgl.GraphAry[I].GraphDataSetCnt = 0;
  }
#endif

#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  GraphicsLayer_Pop(GuiLib_GRAPHICS_LAYER_BASE);
  for (I = 0; I < GuiConst_GRAPHICS_LAYER_MAX; I++)
    sgl.GraphicsLayerLifo[I] = -1;
  sgl.GraphicsLayerLifoCnt = 0;
  sgl.GlobalGraphicsLayerIndex = 0;
  for (I = 0; I < GuiConst_GRAPHICS_LAYER_MAX; I++)
  {
    sgl.GraphicsLayerList[I].InUse = GuiLib_GRAPHICS_LAYER_UNDEF;
    sgl.GraphicsLayerList[I].SizeMode = 0;
    sgl.GraphicsLayerList[I].InitMode = 0;
  }
  sgl.GlobalGraphicsFilterIndex = 0;
  for (I = 0; I < GuiConst_GRAPHICS_FILTER_MAX; I++)
  {
    sgl.GraphicsFilterList[I].InUse = GuiLib_GRAPHICS_FILTER_UNDEF;
    sgl.GraphicsFilterList[I].GraphicsFilterFunc = 0;
    sgl.GraphicsFilterList[I].SourceLayerIndexNo = GuiLib_GRAPHICS_LAYER_BASE;
    sgl.GraphicsFilterList[I].DestLayerIndexNo = GuiLib_GRAPHICS_LAYER_BASE;
    sgl.GraphicsFilterList[I].ContAtLayerIndexNo = GuiLib_GRAPHICS_LAYER_BASE;
    for (N = 0; N <= 9; N++)
    {
      sgl.GraphicsFilterList[I].ParVarType[N] = 0;
      sgl.GraphicsFilterList[I].ParVarPtr[N] = 0;
      sgl.GraphicsFilterList[I].ParValueNum[N] = 0;
    }
  }
#endif

#ifdef GuiConst_BITMAP_SUPPORT_ON
  sgl.GlobalBackgrBitmapIndex = 0;
  for (I = 0; I < GuiConst_MAX_BACKGROUND_BITMAPS; I++)
  {
    sgl.BackgrBitmapAry[I].InUse = 0;
    sgl.BackgrBitmapAry[I].Index = 0;
    sgl.BackgrBitmapAry[I].X = 0;
    sgl.BackgrBitmapAry[I].Y = 0;
  }
#endif

  GuiLib_ClearPositionCallbacks();
}

// Groupstart CHARS

#ifdef GuiConst_CHARMODE_UNICODE
//------------------------------------------------------------------------------
GuiConst_INT16U GuiLib_UnicodeStrLen(
   GuiConst_TEXT *S)
{
  GuiConst_INT16U StrLen;

  StrLen = 0;
  while (*S != 0)
  {
    StrLen++;
    S++;
  }
  return (StrLen);
}

//------------------------------------------------------------------------------
void GuiLib_StrAnsiToUnicode(
   GuiConst_TEXT *S2,
   GuiConst_CHAR *S1)
{
  do
  {
    *S2 = (GuiConst_TEXT)(*S1);
    *S2 &= 0x00ff;
    if (*S1 == 0)
      return;
    S1++;
    S2++;
  }
  while (1);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_UnicodeStrCmp(
   GuiConst_TEXT *S1,
   GuiConst_TEXT *S2)
{
  do
  {
    if ((*S1 == 0) && (*S2 == 0))
      return (0);

    else if (*S1 == 0)
      return (-1);
    else if (*S2 == 0)
      return (1);
    else if (*S1 < *S2)
      return (-1);
    else if (*S1 > *S2)
      return (1);
    S1++;
    S2++;
  }
  while (1);
}

//------------------------------------------------------------------------------
GuiConst_INT16S GuiLib_UnicodeStrNCmp(
   GuiConst_TEXT *S1,
   GuiConst_TEXT *S2,
   GuiConst_INT16U StrLen)
{
  while (StrLen > 0)
  {
    if ((*S1 == 0) && (*S2 == 0))
      return (0);
    else if (*S1 == 0)
      return (-1);
    else if (*S2 == 0)
      return (1);
    else if (*S1 < *S2)
      return (-1);
    else if (*S1 > *S2)
      return (1);
    S1++;
    S2++;
    StrLen--;
  }
  return (0);
}

//------------------------------------------------------------------------------
void GuiLib_UnicodeStrCpy(
   GuiConst_TEXT *S2,
   GuiConst_TEXT *S1)
{
  do
  {
    *S2 = *S1;
    if (*S1 == 0)
      return;
    S1++;
    S2++;
  }
  while (1);
}

//------------------------------------------------------------------------------
void GuiLib_UnicodeStrNCpy(
   GuiConst_TEXT *S2,
   GuiConst_TEXT *S1,
   GuiConst_INT16U StrLen)
{
  while (StrLen > 0)
  {
    *S2 = *S1;
    if (*S1 != 0)
      S1++;
    S2++;
    StrLen--;
  }
}
#endif // GuiConst_CHARMODE_UNICODE

//------------------------------------------------------------------------------
static void ConvertIntToStr(
   GuiConst_INT32U num,
   GuiConst_CHAR *string,
   GuiConst_INT32U base)
{
  #define BUFFER_SIZE 11

  GuiConst_INT8U digits[16] =
     { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70 };
  GuiConst_CHAR buffer[BUFFER_SIZE];
  GuiConst_INT8U bufferLen;
  GuiConst_INT8U i;

  bufferLen = 0;
  do
  {
    i = num % base;
    buffer[bufferLen++] = digits[i];
    num /= base;
  }
  while ((num != 0) && (bufferLen <= BUFFER_SIZE));

  if (bufferLen <= GuiConst_MAX_VARNUM_TEXT_LEN)
    while (bufferLen-- > 0)
      *string++ = buffer[bufferLen];

  *string = '\0';
}

//------------------------------------------------------------------------------
static GuiConst_INT16S CharDist(
   GuiConst_INT16U ChPos1,
   GuiConst_INT16U ChPos2,
   GuiConst_INT8U Ps)
{
  GuiConst_INT16S Result, D;
#ifndef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U PrefixRom *Ps1;
  GuiConst_INT8U PrefixRom *Ps2;
#endif
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (Ps == GuiLib_PS_OFF)
    return (sgl.CurFont->XSize);
  else if ((Ps == GuiLib_PS_ON) || (Ps == GuiLib_PS_NUM))
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[ChPos1]],
       GuiLib_CHR_LINECTRL_OFS, CharHeader1);
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[ChPos2]],
       GuiLib_CHR_LINECTRL_OFS, CharHeader2);
    if ((Ps == GuiLib_PS_ON) || (sgl.TextPsMode[ChPos1] && sgl.TextPsMode[ChPos2]))
    {
      Result = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_TOP_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_TOP_OFS];
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_MID_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_MID_OFS];
      if (D > Result)
        Result = D;
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_MIDBASE_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_MIDBASE_OFS];
      if (D > Result)
        Result = D;
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_BASE_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_BASE_OFS];
      if (D > Result)
        Result = D;
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_BOTTOM_OFS] -
          CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_BOTTOM_OFS];
      if (D > Result)
        Result = D;
      return (Result + sgl.CurFont->PsSpace + 1);
    }
    else if (sgl.TextPsMode[ChPos1])
    {
      Result = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_TOP_OFS];
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_MID_OFS];
      if (D > Result)
        Result = D;
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_MIDBASE_OFS];
      if (D > Result)
        Result = D;
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_BASE_OFS];
      if (D > Result)
        Result = D;
      D = CharHeader1[GuiLib_CHR_PSRIGHT_OFS + GuiLib_CHR_PS_BOTTOM_OFS];
      if (D > Result)
        Result = D;
      return (Result + sgl.CurFont->PsSpace + 1);
    }
    else if (sgl.TextPsMode[ChPos2])
    {
      Result = CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_TOP_OFS];
      D = CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_MID_OFS];
      if (D < Result)
        Result = D;
      D = CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_MIDBASE_OFS];
      if (D < Result)
        Result = D;
      D = CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_BASE_OFS];
      if (D < Result)
        Result = D;
      D = CharHeader2[GuiLib_CHR_PSLEFT_OFS + GuiLib_CHR_PS_BOTTOM_OFS];
      if (D < Result)
        Result = D;
      return (sgl.CurFont->PsNumWidth - Result + sgl.CurFont->PsSpace);
    }
    else
      return (sgl.CurFont->PsNumWidth + sgl.CurFont->PsSpace);
  }
  else
    return (0);
#else
  if (Ps == GuiLib_PS_OFF)
    return (ReadByte(sgl.CurFont->XSize));
  else if ((Ps == GuiLib_PS_ON) || (Ps == GuiLib_PS_NUM))
  {
    if ((Ps == GuiLib_PS_ON) || (sgl.TextPsMode[ChPos1] && sgl.TextPsMode[ChPos2]))
    {
      Ps1 = sgl.TextCharPtrAry[ChPos1] + GuiLib_CHR_PSRIGHT_OFS;
      Ps2 = sgl.TextCharPtrAry[ChPos2] + GuiLib_CHR_PSLEFT_OFS;
      Result = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_TOP_OFS)) -
         (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_TOP_OFS));
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_MID_OFS)) -
          (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_MID_OFS));
      if (D > Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_MIDBASE_OFS)) -
          (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_MIDBASE_OFS));
      if (D > Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_BASE_OFS)) -
          (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_BASE_OFS));
      if (D > Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_BOTTOM_OFS)) -
          (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_BOTTOM_OFS));
      if (D > Result)
        Result = D;
      return (Result + ReadByte(sgl.CurFont->PsSpace) + 1);
    }
    else if (sgl.TextPsMode[ChPos1])
    {
      Ps1 = sgl.TextCharPtrAry[ChPos1] + GuiLib_CHR_PSRIGHT_OFS;
      Result = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_TOP_OFS));
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_MID_OFS));
      if (D > Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_MIDBASE_OFS));
      if (D > Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_BASE_OFS));
      if (D > Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps1 + GuiLib_CHR_PS_BOTTOM_OFS));
      if (D > Result)
        Result = D;
      return (Result + ReadByte(sgl.CurFont->PsSpace) + 1);
    }
    else if (sgl.TextPsMode[ChPos2])
    {
      Ps2 = sgl.TextCharPtrAry[ChPos2] + GuiLib_CHR_PSLEFT_OFS;
      Result = (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_TOP_OFS));
      D = (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_MID_OFS));
      if (D < Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_MIDBASE_OFS));
      if (D < Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_BASE_OFS));
      if (D < Result)
        Result = D;
      D = (GuiConst_INT16S)(ReadBytePtr(Ps2 + GuiLib_CHR_PS_BOTTOM_OFS));
      if (D < Result)
        Result = D;
      return (ReadByte(sgl.CurFont->PsNumWidth) - Result +
              ReadByte(sgl.CurFont->PsSpace));
    }
    else
      return (ReadByte(sgl.CurFont->PsNumWidth) +
         ReadByte(sgl.CurFont->PsSpace));
  }
  else
    return (0);
#endif
}

//------------------------------------------------------------------------------
static GuiConst_INT16S TextPixelLength(
   GuiConst_INT8U Ps,
   GuiConst_INT16U CharCnt,
   TextXOfsAryPtr TextXOfsPtr)
{
  GuiConst_INT16U P;
  GuiConst_INT16S L;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS + 1];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS + 1];
#endif

  if (CharCnt == 0)
    return (0);
  else
  {
#ifdef GuiConst_REMOTE_FONT_DATA
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[0]],
       GuiLib_CHR_LINECTRL_OFS + 1, CharHeader1);
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[CharCnt - 1]],
       GuiLib_CHR_LINECTRL_OFS + 1, CharHeader2);
#endif

    if (sgl.TextPsMode[0])
    {
#ifdef GuiConst_REMOTE_FONT_DATA
      if (CharHeader1[GuiLib_CHR_LINECTRL_OFS] & 0x01)
        L = -(GuiConst_INT16S)CharHeader1[GuiLib_CHR_PSLEFT_OFS];
      else
        L = -(GuiConst_INT16S)CharHeader1[GuiLib_CHR_XLEFT_OFS];
#else
      if (ReadBytePtr(sgl.TextCharPtrAry[0] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L = -(GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[0] +
           GuiLib_CHR_PSLEFT_OFS));
      else
        L = -(GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[0] +
         GuiLib_CHR_XLEFT_OFS));
#endif
    }
    else
      L = 0;

    if (TextXOfsPtr != 0)
      (*TextXOfsPtr)[0] = L;

    for (P = 0; P < CharCnt - 1; P++)
    {
      L += CharDist (P, P + 1, Ps);
      if (TextXOfsPtr != 0)
        (*TextXOfsPtr)[P + 1] = L;
    }

#ifdef GuiConst_REMOTE_FONT_DATA
    if (sgl.TextPsMode[CharCnt - 1])
    {
      if (CharHeader2[GuiLib_CHR_LINECTRL_OFS] & 0x01)
        L += (GuiConst_INT16S)CharHeader2[GuiLib_CHR_PSRIGHT_OFS] + 1;
      else
        L += (GuiConst_INT16S)CharHeader2[GuiLib_CHR_XLEFT_OFS] +
             (GuiConst_INT16S)CharHeader2[GuiLib_CHR_XWIDTH_OFS];
    }
    else if (Ps == GuiLib_PS_NUM)
    {
      if (CharHeader2[GuiLib_CHR_LINECTRL_OFS] & 0x01)
        L += (GuiConst_INT16S)CharHeader2[GuiLib_CHR_PSRIGHT_OFS] + 1;
      else
        L += sgl.CurFont->PsNumWidth + sgl.CurFont->PsSpace;
    }
    else
      L += sgl.CurFont->XSize;
#else
    if (sgl.TextPsMode[CharCnt - 1])
    {
      if (ReadBytePtr(sgl.TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L += (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharCnt - 1] +
              GuiLib_CHR_PSRIGHT_OFS)) + 1;
      else
        L += ReadBytePtr(sgl.TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_XLEFT_OFS) +
          ReadBytePtr(sgl.TextCharPtrAry[CharCnt - 1] + GuiLib_CHR_XWIDTH_OFS);
    }
    else if (Ps == GuiLib_PS_NUM)
    {
      if (ReadBytePtr(sgl.TextCharPtrAry[CharCnt - 1] +
          GuiLib_CHR_LINECTRL_OFS) & 0x01)
        L += (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharCnt - 1] +
              GuiLib_CHR_PSRIGHT_OFS)) + 1;
      else
        L += ReadByte(sgl.CurFont->PsNumWidth) +
           ReadByte(sgl.CurFont->PsSpace);
    }
    else
      L += ReadByte(sgl.CurFont->XSize);
#endif
    if (TextXOfsPtr != 0)
      (*TextXOfsPtr)[CharCnt] = L;
    return (L);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INT16U CalcCharsWidth(
   GuiConst_TEXT* pChar,
   GuiConst_INT16U CharPos1,
   GuiConst_INT16U CharPos2,
   TextXOfsAryPtr TextXOfsPtr,
   GuiConst_INT16S *pXStart,
   GuiConst_INT16S *pXEnd)
{
  GuiConst_INT16S X1;
  GuiConst_INT16S X2;

#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];

  GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[CharPos1]],
      GuiLib_CHR_LINECTRL_OFS, CharHeader1);
  GuiLib_RemoteDataReadBlock(
     (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[CharPos2]],
      GuiLib_CHR_LINECTRL_OFS, CharHeader2);

  if (sgl.TextPsMode[CharPos1])
  {
    if (CharHeader1[GuiLib_CHR_LINECTRL_OFS] & 0x01)
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)CharHeader1[GuiLib_CHR_PSLEFT_OFS];
    else
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)CharHeader1[GuiLib_CHR_XLEFT_OFS];
  }
  else
    X1 = (*TextXOfsPtr)[CharPos1];
  if (sgl.TextPsMode[CharPos2])
  {
    if (CharHeader2[GuiLib_CHR_LINECTRL_OFS] & 0x01)
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)CharHeader2[GuiLib_CHR_PSRIGHT_OFS];
    else
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)CharHeader1[GuiLib_CHR_XLEFT_OFS] +
           (GuiConst_INT16S)CharHeader2[GuiLib_CHR_XWIDTH_OFS] - 1;
  }
  else
    X2 = (*TextXOfsPtr)[CharPos2] + sgl.CurFont->XSize - 1;
#else
  if (sgl.TextPsMode[CharPos1])
  {
    if (ReadBytePtr(sgl.TextCharPtrAry[CharPos1] +
        GuiLib_CHR_LINECTRL_OFS) & 0x01)
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharPos1] +
            GuiLib_CHR_PSLEFT_OFS));
    else
      X1 = (*TextXOfsPtr)[CharPos1] +
           (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharPos1] +
            GuiLib_CHR_XLEFT_OFS));
  }
  else
    X1 = (*TextXOfsPtr)[CharPos1];
  if (sgl.TextPsMode[CharPos2])
  {
    if (ReadBytePtr(sgl.TextCharPtrAry[CharPos2] +
        GuiLib_CHR_LINECTRL_OFS) & 0x01)
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharPos2] +
            GuiLib_CHR_PSRIGHT_OFS));
    else
      X2 = (*TextXOfsPtr)[CharPos2] +
           (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharPos2] +
            GuiLib_CHR_XLEFT_OFS)) +
           (GuiConst_INT16S)(ReadBytePtr(sgl.TextCharPtrAry[CharPos2] +
            GuiLib_CHR_XWIDTH_OFS)) - 1;
  }
  else
    X2 = (*TextXOfsPtr)[CharPos2] + sgl.CurFont->XSize - 1;
#endif

  *pXStart = X1;
  *pXEnd = X2;
  return(X2 - X1 + 1);
}

#ifdef GuiConst_CHARMODE_UNICODE
//------------------------------------------------------------------------------
static GuiConst_INT32U GetCharNdx(
   GuiLib_FontRecPtr Font,
   GuiConst_INT16U CharCode)
{
  GuiConst_INT32U CharNdx,CharNdx1,CharNdx2;

  CharNdx1 = ReadWord(Font->FirstCharNdx) + 1;
  CharNdx2 = CharNdx1 + ReadWord(Font->CharCount) - 1;

  do
  {
    CharNdx = CharNdx1 + ((CharNdx2 - CharNdx1) >> 1);
    if (ReadWord(GuiFont_ChUnicodeList[CharNdx]) == CharCode)
      return (CharNdx);
    if (CharNdx1 == CharNdx2)
      return (ReadWord(Font->FirstCharNdx));

    if (ReadWord(GuiFont_ChUnicodeList[CharNdx]) > CharCode)
      CharNdx2 = CharNdx - 1;
    else
      CharNdx1 = CharNdx + 1;
    if (CharNdx1 > CharNdx2)
      return (ReadWord(Font->FirstCharNdx));
  }
  while (1);
}
#endif

//------------------------------------------------------------------------------
static void PrepareText(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INT8U TextNdx)
{
  GuiConst_INT16S P;
#ifdef GuiConst_CHARMODE_ANSI
  GuiConst_INT8U CharCode;
#else
  GuiConst_INT16U CharCode;
  GuiConst_INT16U CharNdx;
#endif

  if (CharCnt > GuiConst_MAX_TEXT_LEN)
    CharCnt = GuiConst_MAX_TEXT_LEN;

  for (P = 0; P < CharCnt; P++)
  {
#ifdef GuiConst_CHARMODE_ANSI

#ifdef GuiConst_AVRGCC_COMPILER
    if (displayVarNow)
      CharCode = (unsigned GuiConst_CHAR) *CharPtr;
    else
      CharCode = (unsigned GuiConst_CHAR) ReadBytePtr(CharPtr);
#else
#ifdef GuiConst_ICC_COMPILER
    if (displayVarNow)
      CharCode = (unsigned GuiConst_CHAR) *CharPtr;
    else
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
    if (displayVarNow)
      CharCode = (unsigned GuiConst_CHAR) *CharPtr;
    else
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
#else
      CharCode = (unsigned GuiConst_TEXT) *CharPtr;
#endif // GuiConst_CODEVISION_COMPILER
#endif // GuiConst_ICC_COMPILER
#endif // GuiConst_AVRGCC_COMPILER

#ifdef GuiConst_REMOTE_FONT_DATA
    if ((CharCode < sgl.CurFont->FirstChar) ||
        (CharCode > sgl.CurFont->LastChar))
      sgl.TextCharNdx[P] = sgl.CurFont->IllegalCharNdx;
    else
    {
      sgl.TextCharNdx[P] =
         sgl.CurFont->FirstCharNdx + (GuiConst_INT16U)CharCode -
         (GuiConst_INT16U)sgl.CurFont->FirstChar;

      if ((GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[P] + 1] -
          (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[sgl.TextCharNdx[P]] == 0)
        sgl.TextCharNdx[P] = sgl.CurFont->IllegalCharNdx;
    }
#else // GuiConst_REMOTE_FONT_DATA
    if ((CharCode < ReadByte(sgl.CurFont->FirstChar)) ||
        (CharCode > ReadByte(sgl.CurFont->LastChar)))
      sgl.TextCharPtrAry[P] = (GuiConst_INT8U*)ReadWord(
         GuiFont_ChPtrList[ReadWord(sgl.CurFont->IllegalCharNdx)]);
    else
      sgl.TextCharPtrAry[P] = (GuiConst_INT8U*)ReadWord(
         GuiFont_ChPtrList[ReadWord(sgl.CurFont->FirstCharNdx) +
         (GuiConst_INT16U)CharCode -
         (GuiConst_INT16U)ReadByte(sgl.CurFont->FirstChar)]);
#endif // GuiConst_REMOTE_FONT_DATA

    if (sgl.CurItem.TextPar[TextNdx].Ps == GuiLib_PS_ON)
      sgl.TextPsMode[P] = 1;
    else if (sgl.CurItem.TextPar[TextNdx].Ps == GuiLib_PS_NUM)
    {
      if (((CharCode >= '0') && (CharCode <= '9')) ||
         (CharCode == ' ') || (CharCode == '+') || (CharCode == '-') ||
         (CharCode == '*') || (CharCode == '/') || (CharCode == '='))
        sgl.TextPsMode[P] = 0;
      else
        sgl.TextPsMode[P] = 1;
    }
    else
      sgl.TextPsMode[P] = 0;

#else // GuiConst_CHARMODE_ANSI
    CharCode = *((GuiConst_INT16U*)CharPtr);
    CharNdx = GetCharNdx(sgl.CurFont, CharCode);

#ifdef GuiConst_REMOTE_FONT_DATA
    sgl.TextCharNdx[P] = CharNdx;
#else
    sgl.TextCharPtrAry[P] =
       (GuiConst_INT8U PrefixRom *)ReadWord(GuiFont_ChPtrList[CharNdx]);
#endif

    if (sgl.CurItem.TextPar[TextNdx].Ps == GuiLib_PS_ON)
      sgl.TextPsMode[P] = 1;
    else if ((sgl.CurItem.TextPar[TextNdx].Ps == GuiLib_PS_NUM) && (CharNdx > 0))
    {
      if (((CharCode >= '0') && (CharCode <= '9')) ||
         (CharCode == ' ') || (CharCode == '+') || (CharCode == '-') ||
         (CharCode == '*') || (CharCode == '/') || (CharCode == '='))
        sgl.TextPsMode[P] = 0;
      else
        sgl.TextPsMode[P] = 1;
    }
    else
      sgl.TextPsMode[P] = 0;
#endif // GuiConst_CHARMODE_ANSI
    CharPtr++;
  }
}

#ifdef GuiConst_ARAB_CHARS_INUSE
//------------------------------------------------------------------------------
static GuiConst_INT16U ArabicCorrection(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INT8U RightToLeftWriting)
{
  GuiConst_INT16S I, J, P;
  GuiConst_INT16U CharCode;
  GuiConst_INT16U CharCode2;
  GuiConst_TEXT *CharPtr2;
  GuiConst_TEXT *CharPtrA;
  GuiConst_TEXT *CharPtrB;

  if (!RightToLeftWriting)
    return CharCnt;

  CharPtr2 = CharPtr;

  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);
    switch (CharCode)
    {
      case 40:
        *((GuiConst_INT16U*)CharPtr) = 41;
        break;
      case 41:
        *((GuiConst_INT16U*)CharPtr) = 40;
        break;

      case 91:
        *((GuiConst_INT16U*)CharPtr) = 93;
        break;
      case 93:
        *((GuiConst_INT16U*)CharPtr) = 91;
        break;

      case 123:
        *((GuiConst_INT16U*)CharPtr) = 125;
        break;
      case 125:
        *((GuiConst_INT16U*)CharPtr) = 123;
        break;
    }

    CharPtr++;
  }

  P = 0;
  CharPtr = CharPtr2;
  do
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);
    CharPtrA = CharPtr;
    CharPtr++;
    CharCode2 = *((GuiConst_INT16U*)CharPtr);

    for (I = 0; I < GuiLib_ARAB_LIGATURES_CNT; I++)
      if ((CharCode == GuiLib_ARAB_LIGATURES[I][0]) &&
          (CharCode2 == GuiLib_ARAB_LIGATURES[I][1]))
      {
        *((GuiConst_INT16U*)CharPtrA) = GuiLib_ARAB_LIGATURES[I][2];
        CharPtrA = CharPtr;
        CharPtrB = CharPtrA;
        CharPtrB++;
        CharCnt--;
        for (J = P + 1; J < CharCnt; J++)
        {
          *((GuiConst_INT16U*)CharPtrA) = *((GuiConst_INT16U*)CharPtrB);
          CharPtrA++;
          CharPtrB++;
        }
        *((GuiConst_INT16U*)CharPtrA) = 0;
        break;
      }

    P++;
  }
  while (P < CharCnt - 1);

  CharPtr = CharPtr2;
  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);

    for (I = 0; I < GuiLib_ARAB_CHAR_CONVERT_CNT; I++)
      if ((CharCode == GuiLib_ARAB_CHAR_CONVERT[I][0]) && (CharCode > 0))
      {
        *((GuiConst_INT16U*)CharPtr) = GuiLib_ARAB_CHAR_CONVERT[I][1];
        break;
      }

    CharPtr++;
  }

  gl.ArabicCharJoiningModeIndex[0] = -1;
  CharPtr = CharPtr2;
  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);

    gl.ArabicCharJoiningModeIndex[P + 1] = -1;
    for (I = 0; I < GuiLib_ARAB_CHAR_CONVERT_CNT; I++)
      if ((CharCode >= GuiLib_ARAB_CHAR_CONVERT[I][1]) &&
          (CharCode <= GuiLib_ARAB_CHAR_CONVERT[I][1] +
          (GuiLib_ARAB_CHAR_CONVERT[I][2] & 7) - 1))
      {
        gl.ArabicCharJoiningModeIndex[P + 1] = I;
        break;
      }

    CharPtr++;
  }
  gl.ArabicCharJoiningModeIndex[CharCnt + 1] = -1;

  for (P = 0; P < CharCnt + 2; P++)
  {
    if (gl.ArabicCharJoiningModeIndex[P] == -1)
      gl.ArabicCharJoiningMode[P] = GuiLib_ARAB_CHAR_TYPE_ISO;
    else
      gl.ArabicCharJoiningMode[P] =
         GuiLib_ARAB_CHAR_CONVERT[gl.ArabicCharJoiningModeIndex[P]][2];
  }

  CharPtr = CharPtr2;
  for (P = 0; P < CharCnt; P++)
  {
    CharCode = *((GuiConst_INT16U*)CharPtr);

    I = P;
    while (gl.ArabicCharJoiningMode[I] == GuiLib_ARAB_CHAR_DIACRITIC)
      I--;
    gl.ArabicCharJoiningModeBefore = gl.ArabicCharJoiningMode[I];
    I = P + 2;
    while (gl.ArabicCharJoiningMode[I] == GuiLib_ARAB_CHAR_DIACRITIC)
      I++;
    gl.ArabicCharJoiningModeAfter = gl.ArabicCharJoiningMode[I];

    switch (gl.ArabicCharJoiningMode[P + 1])
    {
      case GuiLib_ARAB_CHAR_ISOFIN:
        if (gl.ArabicCharJoiningModeBefore == GuiLib_ARAB_CHAR_ISOFININIMED)
          *((GuiConst_INT16U*)CharPtr) =
             GuiLib_ARAB_CHAR_CONVERT[gl.ArabicCharJoiningModeIndex[P + 1]][1] +
             GuiLib_ARAB_CHAR_TYPE_FIN;
        break;

      case GuiLib_ARAB_CHAR_ISOFININIMED:
        if ((gl.ArabicCharJoiningModeAfter == GuiLib_ARAB_CHAR_ISOFIN) ||
            (gl.ArabicCharJoiningModeAfter == GuiLib_ARAB_CHAR_ISOFININIMED))
        {
          if (gl.ArabicCharJoiningModeBefore == GuiLib_ARAB_CHAR_ISOFININIMED)
            *((GuiConst_INT16U*)CharPtr) =
               GuiLib_ARAB_CHAR_CONVERT[gl.ArabicCharJoiningModeIndex[P + 1]][1] +
               GuiLib_ARAB_CHAR_TYPE_MED;
          else
            *((GuiConst_INT16U*)CharPtr) =
               GuiLib_ARAB_CHAR_CONVERT[gl.ArabicCharJoiningModeIndex[P + 1]][1] +
               GuiLib_ARAB_CHAR_TYPE_INI;
        }
        else if (gl.ArabicCharJoiningModeBefore == GuiLib_ARAB_CHAR_ISOFININIMED)
          *((GuiConst_INT16U*)CharPtr) =
             GuiLib_ARAB_CHAR_CONVERT[gl.ArabicCharJoiningModeIndex[P + 1]][1] +
             GuiLib_ARAB_CHAR_TYPE_FIN;
        break;

      case GuiLib_ARAB_CHAR_DIACRITIC:
        if (((gl.ArabicCharJoiningMode[P + 2] == GuiLib_ARAB_CHAR_ISOFIN) ||
             (gl.ArabicCharJoiningMode[P + 2] == GuiLib_ARAB_CHAR_ISOFININIMED)) &&
            (gl.ArabicCharJoiningMode[P] == GuiLib_ARAB_CHAR_ISOFININIMED))
          *((GuiConst_INT16U*)CharPtr) =
             GuiLib_ARAB_CHAR_CONVERT[gl.ArabicCharJoiningModeIndex[P + 1]][1] + 1;
        break;
    }

    CharPtr++;
  }

  return (CharCnt);
}
#endif

// Groupend CHARS
// Groupstart DRAW

//------------------------------------------------------------------------------
static void ResetDrawLimits(void)
{
  gl.Drawn = 0;
  gl.DrawnX1 = 0x7FFF;
  gl.DrawnY1 = 0x7FFF;
  gl.DrawnX2 = 0x8000;
  gl.DrawnY2 = 0x8000;
}

//------------------------------------------------------------------------------
static void UpdateDrawLimits(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  sgl.CurItem.Drawn = 1;
  sgl.CurItem.DrawnX1 = X1;
  sgl.CurItem.DrawnY1 = Y1;
  sgl.CurItem.DrawnX2 = X2;
  sgl.CurItem.DrawnY2 = Y2;

  gl.Drawn = 1;
  gl.DrawnX1 = GuiLib_GET_MIN(gl.DrawnX1, X1);
  gl.DrawnY1 = GuiLib_GET_MIN(gl.DrawnY1, Y1);
  gl.DrawnX2 = GuiLib_GET_MAX(gl.DrawnX2, X2);
  gl.DrawnY2 = GuiLib_GET_MAX(gl.DrawnY2, Y2);
}

//------------------------------------------------------------------------------
static void DrawBorderBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR BorderColor,
   GuiConst_INTCOLOR FillColor,
   GuiConst_INT8U FillTransparent,
   GuiConst_INT16U BorderThickness)
{
  if (BorderThickness == 1)
    GuiLib_Box(X1, Y1, X2, Y2, BorderColor);
  else
  {
    GuiLib_FillBox(X1, Y1, X1 + BorderThickness - 1, Y2, BorderColor);
    GuiLib_FillBox(X2 - BorderThickness + 1, Y1, X2, Y2, BorderColor);
    GuiLib_FillBox(X1, Y1, X2, Y1 + BorderThickness - 1, BorderColor);
    GuiLib_FillBox(X1, Y2 - BorderThickness + 1, X2, Y2, BorderColor);
  }
  if ((!FillTransparent) &&
     (X2 - X1 >= 2 * BorderThickness) &&
     (Y2 - Y1 >= 2 * BorderThickness))
    GuiLib_FillBox(X1 + BorderThickness,
                   Y1 + BorderThickness,
                   X2 - BorderThickness,
                   Y2 - BorderThickness,
                   FillColor);
}

//------------------------------------------------------------------------------
static void SetBackColorBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT8U TextNdx)
{
  sgl.BbX1 = X1;
  sgl.BbY1 = Y1;
  sgl.BbX2 = X2;
  sgl.BbY2 = Y2;

  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_LEFT) != 0)
    sgl.BbX1--;
  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_RIGHT) != 0)
    sgl.BbX2++;
  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_TOP) != 0)
    sgl.BbY1--;
  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_BOTTOM) != 0)
    sgl.BbY2++;
}

//------------------------------------------------------------------------------
static void SetBackBox(
   GuiConst_INT8U TextNdx)
{
  GuiConst_INT16S L;

  L = sgl.CurItem.TextPar[TextNdx].BackBoxSizeX;
  sgl.BbX1 = sgl.CurItem.X1;
  sgl.BbX2 = sgl.CurItem.X1 + L - 1;

  if (sgl.CurItem.TextPar[TextNdx].Alignment == GuiLib_ALIGN_CENTER)
  {
    sgl.BbX1 -= L / 2;
    sgl.BbX2 -= L / 2;
  }
  else if (sgl.CurItem.TextPar[TextNdx].Alignment == GuiLib_ALIGN_RIGHT)
  {
    sgl.BbX1 -= L - 1;
    sgl.BbX2 -= L - 1;
  }

  if (sgl.CurItem.TextPar[TextNdx].BackBoxSizeY1 > 0)
    sgl.BbY1 = sgl.CurItem.Y1 - sgl.CurItem.TextPar[TextNdx].BackBoxSizeY1;
  else
    sgl.BbY1 = sgl.CurItem.Y1 - ReadByte(sgl.CurFont->BaseLine);
  if (sgl.CurItem.TextPar[TextNdx].BackBoxSizeY2 > 0)
    sgl.BbY2 = sgl.CurItem.Y1 + sgl.CurItem.TextPar[TextNdx].BackBoxSizeY2;
  else
    sgl.BbY2 = sgl.CurItem.Y1 - ReadByte(sgl.CurFont->BaseLine) +
       ReadByte(sgl.CurFont->YSize) - 1;

  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_LEFT) != 0)
    sgl.BbX1--;
  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_RIGHT) != 0)
    sgl.BbX2++;
  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_TOP) != 0)
    sgl.BbY1--;
  if ((sgl.CurItem.TextPar[TextNdx].BackBorderPixels & GuiLib_BBP_BOTTOM) != 0)
    sgl.BbY2++;
}

//------------------------------------------------------------------------------
static void DrawBackBox(
   GuiConst_INTCOLOR BoxColor,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U TextNdx)
{
  SetBackBox(TextNdx);

  if (!Transparent)
    GuiLib_FillBox(sgl.BbX1, sgl.BbY1, sgl.BbX2, sgl.BbY2, BoxColor);
}

//------------------------------------------------------------------------------
#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_ICC_COMPILER
#define DRAW_ROM_TEXT(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)       \
        DrawText((GuiConst_TEXT *)ChPtr,                                       \
                  ChCnt,                                                       \
                  TxtNdx,                                                      \
                  FColor,                                                      \
                  BColor,                                                      \
                  Transparent);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
#define DRAW_ROM_TEXT(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)       \
        DrawText((GuiConst_TEXT *)ChPtr,                                       \
                  ChCnt,                                                       \
                  TxtNdx,                                                      \
                  FColor,                                                      \
                  BColor,                                                      \
                  Transparent);
  #else
#define DRAW_ROM_TEXT(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)       \
        DrawText((GuiConst_TEXT PrefixRom *)ChPtr,                             \
                  ChCnt,                                                       \
                  TxtNdx,                                                      \
                  FColor,                                                      \
                  BColor,                                                      \
                  Transparent);
  #endif
  #endif
#else
  #ifdef GuiConst_ICC_COMPILER
#define DRAW_ROM_TEXT(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)       \
        ExtractUnicodeString((GuiConst_INT8U *)ChPtr, ChCnt);                  \
        DrawText((GuiConst_TEXT *)&sgl.UnicodeTextBuf,                         \
                  ChCnt,                                                       \
                  TxtNdx,                                                      \
                  FColor,                                                      \
                  BColor,                                                      \
                  Transparent);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
#define DRAW_ROM_TEXT(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)       \
        ExtractUnicodeString((GuiConst_INT8U *)ChPtr, ChCnt);                  \
        DrawText((GuiConst_TEXT *)sgl.UnicodeTextBuf,                          \
                  ChCnt,                                                       \
                  TxtNdx,                                                      \
                  FColor,                                                      \
                  BColor,                                                      \
                  Transparent);
  #else
#define DRAW_ROM_TEXT(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)       \
        ExtractUnicodeString((GuiConst_INT8U PrefixRom *)ChPtr, ChCnt);        \
        DrawText((GuiConst_TEXT *)&sgl.UnicodeTextBuf,                         \
                  ChCnt,                                                       \
                  TxtNdx,                                                      \
                  FColor,                                                      \
                  BColor,                                                      \
                  Transparent);
  #endif
  #endif
#endif

//------------------------------------------------------------------------------
static void DrawText(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCount,
   GuiConst_INT8U TextNdx,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor,
   GuiConst_INT8U Transparent)
{
  GuiConst_INT16S P, N;
  GuiConst_INT16U CharCnt;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U PrefixRom TempOfs;
#else
  GuiConst_INT8U PrefixRom * TempPtr;
#endif
  GuiConst_INT8U  TempPsMode;
  GuiConst_INT16S TextPixelLen;
  GuiConst_INT16S X1, Y1, X2, Y2;
  TextXOfsAry TextXOfs;

#ifdef GuiConst_ARAB_CHARS_INUSE
  CharCnt = ArabicCorrection(CharPtr,
                             CharCount,
                            (sgl.CurItem.TextPar[TextNdx].BitFlags &
               GuiLib_BITFLAG_REVERSEWRITING) > 0);
#else
  CharCnt = CharCount;
#endif
  PrepareText(CharPtr, CharCnt, TextNdx);

  if (sgl.CurItem.TextPar[TextNdx].BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
  {
    for (N = 0; N < CharCnt / 2; N++)
    {
#ifdef GuiConst_REMOTE_FONT_DATA
      TempOfs = sgl.TextCharNdx[N];
      sgl.TextCharNdx[N] = sgl.TextCharNdx[CharCnt - 1 - N];
      sgl.TextCharNdx[CharCnt - 1 - N] = TempOfs;
#else
      TempPtr = sgl.TextCharPtrAry[N];
      sgl.TextCharPtrAry[N] =
         (GuiConst_INT8U PrefixRom *)sgl.TextCharPtrAry[CharCnt - 1 - N];
      sgl.TextCharPtrAry[CharCnt - 1 - N] = (GuiConst_INT8U PrefixRom *)TempPtr;
#endif
      TempPsMode = sgl.TextPsMode[N];
      sgl.TextPsMode[N] = sgl.TextPsMode[CharCnt - 1 - N];
      sgl.TextPsMode[CharCnt - 1 - N] = TempPsMode;
    }
  }

  TextPixelLen = TextPixelLength(sgl.CurItem.TextPar[TextNdx].Ps,
                                 CharCnt,
                                 (TextXOfsAryPtr)TextXOfs);

  X1 = sgl.CurItem.X1;
  switch (sgl.CurItem.TextPar[TextNdx].Alignment)
  {
    case GuiLib_ALIGN_CENTER:
      if (CharCnt > 0)
        X1 -= TextPixelLen / 2;
      break;

    case GuiLib_ALIGN_RIGHT:
      X1 -= TextPixelLen - 1;
      break;
  }
  Y1 = sgl.CurItem.Y1 - ReadByte(sgl.CurFont->BaseLine);
  X2 = X1 + TextPixelLen - 1;
  Y2 = Y1 + ReadByte(sgl.CurFont->YSize) - 1;

  sgl.FontWriteX1 = X1;
  sgl.FontWriteY1 = Y1;
  sgl.FontWriteX2 = X2;
  sgl.FontWriteY2 = Y2;

  if (sgl.DisplayWriting)
  {
    if (CharCnt > 0)
    {
      if (!Transparent && (sgl.CurItem.TextPar[TextNdx].BackBoxSizeX == 0))
      {
        SetBackColorBox(X1, Y1, X2, Y2, TextNdx);
        GuiLib_FillBox(sgl.BbX1, sgl.BbY1, sgl.BbX2, sgl.BbY2, BackColor);
      }

      for (P = 0; P < CharCnt; P++)
#ifdef GuiConst_REMOTE_FONT_DATA
        DrawChar(X1 + TextXOfs[P], Y1, sgl.CurFont, sgl.TextCharNdx[P], ForeColor);
#else
        DrawChar(X1 + TextXOfs[P], Y1, sgl.CurFont, sgl.TextCharPtrAry[P], ForeColor);
#endif
    }

    if ((sgl.CurItem.TextPar[TextNdx].BitFlags & GuiLib_BITFLAG_UNDERLINE) &&
        (CharCnt > 0))
      GuiLib_FillBox(X1, Y1 + ReadByte(sgl.CurFont->Underline1), X2,
         Y1 + ReadByte(sgl.CurFont->Underline2), ForeColor);

    GuiLib_MarkDisplayBoxRepaint(X1, Y1, X2, Y2);

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
    if ((sgl.CurItem.TextPar[TextNdx].BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
        (sgl.CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX))
    {
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].InUse = 1;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].BlinkBoxInverted = 0;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].ItemType = sgl.CurItem.ItemType;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].CharCnt = CharCnt;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].TextPar = sgl.CurItem.TextPar[TextNdx];
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].FormatFieldWidth =
         sgl.CurItem.FormatFieldWidth;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].FormatDecimals =
         sgl.CurItem.FormatDecimals;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].FormatAlignment =
         sgl.CurItem.FormatAlignment;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].FormatFormat = sgl.CurItem.FormatFormat;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].VarType = sgl.CurItem.VarType;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].XSize =
      ReadByte(sgl.CurFont->XSize);
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].PsNumWidth =
      ReadByte(sgl.CurFont->PsNumWidth);
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].PsSpace =
      ReadByte(sgl.CurFont->PsSpace);
      if (sgl.CurItem.ItemType == GuiLib_ITEM_TEXT)
        sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].TextPtr =
           sgl.CurItem.TextPtr[TextNdx];
      else
        sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].TextPtr =
           (GuiConst_TEXT*)sgl.CurItem.VarPtr;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].X1 = sgl.CurItem.X1;
      sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].X2 = sgl.CurItem.X2;
      if (sgl.CurItem.TextPar[TextNdx].BackBoxSizeX > 0)
      {
        if (sgl.CurItem.TextPar[TextNdx].BackBoxSizeY1 > 0)
          sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Y1 =
             sgl.CurItem.Y1 - sgl.CurItem.TextPar[TextNdx].BackBoxSizeY1;
        else
          sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Y1 =
        sgl.CurItem.Y1 - ReadByte(sgl.CurFont->BaseLine);
        if (sgl.CurItem.TextPar[TextNdx].BackBoxSizeY1 > 0)
          sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Y2 =
             sgl.CurItem.Y1 + sgl.CurItem.TextPar[TextNdx].BackBoxSizeY2;
        else
          sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Y2 =
          sgl.CurItem.Y1 - ReadByte(sgl.CurFont->BaseLine) +
                ReadByte(sgl.CurFont->YSize) - 1;
      }
      else
      {
        sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Y1 = Y1;
        sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Y2 = Y2;
      }
    }
#endif
#endif
  }
}

#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE

//------------------------------------------------------------------------------
#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_ICC_COMPILER
#define DRAW_ROM_TEXTBLOCK(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)  \
        DrawTextBlock((GuiConst_TEXT *)ChPtr,                                  \
                       ChCnt,                                                  \
                       TxtNdx,                                                 \
                       FColor,                                                 \
                       BColor,                                                 \
                       Transparent);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
#define DRAW_ROM_TEXTBLOCK(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)  \
        DrawTextBlock((GuiConst_TEXT *)ChPtr,                                  \
                       ChCnt,                                                  \
                       TxtNdx,                                                 \
                       FColor,                                                 \
                       BColor,                                                 \
                       Transparent);
  #else
#define DRAW_ROM_TEXTBLOCK(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)  \
        DrawTextBlock((GuiConst_TEXT PrefixRom *)ChPtr,                        \
                       ChCnt,                                                  \
                       TxtNdx,                                                 \
                       FColor,                                                 \
                       BColor,                                                 \
                       Transparent);
  #endif
  #endif
#else
  #ifdef GuiConst_ICC_COMPILER
#define DRAW_ROM_TEXTBLOCK(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)  \
        ExtractUnicodeString((GuiConst_INT8U *)ChPtr, ChCnt);                  \
        DrawTextBlock((GuiConst_TEXT *)&sgl.UnicodeTextBuf,                    \
                       ChCnt,                                                  \
                       TxtNdx,                                                 \
                       FColor,                                                 \
                       BColor,                                                 \
                       Transparent);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
#define DRAW_ROM_TEXTBLOCK(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)  \
        ExtractUnicodeString((GuiConst_INT8U *)ChPtr, ChCnt);                  \
        DrawTextBlock((GuiConst_TEXT *)sgl.UnicodeTextBuf,                     \
                       ChCnt,                                                  \
                       TxtNdx,                                                 \
                       FColor,                                                 \
                       BColor,                                                 \
                       Transparent);
  #else
#define DRAW_ROM_TEXTBLOCK(ChPtr, ChCnt, TxtNdx, FColor, BColor, Transparent)  \
        ExtractUnicodeString((GuiConst_INT8U PrefixRom *)ChPtr, ChCnt);        \
        DrawTextBlock((GuiConst_TEXT *)&sgl.UnicodeTextBuf,                    \
                       ChCnt,                                                  \
                       TxtNdx,                                                 \
                       FColor,                                                 \
                       BColor,                                                 \
                       Transparent);
  #endif
  #endif
#endif

//------------------------------------------------------------------------------
static void DrawTextBlock(
   GuiConst_TEXT PrefixGeneric *CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INT8U TextNdx,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor,
   GuiConst_INT8U Transparent)
{
  GuiConst_INT16S X1, Y1, X2, Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S CX1, CX2;
#endif
  GuiConst_INT16S CY1, CY2;
  TextXOfsAry TextXOfs;
  GuiConst_INT16S TextCharLineStart[GuiConst_MAX_PARAGRAPH_LINE_CNT + 1];
  GuiConst_INT16S TextCharLineEnd[GuiConst_MAX_PARAGRAPH_LINE_CNT + 1];
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S RemClipX1, RemClipY1, RemClipX2, RemClipY2;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
  GuiConst_INT16S BlinkNo;
#endif
#endif
  GuiConst_INT16S PixWidth;
  GuiConst_INT16S N, P, M, F;
  GuiConst_INT16S LineLen;
  GuiConst_INT16S LineCnt;
  GuiConst_INT16S LineCnt2;
  GuiConst_INT16S BlindLinesAtTop;
  GuiConst_INT16S XPos, YPos;
  GuiConst_INT16S XStart, XEnd;
  GuiConst_INT8U  TempPsMode;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U PrefixRom TempOfs;
  GuiConst_INT8U CharHeader1[GuiLib_CHR_LINECTRL_OFS];
  GuiConst_INT8U CharHeader2[GuiLib_CHR_LINECTRL_OFS];
#else
  GuiConst_INT8U PrefixRom * TempPtr;
#endif

  X1 = sgl.CurItem.X1;
  Y1 = sgl.CurItem.Y1;
  X2 = sgl.CurItem.X2;
  Y2 = sgl.CurItem.Y2;
  sgl.FontWriteX1 = X1;
  sgl.FontWriteY1 = Y1;
  sgl.FontWriteX2 = X2;
  sgl.FontWriteY2 = Y2;

  PixWidth = 0;

  if (sgl.DisplayWriting)
  {
    if (!Transparent && (sgl.CurItem.TextPar[TextNdx].BackBoxSizeX == 0))
    {
      SetBackColorBox(X1, Y1, X2, Y2, TextNdx);
      GuiLib_FillBox(sgl.BbX1, sgl.BbY1, sgl.BbX2, sgl.BbY2, BackColor);
    }

    if (sgl.CurItem.CompPars.CompTextBox.LineDistRelToFont)
    {
      sgl.CurItem.CompPars.CompTextBox.LineDist +=sgl.CurFont->YSize;
      sgl.CurItem.CompPars.CompTextBox.LineDistRelToFont = 0;
    }

    LineCnt = 0;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
    sgl.CurItem.CompPars.CompTextBox.Lines = 1;
#endif
    if (CharCnt > 0)
    {
      PixWidth = X2 - X1 + 1;

#ifdef GuiConst_ARAB_CHARS_INUSE
      CharCnt =
         ArabicCorrection(CharPtr,
                          CharCnt,
                          (sgl.CurItem.TextPar[TextNdx].BitFlags &
                           GuiLib_BITFLAG_REVERSEWRITING) > 0);
#endif
      PrepareText(CharPtr, CharCnt, TextNdx);
      TextPixelLength(sgl.CurItem.TextPar[TextNdx].Ps,
                      CharCnt,
                      (TextXOfsAryPtr)TextXOfs);

      TextCharLineStart[0] = 0;
      TextCharLineEnd[0] = -1;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
      if (sgl.CurItem.CompPars.CompTextBox.ScrollIndex == 0xFF)
        BlindLinesAtTop = 0;
      else
        BlindLinesAtTop = sgl.CurItem.CompPars.CompTextBox.ScrollPos /
           sgl.CurItem.CompPars.CompTextBox.LineDist;
#else
      BlindLinesAtTop = 0;
#endif
      LineCnt = 1 - BlindLinesAtTop;
      if (LineCnt >= 1)
        LineCnt = 1;
      LineCnt2 = 1;
      P = 0;
#ifdef GuiConst_REMOTE_FONT_DATA
      GuiLib_RemoteDataReadBlock(
         (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[
         sgl.TextCharNdx[TextCharLineStart[LineCnt2 - 1]]],
         GuiLib_CHR_LINECTRL_OFS,
         CharHeader2);
#endif
      while (P < CharCnt)
      {
        while ((P < CharCnt - 1) &&
           !((ReadBytePtr(CharPtr + P) == GuiLib_LINEFEED) ||
           ((ReadBytePtr(CharPtr + P) != ' ') &&
           (ReadBytePtr(CharPtr + P + 1) == ' ')) ||
           ((ReadBytePtr(CharPtr + P) == '-') &&
           (ReadBytePtr(CharPtr + P + 1) != ' '))))
          P++;

        F = 0;

        if (CalcCharsWidth(CharPtr, TextCharLineStart[LineCnt2 - 1], P,
            &TextXOfs, &XStart, &XEnd) > PixWidth)
        {
          if (TextCharLineEnd[LineCnt2 - 1] == -1)
          {
            if (ReadBytePtr(CharPtr + P) == GuiLib_LINEFEED)
            {
              TextCharLineEnd[LineCnt2 - 1] = P - 1;
              F = 1;
            }
            else
              TextCharLineEnd[LineCnt2 - 1] = P;

            TextCharLineStart[LineCnt2] = P + 1;
            TextCharLineEnd[LineCnt2] = -1;
          }
          else
          {
            TextCharLineStart[LineCnt2] = TextCharLineEnd[LineCnt2 - 1] + 1;
            while ((TextCharLineStart[LineCnt2] < P) &&
               (ReadBytePtr(CharPtr + TextCharLineStart[LineCnt2]) == ' '))
              TextCharLineStart[LineCnt2]++;
            TextCharLineEnd[LineCnt2] = P;
          }
          if (LineCnt >= GuiConst_MAX_PARAGRAPH_LINE_CNT)
          {
            P = CharCnt;
            break;
          }
          LineCnt++;
          if (LineCnt > 1)
            LineCnt2 = LineCnt;
          else
          {
            TextCharLineStart[LineCnt2 - 1] = TextCharLineStart[LineCnt2];
            TextCharLineEnd[LineCnt2 - 1] = TextCharLineEnd[LineCnt2];
          }
#ifdef GuiConst_TEXTBOX_FIELDS_ON
          sgl.CurItem.CompPars.CompTextBox.Lines++;
#endif
        }
        else
          TextCharLineEnd[LineCnt2 - 1] = P;

        if ((ReadBytePtr(CharPtr + P) == GuiLib_LINEFEED) &&
            (F == 0))
        {
          TextCharLineEnd[LineCnt2 - 1] = P - 1;
          TextCharLineStart[LineCnt2] = P + 1;
          TextCharLineEnd[LineCnt2] = -1;

          if (LineCnt >= GuiConst_MAX_PARAGRAPH_LINE_CNT)
          {
            P = CharCnt;
            break;
          }
          LineCnt++;
          if (LineCnt > 1)
            LineCnt2 = LineCnt;
          else
          {
            TextCharLineStart[LineCnt2 - 1] = TextCharLineStart[LineCnt2];
            TextCharLineEnd[LineCnt2 - 1] = TextCharLineEnd[LineCnt2];
          }
#ifdef GuiConst_TEXTBOX_FIELDS_ON
          sgl.CurItem.CompPars.CompTextBox.Lines++;
#endif
        }

        P++;
      }
    }

    if (LineCnt >= 1)
    {
      if (sgl.CurItem.TextPar[TextNdx].BitFlags & GuiLib_BITFLAG_REVERSEWRITING)
      {
        for (M = 0; M < LineCnt ; M++)
        {
          for (P = TextCharLineStart[M]; P <= (TextCharLineStart[M] +
             ((TextCharLineEnd[M] - TextCharLineStart[M] + 1) / 2) - 1); P++)
          {
#ifdef GuiConst_REMOTE_FONT_DATA
            TempOfs = sgl.TextCharNdx[P];
            sgl.TextCharNdx[P] =
               sgl.TextCharNdx[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
            sgl.TextCharNdx[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
               TempOfs;
#else
#ifdef GuiConst_CODEVISION_COMPILER
            TempPtr = &sgl.TextCharPtrAry[P];
#else
            TempPtr = sgl.TextCharPtrAry[P];
#endif
            sgl.TextCharPtrAry[P] = (GuiConst_INT8U PrefixRom *)
               sgl.TextCharPtrAry[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
            sgl.TextCharPtrAry[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
               (GuiConst_INT8U PrefixRom *)TempPtr;
#endif
            TempPsMode = sgl.TextPsMode[P];
            sgl.TextPsMode[P] =
               sgl.TextPsMode[TextCharLineEnd[M] - (P - TextCharLineStart[M])];
            sgl.TextPsMode[TextCharLineEnd[M] - (P - TextCharLineStart[M])] =
               TempPsMode;
          }
        }
        TextPixelLength(sgl.CurItem.TextPar[TextNdx].Ps,
                        CharCnt,
                        (TextXOfsAryPtr)TextXOfs);
      }

      CY1 = Y1;
      CY2 = Y2;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      CX1 = X1;
      CX2 = X2;
      RemClipX1 = sgl.CurItem.ClipRectX1;
      RemClipY1 = sgl.CurItem.ClipRectY1;
      RemClipX2 = sgl.CurItem.ClipRectX2;
      RemClipY2 = sgl.CurItem.ClipRectY2;
      if (RemClipX1 > CX1)
        CX1 = RemClipX1;
      if (RemClipY1 > CY1)
        CY1 = RemClipY1;
      if (RemClipX2 < CX2)
        CX2 = RemClipX2;
      if (RemClipY2 < CY2)
        CY2 = RemClipY2;
      GuiLib_SetClipping(CX1, CY1, CX2, CY2);
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
      if ((sgl.CurItem.TextPar[TextNdx].BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
          (sgl.CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX))
      {
        BlinkNo = sgl.CurItem.BlinkFieldNo;
        sgl.BlinkTextItems[BlinkNo].InUse = 1;
        sgl.BlinkTextItems[BlinkNo].ItemType = sgl.CurItem.ItemType;
        sgl.BlinkTextItems[BlinkNo].VarType = sgl.CurItem.VarType;
        sgl.BlinkTextItems[BlinkNo].CharCnt = CharCnt;
        sgl.BlinkTextItems[BlinkNo].TextPar = sgl.CurItem.TextPar[TextNdx];
        sgl.BlinkTextItems[BlinkNo].XSize = sgl.CurFont->XSize;
        sgl.BlinkTextItems[BlinkNo].YSize = sgl.CurFont->YSize;
        sgl.BlinkTextItems[BlinkNo].PsNumWidth = sgl.CurFont->PsNumWidth;
        sgl.BlinkTextItems[BlinkNo].PsSpace = sgl.CurFont->PsSpace;
        if (sgl.CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK)
          sgl.BlinkTextItems[BlinkNo].TextPtr = sgl.CurItem.TextPtr[TextNdx];
        else
          sgl.BlinkTextItems[BlinkNo].TextPtr = (GuiConst_TEXT*)sgl.CurItem.VarPtr;
        sgl.BlinkTextItems[BlinkNo].X1 = X1;
        sgl.BlinkTextItems[BlinkNo].X2 = X2;
        sgl.BlinkTextItems[BlinkNo].Y1 = Y1;
        sgl.BlinkTextItems[BlinkNo].Y2 = Y2;
        sgl.BlinkTextItems[BlinkNo].LineCnt = LineCnt;
        sgl.BlinkTextItems[BlinkNo].TextBoxLineDist =
           sgl.CurItem.CompPars.CompTextBox.LineDist;
        sgl.BlinkTextItems[BlinkNo].BlindLinesAtTop = BlindLinesAtTop;
        sgl.BlinkTextItems[BlinkNo].FormatFieldWidth = sgl.CurItem.FormatFieldWidth;
        sgl.BlinkTextItems[BlinkNo].FormatDecimals = sgl.CurItem.FormatDecimals;
        sgl.BlinkTextItems[BlinkNo].FormatAlignment = sgl.CurItem.FormatAlignment;
        sgl.BlinkTextItems[BlinkNo].FormatFormat = sgl.CurItem.FormatFormat;
        sgl.BlinkTextItems[BlinkNo].TextBoxHorzAlignment =
           sgl.CurItem.CompPars.CompTextBox.HorzAlignment;
        sgl.BlinkTextItems[BlinkNo].TextBoxVertAlignment =
           sgl.CurItem.CompPars.CompTextBox.VertAlignment;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
        sgl.BlinkTextItems[BlinkNo].TextBoxScrollPos =
           sgl.CurItem.CompPars.CompTextBox.ScrollPos;
#endif
      }
      else
        BlinkNo = -1;
#endif
#endif

      YPos = Y1;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
      if (BlindLinesAtTop < 0)
        BlindLinesAtTop = 0;
      YPos -= sgl.CurItem.CompPars.CompTextBox.ScrollPos -
             (sgl.CurItem.CompPars.CompTextBox.LineDist * BlindLinesAtTop);
#endif

      N = ReadByte(sgl.CurFont->YSize) +
         (LineCnt - 1) * sgl.CurItem.CompPars.CompTextBox.LineDist;
      switch (sgl.CurItem.CompPars.CompTextBox.VertAlignment)
      {
        case GuiLib_ALIGN_CENTER:
          YPos += (Y2 - Y1 + 1 - N) / 2;
          break;

        case GuiLib_ALIGN_RIGHT:
          YPos += Y2 - Y1 + 1 - N;
          break;
      }

      for (N = 0; N < LineCnt; N++)
      {
        if (TextCharLineEnd[N] >= TextCharLineStart[N])
        {
          XPos = X1;

          LineLen = CalcCharsWidth(CharPtr,
                                   TextCharLineStart[N],
                                   TextCharLineEnd[N],
                                   &TextXOfs,
                                   &XStart,
                                   &XEnd);
          switch (sgl.CurItem.CompPars.CompTextBox.HorzAlignment)
          {
            case GuiLib_ALIGN_CENTER:
              XPos += (PixWidth - LineLen) / 2;
              break;

            case GuiLib_ALIGN_RIGHT:
              XPos += PixWidth - LineLen;
              break;
          }

          if (sgl.CurItem.TextPar[TextNdx].BitFlags & GuiLib_BITFLAG_UNDERLINE)
            GuiLib_FillBox(XPos, YPos + ReadByte(sgl.CurFont->Underline1),
               XPos + LineLen - 1, YPos + ReadByte(sgl.CurFont->Underline2),
               ForeColor);

          XPos -= XStart;
          if ((YPos + sgl.CurFont->YSize >= CY1) && (YPos <= CY2))
            for (P = TextCharLineStart[N]; P <= TextCharLineEnd[N]; P++)
#ifdef GuiConst_REMOTE_FONT_DATA
              DrawChar(
                 XPos + TextXOfs[P], YPos, sgl.CurFont, sgl.TextCharNdx[P], ForeColor);
#else
              DrawChar(XPos + TextXOfs[P], YPos, sgl.CurFont, sgl.TextCharPtrAry[P],
                 ForeColor);
#endif
        }
        YPos += sgl.CurItem.CompPars.CompTextBox.LineDist;
      }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
      GuiLib_SetClipping(RemClipX1, RemClipY1, RemClipX2, RemClipY2);
#endif
    }
    GuiLib_MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
  }
}
#endif
// Groupend DRAW

#ifdef GuiConst_FLOAT_SUPPORT_ON
static float Floor(float X)
{
  GuiConst_INT32S I;

  I = X - 2;
  while(++I <= X - 1);
  return I;
}
#endif

//------------------------------------------------------------------------------
static GuiConst_INT32S ReadVar(
   void *VarPtr,
   GuiConst_INT8U VarType)
{
#ifdef GuiConst_FLOAT_SUPPORT_ON
  GuiConst_INT32S tmp;
  GuiConst_INT8U i;
  double dfactor;
  double dx;
  float ffactor;
  float fx;
#endif

  if (VarPtr == 0)
    return (0);
  else
  {
    if (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP)
    {
#ifdef GuiConst_FLOAT_SUPPORT_ON
      if ((VarType == GuiLib_VAR_FLOAT) || (VarType == GuiLib_VAR_DOUBLE))
        sgl.VarExponent = 0;
      else
#endif
        sgl.CurItem.FormatFormat = GuiLib_FORMAT_DEC;
    }
    switch (VarType)
    {
      case GuiLib_VAR_BOOL:
        if (*(GuiConst_INT8U *) VarPtr == 0)
          return (0);
        else
          return (1);

      case GuiLib_VAR_UNSIGNED_CHAR:
        return (*(GuiConst_INT8U *) VarPtr);

      case GuiLib_VAR_SIGNED_CHAR:
        return (*(GuiConst_INT8S *) VarPtr);

      case GuiLib_VAR_UNSIGNED_INT:
        return (*(GuiConst_INT16U *) VarPtr);

      case GuiLib_VAR_SIGNED_INT:
        return (*(GuiConst_INT16S *) VarPtr);

      case GuiLib_VAR_UNSIGNED_LONG:
        return (*(GuiConst_INT32U *) VarPtr);

      case GuiLib_VAR_SIGNED_LONG:
        return (*(GuiConst_INT32S *) VarPtr);

      case GuiLib_VAR_COLOR:
        return (*(GuiConst_INTCOLOR *) VarPtr);

      case GuiLib_VAR_FLOAT:
#ifdef GuiConst_FLOAT_SUPPORT_ON
        fx = *(float*) VarPtr;
        if (fx < 0)
          fx = -fx;
        if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP) && (fx != 0))
        {
          while (Floor(fx + 0.5) > 10)
          {
            fx /= 10;
            sgl.VarExponent++;
          }
          while (Floor(fx) < 1)
          {
            fx *= 10;
            sgl.VarExponent--;
          }
        }
        if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
            (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP))
        {
          ffactor=1.0;
          for (i = 0; i < sgl.CurItem.FormatDecimals ; i++)
            ffactor *= 10.0;
          tmp = (GuiConst_INT32S) (fx * ffactor);
          ffactor *= 100.0;
          if (((GuiConst_INT32S) (fx * ffactor) - (tmp * 100)) >= 45)
            tmp++;
          if (*(float*) VarPtr < 0)
            tmp = -tmp;
        }
        else
          tmp = (GuiConst_INT32S) *(float*) VarPtr;
        return (tmp);
#else
        return (0);
#endif

      case GuiLib_VAR_DOUBLE:
#ifdef GuiConst_FLOAT_SUPPORT_ON
        dx = *(double*) VarPtr;
        if (dx < 0)
          dx = -dx;
        if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP) && (dx != 0))
        {
          while (dx > 10)
          {
            dx /= 10;
            sgl.VarExponent++;
          }
          while (dx < 1)
          {
            dx *= 10;
            sgl.VarExponent--;
          }
        }
        if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
            (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP))
        {
          dfactor=1.0;
          for (i = 0; i < sgl.CurItem.FormatDecimals ; i++)
            dfactor *= 10.0;
          tmp = (GuiConst_INT32S) (dx * dfactor);
          dfactor *= 100.0;
          if (((GuiConst_INT32S) (dx * dfactor) - (tmp * 100)) >= 45)
            tmp++;
          if (*(double*) VarPtr < 0)
            tmp = -tmp;
        }
        else
          tmp = (GuiConst_INT32S) dx;
        return (tmp);
#else
        return (0);
#endif

      default:
        return (0);
    }
  }
}

//------------------------------------------------------------------------------
static GuiConst_INT8U DataNumStr(
   GuiConst_INT32S DataValue,
   GuiConst_INT8U VarType,
   GuiConst_INT8U TextNdx)
{
  GuiConst_CHAR *S1;
  GuiConst_INT8U StrLen, L;
  GuiConst_INT16S I, N, P;
  GuiConst_INT8U Offset;
  GuiConst_INT8U Sign;
  GuiConst_INT8U ShowSign;
  GuiConst_INT8U ZeroPadding;
  GuiConst_INT8U TrailingZeros;
  GuiConst_INT8U ThousandsSeparator;
  GuiConst_INT8U Time;
  GuiConst_INT16S TT1, TT2, TT3;
  GuiConst_INT8U am;
  GuiConst_INT8U DecimalsPos;
#ifdef GuiConst_FLOAT_SUPPORT_ON
  GuiConst_CHAR ExponentStr[5];
#endif

  sgl.VarNumTextStr[0] = 0;
  am = 0;

  Sign = 0;
  switch (VarType)
  {
    case GuiLib_VAR_BOOL:
    case GuiLib_VAR_UNSIGNED_CHAR:
      if (DataValue > 255)
        DataValue = 255;
      else if (DataValue < 0)
        DataValue = 0;
      break;

    case GuiLib_VAR_SIGNED_CHAR:
      if (DataValue > 127)
        DataValue = 127;
      else if (DataValue < -128)
        DataValue = -128;
      if (DataValue < 0)
        Sign = 1;
      break;

    case GuiLib_VAR_UNSIGNED_INT:
      if (DataValue > 65535)
        DataValue = 65535;
      else if (DataValue < 0)
        DataValue = 0;
      break;

    case GuiLib_VAR_SIGNED_INT:
      if (DataValue > 32767)
        DataValue = 32767;
      else if (DataValue < -32768)
        DataValue = -32768;
      if (DataValue < 0)
        Sign = 1;
      break;

    case GuiLib_VAR_SIGNED_LONG:
    case GuiLib_VAR_FLOAT:
    case GuiLib_VAR_DOUBLE:
      if (DataValue < 0)
        Sign = 1;
      break;

    case GuiLib_VAR_COLOR:
      if (DataValue < 0)
          DataValue = 0;
#if GuiConst_PIXEL_BYTE_SIZE == 1
      if (DataValue > 255)
        DataValue = 255;
#elif GuiConst_PIXEL_BYTE_SIZE == 2
      if (DataValue > 65535)
        DataValue = 65535;
#elif GuiConst_PIXEL_BYTE_SIZE == 3
      if (DataValue > 16777215)
        DataValue = 16777215;
#endif
      break;
  }

  if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP))
    DecimalsPos = sgl.CurItem.FormatDecimals;
  else
    DecimalsPos = 0;
  Time = ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_MMSS) ||
          (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_24) ||
          (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_24) ||
          (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_ampm) ||
          (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm) ||
          (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_AMPM) ||
          (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM));
  if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP))
    ShowSign = ((sgl.CurItem.TextPar[TextNdx].BitFlags &
                 GuiLib_BITFLAG_FORMATSHOWSIGN) > 0);
  else
  {
    Sign = 0;
    ShowSign = 0;
  }
  if (sgl.CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_RIGHT)
    ZeroPadding = ((sgl.CurItem.TextPar[TextNdx].BitFlags &
                    GuiLib_BITFLAG_FORMATZEROPADDING) > 0);
  else
    ZeroPadding = 0;
  if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP))
    TrailingZeros = ((sgl.CurItem.TextPar[TextNdx].BitFlags &
                      GuiLib_BITFLAG_FORMATTRAILINGZEROS) > 0);
  else
    TrailingZeros = 0;
  if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_HEX_POSTFIX_H) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_HEX_PREFIX_0X) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_HEX_CLEAN))
    ThousandsSeparator = ((sgl.CurItem.TextPar[TextNdx].BitFlags &
                           GuiLib_BITFLAG_FORMATTHOUSANDSSEP) > 0);
  else
    ThousandsSeparator = 0;

  if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_DEC) ||
      (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP))
  {
    if (Sign)
      ConvertIntToStr(-DataValue, sgl.VarNumTextStr, 10);
    else
      ConvertIntToStr(DataValue, sgl.VarNumTextStr, 10);
  }

  else if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_HEX_POSTFIX_H) ||
           (sgl.CurItem.FormatFormat == GuiLib_FORMAT_HEX_PREFIX_0X) ||
           (sgl.CurItem.FormatFormat == GuiLib_FORMAT_HEX_CLEAN))
  {
    ConvertIntToStr(DataValue, sgl.VarNumTextStr, 16);
    S1 = sgl.VarNumTextStr;
    StrLen = 0;
    while (*S1 != 0)
    {
      if ((*S1 >= 'a') && (*S1 <= 'f'))
        *S1 -= 32;
      S1++;
      StrLen++;
    }
    if (DataValue < 0)
    {
      while ((StrLen > 1) && (StrLen >= sgl.CurItem.FormatFieldWidth))
      {
        if (sgl.VarNumTextStr[0] == 'F')
        {
          for (N = 0; N < StrLen; N++)
            sgl.VarNumTextStr[N] = sgl.VarNumTextStr[N + 1];
          sgl.VarNumTextStr[StrLen] = 0;
          StrLen--;
        }
        else
          break;
      }

    }
    if (StrLen >= GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);

    switch (sgl.CurItem.FormatFormat)
    {
      case GuiLib_FORMAT_HEX_POSTFIX_H:
        if (StrLen < GuiConst_MAX_VARNUM_TEXT_LEN - 1)
        {
          #ifdef GuiConst_CODEVISION_COMPILER
          strcatf(sgl.VarNumTextStr, "h");
          #else
          strcat(sgl.VarNumTextStr, "h");
          #endif
        }
        break;
      case GuiLib_FORMAT_HEX_PREFIX_0X:
        if (StrLen < GuiConst_MAX_VARNUM_TEXT_LEN - 2)
        {
          for (N = StrLen - 1; N >= 0; N--)
            sgl.VarNumTextStr[N + 2] = sgl.VarNumTextStr[N];
          sgl.VarNumTextStr[0] = '0';
          sgl.VarNumTextStr[1] = 'x';
        }
        break;
    }
  }

  else if (Time)
  {
    if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_24) ||
        (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm) ||
        (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM))
    {
      DataValue %= 360000;
      TT1 = DataValue / 3600;
      TT2 = (DataValue % 3600) / 60;
      TT3 = DataValue % 60;
    }
    else
    {
      DataValue %= 6000;
      TT1 = DataValue / 60;
      TT2 = DataValue % 60;
      TT3 = -1;
    }

    if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_ampm) ||
        (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm) ||
        (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_AMPM) ||
        (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM))
    {
      am = (TT1 < 12);
      TT1 %= 12;
      if (TT1 == 0)
        TT1 = 12;
    }
    else
      am = 0;   // To avoid compiler warning

    if (ZeroPadding && (TT1 < 10))
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(sgl.VarNumTextStr, "0");
#else
      strcat(sgl.VarNumTextStr, "0");
#endif
    ConvertIntToStr(TT1, sgl.VarNumTextStr + strlen(sgl.VarNumTextStr), 10);

#ifdef GuiConst_CODEVISION_COMPILER
    strcatf(sgl.VarNumTextStr, ":");
#else
    strcat(sgl.VarNumTextStr, ":");
#endif

    if (TT2 < 10)
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(sgl.VarNumTextStr, "0");
#else
      strcat(sgl.VarNumTextStr, "0");
#endif
    ConvertIntToStr(TT2, sgl.VarNumTextStr + strlen(sgl.VarNumTextStr), 10);

    if (TT3 >= 0)
    {
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(sgl.VarNumTextStr, ":");
#else
      strcat(sgl.VarNumTextStr, ":");
#endif

      if (TT3 < 10)
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(sgl.VarNumTextStr, "0");
#else
        strcat(sgl.VarNumTextStr, "0");
#endif
      ConvertIntToStr(TT3, sgl.VarNumTextStr + strlen(sgl.VarNumTextStr), 10);
    }

    if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_ampm) ||
        (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_ampm))
    {
      if (am)
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(sgl.VarNumTextStr, "am");
#else
        strcat(sgl.VarNumTextStr, "am");
#endif
      else
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(sgl.VarNumTextStr, "pm");
#else
        strcat(sgl.VarNumTextStr, "pm");
#endif
    }
    else if ((sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMM_12_AMPM) ||
             (sgl.CurItem.FormatFormat == GuiLib_FORMAT_TIME_HHMMSS_12_AMPM))
    {
      if (am)
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(sgl.VarNumTextStr, "AM");
#else
        strcat(sgl.VarNumTextStr, "AM");
#endif
      else
#ifdef GuiConst_CODEVISION_COMPILER
        strcatf(sgl.VarNumTextStr, "PM");
#else
        strcat(sgl.VarNumTextStr, "PM");
#endif
    }
  }
  else
    return (0);

  StrLen = 0;
  S1 = sgl.VarNumTextStr;
  while (*S1 != 0)
  {
    StrLen++;
    S1++;
  }

  if (DecimalsPos > 0)
  {
    L = DecimalsPos + 1;
    if (StrLen < L)
    {
      if (L > GuiConst_MAX_VARNUM_TEXT_LEN)
        return (0);
      for (N = 1; N <= StrLen; N++)
        sgl.VarNumTextStr[L - N] = sgl.VarNumTextStr[StrLen - N];
      for (N = 0; N < L - StrLen; N++)
        sgl.VarNumTextStr[N] = '0';

      StrLen = L;
    }

    if ((!TrailingZeros) && (!Time) && (VarType != GuiLib_VAR_BOOL))
    {
      L = StrLen;
      for (N = L - 1; N > L - sgl.CurItem.FormatDecimals; N--)
        if (sgl.VarNumTextStr[N] == '0')
        {
          DecimalsPos--;
          StrLen--;
        }
        else
          break;
    }

    if (StrLen >= GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);
    P = StrLen - DecimalsPos;
    for (N = StrLen; N > P; N--)
      sgl.VarNumTextStr[N] = sgl.VarNumTextStr[N - 1];
    if (ReadByte(GuiFont_DecimalChar[GuiLib_LanguageIndex]) == GuiLib_DECIMAL_PERIOD)
      sgl.VarNumTextStr[P] = '.';
    else
      sgl.VarNumTextStr[P] = ',';
    StrLen++;
    sgl.VarNumTextStr[StrLen] = 0;
  }
  else
    P = StrLen;

  if (ThousandsSeparator)
  {
    I = 0;
    while (P > 0)
    {
      if ((I > 0) && (I % 3 ==0 ))
      {
        for (N = StrLen; N > P; N--)
          sgl.VarNumTextStr[N] = sgl.VarNumTextStr[N - 1];
        if (ReadByte(GuiFont_DecimalChar[GuiLib_LanguageIndex]) == GuiLib_DECIMAL_PERIOD)
          sgl.VarNumTextStr[P] = ',';
        else
          sgl.VarNumTextStr[P] = '.';
        StrLen++;
        if (StrLen >= GuiConst_MAX_VARNUM_TEXT_LEN)
          return (0);
        sgl.VarNumTextStr[StrLen] = 0;
      }
      I++;
      P--;
    }
  }

  if (Sign || ShowSign)
  {
    if (StrLen > GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);
    for (N = StrLen; N >= 1; N--)
      sgl.VarNumTextStr[N] = sgl.VarNumTextStr[N - 1];
    if (Sign)
      sgl.VarNumTextStr[0] = '-';
    else
      sgl.VarNumTextStr[0] = '+';
    StrLen++;
    sgl.VarNumTextStr[StrLen] = 0;
  }

#ifdef GuiConst_FLOAT_SUPPORT_ON
  if (sgl.CurItem.FormatFormat == GuiLib_FORMAT_EXP)
  {
    N = sgl.VarExponent;
    if (N < 0)
      N = -N;
    ConvertIntToStr(N, ExponentStr, 10);
    S1 = ExponentStr;
    N = 0;
    while (*S1 != 0)
    {
      S1++;
      N++;
    }
    if (N == 1)
      I = 2;
    else
      I = N;
    if (StrLen + 2 + I >= GuiConst_MAX_VARNUM_TEXT_LEN)
      return (0);
#ifdef GuiConst_CODEVISION_COMPILER
    strcatf(sgl.VarNumTextStr, "E");
#else
    strcat(sgl.VarNumTextStr, "E");
#endif
    StrLen++;
    if (sgl.VarExponent >= 0)
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(sgl.VarNumTextStr, "+");
#else
      strcat(sgl.VarNumTextStr, "+");
#endif
    else
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(sgl.VarNumTextStr, "-");
#else
      strcat(sgl.VarNumTextStr, "-");
#endif
    StrLen++;
    if (N == 1)
    {
#ifdef GuiConst_CODEVISION_COMPILER
      strcatf(sgl.VarNumTextStr, "0");
#else
      strcat(sgl.VarNumTextStr, "0");
#endif
      StrLen++;
    }
    strcat(sgl.VarNumTextStr, ExponentStr);
    StrLen += N;
  }
#endif

  if (sgl.CurItem.FormatFieldWidth > 0)
  {
    if (StrLen > sgl.CurItem.FormatFieldWidth)
    {
      for (N = 0; N < sgl.CurItem.FormatFieldWidth; N++)
        sgl.VarNumTextStr[N] = '-';
    }
    else
    {
      if (sgl.CurItem.FormatFieldWidth > GuiConst_MAX_VARNUM_TEXT_LEN)
        return (0);
      if (ZeroPadding && (!Time))
      {
        if ((sgl.VarNumTextStr[0] == '-') || (sgl.VarNumTextStr[0] == '+'))
          Offset = 1;
        else
          Offset = 0;
        for (N = 1; N <= StrLen - Offset; N++)
          sgl.VarNumTextStr[sgl.CurItem.FormatFieldWidth - N] =
            sgl.VarNumTextStr[StrLen - N];
        for (N = 0; N < sgl.CurItem.FormatFieldWidth - StrLen; N++)
          sgl.VarNumTextStr[N + Offset] = '0';
      }
      else if (sgl.CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_LEFT)
      {
        for (N = StrLen; N < sgl.CurItem.FormatFieldWidth; N++)
          sgl.VarNumTextStr[N] = ' ';
      }
      else if (sgl.CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_CENTER)
      {
        Offset = (sgl.CurItem.FormatFieldWidth - StrLen) / 2;
        if (Offset > 0)
        {
          for (N = StrLen - 1; N >= 0; N--)
            sgl.VarNumTextStr[N + Offset] = sgl.VarNumTextStr[N];
          for (N = 0; N < Offset; N++)
            sgl.VarNumTextStr[N] = ' ';
        }
        Offset = sgl.CurItem.FormatFieldWidth - StrLen - Offset;
        if (Offset > 0)
          for (N = sgl.CurItem.FormatFieldWidth - Offset;
               N < sgl.CurItem.FormatFieldWidth; N++)
            sgl.VarNumTextStr[N] = ' ';
      }
      else if (sgl.CurItem.FormatAlignment == GuiLib_FORMAT_ALIGNMENT_RIGHT)
      {
        for (N = 1; N <= StrLen; N++)
          sgl.VarNumTextStr[sgl.CurItem.FormatFieldWidth - N] =
            sgl.VarNumTextStr[StrLen - N];
        for (N = 0; N < sgl.CurItem.FormatFieldWidth - StrLen; N++)
          sgl.VarNumTextStr[N] = ' ';
      }
    }
    sgl.VarNumTextStr[sgl.CurItem.FormatFieldWidth] = 0;
    return (sgl.CurItem.FormatFieldWidth);
  }
  else
  {
    sgl.VarNumTextStr[StrLen] = 0;
    return (StrLen);
  }
}

#ifdef GuiConst_REMOTE_STRUCT_DATA
//------------------------------------------------------------------------------
static GuiLib_StructPtr GetRemoteStructData(
   GuiConst_INT32S StructIndex)
{
  sgl.RemoteStructOffset =
     (GuiConst_INT32U PrefixRom)GuiStruct_StructPtrList[StructIndex];
  GuiLib_RemoteDataReadBlock(
     sgl.RemoteStructOffset,
     1,
     sgl.GuiLib_RemoteStructBuffer);
  sgl.RemoteStructOffset++;
  return((GuiLib_StructPtr)&sgl.GuiLib_RemoteStructBuffer[0]);
}

#ifdef GuiConst_REMOTE_TEXT_DATA
//------------------------------------------------------------------------------
static GuiConst_INT16U GetRemoteText(
   GuiConst_INT16U TextIndex)
{
  GuiConst_INT32S TextOfs;

  if (TextIndex != sgl.CurRemoteText)
  {
    if (sgl.RemoteTextTableOfs == -1)
      GuiLib_RemoteTextReadBlock(4, 4, &sgl.RemoteTextTableOfs);

    GuiLib_RemoteTextReadBlock(sgl.RemoteTextTableOfs + 6 * TextIndex, 4, &TextOfs);
    GuiLib_RemoteTextReadBlock(
       sgl.RemoteTextTableOfs + 6 * TextIndex + 4, 2, &sgl.RemoteTextLen);

#ifdef GuiConst_CHARMODE_ANSI
    GuiLib_RemoteTextReadBlock(TextOfs, sgl.RemoteTextLen, sgl.GuiLib_RemoteTextBuffer);
#else
    GuiLib_RemoteTextReadBlock(TextOfs, 2 * sgl.RemoteTextLen, sgl.GuiLib_RemoteTextBuffer);
#endif
    sgl.CurRemoteText = TextIndex;
  }
  return(sgl.RemoteTextLen);
}
#endif

#endif

#ifdef GuiConst_CHARMODE_UNICODE
//------------------------------------------------------------------------------
static void ExtractUnicodeString(
   GuiConst_INT8U *ItemTextPtr,
   GuiConst_INT16U TextLength)
{
  GuiConst_INT16U P;

  for (P = 0; P < TextLength; P++)
    sgl.UnicodeTextBuf[P] = GetItemWord(&ItemTextPtr);
  sgl.UnicodeTextBuf[TextLength] = 0;
}
#endif

//------------------------------------------------------------------------------
static void SetCurFont(
   GuiConst_INT8U FontIndex)
{
  if (FontIndex >= GuiFont_FontCnt)
    sgl.CurFont = (GuiLib_FontRecPtr)ReadWord(GuiFont_FontList[0]);
  else
    sgl.CurFont = (GuiLib_FontRecPtr)ReadWord(GuiFont_FontList[FontIndex]);
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void UpdateBackgroundBitmap(void)
{
  GuiConst_INT8U BackgroundOn;
  GuiConst_INT16U BackgroundX1, BackgroundY1, BackgroundX2, BackgroundY2;
  GuiConst_INT16S N;

  for (N = 0; N < GuiConst_MAX_BACKGROUND_BITMAPS; N++)
    if (sgl.BackgrBitmapAry[N].InUse)
    {
      BackgroundOn = 0;
      switch (sgl.CurItem.ItemType)
      {
        case GuiLib_ITEM_TEXT:
        case GuiLib_ITEM_VAR:
          BackgroundOn = 1;
          if (sgl.CurItem.TextPar[0].BackBoxSizeX == 0)
          {
            BackgroundX1 = sgl.CurItem.DrawnX1;
            BackgroundY1 = sgl.CurItem.DrawnY1;
            BackgroundX2 = sgl.CurItem.DrawnX2;
            BackgroundY2 = sgl.CurItem.DrawnY2;
          }
          else
          {
            SetCurFont(sgl.CurItem.TextPar[0].FontIndex);
            SetBackBox(0);
            BackgroundX1 = sgl.BbX1;
            BackgroundY1 = sgl.BbY1;
            BackgroundX2 = sgl.BbX2;
            BackgroundY2 = sgl.BbY2;
          }
          break;

        case GuiLib_ITEM_STRUCTURE:
        case GuiLib_ITEM_STRUCTARRAY:
        case GuiLib_ITEM_STRUCTCOND:
          if (sgl.CurItem.Drawn)
          {
            BackgroundOn = 1;
            BackgroundX1 = sgl.CurItem.DrawnX1;
            BackgroundY1 = sgl.CurItem.DrawnY1;
            BackgroundX2 = sgl.CurItem.DrawnX2;
            BackgroundY2 = sgl.CurItem.DrawnY2;
          }
          break;

        case GuiLib_ITEM_TEXTBLOCK:
        case GuiLib_ITEM_VARBLOCK:
        case GuiLib_ITEM_CHECKBOX:
        case GuiLib_ITEM_BUTTON:
        case GuiLib_ITEM_EDITBOX:
        case GuiLib_ITEM_PANEL:
        case GuiLib_ITEM_MEMO:
        case GuiLib_ITEM_LISTBOX:
        case GuiLib_ITEM_COMBOBOX:
        case GuiLib_ITEM_SCROLLBOX:
        case GuiLib_ITEM_SCROLLAREA:
        case GuiLib_ITEM_PROGRESSBAR:
          if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_TRANSPARENT)
          {
            BackgroundOn = 1;
            SetBackColorBox(sgl.CurItem.X1, sgl.CurItem.Y1,
                            sgl.CurItem.X2, sgl.CurItem.Y2,
                            0);
            BackgroundX1 = sgl.BbX1;
            BackgroundY1 = sgl.BbY1;
            BackgroundX2 = sgl.BbX2;
            BackgroundY2 = sgl.BbY2;
          }
          break;
#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
        case GuiLib_ITEM_RADIOBUTTON:
          if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_TRANSPARENT)
          {
            BackgroundOn = 1;
            SetBackColorBox(sgl.CurItem.X1,
                            sgl.CurItem.Y1,
                            sgl.CurItem.X2,
                            sgl.CurItem.Y2 +
                              (sgl.CurItem.CompPars.CompRadioButton.Count - 1) *
                               sgl.CurItem.CompPars.CompRadioButton.InterDistance,
                            0);
            BackgroundX1 = sgl.BbX1;
            BackgroundY1 = sgl.BbY1;
            BackgroundX2 = sgl.BbX2;
            BackgroundY2 = sgl.BbY2;
          }
          break;
#endif
      }
      if (BackgroundOn)
        GuiLib_ShowBitmapArea(sgl.BackgrBitmapAry[N].Index,
           sgl.BackgrBitmapAry[N].X, sgl.BackgrBitmapAry[N].Y,
           BackgroundX1, BackgroundY1, BackgroundX2, BackgroundY2, -1);
    }
}
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void StartClipping(
   GuiConst_INT8U Clipping)
{
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  sgl.CurItem.ClipRectX1 += sgl.CoordOrigoX;
  sgl.CurItem.ClipRectY1 += sgl.CoordOrigoY;
  sgl.CurItem.ClipRectX2 += sgl.CoordOrigoX;
  sgl.CurItem.ClipRectY2 += sgl.CoordOrigoY;
#endif
  if (Clipping)
  {
    OrderCoord(&sgl.CurItem.ClipRectX1, &sgl.CurItem.ClipRectX2);
    OrderCoord(&sgl.CurItem.ClipRectY1, &sgl.CurItem.ClipRectY2);
    if (sgl.CurItem.ClipRectX1 < sgl.ActiveAreaX1)
      sgl.CurItem.ClipRectX1 = sgl.ActiveAreaX1;
    if (sgl.CurItem.ClipRectY1 < sgl.ActiveAreaY1)
      sgl.CurItem.ClipRectY1 = sgl.ActiveAreaY1;
    if (sgl.CurItem.ClipRectX2 > sgl.ActiveAreaX2)
      sgl.CurItem.ClipRectX2 = sgl.ActiveAreaX2;
    if (sgl.CurItem.ClipRectY2 > sgl.ActiveAreaY2)
      sgl.CurItem.ClipRectY2 = sgl.ActiveAreaY2;
  }
  else
  {
    sgl.CurItem.ClipRectX1 = sgl.ActiveAreaX1;
    sgl.CurItem.ClipRectY1 = sgl.ActiveAreaY1;
    sgl.CurItem.ClipRectX2 = sgl.ActiveAreaX2;
    sgl.CurItem.ClipRectY2 = sgl.ActiveAreaY2;
  }
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  sgl.CurItem.ClipRectX1 -= sgl.CoordOrigoX;
  sgl.CurItem.ClipRectY1 -= sgl.CoordOrigoY;
  sgl.CurItem.ClipRectX2 -= sgl.CoordOrigoX;
  sgl.CurItem.ClipRectY2 -= sgl.CoordOrigoY;
#endif
  GuiLib_SetClipping(sgl.CurItem.ClipRectX1, sgl.CurItem.ClipRectY1,
                     sgl.CurItem.ClipRectX2, sgl.CurItem.ClipRectY2);
}
#endif

//------------------------------------------------------------------------------
static void PrepareInternalStruct(void)
{
  sgl.Memory.X[GuiLib_MEMORY_CNT] = sgl.CurItem.X1;
  sgl.Memory.Y[GuiLib_MEMORY_CNT] = sgl.CurItem.Y1;
}

//------------------------------------------------------------------------------
static void DrawSubStruct(
   GuiConst_INT16U SubStructIndex,
   GuiConst_INT8U ColorInvert,
   GuiConst_INT8U SubInt)
{
  GuiLib_ItemRec RemCurItem;
  GuiLib_StructPtr StructToCall;
  GuiConst_INT8U RemBackBox;
  GuiConst_INT16S RemBbX1, RemBbX2;
  GuiConst_INT16S RemBbY1, RemBbY2;
  GuiConst_INT8U RemDrawn;
  GuiConst_INT16S RemDrawnX1, RemDrawnY1, RemDrawnX2, RemDrawnY2;
  GuiConst_INT16S RemAutoRedrawSaveIndex;
  GuiConst_INT16S RemAutoRedrawParent;
  GuiConst_INT16S RemAutoRedrawLatest;
#ifdef GuiConst_REMOTE_STRUCT_DATA
  GuiConst_INT32U RemRemoteStructOffset;
#endif

  if (SubStructIndex != 0xFFFF)
  {
    RemAutoRedrawParent = sgl.AutoRedrawParent;
    RemAutoRedrawLatest = sgl.AutoRedrawLatest;

    sgl.AutoRedrawParent = sgl.AutoRedrawLatest;

    if (SubInt)
      memcpy(&RemCurItem, &sgl.CurItem, sizeof(GuiLib_ItemRec));

#ifdef GuiConst_REMOTE_STRUCT_DATA
    RemRemoteStructOffset = sgl.RemoteStructOffset;
#endif
    RemBackBox = (sgl.CurItem.TextPar[0].BackBoxSizeX);
    RemBbX1 = sgl.BbX1;
    RemBbX2 = sgl.BbX2;
    RemBbY1 = sgl.BbY1;
    RemBbY2 = sgl.BbY2;
    RemDrawn = gl.Drawn;
    RemDrawnX1 = gl.DrawnX1;
    RemDrawnY1 = gl.DrawnY1;
    RemDrawnX2 = gl.DrawnX2;
    RemDrawnY2 = gl.DrawnY2;
    ResetDrawLimits();
    RemAutoRedrawSaveIndex = sgl.AutoRedrawSaveIndex;

    sgl.DisplayLevel++;
    StructToCall =
#ifdef GuiConst_REMOTE_STRUCT_DATA
       GetRemoteStructData(SubStructIndex);
#else
       (GuiLib_StructPtr)ReadWord(GuiStruct_StructPtrList[SubStructIndex]);
#endif
    DrawStructure(StructToCall, ColorInvert);
    sgl.DisplayLevel--;

    if (RemBackBox)
    {
      sgl.CurItem.Drawn = 1;
      sgl.CurItem.DrawnX1 = RemBbX1;
      sgl.CurItem.DrawnY1 = RemBbY1;
      sgl.CurItem.DrawnX2 = RemBbX2;
      sgl.CurItem.DrawnY2 = RemBbY2;

      gl.DrawnX1 = GuiLib_GET_MIN(sgl.BbX1, RemDrawnX1);
      gl.DrawnY1 = GuiLib_GET_MIN(sgl.BbY1, RemDrawnY1);
      gl.DrawnX2 = GuiLib_GET_MAX(sgl.BbX2, RemDrawnX2);
      gl.DrawnY2 = GuiLib_GET_MAX(sgl.BbY2, RemDrawnY2);
    }
    else if (gl.Drawn)
    {
      sgl.CurItem.Drawn = 1;
      sgl.CurItem.DrawnX1 = gl.DrawnX1;
      sgl.CurItem.DrawnY1 = gl.DrawnY1;
      sgl.CurItem.DrawnX2 = gl.DrawnX2;
      sgl.CurItem.DrawnY2 = gl.DrawnY2;

      gl.DrawnX1 = GuiLib_GET_MIN(gl.DrawnX1, RemDrawnX1);
      gl.DrawnY1 = GuiLib_GET_MIN(gl.DrawnY1, RemDrawnY1);
      gl.DrawnX2 = GuiLib_GET_MAX(gl.DrawnX2, RemDrawnX2);
      gl.DrawnY2 = GuiLib_GET_MAX(gl.DrawnY2, RemDrawnY2);
    }
    else
    {
      gl.Drawn = RemDrawn;
      gl.DrawnX1 = RemDrawnX1;
      gl.DrawnY1 = RemDrawnY1;
      gl.DrawnX2 = RemDrawnX2;
      gl.DrawnY2 = RemDrawnY2;
    }
    sgl.AutoRedrawSaveIndex = RemAutoRedrawSaveIndex;

#ifdef GuiConst_REMOTE_STRUCT_DATA
    sgl.RemoteStructOffset = RemRemoteStructOffset;
#endif

    if (SubInt)
      memcpy(&sgl.CurItem, &RemCurItem, sizeof(GuiLib_ItemRec));

    sgl.AutoRedrawLatest = RemAutoRedrawLatest;
    sgl.AutoRedrawParent = RemAutoRedrawParent;
  }
}

#include "GuiItems.c"

//------------------------------------------------------------------------------
static void DrawStructure(
   GuiLib_StructPtr Structure,
   GuiConst_INT8U ColorInvert) PrefixReentrant
{
  GuiConst_INT16S ItemNdx;
  GuiConst_INT16S X, RemAutoRedrawLatest;
  void *XVarPtr;
  void *YVarPtr;
  GuiConst_INT8U ItemCnt;
  GuiConst_INT8U *LocalItemDataPtr;
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  GuiConst_INT16S I;
#endif
#ifdef GuiConst_REMOTE_STRUCT_DATA
  GuiConst_INT8U ItemSizeBuf[2];
  GuiConst_INT16U ItemSize;
#endif

#ifdef GuiConst_REMOTE_STRUCT_DATA
  LocalItemDataPtr = sgl.GuiLib_RemoteStructBuffer;
#else
  LocalItemDataPtr = (GuiConst_INT8U*)Structure;
#endif
  ItemCnt = GetItemByte(&LocalItemDataPtr);

  for (ItemNdx = 0; ItemNdx < ItemCnt; ItemNdx++)
  {
    RemAutoRedrawLatest = sgl.AutoRedrawLatest;

#ifdef GuiConst_REMOTE_STRUCT_DATA
    GuiLib_RemoteDataReadBlock(
       sgl.RemoteStructOffset,
       2,
       (GuiConst_INT8U*)&ItemSizeBuf);
    ItemSize = (256 * (GuiConst_INT16U)ItemSizeBuf[1]) + ItemSizeBuf[0];
    sgl.RemoteStructOffset += 2;
    GuiLib_RemoteDataReadBlock(
       sgl.RemoteStructOffset,
       ItemSize,
       sgl.GuiLib_RemoteItemBuffer);
    sgl.RemoteStructOffset += ItemSize;
    LocalItemDataPtr = &sgl.GuiLib_RemoteItemBuffer[0];
#endif

    sgl.ItemDataPtr = LocalItemDataPtr;
    ReadItem(GuiLib_LanguageIndex);
    LocalItemDataPtr = sgl.ItemDataPtr;

    if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CLEARAREA +
                             GuiLib_ITEMBIT_TEXT +
                             GuiLib_ITEMBIT_TEXTBLOCK +
                             GuiLib_ITEMBIT_DOT +
                             GuiLib_ITEMBIT_LINE +
                             GuiLib_ITEMBIT_FRAME +
                             GuiLib_ITEMBIT_BLOCK +
                             GuiLib_ITEMBIT_CIRCLE +
                             GuiLib_ITEMBIT_ELLIPSE +
                             GuiLib_ITEMBIT_BITMAP +
                             GuiLib_ITEMBIT_BACKGROUND +
                             GuiLib_ITEMBIT_STRUCTURE +
                             GuiLib_ITEMBIT_STRUCTARRAY +
                             GuiLib_ITEMBIT_ACTIVEAREA +
                             GuiLib_ITEMBIT_CLIPRECT +
                             GuiLib_ITEMBIT_VAR +
                             GuiLib_ITEMBIT_VARBLOCK +
                             GuiLib_ITEMBIT_TOUCHAREA +
                             GuiLib_ITEMBIT_SCROLLBOX +
                             GuiLib_ITEMBIT_GRAPH +
                             GuiLib_ITEMBIT_GRAPHICSLAYER)) ||
        (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                             GuiLib_ITEMBIT_ROUNDEDBLOCK +
                             GuiLib_ITEMBIT_QUARTERCIRCLE +
                             GuiLib_ITEMBIT_QUARTERELLIPSE +
                             GuiLib_ITEMBIT_CHECKBOX +
                             GuiLib_ITEMBIT_RADIOBUTTON +
                             GuiLib_ITEMBIT_BUTTON +
                             GuiLib_ITEMBIT_EDITBOX +
                             GuiLib_ITEMBIT_PANEL +
                             GuiLib_ITEMBIT_MEMO +
                             GuiLib_ITEMBIT_LISTBOX +
                             GuiLib_ITEMBIT_COMBOBOX +
                             GuiLib_ITEMBIT_SCROLLAREA +
                             GuiLib_ITEMBIT_PROGRESSBAR +
                             GuiLib_ITEMBIT_STRUCTCOND +
                             GuiLib_ITEMBIT_POSCALLBACK)))
    {
      if (sgl.CommonByte2 & 0x10)
      {
        XVarPtr = (void*)ReadWord(GuiStruct_VarPtrList[sgl.X1VarIdx]);
        sgl.X1VarType = ReadByte(GuiStruct_VarTypeList[sgl.X1VarIdx]);
      }
      else
        XVarPtr = 0;
      if (XVarPtr != 0)
        sgl.ItemX1 += ReadVar(XVarPtr, sgl.X1VarType);
      if (sgl.X1MemoryRead > 0)
        sgl.ItemX1 += sgl.Memory.X[sgl.X1MemoryRead - 1];
      if (sgl.X1Mode == GuiLib_COOR_REL)
        sgl.ItemX1 += sgl.CurItem.RX;
      else if (sgl.X1Mode == GuiLib_COOR_REL_1)
        sgl.ItemX1 += sgl.CurItem.RX1;
      else if (sgl.X1Mode == GuiLib_COOR_REL_2)
        sgl.ItemX1 += sgl.CurItem.RX2;
      if (sgl.X1MemoryWrite > 0)
        sgl.Memory.X[sgl.X1MemoryWrite - 1] = sgl.ItemX1;
      sgl.CurItem.X1 = sgl.ItemX1;

      if (sgl.CommonByte2 & 0x20)
      {
        YVarPtr = (void*)ReadWord(GuiStruct_VarPtrList[sgl.Y1VarIdx]);
        sgl.Y1VarType = ReadByte(GuiStruct_VarTypeList[sgl.Y1VarIdx]);
      }
      else
        YVarPtr = 0;
      if (YVarPtr != 0)
        sgl.ItemY1 += ReadVar(YVarPtr, sgl.Y1VarType);
      if (sgl.Y1MemoryRead > 0)
        sgl.ItemY1 += sgl.Memory.Y[sgl.Y1MemoryRead - 1];
      if (sgl.Y1Mode == GuiLib_COOR_REL)
        sgl.ItemY1 += sgl.CurItem.RY;
      else if (sgl.Y1Mode == GuiLib_COOR_REL_1)
        sgl.ItemY1 += sgl.CurItem.RY1;
      else if (sgl.Y1Mode == GuiLib_COOR_REL_2)
        sgl.ItemY1 += sgl.CurItem.RY2;
      if (sgl.Y1MemoryWrite > 0)
        sgl.Memory.Y[sgl.Y1MemoryWrite - 1] = sgl.ItemY1;
      sgl.CurItem.Y1 = sgl.ItemY1;

      sgl.CurItem.RX = sgl.CurItem.X1;
      sgl.CurItem.RY = sgl.CurItem.Y1;
    }

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_SCROLLBOX)
    {
      sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].X1 = sgl.CurItem.X1;
      sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].Y1 = sgl.CurItem.Y1;
      sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].BackColor = sgl.CurItem.BackColor;

#ifndef GuiConst_SCROLLITEM_BAR_NONE
      if ((sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].BarMode & 0x03) ==
         GuiLib_COOR_REL)
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].BarPositionX += sgl.CurItem.X1;
      if (((sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].BarMode >> 2) & 0x03) ==
         GuiLib_COOR_REL)
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].BarPositionY += sgl.CurItem.Y1;
#endif
#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
      if ((sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].IndicatorMode & 0x03) ==
         GuiLib_COOR_REL)
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].IndicatorPositionX += sgl.CurItem.X1;
      if (((sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].IndicatorMode >> 2) & 0x03) ==
         GuiLib_COOR_REL)
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].IndicatorPositionY += sgl.CurItem.Y1;
#endif
    }
#endif

    if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CLEARAREA +
                             GuiLib_ITEMBIT_TEXTBLOCK +
                             GuiLib_ITEMBIT_VARBLOCK +
                             GuiLib_ITEMBIT_LINE +
                             GuiLib_ITEMBIT_FRAME +
                             GuiLib_ITEMBIT_BLOCK +
                             GuiLib_ITEMBIT_BITMAP +
                             GuiLib_ITEMBIT_BACKGROUND +
                             GuiLib_ITEMBIT_ACTIVEAREA +
                             GuiLib_ITEMBIT_CLIPRECT +
                             GuiLib_ITEMBIT_TOUCHAREA +
                             GuiLib_ITEMBIT_GRAPH +
                             GuiLib_ITEMBIT_GRAPHICSLAYER)) ||
        (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                             GuiLib_ITEMBIT_ROUNDEDBLOCK +
                             GuiLib_ITEMBIT_BUTTON +
                             GuiLib_ITEMBIT_EDITBOX +
                             GuiLib_ITEMBIT_PANEL +
                             GuiLib_ITEMBIT_MEMO +
                             GuiLib_ITEMBIT_LISTBOX +
                             GuiLib_ITEMBIT_COMBOBOX +
                             GuiLib_ITEMBIT_SCROLLAREA +
                             GuiLib_ITEMBIT_PROGRESSBAR)))
    {
      if (sgl.CommonByte2 & 0x40)
      {
        XVarPtr = (void*)ReadWord(GuiStruct_VarPtrList[sgl.X2VarIdx]);
        sgl.X2VarType = ReadByte(GuiStruct_VarTypeList[sgl.X2VarIdx]);
      }
      else
        XVarPtr = 0;
      if (XVarPtr != 0)
        sgl.ItemX2 += ReadVar(XVarPtr, sgl.X2VarType);
      if (sgl.X2MemoryRead > 0)
        sgl.ItemX2 += sgl.Memory.X[sgl.X2MemoryRead - 1];
      if (sgl.X2Mode == GuiLib_COOR_REL)
        sgl.ItemX2 += sgl.CurItem.X1;
      else if (sgl.X2Mode == GuiLib_COOR_REL_1)
        sgl.ItemX2 += sgl.CurItem.RX1;
      else if (sgl.X2Mode == GuiLib_COOR_REL_2)
        sgl.ItemX2 += sgl.CurItem.RX2;
      if (sgl.X2MemoryWrite > 0)
        sgl.Memory.X[sgl.X2MemoryWrite - 1] = sgl.ItemX2;
      sgl.CurItem.X2 = sgl.ItemX2;

      if (sgl.CommonByte2 & 0x80)
      {
        YVarPtr = (void*)ReadWord(GuiStruct_VarPtrList[sgl.Y2VarIdx]);
        sgl.Y2VarType = ReadByte(GuiStruct_VarTypeList[sgl.Y2VarIdx]);
      }
      else
        YVarPtr = 0;
      if (YVarPtr != 0)
        sgl.ItemY2 += ReadVar(YVarPtr, sgl.Y2VarType);
      if (sgl.Y2MemoryRead > 0)
        sgl.ItemY2 += sgl.Memory.Y[sgl.Y2MemoryRead - 1];
      if (sgl.Y2Mode == GuiLib_COOR_REL)
        sgl.ItemY2 += sgl.CurItem.Y1;
      else if (sgl.Y2Mode == GuiLib_COOR_REL_1)
        sgl.ItemY2 += sgl.CurItem.RY1;
      else if (sgl.Y2Mode == GuiLib_COOR_REL_2)
        sgl.ItemY2 += sgl.CurItem.RY2;
      if (sgl.Y2MemoryWrite > 0)
        sgl.Memory.Y[sgl.Y2MemoryWrite - 1] = sgl.ItemY2;
      sgl.CurItem.Y2 = sgl.ItemY2;
    }
#ifdef GuiConst_ITEM_CHECKBOX_INUSE
    else if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_CHECKBOX)
    {
      switch (sgl.CurItem.CompPars.CompCheckBox.Style)
      {
        case GuiLib_CHECKBOX_FLAT:
        case GuiLib_CHECKBOX_3D:
        case GuiLib_CHECKBOX_NONE:
          sgl.CurItem.X2 = sgl.CurItem.X1 +
                           sgl.CurItem.CompPars.CompCheckBox.Size - 1;
          sgl.CurItem.Y2 = sgl.CurItem.Y1 +
                           sgl.CurItem.CompPars.CompCheckBox.Size - 1;
          break;
        case GuiLib_CHECKBOX_ICON:
          sgl.CurItem.X2 = sgl.CurItem.X1;
          sgl.CurItem.Y2 = sgl.CurItem.Y1;
          break;
        case GuiLib_CHECKBOX_BITMAP:
          sgl.CurItem.X2 = sgl.CurItem.X1;
          sgl.CurItem.Y2 = sgl.CurItem.Y1;
          break;
      }
    }
#endif
#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
    else if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_RADIOBUTTON)
    {
      switch (sgl.CurItem.CompPars.CompRadioButton.Style)
      {
        case GuiLib_RADIOBUTTON_FLAT:
        case GuiLib_RADIOBUTTON_3D:
          sgl.CurItem.X2 = sgl.CurItem.X1 +
                           2 * sgl.CurItem.CompPars.CompRadioButton.Size;
          sgl.CurItem.Y2 = sgl.CurItem.Y1 +
                           2 * sgl.CurItem.CompPars.CompRadioButton.Size;
          break;
        case GuiLib_RADIOBUTTON_ICON:
          sgl.CurItem.X2 = sgl.CurItem.X1;
          sgl.CurItem.Y2 = sgl.CurItem.Y1;
          break;
        case GuiLib_RADIOBUTTON_BITMAP:
          sgl.CurItem.X2 = sgl.CurItem.X1;
          sgl.CurItem.Y2 = sgl.CurItem.Y1;
          break;
      }
    }
#endif
#ifdef GuiConst_ITEM_BUTTON_INUSE
    else if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_BUTTON)
    {
      sgl.CurItem.X2 = sgl.CurItem.X1;
      sgl.CurItem.Y2 = sgl.CurItem.Y1;
    }
#endif

    if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CIRCLE +
                             GuiLib_ITEMBIT_ELLIPSE)) ||
        (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                             GuiLib_ITEMBIT_ROUNDEDBLOCK +
                             GuiLib_ITEMBIT_QUARTERCIRCLE +
                             GuiLib_ITEMBIT_QUARTERELLIPSE +
                             GuiLib_ITEMBIT_BUTTON +
                             GuiLib_ITEMBIT_PANEL)))
    {
      if (sgl.CommonByte6 & 0x40)
      {
        XVarPtr = (void*)ReadWord(GuiStruct_VarPtrList[sgl.R1VarIdx]);
        sgl.R1VarType = ReadByte(GuiStruct_VarTypeList[sgl.R1VarIdx]);
      }
      else
        XVarPtr = 0;
      if (XVarPtr != 0)
        sgl.ItemR1 += ReadVar(XVarPtr, sgl.R1VarType);
      if (sgl.R1MemoryRead > 0)
        sgl.ItemR1 += sgl.Memory.X[sgl.R1MemoryRead - 1];
      if (sgl.R1Mode == GuiLib_COOR_REL)
        sgl.ItemR1 += sgl.CurItem.X1;
      else if (sgl.R1Mode == GuiLib_COOR_REL_1)
        sgl.ItemR1 += sgl.CurItem.RX1;
      else if (sgl.R1Mode == GuiLib_COOR_REL_2)
        sgl.ItemR1 += sgl.CurItem.RX2;
      if (sgl.R1MemoryWrite > 0)
        sgl.Memory.X[sgl.R1MemoryWrite - 1] = sgl.ItemR1;
      sgl.CurItem.R1 = sgl.ItemR1;
    }

    if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_ELLIPSE)) ||
        (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_QUARTERELLIPSE)))
    {
      if (sgl.CommonByte6 & 0x80)
      {
        YVarPtr = (void*)ReadWord(GuiStruct_VarPtrList[sgl.R2VarIdx]);
        sgl.R2VarType = ReadByte(GuiStruct_VarTypeList[sgl.R2VarIdx]);
      }
      else
        YVarPtr = 0;
      if (YVarPtr != 0)
        sgl.ItemR2 += ReadVar(YVarPtr, sgl.R2VarType);
      if (sgl.R2MemoryRead > 0)
        sgl.ItemR2 += sgl.Memory.Y[sgl.R2MemoryRead - 1];
      if (sgl.R2Mode == GuiLib_COOR_REL)
        sgl.ItemR2 += sgl.CurItem.Y1;
      else if (sgl.R2Mode == GuiLib_COOR_REL_1)
        sgl.ItemR2 += sgl.CurItem.RY1;
      else if (sgl.R2Mode == GuiLib_COOR_REL_2)
        sgl.ItemR2 += sgl.CurItem.RY2;
      if (sgl.R2MemoryWrite > 0)
        sgl.Memory.Y[sgl.R2MemoryWrite - 1] = sgl.ItemR2;
      sgl.CurItem.R2 = sgl.ItemR2;
    }

    if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CLEARAREA +
                             GuiLib_ITEMBIT_TEXTBLOCK +
                             GuiLib_ITEMBIT_VARBLOCK +
                             GuiLib_ITEMBIT_LINE +
                             GuiLib_ITEMBIT_FRAME +
                             GuiLib_ITEMBIT_BLOCK +
                             GuiLib_ITEMBIT_BITMAP +
                             GuiLib_ITEMBIT_BACKGROUND +
                             GuiLib_ITEMBIT_ACTIVEAREA +
                             GuiLib_ITEMBIT_CLIPRECT +
                             GuiLib_ITEMBIT_TOUCHAREA +
                             GuiLib_ITEMBIT_GRAPH +
                             GuiLib_ITEMBIT_GRAPHICSLAYER)) ||
        (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                             GuiLib_ITEMBIT_ROUNDEDBLOCK +
                             GuiLib_ITEMBIT_CHECKBOX +
                             GuiLib_ITEMBIT_RADIOBUTTON +
                             GuiLib_ITEMBIT_BUTTON +
                             GuiLib_ITEMBIT_EDITBOX +
                             GuiLib_ITEMBIT_PANEL +
                             GuiLib_ITEMBIT_MEMO +
                             GuiLib_ITEMBIT_LISTBOX +
                             GuiLib_ITEMBIT_COMBOBOX +
                             GuiLib_ITEMBIT_SCROLLAREA +
                             GuiLib_ITEMBIT_PROGRESSBAR)))
    {
      X = sgl.CurItem.X2 - sgl.CurItem.X1;
      if (sgl.CurItem.TextPar[0].Alignment == GuiLib_ALIGN_CENTER)
      {
        sgl.CurItem.X1 -= X / 2;
        sgl.CurItem.X2 -= X / 2;
      }
      else if (sgl.CurItem.TextPar[0].Alignment == GuiLib_ALIGN_RIGHT)
      {
        sgl.CurItem.X1 -= X;
        sgl.CurItem.X2 -= X;
      }
    }
    else if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CIRCLE +
                                  GuiLib_ITEMBIT_ELLIPSE)) ||
             (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_QUARTERCIRCLE +
                                  GuiLib_ITEMBIT_QUARTERELLIPSE)))
    {
      if (sgl.CurItem.TextPar[0].Alignment == GuiLib_ALIGN_CENTER)
        sgl.CurItem.X1 -= sgl.CurItem.R1;
      else if (sgl.CurItem.TextPar[0].Alignment == GuiLib_ALIGN_RIGHT)
        sgl.CurItem.X1 -= 2 * sgl.CurItem.R1;
    }

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_SCROLLBOX)
      memcpy(&sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].ScrollBoxItem, &sgl.CurItem,
         sizeof(GuiLib_ItemRec));
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
    if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_GRAPH)
    {
      memcpy(&sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem, &sgl.CurItem,
         sizeof(GuiLib_ItemRec));
      OrderCoord(&sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem.X1,
                 &sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem.X2);
      OrderCoord(&sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem.Y1,
                 &sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem.Y2);
      sgl.GraphAry[sgl.GlobalGraphIndex].OrigoX =
         sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem.X1 +
         sgl.GraphAry[sgl.GlobalGraphIndex].OriginOffsetX;
      sgl.GraphAry[sgl.GlobalGraphIndex].OrigoY =
         sgl.GraphAry[sgl.GlobalGraphIndex].GraphItem.Y2 -
         sgl.GraphAry[sgl.GlobalGraphIndex].OriginOffsetY;
    }
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    if (sgl.NextScrollLineReading)
      sgl.DisplayWriting = 0;
    else
#endif
      sgl.DisplayWriting = 1;
    DrawItem(ColorInvert);

    sgl.DisplayWriting = 1;

    switch (sgl.CurItem.ItemType)
    {
      case GuiLib_ITEM_TEXT:
      case GuiLib_ITEM_VAR:
        sgl.CurItem.RX1 = sgl.FontWriteX1;
        sgl.CurItem.RY1 = sgl.FontWriteY1;
        sgl.CurItem.RX2 = sgl.FontWriteX2 + 1;
        sgl.CurItem.RY2 = sgl.FontWriteY2;
        break;
      case GuiLib_ITEM_DOT:
        sgl.CurItem.RX1 = sgl.CurItem.X1;
        sgl.CurItem.RY1 = sgl.CurItem.Y1;
        sgl.CurItem.RX2 = sgl.CurItem.X1 + 1;
        sgl.CurItem.RY2 = sgl.CurItem.Y1;
        break;
      case GuiLib_ITEM_CIRCLE:
      case GuiLib_ITEM_QUARTERCIRCLE:
        sgl.CurItem.RX1 = sgl.CurItem.X1;
        sgl.CurItem.RY1 = sgl.CurItem.Y1 - sgl.CurItem.R1;
        sgl.CurItem.RX2 = sgl.CurItem.X1 + (2 * sgl.CurItem.R1) + 1;
        sgl.CurItem.RY2 = sgl.CurItem.Y1 + sgl.CurItem.R1;
        break;
      case GuiLib_ITEM_ELLIPSE:
      case GuiLib_ITEM_QUARTERELLIPSE:
        sgl.CurItem.RX1 = sgl.CurItem.X1;
        sgl.CurItem.RY1 = sgl.CurItem.Y1 - sgl.CurItem.R2;
        sgl.CurItem.RX2 = sgl.CurItem.X1 + (2 * sgl.CurItem.R1) + 1;
        sgl.CurItem.RY2 = sgl.CurItem.Y1 + sgl.CurItem.R2;
        break;
      case GuiLib_ITEM_CLEARAREA:
      case GuiLib_ITEM_LINE:
      case GuiLib_ITEM_FRAME:
      case GuiLib_ITEM_ROUNDEDFRAME:
      case GuiLib_ITEM_BLOCK:
      case GuiLib_ITEM_ROUNDEDBLOCK:
      case GuiLib_ITEM_BITMAP:
      case GuiLib_ITEM_BACKGROUND:
      case GuiLib_ITEM_ACTIVEAREA:
      case GuiLib_ITEM_CLIPRECT:
      case GuiLib_ITEM_TEXTBLOCK:
      case GuiLib_ITEM_VARBLOCK:
      case GuiLib_ITEM_CHECKBOX:
      case GuiLib_ITEM_BUTTON:
      case GuiLib_ITEM_EDITBOX:
      case GuiLib_ITEM_PANEL:
      case GuiLib_ITEM_MEMO:
      case GuiLib_ITEM_LISTBOX:
      case GuiLib_ITEM_COMBOBOX:
      case GuiLib_ITEM_SCROLLAREA:
      case GuiLib_ITEM_PROGRESSBAR:
      case GuiLib_ITEM_TOUCHAREA:
      case GuiLib_ITEM_SCROLLBOX:
      case GuiLib_ITEM_GRAPH:
      case GuiLib_ITEM_GRAPHICSLAYER:
        sgl.CurItem.RX1 = sgl.CurItem.X1;
        sgl.CurItem.RY1 = sgl.CurItem.Y1;
        sgl.CurItem.RX2 = sgl.CurItem.X2 + 1;
        sgl.CurItem.RY2 = sgl.CurItem.Y2;
        break;
      case GuiLib_ITEM_POSCALLBACK:
        sgl.CurItem.RX1 = sgl.CurItem.X1;
        sgl.CurItem.RY1 = sgl.CurItem.Y1;
        sgl.CurItem.RX2 = sgl.CurItem.X1;
        sgl.CurItem.RY2 = sgl.CurItem.Y1;
        break;
#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
      case GuiLib_ITEM_RADIOBUTTON:
        sgl.CurItem.RX1 = sgl.CurItem.X1;
        sgl.CurItem.RY1 = sgl.CurItem.Y1;
        sgl.CurItem.RX2 = sgl.CurItem.X2 + 1;
        sgl.CurItem.RY2 = sgl.CurItem.Y2 +
                         (sgl.CurItem.CompPars.CompRadioButton.Count - 1) *
                          sgl.CurItem.CompPars.CompRadioButton.InterDistance;
        break;
#endif
    }

    sgl.AutoRedrawLatest = RemAutoRedrawLatest;
  }
}

//------------------------------------------------------------------------------
void GuiLib_ShowScreen(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16S CursorFieldToShow,
   GuiConst_INT8U ResetAutoRedraw)
{
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  GuiConst_INT16S M;
#endif
  GuiConst_INT16S N;
  GuiLib_StructPtr StructureToCall;

  GuiDisplay_Lock();

  GuiLib_CurStructureNdx = StructureNdx;
  StructureToCall =
#ifdef GuiConst_REMOTE_STRUCT_DATA
     GetRemoteStructData(StructureNdx);
#else
     (GuiLib_StructPtr)ReadWord(GuiStruct_StructPtrList[StructureNdx]);
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
  sgl.CursorFieldFound = -1;
  sgl.CursorActiveFieldFound = 0;
  GuiLib_ActiveCursorFieldNo = CursorFieldToShow;
  sgl.CursorInUse = (GuiLib_ActiveCursorFieldNo >= 0);
#else
  gl.Dummy1_16S = CursorFieldToShow;   // To avoid compiler warning
#endif

  if ((GuiLib_StructPtr)StructureToCall != 0)
  {
    sgl.CurItem.X1 = 0;
    sgl.CurItem.Y1 = 0;
    sgl.CurItem.X2 = 0;
    sgl.CurItem.Y2 = 0;
    sgl.CurItem.R1 = 0;
    sgl.CurItem.R2 = 0;

    sgl.CurItem.RX = 0;
    sgl.CurItem.RY = 0;
    sgl.CurItem.RX1 = 0;
    sgl.CurItem.RY1 = 0;
    sgl.CurItem.RX2 = 0;
    sgl.CurItem.RY2 = 0;

    for (N = 0; N < 2 * GuiLib_MEMORY_CNT; N++)
    {
      sgl.Memory.X[N] = 0;
      sgl.Memory.Y[N] = 0;
    }
    for (N = 0; N < GuiLib_MEMORY_CNT; N++)
      sgl.Memory.C[N] = 0;
    sgl.ThicknessMemory = 1;

    sgl.CurItem.ForeColor = GuiConst_PIXEL_ON;
    sgl.CurItem.BackColor = GuiConst_PIXEL_OFF;
    sgl.CurItem.BarForeColor = GuiConst_PIXEL_OFF;
    sgl.CurItem.BarBackColor = GuiConst_PIXEL_ON;

    for (N = 0; N < 3; N++)
    {
      sgl.CurItem.TextPar[N].BitFlags = GuiLib_BITFLAG_TRANSPARENT;
      sgl.CurItem.TextPar[N].Alignment = GuiLib_ALIGN_LEFT;
      sgl.CurItem.TextPar[N].BackBoxSizeX = 0;
      sgl.CurItem.TextPar[N].FontIndex = 0;
      sgl.CurItem.TextPar[N].Ps = GuiLib_PS_ON;
    }

    sgl.CurItem.FormatFieldWidth = 10;
    sgl.CurItem.FormatDecimals = 0;
    sgl.CurItem.FormatAlignment = GuiLib_FORMAT_ALIGNMENT_RIGHT;
    sgl.CurItem.FormatFormat = GuiLib_FORMAT_DEC;

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
    for (N = 0; N < GuiConst_BLINK_FIELDS_MAX; N++)
    {
      sgl.BlinkTextItems[N].InUse = 0;
      sgl.BlinkTextItems[N].Active = 0;
    }
#endif
#endif

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
    sgl.TouchAreaCnt = 0;
#endif

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    for (M = 0; M < GuiConst_SCROLLITEM_BOXES_MAX; M++)
    {
      sgl.ScrollBoxesAry[M].InUse = GuiLib_SCROLL_STRUCTURE_UNDEF;
      for (N = 0; N < GuiConst_SCROLLITEM_MARKERS_MAX; N++)
      {
        sgl.ScrollBoxesAry[M].MarkerColor[N] = GuiConst_PIXEL_ON;
        sgl.ScrollBoxesAry[M].MarkerColorTransparent[N] = 1;
      }
    }
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
    for (N = 0; N < GuiConst_GRAPH_MAX; N++)
      sgl.GraphAry[N].InUse = GuiLib_GRAPH_STRUCTURE_UNDEF;
#endif

    sgl.BaseLayerDrawing = 1;
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
    GraphicsLayer_Pop(GuiLib_GRAPHICS_LAYER_BASE);
    for (N = 0; N < GuiConst_GRAPHICS_LAYER_MAX; N++)
    {
      sgl.GraphicsLayerList[N].InUse = GuiLib_GRAPHICS_LAYER_UNDEF;
      sgl.GraphicsFilterList[N].InUse = GuiLib_GRAPHICS_FILTER_UNDEF;
    }
#endif

#ifdef GuiConst_BITMAP_SUPPORT_ON
    sgl.GlobalBackgrBitmapIndex = 0;
    for (N = 0; N < GuiConst_MAX_BACKGROUND_BITMAPS; N++)
      sgl.BackgrBitmapAry[N].InUse = 0;
#endif

    if (ResetAutoRedraw)
      AutoRedraw_Destroy();
#ifdef GuiConst_CURSOR_SUPPORT_ON
    else
      AutoRedraw_ResetCursor();
#endif

    sgl.DrawingLevel = 0;
    sgl.TopLevelStructure = 0;

#ifdef GuiConst_TEXTBOX_FIELDS_ON
    sgl.CurItem.CompPars.CompTextBox.ScrollIndex = 0;
    sgl.CurItem.CompPars.CompTextBox.ScrollPos = 0;
    for (N = 0; N < GuiConst_TEXTBOX_FIELDS_MAX; N++)
      sgl.TextBoxScrollPositions[N].index = -1;
#endif

    sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
    sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
    sgl.CurItem.ClipRectX1 = sgl.DisplayActiveAreaX1;
    sgl.CurItem.ClipRectY1 = sgl.DisplayActiveAreaY1;
    sgl.CurItem.ClipRectX2 = sgl.DisplayActiveAreaX2;
    sgl.CurItem.ClipRectY2 = sgl.DisplayActiveAreaY2;
    sgl.ActiveAreaX1 = sgl.DisplayActiveAreaX1;
    sgl.ActiveAreaY1 = sgl.DisplayActiveAreaY1;
    sgl.ActiveAreaX2 = sgl.DisplayActiveAreaX2;
    sgl.ActiveAreaY2 = sgl.DisplayActiveAreaY2;
    if (sgl.DisplayWriting)
    {
      if ((sgl.DisplayActiveAreaX1 != 0) || (sgl.DisplayActiveAreaY1 != 0) ||
          (sgl.DisplayActiveAreaX2 != GuiConst_DISPLAY_WIDTH - 1) ||
          (sgl.DisplayActiveAreaY2 != GuiConst_DISPLAY_HEIGHT - 1))
        GuiLib_SetClipping(sgl.DisplayActiveAreaX1, sgl.DisplayActiveAreaY1,
                           sgl.DisplayActiveAreaX2, sgl.DisplayActiveAreaY2);
      else
        GuiLib_ResetClipping();
    }
#endif
#ifdef GuiConst_CURSOR_SUPPORT_ON
    sgl.CurItem.CursorFieldLevel = 0;
#endif
    sgl.DisplayLevel = 0;
    sgl.InitialDrawing = 1;
    sgl.SwapColors = 0;
    DrawStructure((GuiLib_StructPtr) StructureToCall,
                   GuiLib_COL_INVERT_IF_CURSOR);
    ResetLayerBufPtr();
    sgl.InitialDrawing = 0;
    sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
    sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
    if (sgl.DisplayWriting)
      GuiLib_ResetClipping();
#endif
  }

#ifdef GuiConst_CURSOR_SUPPORT_ON
  if (sgl.CursorFieldFound == -1)
  {
    sgl.CursorInUse = 0;
    GuiLib_ActiveCursorFieldNo = -1;
  }
  else if (sgl.CursorInUse)
  {
    if (sgl.CursorActiveFieldFound == 0)
    {
      GuiLib_ActiveCursorFieldNo = sgl.CursorFieldFound;

      DrawCursorItem(1);
    }
  }
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
  sgl.BlinkBoxInverted = 0;
#endif

  GuiDisplay_Unlock();
}

//------------------------------------------------------------------------------
GuiConst_INT16S CheckLanguageIndex(
   GuiConst_INT16S LanguageIndex)
{
  if ((LanguageIndex < 0) || (LanguageIndex > GuiConst_LANGUAGE_CNT - 1))
    LanguageIndex = 0;

#ifdef GuiConst_LANGUAGE_SOME_ACTIVE
  if (!ReadByte(GuiFont_LanguageActive[LanguageIndex]))
    LanguageIndex = GuiConst_LANGUAGE_FIRST;
#endif

  return(LanguageIndex);
}

//------------------------------------------------------------------------------
GuiConst_TEXT *GuiLib_GetTextLanguagePtr(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo,
   GuiConst_INT16S LanguageIndex)
{
  GuiConst_INT16U I;
  GuiConst_INT16S ItemNdx;
  GuiConst_INT8U ItemCnt;
  GuiLib_StructPtr StructureToCall;
#ifdef GuiConst_REMOTE_STRUCT_DATA
  GuiConst_INT8U ItemSizeBuf[2];
  GuiConst_INT16U ItemSize;
#endif

  StructureToCall =
#ifdef GuiConst_REMOTE_STRUCT_DATA
     GetRemoteStructData(StructureNdx);
#else
     (GuiLib_StructPtr)ReadWord(GuiStruct_StructPtrList[StructureNdx]);
#endif

  if (StructureToCall != 0)
  {
    LanguageIndex = CheckLanguageIndex(LanguageIndex);

    sgl.ItemDataPtr = (GuiConst_INT8U*)StructureToCall;
    ItemCnt = GetItemByte(&sgl.ItemDataPtr);

    I = 0;
    for (ItemNdx = 0; ItemNdx < ItemCnt; ItemNdx++)
    {
#ifdef GuiConst_REMOTE_STRUCT_DATA
      GuiLib_RemoteDataReadBlock(
         sgl.RemoteStructOffset,
         2,
         (GuiConst_INT8U*)&ItemSizeBuf);
      ItemSize = (256 * (GuiConst_INT16U)ItemSizeBuf[1]) + ItemSizeBuf[0];
      sgl.RemoteStructOffset += 2;
      GuiLib_RemoteDataReadBlock(
         sgl.RemoteStructOffset,
         ItemSize,
         sgl.GuiLib_RemoteItemBuffer);
      sgl.RemoteStructOffset += ItemSize;
      sgl.ItemDataPtr = &sgl.GuiLib_RemoteItemBuffer[0];
#endif

      ReadItem(LanguageIndex);

      if ((sgl.CurItem.ItemType == GuiLib_ITEM_TEXT) ||
          (sgl.CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK))
      {
        if (I == TextNo)
        {
#ifdef GuiConst_CHARMODE_ANSI
          return (GuiConst_TEXT *) sgl.CurItem.TextPtr[0];
#else
          #ifdef GuiConst_ICC_COMPILER
          ExtractUnicodeString((GuiConst_INT8U *)sgl.CurItem.TextPtr[0],
                               sgl.CurItem.TextLength[0]);
          #else
          #ifdef GuiConst_CODEVISION_COMPILER
          ExtractUnicodeString((GuiConst_INT8U *)sgl.CurItem.TextPtr[0],
                               sgl.CurItem.TextLength[0]);
          #else
          ExtractUnicodeString((GuiConst_INT8U PrefixRom *)sgl.CurItem.TextPtr[0],
                               sgl.CurItem.TextLength[0]);
          #endif
          #endif

          return (sgl.UnicodeTextBuf);
#endif
        }
        I++;
      }
    }
  }

  return (0);
}

//------------------------------------------------------------------------------
GuiConst_TEXT *GuiLib_GetTextPtr(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo)
{
  return (GuiLib_GetTextLanguagePtr(StructureNdx, TextNo, GuiLib_LanguageIndex));
}

//------------------------------------------------------------------------------
GuiConst_INT16U GuiLib_GetTextWidth(
   GuiConst_TEXT *String,
   GuiLib_FontRecConstPtr Font,
   GuiConst_INT8U PsWriting)
{
  GuiLib_ItemRec TextData;
  GuiConst_INT16U CharCnt;

  if ((String[0] != 0) && (Font != 0))
  {
    TextData.TextPar[0].Ps = PsWriting;
    sgl.CurItem.TextPar[0].Ps = TextData.TextPar[0].Ps;
#ifdef GuiConst_CHARMODE_ANSI
    CharCnt = strlen(String);
#else
    CharCnt = GuiLib_UnicodeStrLen(String);
#endif
    sgl.CurFont = (GuiLib_FontRecPtr)Font;
#ifdef GuiConst_DISP_VAR_NOW
    displayVarNow = 1;
#endif

#ifdef GuiConst_CODEVISION_COMPILER
    PrepareText((GuiConst_TEXT *)String, CharCnt, 0);
#else
    PrepareText(String, CharCnt, 0);
#endif
#ifdef GuiConst_DISP_VAR_NOW
    displayVarNow = 0;
#endif

    return (TextPixelLength(TextData.TextPar[0].Ps, CharCnt, 0));
  }

  return (0);
}

//---------------------------------------------------------------
static GuiConst_TEXT GetCharCode(
   GuiConst_TEXT PrefixRom * CharPtr,
   GuiConst_INT16U CharCnt,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode)
{
  GuiConst_INT16U P;
  GuiConst_TEXT CharCode, PreviousCharCode;

  if (CharCnt > GuiConst_MAX_TEXT_LEN)
    CharCnt = GuiConst_MAX_TEXT_LEN;

  if (CharNo > CharCnt)
    return 0;

  if (CharCnt > GuiConst_MAX_TEXT_LEN)
    CharCnt = GuiConst_MAX_TEXT_LEN;

  if (OmitCtrlCode)
  {
    for (P = 0; P < CharNo + 1; P++)
    {
#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_AVRGCC_COMPILER
      CharCode = (unsigned GuiConst_CHAR) ReadBytePtr(CharPtr);
  #else
  #ifdef GuiConst_ICC_COMPILER
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
      CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
      CharCode = (unsigned GuiConst_TEXT) *CharPtr;
  #endif
  #endif
  #endif
#else
      CharCode = *((GuiConst_INT16U*)CharPtr);
#endif

      if (P)
      {
        if ((CharCode == GuiLib_LINEFEED) ||
           ((CharCode == ' ') && (PreviousCharCode == ' ')) ||
           ((CharCode == ' ') && (PreviousCharCode == '-')))
          CharNo++;
      }
      PreviousCharCode = CharCode;

      CharPtr++;
    }
  }
  else
  {
    CharPtr += CharNo;

#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_AVRGCC_COMPILER
    CharCode = (unsigned GuiConst_CHAR) ReadBytePtr(CharPtr);
  #else
  #ifdef GuiConst_ICC_COMPILER
    CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
  #ifdef GuiConst_CODEVISION_COMPILER
    CharCode = *((GuiConst_INT8U PrefixRom *)CharPtr);
  #else
    CharCode = (unsigned GuiConst_TEXT) *CharPtr;
  #endif
  #endif
  #endif
#else
    CharCode = *((GuiConst_INT16U*)CharPtr);
#endif
  }

  return (GuiConst_TEXT)CharCode;
}

//------------------------------------------------------------------------------
GuiConst_TEXT GuiLib_GetCharCode(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode)
{
  GuiConst_TEXT PrefixRom * CharPtr;

  CharPtr = GuiLib_GetTextPtr(StructureNdx, TextNo);

#ifdef GuiConst_CHARMODE_ANSI
  return GetCharCode(CharPtr,
                     strlen(CharPtr),
                     CharNo,
                     OmitCtrlCode);
#else
  return GetCharCode(CharPtr,
                     GuiLib_UnicodeStrLen(CharPtr),
                     CharNo,
                     OmitCtrlCode);
#endif
}

//------------------------------------------------------------------------------
#ifdef GuiConst_BLINK_SUPPORT_ON
GuiConst_TEXT GuiLib_GetBlinkingCharCode(
   GuiConst_INT16U BlinkFieldNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode)
{
#ifndef GuiConst_BLINK_FIELDS_OFF
  GuiConst_INT16U StrLen;

  if (BlinkFieldNo >= GuiConst_BLINK_FIELDS_MAX)
    return 0;

  if (sgl.BlinkTextItems[BlinkFieldNo].InUse)
  {
    #ifdef GuiConst_CHARMODE_ANSI
    StrLen = strlen((GuiConst_TEXT*)sgl.BlinkTextItems[BlinkFieldNo].TextPtr);
    #else
    #ifdef GuiConst_CODEVISION_COMPILER
    StrLen = GuiLib_UnicodeStrLen(
       (GuiConst_TEXT*)sgl.BlinkTextItems[BlinkFieldNo].TextPtr);
    #else
    StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)sgl.BlinkTextItems[BlinkFieldNo].TextPtr);
    #endif
    #endif

    return GetCharCode((GuiConst_TEXT*)sgl.BlinkTextItems[BlinkFieldNo].TextPtr,
                       StrLen,
                       CharNo,
                       OmitCtrlCode);
  }
  else
#endif
    return 0;
}
#endif

//------------------------------------------------------------------------------
void GuiLib_SetLanguage(
   GuiConst_INT16S NewLanguage)
{
  GuiLib_LanguageIndex = CheckLanguageIndex(NewLanguage);
}

//------------------------------------------------------------------------------
static void InvertBox(void)
{
  if (sgl.DisplayWriting)
    GuiLib_InvertBox(sgl.InvertBoxX1, sgl.InvertBoxY1, sgl.InvertBoxX2, sgl.InvertBoxY2);
  sgl.InvertBoxOn = !sgl.InvertBoxOn;
}

//------------------------------------------------------------------------------
void GuiLib_InvertBoxStart(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiLib_InvertBoxStop();
  sgl.InvertBoxX1 = X1;
  sgl.InvertBoxY1 = Y1;
  sgl.InvertBoxX2 = X2;
  sgl.InvertBoxY2 = Y2;
  InvertBox();
}

//------------------------------------------------------------------------------
void GuiLib_InvertBoxStop(void)
{
  if (sgl.InvertBoxOn)
    InvertBox();
}

//------------------------------------------------------------------------------
void GuiLib_Refresh(void)
{
  GuiConst_INT16S N, L;
  GuiConst_INT8U RedrawBottomLevel;
  ItemMemory     RemMemory;
  GuiConst_INT8U ColorInvert;
#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  GuiConst_INT16U VarLen;
  GuiConst_INT16S VarChange;
#endif

  GuiDisplay_Lock();

  sgl.RefreshClock++;

  RedrawBottomLevel = 0;

  N = AutoRedraw_Reset();

  memcpy(&RemMemory, &sgl.Memory, sizeof(ItemMemory));

  while (N != -1)
  {
#ifdef GuiConst_CURSOR_SUPPORT_ON
    if (!AutoRedraw_IsOnlyCursor(N))
#endif
    {
      if (AutoRedraw_VarChanged(N) != 0)
      {
        memcpy(&sgl.CurItem, AutoRedraw_GetItem(N), sizeof(GuiLib_ItemRec));
        memcpy(&sgl.Memory, AutoRedraw_GetItemMemory(N), sizeof(ItemMemory));

        AutoRedraw_UpdateVar(N);

        ColorInvert = GuiLib_COL_INVERT_IF_CURSOR;

#ifdef GuiConst_CURSOR_SUPPORT_ON
        if (AutoRedraw_CheckCursorInheritance(N) == 0)
          ColorInvert = GuiLib_COL_INVERT_ON;
#endif

        L = AutoRedraw_GetLevel(N);

        if (AutoRedraw_ItemIsStruct(N))
        {
          N = AutoRedraw_DeleteStruct(N);
          sgl.AutoRedrawUpdate = GuiLib_TRUE;
          if (sgl.AutoRedrawInsertPoint >= 0)
            N = AutoRedraw_GetNext(N);
        }
        else
        {
          sgl.AutoRedrawUpdate = GuiLib_FALSE;
          N = AutoRedraw_GetNext(N);
        }

#ifdef GuiConst_BITMAP_SUPPORT_ON
        UpdateBackgroundBitmap();
#endif

        sgl.DisplayLevel = 0;
        sgl.SwapColors = 0;

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
        if ((sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_BLINKTEXTFIELD) &&
            (sgl.CurItem.BlinkFieldNo < GuiConst_BLINK_FIELDS_MAX) &&
             sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].InUse &&
             sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].Active &&
             sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].BlinkBoxInverted)
           GuiLib_InvertBox(sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].BlinkBoxX1,
                            sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].BlinkBoxY1,
                            sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].BlinkBoxX2,
                            sgl.BlinkTextItems[sgl.CurItem.BlinkFieldNo].BlinkBoxY2);
#endif
#endif

        DrawItem(ColorInvert);

        if (L == 0)
          RedrawBottomLevel = 1;

        sgl.AutoRedrawUpdate = GuiLib_FALSE;
      }
      else
        N = AutoRedraw_GetNext(N);
    }
#ifdef GuiConst_CURSOR_SUPPORT_ON
    else
      N = AutoRedraw_GetNext(N);
#endif
  }

  memcpy(&sgl.Memory, &RemMemory, sizeof(ItemMemory));


  if ((sgl.DrawingLevel > 0) && RedrawBottomLevel && (sgl.TopLevelStructure != 0))
  {
    sgl.DisplayLevel = 0;
    sgl.SwapColors = 0;
    DrawStructure(sgl.TopLevelStructure, GuiLib_COL_INVERT_IF_CURSOR);
    ResetLayerBufPtr();
  }

#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
  for (N = 0; N < GuiConst_BLINK_FIELDS_MAX; N++)
    if (sgl.BlinkTextItems[N].InUse &&
        sgl.BlinkTextItems[N].Active)
    {
      if (sgl.BlinkTextItems[N].BlinkBoxInverted !=
          sgl.BlinkTextItems[N].BlinkBoxLast)
      {
         GuiLib_InvertBox(sgl.BlinkTextItems[N].BlinkBoxX1,
                          sgl.BlinkTextItems[N].BlinkBoxY1,
                          sgl.BlinkTextItems[N].BlinkBoxX2,
                          sgl.BlinkTextItems[N].BlinkBoxY2);
         sgl.BlinkTextItems[N].BlinkBoxInverted =
            sgl.BlinkTextItems[N].BlinkBoxLast;
      }
      if (sgl.BlinkTextItems[N].BlinkBoxState < 255)
      {
        sgl.BlinkTextItems[N].BlinkBoxState--;
        if (sgl.BlinkTextItems[N].BlinkBoxState == 0)
        {
          sgl.BlinkTextItems[N].BlinkBoxState = sgl.BlinkTextItems[N].BlinkBoxRate;
          if (sgl.DisplayWriting)
          {
            GuiLib_InvertBox(sgl.BlinkTextItems[N].BlinkBoxX1,
                             sgl.BlinkTextItems[N].BlinkBoxY1,
                             sgl.BlinkTextItems[N].BlinkBoxX2,
                             sgl.BlinkTextItems[N].BlinkBoxY2);
            sgl.BlinkTextItems[N].BlinkBoxInverted =
               !sgl.BlinkTextItems[N].BlinkBoxInverted;
            sgl.BlinkTextItems[N].BlinkBoxLast =
               sgl.BlinkTextItems[N].BlinkBoxInverted;
          }
        }
      }
    }
  if (sgl.BlinkBoxRate)
  {
    if (sgl.BlinkBoxState < 255)
      sgl.BlinkBoxState--;
    if (sgl.BlinkBoxState == 0)
    {
      sgl.BlinkBoxState = sgl.BlinkBoxRate;
      BlinkBox();
    }
  }
#endif
#endif

  GuiDisplay_Unlock();

  GuiDisplay_Refresh();
}

//------------------------------------------------------------------------------
void GuiLib_DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   GuiConst_TEXT Character,
   GuiConst_INTCOLOR Color)
{
  GuiLib_FontRecPtr Font;
#ifndef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U PrefixRom *CharPtr;
#endif
  GuiConst_INT16S X1,Y1,X2,Y2;
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT32U CharNdx;
#else
#ifdef GuiConst_CHARMODE_UNICODE
  GuiConst_INT32U CharNdx;
#endif
#endif

  Font = (GuiLib_FontRecPtr) ReadWord(GuiFont_FontList[FontNo]);

  Y -= ReadByte(Font->BaseLine);

#ifdef GuiConst_CHARMODE_ANSI
  #ifdef GuiConst_REMOTE_FONT_DATA
  if ((Character < Font->FirstChar) || (Character > Font->LastChar))
    CharNdx =
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[Font->IllegalCharNdx];
  else
    CharNdx =
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[Font->FirstCharNdx +
       (GuiConst_INT16U)Character - (GuiConst_INT16U)Font->FirstChar];
  DrawChar(X, Y, Font, CharNdx, Color);
  #else
  if ((Character < ReadByte(Font->FirstChar)) ||
     (Character > ReadByte(Font->LastChar)))
    CharPtr = (GuiConst_INT8U PrefixRom *)ReadWord(
       GuiFont_ChPtrList[ReadWord(Font->IllegalCharNdx)]);
  else
    CharPtr = (GuiConst_INT8U PrefixRom *)ReadWord(
       GuiFont_ChPtrList[ReadWord(Font->FirstCharNdx) +
       (GuiConst_INT16U)Character -
       (GuiConst_INT16U)ReadByte(Font->FirstChar)]);
  DrawChar(X, Y, Font, CharPtr, Color);
  #endif
#else
  CharNdx = GetCharNdx(Font, Character);
  #ifdef GuiConst_REMOTE_FONT_DATA
  DrawChar(X, Y, Font, CharNdx, Color);
  #else
  CharPtr = (GuiConst_INT8U PrefixRom *)ReadWord(GuiFont_ChPtrList[CharNdx]);
  DrawChar(X, Y, Font, CharPtr, Color);
  #endif
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  X1 = X + sgl.GuiLib_RemoteFontBuffer[GuiLib_CHR_XLEFT_OFS];
  X2 = X1 + sgl.GuiLib_RemoteFontBuffer[GuiLib_CHR_XWIDTH_OFS] - 1;
  Y1 = Y + sgl.GuiLib_RemoteFontBuffer[GuiLib_CHR_YTOP_OFS];
  Y2 = Y1 + sgl.GuiLib_RemoteFontBuffer[GuiLib_CHR_YHEIGHT_OFS] - 1;
#else
  X1 = X + ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
  X2 = X1 + ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) - 1;
  Y1 = Y + ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y2 = Y1 + ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) - 1;
#endif

  GuiLib_MarkDisplayBoxRepaint(X1,Y1,X2,Y2);
}

//------------------------------------------------------------------------------
void GuiLib_DrawStr(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   GuiConst_TEXT *String,
   GuiConst_INT8U Alignment,
   GuiConst_INT8U PsWriting,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U Underlining,
   GuiConst_INT16S BackBoxSizeX,
   GuiConst_INT16S BackBoxSizeY1,
   GuiConst_INT16S BackBoxSizeY2,
   GuiConst_INT8U BackBorderPixels,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor)
{
  if (FontNo >= GuiFont_FontCnt)
    return;
  if (*String == 0)
    return;

  SetCurFont(FontNo);

#ifdef GuiConst_AVRGCC_COMPILER
  #ifdef GuiConst_CHARMODE_ANSI
  sgl.CurItem.TextPtr[0] = (GuiConst_INT8U *)String;
  #else
  sgl.CurItem.TextPtr[0] = (GuiConst_INT16U *)String;
  #endif
#else
#ifdef GuiConst_ICC_COMPILER
  #ifdef GuiConst_CHARMODE_ANSI
  sgl.CurItem.TextPtr[0] = (GuiConst_INT8U *)String;
  #else
  sgl.CurItem.TextPtr[0] = (GuiConst_INT16U *)String;
  #endif
#else
#ifdef GuiConst_CODEVISION_COMPILER
  #ifdef GuiConst_CHARMODE_ANSI
  sgl.CurItem.TextPtr[0] = (GuiConst_INT8U *)String;
  #else
  sgl.CurItem.TextPtr[0] = (GuiConst_INT16U *)String;
  #endif
#else
  sgl.CurItem.TextPtr[0] = String;
#endif
#endif
#endif

  sgl.CurItem.TextLength[0] = 0;
  while (*String != 0)
  {
    sgl.CurItem.TextLength[0]++;
    String++;
  }
  sgl.CurItem.X1 = X;
  sgl.CurItem.Y1 = Y;
  sgl.CurItem.TextPar[0].FontIndex = FontNo;
  sgl.CurItem.TextPar[0].Alignment = Alignment;
  sgl.CurItem.TextPar[0].Ps = PsWriting;
  sgl.CurItem.TextPar[0].BitFlags = 0;
  if (Underlining)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_UNDERLINE;
  sgl.CurItem.TextPar[0].BackBoxSizeX = BackBoxSizeX;
  sgl.CurItem.TextPar[0].BackBoxSizeY1 = BackBoxSizeY1;
  sgl.CurItem.TextPar[0].BackBoxSizeY2 = BackBoxSizeY2;
  sgl.CurItem.TextPar[0].BackBorderPixels = BackBorderPixels;
  if (sgl.CurItem.TextPar[0].BackBoxSizeX > 0)
    DrawBackBox(BackColor, Transparent, 0);

#ifdef GuiConst_DISP_VAR_NOW
  displayVarNow = 1;
#endif

  DrawText(sgl.CurItem.TextPtr[0],
           sgl.CurItem.TextLength[0],
           0,
           ForeColor,
           BackColor,
           Transparent);

#ifdef GuiConst_DISP_VAR_NOW
  displayVarNow = 0;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_DrawVar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   void *VarPtr,
   GuiConst_INT8U VarType,
   GuiConst_INT8U FormatterFormat,
   GuiConst_INT8U FormatterFieldWidth,
   GuiConst_INT8U FormatterAlignment,
   GuiConst_INT8U FormatterDecimals,
   GuiConst_INT8U FormatterShowSign,
   GuiConst_INT8U FormatterZeroPadding,
   GuiConst_INT8U FormatterTrailingZeros,
   GuiConst_INT8U FormatterThousandsSeparator,
   GuiConst_INT8U Alignment,
   GuiConst_INT8U PsWriting,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U Underlining,
   GuiConst_INT16S BackBoxSizeX,
   GuiConst_INT16S BackBoxSizeY1,
   GuiConst_INT16S BackBoxSizeY2,
   GuiConst_INT8U BackBorderPixels,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor)
{
  GuiConst_INT32S VarValue;
  GuiConst_INT16U StrLen;
#ifndef GuiConst_CHARMODE_ANSI
  GuiConst_INT16U P;
#endif
  GuiConst_TEXT *CharPtr;

  if (FontNo >= GuiFont_FontCnt)
    return;

  SetCurFont(FontNo);

  sgl.CurItem.VarPtr = VarPtr;
  sgl.CurItem.VarType = VarType;
  sgl.CurItem.FormatFormat = FormatterFormat;
  sgl.CurItem.FormatFieldWidth = FormatterFieldWidth;
  sgl.CurItem.FormatAlignment = FormatterAlignment;
  sgl.CurItem.FormatDecimals = FormatterDecimals;
  sgl.CurItem.TextPar[0].BitFlags = 0;
  if (FormatterShowSign)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATSHOWSIGN;
  if (FormatterZeroPadding)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATZEROPADDING;
  if (FormatterTrailingZeros)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATTRAILINGZEROS;
  if (FormatterThousandsSeparator)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
  sgl.CurItem.X1 = X;
  sgl.CurItem.Y1 = Y;
  sgl.CurItem.TextPar[0].FontIndex = FontNo;
  sgl.CurItem.TextPar[0].Alignment = Alignment;
  sgl.CurItem.TextPar[0].Ps = PsWriting;
  if (Underlining)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_UNDERLINE;
  sgl.CurItem.TextPar[0].BackBoxSizeX = BackBoxSizeX;
  sgl.CurItem.TextPar[0].BackBoxSizeY1 = BackBoxSizeY1;
  sgl.CurItem.TextPar[0].BackBoxSizeY2 = BackBoxSizeY2;
  sgl.CurItem.TextPar[0].BackBorderPixels = BackBorderPixels;
  if (sgl.CurItem.TextPar[0].BackBoxSizeX > 0)
    DrawBackBox(BackColor, Transparent, 0);

#ifdef GuiConst_DISP_VAR_NOW
  displayVarNow = 1;
#endif

  if (sgl.CurItem.VarType == GuiLib_VAR_STRING)
  {
    CharPtr = (GuiConst_TEXT*)sgl.CurItem.VarPtr;
#ifdef GuiConst_CHARMODE_ANSI
    StrLen = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
    StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
    StrLen = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
  }
  else
  {
    VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);
    StrLen = DataNumStr(VarValue, sgl.CurItem.VarType, 0);
#ifdef GuiConst_CHARMODE_ANSI
    CharPtr = (GuiConst_TEXT *) sgl.VarNumTextStr;
#else
    for (P = 0; P <= StrLen; P++)
      sgl.VarNumUnicodeTextStr[P] = sgl.VarNumTextStr[P];
    CharPtr = (GuiConst_TEXT *) sgl.VarNumUnicodeTextStr;
#endif
  }

  DrawText(CharPtr,
           StrLen,
           0,
           ForeColor,
           BackColor,
           Transparent);

#ifdef GuiConst_DISP_VAR_NOW
  displayVarNow = 0;
#endif
}

//------------------------------------------------------------------------------
void GuiLib_TestPattern(void)
{
  GuiLib_HLine(0, 31, 0, GuiConst_PIXEL_ON);
  GuiLib_VLine(0, 1, 31, GuiConst_PIXEL_ON);
  GuiLib_HLine(2, 8, 2, GuiConst_PIXEL_ON);
  GuiLib_HLine(11, 16, 2, GuiConst_PIXEL_ON);
  GuiLib_VLine(4, 4, 10, GuiConst_PIXEL_ON);
  GuiLib_VLine(4, 13, 18, GuiConst_PIXEL_ON);
}

#include "GuiComponents.c"

#ifdef GuiConst_REMOTE_DATA
//------------------------------------------------------------------------------
GuiConst_INT8U GuiLib_RemoteCheck(void)
{
  union
  {
    GuiConst_INT8U Bytes[4];
    GuiConst_INT32U IdInFile;
  } RemoteIdUnion;
  GuiConst_INT8U ok;

  GuiLib_RemoteDataReadBlock(0, 4, RemoteIdUnion.Bytes);
  ok = (RemoteIdUnion.IdInFile == GuiConst_REMOTE_ID);

#ifdef GuiConst_REMOTE_TEXT_DATA
  GuiLib_RemoteTextReadBlock(0, 4, RemoteIdUnion.Bytes);
  ok = ok && (RemoteIdUnion.IdInFile == GuiConst_REMOTE_ID);
#endif

  return (ok);
}
#endif


