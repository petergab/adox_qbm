/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2012 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                               v6.0.4.003                                 */
/*                                                                          */
/* ************************************************************************ */

#ifndef __GUILIBSTRUCT_H_
#define __GUILIBSTRUCT_H_

#include "GuiConst.h"
#include "GuiLib.h"

#define GuiLib_ITEM_TEXT                   0
#define GuiLib_ITEM_DOT                    1
#define GuiLib_ITEM_LINE                   2
#define GuiLib_ITEM_FRAME                  3
#define GuiLib_ITEM_BLOCK                  4
#define GuiLib_ITEM_STRUCTURE              5
#define GuiLib_ITEM_STRUCTARRAY            6
#define GuiLib_ITEM_CLIPRECT               7
#define GuiLib_ITEM_VAR                    8
#define GuiLib_ITEM_FORMATTER              9
#define GuiLib_ITEM_BITMAP                 10
#define GuiLib_ITEM_TEXTBLOCK              11
#define GuiLib_ITEM_TOUCHAREA              12
#define GuiLib_ITEM_VARBLOCK               13
#define GuiLib_ITEM_ACTIVEAREA             14
#define GuiLib_ITEM_SCROLLBOX              15
#define GuiLib_ITEM_CIRCLE                 16
#define GuiLib_ITEM_ELLIPSE                17
#define GuiLib_ITEM_BACKGROUND             18
#define GuiLib_ITEM_CLEARAREA              19
#define GuiLib_ITEM_ADVGRAPH_COORDSYST     20
#define GuiLib_ITEM_ADVGRAPH_PIXEL         21
#define GuiLib_ITEM_ADVGRAPH_LINE          22
#define GuiLib_ITEM_ADVGRAPH_ARC           23
#define GuiLib_ITEM_ADVGRAPH_RECT          24
#define GuiLib_ITEM_ADVGRAPH_ELLIPSE       25
#define GuiLib_ITEM_ADVGRAPH_SEGMENT       26
#define GuiLib_ITEM_ADVGRAPH_TRIANGLE      27
#define GuiLib_ITEM_ADVGRAPH_POLYGON       28
#define GuiLib_ITEM_GRAPH                  29
#define GuiLib_ITEM_GRAPHICSLAYER          30
#define GuiLib_ITEM_GRAPHICSFILTER         31
#define GuiLib_ITEM_ROUNDEDFRAME           32
#define GuiLib_ITEM_ROUNDEDBLOCK           33
#define GuiLib_ITEM_QUARTERCIRCLE          34
#define GuiLib_ITEM_QUARTERELLIPSE         35
#define GuiLib_ITEM_CHECKBOX               36
#define GuiLib_ITEM_RADIOBUTTON            37
#define GuiLib_ITEM_BUTTON                 38
#define GuiLib_ITEM_EDITBOX                39
#define GuiLib_ITEM_PANEL                  40
#define GuiLib_ITEM_MEMO                   41
#define GuiLib_ITEM_LISTBOX                42
#define GuiLib_ITEM_COMBOBOX               43
#define GuiLib_ITEM_SCROLLAREA             44
#define GuiLib_ITEM_PROGRESSBAR            45
#define GuiLib_ITEM_STRUCTCOND             46
#define GuiLib_ITEM_POSCALLBACK            47

#define GuiLib_ITEMBIT_TEXT                0x00000001
#define GuiLib_ITEMBIT_DOT                 0x00000002
#define GuiLib_ITEMBIT_LINE                0x00000004
#define GuiLib_ITEMBIT_FRAME               0x00000008
#define GuiLib_ITEMBIT_BLOCK               0x00000010
#define GuiLib_ITEMBIT_STRUCTURE           0x00000020
#define GuiLib_ITEMBIT_STRUCTARRAY         0x00000040
#define GuiLib_ITEMBIT_CLIPRECT            0x00000080
#define GuiLib_ITEMBIT_VAR                 0x00000100
#define GuiLib_ITEMBIT_FORMATTER           0x00000200
#define GuiLib_ITEMBIT_BITMAP              0x00000400
#define GuiLib_ITEMBIT_TEXTBLOCK           0x00000800
#define GuiLib_ITEMBIT_TOUCHAREA           0x00001000
#define GuiLib_ITEMBIT_VARBLOCK            0x00002000
#define GuiLib_ITEMBIT_ACTIVEAREA          0x00004000
#define GuiLib_ITEMBIT_SCROLLBOX           0x00008000
#define GuiLib_ITEMBIT_CIRCLE              0x00010000
#define GuiLib_ITEMBIT_ELLIPSE             0x00020000
#define GuiLib_ITEMBIT_BACKGROUND          0x00040000
#define GuiLib_ITEMBIT_CLEARAREA           0x00080000
#define GuiLib_ITEMBIT_ADVGRAPH_COORDSYST  0x00100000
#define GuiLib_ITEMBIT_ADVGRAPH_PIXEL      0x00200000
#define GuiLib_ITEMBIT_ADVGRAPH_LINE       0x00400000
#define GuiLib_ITEMBIT_ADVGRAPH_ARC        0x00800000
#define GuiLib_ITEMBIT_ADVGRAPH_RECT       0x01000000
#define GuiLib_ITEMBIT_ADVGRAPH_ELLIPSE    0x02000000
#define GuiLib_ITEMBIT_ADVGRAPH_SEGMENT    0x04000000
#define GuiLib_ITEMBIT_ADVGRAPH_TRIANGLE   0x08000000
#define GuiLib_ITEMBIT_ADVGRAPH_POLYGON    0x10000000
#define GuiLib_ITEMBIT_GRAPH               0x20000000
#define GuiLib_ITEMBIT_GRAPHICSLAYER       0x40000000
#define GuiLib_ITEMBIT_GRAPHICSFILTER      0x80000000
#define GuiLib_ITEMBIT_ROUNDEDFRAME        0x00000001
#define GuiLib_ITEMBIT_ROUNDEDBLOCK        0x00000002
#define GuiLib_ITEMBIT_QUARTERCIRCLE       0x00000004
#define GuiLib_ITEMBIT_QUARTERELLIPSE      0x00000008
#define GuiLib_ITEMBIT_CHECKBOX            0x00000010
#define GuiLib_ITEMBIT_RADIOBUTTON         0x00000020
#define GuiLib_ITEMBIT_BUTTON              0x00000040
#define GuiLib_ITEMBIT_EDITBOX             0x00000080
#define GuiLib_ITEMBIT_PANEL               0x00000100
#define GuiLib_ITEMBIT_MEMO                0x00000200
#define GuiLib_ITEMBIT_LISTBOX             0x00000400
#define GuiLib_ITEMBIT_COMBOBOX            0x00000800
#define GuiLib_ITEMBIT_SCROLLAREA          0x00001000
#define GuiLib_ITEMBIT_PROGRESSBAR         0x00002000
#define GuiLib_ITEMBIT_STRUCTCOND          0x00004000
#define GuiLib_ITEMBIT_POSCALLBACK         0x00008000

#ifdef GuiConst_ITEM_GRAPH_INUSE
  #define GuiLib_GETITEMLONG
#else
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  #define GuiLib_GETITEMLONG
#endif
#endif

#define GuiLib_TRUE                        1
#define GuiLib_FALSE                       0

#define GuiLib_UPDATE_ALWAYS               0
#define GuiLib_UPDATE_ON_CHANGE            1

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
#define GuiLib_AUTOREDRAW_MODE GuiLib_UPDATE_ON_CHANGE
#else
#define GuiLib_AUTOREDRAW_MODE GuiLib_UPDATE_ALWAYS
#endif

#define GuiLib_COOR_ABS                    0
#define GuiLib_COOR_REL                    1
#define GuiLib_COOR_REL_1                  2
#define GuiLib_COOR_REL_2                  3

#define GuiLib_COLOR_NOCHANGE              0
#define GuiLib_COLOR_FORE                  1
#define GuiLib_COLOR_BACK                  2
#define GuiLib_COLOR_OTHER                 3
#define GuiLib_COLOR_INVERT                4
#define GuiLib_COLOR_TRANSP                5
#define GuiLib_COLOR_TABLE                 6
#define GuiLib_COLOR_VAR                   7

#define GuiLib_MARKER_NONE                 0
#define GuiLib_MARKER_ICON                 1
#define GuiLib_MARKER_BITMAP               2
#define GuiLib_MARKER_FIXED_BLOCK          3
#define GuiLib_MARKER_VARIABLE_BLOCK       4

#define GuiLib_MEMORY_MIN                  1
#define GuiLib_MEMORY_MAX                  3
#define GuiLib_MEMORY_CNT                  GuiLib_MEMORY_MAX - GuiLib_MEMORY_MIN + 1

#define GuiLib_COL_INVERT_OFF              0
#define GuiLib_COL_INVERT_ON               1
#define GuiLib_COL_INVERT_IF_CURSOR        2

#define GuiLib_BITFLAG_INUSE               0x00000001
#define GuiLib_BITFLAG_TRANSPARENT         0x00000002
#define GuiLib_BITFLAG_UNDERLINE           0x00000004
#define GuiLib_BITFLAG_PATTERNEDLINE       0x00000004
#define GuiLib_BITFLAG_CIRCLE_IF           0x00000004
#define GuiLib_BITFLAG_FORMATSHOWSIGN      0x00000008
#define GuiLib_BITFLAG_FORMATZEROPADDING   0x00000010
#define GuiLib_BITFLAG_AUTOREDRAWFIELD     0x00000020
#define GuiLib_BITFLAG_NOTINUSE            0x00000040
#define GuiLib_BITFLAG_TRANSLATION         0x00000080
#define GuiLib_BITFLAG_BLINKTEXTFIELD      0x00000100
#define GuiLib_BITFLAG_CLIPPING            0x00000200
#define GuiLib_BITFLAG_ACTIVEAREARELCOORD  0x00000400
#define GuiLib_BITFLAG_REVERSEWRITING      0x00000800
#define GuiLib_BITFLAG_FORMATTRAILINGZEROS 0x00001000
#define GuiLib_BITFLAG_FORMATTHOUSANDSSEP  0x00002000
#define GuiLib_BITFLAG_FIELDSCROLLBOX      0x00004000
#define GuiLib_BITFLAG_BARTRANSPARENT      0x00008000

#define GuiLib_SCROLL_STRUCTURE_UNDEF      0
#define GuiLib_SCROLL_STRUCTURE_READ       1
#define GuiLib_SCROLL_STRUCTURE_USED       2

#define GuiLib_GRAPH_STRUCTURE_UNDEF       0
#define GuiLib_GRAPH_STRUCTURE_USED        1

#define GuiLib_GRAPH_DATATYPE_DOT          0
#define GuiLib_GRAPH_DATATYPE_LINE         1
#define GuiLib_GRAPH_DATATYPE_BAR          2
#define GuiLib_GRAPH_DATATYPE_CROSS        3
#define GuiLib_GRAPH_DATATYPE_X            4

#define GuiLib_GRAPHAXIS_X                 0
#define GuiLib_GRAPHAXIS_Y                 1

#define GuiLib_GRAPHICS_LAYER_UNDEF        0
#define GuiLib_GRAPHICS_LAYER_USED         1

#define GuiLib_GRAPHICS_LAYER_SIZE_COORD   0
#define GuiLib_GRAPHICS_LAYER_SIZE_SCREEN  1
#define GuiLib_GRAPHICS_LAYER_SIZE_CLIP    2

#define GuiLib_GRAPHICS_LAYER_INIT_NONE    0
#define GuiLib_GRAPHICS_LAYER_INIT_COL     1
#define GuiLib_GRAPHICS_LAYER_INIT_COPY    2

#define GuiLib_GRAPHICS_LAYER_CURRENT      -3
#define GuiLib_GRAPHICS_LAYER_PREVIOUS     -2
#define GuiLib_GRAPHICS_LAYER_BASE         -1

#define GuiLib_GRAPHICS_FILTER_UNDEF       0
#define GuiLib_GRAPHICS_FILTER_USED        1

#define GuiLib_MARKER_NONE                 0
#define GuiLib_INDICATOR_NONE              0
#define GuiLib_MARKER_SIZE                 8

#define GuiLib_FULL_BITMAP                 0
#define GuiLib_AREA_BITMAP                 1

#define GuiLib_LANGUAGE_INACTIVE           9999

#define GuiLib_LINEFEED                    0x0A

#define GuiLib_CHECKBOX_FLAT               0
#define GuiLib_CHECKBOX_3D                 1
#define GuiLib_CHECKBOX_ICON               2
#define GuiLib_CHECKBOX_BITMAP             3
#define GuiLib_CHECKBOX_NONE               4
#define GuiLib_CHECKBOX_MARK_CHECKED       0
#define GuiLib_CHECKBOX_MARK_CROSSED       1
#define GuiLib_CHECKBOX_MARK_FILLED        2
#define GuiLib_CHECKBOX_MARK_ICON          3
#define GuiLib_CHECKBOX_MARK_BITMAP        4

#define GuiLib_RADIOBUTTON_FLAT            0
#define GuiLib_RADIOBUTTON_3D              1
#define GuiLib_RADIOBUTTON_ICON            2
#define GuiLib_RADIOBUTTON_BITMAP          3
#define GuiLib_RADIOBUTTON_MARK_STANDARD   0
#define GuiLib_RADIOBUTTON_MARK_ICON       1
#define GuiLib_RADIOBUTTON_MARK_BITMAP     2

#define GuiLib_BUTTON_LAYOUT_TEXT          0
#define GuiLib_BUTTON_LAYOUT_GLYPH         1
#define GuiLib_BUTTON_LAYOUT_GLYPHLEFT     2
#define GuiLib_BUTTON_LAYOUT_GLYPHRIGHT    3
#define GuiLib_BUTTON_LAYOUT_GLYPHBOTTOM   4
#define GuiLib_BUTTON_LAYOUT_GLYPHTOP      5

#define GuiLib_BUTTON_BODY_FLAT            0
#define GuiLib_BUTTON_BODY_3D              1
#define GuiLib_BUTTON_BODY_ICON            2
#define GuiLib_BUTTON_BODY_BITMAP          3

#define GuiLib_BUTTON_GLYPH_ICON           0
#define GuiLib_BUTTON_GLYPH_BITMAP         1

#define GuiLib_PANEL_FLAT                  0
#define GuiLib_PANEL_3D_RAISED             1
#define GuiLib_PANEL_3D_LOWERED            2
#define GuiLib_PANEL_EMBOSSED_RAISED       3
#define GuiLib_PANEL_EMBOSSED_LOWERED      4

#define GuiLib_AUTOREDRAW_MAX_VAR_SIZE GuiConst_AUTOREDRAW_MAX_VAR_SIZE

//----------------------X-----------------------
#ifdef GuiConst_REMOTE_STRUCT_DATA
  typedef GuiConst_PTR *GuiLib_StructPtr;
#else
  #ifdef GuiConst_DISP_VAR_NOW
    unsigned char displayVarNow;
  #endif
  #ifdef GuiConst_AVRGCC_COMPILER
    typedef GuiConst_PTR *GuiLib_StructPtr;
    #define ReadBytePtr(X) pgm_read_byte(X)
    #define ReadByte(X)    pgm_read_byte(&X)
    #define ReadWordPtr(X) pgm_read_word(X)
    #define ReadWord(X)    pgm_read_word(&X)
  #else
    #if defined GuiConst_ICC_COMPILER
      typedef void PrefixRom *GuiLib_StructPtr;
    #elif defined GuiConst_CODEVISION_COMPILER
      typedef void PrefixRom *GuiLib_StructPtr;
    #else
      typedef GuiConst_PTR *GuiLib_StructPtr;
    #endif
  #endif
#endif
//----------------------X-----------------------
#ifndef ReadBytePtr
#define ReadBytePtr(X) *(X)
#endif
#ifndef ReadByte
#define ReadByte(X)    X
#endif
#ifndef ReadWordPtr
#define ReadWordPtr(X) *(X)
#endif
#ifndef ReadWord
#define ReadWord(X)    X
#endif

//----------------------X-----------------------


//----------------------X-----------------------
typedef struct
{
  GuiConst_INT32U BitFlags;
  GuiConst_INT16S BackBoxSizeX;
  GuiConst_INT8U BackBoxSizeY1, BackBoxSizeY2;
  GuiConst_INT8U FontIndex;
  GuiConst_INT8U Alignment;
  GuiConst_INT8U Ps;
  GuiConst_INT8U BackBorderPixels;
} TextParRec;
//----------------------X-----------------------
#ifdef GuiConst_ITEM_STRUCTCOND_INUSE
typedef struct
{
  GuiConst_INT16U IndexCount;
  GuiConst_INT16S IndexFirst[GuiConst_STRUCTCOND_MAX];
  GuiConst_INT16S IndexLast[GuiConst_STRUCTCOND_MAX];
  GuiConst_INT16U CallIndex[GuiConst_STRUCTCOND_MAX];
} CompStructCallRec;
#else
typedef struct
{
  GuiConst_INT16U IndexCount;
} CompStructCallRec;
#endif // GuiConst_ITEM_STRUCTCOND_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
typedef struct
{
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  GuiConst_INT16S ScrollPos;
  GuiConst_INT16U Lines;
  GuiConst_INT8U ScrollIndex;
#endif // GuiConst_TEXTBOX_FIELDS_ON
  GuiConst_INT8U HorzAlignment;
  GuiConst_INT8U VertAlignment;
  GuiConst_INT8S LineDist;
  GuiConst_INT8U LineDistRelToFont;
} CompTextBoxRec;
#endif // GuiConst_ITEM_TEXTBLOCK_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
typedef struct
{
  GuiConst_INT16U AreaNo;
} CompTouchRec;
#endif
//----------------------X-----------------------
#ifdef GuiConst_BITMAP_SUPPORT_ON
typedef struct
{
  GuiConst_INTCOLOR TranspColor;
} CompBitmapRec;
#endif
//----------------------X-----------------------
#ifdef GuiConst_ITEM_CHECKBOX_INUSE
typedef struct
{
  GuiConst_INT16U MarkOffsetX;
  GuiConst_INT16U MarkOffsetY;
  GuiConst_INT16U MarkBitmapIndex;
  GuiConst_INT16U IconOffsetX;
  GuiConst_INT16U IconOffsetY;
  GuiConst_INT16U BitmapIndex;
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR BitmapTranspColor;
  GuiConst_INTCOLOR MarkBitmapTranspColor;
#endif // GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR MarkColor;
  GuiConst_INT16U MarkColorIndex;
  GuiConst_TEXT *IconPtr;
  GuiConst_TEXT *MarkIconPtr;
  GuiConst_INT8U Style;
  GuiConst_INT8U Size;
  GuiConst_INT8U IconFont;
  GuiConst_INT8U BitmapIsTransparent;
  GuiConst_INT8U MarkStyle;
  GuiConst_INT8U MarkIconFont;
  GuiConst_INT8U MarkBitmapIsTransparent;
} CompCheckBoxRec;
#endif // GuiConst_ITEM_CHECKBOX_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
typedef struct
{
  GuiConst_INT16U InterDistance;
  GuiConst_INT16U MarkOffsetX;
  GuiConst_INT16U MarkOffsetY;
  GuiConst_INT16U MarkBitmapIndex;
  GuiConst_INT16U IconOffsetX;
  GuiConst_INT16U IconOffsetY;
  GuiConst_INT16U BitmapIndex;
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR BitmapTranspColor;
  GuiConst_INTCOLOR MarkBitmapTranspColor;
#endif // GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR MarkColor;
  GuiConst_INT16U MarkColorIndex;
  GuiConst_TEXT *IconPtr;
  GuiConst_TEXT *MarkIconPtr;
  GuiConst_INT8U Style;
  GuiConst_INT8U Size;
  GuiConst_INT8U Count;
  GuiConst_INT8U IconFont;
  GuiConst_INT8U BitmapIsTransparent;
  GuiConst_INT8U MarkStyle;
  GuiConst_INT8U MarkIconFont;
  GuiConst_INT8U MarkBitmapIsTransparent;
} CompRadioButtonRec;
#endif // GuiConst_ITEM_RADIOBUTTON_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_BUTTON_INUSE
typedef struct
{
  GuiConst_INT8U Layout;
  GuiConst_INT8U BodyStyle;
  GuiConst_INT8U BodyLikeUp;
  GuiConst_TEXT *BodyIconPtr[3];
  GuiConst_INT8U BodyIconFont[3];
  GuiConst_INT16U BodyIconOffsetX[3];
  GuiConst_INT16U BodyIconOffsetY[3];
  GuiConst_INT16U BodyBitmapIndex[3];
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR BodyBitmapTranspColor[3];
#endif // GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INT8U BodyBitmapIsTransparent[3];
  GuiConst_INT8U TextLikeUp;
  GuiConst_INTCOLOR TextColor[3];
  GuiConst_INT16U TextColorIndex[3];
  GuiConst_INT8U GlyphStyle;
  GuiConst_INT8U GlyphLikeUp;
  GuiConst_INTCOLOR GlyphIconColor[3];
  GuiConst_INT16U GlyphIconColorIndex[3];
  GuiConst_TEXT *GlyphIconPtr[3];
  GuiConst_INT8U GlyphIconFont[3];
  GuiConst_INT16U GlyphIconOffsetX[3];
  GuiConst_INT16U GlyphIconOffsetY[3];
  GuiConst_INT16U GlyphBitmapIndex[3];
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR GlyphBitmapTranspColor[3];
#endif // GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INT8U GlyphBitmapIsTransparent[3];
  GuiConst_INT16U GlyphBitmapOffsetX[3];
  GuiConst_INT16U GlyphBitmapOffsetY[3];
} CompButtonRec;
#endif // GuiConst_ITEM_BUTTON_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_PANEL_INUSE
typedef struct
{
  GuiConst_INT8U Style;
} CompPanelRec;
#endif
//----------------------X-----------------------
typedef union
{
  CompStructCallRec StructCall;
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  CompTextBoxRec     CompTextBox;
#endif
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  CompTouchRec       CompTouch;
#endif
#ifdef GuiConst_ITEM_CHECKBOX_INUSE
  CompCheckBoxRec    CompCheckBox;
#endif
#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
  CompRadioButtonRec CompRadioButton;
#endif
#ifdef GuiConst_ITEM_BUTTON_INUSE
  CompButtonRec CompButton;
#endif
#ifdef GuiConst_ITEM_PANEL_INUSE
  CompPanelRec       CompPanel;
#endif
#ifdef GuiConst_BITMAP_SUPPORT_ON
  CompBitmapRec      CompBitmap;
#endif
} CompUnion;
//----------------------X-----------------------
typedef struct
{
  TextParRec TextPar[3];
  void *VarPtr;
  GuiConst_INT16U StructToCallIndex;
  GuiConst_INT16S X1, Y1, X2, Y2;
  GuiConst_INT16S R1, R2;
  GuiConst_INT16S RX, RY;
  GuiConst_INT16S RX1, RY1, RX2, RY2;
  GuiConst_INT16S DrawnX1, DrawnY1, DrawnX2, DrawnY2;
  GuiConst_INT16U TextLength[3];
  GuiConst_INT16S ForeColorEnhance;
  GuiConst_INT16S BackColorEnhance;
  GuiConst_INT16S BarForeColorEnhance;
  GuiConst_INT16S BarBackColorEnhance;
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  GuiConst_INT16S CoordOrigoX, CoordOrigoY;
#endif
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S ClipRectX1, ClipRectY1, ClipRectX2, ClipRectY2;
#endif
  GuiConst_INT16U PosCallbackNo;
  GuiConst_INTCOLOR ForeColor, BackColor;
  GuiConst_INTCOLOR BarForeColor, BarBackColor;
  GuiConst_INT16U ForeColorIndex;
  GuiConst_INT16U BackColorIndex;
  GuiConst_INT16U BarForeColorIndex;
  GuiConst_INT16U BarBackColorIndex;
  GuiConst_TEXT *TextPtr[3];
  GuiConst_INT8U TextCnt;
  GuiConst_INT8U Drawn;
  GuiConst_INT8U ItemType;
  GuiConst_INT8U FrameThickness;
  GuiConst_INT8U FormatFieldWidth;
  GuiConst_INT8U FormatDecimals;
  GuiConst_INT8U FormatAlignment;
  GuiConst_INT8U FormatFormat;
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8S CursorFieldNo;
  GuiConst_INT8U CursorFieldLevel;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
  GuiConst_INT8S BlinkFieldNo;
#endif
  GuiConst_INT8U UpdateType;
  GuiConst_INT8U VarType;
  GuiConst_INT8U LinePattern;
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  GuiConst_INT8U CursorScrollBoxIndex;
#endif
  CompUnion CompPars;
} GuiLib_ItemRec;
typedef GuiLib_ItemRec* GuiLib_ItemRecPtr;
//----------------------X-----------------------
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
typedef struct
{
  GuiConst_INT16S X1, Y1, X2, Y2;
  GuiConst_INT16U IndexNo;
} GuiLib_TouchAreaRec;
#endif
//----------------------X-----------------------
typedef struct
{
  void (*PosCallbackFunc) (GuiConst_INT16U IndexNo,
                           GuiConst_INT16S X,
                           GuiConst_INT16S Y);
  GuiConst_INT16U IndexNo;
  GuiConst_INT8U InUse;
} GuiLib_PosCallbackRec;

//----------------------X-----------------------
typedef GuiConst_INT16S TextXOfsAry[GuiConst_MAX_TEXT_LEN + 2];
typedef TextXOfsAry* TextXOfsAryPtr;
//----------------------X-----------------------
#ifdef GuiConst_BLINK_SUPPORT_ON
#ifndef GuiConst_BLINK_FIELDS_OFF
typedef struct
{
  TextParRec TextPar;
  GuiConst_INT16U CharNo;
  GuiConst_INT16U CharCnt;
  GuiConst_INT16S X1, X2;
  GuiConst_INT16S Y1, Y2;
  GuiConst_INT16S BlinkBoxX1, BlinkBoxX2;
  GuiConst_INT16S BlinkBoxY1, BlinkBoxY2;
  GuiConst_INT16S LineCnt;
  GuiConst_INT16S BlindLinesAtTop;
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  GuiConst_INT16S TextBoxScrollPos;
#endif
  GuiConst_TEXT PrefixGeneric *TextPtr;
  GuiConst_INT8U InUse;
  GuiConst_INT8U Active;
  GuiConst_INT8U XSize;
  GuiConst_INT8U ItemType;
  GuiConst_INT8U VarType;
  GuiConst_INT8U FormatFieldWidth;
  GuiConst_INT8U FormatDecimals;
  GuiConst_INT8U FormatAlignment;
  GuiConst_INT8U FormatFormat;
  GuiConst_INT8U PsNumWidth;
  GuiConst_INT8U PsSpace;
  GuiConst_INT8U BlinkBoxRate;
  GuiConst_INT8U BlinkBoxState;
  GuiConst_INT8U BlinkBoxInverted;
  GuiConst_INT8U BlinkBoxLast;
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  GuiConst_INT8U YSize;
  GuiConst_INT8S TextBoxLineDist;
  GuiConst_INT8U TextBoxHorzAlignment;
  GuiConst_INT8U TextBoxVertAlignment;
#endif
} GuiLib_BlinkTextItemRec;
#endif // GuiConst_BLINK_FIELDS_OFF
#endif // GuiConst_BLINK_SUPPORT_ON
//----------------------X-----------------------
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
typedef struct
{
  void (*ScrollLineDataFunc) (GuiConst_INT16S LineIndex);
  GuiLib_ItemRec ScrollBoxItem;
  GuiConst_INT16S X1;
  GuiConst_INT16S Y1;
  GuiConst_INT16S ScrollTopLine;
  GuiConst_INT16U LastScrollTopLine;
  GuiConst_INT16S LastMarkerLine;
  GuiConst_INT16U ScrollActiveLine;
  GuiConst_INT16U NumberOfLines;
  GuiConst_INT16U MakeUpStructIndex;
  GuiConst_INT16U ScrollVisibleLines;
  GuiConst_INT16S LineVerticalOffset;
  GuiConst_INT16S LineOffsetX;
  GuiConst_INT16S LineOffsetY;
  GuiConst_INT16S LineSizeX;
  GuiConst_INT16S LineSizeY;
  GuiConst_INT16S LineSizeY2;
  GuiConst_INT16U LineStructIndex;
  GuiConst_INT16S LineStructOffsetX;
  GuiConst_INT16S LineStructOffsetY;
  GuiConst_INT16U MarkerStructIndex[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT16S MarkerStartLine[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT16S MarkerSize[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INTCOLOR LineColor;
  GuiConst_INT16U LineColorIndex;
  GuiConst_INTCOLOR BackColor;
  GuiConst_INT16U BackColorIndex;
  GuiConst_INTCOLOR MarkerColor[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT16U MarkerColorIndex[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT8U InUse;
  GuiConst_INT8U ScrollBoxType;
  GuiConst_INT8U LineColorTransparent;
  GuiConst_INT8U WrapMode;
  GuiConst_INT8U ScrollStartOfs;
  GuiConst_INT8U ScrollMode;
  GuiConst_INT8U LineMarkerCount;
  GuiConst_INT8U MarkerColorTransparent[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT8U MarkerDrawingOrder[GuiConst_SCROLLITEM_MARKERS_MAX];
  GuiConst_INT8U BarType;
  GuiConst_INT8U IndicatorType;
#ifndef GuiConst_SCROLLITEM_BAR_NONE
  GuiConst_INT16S BarPositionX;
  GuiConst_INT16S BarPositionY;
  GuiConst_INT16S BarSizeX;
  GuiConst_INT16S BarSizeY;
  GuiConst_INT16U BarStructIndex;
  GuiConst_INT16S BarMarkerLeftOffset;
  GuiConst_INT16S BarMarkerRightOffset;
  GuiConst_INT16S BarMarkerTopOffset;
  GuiConst_INT16S BarMarkerBottomOffset;
  GuiConst_INT16U BarMarkerIconOffsetX;
  GuiConst_INT16U BarMarkerIconOffsetY;
  GuiConst_INT16U BarMarkerBitmapIndex;
  GuiConst_INT16U BarMarkerBitmapHeight;
  GuiConst_INTCOLOR BarForeColor;
  GuiConst_INT16U BarForeColorIndex;
  GuiConst_INTCOLOR BarBackColor;
  GuiConst_INT16U BarBackColorIndex;
  GuiConst_INTCOLOR BarMarkerForeColor;
  GuiConst_INT16U BarMarkerForeColorIndex;
  GuiConst_INTCOLOR BarMarkerBackColor;
  GuiConst_INT16U BarMarkerBackColorIndex;
  GuiConst_INT8U BarMarkerBitmapIsTransparent;
  GuiConst_INT8U BarMode;
  GuiConst_INT8U BarTransparent;
  GuiConst_INT8U BarThickness;
  GuiConst_INT8U BarMarkerIconFont;
  GuiConst_INT8U BarMarkerTransparent;
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR BarMarkerBitmapTranspColor;
#endif // GuiConst_BITMAP_SUPPORT_ON
  GuiConst_TEXT *BarIconPtr;
#endif // GuiConst_SCROLLITEM_BAR_NONE
#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
  GuiConst_INT16S IndicatorPositionX;
  GuiConst_INT16S IndicatorPositionY;
  GuiConst_INT16S IndicatorSizeX;
  GuiConst_INT16S IndicatorSizeY;
  GuiConst_INT16U IndicatorStructIndex;
  GuiConst_INT16S IndicatorMarkerLeftOffset;
  GuiConst_INT16S IndicatorMarkerRightOffset;
  GuiConst_INT16S IndicatorMarkerTopOffset;
  GuiConst_INT16S IndicatorMarkerBottomOffset;
  GuiConst_INT16U IndicatorMarkerIconFont;
  GuiConst_INT16U IndicatorMarkerIconOffsetX;
  GuiConst_INT16U IndicatorMarkerIconOffsetY;
  GuiConst_INT16U IndicatorMarkerBitmapIndex;
  GuiConst_INT16S IndicatorLine;
  GuiConst_INTCOLOR IndicatorForeColor;
  GuiConst_INT16U IndicatorForeColorIndex;
  GuiConst_INTCOLOR IndicatorBackColor;
  GuiConst_INT16U IndicatorBackColorIndex;
  GuiConst_INTCOLOR IndicatorMarkerForeColor;
  GuiConst_INT16U IndicatorMarkerForeColorIndex;
  GuiConst_INTCOLOR IndicatorMarkerBackColor;
  GuiConst_INT16U IndicatorMarkerBackColorIndex;
#ifdef GuiConst_BITMAP_SUPPORT_ON
  GuiConst_INTCOLOR IndicatorMarkerBitmapTranspColor;
#endif // GuiConst_BITMAP_SUPPORT_ON
  GuiConst_TEXT *IndicatorIconPtr;
  GuiConst_INT8U IndicatorMode;
  GuiConst_INT8U IndicatorTransparent;
  GuiConst_INT8U IndicatorThickness;
  GuiConst_INT8U IndicatorMarkerTransparent;
  GuiConst_INT8U IndicatorMarkerBitmapIsTransparent;
#endif // GuiConst_SCROLLITEM_INDICATOR_NONE
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8U ContainsCursorFields;
#endif // GuiConst_CURSOR_SUPPORT_ON
} ScrollBoxRec;
#endif // GuiConst_ITEM_SCROLLBOX_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_GRAPH_INUSE
typedef struct
{
  GuiConst_INT32S NumbersMinValue;
  GuiConst_INT32S NumbersMaxValue;
  GuiConst_INT32S NumbersMinValueOrg;
  GuiConst_INT32S NumbersMaxValueOrg;
  GuiConst_INT32S NumbersStepMajor;
  GuiConst_INT32S NumbersStepMinor;
  GuiConst_INT32S Scale;
  GuiConst_INT32U BitFlags;
  GuiConst_INT16S Offset;
  GuiConst_INT16S ArrowLength;
  GuiConst_INT16S ArrowWidth;
  GuiConst_INT16S TicksMajorLength;
  GuiConst_INT16S TicksMajorWidth;
  GuiConst_INT16S TicksMinorLength;
  GuiConst_INT16S TicksMinorWidth;
  GuiConst_INT16S NumbersAtEnd;
  GuiConst_INT16S NumbersOffset;
  GuiConst_INT8U Visible;
  GuiConst_INT8U Line;
  GuiConst_INT8U LineBetweenAxes;
  GuiConst_INT8U LineNegative;
  GuiConst_INT8U Arrow;
  GuiConst_INT8U TicksMajor;
  GuiConst_INT8U TicksMinor;
  GuiConst_INT8U Numbers;
  GuiConst_INT8U NumbersAtOrigo;
  GuiConst_INT8U FormatFieldWidth;
  GuiConst_INT8U FormatDecimals;
  GuiConst_INT8U FormatAlignment;
  GuiConst_INT8U FormatFormat;
} GraphAxisRec;
//----------------------X-----------------------
typedef struct
{
  GuiLib_GraphDataPoint *DataPtr;
  GuiConst_INT16U DataSize;
  GuiConst_INT16U DataFirst;
  GuiConst_INT16U DataCount;
  GuiConst_INT16S Width;
  GuiConst_INT16S Height;
  GuiConst_INT16S Thickness;
  GuiConst_INTCOLOR ForeColor, BackColor;
  GuiConst_INT16U ForeColorIndex;
  GuiConst_INT16U BackColorIndex;
  GuiConst_INT8U Visible;
  GuiConst_INT8U Representation;
  GuiConst_INT8U BackColorTransparent;
  GuiConst_INT8U AxisIndexX, AxisIndexY;
} GraphDataSetRec;
//----------------------X-----------------------
typedef struct
{
  GuiLib_ItemRec GraphItem;
  GraphAxisRec GraphAxes[GuiConst_GRAPH_AXES_MAX][2];
  GraphDataSetRec GraphDataSets[GuiConst_GRAPH_DATASETS_MAX];
  GuiConst_INT16S OrigoX, OrigoY;
  GuiConst_INT16S OriginOffsetX, OriginOffsetY;
  GuiConst_INTCOLOR ForeColor, BackColor;
  GuiConst_INT16U ForeColorIndex;
  GuiConst_INT16U BackColorIndex;
  GuiConst_INT8U InUse;
  GuiConst_INT8U GraphAxesCnt[2];
  GuiConst_INT8U GraphDataSetCnt;
} GraphItemRec;
#endif //GuiConst_ITEM_GRAPH_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
typedef struct
{
  GuiConst_INT8U *BaseAddress;
  GuiConst_INT16U X, Y;
  GuiConst_INT16U LineSize;
  GuiConst_INT16U Width, Height;
  GuiConst_INT8U InUse;
  GuiConst_INT8U SizeMode;
  GuiConst_INT8U InitMode;
} GraphicsLayerRec;
typedef struct
{
  void (*GraphicsFilterFunc)
     (GuiConst_INT8U *DestAddress,
      GuiConst_INT16U DestLineSize,
      GuiConst_INT8U *SourceAddress,
      GuiConst_INT16U SourceLineSize,
      GuiConst_INT16U Width,
      GuiConst_INT16U Height,
      GuiConst_INT32S FilterPars[10]);
  void *ParVarPtr[10];
  GuiConst_INT32S ParValueNum[10];
  GuiConst_INT16S SourceLayerIndexNo;
  GuiConst_INT16S DestLayerIndexNo;
  GuiConst_INT16S ContAtLayerIndexNo;
  GuiConst_INT8U InUse;
  GuiConst_INT8U ParVarType[10];
} GraphicsFilterRec;
#endif // GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
//----------------------X-----------------------
#ifdef GuiConst_BITMAP_SUPPORT_ON
typedef struct
{
  GuiConst_INT16S Index;
  GuiConst_INT16S X;
  GuiConst_INT16S Y;
  GuiConst_INT8U InUse;
} BackgrBitmapRec;
#endif // GuiConst_BITMAP_SUPPORT_ON
//----------------------X-----------------------
typedef struct
{
  GuiConst_INT16S X[2 * GuiLib_MEMORY_CNT];
  GuiConst_INT16S Y[2 * GuiLib_MEMORY_CNT];
  GuiConst_INTCOLOR C[GuiLib_MEMORY_CNT];
} ItemMemory;
//----------------------X-----------------------
#define ITEM_NONE         0
#define ITEM_AUTOREDRAW   1
#define ITEM_CURSOR       2
typedef struct
{
  GuiLib_ItemRec  Item;
  ItemMemory      Memory;
  GuiConst_INT16S Prev;
  GuiConst_INT16S Next;
  GuiConst_INT16S Parent;
  GuiConst_INT16S Padding;
  GuiConst_INT8U  Valid;
  GuiConst_INT8U  Level;
  GuiConst_INT16U ValueSize;
  GuiConst_INT8U  Value[GuiLib_AUTOREDRAW_MAX_VAR_SIZE];
} AutoRedrawItems;
//----------------------X-----------------------
static GuiConst_INT8U GetItemByte(GuiConst_INT8U**ItemDataPtrPtr);
static GuiConst_INT16S GetItemWord(GuiConst_INT8U**ItemDataPtrPtr);
#ifdef GuiLib_COLOR_BYTESIZE_3
static GuiConst_INT32S GetItemTriple(GuiConst_INT8U**ItemDataPtrPtr);
#endif
#ifdef GuiLib_GETITEMLONG
static GuiConst_INT32S GetItemLong(GuiConst_INT8U**ItemDataPtrPtr);
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
static void BlinkBox(void);
#endif
//----------------------X-----------------------
void AutoRedraw_Init(void);
void AutoRedraw_UpdateDrawn(GuiConst_INT16S I, GuiLib_ItemRec *Item);
void AutoRedraw_Delete(GuiConst_INT16S I);
void AutoRedraw_Destroy(void);
GuiConst_INT8S AutoRedraw_GetLevel(GuiConst_INT16S I);
GuiConst_INT8S AutoRedraw_ItemIsStruct(GuiConst_INT16S I);
GuiConst_INT16S AutoRedraw_DeleteStruct(GuiConst_INT16S Struct_id);
GuiConst_INT16S AutoRedraw_Reset(void);
GuiConst_INT16S AutoRedraw_GetNext(GuiConst_INT16S I);
GuiConst_INT16S AutoRedraw_Add(GuiLib_ItemRec *Item, GuiConst_INT16S Struct, GuiConst_INT8U Level);
GuiConst_INT16S AutoRedraw_Insert(GuiLib_ItemRec *Item, GuiConst_INT16S Struct, GuiConst_INT8U Level);
GuiLib_ItemRec *AutoRedraw_GetItem(GuiConst_INT16S I);
ItemMemory     *AutoRedraw_GetItemMemory(GuiConst_INT16S I);
GuiConst_INT8S RefreshColorVariable(GuiConst_INTCOLOR *comp, GuiConst_INT16U idx);
void AutoRedraw_UpdateOnChange(GuiConst_INT16S I);
void AutoRedraw_UpdateVar(GuiConst_INT16S I);
GuiConst_INT8S AutoRedraw_VarChanged(GuiConst_INT16S I);
#ifdef GuiConst_CURSOR_SUPPORT_ON
void AutoRedraw_ResetCursor(void);
GuiConst_INT16S AutoRedraw_InsertCursor(GuiLib_ItemRec *Item,
                  GuiConst_INT16S Struct,
                  GuiConst_INT8U Level);
void AutoRedraw_SetAsCursor(GuiConst_INT16S I);
GuiConst_INT16S AutoRedraw_IsOnlyCursor(GuiConst_INT16S I);
GuiConst_INT16S AutoRedraw_GetCursor(GuiConst_INT8S C, GuiConst_INT16S I);
GuiConst_INT8S AutoRedraw_GetCursorNumber(GuiConst_INT16S I);
GuiConst_INT16S AutoRedraw_GetFirstCursor(void);
GuiConst_INT16S AutoRedraw_GetLastCursor(void);
GuiConst_INT16S AutoRedraw_GetNextCursor(GuiConst_INT8S C);
GuiConst_INT16S AutoRedraw_GetPrevCursor(GuiConst_INT8S C);
GuiConst_INT16S AutoRedraw_CheckCursorInheritance(GuiConst_INT16S N);
#endif
//----------------------X-----------------------
typedef struct
{
  GuiConst_INT16S DrawnX1, DrawnY1, DrawnX2, DrawnY2;
  GuiConst_INT16U Dummy1_16U;
  GuiConst_INT16U Dummy2_16U;
  GuiConst_INT16S Dummy1_16S;
#ifdef GuiConst_ARAB_CHARS_INUSE
  GuiConst_INT16S ArabicCharJoiningModeIndex[GuiConst_MAX_TEXT_LEN + 2];
  GuiConst_INT8U ArabicCharJoiningMode[GuiConst_MAX_TEXT_LEN + 2];
  GuiConst_INT8U ArabicCharJoiningModeBefore;
  GuiConst_INT8U ArabicCharJoiningModeAfter;
#endif
  GuiConst_INT8U Drawn;
  GuiConst_INT8U Dummy1_8U;
  GuiConst_INT8U Dummy2_8U;
  GuiConst_INT8U Dummy3_8U;
} GuiLib_GLOBAL;
typedef struct
{
  GuiLib_ItemRec CurItem;
  GuiLib_FontRecPtr CurFont;
  GuiLib_StructPtr TopLevelStructure;
  ItemMemory      Memory;
  AutoRedrawItems AutoRedraw[GuiConst_MAX_DYNAMIC_ITEMS];
  GuiConst_INT8U *ItemDataPtr;
  GuiConst_INT8U *CurLayerBufPtr;
  GuiConst_INT32U RefreshClock;
  GuiConst_INT32U CurLayerBytes;
  GuiConst_INT32U ItemTypeBit1, ItemTypeBit2;
  GuiConst_INT16S AutoRedrawFirst;
  GuiConst_INT16S AutoRedrawLast;
  GuiConst_INT16S AutoRedrawLatest;
  GuiConst_INT16S AutoRedrawNext;
  GuiConst_INT16S AutoRedrawCount;
  GuiConst_INT16S AutoRedrawParent;
  GuiConst_INT16S AutoRedrawUpdate;
  GuiConst_INT16S AutoRedrawInsertPoint;
  GuiConst_INT16U CurLayerLineSize;
  GuiConst_INT16U CurLayerWidth;
  GuiConst_INT16U CurLayerHeight;
  GuiConst_INT16S DisplayOrigoX, DisplayOrigoY;
  GuiConst_INT16S LayerOrigoX, LayerOrigoY;
  GuiConst_INT16S CoordOrigoX, CoordOrigoY;
  GuiConst_INT16S InvertBoxX1, InvertBoxY1, InvertBoxX2, InvertBoxY2;
  GuiConst_INT16S ItemX1, ItemY1;
  GuiConst_INT16S ItemX2, ItemY2;
  GuiConst_INT16S ItemR1, ItemR2;
  GuiConst_INT16U X1VarIdx, Y1VarIdx, X2VarIdx, Y2VarIdx;
  GuiConst_INT16U R1VarIdx, R2VarIdx;
  GuiConst_INT16S BbX1, BbX2;
  GuiConst_INT16S BbY1, BbY2;
  GuiConst_INT16S DisplayLevel;
  GuiConst_INT16S AutoRedrawSaveIndex;
  GuiConst_INT16S ThicknessMemory;
  GuiConst_INT16S FontWriteX1, FontWriteY1, FontWriteX2, FontWriteY2;
  GuiConst_INT16U ColMemoryIndex[GuiLib_MEMORY_CNT];
  GuiConst_INT8U TextPsMode[GuiConst_MAX_TEXT_LEN + 1];
  GuiConst_CHAR  VarNumTextStr[GuiConst_MAX_VARNUM_TEXT_LEN + 1];
  GuiConst_INT8U InvertBoxOn;
  GuiConst_INT8U CommonByte0;
  GuiConst_INT8U CommonByte1;
  GuiConst_INT8U CommonByte2;
  GuiConst_INT8U CommonByte3;
  GuiConst_INT8U CommonByte4;
  GuiConst_INT8U CommonByte5;
  GuiConst_INT8U CommonByte6;
  GuiConst_INT8U CommonByte7;
  GuiConst_INT8U X1Mode, Y1Mode;
  GuiConst_INT8U X2Mode, Y2Mode;
  GuiConst_INT8U R1Mode, R2Mode;
  GuiConst_INT8U X1MemoryRead, Y1MemoryRead;
  GuiConst_INT8U X1MemoryWrite, Y1MemoryWrite;
  GuiConst_INT8U X2MemoryRead, Y2MemoryRead;
  GuiConst_INT8U X2MemoryWrite, Y2MemoryWrite;
  GuiConst_INT8U R1MemoryRead, R2MemoryRead;
  GuiConst_INT8U R1MemoryWrite, R2MemoryWrite;
  GuiConst_INT8U X1VarType, Y1VarType, X2VarType, Y2VarType;
  GuiConst_INT8U R1VarType, R2VarType;
  GuiConst_INT8U DisplayWriting;
  GuiConst_INT8U InitialDrawing;
  GuiConst_INT8U DrawingLevel;
  GuiConst_INT8U SwapColors;
  GuiConst_INT8U BaseLayerDrawing;
  GuiLib_PosCallbackRec PosCallbacks[GuiConst_POSCALLBACK_CNT];
//----------------------X-----------------------
  #ifdef GuiConst_TEXTBOX_FIELDS_ON
    GuiLib_ItemRec TextboxScrollItems[GuiConst_TEXTBOX_FIELDS_MAX];
  #endif
//----------------------X-----------------------
  #ifdef GuiConst_ITEM_TOUCHAREA_INUSE
    GuiLib_TouchAreaRec TouchAreas[GuiConst_TOUCHAREA_CNT];
    GuiConst_INT32S TouchAreaCnt;
    GuiConst_INT32S TouchAdjustXMeasured[4];
    GuiConst_INT32S TouchAdjustYMeasured[4];
    GuiConst_INT32S TouchAdjustXTL, TouchAdjustYTL;
    GuiConst_INT32S TouchAdjustXTR, TouchAdjustYTR;
    GuiConst_INT32S TouchAdjustXBL, TouchAdjustYBL;
    GuiConst_INT32S TouchAdjustXBR, TouchAdjustYBR;
    GuiConst_INT16S TouchAdjustXTrue[4];
    GuiConst_INT16S TouchAdjustYTrue[4];
    GuiConst_INT8U TouchAdjustInUse[4];
    GuiConst_INT8U TouchAdjustActive;
    GuiConst_INT16S TouchConvertX, TouchConvertY;
  #endif // GuiConst_ITEM_TOUCHAREA_INUSE
//----------------------X-----------------------
  #ifdef GuiConst_FLOAT_SUPPORT_ON
    GuiConst_INT16S VarExponent;
  #endif
//----------------------X-----------------------
  #ifdef GuiConst_CHARMODE_UNICODE
    GuiConst_TEXT VarNumUnicodeTextStr[GuiConst_MAX_VARNUM_TEXT_LEN + 1];
    GuiConst_TEXT UnicodeTextBuf[GuiConst_MAX_TEXT_LEN + 1];
  #else
    GuiConst_TEXT AnsiTextBuf[GuiConst_MAX_TEXT_LEN + 1];
  #endif // GuiConst_CHARMODE_UNICODE
//----------------------X-----------------------
  #ifdef GuiConst_CURSOR_SUPPORT_ON
    GuiConst_INT16S CursorFieldFound;
    GuiConst_INT8U CursorInUse;
    GuiConst_INT8U CursorActiveFieldFound;
  #endif // GuiConst_CURSOR_SUPPORT_ON
//----------------------X-----------------------
  #ifdef GuiConst_BLINK_SUPPORT_ON
    #ifndef GuiConst_BLINK_FIELDS_OFF
      GuiLib_BlinkTextItemRec BlinkTextItems[GuiConst_BLINK_FIELDS_MAX];
    #endif // GuiConst_BLINK_FIELDS_OFF
    GuiConst_INT16S BlinkBoxX1, BlinkBoxY1, BlinkBoxX2, BlinkBoxY2;
    GuiConst_INT16S BlinkBoxRate;
    GuiConst_INT16S BlinkBoxState;
    GuiConst_INT8U BlinkBoxInverted;
  #endif // GuiConst_BLINK_SUPPORT_ON
//----------------------X-----------------------
  #ifdef GuiConst_CLIPPING_SUPPORT_ON
    GuiConst_INT16S DisplayActiveAreaX1, DisplayActiveAreaY1;
    GuiConst_INT16S DisplayActiveAreaX2, DisplayActiveAreaY2;
    GuiConst_INT16S ClippingX1, ClippingY1, ClippingX2, ClippingY2;
    GuiConst_INT16S ActiveAreaX1, ActiveAreaY1, ActiveAreaX2, ActiveAreaY2;
    GuiConst_INT8U ClippingTotal;
  #endif // GuiConst_CLIPPING_SUPPORT_ON
//----------------------X-----------------------
  #ifdef GuiConst_REMOTE_DATA
    #ifdef GuiConst_REMOTE_FONT_DATA
      GuiConst_INT32S CurRemoteFont;
      GuiConst_INT8U GuiLib_RemoteFontBuffer[GuiConst_REMOTE_FONT_BUF_SIZE];
    #endif // GuiConst_REMOTE_FONT_DATA
    #ifdef GuiConst_REMOTE_STRUCT_DATA
      GuiConst_INT32U RemoteStructOffset;
      GuiConst_TEXT GuiLib_RemoteStructText[GuiConst_MAX_TEXT_LEN + 1];
      GuiConst_INT8U GuiLib_RemoteStructBuffer[1];
      GuiConst_INT8U GuiLib_RemoteItemBuffer[GuiConst_REMOTE_STRUCT_BUF_SIZE];
    #endif // GuiConst_REMOTE_STRUCT_DATA
    #ifdef GuiConst_REMOTE_BITMAP_DATA
      GuiConst_INT32S CurRemoteBitmap;
      GuiConst_INT8U GuiLib_RemoteBitmapBuffer[GuiConst_REMOTE_BITMAP_BUF_SIZE];
    #endif // GuiConst_REMOTE_BITMAP_DATA
    #ifdef GuiConst_REMOTE_TEXT_DATA
      GuiConst_INT32S CurRemoteText;
      GuiConst_INT32S RemoteTextTableOfs;
      GuiConst_INT16U RemoteTextLen;
      GuiConst_TEXT GuiLib_RemoteTextBuffer[GuiConst_REMOTE_TEXT_BUF_SIZE];
    #endif // GuiConst_REMOTE_TEXT_DATA
  #endif // GuiConst_REMOTE_DATA
  #ifdef GuiConst_REMOTE_FONT_DATA
    GuiConst_INT32U TextCharNdx[GuiConst_MAX_TEXT_LEN + 1];
  #else
    GuiConst_INT8U PrefixRom *TextCharPtrAry[GuiConst_MAX_TEXT_LEN + 1];
  #endif // GuiConst_REMOTE_FONT_DATA
//----------------------X-----------------------
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  ScrollBoxRec ScrollBoxesAry[GuiConst_SCROLLITEM_BOXES_MAX];
  GuiConst_INT8U NextScrollLineReading;
  GuiConst_INT8U GlobalScrollBoxIndex;
#endif // GuiConst_ITEM_SCROLLBOX_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_GRAPH_INUSE
  GraphItemRec GraphAry[GuiConst_GRAPH_MAX];
  GuiConst_INT16U GlobalGraphIndex;
#endif
//----------------------X-----------------------
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  GraphicsLayerRec GraphicsLayerList[GuiConst_GRAPHICS_LAYER_MAX];
  GraphicsFilterRec GraphicsFilterList[GuiConst_GRAPHICS_FILTER_MAX];
  GuiConst_INT16U GlobalGraphicsLayerIndex;
  GuiConst_INT16U GlobalGraphicsFilterIndex;
  GuiConst_INT16S GraphicsLayerLifo[GuiConst_GRAPHICS_LAYER_MAX];
  GuiConst_INT8U LayerBuf[GuiConst_GRAPHICS_LAYER_BUF_BYTES];
  GuiConst_INT8U GraphicsLayerLifoCnt;
#endif // GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
//----------------------X-----------------------
#ifdef GuiConst_ITEM_BUTTON_INUSE
  GuiConst_INTCOLOR DisabledButtonColor;
  GuiConst_INT16S   ButtonColorOverride;
#endif // GuiConst_ITEM_BUTTON_INUSE
//----------------------X-----------------------
#ifdef GuiConst_BITMAP_SUPPORT_ON
  BackgrBitmapRec BackgrBitmapAry[GuiConst_MAX_BACKGROUND_BITMAPS];
  GuiConst_INT16U GlobalBackgrBitmapIndex;
  GuiConst_INT16S BitmapWriteX2, BitmapWriteY2;
  GuiConst_INT16S BitmapSizeX, BitmapSizeY;
#endif // GuiConst_BITMAP_SUPPORT_ON
} GuiLib_STATIC;


//----------------------X-----------------------
//----------------------X-----------------------


#endif

/* End of File */
