/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*          4 bit (16 color/grayscale) graphics primitives library          */
/*                      Horizontal display bytes layout                     */
/*                               v6.0.8.004                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];

#ifdef GuiConst_BIT_TOPLEFT
const GuiConst_INT8U LinePattern2Left[2] = {0xFF, 0xF0};
const GuiConst_INT8U LinePattern2Right[2] = {0x0F, 0xFF};
const GuiConst_INT8U LinePattern2Pixel[2] = {0x0F, 0xF0};

const GuiConst_INT8U Color4BitPixelPattern[4][16] =
   {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
    0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};
#else
const GuiConst_INT8U LinePattern2Left[2] = {0xFF, 0x0F};
const GuiConst_INT8U LinePattern2Right[2] = {0xF0, 0xFF};
const GuiConst_INT8U LinePattern2Pixel[2] = {0xF0, 0x0F};

const GuiConst_INT8U Color4BitPixelPattern[4][16] =
   {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
    0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0,
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};
#endif

const GuiConst_INT8U Color4BitPattern[16] =
   {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > sgl.CurLayerWidth - 1)
    X2 = sgl.CurLayerWidth - 1;
  if (Y2 > sgl.CurLayerHeight - 1)
    Y2 = sgl.CurLayerHeight - 1;

  sgl.ClippingX1 = X1;
  sgl.ClippingY1 = Y1;
  sgl.ClippingX2 = X2;
  sgl.ClippingY2 = Y2;
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  X1 = 2 * (X1 / 4);
  X2 = 2 * (X2 / 4) + 1;
#else
  X1 = X1 / 2;
  X2 = X2 / 2;
#endif

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(sgl.CurLayerBufPtr, Color4BitPattern[GuiConst_PIXEL_OFF], sgl.CurLayerBytes);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Xb;
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    Xb = (X / 2) - (2 * ((X / 2) % 2)) + 1;
#else
    Xb = X / 2;
#endif
    PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + Xb;
    *PixelPtr =
       (*PixelPtr & ~LinePattern2Pixel[X % 2]) |
       (Color4BitPattern[Color] & LinePattern2Pixel[X % 2]);

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  if ((X < 0) || (X >= sgl.CurLayerWidth) ||
      (Y < 0) || (Y >= sgl.CurLayerHeight))
    return (0);
  else
#ifdef GuiConst_BIT_TOPLEFT
    return ((*(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 2)) &
             LinePattern2Pixel[X % 2]) >> (4 * X % 2));
#else
    return ((*(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 2)) &
             LinePattern2Pixel[X % 2]) >> (4 * (1 - X % 2)));
#endif
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, X1b, X2b, Xb;
  GuiConst_INT8U *PixelPtr;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  GuiConst_INT8U *PixelPtr2;
#endif

  X1b = X1 / 2;
  X2b = X2 / 2;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  X1b = (X1b % 2) ? X1b - 1 : X1b + 1;
  X2b = (X2b % 2) ? X2b - 1 : X2b + 1;
  PixelPtr2 = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize;
#endif

  PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X1b;
  if (X1b == X2b)
  *PixelPtr = (*PixelPtr &
     ~(LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 2])) |
     (Color4BitPattern[Color] &
     (LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 2]));
  else
  {
    *PixelPtr =
     (*PixelPtr & ~LinePattern2Left[X1 % 2]) |
     (Color4BitPattern[Color] & LinePattern2Left[X1 % 2]);

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    X1b = (X1b % 2) ? X1b -1 : X1b + 3;
    while (X1b != X2b)
    {
      PixelPtr2[X1b] = Color4BitPattern[Color];
      X1b = (X1b % 2) ? X1b - 1 : X1b + 3;
    }
    PixelPtr2[X1b] =
      (PixelPtr2[X1b] & ~LinePattern2Right[X2 % 2]) |
      (Color4BitPattern[Color] & LinePattern2Right[X2 % 2]);

#else
    PixelPtr++;
    for (X = X1b + 1; X < X2b; X++)
    {
      *PixelPtr = Color4BitPattern[Color];
      PixelPtr++;
    }

    *PixelPtr =
       (*PixelPtr & ~LinePattern2Right[X2 % 2]) |
       (Color4BitPattern[Color] & LinePattern2Right[X2 % 2]);
#endif
  }
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Xb;
  GuiConst_INT8U Pattern;
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  Xb = (X / 2) - (2 * ((X / 2) % 2)) + 1;
#else
  Xb = X / 2;
#endif
  PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + Xb;

#ifdef GuiConst_BIT_TOPLEFT
  if ((X % 2) == 0)
    Pattern = Color;
  else
    Pattern = Color << 4;
#else
  if ((X % 2) == 0)
    Pattern = Color << 4;
  else
    Pattern = Color;
#endif
  while (Y1 <= Y2)
  {
    *PixelPtr = (*PixelPtr & ~LinePattern2Pixel[X % 2]) | Pattern;
    PixelPtr += sgl.CurLayerLineSize;
    Y1++;
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT16U PixelPattern;
  GuiConst_INT16S N;
  GuiConst_INT8U YHeight;
  GuiConst_INT16S Bx;
  GuiConst_INT16S PY, Y2;
  GuiConst_INT8U PixN;
  GuiConst_INT16S Xb;
  GuiConst_INT8U Pattern;
  GuiConst_INT8U PixelLineSize;
  GuiConst_INT8U *PixelPtr;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif
#ifdef GuiConst_ADV_FONTS_ON
  GuiConst_INT8U PixelShade, PixelShadeInv;
  GuiConst_INTCOLOR PixelColor;
  GuiConst_INT8U BitOffset;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (sgl.ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != sgl.CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       sgl.GuiLib_RemoteFontBuffer);
    sgl.CurRemoteFont = CharNdx;
  }
  CharPtr = &sgl.GuiLib_RemoteFontBuffer[0];
#endif

  if ((*(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

  gl.Dummy1_8U = Font->LineSize;   // To avoid compiler warning
#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  YHeight = Font->XSize;
  #else
  YHeight = Font->YSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
  PixelLineSize = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #else
  PixelLineSize = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = (YHeight + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
#ifdef GuiConst_ADV_FONTS_ON
  if (Font->ColorDepth == 4)
    PixelLineSize = (PixelLineSize + 1) / 2;
  else
#endif
    PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif

#else

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif

#endif

  PY = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = *LineCtrl;
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PY < YHeight)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PY >= YHeight - 1));

      PY++;
      if (PY % 8 == 7)
      {
        LineCtrlByte = *LineCtrl;
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

#ifdef GuiConst_ADV_FONTS_ON
    if (Font->ColorDepth == 4)
      Bx = X;
    else
#endif
      Bx = X;

    for (N = 0; N < PixelLineSize; N++)
    {
      PixelPattern = *PixelData;

      if (PixelPattern != 0)
      {
#ifdef GuiConst_ADV_FONTS_ON
        if (Font->ColorDepth == 4)
        {
          for (PixN = 0; PixN < 2; PixN++)
          {
            if (PixN == 0)
              PixelShade = PixelPattern & 0x0F;
            else
              PixelShade = (PixelPattern & 0xF0) >> 4;
            if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                (Bx + PixN >= sgl.ClippingX1) && (Bx + PixN <= sgl.ClippingX2) &&
#endif
               (PixelShade > 0))
            {
              PixelShadeInv = 15-PixelShade;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
              Xb = ((Bx + PixN) / 2) - (2 * (((Bx + PixN) / 2) & 0x01)) + 1;
#else
              Xb = (Bx + PixN) / 2;
#endif
              Y2 = Y;
              PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Xb;

#ifdef GuiConst_BIT_TOPLEFT
              if (((Bx + PixN) % 2) == 0)
                BitOffset = 0;
              else
                BitOffset = 4;
#else
              if (((Bx + PixN) % 2) == 0)
                BitOffset = 4;
              else
                BitOffset = 0;
#endif
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                {
                  if (PixelShade == 0x0F)
                    *PixelPtr = (*PixelPtr &
                       ~LinePattern2Pixel[(Bx + PixN) % 2]) |
                       (Color << BitOffset);
                  else
                  {
                    PixelColor = (*PixelPtr >> BitOffset) & 0x0F;
                    *PixelPtr = (*PixelPtr &
                       ~LinePattern2Pixel[(Bx + PixN) % 2]) |
                       (((PixelShade * Color + PixelShadeInv * PixelColor) /
                       15) << BitOffset);
                  }
                }

                Y2++;
                PixelPtr += sgl.CurLayerLineSize;
              }
            }
          }
        }
        else
#endif
        {
          for (PixN = 0; PixN < 8; PixN++)
          {
            if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                (Bx + PixN >= sgl.ClippingX1) && (Bx + PixN <= sgl.ClippingX2) &&
#endif
#ifdef GuiConst_BIT_TOPLEFT
               ((PixelPattern >> PixN) & 0x01))
#else
               ((PixelPattern >> (7-PixN)) & 0x01))
#endif
            {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
              Xb = ((Bx + PixN) / 2) - (2 * (((Bx + PixN) / 2) & 0x01)) + 1;
#else
              Xb = (Bx + PixN) / 2;
#endif
              Y2 = Y;
              PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Xb;

#ifdef GuiConst_BIT_TOPLEFT
              if (((Bx + PixN) % 2) == 0)
                Pattern = Color;
              else
                Pattern = Color << 4;
#else
              if (((Bx + PixN) % 2) == 0)
                Pattern = Color << 4;
              else
                Pattern = Color;
#endif
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                  *PixelPtr = (*PixelPtr &
                     ~LinePattern2Pixel[(Bx + PixN) % 2]) | Pattern;

                Y2++;
                PixelPtr += sgl.CurLayerLineSize;
              }
            }
          }
        }
      }

      PixelData++;
#ifdef GuiConst_ADV_FONTS_ON
      if (Font->ColorDepth == 4)
        Bx+=2;
      else
#endif
        Bx+=8;
    }

#ifdef GuiConst_FONT_UNCOMPRESSED
    PY++;
    Y++;
#else
    Y += LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT16S ByteSizeX;
  GuiConst_INT8U PrefixRom *PixelDataPtr2;
  GuiConst_INT8U B;
  GuiConst_INT16S X1b, X2b, Xb, Xbyte;
  GuiConst_INT16S Count;
  GuiConst_INT8U BitOffsetX1, BitOffsetX2;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  SizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  ByteSizeX = (SizeX + 1) / 2;

#ifdef GuiConst_ROTATED_90DEGREE
  sgl.BitmapWriteX2 = X + SizeY - 1;
  sgl.BitmapWriteY2 = Y + SizeX - 1;
#else
  sgl.BitmapWriteX2 = X + SizeX - 1;
  sgl.BitmapWriteY2 = Y + SizeY - 1;
#endif

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST_TRANSP(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
      return;
    if (X1 < X)
      X1 = X;
    if (X2 > X + SizeX - 1)
      X2 = X + SizeX - 1;
    if (Y1 < Y)
      Y1 = Y;
    if (Y2 > Y + SizeY - 1)
      Y2 = Y + SizeY - 1;
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

    if (X < 0)
      Xb = -((1 - X) / 2);
    else
      Xb = X / 2;
    X1b = X1 / 2;
    X2b = X2 / 2;

    PixelDataPtr += ByteSizeX * (Y1 - Y) + X1b - Xb;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    Xb  = (Xb  % 2) ? Xb  - 1 : Xb  + 1;
    X1b = (X1b % 2) ? X1b - 1 : X1b + 1;
    X2b = (X2b % 2) ? X2b - 1 : X2b + 1;
#endif

    BitOffsetX1 = X1 % 2;
    BitOffsetX2 = X2 % 2;

    while (Y1 <= Y2)
    {
      PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
      if (X % 2 == 0)
      {
        if (X1b == X2b)
          *PixelPtr1 = (*PixelPtr1 &
             ~(LinePattern2Left[BitOffsetX1] &
             LinePattern2Right[BitOffsetX2])) |
             (*PixelDataPtr &
             (LinePattern2Left[BitOffsetX1] & LinePattern2Right[BitOffsetX2]));
        else
        {
          PixelDataPtr2 = PixelDataPtr;
          PixelPtr2 = PixelPtr1;

          *PixelPtr2 =
             (*PixelPtr2 & ~LinePattern2Left[BitOffsetX1]) |
             (*PixelDataPtr & LinePattern2Left[BitOffsetX1]);
          PixelDataPtr2++;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
      Count = X1b;
          if (Count % 2)
            PixelPtr2--;
          else
            PixelPtr2 += 3;
          Count++;
          for (Xbyte = X1 / 2 + 1; Xbyte < X2 / 2; Xbyte++)
          {
            *PixelPtr2 = *PixelDataPtr2;
            if (Count % 2)
              PixelPtr2--;
            else
              PixelPtr2 += 3;
            Count++;
#else
          PixelPtr2++;
          for (Xbyte = X1b + 1; Xbyte < X2b; Xbyte++)
          {
            *PixelPtr2 = *PixelDataPtr2;
            PixelPtr2++;
#endif
            PixelDataPtr2++;
          }

          *PixelPtr2 =
             (*PixelPtr2 & ~LinePattern2Right[BitOffsetX2]) |
             (*PixelDataPtr2 & LinePattern2Right[BitOffsetX2]);
        }
      }
      else
      {
        PixelDataPtr2 = PixelDataPtr;
        PixelPtr2 = PixelPtr1;

        if (X1b == Xb)
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*PixelDataPtr2) & 0x0F) << 4;
#else
          B = (*PixelDataPtr2) >> 4;
#endif
        else
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 1)) >> 4) +
              (((*PixelDataPtr2) & 0x0F) << 4);
#else
          B = (((*(PixelDataPtr2 - 1)) & 0x0F) << 4) +
              ((*PixelDataPtr2) >> 4);
#endif
        *PixelPtr2 =
           (*PixelPtr2 & ~LinePattern2Left[BitOffsetX1]) |
           (B & LinePattern2Left[BitOffsetX1]);
        PixelPtr2++;
        PixelDataPtr2++;

        if (X2b > X1b)
        {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
          Count = X1b;
          if ((Count % 2))
            PixelPtr2 -= 2;
          else
            PixelPtr2 += 2;
          Count++;
          for (Xbyte = X1 / 2 + 1; Xbyte < X2 / 2; Xbyte++)
          {
#else
          for (Xbyte = X1b + 1; Xbyte < X2b; Xbyte++)
          {
#endif
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 1)) >> 4) +
                (((*PixelDataPtr2) & 0x0F) << 4);
#else
            B = (((*(PixelDataPtr2 - 1)) & 0x0F) << 4) +
                ((*PixelDataPtr2) >> 4);
#endif
            *PixelPtr2 = B;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
            if ((Count % 2))
              PixelPtr2--;
            else
              PixelPtr2 += 3;
            Count++;
#else
            PixelPtr2++;
#endif
            PixelDataPtr2++;
          }

#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 1)) >> 4) +
              (((*PixelDataPtr2) & 0x0F) << 4);
#else
          B = (((*(PixelDataPtr2 - 1)) & 0x0F) << 4) +
              ((*PixelDataPtr2) >> 4);
#endif
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePattern2Right[BitOffsetX2]) |
             (B & LinePattern2Right[BitOffsetX2]);
        }
      }

      PixelDataPtr += ByteSizeX;
      Y1++;
      PixelPtr1 += sgl.CurLayerLineSize;
    }
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S X, X1b, X2b, Xb;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    PixelPtr1 =
       sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1 - (2 * (X1 % 2)) + 1;
#else
    PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + (X1/2);
#endif

    while (Y1 <= Y2)
    {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
      X1b = (X1 / 2) - (2 * ((X1 / 2) % 2)) + 1;
      X2b = (X2 / 2) - (2 * ((X2 / 2) % 2)) + 1;
#else
      X1b = X1 / 2;
      X2b = X2 / 2;
#endif

      if (X1b == X2b)
        *PixelPtr1 ^=
           LinePattern2Left[X1 % 2] & LinePattern2Right[X2 % 2];
      else
      {
        PixelPtr2 = PixelPtr1;
        *PixelPtr2 ^= LinePattern2Left[X1 % 2];

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
        if (X1b % 2 == 0)
          PixelPtr2--;
        else
          PixelPtr2 += 3;
        for (X = X1 / 2 + 1; X < X2 / 2; X++)
        {
          *PixelPtr2 = ~*PixelPtr2;
          if (X % 2 == 0)
            PixelPtr2 += 3;
          else
            PixelPtr2--;
#else
        PixelPtr2++;
        for (Xb = X1b + 1; Xb < X2b; Xb++)
        {
          *PixelPtr2 = ~*PixelPtr2;
          PixelPtr2++;
#endif
        }

        *PixelPtr2 ^= LinePattern2Right[X2 % 2];
      }

      Y1++;
      PixelPtr1 += sgl.CurLayerLineSize;
    }
  }
}

