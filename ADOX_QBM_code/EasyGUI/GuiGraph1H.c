/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                   Monochrome graphics primitives library                 */
/*                      Horizontal display bytes layout                     */
/*                               v6.0.8.004                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
GuiConst_INT8U
   GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];

#ifdef GuiConst_CLIPPING_SUPPORT_ON
GuiConst_INT8U ClippingXPattern[GuiConst_BYTES_PR_LINE];
#endif
const GuiConst_INT8U LinePattern8Left[8] =
#ifdef GuiConst_BIT_TOPLEFT
   {0xFF, 0xFE, 0xFC, 0xF8, 0xF0, 0xE0, 0xC0, 0x80};
#else
   {0xFF, 0x7F, 0x3F, 0x1F, 0x0F, 0x07, 0x03, 0x01};
#endif
const GuiConst_INT8U LinePattern8Right[8] =
#ifdef GuiConst_BIT_TOPLEFT
   {0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0xFF};
#else
   {0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF};
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
static GuiConst_INT8U ClippingXOff;
#endif

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S C;
  GuiConst_INT16S ClippingMinXb;
  GuiConst_INT16S ClippingMaxXb;

  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > sgl.CurLayerWidth - 1)
    X2 = sgl.CurLayerWidth - 1;
  if (Y2 > sgl.CurLayerHeight - 1)
    Y2 = sgl.CurLayerHeight - 1;

  sgl.ClippingX1 = X1;
  sgl.ClippingY1 = Y1;
  sgl.ClippingX2 = X2;
  sgl.ClippingY2 = Y2;
  ClippingXOff = ((X1 == 0) && (X2 == sgl.CurLayerWidth - 1));

  if (!ClippingXOff)
  {
    ClippingMinXb = sgl.ClippingX1 / 8;
    ClippingMaxXb = sgl.ClippingX2 / 8;

    for (C = 0; C < ClippingMinXb; C++)
      ClippingXPattern[C] = 0x00;
    ClippingXPattern[ClippingMinXb] = LinePattern8Left[sgl.ClippingX1 % 8];
    for (C = ClippingMinXb + 1; C < ClippingMaxXb; C++)
      ClippingXPattern[C] = 0xFF;
    ClippingXPattern[ClippingMaxXb] = LinePattern8Right[sgl.ClippingX2 % 8];
    for (C = ClippingMaxXb + 1; C < GuiConst_BYTES_PR_LINE; C++)
      ClippingXPattern[C] = 0x00;
  }
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

  X1 = X1 / 8;
  X2 = X2 / 8;

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(sgl.CurLayerBufPtr, 0x00, sgl.CurLayerBytes);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
    PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 8);
#ifdef GuiConst_BIT_TOPLEFT
    if (Color)
      *PixelPtr |= (0x01 << (X % 8));
    else
      *PixelPtr &= ~(0x01 << (X % 8));
#else
    if (Color)
      *PixelPtr |= (0x80 >> (X % 8));
    else
      *PixelPtr &= ~(0x80 >> (X % 8));
#endif

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  if ((X < 0) || (X >= sgl.CurLayerWidth) ||
      (Y < 0) || (Y >= sgl.CurLayerHeight))
    return(GuiConst_PIXEL_OFF);
  else
  {
#ifdef GuiConst_BIT_TOPLEFT
    if (*(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 8)) & (0x01 << (X % 8)))
#else
    if (*(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 8)) & (0x80 >> (X % 8)))
#endif
      return(GuiConst_PIXEL_ON);
    else
      return(GuiConst_PIXEL_OFF);
  }
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, X1b, X2b;
  GuiConst_INT8U *PixelPtr;

  X1b = X1 / 8;
  X2b = X2 / 8;

  PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X1b;
  if (Color)
  {
    if (X1b == X2b)
      *PixelPtr |= LinePattern8Left[X1 % 8] & LinePattern8Right[X2 % 8];
    else
    {
      *PixelPtr |= LinePattern8Left[X1 % 8];
      PixelPtr++;

      for (X = X1b + 1; X < X2b; X++)
      {
        *PixelPtr = 0xFF;
        PixelPtr++;
      }

      *PixelPtr |= LinePattern8Right[X2 % 8];
    }
  }
  else
  {
    if (X1b == X2b)
      *PixelPtr &= ~(LinePattern8Left[X1 % 8] & LinePattern8Right[X2 % 8]);
    else
    {
      *PixelPtr &= ~LinePattern8Left[X1 % 8];
      PixelPtr++;

      for (X = X1b + 1; X < X2b; X++)
      {
        *PixelPtr = 0x00;
        PixelPtr++;
      }

      *PixelPtr &= ~LinePattern8Right[X2 % 8];
    }
  }
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT8U Pattern;
  GuiConst_INT8U *PixelPtr;

  PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + (X / 8);
  if (Color)
  {
#ifdef GuiConst_BIT_TOPLEFT
    Pattern = 0x01 << (X % 8);
#else
    Pattern = 0x80 >> (X % 8);
#endif
    while (Y1 <= Y2)
    {
      *PixelPtr |= Pattern;
      PixelPtr += sgl.CurLayerLineSize;
      Y1++;
    }
  }
  else
  {
#ifdef GuiConst_BIT_TOPLEFT
    Pattern = ~(0x01 << (X % 8));
#else
    Pattern = ~(0x80 >> (X % 8));
#endif
    while (Y1 <= Y2)
    {
      *PixelPtr &= Pattern;
      PixelPtr += sgl.CurLayerLineSize;
      Y1++;
    }
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT8S BitOffset1, BitOffset2;
  GuiConst_INT8U PixelByte1Visible, PixelByte2Visible;
  GuiConst_INT8U PixelPattern, PixelPattern1, PixelPattern2;
  GuiConst_INT16S N;
  GuiConst_INT8U YHeight;
  GuiConst_INT16S Bx;
  GuiConst_INT16S PY, Y2;
  GuiConst_INT8U PixelLineSize;
  GuiConst_INT16S CoordIncrement;
  GuiConst_INT8U *PixelPtr;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (sgl.ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != sgl.CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       sgl.GuiLib_RemoteFontBuffer);
    sgl.CurRemoteFont = CharNdx;
  }
  CharPtr = &sgl.GuiLib_RemoteFontBuffer[0];
#endif

  if ((ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

  gl.Dummy1_8U = Font->LineSize;   // To avoid compiler warning
#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  YHeight = Font->XSize;
  #else
  YHeight = Font->YSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
    PixelLineSize = ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
    YHeight = ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #else
    PixelLineSize = ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS);
    YHeight = ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = (YHeight + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
  PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  if (GuiLib_DisplayUpsideDown)
    X -= 7;
#else
#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif
#endif

#else

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  if (GuiLib_DisplayUpsideDown)
  {
    X -= 7 + ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
    Y -= ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
  }
  else
  {
    X += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
    Y += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
  }
#else
#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
      X += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
      X += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
      X += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
      X += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
      Y -= (ReadBytePtr(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
      X += ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
      X -= (ReadBytePtr(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        ReadBytePtr(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
      Y += ReadBytePtr(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif
#endif

#endif

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  if (GuiLib_DisplayUpsideDown)
    CoordIncrement = -1;
  else
    CoordIncrement = 1;
#else
  CoordIncrement = 1;
#endif

  BitOffset1 = X % 8;
  if (X < 0)
    BitOffset1 += 8;
  BitOffset2 = 8 - BitOffset1;

  PY = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = ReadBytePtr(LineCtrl);
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PY < YHeight)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PY >= YHeight - 1));

      PY++;
      if (PY % 8 == 7)
      {
        LineCtrlByte = ReadBytePtr(LineCtrl);
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

    Bx = X / 8;
    if (X < 0)
      Bx--;

    for (N = 0; N < PixelLineSize; N++)
    {
      PixelPattern = ReadBytePtr(PixelData);
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
      if (GuiLib_DisplayUpsideDown)
        GuiLib_MIRROR_BITS(PixelPattern);
#endif
      if (PixelPattern != 0)
      {
        if (!BitOffset1)
        {
          PixelByte1Visible = ((Bx >= 0) && (Bx < GuiConst_BYTES_PR_LINE));
          if (PixelByte1Visible)
          {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
            if (!ClippingXOff)
              PixelPattern = PixelPattern & ClippingXPattern[Bx];
#endif

            Y2 = Y;
            PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Bx;
            if (Color)
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                  *PixelPtr |= PixelPattern;
                Y2 += CoordIncrement;
                PixelPtr += CoordIncrement * sgl.CurLayerLineSize;
              }
            else
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                  *PixelPtr &= ~PixelPattern;
                Y2 += CoordIncrement;
                PixelPtr += CoordIncrement * sgl.CurLayerLineSize;
              }
          }
        }
        else
        {
          PixelByte1Visible = ((Bx >= 0) && (Bx < GuiConst_BYTES_PR_LINE));
          PixelByte2Visible =
             ((Bx + 1 >= 0) && (Bx + 1 < GuiConst_BYTES_PR_LINE));
#ifdef GuiConst_BIT_TOPLEFT
          PixelPattern1 = PixelPattern << BitOffset1;
          PixelPattern2 = PixelPattern >> BitOffset2;
#else
          PixelPattern1 = PixelPattern >> BitOffset1;
          PixelPattern2 = PixelPattern << BitOffset2;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
          if (!ClippingXOff)
          {
            PixelPattern1 = PixelPattern1 & ClippingXPattern[Bx];
            PixelPattern2 = PixelPattern2 & ClippingXPattern[Bx + 1];
          }
#endif

          Y2 = Y;
          PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Bx;
          if (Color)
#ifndef GuiConst_FONT_UNCOMPRESSED
            for (M = 0; M < LineRepeat; M++)
#endif
            {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
              if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
              {
                if (PixelByte1Visible)
                  *PixelPtr |= PixelPattern1;
                if (PixelByte2Visible)
                  *(PixelPtr + 1) |= PixelPattern2;
              }
              Y2 += CoordIncrement;
              PixelPtr += CoordIncrement * sgl.CurLayerLineSize;
            }
          else
#ifndef GuiConst_FONT_UNCOMPRESSED
            for (M = 0; M < LineRepeat; M++)
#endif
            {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
              if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
              {
                if (PixelByte1Visible)
                  *PixelPtr &= ~PixelPattern1;
                if (PixelByte2Visible)
                  *(PixelPtr + 1) &= ~PixelPattern2;
              }
              Y2 += CoordIncrement;
              PixelPtr += CoordIncrement * sgl.CurLayerLineSize;
            }
        }
      }

      PixelData++;
      Bx += CoordIncrement;
    }
#ifdef GuiConst_FONT_UNCOMPRESSED
    PY++;
    Y += CoordIncrement;
#else
    Y += CoordIncrement * LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT16S ByteX, LineY;
  GuiConst_INT8U PrefixRom *PixelDataPtr2;
  GuiConst_INT8U B;
  GuiConst_INT16S ByteSizeX;
  GuiConst_INT16S LineSizeY;
  GuiConst_INT16S X1b, X2b, Xb, XbCount;
  GuiConst_INT8U BitOffset1a, BitOffset1b;
  GuiConst_INT8U BitOffsetX1, BitOffsetX2;
  GuiConst_INT16S CoordIncrement;
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  if (GuiLib_DisplayUpsideDown)
    CoordIncrement = -1;
  else
    CoordIncrement = 1;
#else
  CoordIncrement = 1;
#endif

  SizeX = (GuiConst_INT16S)ReadBytePtr(PixelDataPtr);
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)ReadBytePtr(PixelDataPtr);
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)ReadBytePtr(PixelDataPtr);
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)ReadBytePtr(PixelDataPtr);
  PixelDataPtr++;
  ByteSizeX = (SizeX + 7) / 8;

#ifdef GuiConst_ROTATED_90DEGREE
  sgl.BitmapWriteX2 = X + SizeY - 1;
  sgl.BitmapWriteY2 = Y + SizeX - 1;
#else
  sgl.BitmapWriteX2 = X + SizeX - 1;
  sgl.BitmapWriteY2 = Y + SizeY - 1;
#endif

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST_TRANSP(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
  if (BitmapType == GuiLib_FULL_BITMAP)
  {
    if (GuiLib_DisplayUpsideDown)
    {
      X -= SizeX - 1;
      Y -= SizeY - 1;
    }
  }
#else
#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if (CoordIncrement == 1)
    {
      if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
        return;

     if (X1 < X)
        X1 = X;
      if (X2 > X + SizeX - 1)
        X2 = X + SizeX - 1;
      if (Y1 < Y)
        Y1 = Y;
      if (Y2 > Y + SizeY - 1)
        Y2 = Y + SizeY - 1;
    }
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
    else
    {
      if ((X1 < X - SizeX - 1) || (X2 > X) || (Y1 < Y - SizeY - 1) || (Y2 > Y))
        return;

     if (X2 > X)
        X2 = X;
      if (X1 < X - SizeX - 1)
        X2 = X - SizeX - 1;
      if (Y2 > Y)
        Y2 = Y;
      if (Y1 < Y - SizeY - 1)
        Y2 = Y - SizeY - 1;
    }
#endif
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
    if (CoordIncrement == -1)
    {
      X = X2;
      Y = Y2;
    }
#endif
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

    LineSizeY = Y2 - Y1 + 1;

    X1b = X1 / 8;
    X2b = X2 / 8;
    XbCount = X2b - X1b + 1;

    if (CoordIncrement == 1)
    {
      PixelDataPtr += ByteSizeX * (Y1 - Y) + X1b - (X / 8);

      BitOffset1a = X % 8;
      if (X < 0)
        BitOffset1a += 8;
      BitOffset1b = 8 - BitOffset1a;
      BitOffsetX1 = X1 % 8;
      BitOffsetX2 = X2 % 8;
    }
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
    else
    {
      PixelDataPtr += ByteSizeX * (Y - Y2) - X2b + (X / 8);

      BitOffset1a = 7 - (X % 8);
      if (X < 0)
        BitOffset1a += 8;
      BitOffset1b = 8 - BitOffset1a;
      BitOffsetX1 = X1 % 8;
      BitOffsetX2 = X2 % 8;
    }

    if (CoordIncrement == -1)
      Y1 = Y2;
#endif

    for (LineY = 1; LineY <= LineSizeY; LineY++)
    {
      if (!BitOffset1a)
      {
        if (X1b == X2b)
        {
          PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
          B = ReadBytePtr(PixelDataPtr);
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          if (GuiLib_DisplayUpsideDown)
            GuiLib_MIRROR_BITS(B);
#endif
          if (CoordIncrement == 1)
            *PixelPtr =
               (*PixelPtr &
               ~(LinePattern8Left[BitOffsetX1] &
               LinePattern8Right[BitOffsetX2])) |
               (B & (LinePattern8Left[BitOffsetX1] &
               LinePattern8Right[BitOffsetX2]));
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            *PixelPtr =
               (*PixelPtr &
               ~(LinePattern8Right[BitOffsetX2] &
               LinePattern8Left[BitOffsetX1])) |
               (B & (LinePattern8Right[BitOffsetX2] &
               LinePattern8Left[BitOffsetX1]));
#endif
        }
        else
        {
          PixelDataPtr2 = PixelDataPtr;
          if (CoordIncrement == 1)
            Xb = X1b;
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            Xb = X2b;
#endif
          PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + Xb;
          B = ReadBytePtr(PixelDataPtr2);

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          if (GuiLib_DisplayUpsideDown)
            GuiLib_MIRROR_BITS(B);
#endif
          if (CoordIncrement == 1)
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Left[BitOffsetX1]) |
               (B & LinePattern8Left[BitOffsetX1]);
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Right[BitOffsetX2]) |
               (B & LinePattern8Right[BitOffsetX2]);
#endif
          PixelDataPtr2++;
          PixelPtr += CoordIncrement;

          for (ByteX = 1; ByteX < XbCount - 1; ByteX++)
          {
            B = ReadBytePtr(PixelDataPtr2);

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
            if (GuiLib_DisplayUpsideDown)
              GuiLib_MIRROR_BITS(B);
#endif
            *PixelPtr = B;

            PixelDataPtr2++;
            PixelPtr += CoordIncrement;
          }

          B = ReadBytePtr(PixelDataPtr2);

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          if (GuiLib_DisplayUpsideDown)
            GuiLib_MIRROR_BITS(B);
#endif
          if (CoordIncrement == 1)
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Right[BitOffsetX2]) |
               (B & LinePattern8Right[BitOffsetX2]);
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Left[BitOffsetX1]) |
               (B & LinePattern8Left[BitOffsetX1]);
#endif
        }
      }
      else
      {
        if (X1b == X2b)
        {
          PixelDataPtr2 = PixelDataPtr;

          if (X1b == (X/8))
#ifdef GuiConst_BIT_TOPLEFT
            B = (ReadBytePtr(PixelDataPtr2) << BitOffset1a);
#else
            B = (ReadBytePtr(PixelDataPtr2) >> BitOffset1a);
#endif
          else
#ifdef GuiConst_BIT_TOPLEFT
            B = (ReadBytePtr(PixelDataPtr2 - 1) >> BitOffset1b) +
                (ReadBytePtr(PixelDataPtr2) << BitOffset1a);
#else
            B = (ReadBytePtr(PixelDataPtr2 - 1) << BitOffset1b) +
                (ReadBytePtr(PixelDataPtr2) >> BitOffset1a);
#endif

#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          if (GuiLib_DisplayUpsideDown)
            GuiLib_MIRROR_BITS(B);
#endif
          PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
          if (CoordIncrement == 1)
            *PixelPtr =
               (*PixelPtr &
               ~(LinePattern8Left[BitOffsetX1] &
               LinePattern8Right[BitOffsetX2])) |
               (B & (LinePattern8Left[BitOffsetX1] &
               LinePattern8Right[BitOffsetX2]));
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            *PixelPtr =
               (*PixelPtr &
               ~(LinePattern8Right[BitOffsetX2] &
               LinePattern8Left[BitOffsetX1])) |
               (B & (LinePattern8Right[BitOffsetX2] &
               LinePattern8Left[BitOffsetX1]));
#endif
        }
        else
        {
          PixelDataPtr2 = PixelDataPtr;
          if (CoordIncrement == 1)
            Xb = X1b;
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            Xb = X2b;
#endif

          if (Xb == (X/8))
#ifdef GuiConst_BIT_TOPLEFT
            B = (ReadBytePtr(PixelDataPtr2) << BitOffset1a);
#else
            B = (ReadBytePtr(PixelDataPtr2) >> BitOffset1a);
#endif
          else
#ifdef GuiConst_BIT_TOPLEFT
            B = (ReadBytePtr(PixelDataPtr2 - 1) >> BitOffset1b) +
                (ReadBytePtr(PixelDataPtr2) << BitOffset1a);
#else
            B = (ReadBytePtr(PixelDataPtr2 - 1) << BitOffset1b) +
                (ReadBytePtr(PixelDataPtr2) >> BitOffset1a);
#endif

          PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + Xb;
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          if (GuiLib_DisplayUpsideDown)
            GuiLib_MIRROR_BITS(B);
#endif
          if (CoordIncrement == 1)
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Left[BitOffsetX1]) |
               (B & LinePattern8Left[BitOffsetX1]);
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Right[BitOffsetX2]) |
               (B & LinePattern8Right[BitOffsetX2]);
#endif
          PixelDataPtr2++;
          PixelPtr += CoordIncrement;

          for (ByteX = 1; ByteX < XbCount - 1; ByteX++)
          {
#ifdef GuiConst_BIT_TOPLEFT
            B = ((ReadBytePtr(PixelDataPtr2 - 1)) >> BitOffset1b) +
                (ReadBytePtr(PixelDataPtr2) << BitOffset1a);
#else
            B = ((ReadBytePtr(PixelDataPtr2 - 1)) << BitOffset1b) +
                (ReadBytePtr(PixelDataPtr2) >> BitOffset1a);
#endif
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
            if (GuiLib_DisplayUpsideDown)
              GuiLib_MIRROR_BITS(B);
#endif
            *PixelPtr = B;
            PixelDataPtr2++;
            PixelPtr += CoordIncrement;
          }

#ifdef GuiConst_BIT_TOPLEFT
          B = ((ReadBytePtr(PixelDataPtr2 - 1)) >> BitOffset1b) +
              (ReadBytePtr(PixelDataPtr2) << BitOffset1a);
#else
          B = ((ReadBytePtr(PixelDataPtr2 - 1)) << BitOffset1b) +
              (ReadBytePtr(PixelDataPtr2) >> BitOffset1a);
#endif
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          if (GuiLib_DisplayUpsideDown)
            GuiLib_MIRROR_BITS(B);
#endif
          if (CoordIncrement == 1)
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Right[BitOffsetX2]) |
               (B & LinePattern8Right[BitOffsetX2]);
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
          else
            *PixelPtr =
               (*PixelPtr & ~LinePattern8Left[BitOffsetX1]) |
               (B & LinePattern8Left[BitOffsetX1]);
#endif
        }
      }

      PixelDataPtr += ByteSizeX;
      Y1 += CoordIncrement;
    }
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S X, X1b, X2b;
  GuiConst_INT8U *PixelPtr;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
    while (Y1 <= Y2)
    {
      X1b = X1 / 8;
      X2b = X2 / 8;

      PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
      if (X1b == X2b)
        *PixelPtr ^= LinePattern8Left[X1 % 8] & LinePattern8Right[X2 % 8];
      else
      {
        *PixelPtr ^= LinePattern8Left[X1 % 8];
        PixelPtr++;

        for (X = X1b + 1; X < X2b; X++)
        {
          *PixelPtr = ~*PixelPtr;
          PixelPtr++;
       }

        *PixelPtr ^= LinePattern8Right[X2 % 8];
      }

      Y1++;
    }
  }
}

