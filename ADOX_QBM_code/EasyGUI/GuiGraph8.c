/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*          8 bit (256 color/grayscale) graphics primitives library         */
/*                               v6.0.8.004                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
#ifdef GuiConst_DISPLAY_BUFFER_EASYGUI
GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > sgl.CurLayerWidth - 1)
    X2 = sgl.CurLayerWidth - 1;
  if (Y2 > sgl.CurLayerHeight - 1)
    Y2 = sgl.CurLayerHeight - 1;

  sgl.ClippingX1 = X1;
  sgl.ClippingY1 = Y1;
  sgl.ClippingX2 = X2;
  sgl.ClippingY2 = Y2;
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  X1 = 2 * (X1 / 2);
  X2 = 2 * (X2 / 2) + 1;
#endif

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;
    #ifdef GuiConst_VNC_REMOTE_SUPPORT_ON
    if ((GuiLib_VncRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_VncRepaint[Y1].ByteBegin))
      GuiLib_VncRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_VncRepaint[Y1].ByteEnd)
      GuiLib_VncRepaint[Y1].ByteEnd = X2;
    #endif

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(sgl.CurLayerBufPtr, GuiConst_PIXEL_OFF, sgl.CurLayerBytes);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    *((GuiConst_INT8U*)sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize +
       X - (2 * (X % 2)) + 1) = Color;
#else
    *((GuiConst_INT8U*)sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X) = Color;
#endif

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  if ((X < 0) || (X >= sgl.CurLayerWidth) ||
      (Y < 0) || (Y >= sgl.CurLayerHeight))
    return (0);
  else
  {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    return (*((GuiConst_INT8U*)sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize +
       X - (2 * (X % 2)) + 1));
#else
    return (*((GuiConst_INT8U*)sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X));
#endif
  }
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  GuiConst_INT8U *PixelPtr;
  GuiConst_INT16S X;

  for (X = X1; X <= X2; X++)
  {
    if (X % 2 == 0)
      PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X + 1;
    else
      PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X - 1;
    *PixelPtr = Color;
  }
#else
  memset((void*)(sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X1),
         Color,
         X2 - X1 + 1);
#endif
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT8U *PixelPtr;

  while (Y1 <= Y2)
  {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X - (2 * (X % 2)) + 1;
#else
    PixelPtr = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X;
#endif
    *PixelPtr = Color;
    Y1++;
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT8U PixelPattern;
  GuiConst_INT16S N;
  GuiConst_INT8U YHeight;
  GuiConst_INT8U PixN;
  GuiConst_INT16S Bx;
  GuiConst_INT16S PY, Y2;
  GuiConst_INT8U *PixelPtr;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
  GuiConst_INT16S Xb;
#endif
  GuiConst_INT8U PixelLineSize;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif
#ifdef GuiConst_ADV_FONTS_ON
  GuiConst_INT8U PixelShade, PixelShadeInv;
#ifndef GuiConst_COLOR_MODE_GRAY
  GuiConst_INTCOLOR PixelColor;
  GuiConst_INT8U PixelR, PixelG, PixelB;
  GuiConst_INT8U ColorR, ColorG, ColorB;
#endif
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (sgl.ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != sgl.CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       sgl.GuiLib_RemoteFontBuffer);
    sgl.CurRemoteFont = CharNdx;
  }
  CharPtr = &sgl.GuiLib_RemoteFontBuffer[0];
#endif

  if ((*(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);

  gl.Dummy1_8U = Font->LineSize;   // To avoid compiler warning
#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  YHeight = Font->XSize;
  #else
  YHeight = Font->YSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
  PixelLineSize = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #else
  PixelLineSize = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = (YHeight + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
#ifdef GuiConst_ADV_FONTS_ON
  if (Font->ColorDepth == 4)
    PixelLineSize = (PixelLineSize + 1) / 2;
  else
#endif
    PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif

#else

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif

#endif

#ifdef GuiConst_ADV_FONTS_ON
#ifndef GuiConst_COLOR_MODE_GRAY
  ColorR = (Color & GuiConst_COLORCODING_R_MASK) >>
     GuiConst_COLORCODING_R_START;
  ColorG = (Color & GuiConst_COLORCODING_G_MASK) >>
     GuiConst_COLORCODING_G_START;
  ColorB = (Color & GuiConst_COLORCODING_B_MASK) >>
     GuiConst_COLORCODING_B_START;
#endif
#endif

  PY = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = *LineCtrl;
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PY < YHeight)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PY >= YHeight - 1));

      PY++;
      if (PY % 8 == 7)
      {
        LineCtrlByte = *LineCtrl;
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

#ifdef GuiConst_ADV_FONTS_ON
    if (Font->ColorDepth == 4)
      Bx = X;
    else
#endif
      Bx = X;

    for (N = 0; N < PixelLineSize; N++)
    {
      PixelPattern = *PixelData;

      if (PixelPattern != 0)
      {
#ifdef GuiConst_ADV_FONTS_ON
        if (Font->ColorDepth == 4)
        {
          for (PixN = 0; PixN < 2; PixN++)
          {
            if (PixN == 0)
              PixelShade = PixelPattern & 0x0F;
            else
              PixelShade = (PixelPattern & 0xF0) >> 4;
            if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                (Bx + PixN >= sgl.ClippingX1) && (Bx + PixN <= sgl.ClippingX2) &&
#endif
               (PixelShade > 0))
            {
              PixelShadeInv = 15-PixelShade;
              Y2 = Y;
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
                  Xb = Bx + PixN;
#endif
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
                  PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize +
                             Xb - (2 * (Xb % 2)) + 1;
#else
                  PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Bx + PixN;
#endif
                  if (PixelShade == 0x0F)
                    *PixelPtr = Color;
                  else
                  {
#ifdef GuiConst_COLOR_MODE_GRAY
                    *PixelPtr =
                       (PixelShade * Color + PixelShadeInv * *PixelPtr) / 15;
#else
                    PixelColor = *PixelPtr;
                    PixelR = (PixelColor & GuiConst_COLORCODING_R_MASK) >>
                       GuiConst_COLORCODING_R_START;
                    PixelG = (PixelColor & GuiConst_COLORCODING_G_MASK) >>
                       GuiConst_COLORCODING_G_START;
                    PixelB = (PixelColor & GuiConst_COLORCODING_B_MASK) >>
                       GuiConst_COLORCODING_B_START;
                    PixelR =
                       (PixelShade * ColorR + PixelShadeInv * PixelR) / 15;
                    PixelG =
                       (PixelShade * ColorG + PixelShadeInv * PixelG) / 15;
                    PixelB =
                       (PixelShade * ColorB + PixelShadeInv * PixelB) / 15;
                    *PixelPtr =
                       (PixelR << GuiConst_COLORCODING_R_START) |
                       (PixelG << GuiConst_COLORCODING_G_START) |
                       (PixelB << GuiConst_COLORCODING_B_START);
#endif
                  }
                }
                Y2++;
              }
            }
          }
        }
        else
#endif
        {
          for (PixN = 0; PixN < 8; PixN++)
          {
            if (
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                (Bx + PixN >= sgl.ClippingX1) && (Bx + PixN <= sgl.ClippingX2) &&
#endif
               ((PixelPattern >> (7-PixN)) & 0x01))
            {
              Y2 = Y;
#ifndef GuiConst_FONT_UNCOMPRESSED
              for (M = 0; M < LineRepeat; M++)
#endif
              {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
                if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
                {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
                  Xb = Bx + PixN;
                  PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize +
                             Xb - (2 * (Xb % 2)) + 1;
#else
                  PixelPtr = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Bx + PixN;
#endif
                  *PixelPtr = Color;
                }
                Y2++;
              }
            }
          }
        }
      }

      PixelData++;
#ifdef GuiConst_ADV_FONTS_ON
      if (Font->ColorDepth == 4)
        Bx+=2;
      else
#endif
        Bx+=8;
    }

#ifdef GuiConst_FONT_UNCOMPRESSED
    PY++;
    Y++;
#else
    Y += LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;
#ifdef GuiConst_BITMAP_COMPRESSED
  GuiConst_INT8U Cnt;
  GuiConst_INT8U Diff;
  GuiConst_INT16U Offset;
  GuiConst_INT16S DX;
  GuiConst_INT16S I;
  GuiConst_INT8U *PixelPtr3;
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * RemPixelDataPtr;
   GuiConst_INT8U * LinePixelDataPtr;
#else
   GuiConst_INT8U PrefixRom * RemPixelDataPtr;
   GuiConst_INT8U PrefixRom * LinePixelDataPtr;
#endif
#endif

  SizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;

#ifdef GuiConst_ROTATED_90DEGREE
  sgl.BitmapWriteX2 = X + SizeY - 1;
  sgl.BitmapWriteY2 = Y + SizeX - 1;
#else
  sgl.BitmapWriteX2 = X + SizeX - 1;
  sgl.BitmapWriteY2 = Y + SizeY - 1;
#endif

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST_TRANSP(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
      return;
    if (X1 < X)
      X1 = X;
    if (X2 > X + SizeX - 1)
      X2 = X + SizeX - 1;
    if (Y1 < Y)
      Y1 = Y;
    if (Y2 > Y + SizeY - 1)
      Y2 = Y + SizeY - 1;
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

#ifdef GuiConst_BITMAP_COMPRESSED
    while (Y < Y1)
    {
      Offset = (GuiConst_INT16U)*PixelDataPtr;
      PixelDataPtr++;
      Offset += 256 * (GuiConst_INT16U)*PixelDataPtr;
      PixelDataPtr++;
      if (Offset != 0)
      {
        LinePixelDataPtr = PixelDataPtr;
        PixelDataPtr += Offset - 2;
      }

      Y++;
    }
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    PixelPtr1 =
       sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1 - (2 * (X1 % 2)) + 1;
#else
    PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1;
#endif
    DX = X1 - X;
    while (Y1 <= Y2)
    {
      PixelPtr2 = PixelPtr1;

      Offset = (GuiConst_INT16U)*PixelDataPtr;
      PixelDataPtr++;
      Offset += 256 * (GuiConst_INT16U)*PixelDataPtr;
      PixelDataPtr++;
      if (Offset == 0)
      {
        RemPixelDataPtr = PixelDataPtr;
        PixelDataPtr = LinePixelDataPtr;
      }
      else
        LinePixelDataPtr = PixelDataPtr;

      Cnt = 0;
      X = DX;
      while (X > 0)
      {
        Diff = *PixelDataPtr;
        Cnt = Diff & 0x7F;
        Diff >>= 7;
        PixelDataPtr++;

        if (X >= Cnt)
        {
          if (Diff)
            PixelDataPtr += Cnt;
          else
            PixelDataPtr++;
          X -= Cnt;
          Cnt = 0;
        }
        else
        {
          if (Diff)
            PixelDataPtr += X;
          Cnt -= X;
          X = 0;
        }
      }

      for (X = X1; X <= X2; X++)
      {
        if (Cnt == 0)
        {
          Diff = *PixelDataPtr;
          Cnt = Diff & 0x7F;
          Diff >>= 7;
          PixelDataPtr++;
        }

        X += Cnt - 1;
        if (X > X2)
          Cnt -= (X - X2);

        if (Diff)
        {
          if (TranspColor == -1)
          {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
            for (I = 0; I < Cnt; I++)
            {
              *PixelPtr3 = *(PixelDataPtr + I);
              if (X % 2 == 0)
                PixelPtr3 += 3;
              else
                PixelPtr3--;
            }
#else
            CopyBytes(PixelPtr2, PixelDataPtr, Cnt);
#endif
            PixelPtr2 += Cnt;
            PixelDataPtr += Cnt;
          }
          else
          {
            while (Cnt > 0)
            {
              if (*(GuiConst_INT8U*)PixelDataPtr == (GuiConst_INT8U)TranspColor)
                PixelPtr2++;
              else
              {
                *PixelPtr2 = *PixelDataPtr;
                PixelPtr2++;
              }

              Cnt--;
              PixelDataPtr++;
            }
          }
        }
        else
        {
          if ((TranspColor == -1) ||
             (*(GuiConst_INT8U*)PixelDataPtr != (GuiConst_INT8U)TranspColor))
          {
            while (Cnt > 0)
            {
              *PixelPtr2 = *PixelDataPtr;
              PixelPtr2++;

              Cnt--;
            }
          }
          else
            PixelPtr2 += Cnt;

          PixelDataPtr++;
        }
        if ((X > X2) && Diff)
          PixelDataPtr += (X - X2);
        Cnt = 0;
      }

      if (Offset == 0)
        PixelDataPtr = RemPixelDataPtr;
      else
        PixelDataPtr = LinePixelDataPtr + Offset - 2;

      Y1++;
      PixelPtr1 += sgl.CurLayerLineSize;
    }
#else

    PixelDataPtr += SizeX * (Y1 - Y) + (X1 - X);
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    PixelPtr1 =
       sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1 - (2 * (X1 % 2)) + 1;
#else
    PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1;
#endif

    while (Y1 <= Y2)
    {
      PixelPtr2 = PixelPtr1;
      if (TranspColor == -1)
      {
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
        for (X = X1; X <= X2; X++)
        {
          *PixelPtr2 = *(PixelDataPtr + X - X1);
          if (X % 2 == 0)
            PixelPtr2 += 3;
          else
            PixelPtr2--;
        }
#else
        CopyBytes(PixelPtr1, PixelDataPtr, X2 - X1 + 1);
#endif
        PixelDataPtr += SizeX;
      }
      else
      {
        for (X = X1; X <= X2; X++)
        {
          if (*PixelDataPtr != TranspColor)
            *PixelPtr2 = *PixelDataPtr;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
          if (X % 2 == 0)
            PixelPtr2 += 3;
          else
            PixelPtr2--;
#else
          PixelPtr2++;
#endif
          PixelDataPtr++;
        }
      }
      Y1++;
      PixelPtr1 += sgl.CurLayerLineSize;
    }
#endif
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S X;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);

#ifdef GuiConst_DISPLAY_BIG_ENDIAN
    PixelPtr1 =
       sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1 - (2 * (X1 % 2)) + 1;
#else
    PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1;
#endif

    while (Y1 <= Y2)
    {
      PixelPtr2 = PixelPtr1;
      for (X = X1; X <= X2; X++)
      {
        *PixelPtr2 = ~*PixelPtr2;
#ifdef GuiConst_DISPLAY_BIG_ENDIAN
        if (X % 2 == 0)
          PixelPtr2 += 3;
        else
          PixelPtr2--;
#else
        PixelPtr2++;
#endif
      }

      Y1++;
      PixelPtr1 += sgl.CurLayerLineSize;
    }
  }
}

