/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2012 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                               v6.0.4.003                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static GuiConst_INT8U GetItemByte(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT8U D;
#ifdef GuiConst_AVRGCC_COMPILER
  D = ReadBytePtr(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
#else
  D = **ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  return (D);
}
//------------------------------------------------------------------------------
static GuiConst_INT16S GetItemWord(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT16U D;

#ifdef GuiConst_AVRGCC_COMPILER
  D = ReadBytePtr(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x0100 * (GuiConst_INT16U)ReadBytePtr(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x0100 * (*ItemFlashPtr);
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x0100 * (*ItemFlashPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x0100 * (*ItemFlashPtr);
#else
  D = **ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x0100 * (GuiConst_INT16U)**ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  return ((GuiConst_INT16S) D);
}
//------------------------------------------------------------------------------
#ifdef GuiLib_COLOR_BYTESIZE_3
static GuiConst_INT32S GetItemTriple(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT32U D;

#ifdef GuiConst_AVRGCC_COMPILER
  D = ReadBytePtr(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)ReadBytePtr(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)ReadBytePtr(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
#else
  D = **ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)**ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)**ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  return ((GuiConst_INT32S) D);
}
#endif

#ifdef GuiLib_GETITEMLONG
//------------------------------------------------------------------------------
static GuiConst_INT32S GetItemLong(
   GuiConst_INT8U**ItemDataPtrPtr)
{
  GuiConst_INT32U D;

#ifdef GuiConst_AVRGCC_COMPILER
  D = ReadBytePtr(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)ReadBytePtr(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)ReadBytePtr(*ItemDataPtrPtr);
  (*ItemDataPtrPtr)++;
  D += 0x01000000 * (GuiConst_INT32U)ReadBytePtr(*ItemDataPtrPtr);
#else
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x01000000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_ICC_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr =
     (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x01000000 * (*ItemFlashPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
  GuiConst_INT8U PrefixRom * ItemFlashPtr;
  ItemFlashPtr = (GuiConst_INT8U PrefixRom *)*ItemDataPtrPtr;
  D = *ItemFlashPtr;
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00000100 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x00010000 * (*ItemFlashPtr);
  (*ItemDataPtrPtr)++;
  ItemFlashPtr++;
  D += 0x01000000 * (*ItemFlashPtr);
#else
  D = **ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00000100 * (GuiConst_INT32U)**ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x00010000 * (GuiConst_INT32U)**ItemDataPtrPtr;
  (*ItemDataPtrPtr)++;
  D += 0x01000000 * (GuiConst_INT32U)**ItemDataPtrPtr;
#endif
#endif
#endif
#endif
  (*ItemDataPtrPtr)++;
  return ((GuiConst_INT32S) D);
}
#endif

//------------------------------------------------------------------------------
#ifdef GuiLib_COLOR_BYTESIZE_1
#define GetItemColor GetItemByte
#endif
#ifdef GuiLib_COLOR_BYTESIZE_2
#define GetItemColor GetItemWord
#endif
#ifdef GuiLib_COLOR_BYTESIZE_3
#define GetItemColor GetItemTriple
#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static void DrawItem(
   GuiConst_INT8U ColorInvert)
{
  GuiConst_INT16U StructToCallIndex;
  GuiConst_INT32S VarValue;
  GuiConst_INT16S I, N;
#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
  GuiConst_INT16S RemYMemory;
#endif
#ifdef GuiConst_ITEM_BUTTON_INUSE
  GuiConst_INT8U DisabledGlyphColorInUse;
  GuiConst_INT8U DisabledTextColorInUse;
  GuiConst_INT16S CX, CY;
  GuiConst_INT16S RemX1, RemY1;
  GuiConst_INT16S GX1, GX2, GY1, GY2;
  GuiConst_INTCOLOR ButtonColor;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  GuiConst_INT16S RemClippingX1, RemClippingY1, RemClippingX2, RemClippingY2;
#endif // GuiConst_CLIPPING_SUPPORT_ON
#endif
  GuiConst_INT16U StrLen;
  TextParRec TempTextPar;
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8U IsCursorField;
  GuiConst_INT8U FoundActiveCursorField;
#endif
  GuiConst_INTCOLOR ForeColor;
  GuiConst_INTCOLOR BackColor;
//  GuiConst_INTCOLOR *ColVarPtr;
  GuiConst_INT8U BackColorTransp;
  GuiConst_INT32S BackColor2;
#ifndef GuiConst_CHARMODE_ANSI
  GuiConst_INT16U P;
#endif
  GuiConst_TEXT *CharPtr;
  GuiConst_INT16S X1, X2, Y1, Y2;
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  GuiConst_INT16S CX1, CX2, CY1, CY2;
  GuiConst_INT16S Y;
  GuiConst_INT16S SourceLayerIndexNo;
  GuiConst_INT16S DestLayerIndexNo;
  GuiConst_INT8U *DestAddress;
  GuiConst_INT16U DestLineSize;
  GuiConst_INT16S DestX;
  GuiConst_INT16S DestY;
  GuiConst_INT16U DestWidth;
  GuiConst_INT16U DestHeight;
  GuiConst_INT8U *SourceAddress;
  GuiConst_INT16U SourceLineSize;
  GuiConst_INT16S SourceX;
  GuiConst_INT16S SourceY;
  GuiConst_INT16U SourceWidth;
  GuiConst_INT16U SourceHeight;
  GuiConst_INT32S FilterPars[10];
#endif
#ifdef GuiConst_ITEM_STRUCTCOND_INUSE
  GuiConst_INT8U FirstRound;
#endif
#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  GuiConst_INT32S TouchSearch;
  GuiConst_INT32S TouchIndex;
#endif

#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  if (!sgl.InitialDrawing)
  {
    sgl.CoordOrigoX = sgl.CurItem.CoordOrigoX;
    sgl.CoordOrigoY = sgl.CurItem.CoordOrigoY;
  }
#endif
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (!sgl.InitialDrawing && sgl.DisplayWriting)
    GuiLib_SetClipping(sgl.CurItem.ClipRectX1, sgl.CurItem.ClipRectY1,
                       sgl.CurItem.ClipRectX2, sgl.CurItem.ClipRectY2);
#endif

  SetCurFont(sgl.CurItem.TextPar[0].FontIndex);

#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
#ifdef GuiConst_TEXTBOX_FIELDS_ON
  if ((sgl.InitialDrawing) &&
      (sgl.CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK) ||
      (sgl.CurItem.ItemType == GuiLib_ITEM_VARBLOCK))
  {
    sgl.CurItem.CompPars.CompTextBox.ScrollIndex = 0;
    sgl.CurItem.CompPars.CompTextBox.ScrollPos = 0;
  }
#endif
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
  IsCursorField = (sgl.CursorInUse && (sgl.CurItem.CursorFieldNo >= 0));
  FoundActiveCursorField =
     (IsCursorField && (GuiLib_ActiveCursorFieldNo == sgl.CurItem.CursorFieldNo));
  if (FoundActiveCursorField)
    sgl.CursorActiveFieldFound = 1;
#endif

  sgl.AutoRedrawSaveIndex = -1;

  if ((sgl.InitialDrawing) || (sgl.AutoRedrawUpdate == GuiLib_TRUE))
  {
#ifdef GuiConst_CURSOR_SUPPORT_ON
    if ((sgl.CurItem.CursorFieldNo >= 0) && (sgl.CursorFieldFound == -1))
      sgl.CursorFieldFound = sgl.CurItem.CursorFieldNo;

    if (IsCursorField)
    {
      sgl.CurItem.CursorFieldLevel++;

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
      if (sgl.NextScrollLineReading)
      {
        sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FIELDSCROLLBOX;
        sgl.CurItem.CursorScrollBoxIndex = sgl.GlobalScrollBoxIndex;
      }
      else
#endif
        sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_FIELDSCROLLBOX;

#ifndef GuiConst_CURSOR_FIELDS_OFF
      if (!(sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_AUTOREDRAWFIELD))
        sgl.AutoRedrawSaveIndex = AutoRedraw_InsertCursor(&sgl.CurItem, 0, sgl.DisplayLevel);
#endif
    }
#endif // GuiConst_CURSOR_SUPPORT_ON

    if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_AUTOREDRAWFIELD)
    {
      sgl.AutoRedrawSaveIndex = AutoRedraw_Insert(&sgl.CurItem, 0, sgl.DisplayLevel);

#ifdef GuiConst_CURSOR_SUPPORT_ON
      if (IsCursorField)
        AutoRedraw_SetAsCursor(sgl.AutoRedrawSaveIndex);
#endif // GuiConst_CURSOR_SUPPORT_ON
    }
  }

  switch (ColorInvert)
  {
    case GuiLib_COL_INVERT_OFF:
      sgl.SwapColors = 0;
      break;

    case GuiLib_COL_INVERT_ON:
      sgl.SwapColors = 1;
      break;

    case GuiLib_COL_INVERT_IF_CURSOR:
#ifdef GuiConst_CURSOR_SUPPORT_ON
      if (FoundActiveCursorField)
        sgl.SwapColors = 1;
#else
      sgl.SwapColors = 0;
#endif
      break;
  }
  if (sgl.SwapColors)
  {
    ForeColor = sgl.CurItem.BarForeColor;
    BackColor = sgl.CurItem.BarBackColor;
    BackColorTransp =
       ((sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_BARTRANSPARENT) > 0);
  }
  else
  {
    ForeColor = sgl.CurItem.ForeColor;
    BackColor = sgl.CurItem.BackColor;
    BackColorTransp =
       ((sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_TRANSPARENT) > 0);
  }

  sgl.CurItem.Drawn = 0;
  switch (sgl.CurItem.ItemType)
  {
    case GuiLib_ITEM_CLEARAREA:
      if (sgl.DisplayWriting)
      {
        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        X2 = sgl.CurItem.X2;
        Y2 = sgl.CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);
        if (!BackColorTransp)
          GuiLib_FillBox(X1, Y1, X2, Y2, BackColor);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }

      break;

    case GuiLib_ITEM_TEXT:
      if (sgl.DisplayWriting)
      {
        if (sgl.CurItem.TextPar[0].BackBoxSizeX > 0)
        {
          DrawBackBox(BackColor, BackColorTransp, 0);
          UpdateDrawLimits(sgl.BbX1, sgl.BbY1, sgl.BbX2, sgl.BbY2);
        }

        DRAW_ROM_TEXT(sgl.CurItem.TextPtr[0],
                      sgl.CurItem.TextLength[0],
                      0,
                      ForeColor,
                      BackColor,
                      BackColorTransp);

        UpdateDrawLimits(sgl.FontWriteX1, sgl.FontWriteY1, sgl.FontWriteX2, sgl.FontWriteY2);
      }

      break;

    case GuiLib_ITEM_TEXTBLOCK:
      if (sgl.DisplayWriting)
      {
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
        DRAW_ROM_TEXTBLOCK(sgl.CurItem.TextPtr[0],
                           sgl.CurItem.TextLength[0],
                           0,
                           ForeColor,
                           BackColor,
                           BackColorTransp);

        UpdateDrawLimits(sgl.CurItem.X1, sgl.CurItem.Y1, sgl.CurItem.X2, sgl.CurItem.Y2);
#endif
      }

      break;

    case GuiLib_ITEM_VAR:
      if (sgl.DisplayWriting)
      {
#ifdef GuiConst_DISP_VAR_NOW
        displayVarNow = 1;
#endif

        if (sgl.CurItem.TextPar[0].BackBoxSizeX > 0)
        {
          DrawBackBox(BackColor, BackColorTransp, 0);
          UpdateDrawLimits(sgl.BbX1, sgl.BbY1, sgl.BbX2, sgl.BbY2);
        }

        if (sgl.CurItem.VarPtr != 0)
        {
          if (sgl.CurItem.VarType == GuiLib_VAR_STRING)
          {
            CharPtr = (GuiConst_TEXT*)sgl.CurItem.VarPtr;
#ifdef GuiConst_CHARMODE_ANSI
            StrLen = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
            StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
            StrLen = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
          }
          else
          {
            VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);
            StrLen = DataNumStr(VarValue, sgl.CurItem.VarType, 0);
#ifdef GuiConst_CHARMODE_ANSI
            CharPtr = (GuiConst_TEXT *) sgl.VarNumTextStr;
#else
            for (P = 0; P <= StrLen; P++)
              sgl.VarNumUnicodeTextStr[P] = sgl.VarNumTextStr[P];
            CharPtr = (GuiConst_TEXT *) sgl.VarNumUnicodeTextStr;
#endif
          }

#ifdef GuiConst_CHARMODE_ANSI
          strcpy(sgl.AnsiTextBuf, CharPtr);
          DrawText(sgl.AnsiTextBuf,
                   StrLen,
                   0,
                   ForeColor,
                   BackColor,
                   BackColorTransp);
#else
          GuiLib_UnicodeStrCpy(sgl.UnicodeTextBuf, CharPtr);
          DrawText(sgl.UnicodeTextBuf,
                   StrLen,
                   0,
                   ForeColor,
                   BackColor,
                   BackColorTransp);
#endif
        }

#ifdef GuiConst_DISP_VAR_NOW
        displayVarNow = 0;
#endif

        UpdateDrawLimits(sgl.FontWriteX1, sgl.FontWriteY1, sgl.FontWriteX2, sgl.FontWriteY2);
      }

      break;

    case GuiLib_ITEM_VARBLOCK:
      if (sgl.DisplayWriting)
      {
#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
#ifdef GuiConst_DISP_VAR_NOW
        displayVarNow = 1;
#endif

        if (sgl.CurItem.VarPtr != 0)
        {
          if (sgl.CurItem.VarType == GuiLib_VAR_STRING)
          {
            CharPtr = (GuiConst_TEXT*)sgl.CurItem.VarPtr;
#ifdef GuiConst_CHARMODE_ANSI
            StrLen = strlen(CharPtr);
#else
#ifdef GuiConst_CODEVISION_COMPILER
            StrLen = GuiLib_UnicodeStrLen((GuiConst_TEXT*)CharPtr);
#else
            StrLen = GuiLib_UnicodeStrLen(CharPtr);
#endif
#endif
          }
          else
          {
            VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);
            StrLen = DataNumStr(VarValue, sgl.CurItem.VarType, 0);
#ifdef GuiConst_CHARMODE_ANSI
            CharPtr = (GuiConst_TEXT *) sgl.VarNumTextStr;
#else
            for (P = 0; P <= StrLen; P++)
              sgl.VarNumUnicodeTextStr[P] = sgl.VarNumTextStr[P];
            CharPtr = (GuiConst_TEXT *) sgl.VarNumUnicodeTextStr;
#endif
          }

#ifdef GuiConst_CHARMODE_ANSI
          strcpy(sgl.AnsiTextBuf, CharPtr);
          DrawTextBlock(sgl.AnsiTextBuf,
                        StrLen,
                        0,
                        ForeColor,
                        BackColor,
                        BackColorTransp);
#else
          GuiLib_UnicodeStrCpy(sgl.UnicodeTextBuf, CharPtr);
          DrawTextBlock(sgl.UnicodeTextBuf,
                        StrLen,
                        0,
                        ForeColor,
                        BackColor,
                        BackColorTransp);
#endif
        }

#ifdef GuiConst_DISP_VAR_NOW
        displayVarNow = 0;
#endif

        UpdateDrawLimits(sgl.CurItem.X1, sgl.CurItem.Y1, sgl.CurItem.X2, sgl.CurItem.Y2);
#endif
      }

      break;

    case GuiLib_ITEM_DOT:
      if (sgl.DisplayWriting)
      {
        GuiLib_Dot(sgl.CurItem.X1, sgl.CurItem.Y1, ForeColor);
        UpdateDrawLimits(sgl.CurItem.X1, sgl.CurItem.Y1, sgl.CurItem.X1, sgl.CurItem.Y1);
      }

      break;

    case GuiLib_ITEM_LINE:
      if (sgl.DisplayWriting)
      {
        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        X2 = sgl.CurItem.X2;
        Y2 = sgl.CurItem.Y2;
        if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_PATTERNEDLINE)
          GuiLib_LinePattern(X1, Y1, X2, Y2, sgl.CurItem.LinePattern, ForeColor);
        else
          GuiLib_Line(X1, Y1, X2, Y2, ForeColor);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }
      break;

    case GuiLib_ITEM_FRAME:
      if (sgl.DisplayWriting)
      {
        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        X2 = sgl.CurItem.X2;
        Y2 = sgl.CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);
        if (sgl.CurItem.FrameThickness == 0)
          N = sgl.ThicknessMemory;
        else
          N = sgl.CurItem.FrameThickness;
        DrawBorderBox(X1, Y1, X2, Y2, ForeColor, BackColor, BackColorTransp, N);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }

      break;

    case GuiLib_ITEM_ROUNDEDFRAME:
      if (sgl.DisplayWriting)
      {
        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        X2 = sgl.CurItem.X2;
        Y2 = sgl.CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);

        if (BackColorTransp)
          BackColor2 = GuiLib_NO_COLOR;
        else
          BackColor2 = BackColor;

        if (sgl.CurItem.R1 <= 0)
          DrawBorderBox(
             X1, Y1, X2, Y2,
             ForeColor, BackColor, BackColorTransp, 1);
        else
        {
          Ellipse(
             X1 + sgl.CurItem.R1, Y1 + sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, BackColor2, 0, 0, 1, 0);
          if (X2 - X1 + 1 - 2 * sgl.CurItem.R1 > 0)
          {
            GuiLib_HLine(X1 + sgl.CurItem.R1, X2 - sgl.CurItem.R1, Y1, ForeColor);
            if (!BackColorTransp)
              GuiLib_FillBox(
                 X1 + sgl.CurItem.R1, Y1 + 1, X2 - sgl.CurItem.R1, Y1 + sgl.CurItem.R1,
                 BackColor);
          }
          Ellipse(
             X2 - sgl.CurItem.R1, Y1 + sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, BackColor2, 0, 0, 0, 1);
          if (Y2 - Y1 + 1 - 2 * sgl.CurItem.R1 > 0)
          {
            GuiLib_VLine(X1, Y1 + sgl.CurItem.R1, Y2 - sgl.CurItem.R1, ForeColor);
            if (!BackColorTransp)
              GuiLib_FillBox(
                 X1 + 1, Y1 + sgl.CurItem.R1, X2 - 1, Y2 - sgl.CurItem.R1, BackColor);
            GuiLib_VLine(X2 , Y1 + sgl.CurItem.R1, Y2 - sgl.CurItem.R1, ForeColor);
          }
          Ellipse(
             X1 + sgl.CurItem.R1, Y2 - sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, BackColor2, 0, 1, 0, 0);
          if (X2 - X1 + 1 - 2 * sgl.CurItem.R1 > 0)
          {
            if (!BackColorTransp)
              GuiLib_FillBox(
                 X1 + sgl.CurItem.R1, Y2 - sgl.CurItem.R1, X2 - sgl.CurItem.R1, Y2 - 1,
                 BackColor);
            GuiLib_HLine(X1 + sgl.CurItem.R1, X2 - sgl.CurItem.R1, Y2, ForeColor);
          }
          Ellipse(
             X2 - sgl.CurItem.R1, Y2 - sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, BackColor2, 1, 0, 0, 0);
        }
      }
      break;

    case GuiLib_ITEM_BLOCK:
      if (sgl.DisplayWriting)
      {
        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        X2 = sgl.CurItem.X2;
        Y2 = sgl.CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);
        GuiLib_FillBox(X1, Y1, X2, Y2, ForeColor);
        UpdateDrawLimits(X1, Y1, X2, Y2);
      }

      break;

    case GuiLib_ITEM_ROUNDEDBLOCK:
      if (sgl.DisplayWriting)
      {
        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        X2 = sgl.CurItem.X2;
        Y2 = sgl.CurItem.Y2;
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);

        if (sgl.CurItem.R1 <= 0)
          GuiLib_FillBox(X1, Y1, X2, Y2, ForeColor);
        else
        {
          Ellipse(
             X1 + sgl.CurItem.R1, Y1 + sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, ForeColor, 0, 0, 1, 0);
          if (X2 - X1 + 1 - 2 * sgl.CurItem.R1 > 0)
            GuiLib_FillBox(
               X1 + sgl.CurItem.R1, Y1, X2 - sgl.CurItem.R1, Y1 + sgl.CurItem.R1,
               ForeColor);
          Ellipse(
             X2 - sgl.CurItem.R1, Y1 + sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, ForeColor, 0, 0, 0, 1);
          if (Y2 - Y1 + 1 - 2 * sgl.CurItem.R1 > 0)
            GuiLib_FillBox(X1, Y1 + sgl.CurItem.R1, X2, Y2 - sgl.CurItem.R1, ForeColor);
          Ellipse(
             X1 + sgl.CurItem.R1, Y2 - sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, ForeColor, 0, 1, 0, 0);
          if (X2 - X1 + 1 - 2 * sgl.CurItem.R1 > 0)
            GuiLib_FillBox(
               X1 + sgl.CurItem.R1, Y2 - sgl.CurItem.R1, X2 - sgl.CurItem.R1, Y2,
               ForeColor);
          Ellipse(
             X2 - sgl.CurItem.R1, Y2 - sgl.CurItem.R1, sgl.CurItem.R1, sgl.CurItem.R1,
             ForeColor, ForeColor, 1, 0, 0, 0);
        }
      }
      break;

    case GuiLib_ITEM_CIRCLE:
      if (sgl.DisplayWriting && (sgl.CurItem.R1 >= 0))
      {
        if (BackColorTransp)
          BackColor2 = GuiLib_NO_COLOR;
        else
          BackColor2 = BackColor;
        if ((sgl.CurItem.FrameThickness == 0xFF) &&
           !(sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_CIRCLE_IF))
        {
          Ellipse(sgl.CurItem.X1 + sgl.CurItem.R1,
                  sgl.CurItem.Y1,
                  sgl.CurItem.R1,
                  sgl.CurItem.R1,
                  ForeColor, BackColor2,
                  1, 1, 1, 1);
          UpdateDrawLimits(sgl.CurItem.X1,
                           sgl.CurItem.Y1 - sgl.CurItem.R1,
                           sgl.CurItem.X1 + 2 * sgl.CurItem.R1,
                           sgl.CurItem.Y1 + sgl.CurItem.R1);
        }
        else
        {
          Circle(sgl.CurItem.X1 + sgl.CurItem.R1,
                 sgl.CurItem.Y1,
                 sgl.CurItem.R1,
                 ForeColor,
                 sgl.CurItem.FrameThickness & 0x01,
                 sgl.CurItem.FrameThickness & 0x02,
                 sgl.CurItem.FrameThickness & 0x04,
                 sgl.CurItem.FrameThickness & 0x08,
                 sgl.CurItem.FrameThickness & 0x10,
                 sgl.CurItem.FrameThickness & 0x20,
                 sgl.CurItem.FrameThickness & 0x40,
                 sgl.CurItem.FrameThickness & 0x80,
                 sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_CIRCLE_IF);
          X1 = sgl.CurItem.X1 + sgl.CurItem.R1;
          Y1 = sgl.CurItem.Y1;
          X2 = sgl.CurItem.X1 + sgl.CurItem.R1;
          Y2 = sgl.CurItem.Y1;
          if (sgl.CurItem.FrameThickness & 0xC3)
            X2 += sgl.CurItem.R1;
          if (sgl.CurItem.FrameThickness & 0x3C)
            X1 -= sgl.CurItem.R1;
          if (sgl.CurItem.FrameThickness & 0xF0)
            Y1 -= sgl.CurItem.R1;
          if (sgl.CurItem.FrameThickness & 0x0F)
            Y2 += sgl.CurItem.R1;
          UpdateDrawLimits(X1, Y1, X2, Y2);
        }
      }
      break;

    case GuiLib_ITEM_QUARTERCIRCLE:
      if (sgl.DisplayWriting && (sgl.CurItem.R1 >= 0))
      {
        if (BackColorTransp)
          BackColor2 = GuiLib_NO_COLOR;
        else
          BackColor2 = BackColor;

        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        Ellipse(X1 + sgl.CurItem.R1,
                Y1,
                sgl.CurItem.R1,
                sgl.CurItem.R1,
                ForeColor, BackColor2,
                (sgl.CurItem.FrameThickness == 0),
                (sgl.CurItem.FrameThickness == 1),
                (sgl.CurItem.FrameThickness == 2),
                (sgl.CurItem.FrameThickness == 3));

        switch (sgl.CurItem.FrameThickness)
        {
          case 1:
            X1 += sgl.CurItem.R1;
            break;
          case 2:
            X1 += sgl.CurItem.R1;
            Y1 += sgl.CurItem.R1;
            break;
          case 3:
            Y1 += sgl.CurItem.R1;
            break;
        }
        UpdateDrawLimits(sgl.CurItem.X1,
                         sgl.CurItem.Y1,
                         sgl.CurItem.X1 + sgl.CurItem.R1,
                         sgl.CurItem.Y1 + sgl.CurItem.R1);
      }
      break;

    case GuiLib_ITEM_ELLIPSE:
      if (sgl.DisplayWriting && (sgl.CurItem.R1 >= 0) && (sgl.CurItem.R2 >= 0))
      {
        if (BackColorTransp)
          BackColor2 = GuiLib_NO_COLOR;
        else
          BackColor2 = BackColor;
        Ellipse(sgl.CurItem.X1 + sgl.CurItem.R1,
                sgl.CurItem.Y1,
                sgl.CurItem.R1,
                sgl.CurItem.R2,
                ForeColor, BackColor2, 1, 1, 1, 1);

        UpdateDrawLimits(sgl.CurItem.X1,
                         sgl.CurItem.Y1 - sgl.CurItem.R2,
                         sgl.CurItem.X1 + 2 * sgl.CurItem.R1,
                         sgl.CurItem.Y1 + sgl.CurItem.R2);
      }
      break;

    case GuiLib_ITEM_QUARTERELLIPSE:
      if (sgl.DisplayWriting && (sgl.CurItem.R1 >= 0) && (sgl.CurItem.R2 >= 0))
      {
        if (BackColorTransp)
          BackColor2 = GuiLib_NO_COLOR;
        else
          BackColor2 = BackColor;

        X1 = sgl.CurItem.X1;
        Y1 = sgl.CurItem.Y1;
        Ellipse(X1 + sgl.CurItem.R1,
                Y1,
                sgl.CurItem.R1,
                sgl.CurItem.R2,
                ForeColor, BackColor2,
                (sgl.CurItem.FrameThickness == 0),
                (sgl.CurItem.FrameThickness == 1),
                (sgl.CurItem.FrameThickness == 2),
                (sgl.CurItem.FrameThickness == 3));

        switch (sgl.CurItem.FrameThickness)
        {
          case 1:
            X1 += sgl.CurItem.R1;
            break;
          case 2:
            X1 += sgl.CurItem.R1;
            Y1 += sgl.CurItem.R2;
            break;
          case 3:
            Y1 += sgl.CurItem.R2;
            break;
        }
        UpdateDrawLimits(sgl.CurItem.X1,
                         sgl.CurItem.Y1,
                         sgl.CurItem.X1 + sgl.CurItem.R1,
                         sgl.CurItem.Y1 + sgl.CurItem.R2);
      }
      break;

    case GuiLib_ITEM_BITMAP:
    case GuiLib_ITEM_BACKGROUND:
#ifdef GuiConst_BITMAP_SUPPORT_ON
      if (sgl.DisplayWriting)
      {
        if (sgl.CommonByte6 & 0x02)
          BackColor2 = sgl.CurItem.CompPars.CompBitmap.TranspColor;
        else
          BackColor2 = -1;
        GuiLib_ShowBitmap(sgl.CurItem.StructToCallIndex,
                          sgl.CurItem.X1,
                          sgl.CurItem.Y1,
                          BackColor2);
        UpdateDrawLimits(sgl.CurItem.X1, sgl.CurItem.Y1,
                         sgl.BitmapWriteX2, sgl.BitmapWriteY2);
      }

      if (sgl.CurItem.ItemType == GuiLib_ITEM_BACKGROUND)
      {
        sgl.BackgrBitmapAry[sgl.GlobalBackgrBitmapIndex].InUse = 1;
        sgl.BackgrBitmapAry[sgl.GlobalBackgrBitmapIndex].Index =
           sgl.CurItem.StructToCallIndex;
        sgl.BackgrBitmapAry[sgl.GlobalBackgrBitmapIndex].X = sgl.CurItem.X1;
        sgl.BackgrBitmapAry[sgl.GlobalBackgrBitmapIndex].Y = sgl.CurItem.Y1;
        if (sgl.GlobalBackgrBitmapIndex < GuiConst_MAX_BACKGROUND_BITMAPS - 1)
          sgl.GlobalBackgrBitmapIndex++;
      }
#endif

      break;

    case GuiLib_ITEM_ACTIVEAREA:
      if (sgl.DisplayWriting)
      {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
        if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_CLIPPING)
        {
          sgl.CurItem.ClipRectX1 = sgl.CurItem.X1;
          sgl.CurItem.ClipRectY1 = sgl.CurItem.Y1;
          sgl.CurItem.ClipRectX2 = sgl.CurItem.X2;
          sgl.CurItem.ClipRectY2 = sgl.CurItem.Y2;
          sgl.ActiveAreaX1 = sgl.CurItem.X1;
          sgl.ActiveAreaY1 = sgl.CurItem.Y1;
          sgl.ActiveAreaX2 = sgl.CurItem.X2;
          sgl.ActiveAreaY2 = sgl.CurItem.Y2;
          OrderCoord(&sgl.ActiveAreaX1, &sgl.ActiveAreaX2);
          OrderCoord(&sgl.ActiveAreaY1, &sgl.ActiveAreaY2);
#ifdef GuiConst_DISPLAY_ACTIVE_AREA_CLIPPING
          if (sgl.ActiveAreaX1 < sgl.DisplayActiveAreaX1)
            sgl.ActiveAreaX1 = sgl.DisplayActiveAreaX1;
          if (sgl.ActiveAreaY1 < sgl.DisplayActiveAreaY1)
            sgl.ActiveAreaY1 = sgl.DisplayActiveAreaY1;
          if (sgl.ActiveAreaX2 > sgl.DisplayActiveAreaX2)
            sgl.ActiveAreaX2 = sgl.DisplayActiveAreaX2;
          if (sgl.ActiveAreaY2 > sgl.DisplayActiveAreaY2)
            sgl.ActiveAreaY2 = sgl.DisplayActiveAreaY2;
#endif
          GuiLib_SetClipping(sgl.ActiveAreaX1, sgl.ActiveAreaY1,
                             sgl.ActiveAreaX2, sgl.ActiveAreaY2);
        }
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_ACTIVEAREARELCOORD)
        {
          sgl.CurItem.ClipRectX1 += sgl.CoordOrigoX;
          sgl.CurItem.ClipRectY1 += sgl.CoordOrigoY;
          sgl.CurItem.ClipRectX2 += sgl.CoordOrigoX;
          sgl.CurItem.ClipRectY2 += sgl.CoordOrigoY;
        }
#endif
#endif

        sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
        sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;
        if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_ACTIVEAREARELCOORD)
        {
          sgl.CoordOrigoX += sgl.CurItem.X1;
          sgl.CoordOrigoY += sgl.CurItem.Y1;
        }
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        sgl.CurItem.CoordOrigoX = sgl.CoordOrigoX;
        sgl.CurItem.CoordOrigoY = sgl.CoordOrigoY;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
#ifdef GuiConst_REL_COORD_ORIGO_INUSE
        sgl.CurItem.ClipRectX1 -= sgl.CoordOrigoX;
        sgl.CurItem.ClipRectY1 -= sgl.CoordOrigoY;
        sgl.CurItem.ClipRectX2 -= sgl.CoordOrigoX;
        sgl.CurItem.ClipRectY2 -= sgl.CoordOrigoY;
#endif
#endif
      }
      break;

    case GuiLib_ITEM_CLIPRECT:
#ifdef GuiConst_CLIPPING_SUPPORT_ON
      if (sgl.DisplayWriting)
      {
        if (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_CLIPPING)
        {
          sgl.CurItem.ClipRectX1 = sgl.CurItem.X1;
          sgl.CurItem.ClipRectY1 = sgl.CurItem.Y1;
          sgl.CurItem.ClipRectX2 = sgl.CurItem.X2;
          sgl.CurItem.ClipRectY2 = sgl.CurItem.Y2;
        }
        StartClipping(
           (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_CLIPPING) != 0);
      }
#endif
      break;

    case GuiLib_ITEM_STRUCTURE:
    case GuiLib_ITEM_STRUCTARRAY:
    case GuiLib_ITEM_STRUCTCOND:
      if (sgl.DisplayWriting && (sgl.CurItem.TextPar[0].BackBoxSizeX > 0))
      {
        DrawBackBox(BackColor, BackColorTransp, 0);
        UpdateDrawLimits(sgl.BbX1, sgl.BbY1, sgl.BbX2, sgl.BbY2);
      }

      StructToCallIndex = sgl.CurItem.StructToCallIndex;
      if (sgl.CurItem.ItemType == GuiLib_ITEM_STRUCTARRAY)
      {
        if (StructToCallIndex != 0xFFFF)
        {
          VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);

          I = sgl.CurItem.CompPars.StructCall.IndexCount;
          while (I > 0)
          {
            if (ReadWord(GuiStruct_StructNdxList[StructToCallIndex]) ==
                VarValue)
              break;

            StructToCallIndex++;
            I--;
          }
          if (I == 0)
          {
            if (ReadWord(GuiStruct_StructNdxList[sgl.CurItem.StructToCallIndex]) == 0xFFFF)
              StructToCallIndex = sgl.CurItem.StructToCallIndex;
            else
              StructToCallIndex = 0xFFFF;
          }
        }
      }
#ifdef GuiConst_ITEM_STRUCTCOND_INUSE
      else if (sgl.CurItem.ItemType == GuiLib_ITEM_STRUCTCOND)
      {
        VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);

        I = sgl.CurItem.CompPars.StructCall.IndexCount - 1;
        FirstRound = 1;
        while (I >= 0)
        {
          if ((VarValue >= sgl.CurItem.CompPars.StructCall.IndexFirst[I]) &&
              (VarValue <= sgl.CurItem.CompPars.StructCall.IndexLast[I]) &&
              ((sgl.CurItem.CompPars.StructCall.IndexFirst[I] ==
                sgl.CurItem.CompPars.StructCall.IndexLast[I]) ||
              !FirstRound))
          {
            StructToCallIndex = sgl.CurItem.CompPars.StructCall.CallIndex[I];
            break;
          }

          I--;

          if ((I == -1) && FirstRound)
          {
            FirstRound = 0;
            I = sgl.CurItem.CompPars.StructCall.IndexCount - 1;
          }
        }
      }
#endif
      DrawSubStruct(StructToCallIndex, ColorInvert, 0);
      break;

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
    case GuiLib_ITEM_TOUCHAREA:
      TouchIndex = -1;
      for (TouchSearch = 0; TouchSearch < sgl.TouchAreaCnt; TouchSearch++)
        if (sgl.TouchAreas[TouchSearch].IndexNo ==
            sgl.CurItem.CompPars.CompTouch.AreaNo)
        {
          TouchIndex = TouchSearch;
          break;
        }
      if (TouchIndex == -1)
      {
        TouchIndex = sgl.TouchAreaCnt;
        sgl.TouchAreaCnt++;
      }
      sgl.TouchAreas[TouchIndex].IndexNo = sgl.CurItem.CompPars.CompTouch.AreaNo;
      sgl.TouchAreas[TouchIndex].X1 = sgl.CurItem.X1;
      sgl.TouchAreas[TouchIndex].Y1 = sgl.CurItem.Y1;
      sgl.TouchAreas[TouchIndex].X2 = sgl.CurItem.X2;
      sgl.TouchAreas[TouchIndex].Y2 = sgl.CurItem.Y2;
      OrderCoord(&sgl.TouchAreas[TouchIndex].X1, &sgl.TouchAreas[TouchIndex].X2);
      OrderCoord(&sgl.TouchAreas[TouchIndex].Y1, &sgl.TouchAreas[TouchIndex].Y2);

      break;
#endif

    case GuiLib_ITEM_POSCALLBACK:
      for (I = 0; I < GuiConst_POSCALLBACK_CNT; I++)
        if (sgl.PosCallbacks[I].InUse &&
           (sgl.PosCallbacks[I].IndexNo == sgl.CurItem.PosCallbackNo) &&
           (sgl.PosCallbacks[I].PosCallbackFunc != 0))
        {
          (*sgl.PosCallbacks[I].PosCallbackFunc)(sgl.CurItem.PosCallbackNo,
                                                 sgl.CurItem.X1,
                                                 sgl.CurItem.Y1);
          break;
        }
      break;

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
    case GuiLib_ITEM_SCROLLBOX:
      break;
#endif

#ifdef GuiConst_ITEM_CHECKBOX_INUSE
    case GuiLib_ITEM_CHECKBOX:
      PrepareInternalStruct();

      GuiVarCompInt1 = sgl.CurItem.CompPars.CompCheckBox.Size;
      sgl.Memory.C[0] = ForeColor;

      switch (sgl.CurItem.CompPars.CompCheckBox.Style)
      {
        case GuiLib_CHECKBOX_FLAT:
          if (BackColorTransp)
            DrawSubStruct(GuiStructCOMP_CBFLATTRANSP, 0, 1);
          else
            DrawSubStruct(GuiStructCOMP_CBFLAT, 0, 1);
          break;
        case GuiLib_CHECKBOX_3D:
          DrawSubStruct(GuiStructCOMP_CB3D, 0, 1);
          if (!BackColorTransp)
            DrawSubStruct(GuiStructCOMP_CB3DINNER, 0, 1);
          break;
        case GuiLib_CHECKBOX_ICON:
          TempTextPar = sgl.CurItem.TextPar[0];
          SetCurFont(sgl.CurItem.CompPars.CompCheckBox.IconFont);
          sgl.CurItem.X1 = sgl.CurItem.X1 + sgl.CurItem.CompPars.CompCheckBox.IconOffsetX;
          sgl.CurItem.Y1 = sgl.CurItem.Y1 +
                       sgl.CurItem.CompPars.CompCheckBox.IconOffsetY +
                       sgl.CurFont->BaseLine;
          sgl.CurItem.TextPar[0].Alignment = GuiLib_ALIGN_LEFT;
          sgl.CurItem.TextPar[0].Ps = GuiLib_PS_OFF;
          sgl.CurItem.TextPar[0].BitFlags = 0;
          sgl.CurItem.TextPar[0].BackBoxSizeX = 0;
          sgl.CurItem.TextPar[0].BackBorderPixels = 0;
          DrawText(sgl.CurItem.CompPars.CompCheckBox.IconPtr,
                   1,
                   0,
                   ForeColor,
                   BackColor,
                   BackColorTransp);
          sgl.CurItem.TextPar[0] = TempTextPar;
          break;
        case GuiLib_CHECKBOX_BITMAP:
          if (sgl.CurItem.CompPars.CompCheckBox.BitmapIsTransparent)
            BackColor2 = sgl.CurItem.CompPars.CompCheckBox.BitmapTranspColor;
          else
            BackColor2 = -1;
          GuiLib_ShowBitmap(sgl.CurItem.CompPars.CompCheckBox.BitmapIndex,
                            sgl.CurItem.X1, sgl.CurItem.Y1,
                            BackColor2);
          break;
      }

      if ((sgl.CurItem.VarPtr != 0) && (sgl.CurItem.VarType != GuiLib_VAR_STRING))
      {
        VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);
        if (VarValue != 0)
        {
          GuiVarCompInt1 = sgl.CurItem.CompPars.CompCheckBox.Size;
          sgl.Memory.C[0] = sgl.CurItem.CompPars.CompCheckBox.MarkColor;
          switch (sgl.CurItem.CompPars.CompCheckBox.MarkStyle)
          {
            case GuiLib_CHECKBOX_MARK_CHECKED:
              GuiVarCompInt1 =
                     (sgl.CurItem.CompPars.CompCheckBox.Size / 2) -
                     ((sgl.CurItem.CompPars.CompCheckBox.Size - 6) / 6);
              GuiVarCompInt2 =
                     (sgl.CurItem.CompPars.CompCheckBox.Size /
                      2) +
                     ((sgl.CurItem.CompPars.CompCheckBox.Size - 8) / 6);
              GuiVarCompInt3 = GuiVarCompInt2 -
                     ((sgl.CurItem.CompPars.CompCheckBox.Size - 1) / 5);
              GuiVarCompInt4 = -GuiVarCompInt3;
              GuiVarCompInt5 = -((GuiVarCompInt3 - 1) / 2);
              if (((sgl.CurItem.CompPars.CompCheckBox.Style ==
                    GuiLib_CHECKBOX_FLAT) ||
                   (sgl.CurItem.CompPars.CompCheckBox.Style ==
                    GuiLib_CHECKBOX_3D) ||
                   (sgl.CurItem.CompPars.CompCheckBox.Style ==
                    GuiLib_CHECKBOX_NONE)) &&
                   (sgl.CurItem.CompPars.CompCheckBox.Size <= 10))
                DrawSubStruct(GuiStructCOMP_CBMCHSMALL, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_CBMCHBIG, 0, 1);
              break;
            case GuiLib_CHECKBOX_MARK_CROSSED:
              if ((sgl.CurItem.CompPars.CompCheckBox.Style ==
                   GuiLib_CHECKBOX_FLAT) &&
                  (sgl.CurItem.CompPars.CompCheckBox.Size <= 8))
                DrawSubStruct(GuiStructCOMP_CBMCRFLSMALL, 0, 1);
              else if (((sgl.CurItem.CompPars.CompCheckBox.Style ==
                         GuiLib_CHECKBOX_3D) ||
                        (sgl.CurItem.CompPars.CompCheckBox.Style ==
                         GuiLib_CHECKBOX_NONE)) &&
                       (sgl.CurItem.CompPars.CompCheckBox.Size <= 8))
                DrawSubStruct(GuiStructCOMP_CBMCR3DSMALL, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_CBMCRBIG, 0, 1);
              break;
            case GuiLib_CHECKBOX_MARK_FILLED:
              if (sgl.CurItem.CompPars.CompCheckBox.Style == GuiLib_CHECKBOX_FLAT)
                DrawSubStruct(GuiStructCOMP_CBMFIFLAT, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_CBMFI3D, 0, 1);
              break;
            case GuiLib_CHECKBOX_MARK_ICON:
              TempTextPar = sgl.CurItem.TextPar[0];
              SetCurFont(sgl.CurItem.CompPars.CompCheckBox.MarkIconFont);
              sgl.CurItem.X1 = sgl.CurItem.X1 +
                           sgl.CurItem.CompPars.CompCheckBox.MarkOffsetX;
              sgl.CurItem.Y1 = sgl.CurItem.Y1 +
                           sgl.CurItem.CompPars.CompCheckBox.MarkOffsetY +
                           sgl.CurFont->BaseLine;
              sgl.CurItem.TextPar[0].Alignment = GuiLib_ALIGN_LEFT;
              sgl.CurItem.TextPar[0].Ps = GuiLib_PS_OFF;
              sgl.CurItem.TextPar[0].BitFlags = 0;
              sgl.CurItem.TextPar[0].BackBoxSizeX = 0;
              sgl.CurItem.TextPar[0].BackBorderPixels = 0;
              DrawText(sgl.CurItem.CompPars.CompCheckBox.MarkIconPtr,
                       1,
                       0,
                       ForeColor,
                       BackColor,
                       BackColorTransp);
              sgl.CurItem.TextPar[0] = TempTextPar;
              break;
            case GuiLib_CHECKBOX_MARK_BITMAP:
              if (sgl.CurItem.CompPars.CompCheckBox.MarkBitmapIsTransparent)
                BackColor2 = sgl.CurItem.CompPars.CompCheckBox.MarkBitmapTranspColor;
              else
                BackColor2 = -1;
              GuiLib_ShowBitmap(sgl.CurItem.CompPars.CompCheckBox.MarkBitmapIndex,
                                sgl.CurItem.X1 +
                                sgl.CurItem.CompPars.CompCheckBox.MarkOffsetX,
                                sgl.CurItem.Y1 +
                                sgl.CurItem.CompPars.CompCheckBox.MarkOffsetY,
                                BackColor2);
              break;
          }
        }
      }
      break;
#endif

#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
    case GuiLib_ITEM_RADIOBUTTON:
      PrepareInternalStruct();

      X1 = sgl.CurItem.X1;
      Y1 = sgl.CurItem.Y1;
      RemYMemory = sgl.Memory.Y[GuiLib_MEMORY_CNT];

      if ((sgl.CurItem.VarPtr != 0) &&
          (sgl.CurItem.VarType != GuiLib_VAR_STRING))
        VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);
      else
        VarValue = -1;

      for (N = 0; N < sgl.CurItem.CompPars.CompRadioButton.Count; N++)
      {
        GuiVarCompInt1 = sgl.CurItem.CompPars.CompRadioButton.Size;
        sgl.Memory.C[0] = ForeColor;
        switch (sgl.CurItem.CompPars.CompRadioButton.Style)
        {
          case GuiLib_RADIOBUTTON_FLAT:
            if (BackColorTransp)
              DrawSubStruct(GuiStructCOMP_RBFLATTRANSP, 0, 1);
            else
              DrawSubStruct(GuiStructCOMP_RBFLAT, 0, 1);
            break;
          case GuiLib_RADIOBUTTON_3D:
            if (!BackColorTransp)
              DrawSubStruct(GuiStructCOMP_RB3DINNER, 0, 1);
            DrawSubStruct(GuiStructCOMP_RB3D, 0, 1);
            break;
          case GuiLib_RADIOBUTTON_ICON:
            TempTextPar = sgl.CurItem.TextPar[0];
            SetCurFont(sgl.CurItem.CompPars.CompRadioButton.IconFont);
            sgl.CurItem.X1 = X1 + sgl.CurItem.CompPars.CompRadioButton.IconOffsetX;
            sgl.CurItem.Y1 = Y1 + sgl.CurItem.CompPars.CompRadioButton.IconOffsetY +
                         sgl.CurFont->BaseLine;
            sgl.CurItem.TextPar[0].Alignment = GuiLib_ALIGN_LEFT;
            sgl.CurItem.TextPar[0].Ps = GuiLib_PS_OFF;
            sgl.CurItem.TextPar[0].BitFlags = 0;
            sgl.CurItem.TextPar[0].BackBoxSizeX = 0;
            sgl.CurItem.TextPar[0].BackBorderPixels = 0;
            DrawText(sgl.CurItem.CompPars.CompRadioButton.IconPtr,
                     1,
                     0,
                     ForeColor,
                     BackColor,
                     BackColorTransp);
            sgl.CurItem.TextPar[0] = TempTextPar;
            break;
          case GuiLib_RADIOBUTTON_BITMAP:
            if (sgl.CurItem.CompPars.CompRadioButton.BitmapIsTransparent)
              BackColor2 = sgl.CurItem.CompPars.CompRadioButton.BitmapTranspColor;
            else
              BackColor2 = -1;
            GuiLib_ShowBitmap(sgl.CurItem.CompPars.CompRadioButton.BitmapIndex,
                              X1, Y1,
                              BackColor2);
            break;
        }

        if (VarValue == N)
        {
          GuiVarCompInt1 = sgl.CurItem.CompPars.CompRadioButton.Size;
          GuiVarCompInt2 = sgl.CurItem.CompPars.CompRadioButton.Size - 3;
          if (sgl.CurItem.CompPars.CompRadioButton.Style ==
              GuiLib_RADIOBUTTON_FLAT)
            GuiVarCompInt2++;
          sgl.Memory.C[0] = sgl.CurItem.CompPars.CompRadioButton.MarkColor;
          switch (sgl.CurItem.CompPars.CompRadioButton.MarkStyle)
          {
            case GuiLib_RADIOBUTTON_MARK_STANDARD:
              if (GuiVarCompInt1 <= 1)
                DrawSubStruct(GuiStructCOMP_RBMSQUA, 0, 1);
              else if (sgl.CurItem.CompPars.CompRadioButton.Style ==
                       GuiLib_RADIOBUTTON_FLAT)
                DrawSubStruct(GuiStructCOMP_RBMFLAT, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_RBM3D, 0, 1);
              break;
            case GuiLib_RADIOBUTTON_MARK_ICON:
              TempTextPar = sgl.CurItem.TextPar[0];
              SetCurFont(sgl.CurItem.CompPars.CompRadioButton.MarkIconFont);
              sgl.CurItem.X1 = X1 + sgl.CurItem.CompPars.CompRadioButton.MarkOffsetX;
              sgl.CurItem.Y1 = Y1 + sgl.CurItem.CompPars.CompRadioButton.MarkOffsetY +
                           sgl.CurFont->BaseLine;
              sgl.CurItem.TextPar[0].Alignment = GuiLib_ALIGN_LEFT;
              sgl.CurItem.TextPar[0].Ps = GuiLib_PS_OFF;
              sgl.CurItem.TextPar[0].BitFlags = 0;
              sgl.CurItem.TextPar[0].BackBoxSizeX = 0;
              sgl.CurItem.TextPar[0].BackBorderPixels = 0;
              DrawText(sgl.CurItem.CompPars.CompRadioButton.MarkIconPtr,
                       1,
                       0,
                       ForeColor,
                       BackColor,
                       BackColorTransp);
              sgl.CurItem.TextPar[0] = TempTextPar;
              break;
            case GuiLib_RADIOBUTTON_MARK_BITMAP:
              if (sgl.CurItem.CompPars.CompRadioButton.MarkBitmapIsTransparent)
                BackColor2 = sgl.CurItem.CompPars.CompRadioButton.MarkBitmapTranspColor;
              else
                BackColor2 = -1;
              GuiLib_ShowBitmap(sgl.CurItem.CompPars.CompRadioButton.MarkBitmapIndex,
                                X1 +
                                sgl.CurItem.CompPars.CompRadioButton.MarkOffsetX,
                                Y1 +
                                sgl.CurItem.CompPars.CompRadioButton.MarkOffsetY,
                                BackColor2);
              break;
          }
        }

        Y1 += sgl.CurItem.CompPars.CompRadioButton.InterDistance;
        sgl.Memory.Y[GuiLib_MEMORY_CNT] +=
           sgl.CurItem.CompPars.CompRadioButton.InterDistance;
      }

      sgl.Memory.Y[GuiLib_MEMORY_CNT] = RemYMemory;
      break;
#endif

#ifdef GuiConst_ITEM_BUTTON_INUSE
    case GuiLib_ITEM_BUTTON:
      PrepareInternalStruct();

      X1 = sgl.CurItem.X1;
      Y1 = sgl.CurItem.Y1;
      X2 = sgl.CurItem.X2;
      Y2 = sgl.CurItem.Y2;
      RemX1 = sgl.CurItem.X1;
      RemY1 = sgl.CurItem.Y1;

      GuiVarCompInt1 = X2 - X1 + 1;
      GuiVarCompInt2 = Y2 - Y1 + 1;
      GuiVarCompInt3 = sgl.CurItem.R1;
      GuiVarCompInt4 = GuiVarCompInt1 - sgl.CurItem.R1;
      GuiVarCompInt5 = GuiVarCompInt2 - sgl.CurItem.R1;

      sgl.Memory.C[0] = ForeColor;
      sgl.Memory.C[1] = BackColor;

      if (sgl.ButtonColorOverride == GuiLib_TRUE)
        sgl.Memory.C[2] = sgl.DisabledButtonColor;
      else
        sgl.Memory.C[2] = GuiLib_DesaturatePixelColor(BackColor,800);

      if ((sgl.CurItem.VarPtr != 0) &&
          (sgl.CurItem.VarType != GuiLib_VAR_STRING))
      {
        VarValue = ReadVar(sgl.CurItem.VarPtr, sgl.CurItem.VarType);
        if ((VarValue < GuiLib_BUTTON_STATE_UP) ||
            (VarValue > GuiLib_BUTTON_STATE_DISABLED))
          VarValue = GuiLib_BUTTON_STATE_UP;
      }
      else
        VarValue = GuiLib_BUTTON_STATE_UP;

      DisabledGlyphColorInUse = 0;
      DisabledTextColorInUse = 0;
      switch (sgl.CurItem.CompPars.CompButton.BodyStyle)
      {
        case GuiLib_BUTTON_BODY_FLAT:
          switch (VarValue)
          {
            case 0 :
              if (sgl.CurItem.R1 == 0)
                DrawSubStruct(GuiStructCOMP_BUFLAT0, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_BUFLATR0, 0, 1);
              break;
            case 1 :
              if (sgl.CurItem.R1 == 0)
                DrawSubStruct(GuiStructCOMP_BUFLAT1, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_BUFLATR1, 0, 1);
              break;
            case 2 :
              if (sgl.CurItem.R1 == 0)
                DrawSubStruct(GuiStructCOMP_BUFLAT2, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_BUFLATR2, 0, 1);
              DisabledGlyphColorInUse =
                 (sgl.CurItem.CompPars.CompButton.GlyphLikeUp & 0x02);
              DisabledTextColorInUse =
                 (sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x08);
              break;
          }
          break;
        case GuiLib_BUTTON_BODY_3D:
          switch (VarValue)
          {
            case 0 :
              if (sgl.CurItem.R1 == 0)
                DrawSubStruct(GuiStructCOMP_BU3D0, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_BU3DR0, 0, 1);
              break;
            case 1 :
              if (sgl.CurItem.R1 == 0)
                DrawSubStruct(GuiStructCOMP_BU3D1, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_BU3DR1, 0, 1);
              break;
            case 2 :
              if (sgl.CurItem.R1 == 0)
                DrawSubStruct(GuiStructCOMP_BU3D2, 0, 1);
              else
                DrawSubStruct(GuiStructCOMP_BU3DR2, 0, 1);
              DisabledGlyphColorInUse =
                 !(sgl.CurItem.CompPars.CompButton.GlyphLikeUp & 0x02);
              DisabledTextColorInUse =
                 !(sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x08);
              break;
          }
          break;
        case GuiLib_BUTTON_BODY_ICON:
          TempTextPar = sgl.CurItem.TextPar[VarValue];
          SetCurFont(sgl.CurItem.CompPars.CompButton.BodyIconFont[VarValue]);
          sgl.CurItem.X1 =
             X1 +
             sgl.CurItem.CompPars.CompButton.BodyIconOffsetX[VarValue];
          sgl.CurItem.Y1 =
             Y1 +
             sgl.CurItem.CompPars.CompButton.BodyIconOffsetY[VarValue] +
             sgl.CurFont->BaseLine;
          sgl.CurItem.TextPar[VarValue].Alignment = GuiLib_ALIGN_LEFT;
          sgl.CurItem.TextPar[VarValue].Ps = GuiLib_PS_OFF;
          sgl.CurItem.TextPar[VarValue].BitFlags = 0;
          sgl.CurItem.TextPar[VarValue].BackBoxSizeX = 0;
          sgl.CurItem.TextPar[VarValue].BackBorderPixels = 0;
          DrawText(sgl.CurItem.CompPars.CompButton.BodyIconPtr[VarValue],
                   1,
                   VarValue,
                   ForeColor,
                   BackColor,
                   BackColorTransp);
          sgl.CurItem.TextPar[VarValue] = TempTextPar;
          X1 = sgl.FontWriteX1;
          Y1 = sgl.FontWriteY1;
          X2 = sgl.FontWriteX2;
          Y2 = sgl.FontWriteY2;
          break;
        case GuiLib_BUTTON_BODY_BITMAP:
          if (sgl.CurItem.CompPars.CompButton.BodyBitmapIsTransparent[VarValue])
            BackColor2 =
               sgl.CurItem.CompPars.CompButton.BodyBitmapTranspColor[VarValue];
          else
            BackColor2 = -1;
          GuiLib_ShowBitmap(sgl.CurItem.CompPars.CompButton.BodyBitmapIndex[VarValue],
                            X1, Y1,
                            BackColor2);
          X2 = sgl.BitmapWriteX2;
          Y2 = sgl.BitmapWriteY2;
          break;
      }

      CX = (X1 + X2) / 2;
      CY = (Y1 + Y2) / 2;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
      RemClippingX1 = sgl.CurItem.ClipRectX1;
      RemClippingY1 = sgl.CurItem.ClipRectY1;
      RemClippingX2 = sgl.CurItem.ClipRectX2;
      RemClippingY2 = sgl.CurItem.ClipRectY2;
      sgl.CurItem.ClipRectX1 = GuiLib_GET_MAX(sgl.CurItem.ClipRectX1, X1);
      sgl.CurItem.ClipRectY1 = GuiLib_GET_MAX(sgl.CurItem.ClipRectY1, Y1);
      sgl.CurItem.ClipRectX2 = GuiLib_GET_MIN(sgl.CurItem.ClipRectX2, X2);
      sgl.CurItem.ClipRectY2 = GuiLib_GET_MIN(sgl.CurItem.ClipRectY2, Y2);
      GuiLib_SetClipping(sgl.CurItem.ClipRectX1, sgl.CurItem.ClipRectY1,
                         sgl.CurItem.ClipRectX2, sgl.CurItem.ClipRectY2);
#endif // GuiConst_CLIPPING_SUPPORT_ON

      if (sgl.CurItem.CompPars.CompButton.Layout != GuiLib_BUTTON_LAYOUT_TEXT)
      {
        switch (sgl.CurItem.CompPars.CompButton.Layout)
        {
          case GuiLib_BUTTON_LAYOUT_GLYPH:
            GX1 = CX;
            GY1 = CY;
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHLEFT:
            GX1 = X1 + GuiLib_GET_MIN((Y2 - Y1) / 2, (X2 - X1) / 4);
            GY1 = CY;
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHRIGHT:
            GX1 = X2 - GuiLib_GET_MIN((Y2 - Y1) / 2, (X2 - X1) / 4);
            GY1 = CY;
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHTOP:
            GX1 = CX;
            GY1 = Y1 + ((Y2 - Y1) / 4);
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHBOTTOM:
            GX1 = CX;
            GY1 = Y2 - ((Y2 - Y1) / 4);
            break;
        }
        switch (sgl.CurItem.CompPars.CompButton.GlyphStyle)
        {
          case GuiLib_BUTTON_GLYPH_ICON:
            TempTextPar = sgl.CurItem.TextPar[VarValue];
            SetCurFont(sgl.CurItem.CompPars.CompButton.GlyphIconFont[VarValue]);
            sgl.CurItem.X1 =
               GX1 +
               sgl.CurItem.CompPars.CompButton.GlyphIconOffsetX[VarValue];
            sgl.CurItem.Y1 =
               GY1 +
               sgl.CurItem.CompPars.CompButton.GlyphIconOffsetY[VarValue] +
               GuiLib_FONT_MID_Y(sgl.CurFont->BaseLine, sgl.CurFont->TopLine);
            sgl.CurItem.TextPar[VarValue].Alignment = GuiLib_ALIGN_CENTER;
            sgl.CurItem.TextPar[VarValue].Ps = GuiLib_PS_OFF;
            sgl.CurItem.TextPar[VarValue].BitFlags = 0;
            sgl.CurItem.TextPar[VarValue].BackBoxSizeX = 0;
            sgl.CurItem.TextPar[VarValue].BackBorderPixels = 0;
            if (DisabledGlyphColorInUse)
              ButtonColor = GuiLib_MiddlePixelColor(
                 sgl.CurItem.CompPars.CompButton.GlyphIconColor[VarValue],
                 sgl.Memory.C[2],
                 800);
            else
              ButtonColor = sgl.CurItem.CompPars.CompButton.GlyphIconColor[VarValue];
            DrawText(sgl.CurItem.CompPars.CompButton.GlyphIconPtr[VarValue],
                     1,
                     VarValue,
                     ButtonColor,
                     0,
                     GuiLib_TRUE);
            sgl.CurItem.TextPar[VarValue] = TempTextPar;
            GX1 = sgl.FontWriteX1;
            GY1 = sgl.FontWriteY1;
            GX2 = sgl.FontWriteX2;
            GY2 = sgl.FontWriteY2;
            break;
          case GuiLib_BUTTON_GLYPH_BITMAP:
            ReadBitmapSizes(sgl.CurItem.CompPars.CompButton.GlyphBitmapIndex[VarValue]);
            GX1 -= sgl.BitmapSizeX / 2;
            GY1 -= sgl.BitmapSizeY / 2;

            GX1 += sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetX[VarValue];
            GY1 += sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetY[VarValue];
            if (sgl.CurItem.CompPars.CompButton.GlyphBitmapIsTransparent[VarValue])
              BackColor2 = sgl.CurItem.CompPars.CompButton.GlyphBitmapTranspColor[VarValue];
            else
              BackColor2 = -1;
            GuiLib_ShowBitmap(sgl.CurItem.CompPars.CompButton.GlyphBitmapIndex[VarValue],
                              GX1, GY1,
                              BackColor2);
            GX2 = sgl.BitmapWriteX2;
            GY2 = sgl.BitmapWriteY2;
            break;
        }
      }

      if (sgl.CurItem.CompPars.CompButton.Layout != GuiLib_BUTTON_LAYOUT_GLYPH)
      {
        TempTextPar = sgl.CurItem.TextPar[VarValue];
        SetCurFont(sgl.CurItem.TextPar[VarValue].FontIndex);
        switch (sgl.CurItem.CompPars.CompButton.Layout)
        {
          case GuiLib_BUTTON_LAYOUT_TEXT:
            sgl.CurItem.X1 = CX;
            sgl.CurItem.Y1 = CY;
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHLEFT:
            sgl.CurItem.X1 = X2 - ((X2 - GX2 + 1) / 2);
            sgl.CurItem.Y1 = CY;
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHRIGHT:
            sgl.CurItem.X1 = X1 + ((GX1 - X1 + 1) / 2);
            sgl.CurItem.Y1 = CY;
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHTOP:
            sgl.CurItem.X1 = CX;
            sgl.CurItem.Y1 = Y2 - ((Y2 - GY2 + 1) / 2);
            break;
          case GuiLib_BUTTON_LAYOUT_GLYPHBOTTOM:
            sgl.CurItem.X1 = CX;
            sgl.CurItem.Y1 = Y1 + ((GY1 - Y1 + 1) / 2);
            break;
        }
        sgl.CurItem.Y1 +=
           GuiLib_FONT_MID_Y(sgl.CurFont->BaseLine, sgl.CurFont->TopLine);
        sgl.CurItem.TextPar[VarValue].Alignment = GuiLib_ALIGN_CENTER;
        sgl.CurItem.TextPar[VarValue].BitFlags = 0;
        sgl.CurItem.TextPar[VarValue].BackBoxSizeX = 0;
        sgl.CurItem.TextPar[VarValue].BackBorderPixels = 0;
        if (DisabledTextColorInUse)
          ButtonColor = GuiLib_MiddlePixelColor(
             sgl.CurItem.CompPars.CompButton.TextColor[VarValue],
             sgl.Memory.C[2],
             800);
        else
          ButtonColor = sgl.CurItem.CompPars.CompButton.TextColor[VarValue];
        DRAW_ROM_TEXT(sgl.CurItem.TextPtr[VarValue],
                      sgl.CurItem.TextLength[VarValue],
                      VarValue,
                      ButtonColor,
                      0,
                      GuiLib_TRUE);
        sgl.CurItem.TextPar[VarValue] = TempTextPar;
      }

      sgl.CurItem.X1 = RemX1;
      sgl.CurItem.Y1 = RemY1;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
      sgl.CurItem.ClipRectX1 = RemClippingX1;
      sgl.CurItem.ClipRectY1 = RemClippingY1;
      sgl.CurItem.ClipRectX2 = RemClippingX2;
      sgl.CurItem.ClipRectY2 = RemClippingY2;
      GuiLib_SetClipping(sgl.CurItem.ClipRectX1, sgl.CurItem.ClipRectY1,
                         sgl.CurItem.ClipRectX2, sgl.CurItem.ClipRectY2);
#endif // GuiConst_CLIPPING_SUPPORT_ON

      break;
#endif

#ifdef GuiConst_ITEM_PANEL_INUSE
    case GuiLib_ITEM_PANEL:
      PrepareInternalStruct();

      X1 = sgl.CurItem.X1;
      Y1 = sgl.CurItem.Y1;
      X2 = sgl.CurItem.X2;
      Y2 = sgl.CurItem.Y2;
      OrderCoord(&X1, &X2);
      OrderCoord(&Y1, &Y2);

      GuiVarCompInt1 = X2 - X1 + 1;
      GuiVarCompInt2 = Y2 - Y1 + 1;
      GuiVarCompInt3 = sgl.CurItem.R1;
      GuiVarCompInt4 = GuiVarCompInt1 - sgl.CurItem.R1;
      GuiVarCompInt5 = GuiVarCompInt2 - sgl.CurItem.R1;

      sgl.Memory.C[0] = ForeColor;
      switch (sgl.CurItem.CompPars.CompPanel.Style)
      {
        case GuiLib_PANEL_FLAT:
          if (sgl.CurItem.R1 == 0)
          {
            if (BackColorTransp)
              DrawSubStruct(GuiStructCOMP_PAFLATTRANSP, 0, 1);
            else
              DrawSubStruct(GuiStructCOMP_PAFLAT, 0, 1);
          }
          else
          {
            if (BackColorTransp)
              DrawSubStruct(GuiStructCOMP_PAFLATTRANSPR, 0, 1);
            else
              DrawSubStruct(GuiStructCOMP_PAFLATR, 0, 1);
          }
          break;

        case GuiLib_PANEL_3D_RAISED:
          if (sgl.CurItem.R1 == 0)
          {
            if (!BackColorTransp)
              DrawSubStruct(GuiStructCOMP_PA3DINNER, 0, 1);
            DrawSubStruct(GuiStructCOMP_PA3DRAIS, 0, 1);
          }
          else
          {
            if (!BackColorTransp)
              DrawSubStruct(GuiStructCOMP_PA3DINNERR, 0, 1);
            DrawSubStruct(GuiStructCOMP_PA3DRAISR, 0, 1);
          }
          break;

        case GuiLib_PANEL_3D_LOWERED:
          if (sgl.CurItem.R1 == 0)
          {
            if (!BackColorTransp)
              DrawSubStruct(GuiStructCOMP_PA3DINNER, 0, 1);
            DrawSubStruct(GuiStructCOMP_PA3DLOW, 0, 1);
          }
          else
          {
            if (!BackColorTransp)
              DrawSubStruct(GuiStructCOMP_PA3DINNERR, 0, 1);
            DrawSubStruct(GuiStructCOMP_PA3DLOWR, 0, 1);
          }
          break;

        case GuiLib_PANEL_EMBOSSED_RAISED:
          if (!BackColorTransp)
            DrawSubStruct(GuiStructCOMP_PA3DINNER, 0, 1);
          DrawSubStruct(GuiStructCOMP_PAEMBRAIS, 0, 1);
          break;

        case GuiLib_PANEL_EMBOSSED_LOWERED:
          if (!BackColorTransp)
            DrawSubStruct(GuiStructCOMP_PA3DINNER, 0, 1);
          DrawSubStruct(GuiStructCOMP_PAEMBLOW, 0, 1);
          break;
      }
      break;
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
    case GuiLib_ITEM_GRAPH:
      break;
#endif

#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
    case GuiLib_ITEM_GRAPHICSLAYER:
#ifdef GuiLib_LAYERS_SUPPORTED
      if (sgl.DisplayWriting)
      {
        sgl.LayerOrigoX = 0;
        sgl.LayerOrigoY = 0;
        sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
        sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;

        X1 = sgl.CurItem.X1;
        X2 = sgl.CurItem.X2;
        Y1 = sgl.CurItem.Y1;
        Y2 = sgl.CurItem.Y2;
        GuiLib_COORD_ADJUST(X1, Y1);
        GuiLib_COORD_ADJUST(X2, Y2);
        OrderCoord(&X1, &X2);
        OrderCoord(&Y1, &Y2);

        switch (sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].SizeMode)
        {
          case GuiLib_GRAPHICS_LAYER_SIZE_COORD:
            X1 = GuiLib_GET_MINMAX(X1, 0, GuiConst_DISPLAY_WIDTH_HW - 1);
            Y1 = GuiLib_GET_MINMAX(Y1, 0, GuiConst_DISPLAY_HEIGHT_HW - 1);
            X2 = GuiLib_GET_MINMAX(X2, 0, GuiConst_DISPLAY_WIDTH_HW - 1);
            Y2 = GuiLib_GET_MINMAX(Y2, 0, GuiConst_DISPLAY_HEIGHT_HW - 1);
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X = GuiLib_GET_MINMAX(
               X1, 0, GuiConst_DISPLAY_WIDTH_HW);
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y = GuiLib_GET_MINMAX(
               Y1, 0, GuiConst_DISPLAY_HEIGHT_HW);
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width = X2 - X1 + 1;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Height = Y2 - Y1 + 1;
            break;

          case GuiLib_GRAPHICS_LAYER_SIZE_SCREEN:
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X = 0;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y = 0;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width =
               GuiConst_DISPLAY_WIDTH_HW;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Height =
               GuiConst_DISPLAY_HEIGHT_HW;
            break;

          case GuiLib_GRAPHICS_LAYER_SIZE_CLIP:
            CX1 = sgl.CurItem.ClipRectX1;
            CX2 = sgl.CurItem.ClipRectX2;
            CY1 = sgl.CurItem.ClipRectY1;
            CY2 = sgl.CurItem.ClipRectY2;
            GuiLib_COORD_ADJUST(CX1, CY1);
            GuiLib_COORD_ADJUST(CX2, CY2);
            OrderCoord(&CX1, &CX2);
            OrderCoord(&CY1, &CY2);
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X = CX1;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y = CY1;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width = CX2 - CX1 + 1;
            sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Height = CY2 - CY1 + 1;
            break;
        }
        sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].LineSize =
             GuiConst_PIXEL_BYTE_SIZE *
             sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width;
        sgl.CurItem.ClipRectX1 = 0;
        sgl.CurItem.ClipRectY1 = 0;
        sgl.CurItem.ClipRectX2 =
           sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width - 1;
        sgl.CurItem.ClipRectY2 =
           sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Height - 1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
        StartClipping(1);
#endif
        sgl.LayerOrigoX = -sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X;
        sgl.LayerOrigoY = -sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y;
        sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
        sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;

        if (GraphicsLayer_Push(sgl.GlobalGraphicsLayerIndex))
        {
          switch (sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].InitMode)
          {
            case GuiLib_GRAPHICS_LAYER_INIT_NONE:
              break;

            case GuiLib_GRAPHICS_LAYER_INIT_COL:
              GuiLib_FillBox(
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X +
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width - 1,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y +
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Height - 1,
                 BackColor);
              break;

            case GuiLib_GRAPHICS_LAYER_INIT_COPY:
              GraphicsLayer_Copy(
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].BaseAddress,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].LineSize,
                 0,
                 0,
#ifdef GuiConst_DISPLAY_BUFFER_EASYGUI
#ifdef GuiLib_COLOR_UNIT_16
                 (GuiConst_INT8U*)GuiLib_DisplayBuf.Bytes,
#else
                 (GuiConst_INT8U*)GuiLib_DisplayBuf,
#endif
#else // GuiConst_DISPLAY_BUFFER_EASYGUI
                 0,
#endif // GuiConst_DISPLAY_BUFFER_EASYGUI
                 GuiConst_BYTES_PR_LINE,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].X,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Y,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Width,
                 sgl.GraphicsLayerList[sgl.GlobalGraphicsLayerIndex].Height);
              break;
          }
        }
      }
#endif
      break;

    case GuiLib_ITEM_GRAPHICSFILTER:
#ifdef GuiLib_LAYERS_SUPPORTED
      if (sgl.DisplayWriting)
      {
        sgl.LayerOrigoX = 0;
        sgl.LayerOrigoY = 0;
        sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
        sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;

        SourceLayerIndexNo = IndexOfGraphicsLayer(
           sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].SourceLayerIndexNo);
        DestLayerIndexNo = IndexOfGraphicsLayer(
           sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].DestLayerIndexNo);
        if (SourceLayerIndexNo != DestLayerIndexNo)
        {
          if (SourceLayerIndexNo == GuiLib_GRAPHICS_LAYER_BASE)
          {
#ifdef GuiConst_DISPLAY_BUFFER_EASYGUI
#ifdef GuiLib_COLOR_UNIT_16
            SourceAddress = &(GuiLib_DisplayBuf.Bytes[0][0]);
#else
  #ifdef GuiConst_COLOR_DEPTH_2
    #ifdef GuiConst_BYTE_HORIZONTAL
      #ifdef GuiConst_COLOR_PLANES_2
            SourceAddress = &(GuiLib_DisplayBuf[0][0][0]);
      #else
            SourceAddress = &(GuiLib_DisplayBuf[0][0]);
      #endif
    #else
      #ifdef GuiConst_COLOR_PLANES_2
            SourceAddress = &(GuiLib_DisplayBuf[0][0][0]);
      #else
            SourceAddress = &(GuiLib_DisplayBuf[0][0]);
      #endif
    #endif
  #else
            SourceAddress = &(GuiLib_DisplayBuf[0][0]);
  #endif
#endif // GuiLib_COLOR_UNIT_16
#else // GuiConst_DISPLAY_BUFFER_EASYGUI
            SourceAddress = 0;
#endif // GuiConst_DISPLAY_BUFFER_EASYGUI
            SourceLineSize = GuiConst_BYTES_PR_LINE;
            SourceX = 0;
            SourceY = 0;
            SourceWidth = GuiConst_DISPLAY_WIDTH_HW;
            SourceHeight = GuiConst_DISPLAY_HEIGHT_HW;
          }
          else
          {
            SourceAddress = sgl.GraphicsLayerList[SourceLayerIndexNo].BaseAddress;
            SourceLineSize = sgl.GraphicsLayerList[SourceLayerIndexNo].LineSize;
            SourceX = sgl.GraphicsLayerList[SourceLayerIndexNo].X;
            SourceY = sgl.GraphicsLayerList[SourceLayerIndexNo].Y;
            SourceWidth = sgl.GraphicsLayerList[SourceLayerIndexNo].Width;
            SourceHeight = sgl.GraphicsLayerList[SourceLayerIndexNo].Height;
          }
          if (DestLayerIndexNo == GuiLib_GRAPHICS_LAYER_BASE)
          {
#ifdef GuiConst_DISPLAY_BUFFER_EASYGUI
#ifdef GuiLib_COLOR_UNIT_16
            DestAddress = &(GuiLib_DisplayBuf.Bytes[0][0]);
#else
  #ifdef GuiConst_COLOR_DEPTH_2
    #ifdef GuiConst_BYTE_HORIZONTAL
      #ifdef GuiConst_COLOR_PLANES_2
            DestAddress = &(GuiLib_DisplayBuf[0][0][0]);
      #else
            DestAddress = &(GuiLib_DisplayBuf[0][0]);
      #endif
    #else
      #ifdef GuiConst_COLOR_PLANES_2
            DestAddress = &(GuiLib_DisplayBuf[0][0][0]);
      #else
            DestAddress = &(GuiLib_DisplayBuf[0][0]);
      #endif
    #endif
  #else
            DestAddress = &(GuiLib_DisplayBuf[0][0]);
  #endif
#endif
#else // GuiConst_DISPLAY_BUFFER_EASYGUI
            DestAddress = 0;
#endif // GuiConst_DISPLAY_BUFFER_EASYGUI
            DestLineSize = GuiConst_BYTES_PR_LINE;
            DestX = SourceX;
            DestY = SourceY;
            DestWidth = SourceWidth;
            DestHeight = SourceHeight;
          }
          else
          {
            DestAddress = sgl.GraphicsLayerList[DestLayerIndexNo].BaseAddress;
            DestLineSize = sgl.GraphicsLayerList[DestLayerIndexNo].LineSize;
            DestX = sgl.GraphicsLayerList[DestLayerIndexNo].X;
            DestY = sgl.GraphicsLayerList[DestLayerIndexNo].Y;
            DestWidth = sgl.GraphicsLayerList[DestLayerIndexNo].Width;
            DestHeight = sgl.GraphicsLayerList[DestLayerIndexNo].Height;
          }

          if ((DestX <= SourceX) && (DestY <= SourceY) &&
              (DestX + DestWidth >= SourceX + SourceWidth) &&
              (DestY + DestHeight >= SourceY + SourceHeight) &&
              (sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].
               GraphicsFilterFunc != 0))
          {
            if (DestLayerIndexNo != GuiLib_GRAPHICS_LAYER_BASE)
            {
              DestX = SourceX - DestX;
              DestY = SourceY - DestY;
            }
            if (SourceLayerIndexNo != GuiLib_GRAPHICS_LAYER_BASE)
            {
              SourceX = 0;
              SourceY = 0;
            }

            for (I = 0; I <= 9; I++)
            {
              if (sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].ParVarType[I] ==
                  GuiLib_VAR_NONE)
                FilterPars[I] =
                   sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].ParValueNum[I];
              else
                FilterPars[I] = ReadVar(
                   sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].ParVarPtr[I],
                   sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].ParVarType[I]);
            }
            sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].GraphicsFilterFunc(
               DestAddress + DestY * DestLineSize +
                  DestX * GuiConst_PIXEL_BYTE_SIZE,
               DestLineSize,
               SourceAddress + SourceY * SourceLineSize +
                  SourceX * GuiConst_PIXEL_BYTE_SIZE,
               SourceLineSize,
               sgl.GraphicsLayerList[SourceLayerIndexNo].Width,
               sgl.GraphicsLayerList[SourceLayerIndexNo].Height,
               FilterPars);
          }

          GraphicsLayer_Pop(
             sgl.GraphicsFilterList[sgl.GlobalGraphicsFilterIndex].ContAtLayerIndexNo);
          MarkDisplayBoxRepaint(
             DestX,
             DestY,
             DestX + sgl.GraphicsLayerList[SourceLayerIndexNo].Width - 1,
             DestY + sgl.GraphicsLayerList[SourceLayerIndexNo].Height - 1);
        }
        else
          GraphicsLayer_Pop(GuiLib_GRAPHICS_LAYER_BASE);

        if (sgl.BaseLayerDrawing)
        {
          sgl.LayerOrigoX = 0;
          sgl.LayerOrigoY = 0;
        }
        else
        {
          I = sgl.GraphicsLayerLifo[sgl.GraphicsLayerLifoCnt - 1];
          sgl.LayerOrigoX = -sgl.GraphicsLayerList[I].X;
          sgl.LayerOrigoY = -sgl.GraphicsLayerList[I].Y;
        }
        sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
        sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;

        if (sgl.BaseLayerDrawing)
        {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
          StartClipping(0);
#endif
        }
        else
        {
          I = sgl.GraphicsLayerLifo[sgl.GraphicsLayerLifoCnt - 1];
          sgl.CurItem.ClipRectX1 = 0;
          sgl.CurItem.ClipRectY1 = 0;
          sgl.CurItem.ClipRectX2 = sgl.GraphicsLayerList[I].Width - 1;
          sgl.CurItem.ClipRectY2 = sgl.GraphicsLayerList[I].Height - 1;
#ifdef GuiConst_CLIPPING_SUPPORT_ON
          StartClipping(1);
#endif
        }
      }
#endif
      break;
#endif
  }

  if (sgl.CurItem.UpdateType == GuiLib_UPDATE_ON_CHANGE)
    if ((sgl.InitialDrawing) || (sgl.AutoRedrawUpdate == GuiLib_TRUE))
      AutoRedraw_UpdateOnChange(sgl.AutoRedrawSaveIndex);

  if (sgl.AutoRedrawSaveIndex >= 0)
    AutoRedraw_UpdateDrawn(sgl.AutoRedrawSaveIndex, &sgl.CurItem);

#ifdef GuiConst_TEXTBOX_FIELDS_ON
  if (sgl.InitialDrawing)
  {
    if (((sgl.CurItem.ItemType == GuiLib_ITEM_TEXTBLOCK) ||
         (sgl.CurItem.ItemType == GuiLib_ITEM_VARBLOCK)) &&
         (sgl.CurItem.CompPars.CompTextBox.ScrollIndex != 0xFF))
      memcpy(&sgl.TextboxScrollItems[sgl.CurItem.CompPars.CompTextBox.ScrollIndex],
             &sgl.CurItem,
             sizeof(GuiLib_ItemRec));
  }
#endif

#ifdef GuiConst_CURSOR_SUPPORT_ON
  if (sgl.InitialDrawing && IsCursorField)
    sgl.CurItem.CursorFieldLevel--;

  if ((ColorInvert == GuiLib_COL_INVERT_IF_CURSOR) && FoundActiveCursorField)
    sgl.SwapColors = 0;
#endif

#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  if (!sgl.InitialDrawing)
  {
    sgl.CoordOrigoX = sgl.DisplayOrigoX + sgl.LayerOrigoX;
    sgl.CoordOrigoY = sgl.DisplayOrigoY + sgl.LayerOrigoY;
  }
#endif
#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (!sgl.InitialDrawing && sgl.DisplayWriting)
    GuiLib_ResetClipping();
#endif
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static void ReadItem(GuiConst_INT16S LanguageIndex)
{
  GuiConst_INT16S X;
  GuiConst_INT16S N;
  GuiConst_INT16S I,J;
  GuiConst_INTCOLOR TmpForeColor, TmpBackColor;
  GuiConst_INTCOLOR *ColVarPtr;
  GuiConst_INT16U ColVarPtrIdx, PtrIdx;
  GuiConst_INT16U TmpColIdx;
  GuiConst_INT16U TmpForeColIdx;
#ifdef GuiConst_ADV_CONTROLS
  GuiConst_INT16U TmpBackColIdx;
#endif
  GuiConst_INT8U B1;
  GuiConst_INT16U W1;
#ifdef GuiConst_REMOTE_TEXT_DATA
  GuiConst_INT16U Ti;
#endif
  GuiConst_INT16U TxtSize;
#ifndef GuiConst_REMOTE_TEXT_DATA
  GuiConst_INT16U TxtSum1, TxtSum2;
#endif
#ifdef GuiConst_REMOTE_STRUCT_DATA
  GuiConst_INT16U TxtReadSize;
#endif
#ifdef GuiConst_CURSOR_SUPPORT_ON
  GuiConst_INT8S BS1;
#endif
#ifdef GuiConst_BLINK_SUPPORT_ON
  GuiConst_INT8S BS2;
#endif
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  GuiConst_INT8U ScrollBoxIndex;
#ifndef GuiConst_SCROLLITEM_BAR_NONE
#ifdef GuiConst_REMOTE_BITMAP_DATA
  GuiConst_INT8U * PixelDataPtr;
  GuiConst_INT8U BitmapHeader[4];
#else
  GuiConst_INT8U PrefixRom * PixelDataPtr;
#endif // GuiConst_REMOTE_BITMAP_DATA
#endif // GuiConst_SCROLLITEM_BAR_NONE
#endif // GuiConst_ITEM_SCROLLBOX_INUSE
#ifdef GuiConst_ADV_COMPONENTS
  GuiConst_INT8U L;
#endif
#ifdef GuiConst_ITEM_GRAPH_INUSE
  GuiConst_INT8U GraphIndex;
  GuiConst_INT8U Axis;
  GuiConst_INT8U B;
#endif
#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  GuiConst_INT8U GraphicsLayerIndex;
  GuiConst_INT8U GraphicsFilterIndex;
#endif
#ifdef GuiConst_ITEM_STRUCTCOND_INUSE
  GuiConst_INT16U CondI;
#endif
  GuiConst_INT8U ColMemoryFore;
  GuiConst_INT8U ColMemoryBack;

  sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_INUSE;

#ifdef GuiConst_REL_COORD_ORIGO_INUSE
  if (sgl.DisplayLevel == 0)
  {
    sgl.CurItem.CoordOrigoX = sgl.CoordOrigoX;
    sgl.CurItem.CoordOrigoY = sgl.CoordOrigoY;
  }
#endif

  sgl.CommonByte0 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte1 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte2 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte3 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte4 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte5 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte6 = GetItemByte(&sgl.ItemDataPtr);
  sgl.CommonByte7 = GetItemByte(&sgl.ItemDataPtr);

  sgl.CurItem.ItemType = sgl.CommonByte0 & 0x3F;
  if (sgl.CurItem.ItemType < 32)
    sgl.ItemTypeBit1 = (GuiConst_INT32U)0x00000001 << (sgl.CurItem.ItemType & 0x1F);
  else
    sgl.ItemTypeBit1 = 0;
  if (sgl.CurItem.ItemType >= 32)
    sgl.ItemTypeBit2 = (GuiConst_INT32U)0x00000001 << (sgl.CurItem.ItemType - 32);
  else
    sgl.ItemTypeBit2 = 0;

  if (sgl.CommonByte7 & 0x20)
  {
    B1 = GetItemByte(&sgl.ItemDataPtr);
    if (B1 & 0x01)
      sgl.CurItem.ForeColorEnhance = GetItemWord(&sgl.ItemDataPtr);
    if (B1 & 0x02)
      sgl.CurItem.BackColorEnhance = GetItemWord(&sgl.ItemDataPtr);
    if (B1 & 0x04)
      sgl.CurItem.BarForeColorEnhance = GetItemWord(&sgl.ItemDataPtr);
    if (B1 & 0x08)
      sgl.CurItem.BarBackColorEnhance = GetItemWord(&sgl.ItemDataPtr);
  }
  else
  {
    sgl.CurItem.ForeColorEnhance = 0;
    sgl.CurItem.BackColorEnhance = 0;
    sgl.CurItem.BarForeColorEnhance = 0;
    sgl.CurItem.BarBackColorEnhance = 0;
  }
  if (sgl.CommonByte7 & 0x40)
  {
    ColMemoryFore = GetItemByte(&sgl.ItemDataPtr);
    ColMemoryBack = GetItemByte(&sgl.ItemDataPtr);
  }
  else
  {
    ColMemoryFore = 0;
    ColMemoryBack = 0;
  }

  TmpForeColor = sgl.CurItem.ForeColor;
  TmpForeColIdx = sgl.CurItem.ForeColorIndex;

  TmpBackColor = sgl.CurItem.BackColor;
#ifdef GuiConst_ADV_CONTROLS
  TmpBackColIdx = sgl.CurItem.BackColorIndex;
#endif

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_DOT +
                           GuiLib_ITEMBIT_LINE +
                           GuiLib_ITEMBIT_FRAME +
                           GuiLib_ITEMBIT_BLOCK +
                           GuiLib_ITEMBIT_CIRCLE +
                           GuiLib_ITEMBIT_ELLIPSE +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK +
                           GuiLib_ITEMBIT_SCROLLBOX +
                           GuiLib_ITEMBIT_GRAPH)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                           GuiLib_ITEMBIT_ROUNDEDBLOCK +
                           GuiLib_ITEMBIT_QUARTERCIRCLE +
                           GuiLib_ITEMBIT_QUARTERELLIPSE +
                           GuiLib_ITEMBIT_CHECKBOX +
                           GuiLib_ITEMBIT_RADIOBUTTON +
                           GuiLib_ITEMBIT_BUTTON +
                           GuiLib_ITEMBIT_EDITBOX +
                           GuiLib_ITEMBIT_PANEL +
                           GuiLib_ITEMBIT_MEMO +
                           GuiLib_ITEMBIT_LISTBOX +
                           GuiLib_ITEMBIT_COMBOBOX +
                           GuiLib_ITEMBIT_SCROLLAREA +
                           GuiLib_ITEMBIT_PROGRESSBAR +
                           GuiLib_ITEMBIT_STRUCTCOND)))
  {
    B1 = ColMemoryFore & 0x03;
    if (B1)
    {
      sgl.CurItem.ForeColor = sgl.Memory.C[B1-1];
      sgl.CurItem.ForeColorIndex = sgl.ColMemoryIndex[B1-1];
    }
    else
    {
      TmpColIdx = sgl.CurItem.ForeColorIndex;
      sgl.CurItem.ForeColorIndex = 0xFFFF;
      switch (sgl.CommonByte3 & 0x07)
      {
        case GuiLib_COLOR_FORE:
          sgl.CurItem.ForeColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.CurItem.ForeColor = GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.CurItem.ForeColor = (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.CurItem.ForeColor = TmpBackColor;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.CurItem.ForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
          {
            sgl.CurItem.ForeColor = GuiConst_PIXEL_ON;
          }
          else
          {
            sgl.CurItem.ForeColor = *ColVarPtr;
            sgl.CurItem.ForeColorIndex = ColVarPtrIdx;
          }
          break;
        default:
          sgl.CurItem.ForeColorIndex = TmpColIdx;
          break;
      }
    }
    if (sgl.CurItem.ForeColorEnhance > 0)
      sgl.CurItem.ForeColor = GuiLib_BrightenPixelColor(
         sgl.CurItem.ForeColor, sgl.CurItem.ForeColorEnhance);
    else if (sgl.CurItem.ForeColorEnhance < 0)
      sgl.CurItem.ForeColor = GuiLib_DarkenPixelColor(
         sgl.CurItem.ForeColor, -sgl.CurItem.ForeColorEnhance);

    B1 = ColMemoryFore & 0x30;
    if (B1)
    {
      sgl.CurItem.BarForeColor = sgl.Memory.C[B1-1];
      sgl.CurItem.BarForeColorIndex = sgl.ColMemoryIndex[B1-1];
    }
    else
    {
      TmpColIdx = sgl.CurItem.BarForeColorIndex;
      sgl.CurItem.BarForeColorIndex = 0xFFFF;
      switch ((sgl.CommonByte5 >> 1) & 0x07)
      {
        case GuiLib_COLOR_FORE:
          sgl.CurItem.BarForeColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.CurItem.BarForeColor = GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.CurItem.BarForeColor = (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.CurItem.BarForeColor = sgl.CurItem.BackColor;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.CurItem.BarForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
          {
            sgl.CurItem.BarForeColor = GuiConst_PIXEL_ON;
          }
          else
          {
            sgl.CurItem.BarForeColor = *ColVarPtr;
            sgl.CurItem.BarForeColorIndex = ColVarPtrIdx;
          }
          break;
        default:
          sgl.CurItem.BarForeColorIndex = TmpColIdx;
          break;
      }
    }
    if (sgl.CurItem.BarForeColorEnhance > 0)
      sgl.CurItem.BarForeColor = GuiLib_BrightenPixelColor(
         sgl.CurItem.BarForeColor, sgl.CurItem.BarForeColorEnhance);
    else if (sgl.CurItem.BarForeColorEnhance < 0)
      sgl.CurItem.BarForeColor = GuiLib_DarkenPixelColor(
         sgl.CurItem.BarForeColor, -sgl.CurItem.BarForeColorEnhance);
  }

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CLEARAREA +
                           GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_FRAME +
                           GuiLib_ITEMBIT_CIRCLE +
                           GuiLib_ITEMBIT_ELLIPSE +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK +
                           GuiLib_ITEMBIT_SCROLLBOX +
                           GuiLib_ITEMBIT_GRAPH +
                           GuiLib_ITEMBIT_GRAPHICSLAYER)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                           GuiLib_ITEMBIT_QUARTERCIRCLE +
                           GuiLib_ITEMBIT_QUARTERELLIPSE +
                           GuiLib_ITEMBIT_CHECKBOX +
                           GuiLib_ITEMBIT_RADIOBUTTON +
                           GuiLib_ITEMBIT_BUTTON +
                           GuiLib_ITEMBIT_EDITBOX +
                           GuiLib_ITEMBIT_PANEL +
                           GuiLib_ITEMBIT_MEMO +
                           GuiLib_ITEMBIT_LISTBOX +
                           GuiLib_ITEMBIT_COMBOBOX +
                           GuiLib_ITEMBIT_SCROLLAREA +
                           GuiLib_ITEMBIT_PROGRESSBAR +
                           GuiLib_ITEMBIT_STRUCTCOND)))
  {
    B1 = ColMemoryBack & 0x03;
    if (B1)
    {
      sgl.CurItem.BackColor = sgl.Memory.C[B1-1];
      sgl.CurItem.BackColorIndex = sgl.ColMemoryIndex[B1-1];
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
    }
    else
    {
      TmpColIdx = sgl.CurItem.BackColorIndex;
      sgl.CurItem.BackColorIndex = 0xFFFF;
      switch ((sgl.CommonByte3 >> 3) & 0x07)
      {
        case GuiLib_COLOR_FORE:
          sgl.CurItem.BackColor = GuiConst_PIXEL_ON;
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
          break;
        case GuiLib_COLOR_BACK:
          sgl.CurItem.BackColor = GuiConst_PIXEL_OFF;
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.CurItem.BackColor = GetItemColor(&sgl.ItemDataPtr);
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
          break;
        case GuiLib_COLOR_INVERT:
          sgl.CurItem.BackColor = TmpForeColor;
          sgl.CurItem.BackColorIndex = TmpForeColIdx;
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_TRANSPARENT;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.CurItem.BackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
          {
            sgl.CurItem.BackColor = GuiConst_PIXEL_ON;
          }
          else
          {
            sgl.CurItem.BackColor = *ColVarPtr;
            sgl.CurItem.BackColorIndex = ColVarPtrIdx;
          }
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSPARENT;
          break;
        default:
          sgl.CurItem.BackColorIndex = TmpColIdx;
          break;
      }
    }
    if (sgl.CurItem.BackColorEnhance > 0)
      sgl.CurItem.BackColor = GuiLib_BrightenPixelColor(
         sgl.CurItem.BackColor, sgl.CurItem.BackColorEnhance);
    else if (sgl.CurItem.BackColorEnhance < 0)
      sgl.CurItem.BackColor = GuiLib_DarkenPixelColor(
         sgl.CurItem.BackColor, -sgl.CurItem.BackColorEnhance);

    B1 = ColMemoryBack & 0x30;
    if (B1)
    {
      sgl.CurItem.BarBackColor = sgl.Memory.C[B1-1];
      sgl.CurItem.BarBackColorIndex = sgl.ColMemoryIndex[B1-1];
    }
    else
    {
      TmpColIdx = sgl.CurItem.BarBackColorIndex;
      sgl.CurItem.BarBackColorIndex = 0xFFFF;
      switch ((sgl.CommonByte5 >> 4) & 0x07)
      {
        case GuiLib_COLOR_FORE:
          sgl.CurItem.BarBackColor = GuiConst_PIXEL_ON;
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
          break;
        case GuiLib_COLOR_BACK:
          sgl.CurItem.BarBackColor = GuiConst_PIXEL_OFF;
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.CurItem.BarBackColor = GetItemColor(&sgl.ItemDataPtr);
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
          break;
        case GuiLib_COLOR_INVERT:
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
          sgl.CurItem.BarBackColor = sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_BARTRANSPARENT;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.CurItem.BarBackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
          {
            sgl.CurItem.BarBackColor = GuiConst_PIXEL_ON;
          }
          else
          {
            sgl.CurItem.BarBackColor = *ColVarPtr;
            sgl.CurItem.BarBackColorIndex = ColVarPtrIdx;
          }
          sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BARTRANSPARENT;
          break;
        default:
          sgl.CurItem.BarBackColorIndex = TmpColIdx;
          break;
      }
    }
    if (sgl.CurItem.BarBackColorEnhance > 0)
      sgl.CurItem.BarBackColor = GuiLib_BrightenPixelColor(
         sgl.CurItem.BarBackColor, sgl.CurItem.BarBackColorEnhance);
    else if (sgl.CurItem.BarBackColorEnhance < 0)
      sgl.CurItem.BarBackColor = GuiLib_DarkenPixelColor(
         sgl.CurItem.BarBackColor, -sgl.CurItem.BarBackColorEnhance);
  }

#ifdef GuiConst_COLOR_DEPTH_1
  #ifdef GuiConst_BITMAP_SUPPORT_ON
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_BITMAP +
                           GuiLib_ITEMBIT_BACKGROUND)) &&
      (sgl.CommonByte6 & 0x02))
    sgl.CurItem.CompPars.CompBitmap.TranspColor = (sgl.CommonByte6 >> 2) & 0x01;
  #endif
#else
  #ifdef GuiConst_BITMAP_SUPPORT_ON
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_BITMAP +
                           GuiLib_ITEMBIT_BACKGROUND)) &&
      (sgl.CommonByte6 & 0x02))
  {
    sgl.CurItem.CompPars.CompBitmap.TranspColor = GetItemColor(&sgl.ItemDataPtr);
  }
  #endif
#endif

  B1 = ColMemoryFore & 0x0C;
  if (B1)
  {
    sgl.Memory.C[B1 - 1] = sgl.CurItem.ForeColor;
    sgl.ColMemoryIndex[B1 - 1] = sgl.CurItem.ForeColorIndex;
  }
  B1 = ColMemoryFore & 0xC0;
  if (B1)
  {
    sgl.Memory.C[B1 - 1] = sgl.CurItem.BarForeColor;
    sgl.ColMemoryIndex[B1 - 1] = sgl.CurItem.BarForeColorIndex;
  }
  B1 = ColMemoryBack & 0x0C;
  if (B1)
  {
    sgl.Memory.C[B1 - 1] = sgl.CurItem.BackColor;
    sgl.ColMemoryIndex[B1 - 1] = sgl.CurItem.BackColorIndex;
  }
  B1 = ColMemoryBack & 0xC0;
  if (B1)
  {
    sgl.Memory.C[B1 - 1] = sgl.CurItem.BarBackColor;
    sgl.ColMemoryIndex[B1 - 1] = sgl.CurItem.BarBackColorIndex;
  }

  if (sgl.CommonByte0 & 0x40)
    B1 = GetItemByte(&sgl.ItemDataPtr);
  else
    B1 = 0;
  sgl.X1MemoryRead = B1 & 0x03;
  sgl.X1MemoryWrite = (B1 >> 2) & 0x03;
  sgl.Y1MemoryRead = (B1 >> 4) & 0x03;
  sgl.Y1MemoryWrite = (B1 >> 6) & 0x03;
  if (sgl.CommonByte0 & 0x80)
    B1 = GetItemByte(&sgl.ItemDataPtr);
  else
    B1 = 0;
  sgl.X2MemoryRead = B1 & 0x03;
  sgl.X2MemoryWrite = (B1 >> 2) & 0x03;
  sgl.Y2MemoryRead = (B1 >> 4) & 0x03;
  sgl.Y2MemoryWrite = (B1 >> 6) & 0x03;
  if (sgl.CommonByte7 & 0x01)
    B1 = GetItemByte(&sgl.ItemDataPtr);
  else
    B1 = 0;
  sgl.R1MemoryRead = B1 & 0x03;
  sgl.R1MemoryWrite = (B1 >> 2) & 0x03;
  sgl.R2MemoryRead = (B1 >> 4) & 0x03;
  sgl.R2MemoryWrite = (B1 >> 6) & 0x03;
  if (sgl.CommonByte7 & 0x80)
  {
    if (sgl.X1MemoryRead)
      sgl.X1MemoryRead += GuiLib_MEMORY_CNT;
    if (sgl.X1MemoryWrite)
      sgl.X1MemoryWrite += GuiLib_MEMORY_CNT;
    if (sgl.Y1MemoryRead)
      sgl.Y1MemoryRead += GuiLib_MEMORY_CNT;
    if (sgl.Y1MemoryWrite)
      sgl.Y1MemoryWrite += GuiLib_MEMORY_CNT;
    if (sgl.X2MemoryRead)
      sgl.X2MemoryRead += GuiLib_MEMORY_CNT;
    if (sgl.X2MemoryWrite)
      sgl.X2MemoryWrite += GuiLib_MEMORY_CNT;
    if (sgl.Y2MemoryRead)
      sgl.Y2MemoryRead += GuiLib_MEMORY_CNT;
    if (sgl.Y2MemoryWrite)
      sgl.Y2MemoryWrite += GuiLib_MEMORY_CNT;
    if (sgl.R1MemoryRead)
      sgl.R1MemoryRead += GuiLib_MEMORY_CNT;
    if (sgl.R1MemoryWrite)
      sgl.R1MemoryWrite += GuiLib_MEMORY_CNT;
    if (sgl.R2MemoryRead)
      sgl.R2MemoryRead += GuiLib_MEMORY_CNT;
    if (sgl.R2MemoryWrite)
      sgl.R2MemoryWrite += GuiLib_MEMORY_CNT;
  }

  if (sgl.CommonByte6 & 0x01)
    B1 = GetItemByte(&sgl.ItemDataPtr);
  else
    B1 = 0;
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_STRUCTCOND)))
    sgl.CurItem.TextPar[0].BackBorderPixels = B1;
  else
    sgl.CurItem.TextPar[0].BackBorderPixels = 0;

  if (sgl.CommonByte6 & 0x08)
  {
    N = GetItemWord(&sgl.ItemDataPtr);
    sgl.CurItem.TextPar[0].BackBoxSizeY1 = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.TextPar[0].BackBoxSizeY2 = GetItemByte(&sgl.ItemDataPtr);
  }
  else
    N = 0;
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_STRUCTCOND)))
    sgl.CurItem.TextPar[0].BackBoxSizeX = N;
  else
    sgl.CurItem.TextPar[0].BackBoxSizeX = 0;
#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_SCROLLBOX)
  {
    if (sgl.NextScrollLineReading)
      if (sgl.CommonByte6 & 0x08)
      {
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].LineSizeX =
           sgl.CurItem.TextPar[0].BackBoxSizeX;
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].LineSizeY =
           sgl.CurItem.TextPar[0].BackBoxSizeY1;
        sgl.ScrollBoxesAry[sgl.GlobalScrollBoxIndex].LineSizeY2 =
           sgl.CurItem.TextPar[0].BackBoxSizeY2;
      }
  }
#endif

  sgl.X1Mode = sgl.CommonByte1 & 0x03;
  sgl.Y1Mode = (sgl.CommonByte1 >> 2) & 0x03;
  sgl.X2Mode = (sgl.CommonByte1 >> 4) & 0x03;
  sgl.Y2Mode = (sgl.CommonByte1 >> 6) & 0x03;
  sgl.R1Mode = (sgl.CommonByte7 >> 1) & 0x03;
  sgl.R2Mode = (sgl.CommonByte7 >> 3) & 0x03;

  if (sgl.CommonByte2 & 0x01)
    sgl.ItemX1 = GetItemWord(&sgl.ItemDataPtr);
  else
    sgl.ItemX1 = 0;
  if (sgl.CommonByte2 & 0x02)
    sgl.ItemY1 = GetItemWord(&sgl.ItemDataPtr);
  else
    sgl.ItemY1 = 0;
  if (sgl.CommonByte2 & 0x04)
    sgl.ItemX2 = GetItemWord(&sgl.ItemDataPtr);
  else
    sgl.ItemX2 = 0;
  if (sgl.CommonByte2 & 0x08)
    sgl.ItemY2 = GetItemWord(&sgl.ItemDataPtr);
  else
    sgl.ItemY2 = 0;
  if (sgl.CommonByte6 & 0x10)
    sgl.ItemR1 = GetItemWord(&sgl.ItemDataPtr);
  else
    sgl.ItemR1 = 0;
  if (sgl.CommonByte6 & 0x20)
    sgl.ItemR2 = GetItemWord(&sgl.ItemDataPtr);
  else
    sgl.ItemR2 = 0;

  if (sgl.CommonByte2 & 0x10)
  {
    sgl.X1VarIdx = GetItemWord(&sgl.ItemDataPtr);
    if (sgl.X1VarIdx >= GuiStruct_VarPtrCnt)
      sgl.X1VarIdx = 0;
  }
  if (sgl.CommonByte2 & 0x20)
  {
    sgl.Y1VarIdx = GetItemWord(&sgl.ItemDataPtr);
    if (sgl.Y1VarIdx >= GuiStruct_VarPtrCnt)
      sgl.Y1VarIdx = 0;
  }
  if (sgl.CommonByte2 & 0x40)
  {
    sgl.X2VarIdx = GetItemWord(&sgl.ItemDataPtr);
    if (sgl.X2VarIdx >= GuiStruct_VarPtrCnt)
      sgl.X2VarIdx = 0;
  }
  if (sgl.CommonByte2 & 0x80)
  {
    sgl.Y2VarIdx = GetItemWord(&sgl.ItemDataPtr);
    if (sgl.Y2VarIdx >= GuiStruct_VarPtrCnt)
      sgl.Y2VarIdx = 0;
  }
  if (sgl.CommonByte6 & 0x40)
  {
    sgl.R1VarIdx = GetItemWord(&sgl.ItemDataPtr);
    if (sgl.R1VarIdx >= GuiStruct_VarPtrCnt)
      sgl.R1VarIdx = 0;
  }
  if (sgl.CommonByte6 & 0x80)
  {
    sgl.R2VarIdx = GetItemWord(&sgl.ItemDataPtr);
    if (sgl.R2VarIdx >= GuiStruct_VarPtrCnt)
      sgl.R2VarIdx = 0;
  }
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_CLEARAREA +
                           GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_LINE +
                           GuiLib_ITEMBIT_FRAME +
                           GuiLib_ITEMBIT_BLOCK +
                           GuiLib_ITEMBIT_CIRCLE +
                           GuiLib_ITEMBIT_ELLIPSE +
                           GuiLib_ITEMBIT_BITMAP +
                           GuiLib_ITEMBIT_BACKGROUND +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_ACTIVEAREA +
                           GuiLib_ITEMBIT_CLIPRECT +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK +
                           GuiLib_ITEMBIT_TOUCHAREA +
                           GuiLib_ITEMBIT_SCROLLBOX +
                           GuiLib_ITEMBIT_GRAPH +
                           GuiLib_ITEMBIT_GRAPHICSLAYER)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                           GuiLib_ITEMBIT_ROUNDEDBLOCK +
                           GuiLib_ITEMBIT_QUARTERCIRCLE +
                           GuiLib_ITEMBIT_QUARTERELLIPSE +
                           GuiLib_ITEMBIT_CHECKBOX +
                           GuiLib_ITEMBIT_RADIOBUTTON +
                           GuiLib_ITEMBIT_BUTTON +
                           GuiLib_ITEMBIT_EDITBOX +
                           GuiLib_ITEMBIT_PANEL +
                           GuiLib_ITEMBIT_MEMO +
                           GuiLib_ITEMBIT_LISTBOX +
                           GuiLib_ITEMBIT_COMBOBOX +
                           GuiLib_ITEMBIT_SCROLLAREA +
                           GuiLib_ITEMBIT_PROGRESSBAR +
                           GuiLib_ITEMBIT_STRUCTCOND)))
  {
    B1 = (sgl.CommonByte3 >> 6) & 0x03;
    if (B1 != GuiLib_ALIGN_NOCHANGE)
      sgl.CurItem.TextPar[0].Alignment = B1;
  }

  B1 = sgl.CommonByte4 & 0x03;
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_EDITBOX +
                           GuiLib_ITEMBIT_MEMO +
                           GuiLib_ITEMBIT_LISTBOX +
                           GuiLib_ITEMBIT_COMBOBOX +
                           GuiLib_ITEMBIT_PROGRESSBAR +
                           GuiLib_ITEMBIT_STRUCTCOND)))
  {
    if (B1 != GuiLib_PS_NOCHANGE)
      sgl.CurItem.TextPar[0].Ps = B1;
  }

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_EDITBOX +
                           GuiLib_ITEMBIT_MEMO +
                           GuiLib_ITEMBIT_LISTBOX +
                           GuiLib_ITEMBIT_COMBOBOX +
                           GuiLib_ITEMBIT_PROGRESSBAR +
                           GuiLib_ITEMBIT_STRUCTCOND)))
  {
    if (sgl.CommonByte4 & 0x10)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_UNDERLINE;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_UNDERLINE;
  }
  else if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_LINE +
                               GuiLib_ITEMBIT_CIRCLE))
  {
    if (sgl.CommonByte4 & 0x10)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_PATTERNEDLINE;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_PATTERNEDLINE;
  }

  if (sgl.CommonByte4 & 0x20)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_AUTOREDRAWFIELD;
  else
    sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_AUTOREDRAWFIELD;

#ifdef GuiConst_CURSOR_SUPPORT_ON
  if (sgl.CommonByte4 & 0x40)
    sgl.CurItem.CursorFieldNo = GetItemByte(&sgl.ItemDataPtr);
  else
    sgl.CurItem.CursorFieldNo = -1;
#endif

#ifdef GuiConst_BLINK_SUPPORT_ON
  if (sgl.CommonByte5 & 0x01)
    BS2 = GetItemByte(&sgl.ItemDataPtr);
  else
    BS2 = -1;
  if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                          GuiLib_ITEMBIT_TEXTBLOCK +
                          GuiLib_ITEMBIT_VAR +
                          GuiLib_ITEMBIT_VARBLOCK))
    sgl.CurItem.BlinkFieldNo = BS2;
  else
    sgl.CurItem.BlinkFieldNo = -1;
#endif

  if (sgl.CommonByte4 & 0x80)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_TRANSLATION;
  else
    sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_TRANSLATION;
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_BUTTON)))
  {
    if (sgl.CommonByte5 & 0x80)
      B1 = GetItemByte(&sgl.ItemDataPtr);
    else
    {
      if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_VAR +
                               GuiLib_ITEMBIT_VARBLOCK)) ||
          (sgl.CommonByte4 & 0x80))
        I = LanguageIndex;
      else
        I = 0;
      B1 = ReadByte(GuiFont_LanguageTextDir[I]);
    }
  }
  else
    B1 = 0;
  if (B1)
    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_REVERSEWRITING;
  else
    sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_REVERSEWRITING;

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_LINE)) &&
      (sgl.CurItem.TextPar[0].BitFlags & GuiLib_BITFLAG_PATTERNEDLINE))
    sgl.CurItem.LinePattern = GetItemByte(&sgl.ItemDataPtr);

#ifdef GuiConst_BLINK_SUPPORT_ON
  if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                          GuiLib_ITEMBIT_TEXTBLOCK +
                          GuiLib_ITEMBIT_VAR +
                          GuiLib_ITEMBIT_VARBLOCK))
  {
    if (sgl.CommonByte5 & 0x01)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_BLINKTEXTFIELD;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_BLINKTEXTFIELD;
  }
#endif

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_BUTTON)))
    sgl.CurItem.TextCnt = GetItemByte(&sgl.ItemDataPtr);
  else
    sgl.CurItem.TextCnt = 1;
  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK +
                           GuiLib_ITEMBIT_SCROLLBOX +
                           GuiLib_ITEMBIT_GRAPH)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_BUTTON +
                           GuiLib_ITEMBIT_EDITBOX +
                           GuiLib_ITEMBIT_LISTBOX +
                           GuiLib_ITEMBIT_COMBOBOX +
                           GuiLib_ITEMBIT_PROGRESSBAR +
                           GuiLib_ITEMBIT_STRUCTCOND)))
  {
    if (sgl.CurItem.ItemType == GuiLib_ITEM_BUTTON)
      J = 3;
    else
      J = 1;
    for (I = 0; I < J; I++)
    {
      B1 = GetItemByte(&sgl.ItemDataPtr);
      if (B1 != 0xFF)
        sgl.CurItem.TextPar[I].FontIndex = B1 + 1;
    }
  }

  if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXTBLOCK +
                          GuiLib_ITEMBIT_VARBLOCK))
  {
#ifdef GuiConst_TEXTBOX_FIELDS_ON
    sgl.CurItem.CompPars.CompTextBox.ScrollIndex = GetItemByte(&sgl.ItemDataPtr);
#else
    B1 = GetItemByte(&sgl.ItemDataPtr);
#endif
  }

  sgl.CurItem.UpdateType = GuiLib_AUTOREDRAW_MODE;

  if (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_CHECKBOX +
                          GuiLib_ITEMBIT_RADIOBUTTON +
                          GuiLib_ITEMBIT_BUTTON +
                          GuiLib_ITEMBIT_EDITBOX +
                          GuiLib_ITEMBIT_LISTBOX +
                          GuiLib_ITEMBIT_COMBOBOX +
                          GuiLib_ITEMBIT_PROGRESSBAR))
  {
    PtrIdx = GetItemWord(&sgl.ItemDataPtr);
    if (PtrIdx >= GuiStruct_VarPtrCnt)
      PtrIdx = 0;

    sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_AUTOREDRAWFIELD;
    sgl.CurItem.UpdateType = GuiLib_UPDATE_ON_CHANGE;
    sgl.CurItem.VarPtr = (void*)ReadWord(GuiStruct_VarPtrList[PtrIdx]);
    sgl.CurItem.VarType = ReadByte(GuiStruct_VarTypeList[PtrIdx]);
  }

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_VAR +
                           GuiLib_ITEMBIT_VARBLOCK)) ||
      (sgl.ItemTypeBit2 &  GuiLib_ITEMBIT_STRUCTCOND))
  {
    PtrIdx = GetItemWord(&sgl.ItemDataPtr);
    if (PtrIdx >= GuiStruct_VarPtrCnt)
      PtrIdx = 0;

    sgl.CurItem.UpdateType = GuiLib_AUTOREDRAW_MODE;
    sgl.CurItem.VarPtr = (void*)ReadWord(GuiStruct_VarPtrList[PtrIdx]);
    sgl.CurItem.VarType = ReadByte(GuiStruct_VarTypeList[PtrIdx]);

  }

#ifdef GuiConst_AUTOREDRAW_ON_CHANGE
  if ((sgl.CommonByte4 & 0x20) &&
     ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK +
                           GuiLib_ITEMBIT_DOT +
                           GuiLib_ITEMBIT_LINE +
                           GuiLib_ITEMBIT_FRAME +
                           GuiLib_ITEMBIT_BLOCK +
                           GuiLib_ITEMBIT_CIRCLE +
                           GuiLib_ITEMBIT_ELLIPSE +
                           GuiLib_ITEMBIT_BITMAP +
                           GuiLib_ITEMBIT_BACKGROUND +
                           GuiLib_ITEMBIT_STRUCTURE)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_ROUNDEDFRAME +
                           GuiLib_ITEMBIT_ROUNDEDBLOCK +
                           GuiLib_ITEMBIT_QUARTERCIRCLE +
                           GuiLib_ITEMBIT_QUARTERELLIPSE))))
  {
    PtrIdx = GetItemWord(&sgl.ItemDataPtr);
    if (PtrIdx >= GuiStruct_VarPtrCnt)
      PtrIdx = 0;

    sgl.CurItem.UpdateType = GuiLib_AUTOREDRAW_MODE;
    sgl.CurItem.VarPtr = (void*)ReadWord(GuiStruct_VarPtrList[PtrIdx]);
    sgl.CurItem.VarType = ReadByte(GuiStruct_VarTypeList[PtrIdx]);
  }
#endif

  if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_FRAME +
                          GuiLib_ITEMBIT_CIRCLE +
                          GuiLib_ITEMBIT_ELLIPSE))
  {
    sgl.CurItem.FrameThickness = GetItemByte(&sgl.ItemDataPtr);
  }

  if (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_QUARTERCIRCLE +
                          GuiLib_ITEMBIT_QUARTERELLIPSE))
  {
    sgl.CurItem.FrameThickness = GetItemByte(&sgl.ItemDataPtr) % 4;
  }

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_STRUCTARRAY)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_STRUCTCOND)))
    sgl.CurItem.CompPars.StructCall.IndexCount = GetItemWord(&sgl.ItemDataPtr);

#ifdef GuiConst_ITEM_STRUCTCOND_INUSE
  if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_STRUCTCOND)
  {
    for (CondI = 0; CondI < sgl.CurItem.CompPars.StructCall.IndexCount; CondI++)
    {
      sgl.CurItem.CompPars.StructCall.IndexFirst[CondI] = GetItemWord(&sgl.ItemDataPtr);
      sgl.CurItem.CompPars.StructCall.IndexLast[CondI] = GetItemWord(&sgl.ItemDataPtr);
      sgl.CurItem.CompPars.StructCall.CallIndex[CondI] = GetItemWord(&sgl.ItemDataPtr);
    }
  }
#endif

  if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_ACTIVEAREA +
                          GuiLib_ITEMBIT_CLIPRECT))
  {
    B1 = GetItemByte(&sgl.ItemDataPtr);
    if (B1 & 0x01)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_CLIPPING;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_CLIPPING;
    if (B1 & 0x02)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_ACTIVEAREARELCOORD;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_ACTIVEAREARELCOORD;
  }

  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_FORMATTER)
  {
    sgl.CurItem.FormatFormat = GetItemByte(&sgl.ItemDataPtr);
    B1 = GetItemByte(&sgl.ItemDataPtr);
    if (B1 & 0x01)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATSHOWSIGN;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_FORMATSHOWSIGN;
    if (B1 & 0x02)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATZEROPADDING;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_FORMATZEROPADDING;
    sgl.CurItem.FormatAlignment = (B1 >> 2) & 0x03;
    if (B1 & 0x10)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATTRAILINGZEROS;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_FORMATTRAILINGZEROS;
    if (B1 & 0x20)
      sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
    else
      sgl.CurItem.TextPar[0].BitFlags &= ~GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
    sgl.CurItem.FormatFieldWidth = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.FormatDecimals = GetItemByte(&sgl.ItemDataPtr);
  }

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_STRUCTURE +
                           GuiLib_ITEMBIT_STRUCTARRAY +
                           GuiLib_ITEMBIT_BITMAP +
                           GuiLib_ITEMBIT_BACKGROUND)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_STRUCTCOND)))
    sgl.CurItem.StructToCallIndex = GetItemWord(&sgl.ItemDataPtr);

#ifdef GuiConst_ITEM_TEXTBLOCK_INUSE
  if (sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXTBLOCK +
                          GuiLib_ITEMBIT_VARBLOCK))
  {
    sgl.CurItem.CompPars.CompTextBox.HorzAlignment = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompTextBox.VertAlignment = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompTextBox.LineDist = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompTextBox.LineDistRelToFont = GetItemByte(&sgl.ItemDataPtr);
  }
#endif

#ifdef GuiConst_ITEM_TOUCHAREA_INUSE
  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_TOUCHAREA)
    sgl.CurItem.CompPars.CompTouch.AreaNo = GetItemWord(&sgl.ItemDataPtr);
#endif

  if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_POSCALLBACK)
    sgl.CurItem.PosCallbackNo = GetItemWord(&sgl.ItemDataPtr);

#ifdef GuiConst_ITEM_SCROLLBOX_INUSE
  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_SCROLLBOX)
  {
    ScrollBoxIndex = GetItemByte(&sgl.ItemDataPtr);
    sgl.GlobalScrollBoxIndex = ScrollBoxIndex;
    sgl.ScrollBoxesAry[ScrollBoxIndex].InUse = GuiLib_SCROLL_STRUCTURE_READ;
    sgl.ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType = GetItemByte(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].MakeUpStructIndex =
       GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].ScrollVisibleLines =
       GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineVerticalOffset =
       GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineOffsetX = GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineOffsetY = GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineSizeX = GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineSizeY = GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineStructIndex = GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineStructOffsetX =
       GetItemWord(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineStructOffsetY =
       GetItemWord(&sgl.ItemDataPtr);
    B1 = GetItemByte(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineColorTransparent = 0;
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineColorIndex = 0xFFFF;

    switch (B1)
    {
      case GuiLib_COLOR_NOCHANGE:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = sgl.CurItem.ForeColor;
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColorIndex = sgl.CurItem.ForeColorIndex;
        break;
      case GuiLib_COLOR_FORE:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = GuiConst_PIXEL_ON;
        break;
      case GuiLib_COLOR_BACK:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = GuiConst_PIXEL_OFF;
        break;
      case GuiLib_COLOR_OTHER:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = (GuiConst_INTCOLOR)
           GetItemColor(&sgl.ItemDataPtr);
        break;
      case GuiLib_COLOR_INVERT:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = TmpForeColor;
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColorIndex = TmpForeColIdx;
        break;
      case GuiLib_COLOR_TRANSP:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColorTransparent = 1;
        break;
      case GuiLib_COLOR_TABLE:
        sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor =
           GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
        break;
      case GuiLib_COLOR_VAR:
        ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
        if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
          ColVarPtrIdx = 0;
        ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
        if (ColVarPtr == 0)
          sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = GuiConst_PIXEL_ON;
        else
        {
          sgl.ScrollBoxesAry[ScrollBoxIndex].LineColor = *ColVarPtr;
          sgl.ScrollBoxesAry[ScrollBoxIndex].LineColorIndex = ColVarPtrIdx;
        }
        break;
    }
    sgl.ScrollBoxesAry[ScrollBoxIndex].WrapMode = GetItemByte(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].ScrollStartOfs = GetItemByte(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].ScrollMode = GetItemByte(&sgl.ItemDataPtr);
    sgl.ScrollBoxesAry[ScrollBoxIndex].LineMarkerCount = GetItemByte(&sgl.ItemDataPtr);
    for (L = 0; L < sgl.ScrollBoxesAry[ScrollBoxIndex].LineMarkerCount; L++)
    {
      sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorIndex[L] = 0xFFFF;
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = sgl.CurItem.BackColor;
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorIndex[L] = sgl.CurItem.BackColorIndex;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = GuiConst_PIXEL_ON;
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = GuiConst_PIXEL_OFF;
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = TmpForeColor;
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorIndex[L] = TmpForeColIdx;
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 1;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = GuiConst_PIXEL_ON;
          else
          {
            sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColor[L] = *ColVarPtr;
            sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorIndex[L] = ColVarPtrIdx;
          }
          sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerColorTransparent[L] = 0;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerStructIndex[L] =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerDrawingOrder[L] =
         GetItemByte(&sgl.ItemDataPtr);
      if (L || (sgl.ScrollBoxesAry[ScrollBoxIndex].ScrollBoxType))
        sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerSize[L] = 0;
      else
        sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerSize[L] = 1;
      sgl.ScrollBoxesAry[ScrollBoxIndex].MarkerStartLine[L] = 0;
    }

    sgl.ScrollBoxesAry[ScrollBoxIndex].BarType = GetItemByte(&sgl.ItemDataPtr);
#ifndef GuiConst_SCROLLITEM_BAR_NONE
    if (sgl.ScrollBoxesAry[ScrollBoxIndex].BarType != GuiLib_MARKER_NONE)
    {
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMode = GetItemByte(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarPositionX = GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarPositionY = GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarSizeX = GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarSizeY = GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarStructIndex = GetItemWord(&sgl.ItemDataPtr);
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = TmpBackColor;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarForeColor = *ColVarPtr;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarTransparent = 0;
      switch ((B1 & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = TmpForeColor;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarTransparent = 1;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarBackColor = *ColVarPtr;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarThickness = GetItemByte(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerLeftOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerRightOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerTopOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBottomOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarIconPtr =
         (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
#ifdef GuiConst_CHARMODE_ANSI
      sgl.ItemDataPtr++;
#else
      sgl.ItemDataPtr += 2;
#endif
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconFont =
         GetItemByte(&sgl.ItemDataPtr) + 1;
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconOffsetX =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerIconOffsetY =
         GetItemWord(&sgl.ItemDataPtr);
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor =
             GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor =
             (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = TmpBackColor;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerForeColor = *ColVarPtr;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerTransparent = 0;
      switch ((B1 & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = sgl.CurItem.BackColor;
          break;

        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = GuiConst_PIXEL_ON;
          break;

        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor =
             GuiConst_PIXEL_OFF;
          break;

        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor =
             (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;

        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = TmpForeColor;
          break;

        case GuiLib_COLOR_TRANSP:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerTransparent = 1;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBackColor = *ColVarPtr;
          break;
      }
      if (sgl.ScrollBoxesAry[ScrollBoxIndex].BarType == GuiLib_MARKER_BITMAP)
      {
        sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex =
           GetItemWord(&sgl.ItemDataPtr);
#ifdef GuiConst_REMOTE_BITMAP_DATA
        GuiLib_RemoteDataReadBlock(
           (GuiConst_INT32U PrefixRom)GuiStruct_BitmapPtrList[
           sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex], 4,
           BitmapHeader);
        PixelDataPtr = &BitmapHeader[0] + 2;
#else
        PixelDataPtr = (GuiConst_INT8U PrefixRom *)ReadWord(GuiStruct_BitmapPtrList[
           sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIndex]) + 2;
#endif
        sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapHeight =
           (GuiConst_INT16S)*PixelDataPtr;
        PixelDataPtr++;
        sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapHeight +=
           256*(GuiConst_INT16S)*PixelDataPtr;
        B1 = GetItemByte(&sgl.ItemDataPtr);
#ifdef GuiConst_BITMAP_SUPPORT_ON
        sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIsTransparent =
           (B1 & 0x01);
        if (sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIsTransparent)
#ifdef GuiConst_COLOR_DEPTH_1
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor =
             (B1 >> 1) & 0x01;
#else
          sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapTranspColor =
             GetItemColor(&sgl.ItemDataPtr);
#endif
#else
        sgl.ScrollBoxesAry[ScrollBoxIndex].BarMarkerBitmapIsTransparent = 0;
#endif
      }
    }
#endif

    sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorType = GetItemByte(&sgl.ItemDataPtr);
#ifndef GuiConst_SCROLLITEM_INDICATOR_NONE
    if (sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorType != GuiLib_INDICATOR_NONE)
    {
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMode = GetItemByte(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorPositionX =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorPositionY =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeX =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorSizeY =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorStructIndex =
          GetItemWord(&sgl.ItemDataPtr);
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor =
             GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor =
             (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = TmpBackColor;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorForeColor = *ColVarPtr;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorTransparent = 0;
      switch ((B1 & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = sgl.CurItem.BackColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor =
              GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor =
             (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = TmpForeColor;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorTransparent = 1;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorBackColor = *ColVarPtr;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorThickness =
         GetItemByte(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerLeftOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerRightOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTopOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBottomOffset =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorIconPtr =
         (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
#ifdef GuiConst_CHARMODE_ANSI
      sgl.ItemDataPtr++;
#else
      sgl.ItemDataPtr += 2;
#endif
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconFont =
         GetItemByte(&sgl.ItemDataPtr) + 1;
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconOffsetX =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerIconOffsetY =
         GetItemWord(&sgl.ItemDataPtr);
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             TmpBackColor;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerForeColor = *ColVarPtr;
          break;
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTransparent = 0;
      switch ((B1 & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             sgl.CurItem.BackColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             TmpForeColor;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerTransparent = 1;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor = GuiConst_PIXEL_ON;
          else
            sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBackColor = *ColVarPtr;
          break;
      }
      if (sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorType == GuiLib_MARKER_BITMAP)
      {
        sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIndex =
           GetItemWord(&sgl.ItemDataPtr);
        B1 = GetItemByte(&sgl.ItemDataPtr);
#ifdef GuiConst_BITMAP_SUPPORT_ON
        sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIsTransparent =
           (B1 & 0x01);
        if (sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIsTransparent)
#ifdef GuiConst_COLOR_DEPTH_1
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor =
             (B1 >> 1) & 0x01;
#else
          sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapTranspColor =
             GetItemColor(&sgl.ItemDataPtr);
#endif
#else
        sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorMarkerBitmapIsTransparent = 0;
#endif
      }
      sgl.ScrollBoxesAry[ScrollBoxIndex].IndicatorLine = -1;
    }
#endif
  }
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_GRAPH)
  {
    GraphIndex = GetItemByte(&sgl.ItemDataPtr);
    sgl.GlobalGraphIndex = GraphIndex;

    sgl.GraphAry[GraphIndex].InUse = GuiLib_GRAPH_STRUCTURE_USED;
    sgl.GraphAry[GraphIndex].OriginOffsetX = GetItemWord(&sgl.ItemDataPtr);
    sgl.GraphAry[GraphIndex].OriginOffsetY = GetItemWord(&sgl.ItemDataPtr);
    sgl.GraphAry[GraphIndex].ForeColor = sgl.CurItem.ForeColor;
    sgl.GraphAry[GraphIndex].BackColor = sgl.CurItem.BackColor;

    for (Axis = GuiLib_GRAPHAXIS_X; Axis <= GuiLib_GRAPHAXIS_Y; Axis++)
    {
      sgl.GraphAry[GraphIndex].GraphAxesCnt[Axis] = GetItemByte(&sgl.ItemDataPtr);
      for (L = 0; L < sgl.GraphAry[GraphIndex].GraphAxesCnt[Axis]; L++)
      {
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags = 0;
        B = GetItemByte(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].Visible = 1;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].Line = B & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].LineBetweenAxes = (B >> 7) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].LineNegative = (B >> 1) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].Arrow = (B >> 2) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMajor = (B >> 3) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMinor = (B >> 4) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].Numbers = (B >> 5) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersAtOrigo = (B >> 6) & 0x01;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].Offset =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].ArrowLength =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].ArrowWidth =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMajorLength =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMajorWidth =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMinorLength =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].TicksMinorWidth =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMinValue =
           GetItemLong(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMinValueOrg =
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMinValue;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMaxValue =
           GetItemLong(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMaxValueOrg =
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersMaxValue;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersStepMajor =
           GetItemLong(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersStepMinor =
           GetItemLong(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersOffset =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].NumbersAtEnd =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].FormatFieldWidth =
           GetItemByte(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].FormatDecimals =
           GetItemByte(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].FormatAlignment =
           GetItemByte(&sgl.ItemDataPtr);
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].FormatFormat =
           GetItemByte(&sgl.ItemDataPtr);
        B = GetItemByte(&sgl.ItemDataPtr);
        if (B & 0x01)
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags |= GuiLib_BITFLAG_FORMATSHOWSIGN;
        else
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags &= ~GuiLib_BITFLAG_FORMATSHOWSIGN;
        if (B & 0x02)
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags |= GuiLib_BITFLAG_FORMATZEROPADDING;
        else
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags &= ~GuiLib_BITFLAG_FORMATZEROPADDING;
        if (B & 0x04)
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags |= GuiLib_BITFLAG_FORMATTRAILINGZEROS;
        else
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags &= ~GuiLib_BITFLAG_FORMATTRAILINGZEROS;
        if (B & 0x08)
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags |= GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
        else
          sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].BitFlags &= ~GuiLib_BITFLAG_FORMATTHOUSANDSSEP;
        sgl.GraphAry[GraphIndex].GraphAxes[L][Axis].Scale = 10000;
      }
    }

    sgl.GraphAry[GraphIndex].GraphDataSetCnt = GetItemByte(&sgl.ItemDataPtr);
    for (L = 0; L < sgl.GraphAry[GraphIndex].GraphDataSetCnt; L++)
    {
      sgl.GraphAry[GraphIndex].GraphDataSets[L].DataSize = 0;
      sgl.GraphAry[GraphIndex].GraphDataSets[L].DataFirst = 0;
      sgl.GraphAry[GraphIndex].GraphDataSets[L].DataCount = 0;
      sgl.GraphAry[GraphIndex].GraphDataSets[L].AxisIndexX = 0;
      sgl.GraphAry[GraphIndex].GraphDataSets[L].AxisIndexY = 0;
      sgl.GraphAry[GraphIndex].GraphDataSets[L].Visible = 1;
      sgl.GraphAry[GraphIndex].GraphDataSets[L].Representation =
         GetItemByte(&sgl.ItemDataPtr);
      sgl.GraphAry[GraphIndex].GraphDataSets[L].Width =
         GetItemWord(&sgl.ItemDataPtr);
      sgl.GraphAry[GraphIndex].GraphDataSets[L].Thickness =
         GetItemWord(&sgl.ItemDataPtr);
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = sgl.CurItem.ForeColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = TmpBackColor;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = GuiConst_PIXEL_ON;
          else
            sgl.GraphAry[GraphIndex].GraphDataSets[L].ForeColor = *ColVarPtr;
          break;
      }
      sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColorTransparent = 0;
      switch ((B1 & 0x38) >> 3)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = sgl.CurItem.BackColor;
          break;
        case GuiLib_COLOR_FORE:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = GuiConst_PIXEL_ON;
          break;
        case GuiLib_COLOR_BACK:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = GuiConst_PIXEL_OFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          break;
        case GuiLib_COLOR_INVERT:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = TmpForeColor;
          break;
        case GuiLib_COLOR_TRANSP:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColorTransparent = 1;
          break;
        case GuiLib_COLOR_TABLE:
          sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
            sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = GuiConst_PIXEL_ON;
          else
            sgl.GraphAry[GraphIndex].GraphDataSets[L].BackColor = *ColVarPtr;
          break;
      }
    }
  }
#endif

#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE
  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_GRAPHICSLAYER)
  {
    GraphicsLayerIndex = GetItemByte(&sgl.ItemDataPtr);
    sgl.GlobalGraphicsLayerIndex = GraphicsLayerIndex;

    sgl.GraphicsLayerList[GraphicsLayerIndex].InUse = GuiLib_GRAPHICS_LAYER_USED;
    sgl.GraphicsLayerList[GraphicsLayerIndex].SizeMode = GetItemByte(&sgl.ItemDataPtr);
    sgl.GraphicsLayerList[GraphicsLayerIndex].InitMode = GetItemByte(&sgl.ItemDataPtr);
  }

  if (sgl.ItemTypeBit1 & GuiLib_ITEMBIT_GRAPHICSFILTER)
  {
    GraphicsFilterIndex = GetItemByte(&sgl.ItemDataPtr);
    sgl.GlobalGraphicsFilterIndex = GraphicsFilterIndex;

    sgl.GraphicsFilterList[GraphicsFilterIndex].InUse = GuiLib_GRAPHICS_FILTER_USED;
    sgl.GraphicsFilterList[GraphicsFilterIndex].SourceLayerIndexNo =
       GetItemWord(&sgl.ItemDataPtr);
    sgl.GraphicsFilterList[GraphicsFilterIndex].DestLayerIndexNo =
       GetItemWord(&sgl.ItemDataPtr);
    sgl.GraphicsFilterList[GraphicsFilterIndex].ContAtLayerIndexNo =
       GetItemWord(&sgl.ItemDataPtr);
    for (I = 0; I <= 9; I++)
    {
      B1 = GetItemByte(&sgl.ItemDataPtr);
      if (B1 == 0)
      {
        sgl.GraphicsFilterList[GraphicsFilterIndex].ParVarType[I] = GuiLib_VAR_NONE;
        sgl.GraphicsFilterList[GraphicsFilterIndex].ParVarPtr[I] = 0;
        sgl.GraphicsFilterList[GraphicsFilterIndex].ParValueNum[I] =
           GetItemLong(&sgl.ItemDataPtr);
      }
      else
      {
        PtrIdx = GetItemWord(&sgl.ItemDataPtr);
        if (PtrIdx >= GuiStruct_VarPtrCnt)
          PtrIdx = 0;

        sgl.GraphicsFilterList[GraphicsFilterIndex].ParVarType[I] =
            ReadByte(GuiStruct_VarTypeList[PtrIdx]);
        sgl.GraphicsFilterList[GraphicsFilterIndex].ParVarPtr[I] =
            (void*)ReadWord(GuiStruct_VarPtrList[PtrIdx]);
        sgl.GraphicsFilterList[GraphicsFilterIndex].ParValueNum[I] = 0;
      }
    }
  }
#endif

#ifdef GuiConst_ITEM_CHECKBOX_INUSE
  if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_CHECKBOX)
  {
    sgl.CurItem.CompPars.CompCheckBox.Style = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompCheckBox.Size = GetItemByte(&sgl.ItemDataPtr);
    switch (sgl.CurItem.CompPars.CompCheckBox.Style)
    {
      case GuiLib_CHECKBOX_ICON:
        sgl.CurItem.CompPars.CompCheckBox.IconPtr =
           (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
  #ifdef GuiConst_CHARMODE_ANSI
        sgl.ItemDataPtr++;
  #else
        sgl.ItemDataPtr += 2;
  #endif
        sgl.CurItem.CompPars.CompCheckBox.IconFont = GetItemByte(&sgl.ItemDataPtr) + 1;
        sgl.CurItem.CompPars.CompCheckBox.IconOffsetX = GetItemWord(&sgl.ItemDataPtr);
        sgl.CurItem.CompPars.CompCheckBox.IconOffsetY = GetItemWord(&sgl.ItemDataPtr);
        break;
      case GuiLib_CHECKBOX_BITMAP:
        sgl.CurItem.CompPars.CompCheckBox.BitmapIndex = GetItemWord(&sgl.ItemDataPtr);
        B1 = GetItemByte(&sgl.ItemDataPtr);
  #ifdef GuiConst_BITMAP_SUPPORT_ON
        sgl.CurItem.CompPars.CompCheckBox.BitmapIsTransparent = (B1 & 0x01);
        if (sgl.CurItem.CompPars.CompCheckBox.BitmapIsTransparent)
  #ifdef GuiConst_COLOR_DEPTH_1
          sgl.CurItem.CompPars.CompCheckBox.BitmapTranspColor = (B1 >> 1) & 0x01;
  #else
          sgl.CurItem.CompPars.CompCheckBox.BitmapTranspColor =
             GetItemColor(&sgl.ItemDataPtr);
  #endif
  #else
        sgl.CurItem.CompPars.CompCheckBox.BitmapIsTransparent = 0;
  #endif
        break;
      case GuiLib_CHECKBOX_NONE:
        sgl.CurItem.TextPar[0].BitFlags |= GuiLib_BITFLAG_TRANSPARENT;
        break;
    }

    sgl.CurItem.CompPars.CompCheckBox.MarkStyle = GetItemByte(&sgl.ItemDataPtr);
    if (sgl.CurItem.CompPars.CompCheckBox.MarkStyle != GuiLib_CHECKBOX_MARK_BITMAP)
    {
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.CurItem.CompPars.CompCheckBox.MarkColor = sgl.CurItem.ForeColor;
          sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = sgl.CurItem.ForeColorIndex;
          break;
        case GuiLib_COLOR_FORE:
          sgl.CurItem.CompPars.CompCheckBox.MarkColor = GuiConst_PIXEL_ON;
          sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_BACK:
          sgl.CurItem.CompPars.CompCheckBox.MarkColor = GuiConst_PIXEL_OFF;
          sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.CurItem.CompPars.CompCheckBox.MarkColor = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_INVERT:
          sgl.CurItem.CompPars.CompCheckBox.MarkColor = TmpBackColor;
          sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = TmpBackColIdx;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.CurItem.CompPars.CompCheckBox.MarkColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
          {
            sgl.CurItem.CompPars.CompCheckBox.MarkColor = GuiConst_PIXEL_ON;
            sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = 0xFFFF;
          }
          else
          {
            sgl.CurItem.CompPars.CompCheckBox.MarkColor = *ColVarPtr;
            sgl.CurItem.CompPars.CompCheckBox.MarkColorIndex = ColVarPtrIdx;
          }
          break;
      }
    }
    switch (sgl.CurItem.CompPars.CompCheckBox.MarkStyle)
    {
      case GuiLib_CHECKBOX_MARK_ICON:
        sgl.CurItem.CompPars.CompCheckBox.MarkIconPtr =
           (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
  #ifdef GuiConst_CHARMODE_ANSI
        sgl.ItemDataPtr++;
  #else
        sgl.ItemDataPtr += 2;
  #endif
        sgl.CurItem.CompPars.CompCheckBox.MarkIconFont =
           GetItemByte(&sgl.ItemDataPtr) + 1;
        sgl.CurItem.CompPars.CompCheckBox.MarkOffsetX =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.CurItem.CompPars.CompCheckBox.MarkOffsetY =
           GetItemWord(&sgl.ItemDataPtr);
        break;
      case GuiLib_CHECKBOX_MARK_BITMAP:
        sgl.CurItem.CompPars.CompCheckBox.MarkBitmapIndex =
           GetItemWord(&sgl.ItemDataPtr);
        B1 = GetItemByte(&sgl.ItemDataPtr);
  #ifdef GuiConst_BITMAP_SUPPORT_ON
        sgl.CurItem.CompPars.CompCheckBox.MarkBitmapIsTransparent = (B1 & 0x01);
        if (sgl.CurItem.CompPars.CompCheckBox.MarkBitmapIsTransparent)
  #ifdef GuiConst_COLOR_DEPTH_1
          sgl.CurItem.CompPars.CompCheckBox.MarkBitmapTranspColor =
             (B1 >> 1) & 0x01;
  #else
          sgl.CurItem.CompPars.CompCheckBox.MarkBitmapTranspColor =
             GetItemColor(&sgl.ItemDataPtr);
  #endif
  #else
        sgl.CurItem.CompPars.CompCheckBox.MarkBitmapIsTransparent = 0;
  #endif
        sgl.CurItem.CompPars.CompCheckBox.MarkOffsetX =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.CurItem.CompPars.CompCheckBox.MarkOffsetY =
           GetItemWord(&sgl.ItemDataPtr);
        break;
    }
  }
#endif

#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
  if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_RADIOBUTTON)
  {
    sgl.CurItem.CompPars.CompRadioButton.Style = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompRadioButton.Size = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompRadioButton.Count = GetItemByte(&sgl.ItemDataPtr);
    sgl.CurItem.CompPars.CompRadioButton.InterDistance = GetItemWord(&sgl.ItemDataPtr);
    switch (sgl.CurItem.CompPars.CompRadioButton.Style)
    {
      case GuiLib_RADIOBUTTON_ICON:
        sgl.CurItem.CompPars.CompRadioButton.IconPtr =
           (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
  #ifdef GuiConst_CHARMODE_ANSI
        sgl.ItemDataPtr++;
  #else
        sgl.ItemDataPtr += 2;
  #endif
        sgl.CurItem.CompPars.CompRadioButton.IconFont = GetItemByte(&sgl.ItemDataPtr) + 1;
        sgl.CurItem.CompPars.CompRadioButton.IconOffsetX = GetItemWord(&sgl.ItemDataPtr);
        sgl.CurItem.CompPars.CompRadioButton.IconOffsetY = GetItemWord(&sgl.ItemDataPtr);
        break;
      case GuiLib_RADIOBUTTON_BITMAP:
        sgl.CurItem.CompPars.CompRadioButton.BitmapIndex = GetItemWord(&sgl.ItemDataPtr);
        B1 = GetItemByte(&sgl.ItemDataPtr);
  #ifdef GuiConst_BITMAP_SUPPORT_ON
        sgl.CurItem.CompPars.CompRadioButton.BitmapIsTransparent = (B1 & 0x01);
        if (sgl.CurItem.CompPars.CompRadioButton.BitmapIsTransparent)
  #ifdef GuiConst_COLOR_DEPTH_1
          sgl.CurItem.CompPars.CompRadioButton.BitmapTranspColor = (B1 >> 1) & 0x01;
  #else
          sgl.CurItem.CompPars.CompRadioButton.BitmapTranspColor =
             GetItemColor(&sgl.ItemDataPtr);
  #endif
  #else
        sgl.CurItem.CompPars.CompRadioButton.BitmapIsTransparent = 0;
  #endif
        break;
    }

    sgl.CurItem.CompPars.CompRadioButton.MarkStyle = GetItemByte(&sgl.ItemDataPtr);
    if (sgl.CurItem.CompPars.CompRadioButton.MarkStyle != GuiLib_RADIOBUTTON_MARK_BITMAP)
    {
      B1 = GetItemByte(&sgl.ItemDataPtr);
      switch (B1 & 0x07)
      {
        case GuiLib_COLOR_NOCHANGE:
          sgl.CurItem.CompPars.CompRadioButton.MarkColor = sgl.CurItem.ForeColor;
          sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = sgl.CurItem.ForeColorIndex;
          break;
        case GuiLib_COLOR_FORE:
          sgl.CurItem.CompPars.CompRadioButton.MarkColor = GuiConst_PIXEL_ON;
          sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_BACK:
          sgl.CurItem.CompPars.CompRadioButton.MarkColor = GuiConst_PIXEL_OFF;
          sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_OTHER:
          sgl.CurItem.CompPars.CompRadioButton.MarkColor = (GuiConst_INTCOLOR)
             GetItemColor(&sgl.ItemDataPtr);
          sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_INVERT:
          sgl.CurItem.CompPars.CompRadioButton.MarkColor = TmpBackColor;
          sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = TmpBackColIdx;
          break;
        case GuiLib_COLOR_TRANSP:
          break;
        case GuiLib_COLOR_TABLE:
          sgl.CurItem.CompPars.CompRadioButton.MarkColor =
             GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
          sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = 0xFFFF;
          break;
        case GuiLib_COLOR_VAR:
          ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
          if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
            ColVarPtrIdx = 0;
          ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
          if (ColVarPtr == 0)
          {
            sgl.CurItem.CompPars.CompRadioButton.MarkColor = GuiConst_PIXEL_ON;
            sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = 0xFFFF;
          }
          else
          {
            sgl.CurItem.CompPars.CompRadioButton.MarkColor = *ColVarPtr;
            sgl.CurItem.CompPars.CompRadioButton.MarkColorIndex = ColVarPtrIdx;
          }
          break;
      }
    }
    switch (sgl.CurItem.CompPars.CompRadioButton.MarkStyle)
    {
      case GuiLib_RADIOBUTTON_MARK_ICON:
        sgl.CurItem.CompPars.CompRadioButton.MarkIconPtr =
           (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
  #ifdef GuiConst_CHARMODE_ANSI
        sgl.ItemDataPtr++;
  #else
        sgl.ItemDataPtr += 2;
  #endif
        sgl.CurItem.CompPars.CompRadioButton.MarkIconFont =
           GetItemByte(&sgl.ItemDataPtr) + 1;
        sgl.CurItem.CompPars.CompRadioButton.MarkOffsetX =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.CurItem.CompPars.CompRadioButton.MarkOffsetY =
           GetItemWord(&sgl.ItemDataPtr);
        break;
      case GuiLib_RADIOBUTTON_MARK_BITMAP:
        sgl.CurItem.CompPars.CompRadioButton.MarkBitmapIndex =
           GetItemWord(&sgl.ItemDataPtr);
        B1 = GetItemByte(&sgl.ItemDataPtr);
  #ifdef GuiConst_BITMAP_SUPPORT_ON
        sgl.CurItem.CompPars.CompRadioButton.MarkBitmapIsTransparent = (B1 & 0x01);
        if (sgl.CurItem.CompPars.CompRadioButton.MarkBitmapIsTransparent)
  #ifdef GuiConst_COLOR_DEPTH_1
          sgl.CurItem.CompPars.CompRadioButton.MarkBitmapTranspColor =
             (B1 >> 1) & 0x01;
  #else
          sgl.CurItem.CompPars.CompRadioButton.MarkBitmapTranspColor =
             GetItemColor(&sgl.ItemDataPtr);
  #endif
  #else
        sgl.CurItem.CompPars.CompRadioButton.MarkBitmapIsTransparent = 0;
  #endif
        sgl.CurItem.CompPars.CompRadioButton.MarkOffsetX =
           GetItemWord(&sgl.ItemDataPtr);
        sgl.CurItem.CompPars.CompRadioButton.MarkOffsetY =
           GetItemWord(&sgl.ItemDataPtr);
        break;
    }
  }
#endif

#ifdef GuiConst_ITEM_BUTTON_INUSE
  if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_BUTTON)
  {
    sgl.CurItem.CompPars.CompButton.Layout = GetItemByte(&sgl.ItemDataPtr);

    sgl.CurItem.CompPars.CompButton.BodyStyle = GetItemByte(&sgl.ItemDataPtr);
    if ((sgl.CurItem.CompPars.CompButton.BodyStyle == GuiLib_BUTTON_BODY_ICON) ||
        (sgl.CurItem.CompPars.CompButton.BodyStyle == GuiLib_BUTTON_BODY_BITMAP))
    {
      sgl.CurItem.CompPars.CompButton.BodyLikeUp = GetItemByte(&sgl.ItemDataPtr);
      for (I = 0; I < 3; I++)
        if ((I == 0) ||
           ((I == 1) && !(sgl.CurItem.CompPars.CompButton.BodyLikeUp & 0x01)) ||
           ((I == 2) && !(sgl.CurItem.CompPars.CompButton.BodyLikeUp & 0x02)))
        {
          switch (sgl.CurItem.CompPars.CompButton.BodyStyle)
          {
            case GuiLib_BUTTON_BODY_ICON:
              sgl.CurItem.CompPars.CompButton.BodyIconPtr[I] =
                 (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
        #ifdef GuiConst_CHARMODE_ANSI
              sgl.ItemDataPtr++;
        #else
              sgl.ItemDataPtr += 2;
        #endif
              sgl.CurItem.CompPars.CompButton.BodyIconFont[I] =
                 GetItemByte(&sgl.ItemDataPtr) + 1;
              sgl.CurItem.CompPars.CompButton.BodyIconOffsetX[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              sgl.CurItem.CompPars.CompButton.BodyIconOffsetY[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              break;
            case GuiLib_BUTTON_BODY_BITMAP:
              sgl.CurItem.CompPars.CompButton.BodyBitmapIndex[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              B1 = GetItemByte(&sgl.ItemDataPtr);
        #ifdef GuiConst_BITMAP_SUPPORT_ON
              sgl.CurItem.CompPars.CompButton.BodyBitmapIsTransparent[I] =
                 (B1 & 0x01);
              if (sgl.CurItem.CompPars.CompButton.BodyBitmapIsTransparent[I])
        #ifdef GuiConst_COLOR_DEPTH_1
                sgl.CurItem.CompPars.CompButton.BodyBitmapTranspColor[I] =
                   (B1 >> 1) & 0x01;
        #else
                sgl.CurItem.CompPars.CompButton.BodyBitmapTranspColor[I] =
                   GetItemColor(&sgl.ItemDataPtr);
        #endif
        #else
              sgl.CurItem.CompPars.CompButton.BodyBitmapIsTransparent[I] = 0;
        #endif
              break;
          }
        }
        else
        {
          switch (sgl.CurItem.CompPars.CompButton.BodyStyle)
          {
            case GuiLib_BUTTON_BODY_ICON:
              sgl.CurItem.CompPars.CompButton.BodyIconPtr[I] =
                 sgl.CurItem.CompPars.CompButton.BodyIconPtr[0];
              sgl.CurItem.CompPars.CompButton.BodyIconFont[I] =
                 sgl.CurItem.CompPars.CompButton.BodyIconFont[0];
              sgl.CurItem.CompPars.CompButton.BodyIconOffsetX[I] =
                 sgl.CurItem.CompPars.CompButton.BodyIconOffsetX[0];
              sgl.CurItem.CompPars.CompButton.BodyIconOffsetY[I] =
                 sgl.CurItem.CompPars.CompButton.BodyIconOffsetY[0];
              break;
            case GuiLib_BUTTON_BODY_BITMAP:
              sgl.CurItem.CompPars.CompButton.BodyBitmapIndex[I] =
                 sgl.CurItem.CompPars.CompButton.BodyBitmapIndex[0];
              sgl.CurItem.CompPars.CompButton.BodyBitmapIsTransparent[I] =
                 sgl.CurItem.CompPars.CompButton.BodyBitmapIsTransparent[0];
              sgl.CurItem.CompPars.CompButton.BodyBitmapTranspColor[I] =
                 sgl.CurItem.CompPars.CompButton.BodyBitmapTranspColor[0];
              break;
          }
        }
    }

    if (sgl.CurItem.CompPars.CompButton.Layout != GuiLib_BUTTON_LAYOUT_GLYPH)
    {
      sgl.CurItem.CompPars.CompButton.TextLikeUp = GetItemByte(&sgl.ItemDataPtr);
      for (I = 0; I < 3; I++)
      {
        if ((I == 0) ||
           ((I == 1) && !(sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x04)) ||
           ((I == 2) && !(sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x08)))
        {
          B1 = GetItemByte(&sgl.ItemDataPtr);
          switch (B1 & 0x07)
          {
            case GuiLib_COLOR_NOCHANGE:
              sgl.CurItem.CompPars.CompButton.TextColor[I] = sgl.CurItem.ForeColor;
              sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = sgl.CurItem.ForeColorIndex;
              break;
            case GuiLib_COLOR_FORE:
              sgl.CurItem.CompPars.CompButton.TextColor[I] = GuiConst_PIXEL_ON;
              sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = 0xFFFF;
              break;
            case GuiLib_COLOR_BACK:
              sgl.CurItem.CompPars.CompButton.TextColor[I] = GuiConst_PIXEL_OFF;
              sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = 0xFFFF;
              break;
            case GuiLib_COLOR_OTHER:
              sgl.CurItem.CompPars.CompButton.TextColor[I] =
                 (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
              sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = 0xFFFF;
              break;
            case GuiLib_COLOR_INVERT:
              sgl.CurItem.CompPars.CompButton.TextColor[I] = TmpBackColor;
              sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = TmpBackColIdx;
              break;
            case GuiLib_COLOR_TRANSP:
              break;
            case GuiLib_COLOR_TABLE:
              sgl.CurItem.CompPars.CompButton.TextColor[I] =
                 GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
              sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = 0xFFFF;
              break;
            case GuiLib_COLOR_VAR:
              ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
              if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
                ColVarPtrIdx = 0;
              ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
              if (ColVarPtr == 0)
              {
                sgl.CurItem.CompPars.CompButton.TextColor[I] = GuiConst_PIXEL_ON;
                sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = 0xFFFF;
              }
              else
              {
                sgl.CurItem.CompPars.CompButton.TextColor[I] = *ColVarPtr;
                sgl.CurItem.CompPars.CompButton.TextColorIndex[I] = ColVarPtrIdx;
              }
              break;
          }

          B1 = GetItemByte(&sgl.ItemDataPtr);
          if (B1 & 0x04)
            sgl.CurItem.TextPar[I].BitFlags |= GuiLib_BITFLAG_UNDERLINE;
          else
            sgl.CurItem.TextPar[I].BitFlags &= ~GuiLib_BITFLAG_UNDERLINE;
          B1 = B1 & 0x03;
          if (B1 != GuiLib_PS_NOCHANGE)
            sgl.CurItem.TextPar[I].Ps = B1;
        }
        else
        {
          sgl.CurItem.CompPars.CompButton.TextColor[I] =
             sgl.CurItem.CompPars.CompButton.TextColor[0];
          sgl.CurItem.TextPar[I] = sgl.CurItem.TextPar[0];
        }
      }
    }

    if (sgl.CurItem.CompPars.CompButton.Layout != GuiLib_BUTTON_LAYOUT_TEXT)
    {
      sgl.CurItem.CompPars.CompButton.GlyphStyle = GetItemByte(&sgl.ItemDataPtr);
      sgl.CurItem.CompPars.CompButton.GlyphLikeUp = GetItemByte(&sgl.ItemDataPtr);
      for (I = 0; I < 3; I++)
        if ((I == 0) ||
           ((I == 1) && !(sgl.CurItem.CompPars.CompButton.GlyphLikeUp & 0x01)) ||
           ((I == 2) && !(sgl.CurItem.CompPars.CompButton.GlyphLikeUp & 0x02)))
        {
          switch (sgl.CurItem.CompPars.CompButton.GlyphStyle)
          {
            case GuiLib_BUTTON_GLYPH_ICON:
              B1 = GetItemByte(&sgl.ItemDataPtr);
              switch (B1 & 0x07)
              {
                case GuiLib_COLOR_NOCHANGE:
                  sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] = sgl.CurItem.ForeColor;
                  sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = sgl.CurItem.ForeColorIndex;
                  break;
                case GuiLib_COLOR_FORE:
                  sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] = GuiConst_PIXEL_ON;
                  sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = 0xFFFF;
                  break;
                case GuiLib_COLOR_BACK:
                  sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] = GuiConst_PIXEL_OFF;
                  sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = 0xFFFF;
                  break;
                case GuiLib_COLOR_OTHER:
                  sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] =
                     (GuiConst_INTCOLOR)GetItemColor(&sgl.ItemDataPtr);
                  sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = 0xFFFF;
                  break;
                case GuiLib_COLOR_INVERT:
                  sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] = TmpBackColor;
                  sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = TmpBackColIdx;
                  break;
                case GuiLib_COLOR_TRANSP:
                  break;
                case GuiLib_COLOR_TABLE:
                  sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] =
                     GuiStruct_ColorTable[GetItemByte(&sgl.ItemDataPtr)];
                  sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = 0xFFFF;
                  break;
                case GuiLib_COLOR_VAR:
                  ColVarPtrIdx = GetItemWord(&sgl.ItemDataPtr);
                  if (ColVarPtrIdx >= GuiStruct_VarPtrCnt)
                    ColVarPtrIdx = 0;
                  ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[ColVarPtrIdx]);
                  if (ColVarPtr == 0)
                  {
                    sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] = GuiConst_PIXEL_ON;
                    sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = 0xFFFF;
                  }
                  else
                  {
                    sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] = *ColVarPtr;
                    sgl.CurItem.CompPars.CompButton.GlyphIconColorIndex[I] = ColVarPtrIdx;
                  }
                  break;
              }

              sgl.CurItem.CompPars.CompButton.GlyphIconPtr[I] =
                 (GuiConst_TEXT PrefixRom *)sgl.ItemDataPtr;
        #ifdef GuiConst_CHARMODE_ANSI
              sgl.ItemDataPtr++;
        #else
              sgl.ItemDataPtr += 2;
        #endif
              sgl.CurItem.CompPars.CompButton.GlyphIconFont[I] =
                 GetItemByte(&sgl.ItemDataPtr) + 1;
              sgl.CurItem.CompPars.CompButton.GlyphIconOffsetX[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              sgl.CurItem.CompPars.CompButton.GlyphIconOffsetY[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              break;
            case GuiLib_BUTTON_GLYPH_BITMAP:
              sgl.CurItem.CompPars.CompButton.GlyphBitmapIndex[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              B1 = GetItemByte(&sgl.ItemDataPtr);
        #ifdef GuiConst_BITMAP_SUPPORT_ON
              sgl.CurItem.CompPars.CompButton.GlyphBitmapIsTransparent[I] =
                 (B1 & 0x01);
              if (sgl.CurItem.CompPars.CompButton.GlyphBitmapIsTransparent[I])
        #ifdef GuiConst_COLOR_DEPTH_1
                sgl.CurItem.CompPars.CompButton.GlyphBitmapTranspColor[I] =
                   (B1 >> 1) & 0x01;
        #else
                sgl.CurItem.CompPars.CompButton.GlyphBitmapTranspColor[I] =
                   GetItemColor(&sgl.ItemDataPtr);
        #endif
        #else
              sgl.CurItem.CompPars.CompButton.GlyphBitmapIsTransparent[I] = 0;
        #endif
              sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetX[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetY[I] =
                 GetItemWord(&sgl.ItemDataPtr);
              break;
          }
        }
        else
        {
          switch (sgl.CurItem.CompPars.CompButton.GlyphStyle)
          {
            case GuiLib_BUTTON_GLYPH_ICON:
              sgl.CurItem.CompPars.CompButton.GlyphIconColor[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphIconColor[0];
              sgl.CurItem.CompPars.CompButton.GlyphIconPtr[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphIconPtr[0];
              sgl.CurItem.CompPars.CompButton.GlyphIconFont[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphIconFont[0];
              sgl.CurItem.CompPars.CompButton.GlyphIconOffsetX[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphIconOffsetX[0];
              sgl.CurItem.CompPars.CompButton.GlyphIconOffsetY[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphIconOffsetY[0];
              break;
            case GuiLib_BUTTON_GLYPH_BITMAP:
              sgl.CurItem.CompPars.CompButton.GlyphBitmapIndex[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphBitmapIndex[0];
              sgl.CurItem.CompPars.CompButton.GlyphBitmapIsTransparent[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphBitmapIsTransparent[0];
              sgl.CurItem.CompPars.CompButton.GlyphBitmapTranspColor[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphBitmapTranspColor[0];
              sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetX[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetX[0];
              sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetY[I] =
                 sgl.CurItem.CompPars.CompButton.GlyphBitmapOffsetY[0];
              break;
          }
        }
    }
  }
#endif

#ifdef GuiConst_ITEM_PANEL_INUSE
  if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_PANEL)
  {
    sgl.CurItem.CompPars.CompPanel.Style = GetItemByte(&sgl.ItemDataPtr);
  }
#endif

  if ((sgl.ItemTypeBit1 & (GuiLib_ITEMBIT_TEXT +
                           GuiLib_ITEMBIT_TEXTBLOCK)) ||
      (sgl.ItemTypeBit2 & (GuiLib_ITEMBIT_BUTTON)))
  {
    if (sgl.CommonByte4 & 0x80)
    {
#ifdef GuiConst_LANGUAGE_ALL_ACTIVE
      N = GuiConst_LANGUAGE_CNT;
      W1 = LanguageIndex;
#else
      N = GuiConst_LANGUAGE_ACTIVE_CNT;
      W1 = ReadWord(GuiFont_LanguageIndex[LanguageIndex]);
#endif
    }
    else
    {
      N = 1;
      W1 = 0;
    }

    J = 0;
    for (I = 0; I < sgl.CurItem.TextCnt; I++)
    {
#ifndef GuiConst_REMOTE_TEXT_DATA
      TxtSum1 = 0;
      TxtSum2 = 0;
#endif
      for (X = 0; X < N; X++)
      {
#ifdef GuiConst_REMOTE_TEXT_DATA
        Ti = GetItemWord(&sgl.ItemDataPtr);
        if (X == W1)
        {
          sgl.CurItem.TextLength[J] = GetRemoteText(Ti);
          sgl.CurItem.TextPtr[J] = &sgl.GuiLib_RemoteTextBuffer[0];
        }
#else
        TxtSize = GetItemByte(&sgl.ItemDataPtr);
        if (TxtSize == 0xFF)
          TxtSize = GetItemWord(&sgl.ItemDataPtr);

        if (X == W1)
        {
          sgl.CurItem.TextLength[J] = GuiLib_GET_MIN(TxtSize, GuiConst_MAX_TEXT_LEN);
#ifdef GuiConst_REMOTE_STRUCT_DATA
          TxtReadSize = sgl.CurItem.TextLength[J] + 1;
#ifndef GuiConst_CHARMODE_ANSI
          TxtReadSize *= 2;
#endif
#endif
        }

        TxtSize++;
#ifndef GuiConst_CHARMODE_ANSI
        TxtSize *= 2;
#endif

#ifndef GuiConst_REMOTE_TEXT_DATA
        if (X < W1)
          TxtSum1 += TxtSize;
        else
          TxtSum2 += TxtSize;
#endif
#endif // GuiConst_REMOTE_TEXT_DATA
      }

#ifndef GuiConst_REMOTE_TEXT_DATA
#ifdef GuiConst_REMOTE_STRUCT_DATA
      sgl.RemoteStructOffset += TxtSum1;
      GuiLib_RemoteDataReadBlock(
         sgl.RemoteStructOffset,
         TxtReadSize,
         (GuiConst_INT8U*)&sgl.GuiLib_RemoteStructText);
      sgl.CurItem.TextPtr[J] = (GuiConst_TEXT *)&sgl.GuiLib_RemoteStructText;
      sgl.RemoteStructOffset += TxtSum2;
#else
      sgl.ItemDataPtr += TxtSum1;
      sgl.CurItem.TextPtr[J] = (GuiConst_TEXT *)(sgl.ItemDataPtr);
      sgl.ItemDataPtr += TxtSum2;
#endif
#endif // GuiConst_REMOTE_TEXT_DATA

#ifdef GuiConst_ITEM_BUTTON_INUSE
      if ((sgl.ItemTypeBit2 & GuiLib_ITEMBIT_BUTTON) &&
         (((I == 0) && (sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x01)) ||
          ((I == 1) && (sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x02))))
        J++;
#endif
      J++;
    }

#ifdef GuiConst_ITEM_BUTTON_INUSE
    if (sgl.ItemTypeBit2 & GuiLib_ITEMBIT_BUTTON)
    {
      if (sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x01)
      {
        sgl.CurItem.TextLength[1] = sgl.CurItem.TextLength[0];
        sgl.CurItem.TextPtr[1] = sgl.CurItem.TextPtr[0];
      }
      if (sgl.CurItem.CompPars.CompButton.TextLikeUp & 0x02)
      {
        sgl.CurItem.TextLength[2] = sgl.CurItem.TextLength[0];
        sgl.CurItem.TextPtr[2] = sgl.CurItem.TextPtr[0];
      }
    }
#endif
  }
}
//------------------------------------------------------------------------------
static GuiConst_INT16S VarStrCmp(
   GuiConst_INT8U *S1,
   GuiConst_INT8U *S2)
{
  GuiConst_INT16S len = 0;

#ifdef GuiConst_CHARMODE_ANSI
  do
  {
    if ((*S1 == 0) && (*S2 == 0))
      return (0);

    else if (*S1 == 0)
      return (-1);
    else if (*S2 == 0)
      return (1);
    else if (*S1 < *S2)
      return (-1);
    else if (*S1 > *S2)
      return (1);
    S1++;
    S2++;
  }
  while ((len++) < GuiConst_AUTOREDRAW_MAX_VAR_SIZE);
#else
  GuiConst_INT16U T1, T2, T3;

  do
  {
    T1 = *S1++;
    T3 = *S1++;
    T1 |= T3 << 8;

    T2 = *S2++;
    T3 = *S2++;
    T2 |= T3 << 8;

    if (T1 < T2)
      return (-1);
    else if (T1 > T2)
      return (1);
    else if (T1 == 0)
      return (0);
  }
  while ((len++) < GuiConst_AUTOREDRAW_MAX_VAR_SIZE);
#endif
  return (1);
}
//------------------------------------------------------------------------------
void AutoRedraw_Init(void)
{
  GuiConst_INT16S I;

  for (I=0;I<GuiConst_MAX_DYNAMIC_ITEMS;I++)
  {
    sgl.AutoRedraw[I].Next = -1;
    sgl.AutoRedraw[I].Prev = -1;
    sgl.AutoRedraw[I].Valid = ITEM_NONE;
  }

  sgl.AutoRedrawCount = 0;
  sgl.AutoRedrawFirst = -1;
  sgl.AutoRedrawLast = -1;
  sgl.AutoRedrawCount = 0;
  sgl.AutoRedrawParent = -1;
  sgl.AutoRedrawUpdate = GuiLib_FALSE;
  sgl.AutoRedrawInsertPoint = GuiConst_MAX_DYNAMIC_ITEMS;
  sgl.AutoRedrawLatest = -1;

  return;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_Reset(void)
{
  sgl.AutoRedrawNext = sgl.AutoRedrawFirst;

  return sgl.AutoRedrawNext;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_GetNext(GuiConst_INT16S I)
{
  GuiConst_INT16S N, result;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return -1;

  result = -1;

  N = sgl.AutoRedraw[I].Next;
  while ((N >= 0) && (N < GuiConst_MAX_DYNAMIC_ITEMS))
  {
    if ((sgl.AutoRedraw[N].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
    {
      result = N;
      break;
    }
    else if (sgl.AutoRedraw[N].Valid == ITEM_NONE)
      break;
    else
    {
      N = sgl.AutoRedraw[N].Next;
      if ((N < 0) || (N >= GuiConst_MAX_DYNAMIC_ITEMS))
        break;
    }
  }

  return result;
}
//------------------------------------------------------------------------------
GuiConst_INT8S AutoRedraw_ItemIsStruct(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return GuiLib_FALSE;

  if ((sgl.AutoRedraw[I].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
  {
    if ((sgl.AutoRedraw[I].Item.ItemType == GuiLib_ITEM_STRUCTARRAY) ||
        (sgl.AutoRedraw[I].Item.ItemType == GuiLib_ITEM_STRUCTCOND))
      return GuiLib_TRUE;
    else
      return GuiLib_FALSE;
  }
  else
    return GuiLib_FALSE;
}
//------------------------------------------------------------------------------
GuiConst_INT8S AutoRedraw_GetLevel(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return -1;

  if ((sgl.AutoRedraw[I].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
    return sgl.AutoRedraw[I].Level;
  else
    return 0;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_Add(GuiLib_ItemRec *Item, GuiConst_INT16S Struct, GuiConst_INT8U Level)
{
  GuiConst_INT16S I, N;

  if (sgl.AutoRedrawCount < GuiConst_MAX_DYNAMIC_ITEMS)
  {
    I = -1;

    if (sgl.AutoRedrawFirst == -1)
    {
      I = 0;
      sgl.AutoRedrawFirst = 0;
      sgl.AutoRedrawLast  = -1;
      sgl.AutoRedrawNext  = 0;
    }
    else
    {
      for (N=0;N<GuiConst_MAX_DYNAMIC_ITEMS;N++)
      {
        if (sgl.AutoRedraw[N].Valid == ITEM_NONE)
        {
          I = N;
          break;
        }
      }
    }

    if (I >= 0)
    {
      sgl.AutoRedraw[I].Next = -1;
      sgl.AutoRedraw[I].Prev = sgl.AutoRedrawLast;
      sgl.AutoRedraw[I].Valid = ITEM_AUTOREDRAW;
      sgl.AutoRedraw[I].Parent = sgl.AutoRedrawParent;
      sgl.AutoRedraw[I].Level  = Level;
      memcpy(&sgl.AutoRedraw[I].Item, Item, sizeof(GuiLib_ItemRec));
      memcpy(&sgl.AutoRedraw[I].Memory, &sgl.Memory, sizeof(ItemMemory));

      if ((sgl.AutoRedrawLast >= 0)
       && (sgl.AutoRedrawLast < GuiConst_MAX_DYNAMIC_ITEMS))
        sgl.AutoRedraw[sgl.AutoRedrawLast].Next = I;

      sgl.AutoRedrawCount++;
      sgl.AutoRedrawLast = I;
      sgl.AutoRedrawLatest = I;
    }

  }

  return I;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_Insert(GuiLib_ItemRec *Item,
          GuiConst_INT16S Struct,
          GuiConst_INT8U Level)
{
  GuiConst_INT16S I, N;

  if ((sgl.AutoRedrawFirst == -1) ||
      (sgl.AutoRedrawInsertPoint >= GuiConst_MAX_DYNAMIC_ITEMS))
  {
    return AutoRedraw_Add(Item, Struct, Level);
  }

  if (sgl.AutoRedrawCount < GuiConst_MAX_DYNAMIC_ITEMS)
  {
    I = -1;

    for (N=0;N<GuiConst_MAX_DYNAMIC_ITEMS;N++)
    {
      if (sgl.AutoRedraw[N].Valid == ITEM_NONE)
      {
        I = N;
        break;
      }
    }
  }
  else
    return -1;

  if (I >= 0)
  {
    if (sgl.AutoRedrawInsertPoint < 0)
    {
      sgl.AutoRedraw[I].Next = sgl.AutoRedrawFirst;
      sgl.AutoRedraw[I].Prev = -1;
    }
    else
    {
      sgl.AutoRedraw[I].Next = sgl.AutoRedraw[sgl.AutoRedrawInsertPoint].Next;
      sgl.AutoRedraw[I].Prev = sgl.AutoRedrawInsertPoint;
      sgl.AutoRedraw[sgl.AutoRedraw[I].Prev].Next = I;
    }

    if (sgl.AutoRedraw[I].Next != -1)
      sgl.AutoRedraw[sgl.AutoRedraw[I].Next].Prev = I;

    sgl.AutoRedraw[I].Valid = ITEM_AUTOREDRAW;
    sgl.AutoRedraw[I].Parent = sgl.AutoRedrawParent;
    sgl.AutoRedraw[I].Level  = Level;

    memcpy(&sgl.AutoRedraw[I].Item, Item, sizeof(GuiLib_ItemRec));
    memcpy(&sgl.AutoRedraw[I].Memory, &sgl.Memory, sizeof(ItemMemory));

    if (sgl.AutoRedraw[I].Next == -1)
      sgl.AutoRedrawLast = I;

    if (sgl.AutoRedraw[I].Prev == -1)
      sgl.AutoRedrawFirst = I;

    sgl.AutoRedrawInsertPoint = I;
    sgl.AutoRedrawLatest = I;
    sgl.AutoRedrawCount++;
  }

  return I;
}
//------------------------------------------------------------------------------
void AutoRedraw_UpdateDrawn(GuiConst_INT16S I, GuiLib_ItemRec *Item)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return;

  if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
  {
    sgl.AutoRedraw[I].Item.Drawn   = Item->Drawn;
    sgl.AutoRedraw[I].Item.DrawnX1 = Item->DrawnX1;
    sgl.AutoRedraw[I].Item.DrawnY1 = Item->DrawnY1;
    sgl.AutoRedraw[I].Item.DrawnX2 = Item->DrawnX2;
    sgl.AutoRedraw[I].Item.DrawnY2 = Item->DrawnY2;
  }

  return;
}
//------------------------------------------------------------------------------
void AutoRedraw_Delete(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return;

  if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
  {
    if (sgl.AutoRedrawLast == I)
      sgl.AutoRedrawLast = sgl.AutoRedraw[I].Prev;

    if (sgl.AutoRedrawFirst == I)
      sgl.AutoRedrawFirst = sgl.AutoRedraw[I].Next;

    if (sgl.AutoRedraw[I].Prev != -1)
      sgl.AutoRedraw[sgl.AutoRedraw[I].Prev].Next = sgl.AutoRedraw[I].Next;

    if (sgl.AutoRedraw[I].Next != -1)
      sgl.AutoRedraw[sgl.AutoRedraw[I].Next].Prev = sgl.AutoRedraw[I].Prev;

    sgl.AutoRedraw[I].Next = -1;
    sgl.AutoRedraw[I].Prev = -1;
    sgl.AutoRedraw[I].Valid = ITEM_NONE;

    sgl.AutoRedrawCount--;
  }

  return;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_DeleteStruct(GuiConst_INT16S Struct_id)
{
  GuiConst_INT16S I, N, X;

  I = sgl.AutoRedrawFirst;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return -1;

  if ((Struct_id < 0) || (Struct_id >= GuiConst_MAX_DYNAMIC_ITEMS))
    return -1;

  if (sgl.AutoRedraw[Struct_id].Valid == ITEM_NONE)
    return -1;

  while (sgl.AutoRedraw[I].Valid != ITEM_NONE)
  {
    N = sgl.AutoRedraw[I].Next;
    if (sgl.AutoRedraw[I].Parent == Struct_id)
    {
      X = AutoRedraw_DeleteStruct(I);
      if ((X >= 0) || (X < GuiConst_MAX_DYNAMIC_ITEMS))
        N = sgl.AutoRedraw[X].Next;
    }

    I = N;

    if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
      break;
  }

  sgl.AutoRedrawInsertPoint = sgl.AutoRedraw[Struct_id].Prev;

  AutoRedraw_Delete(Struct_id);

  if (sgl.AutoRedrawInsertPoint >= 0)
    return sgl.AutoRedrawInsertPoint;
  else
    return sgl.AutoRedrawFirst;
}
//------------------------------------------------------------------------------
void AutoRedraw_Destroy(void)
{
  GuiConst_INT16S I;

  for (I=0;I<GuiConst_MAX_DYNAMIC_ITEMS;I++)
  {
    sgl.AutoRedraw[I].Next = -1;
    sgl.AutoRedraw[I].Prev = -1;
    sgl.AutoRedraw[I].Valid = ITEM_NONE;
  }

  sgl.AutoRedrawCount = 0;
  sgl.AutoRedrawFirst = -1;
  sgl.AutoRedrawLast = -1;
  sgl.AutoRedrawCount = 0;
  sgl.AutoRedrawParent = -1;
  sgl.AutoRedrawUpdate = GuiLib_FALSE;
  sgl.AutoRedrawInsertPoint = GuiConst_MAX_DYNAMIC_ITEMS;
  sgl.AutoRedrawLatest = -1;

  return;
}
//------------------------------------------------------------------------------
GuiLib_ItemRec *AutoRedraw_GetItem(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return NULL;
  else
    return &sgl.AutoRedraw[I].Item;
}
//------------------------------------------------------------------------------
ItemMemory *AutoRedraw_GetItemMemory(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return NULL;
  else
    return &sgl.AutoRedraw[I].Memory;
}
//------------------------------------------------------------------------------
void AutoRedraw_UpdateOnChange(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return;

  if ((sgl.AutoRedraw[I].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
  {
    sgl.AutoRedraw[I].ValueSize = 0;

    if (sgl.AutoRedraw[I].Item.UpdateType == GuiLib_UPDATE_ON_CHANGE)
    {
      if (sgl.AutoRedraw[I].Item.VarPtr != 0)
      {
        switch (sgl.AutoRedraw[I].Item.VarType)
        {
          case GuiLib_VAR_BOOL:
          case GuiLib_VAR_UNSIGNED_CHAR:
          case GuiLib_VAR_SIGNED_CHAR:
            sgl.AutoRedraw[I].ValueSize = 1;
            break;

          case GuiLib_VAR_UNSIGNED_INT:
          case GuiLib_VAR_SIGNED_INT:
            sgl.AutoRedraw[I].ValueSize = 2;
            break;

          case GuiLib_VAR_UNSIGNED_LONG:
          case GuiLib_VAR_SIGNED_LONG:
          case GuiLib_VAR_FLOAT:
            sgl.AutoRedraw[I].ValueSize = 4;
            break;

          case GuiLib_VAR_DOUBLE:
            sgl.AutoRedraw[I].ValueSize = 8;
            break;

          case GuiLib_VAR_COLOR:
            sgl.AutoRedraw[I].ValueSize =
               GuiConst_PIXEL_BYTE_SIZE;
            break;

          case GuiLib_VAR_STRING:
            sgl.AutoRedraw[I].ValueSize =
               GuiLib_AUTOREDRAW_MAX_VAR_SIZE;
            break;

          default:
            sgl.AutoRedraw[I].ValueSize = 0;
        }
        if (sgl.AutoRedraw[I].ValueSize > 0)
        {
          memcpy(&sgl.AutoRedraw[I].Value[0],
                  sgl.AutoRedraw[I].Item.VarPtr,
                  sgl.AutoRedraw[I].ValueSize);
        }
      }
    }
  }

  return;
}
//------------------------------------------------------------------------------
GuiConst_INT8S RefreshColorVariable(GuiConst_INTCOLOR *comp, GuiConst_INT16U idx)
{
  GuiConst_INTCOLOR *ColVarPtr;
  GuiConst_INTCOLOR ColVal;
  GuiConst_INT8S    changed = GuiLib_FALSE;

  if (idx != 0xFFFF)
  {
    if (idx <= GuiStruct_VarPtrCnt)
    {
      ColVarPtr = (GuiConst_INTCOLOR*)ReadWord(GuiStruct_VarPtrList[idx]);
      if (ColVarPtr != 0)
      {
        ColVal = *ColVarPtr;
        if (*comp != ColVal)
        {
          *comp = ColVal;
          changed = GuiLib_TRUE;
        }
      }
    }
  }

  return changed;
}
//------------------------------------------------------------------------------
GuiConst_INT8S AutoRedraw_VarChanged(GuiConst_INT16S I)
{
  GuiLib_ItemRecPtr Item;
  GuiConst_INT16S VarChange;
  GuiConst_INT8S  changed;

  changed = GuiLib_FALSE;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return changed;

  if ((sgl.AutoRedraw[I].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
  {
    Item = &sgl.AutoRedraw[I].Item;
    if (Item->UpdateType == GuiLib_UPDATE_ON_CHANGE)
    {
      if ((Item->TextPar[0].BitFlags & GuiLib_BITFLAG_INUSE) &&
          (Item->VarPtr != 0))
      {
        VarChange = 0;
        if (sgl.AutoRedraw[I].ValueSize > 0)
        {
          if (Item->VarType == GuiLib_VAR_STRING)
            VarChange = VarStrCmp(&sgl.AutoRedraw[I].Value[0],
               (GuiConst_INT8U*)Item->VarPtr);
          else
            VarChange =
               memcmp(&sgl.AutoRedraw[I].Value[0],
                       Item->VarPtr,
                       sgl.AutoRedraw[I].ValueSize);
        }
        if ((sgl.AutoRedraw[I].ValueSize == 0) || (VarChange != 0))
          changed = GuiLib_TRUE;
      }
    }
    else
      changed = GuiLib_TRUE;

    changed |= RefreshColorVariable(&Item->ForeColor, Item->ForeColorIndex);
    changed |= RefreshColorVariable(&Item->BackColor, Item->BackColorIndex);
    changed |= RefreshColorVariable(&Item->BarForeColor, Item->BarForeColorIndex);
    changed |= RefreshColorVariable(&Item->BarBackColor, Item->BarBackColorIndex);

#ifdef GuiConst_ITEM_RADIOBUTTON_INUSE
    if (Item->ItemType == GuiLib_ITEM_RADIOBUTTON)
      changed |= RefreshColorVariable(&Item->CompPars.CompRadioButton.MarkColor,
                           Item->CompPars.CompRadioButton.MarkColorIndex);
#endif
#ifdef GuiConst_ITEM_CHECKBOX_INUSE
    if (Item->ItemType == GuiLib_ITEM_CHECKBOX)
      changed |= RefreshColorVariable(&Item->CompPars.CompCheckBox.MarkColor,
                           Item->CompPars.CompCheckBox.MarkColorIndex);
#endif
#ifdef GuiConst_ITEM_BUTTON_INUSE
    if (Item->ItemType == GuiLib_ITEM_BUTTON)
    {
      GuiConst_INT32S state;

      if ((sgl.CurItem.VarPtr != 0) &&
          (sgl.CurItem.VarType != GuiLib_VAR_STRING))
      {
        state = ReadVar(Item->VarPtr, Item->VarType);
        if ((state < GuiLib_BUTTON_STATE_UP) ||
            (state > GuiLib_BUTTON_STATE_DISABLED))
          state = GuiLib_BUTTON_STATE_UP;
      }
      else
        state = GuiLib_BUTTON_STATE_UP;


      changed |= RefreshColorVariable(&Item->CompPars.CompButton.TextColor[state],
                           Item->CompPars.CompButton.TextColorIndex[state]);

      changed |= RefreshColorVariable(&Item->CompPars.CompButton.GlyphIconColor[state],
                           Item->CompPars.CompButton.GlyphIconColorIndex[state]);
    }
#endif
  }

  return changed;
}
//------------------------------------------------------------------------------
void AutoRedraw_UpdateVar(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return;

  if ((sgl.AutoRedraw[I].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
  {
    if (sgl.AutoRedraw[I].Item.UpdateType == GuiLib_UPDATE_ON_CHANGE)
    {
      if ((sgl.AutoRedraw[I].Item.TextPar[0].BitFlags & GuiLib_BITFLAG_INUSE) &&
          (sgl.AutoRedraw[I].Item.VarPtr != 0))
      {
        if (sgl.AutoRedraw[I].ValueSize > 0)
        {
          memcpy(&sgl.AutoRedraw[I].Value[0],
                  sgl.AutoRedraw[I].Item.VarPtr,
                  sgl.AutoRedraw[I].ValueSize);
        }
      }
    }
  }

  return;
}

#ifdef GuiConst_CURSOR_SUPPORT_ON
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_InsertCursor(GuiLib_ItemRec *Item,
                  GuiConst_INT16S Struct,
                  GuiConst_INT8U Level)
{
  GuiConst_INT16S I;

  I = AutoRedraw_Insert(Item, Struct, Level);

  if (I != -1)
    sgl.AutoRedraw[I].Valid = ITEM_CURSOR;

  return I;
}
//------------------------------------------------------------------------------
void AutoRedraw_SetAsCursor(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return;

  if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    sgl.AutoRedraw[I].Valid |= ITEM_CURSOR;

  return;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_IsOnlyCursor(GuiConst_INT16S I)
{
  GuiConst_INT16S ret = GuiLib_FALSE;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return ret;

  if (sgl.AutoRedraw[I].Valid == ITEM_CURSOR)
    ret = GuiLib_TRUE;

  return ret;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_GetCursor(GuiConst_INT8S C, GuiConst_INT16S I)
{
  if (I == -1)
  {
    I = sgl.AutoRedrawFirst;

    if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
      return -1;
  }
  else
  {
    if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
      return -1;

    if (sgl.AutoRedraw[I].Valid == ITEM_NONE)
      return -1;

    I = sgl.AutoRedraw[I].Next;

    if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
      return -1;
  }

  while (I != -1)
  {
    if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    {
      if ((sgl.AutoRedraw[I].Valid & ITEM_CURSOR) == ITEM_CURSOR)
      {
        if (sgl.AutoRedraw[I].Item.CursorFieldNo == C)
          break;
      }
      I = sgl.AutoRedraw[I].Next;
    }
    else
      return -1;
  }

  return I;
}
//------------------------------------------------------------------------------
GuiConst_INT8S AutoRedraw_GetCursorNumber(GuiConst_INT16S I)
{
  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return -1;

  if (sgl.AutoRedraw[I].Valid & ITEM_CURSOR)
    return sgl.AutoRedraw[I].Item.CursorFieldNo;

  return -1;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_GetFirstCursor(void)
{
  GuiConst_INT16S I, result;
  GuiConst_INT8S C;

  I = sgl.AutoRedrawFirst;
  C = 0x7F;

  result = -1;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return result;

  while (I != -1)
  {
    if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    {
      if ((sgl.AutoRedraw[I].Valid & ITEM_CURSOR) == ITEM_CURSOR)
      {
        if (sgl.AutoRedraw[I].Item.CursorFieldNo < C)
        {
          C = sgl.AutoRedraw[I].Item.CursorFieldNo;
          result = I;
        }
      }
      I = sgl.AutoRedraw[I].Next;
    }
    else
      return -1;
  }

  return result;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_GetLastCursor(void)
{
  GuiConst_INT16S I, result;
  GuiConst_INT8S C;

  I = sgl.AutoRedrawFirst;
  C = 0;

  result = -1;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return result;

  while (I != -1)
  {
    if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    {
      if ((sgl.AutoRedraw[I].Valid & ITEM_CURSOR) == ITEM_CURSOR)
      {
        if (sgl.AutoRedraw[I].Item.CursorFieldNo > C)
        {
          C = sgl.AutoRedraw[I].Item.CursorFieldNo;
          result = I;
        }
      }
      I = sgl.AutoRedraw[I].Next;
    }
    else
      return -1;
  }

  return result;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_GetNextCursor(GuiConst_INT8S C)
{
  GuiConst_INT16S I, result;
  GuiConst_INT8S closest;

  I = sgl.AutoRedrawFirst;
  closest = 0x7f;

  result = -1;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return result;

  while (I != -1)
  {
    if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    {
      if ((sgl.AutoRedraw[I].Valid & ITEM_CURSOR) == ITEM_CURSOR)
      {
        if ((sgl.AutoRedraw[I].Item.CursorFieldNo > C)
         && (sgl.AutoRedraw[I].Item.CursorFieldNo <= closest))
        {
          closest = sgl.AutoRedraw[I].Item.CursorFieldNo;
          result = I;
        }
      }
      I = sgl.AutoRedraw[I].Next;
    }
    else
      return -1;
  }

  return result;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_GetPrevCursor(GuiConst_INT8S C)
{
  GuiConst_INT16S I, result;
  GuiConst_INT8S closest;

  I = sgl.AutoRedrawFirst;
  closest = 0;

  result = -1;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return result;

  while (I != -1)
  {
    if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    {
      if ((sgl.AutoRedraw[I].Valid & ITEM_CURSOR) == ITEM_CURSOR)
      {
        if ((sgl.AutoRedraw[I].Item.CursorFieldNo < C)
         && (sgl.AutoRedraw[I].Item.CursorFieldNo >= closest))
        {
          closest = sgl.AutoRedraw[I].Item.CursorFieldNo;
          result = I;
        }
      }
      I = sgl.AutoRedraw[I].Next;
    }
    else
      return -1;
  }

  return result;
}
//------------------------------------------------------------------------------
GuiConst_INT16S AutoRedraw_CheckCursorInheritance(GuiConst_INT16S N)
{
  GuiConst_INT16S I, J, result;
  GuiConst_INT16S cursor_id;

  if (GuiLib_ActiveCursorFieldNo < 0)
    return -1;

  if ((N < 0) || (N >= GuiConst_MAX_DYNAMIC_ITEMS))
    return -1;

  if (sgl.AutoRedraw[N].Valid == ITEM_NONE)
    return -1;

  I = sgl.AutoRedraw[N].Parent;
  result = 1;

  while (result == 1)
  {
    if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    {
      result = -1;
      break;
    }

    if (sgl.AutoRedraw[I].Valid != ITEM_NONE)
    {
      if ((sgl.AutoRedraw[I].Valid & ITEM_CURSOR) == ITEM_CURSOR)
      {
        cursor_id = sgl.AutoRedraw[I].Item.CursorFieldNo;

        if (cursor_id == GuiLib_ActiveCursorFieldNo)
        {
          result = 0;
          break;
        }
      }
      J = sgl.AutoRedraw[I].Parent;
      if (J == I)
        result = -1;
      I = J;
    }
    else
    {
      result = -1;
      break;
    }

  }

  return result;
}
//------------------------------------------------------------------------------
void AutoRedraw_ResetCursor(void)
{
  GuiConst_INT16S I;

  I = sgl.AutoRedrawFirst;

  if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
    return;

  while ((sgl.AutoRedraw[I].Valid & ITEM_AUTOREDRAW) == ITEM_AUTOREDRAW)
  {
    sgl.AutoRedraw[I].Item.CursorFieldNo = GuiLib_NO_CURSOR;

    I = sgl.AutoRedraw[I].Next;
    if ((I < 0) || (I >= GuiConst_MAX_DYNAMIC_ITEMS))
      break;
  }

  return;
}
#endif // GuiConst_CURSOR_SUPPORT_ON

