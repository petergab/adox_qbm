// My own header is inserted here - edit it in the C code generation window

// -----------------------------------------------------------------------
//
//                            *** ATTENTION ***
//
//   This file is maintained by the easyGUI Graphical User Interface
//   program. Modifications should therefore not be made directly in
//   this file, as the changes will be lost next time easyGUI creates
//   the file.
//
//                            *** ATTENTION ***
//
// -----------------------------------------------------------------------

//
// Generated by easyGUI version 6.0.8.004
// Generated for project CIE_LPC4088_7inc
// in project file 
//    C:\Users\Sergio.Cappuccio\Desktop\2019 05 21 SBM ver 1.03 - ADOX 2019\work - G1.10.000.gui
//

// Ensure that this header file is only included once
#ifndef __GUIFONT_H
#define __GUIFONT_H

// --------------------------  INCLUDE SECTION  --------------------------

#include "GuiConst.h"
#include "GuiLib.h"

// ---------------------  GLOBAL DECLARATION SECTION  --------------------

#define GuiFont_CharCnt  1662
extern const GuiConst_PTR const GuiFont_ChPtrList[GuiFont_CharCnt];

// Fonts
// =====

#define GuiFont_DEFAULT_TEXT_FONT      0

#define GuiFont_ANSI7                  1
#define GuiFont_ANSI11Condensed        2
#define GuiFont_ANSI13                 3
#define GuiFont_ANSI17AA               4
#define GuiFont_ANSI19                 5
#define GuiFont_ANSI24                 6
#define GuiFont_ANSI30                 7
#define GuiFont_list_font              8
#define GuiFont_ANT                    9
#define GuiFont_Icon32x32              10
#define GuiFont_Icon48x48              11
#define GuiFont_Rob_1                  12
#define GuiFont_Rob_2                  13
#define GuiFont_Rob_3                  14
#define GuiFont_Rob_4                  15

#define GuiFont_FontCnt  16
extern const GuiLib_FontRecPtr GuiFont_FontList[GuiFont_FontCnt];

extern const GuiConst_INT8U GuiFont_LanguageActive[GuiConst_LANGUAGE_CNT];

extern const GuiConst_INT16U GuiFont_LanguageIndex[GuiConst_LANGUAGE_CNT];

extern const GuiConst_INT8U GuiFont_LanguageTextDir[GuiConst_LANGUAGE_CNT];

extern const GuiConst_INT8U GuiFont_DecimalChar[GuiConst_LANGUAGE_CNT];


// --------------------------  CUSTOM SECTION  ---------------------------

// My own code is inserted here - edit it in the C code generation window

// -----------------------------------------------------------------------

#endif

// -----------------------------------------------------------------------

// My own footer is inserted here - edit it in the C code generation window
