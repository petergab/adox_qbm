/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*           2 bit (4 shade grayscale) graphics primitives library          */
/*                       Vertical display bytes layout                      */
/*                              2 color planes                              */
/*                               v6.0.8.004                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
GuiConst_INT8U
   GuiLib_DisplayBuf[2][GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];

#ifdef GuiConst_CLIPPING_SUPPORT_ON
GuiConst_INT8U ClippingYPattern[GuiConst_BYTE_LINES];
#endif
const GuiConst_INT8U LinePatternTop[8] =
#ifdef GuiConst_BIT_TOPLEFT
   {0xFF, 0xFE, 0xFC, 0xF8, 0xF0, 0xE0, 0xC0, 0x80};
#else
   {0xFF, 0x7F, 0x3F, 0x1F, 0x0F, 0x07, 0x03, 0x01};
#endif
const GuiConst_INT8U LinePatternBottom[8] =
#ifdef GuiConst_BIT_TOPLEFT
  {0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0xFF};
#else
  {0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF};
#endif
const GuiConst_INT8U LinePatternPixel[8] =
#ifdef GuiConst_BIT_TOPLEFT
  {0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF, 0xBF, 0x7F};
#else
  {0x7F, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0xFE};
#endif
const GuiConst_INT8U ColorPattern0[8][4] = {
#ifdef GuiConst_BIT_TOPLEFT
  {0x00, 0x00, 0x01, 0x01},
  {0x00, 0x00, 0x02, 0x02},
  {0x00, 0x00, 0x04, 0x04},
  {0x00, 0x00, 0x08, 0x08},
  {0x00, 0x00, 0x10, 0x10},
  {0x00, 0x00, 0x20, 0x20},
  {0x00, 0x00, 0x40, 0x40},
  {0x00, 0x00, 0x80, 0x80}
#else
  {0x00, 0x00, 0x80, 0x80},
  {0x00, 0x00, 0x40, 0x40},
  {0x00, 0x00, 0x20, 0x20},
  {0x00, 0x00, 0x10, 0x10},
  {0x00, 0x00, 0x08, 0x08},
  {0x00, 0x00, 0x04, 0x04},
  {0x00, 0x00, 0x02, 0x02},
  {0x00, 0x00, 0x01, 0x01}
#endif
  };
const GuiConst_INT8U ColorPattern1[8][4] = {
#ifdef GuiConst_BIT_TOPLEFT
  {0x00, 0x01, 0x00, 0x01},
  {0x00, 0x02, 0x00, 0x02},
  {0x00, 0x04, 0x00, 0x04},
  {0x00, 0x08, 0x00, 0x08},
  {0x00, 0x10, 0x00, 0x10},
  {0x00, 0x20, 0x00, 0x20},
  {0x00, 0x40, 0x00, 0x40},
  {0x00, 0x80, 0x00, 0x80}
#else
  {0x00, 0x80, 0x00, 0x80},
  {0x00, 0x40, 0x00, 0x40},
  {0x00, 0x20, 0x00, 0x20},
  {0x00, 0x10, 0x00, 0x10},
  {0x00, 0x08, 0x00, 0x08},
  {0x00, 0x04, 0x00, 0x04},
  {0x00, 0x02, 0x00, 0x02},
  {0x00, 0x01, 0x00, 0x01}
#endif
  };
GuiConst_INT8U ColorBitPattern0[4] = {0x00, 0x00, 0xFF, 0xFF};
GuiConst_INT8U ColorBitPattern1[4] = {0x00, 0xFF, 0x00, 0xFF};

#ifdef GuiConst_CLIPPING_SUPPORT_ON
static GuiConst_INT8U ClippingYOff;
#endif

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S C;
  GuiConst_INT16S ClippingMinYb;
  GuiConst_INT16S ClippingMaxYb;

  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > sgl.CurLayerWidth - 1)
    X2 = sgl.CurLayerWidth - 1;
  if (Y2 > sgl.CurLayerHeight - 1)
    Y2 = sgl.CurLayerHeight - 1;

  sgl.ClippingX1 = X1;
  sgl.ClippingY1 = Y1;
  sgl.ClippingX2 = X2;
  sgl.ClippingY2 = Y2;
  ClippingYOff = ((Y1 == 0) && (Y2 == sgl.CurLayerHeight - 1));

  if (!ClippingYOff)
  {
    ClippingMinYb = sgl.ClippingY1 / 8;
    ClippingMaxYb = sgl.ClippingY2 / 8;

    for (C = 0; C < ClippingMinYb; C++)
      ClippingYPattern[C] = 0x00;
    ClippingYPattern[ClippingMinYb] = LinePatternTop[sgl.ClippingY1 % 8];
    for (C = ClippingMinYb + 1; C < ClippingMaxYb; C++)
      ClippingYPattern[C] = 0xFF;
    ClippingYPattern[ClippingMaxYb] = LinePatternBottom[sgl.ClippingY2 % 8];
    for (C = ClippingMaxYb + 1; C < GuiConst_BYTE_LINES; C++)
      ClippingYPattern[C] = 0x00;
  }
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

  Y1 = Y1 / 8;
  Y2 = Y2 / 8;

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(sgl.CurLayerBufPtr, 0xFF, sgl.CurLayerBytes);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
    PixelPtr = sgl.CurLayerBufPtr + (Y / 8) * sgl.CurLayerLineSize + X;
    *PixelPtr = ColorPattern0[Y % 8][Color] |
       (*PixelPtr & LinePatternPixel[Y % 8]);
    PixelPtr += (GuiConst_DISPLAY_BYTES / 2);
    *PixelPtr = ColorPattern1[Y % 8][Color] |
       (*PixelPtr & LinePatternPixel[Y % 8]);

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  GuiConst_INT8U Pix;
  GuiConst_INT8U *PixelPtr;

  if ((X < 0) || (X >= sgl.CurLayerWidth) ||
      (Y < 0) || (Y >= sgl.CurLayerHeight))
    return (0);
  else
  {
    PixelPtr = sgl.CurLayerBufPtr + (Y / 8) * sgl.CurLayerLineSize + X;
#ifdef GuiConst_BIT_TOPLEFT
    if (*PixelPtr && (0x01 << (Y % 8)) == 0)
#else
    if (*PixelPtr && (0x80 >> (Y % 8)) == 0)
#endif
      Pix = 0;
    else
      Pix = 1;
    PixelPtr += (GuiConst_DISPLAY_BYTES / 2);
#ifdef GuiConst_BIT_TOPLEFT
    if (*PixelPtr && (0x01 << (Y % 8)) == 0)
#else
    if (*PixelPtr && (0x80 >> (Y % 8)) == 0)
#endif
      return (Pix);
    else
      return (2+Pix);
  }
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Yb, Ym;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  Yb = Y / 8;
  Ym = Y % 8;

  PixelPtr1 = sgl.CurLayerBufPtr + Yb * sgl.CurLayerLineSize + X1;
  PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
  while (X1 <= X2)
  {
    *PixelPtr1 =
       ColorPattern0[Ym][Color] | (*PixelPtr1 & LinePatternPixel[Ym]);
    *PixelPtr2 =
       ColorPattern1[Ym][Color] | (*PixelPtr2 & LinePatternPixel[Ym]);
    PixelPtr1++;
    PixelPtr2++;
    X1++;
  }
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Y, Y1b, Y2b, Y1m, Y2m;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  Y1b = Y1 / 8;
  Y2b = Y2 / 8;
  Y1m = Y1 % 8;
  Y2m = Y2 % 8;
  PixelPtr1 = sgl.CurLayerBufPtr + Y1b * sgl.CurLayerLineSize + X;
  PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);

  if (Y1b == Y2b)
  {
    *PixelPtr1 = (*PixelPtr1 &
       ~(LinePatternTop[Y1m] & LinePatternBottom[Y2m])) |
       (ColorBitPattern0[Color] &
       (LinePatternTop[Y1m] & LinePatternBottom[Y2m]));
    *PixelPtr2 = (*PixelPtr2 &
       ~(LinePatternTop[Y1m] & LinePatternBottom[Y2m])) |
       (ColorBitPattern1[Color] &
       (LinePatternTop[Y1m] & LinePatternBottom[Y2m]));
  }
  else
  {
    *PixelPtr1 =
       (*PixelPtr1 & ~LinePatternTop[Y1m]) |
       (ColorBitPattern0[Color] & LinePatternTop[Y1m]);
    *PixelPtr2 =
       (*PixelPtr2 & ~LinePatternTop[Y1m]) |
       (ColorBitPattern1[Color] & LinePatternTop[Y1m]);
    PixelPtr1 += sgl.CurLayerLineSize;
    PixelPtr2 += sgl.CurLayerLineSize;

    for (Y = Y1b + 1; Y < Y2b; Y++)
    {
      *PixelPtr1 = ColorBitPattern0[Color];
      *PixelPtr2 = ColorBitPattern1[Color];
      PixelPtr1 += sgl.CurLayerLineSize;
      PixelPtr2 += sgl.CurLayerLineSize;
    }

    *PixelPtr1 =
       (*PixelPtr1 & ~LinePatternBottom[Y2m]) |
       (ColorBitPattern0[Color] & LinePatternBottom[Y2m]);
    *PixelPtr2 =
       (*PixelPtr2 & ~LinePatternBottom[Y2m]) |
       (ColorBitPattern1[Color] & LinePatternBottom[Y2m]);
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT8S BitOffset1, BitOffset2;
  GuiConst_INT8U PixelByte1Visible, PixelByte2Visible;
  GuiConst_INT8U PixelPattern1, PixelPattern2;
  GuiConst_INT16S N;
  GuiConst_INT8U XWidth;
  GuiConst_INT16S By;
  GuiConst_INT16S PX, X2;
  GuiConst_INT8U PixelLineSize;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif
  GuiConst_INT8U ColorBit0;
  GuiConst_INT8U ColorBit1;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (sgl.ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != sgl.CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       sgl.GuiLib_RemoteFontBuffer);
    sgl.CurRemoteFont = CharNdx;
  }
  CharPtr = &sgl.GuiLib_RemoteFontBuffer[0];
#endif

  if ((*(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);
  ColorBit0 = (Color & 0x02) >> 1;
  ColorBit1 = Color & 0x01;

  gl.Dummy1_8U = Font->LineSize;   // To avoid compiler warning
#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  XWidth = Font->YSize;
  #else
  XWidth = Font->XSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
  PixelLineSize = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  XWidth = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #else
  PixelLineSize = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  XWidth = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = (XWidth + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
  PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif

#else

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif

#endif

  BitOffset1 = Y % 8;
  if (Y < 0)
    BitOffset1 += 8;
  BitOffset2 = 8 - BitOffset1;

  PX = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = *LineCtrl;
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PX < XWidth)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PX >= XWidth - 1));

      PX++;
      if (PX % 8 == 7)
      {
        LineCtrlByte = *LineCtrl;
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

    By = Y / 8;
    if (Y < 0)
      By--;

    for (N = 0; N < PixelLineSize; N++)
    {
      if (*PixelData != 0)
      {
        if (!BitOffset1)
        {
          PixelByte1Visible = ((By >= 0) && (By < GuiConst_BYTE_LINES));
          if (PixelByte1Visible)
          {
            PixelPattern1 = *PixelData;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
            if (!ClippingYOff)
              PixelPattern1 = PixelPattern1 & ClippingYPattern[By];
#endif

            X2 = X;
            PixelPtr1 = sgl.CurLayerBufPtr + By * sgl.CurLayerLineSize + X2;
            PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
#ifndef GuiConst_FONT_UNCOMPRESSED
            for (M = 0; M < LineRepeat; M++)
#endif
            {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
              if ((X2 >= sgl.ClippingX1) && (X2 <= sgl.ClippingX2))
#endif
              {
                if (ColorBit0)
                  *PixelPtr1 |= PixelPattern1;
                else
                  *PixelPtr1 &= ~PixelPattern1;
                if (ColorBit1)
                  *PixelPtr2 |= PixelPattern1;
                else
                  *PixelPtr2 &= ~PixelPattern1;
              }
              PixelPtr1++;
              PixelPtr2++;
              X2++;
            }
          }
        }
        else
        {
          PixelByte1Visible = ((By >= 0) && (By < GuiConst_BYTE_LINES));
          PixelByte2Visible = ((By + 1 >= 0) && (By + 1 < GuiConst_BYTE_LINES));
#ifdef GuiConst_BIT_TOPLEFT
          PixelPattern1 = *PixelData << BitOffset1;
          PixelPattern2 = *PixelData >> BitOffset2;
#else
          PixelPattern1 = *PixelData >> BitOffset1;
          PixelPattern2 = *PixelData << BitOffset2;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
          if (!ClippingYOff)
          {
            PixelPattern1 = PixelPattern1 & ClippingYPattern[By];
            PixelPattern2 = PixelPattern2 & ClippingYPattern[By + 1];
          }
#endif

          X2 = X;
          PixelPtr1 = sgl.CurLayerBufPtr + By * sgl.CurLayerLineSize + X2;
          PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
#ifndef GuiConst_FONT_UNCOMPRESSED
          for (M = 0; M < LineRepeat; M++)
#endif
          {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
            if ((X2 >= sgl.ClippingX1) && (X2 <= sgl.ClippingX2))
#endif
            {
              if (PixelByte1Visible)
              {
                if (ColorBit0)
                  *PixelPtr1 |= PixelPattern1;
                else
                  *PixelPtr1 &= ~PixelPattern1;
                if (ColorBit1)
                  *PixelPtr2 |= PixelPattern1;
                else
                  *PixelPtr2 &= ~PixelPattern1;
              }
              if (PixelByte2Visible)
              {
                if (ColorBit0)
                  *(PixelPtr1 + sgl.CurLayerLineSize) |= PixelPattern2;
                else
                  *(PixelPtr1 + sgl.CurLayerLineSize) &= ~PixelPattern2;
                if (ColorBit1)
                  *(PixelPtr2 + sgl.CurLayerLineSize) |= PixelPattern2;
                else
                  *(PixelPtr2 + sgl.CurLayerLineSize) &= ~PixelPattern2;
              }
            }
            PixelPtr1++;
            PixelPtr2++;
            X2++;
          }
        }
      }

      PixelData++;
      By++;
    }

#ifdef GuiConst_FONT_UNCOMPRESSED
    PX++;
    X++;
#else
    X += LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT16S ByteSizeY;
  GuiConst_INT8U PrefixRom *PixelDataPtr2;
  GuiConst_INT8U B;
  GuiConst_INT16S Y1b, Y2b, Yb;
  GuiConst_INT8S BitOffset1a, BitOffset1b;
  GuiConst_INT8U BitOffsetY1, BitOffsetY2;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  SizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  ByteSizeY = (SizeY + 7) / 8;

#ifdef GuiConst_ROTATED_90DEGREE
  sgl.BitmapWriteX2 = X + SizeY - 1;
  sgl.BitmapWriteY2 = Y + SizeX - 1;
#else
  sgl.BitmapWriteX2 = X + SizeX - 1;
  sgl.BitmapWriteY2 = Y + SizeY - 1;
#endif

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST_TRANSP(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
      return;
    if (X1 < X)
      X1 = X;
    if (X2 > X + SizeX - 1)
      X2 = X + SizeX - 1;
    if (Y1 < Y)
      Y1 = Y;
    if (Y2 > Y + SizeY - 1)
      Y2 = Y + SizeY - 1;
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

    Y1b = Y1 / 8;
    Y2b = Y2 / 8;

    PixelDataPtr += 2 * (ByteSizeY * (X1 - X) + Y1b - (Y / 8));

    BitOffset1a = Y % 8;
    if (BitOffset1a < 0)
      BitOffset1a += 8;
    BitOffset1b = 8 - BitOffset1a;
    BitOffsetY1 = Y1 % 8;
    BitOffsetY2 = Y2 % 8;

    while (X1 <= X2)
    {
      PixelDataPtr2 = PixelDataPtr;

      PixelPtr1 = sgl.CurLayerBufPtr + Y1b * sgl.CurLayerLineSize + X1;
      PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
      if (Y % 8 == 7)
      {
        if (Y1b == Y2b)
        {
          *PixelPtr1 =
             (*PixelPtr1 &
             ~(LinePatternTop[BitOffsetY1] &
             LinePatternBottom[BitOffsetY2])) |
             (*PixelDataPtr2 &
             (LinePatternTop[BitOffsetY1] & LinePatternBottom[BitOffsetY2]));
          PixelDataPtr2++;
          *PixelPtr2 =
             (*PixelPtr2 &
             ~(LinePatternTop[BitOffsetY1] &
             LinePatternBottom[BitOffsetY2])) |
             (*PixelDataPtr2 &
             (LinePatternTop[BitOffsetY1] & LinePatternBottom[BitOffsetY2]));
        }
        else
        {
          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternTop[BitOffsetY1]) |
             (*PixelDataPtr2 & LinePatternTop[BitOffsetY1]);
          PixelDataPtr2++;
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternTop[BitOffsetY1]) |
             (*PixelDataPtr2 & LinePatternTop[BitOffsetY1]);
          PixelDataPtr2++;
          PixelPtr1 += sgl.CurLayerLineSize;
          PixelPtr2 += sgl.CurLayerLineSize;

          for (Yb = Y1b + 1; Yb < Y2b; Yb++)
          {
            *PixelPtr1 = *PixelDataPtr2;
            PixelDataPtr2++;
            *PixelPtr2 = *PixelDataPtr2;
            PixelDataPtr2++;
            PixelPtr1 += sgl.CurLayerLineSize;
            PixelPtr2 += sgl.CurLayerLineSize;
          }

          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternBottom[BitOffsetY2]) |
             (*PixelDataPtr2 & LinePatternBottom[BitOffsetY2]);
          PixelDataPtr2++;
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternBottom[BitOffsetY2]) |
             (*PixelDataPtr2 & LinePatternBottom[BitOffsetY2]);
        }
      }
      else
      {
        if (Y1b == Y2b)
        {
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*PixelDataPtr2) & LinePatternBottom[BitOffsetY1]) <<
             BitOffset1a;
#else
          B = (*PixelDataPtr2) >> BitOffset1a;
#endif
          *PixelPtr1 =
             (*PixelPtr1 &
             ~(LinePatternTop[BitOffsetY1] &
             LinePatternBottom[BitOffsetY2])) |
             (B & (LinePatternTop[BitOffsetY1] &
             LinePatternBottom[BitOffsetY2]));
          PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*PixelDataPtr2) & LinePatternBottom[BitOffsetY1]) <<
             BitOffset1a;
#else
          B = (*PixelDataPtr2) >> BitOffset1a;
#endif
          *PixelPtr2 =
             (*PixelPtr2 &
             ~(LinePatternTop[BitOffsetY1] &
             LinePatternBottom[BitOffsetY2])) |
             (B & (LinePatternTop[BitOffsetY1] &
             LinePatternBottom[BitOffsetY2]));
        }
        else
        {
#ifdef GuiConst_BIT_TOPLEFT
          B = *PixelDataPtr2 << BitOffset1a;
#else
          B = *PixelDataPtr2 >> BitOffset1a;
#endif
          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternTop[BitOffsetY1]) |
             (B & LinePatternTop[BitOffsetY1]);
          PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
          B = *PixelDataPtr2 << BitOffset1a;
#else
          B = *PixelDataPtr2 >> BitOffset1a;
#endif
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternTop[BitOffsetY1]) |
             (B & LinePatternTop[BitOffsetY1]);
          PixelDataPtr2++;
          PixelPtr1 += sgl.CurLayerLineSize;
          PixelPtr2 += sgl.CurLayerLineSize;

          for (Yb = Y1b + 1; Yb < Y2b; Yb++)
          {
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 2)) >> BitOffset1b) +
                (*PixelDataPtr2 << BitOffset1a);
#else
            B = ((*(PixelDataPtr2 - 2)) << BitOffset1b) +
                (*PixelDataPtr2 >> BitOffset1a);
#endif
            *PixelPtr1 = B;
            PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 2)) >> BitOffset1b) +
                (*PixelDataPtr2 << BitOffset1a);
#else
            B = ((*(PixelDataPtr2 - 2)) << BitOffset1b) +
                (*PixelDataPtr2 >> BitOffset1a);
#endif
            *PixelPtr2 = B;
            PixelDataPtr2++;
            PixelPtr1 += sgl.CurLayerLineSize;
            PixelPtr2 += sgl.CurLayerLineSize;
          }

#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 2)) >> BitOffset1b) +
              (*PixelDataPtr2 << BitOffset1a);
#else
          B = ((*(PixelDataPtr2 - 2)) << BitOffset1b) +
              (*PixelDataPtr2 >> BitOffset1a);
#endif
          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternBottom[BitOffsetY2]) |
             (B & LinePatternBottom[BitOffsetY2]);
          PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 2)) >> BitOffset1b) +
              (*PixelDataPtr2 << BitOffset1a);
#else
          B = ((*(PixelDataPtr2 - 2)) << BitOffset1b) +
              (*PixelDataPtr2 >> BitOffset1a);
#endif
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternBottom[BitOffsetY2]) |
             (B & LinePatternBottom[BitOffsetY2]);
        }
      }

      PixelDataPtr += 2 * ByteSizeY;
      X1++;
    }
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S Y, Y1b, Y2b;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
    while (X1 <= X2)
    {
      Y1b = Y1 / 8;
      Y2b = Y2 / 8;
      PixelPtr1 = sgl.CurLayerBufPtr + Y1b * sgl.CurLayerLineSize + X1;
      PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);

      if (Y1b == Y2b)
      {
        *PixelPtr1 ^= LinePatternTop[Y1 % 8] & LinePatternBottom[Y2 % 8];
        *PixelPtr2 ^= LinePatternTop[Y1 % 8] & LinePatternBottom[Y2 % 8];
      }
      else
      {
        *PixelPtr1 ^= LinePatternTop[Y1 % 8];
        *PixelPtr2 ^= LinePatternTop[Y1 % 8];
        PixelPtr1 += sgl.CurLayerLineSize;
        PixelPtr2 += sgl.CurLayerLineSize;

        for (Y = Y1b + 1; Y < Y2b; Y++)
        {
          *PixelPtr1 = ~*PixelPtr1;
          *PixelPtr2 = ~*PixelPtr2;
          PixelPtr1 += sgl.CurLayerLineSize;
          PixelPtr2 += sgl.CurLayerLineSize;
        }

        *PixelPtr1 ^= LinePatternBottom[Y2 % 8];
        *PixelPtr2 ^= LinePatternBottom[Y2 % 8];
      }

      X1++;
    }
  }
}

