/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2012 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                               v6.0.4.003                                 */
/*                                                                          */
/* ************************************************************************ */


// =============================================================================
#ifndef __GUILIB_H
#define __GUILIB_H
#include "GuiConst.h"
#include "GuiVar.h"
#include "GuiDisplay.h"


// =============================================================================
#ifdef GuiConst_CODEVISION_COMPILER
// Remove warnings about unused functions
#pragma used+
#endif


// =============================================================================
#ifdef GuiConst_COLOR_DEPTH_5
  #define GuiLib_COLOR_UNIT_8
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_8
  #define GuiLib_COLOR_UNIT_8
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_12
  #define GuiLib_COLOR_UNIT_16
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_15
  #define GuiLib_COLOR_UNIT_16
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_16
  #define GuiLib_COLOR_UNIT_16
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_18
  #define GuiLib_COLOR_UNIT_24
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_24
  #define GuiLib_COLOR_UNIT_24
  #define GuiLib_LAYERS_SUPPORTED
#endif
#ifdef GuiConst_COLOR_DEPTH_32
  #define GuiLib_COLOR_UNIT_32
  #define GuiLib_LAYERS_SUPPORTED
#endif


// =============================================================================
#ifdef GuiConst_AVR_COMPILER_FLASH_RAM
  #define PrefixRom __flash
  #define PrefixGeneric __generic
  #define PrefixReentrant
#else
#ifdef GuiConst_PICC_COMPILER_ROM
  #define PrefixRom rom
  #define PrefixGeneric
  #define PrefixReentrant
#else
#ifdef GuiConst_KEIL_COMPILER_REENTRANT
  #define PrefixRom
  #define PrefixGeneric
  #define PrefixReentrant reentrant
#else
#ifdef GuiConst_ICC_COMPILER
  #define PrefixRom const
  #define PrefixGeneric
  #define PrefixReentrant
#else
#ifdef GuiConst_CODEVISION_COMPILER
  #define PrefixRom flash
  #define PrefixGeneric
  #define PrefixReentrant
#else
  #define PrefixRom
  #define PrefixGeneric
  #define PrefixReentrant
#endif
#endif
#endif
#endif
#endif


// =============================================================================
#ifdef GuiConst_COLOR_DEPTH_1
  #define GuiLib_COLOR_BYTESIZE_1
#endif
#ifdef GuiConst_COLOR_DEPTH_2
  #define GuiLib_COLOR_BYTESIZE_1
#endif
#ifdef GuiConst_COLOR_DEPTH_4
  #define GuiLib_COLOR_BYTESIZE_1
#endif
#ifdef GuiConst_COLOR_DEPTH_5
  #define GuiLib_COLOR_BYTESIZE_1
#endif
#ifdef GuiConst_COLOR_DEPTH_8
  #define GuiLib_COLOR_BYTESIZE_1
#endif
#ifdef GuiConst_COLOR_DEPTH_12
  #define GuiLib_COLOR_BYTESIZE_2
#endif
#ifdef GuiConst_COLOR_DEPTH_15
  #define GuiLib_COLOR_BYTESIZE_2
#endif
#ifdef GuiConst_COLOR_DEPTH_16
  #define GuiLib_COLOR_BYTESIZE_2
#endif
#ifdef GuiConst_COLOR_DEPTH_18
  #define GuiLib_COLOR_BYTESIZE_3
#endif
#ifdef GuiConst_COLOR_DEPTH_24
  #define GuiLib_COLOR_BYTESIZE_3
#endif
#ifdef GuiConst_COLOR_DEPTH_32
  #define GuiLib_COLOR_BYTESIZE_3
#endif

#define GuiLib_NO_CURSOR                    -1

#define GuiLib_NO_RESET_AUTO_REDRAW         0
#define GuiLib_RESET_AUTO_REDRAW            1

#define GuiLib_ALIGN_NOCHANGE               0
#define GuiLib_ALIGN_LEFT                   1
#define GuiLib_ALIGN_CENTER                 2
#define GuiLib_ALIGN_RIGHT                  3

#define GuiLib_PS_NOCHANGE                  0
#define GuiLib_PS_OFF                       1
#define GuiLib_PS_ON                        2
#define GuiLib_PS_NUM                       3

#define GuiLib_TRANSPARENT_OFF              0
#define GuiLib_TRANSPARENT_ON               1

#define GuiLib_UNDERLINE_OFF                0
#define GuiLib_UNDERLINE_ON                 1

#define GuiLib_BBP_NONE                     0
#define GuiLib_BBP_LEFT                     1
#define GuiLib_BBP_RIGHT                    2
#define GuiLib_BBP_TOP                      4
#define GuiLib_BBP_BOTTOM                   8

#define GuiLib_VAR_BOOL                     0
#define GuiLib_VAR_UNSIGNED_CHAR            1
#define GuiLib_VAR_SIGNED_CHAR              2
#define GuiLib_VAR_UNSIGNED_INT             3
#define GuiLib_VAR_SIGNED_INT               4
#define GuiLib_VAR_UNSIGNED_LONG            5
#define GuiLib_VAR_SIGNED_LONG              6
#define GuiLib_VAR_FLOAT                    7
#define GuiLib_VAR_DOUBLE                   8
#define GuiLib_VAR_STRING                   9
#define GuiLib_VAR_COLOR                    10
#define GuiLib_VAR_NONE                     255

#define GuiLib_FORMAT_DEC                   0
#define GuiLib_FORMAT_EXP                   1
#define GuiLib_FORMAT_HEX_POSTFIX_H         2
#define GuiLib_FORMAT_TIME_MMSS             3
#define GuiLib_FORMAT_TIME_HHMM_24          4
#define GuiLib_FORMAT_TIME_HHMMSS_24        5
#define GuiLib_FORMAT_TIME_HHMM_12_ampm     6
#define GuiLib_FORMAT_TIME_HHMMSS_12_ampm   7
#define GuiLib_FORMAT_TIME_HHMM_12_AMPM     8
#define GuiLib_FORMAT_TIME_HHMMSS_12_AMPM   9
#define GuiLib_FORMAT_HEX_PREFIX_0X         10
#define GuiLib_FORMAT_HEX_CLEAN             11

#define GuiLib_FORMAT_ALIGNMENT_LEFT        0
#define GuiLib_FORMAT_ALIGNMENT_CENTER      1
#define GuiLib_FORMAT_ALIGNMENT_RIGHT       2

#define GuiLib_DECIMAL_PERIOD               0
#define GuiLib_DECIMAL_COMMA                1

#define GuiLib_LANGUAGE_TEXTDIR_LEFTTORIGHT 0
#define GuiLib_LANGUAGE_TEXTDIR_RIGHTTOLEFT 1

#define GuiLib_TEXTBOX_SCROLL_ILLEGAL_NDX   0
#define GuiLib_TEXTBOX_SCROLL_INSIDE_BLOCK  1
#define GuiLib_TEXTBOX_SCROLL_AT_HOME       2
#define GuiLib_TEXTBOX_SCROLL_AT_END        3
#define GuiLib_TEXTBOX_SCROLL_ABOVE_HOME    4
#define GuiLib_TEXTBOX_SCROLL_BELOW_END     5

#define GuiLib_BUTTON_STATE_UP              0
#define GuiLib_BUTTON_STATE_DOWN            1
#define GuiLib_BUTTON_STATE_DISABLED        2

#define GuiLib_CHECKBOX_OFF                 0
#define GuiLib_CHECKBOX_ON                  1

#define GuiLib_RADIOBUTTON_OFF             -1


#define GuiLib_NO_COLOR                     0x10000000


// =============================================================================
typedef struct
{
  GuiConst_INT16S ByteBegin;
  GuiConst_INT16S ByteEnd;
}
GuiLib_DisplayLineRec;

#ifdef GuiConst_CODEVISION_COMPILER
typedef flash struct
#else
typedef struct
#endif
{
#ifdef GuiConst_CHARMODE_ANSI
  GuiConst_INT8U FirstChar;
  GuiConst_INT8U LastChar;
  GuiConst_INT16U FirstCharNdx;
  GuiConst_INT16U IllegalCharNdx;
#else
  GuiConst_INT16U CharCount;
  GuiConst_INT32U FirstCharNdx;
#endif
  GuiConst_INT8U XSize, YSize;
#ifdef GuiConst_ADV_FONTS_ON
  GuiConst_INT8U ColorDepth;
#endif
  GuiConst_INT8U TopLine, MidLine, BaseLine;
  GuiConst_INT8U Cursor1, Cursor2;
  GuiConst_INT8U Underline1, Underline2;
  GuiConst_INT8U BoxWidth;
  GuiConst_INT8U PsNumWidth;
  GuiConst_INT8U PsSpace;
  GuiConst_INT8U LineSize;
}
GuiLib_FontRec;

#ifdef GuiConst_ICC_COMPILER
typedef GuiLib_FontRec PrefixRom *GuiLib_FontRecPtr;
typedef const GuiLib_FontRec * GuiLib_FontRecConstPtr;
#else
#ifdef GuiConst_CODEVISION_COMPILER
typedef flash GuiLib_FontRec *GuiLib_FontRecPtr;
typedef flash GuiLib_FontRec *GuiLib_FontRecConstPtr;
#else
typedef GuiLib_FontRec PrefixRom *GuiLib_FontRecPtr;
typedef const GuiLib_FontRec PrefixRom *GuiLib_FontRecConstPtr;
#endif
#endif

#ifdef GuiConst_ITEM_GRAPH_INUSE
typedef struct
{
  GuiConst_INT32S X;
  GuiConst_INT32S Y;
} GuiLib_GraphDataPoint;
#endif

// =============================================================================
#ifdef GuiConst_DISPLAY_BUFFER_EASYGUI
#ifdef GuiConst_COLOR_DEPTH_1
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#ifdef GuiConst_COLOR_DEPTH_2
#ifdef GuiConst_COLOR_PLANES_2
extern GuiConst_INT8U
  GuiLib_DisplayBuf[2][GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#else
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#endif
#ifdef GuiConst_COLOR_DEPTH_4
#ifdef GuiConst_COLOR_PLANES_4
extern GuiConst_INT8U
  GuiLib_DisplayBuf[4][GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#else
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#endif
#ifdef GuiConst_COLOR_DEPTH_5
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#ifdef GuiConst_COLOR_DEPTH_8
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#ifdef GuiConst_COLOR_DEPTH_12
typedef union
{
  GuiConst_INT8U  Bytes[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
  GuiConst_INT16U Words[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE / 2];
} DisplayBufUnion;
extern DisplayBufUnion GuiLib_DisplayBuf;
#endif
#ifdef GuiConst_COLOR_DEPTH_15
typedef union
{
  GuiConst_INT8U  Bytes[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
  GuiConst_INT16U Words[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE / 2];
} DisplayBufUnion;
extern DisplayBufUnion GuiLib_DisplayBuf;
#endif
#ifdef GuiConst_COLOR_DEPTH_16
typedef union
{
  GuiConst_INT8U  Bytes[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
  GuiConst_INT16U Words[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE / 2];
} DisplayBufUnion;
extern DisplayBufUnion GuiLib_DisplayBuf;
#endif
#ifdef GuiConst_COLOR_DEPTH_18
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#ifdef GuiConst_COLOR_DEPTH_24
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#ifdef GuiConst_COLOR_DEPTH_32
extern GuiConst_INT8U
  GuiLib_DisplayBuf[GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];
#endif
#endif

extern GuiLib_DisplayLineRec GuiLib_DisplayRepaint[GuiConst_BYTE_LINES];
#ifdef GuiConst_VNC_REMOTE_SUPPORT_ON
extern GuiLib_DisplayLineRec GuiLib_VncRepaint[GuiConst_BYTE_LINES];
#endif // GuiConst_VNC_REMOTE_SUPPORT_ON
#ifdef GuiConst_CURSOR_SUPPORT_ON
extern GuiConst_INT16S GuiLib_ActiveCursorFieldNo;
#endif
extern GuiConst_INT16S GuiLib_LanguageIndex;
extern GuiConst_INT16S GuiLib_CurStructureNdx;
#ifdef GuiConst_ALLOW_UPSIDEDOWN_AT_RUNTIME
extern GuiConst_INT8U GuiLib_DisplayUpsideDown;
#endif

#ifdef GuiConst_REMOTE_DATA
extern void (*GuiLib_RemoteDataReadBlock) (
   GuiConst_INT32U SourceOffset,
   GuiConst_INT32U SourceSize,
   GuiConst_INT8U * TargetAddr);
#ifdef GuiConst_REMOTE_TEXT_DATA
extern void (*GuiLib_RemoteTextReadBlock) (
   GuiConst_INT32U SourceOffset,
   GuiConst_INT32U SourceSize,
   void * TargetAddr);
#endif
#endif


// =============================================================================
#include "GuiFont.h"
#include "GuiStruct.h"
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DegToRad
// ===============
//
// Converts angle from degrees to radians.
//
// Input:
// ------
// Angle in degrees * 10 (1� = 10)
//
// Output:
// -------
// Angle in radians * 4096 (1 rad = 4096)
//
extern GuiConst_INT32S GuiLib_DegToRad(
   GuiConst_INT32S Angle);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_RadToDeg
// ===============
//
// Converts angle from radians to degrees.
//
// Input:
// ------
// Angle in radians * 4096 (1 rad = 4096)
//
// Output:
// -------
// Angle in degrees * 10 (1� = 10)
//
extern GuiConst_INT32S GuiLib_RadToDeg(
   GuiConst_INT32S Angle);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_SinRad
// =============
//
// Calculates Sin(angle), where angle is in radians (factored).
//
// Input:
// ------
// Angle in radians * 4096 (1 rad = 4096)
//
// Output:
// -------
// Sine of angle in 1/4096 units
//
extern GuiConst_INT32S GuiLib_SinRad(
   GuiConst_INT32S Angle);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_SinDeg
// =============
//
// Calculates Sin(angle), where angle is in degrees (factored).
//
// Input:
// ------
// Angle in degrees * 10 (1� = 10)
//
// Output:
// -------
// Sine of angle in 1/4096 units
//
extern GuiConst_INT32S GuiLib_SinDeg(
   GuiConst_INT32S Angle);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_CosRad
// =============
//
// Calculates Cos(angle), where angle is in radians (factored).
//
// Input:
// ------
// Angle in radians * 4096 (1 rad = 4096)
//
// Output:
// -------
// Cosine of angle in 1/4096 units
//
extern GuiConst_INT32S GuiLib_CosRad(
   GuiConst_INT32S Angle);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_CosDeg
// =============
//
// Calculates Cos(angle), where angle is in degrees (factored).
//
// Input:
// ------
// Angle in degrees * 10 (1� = 10)
//
// Output:
// -------
// Cosine of angle in 1/4096 units
//
extern GuiConst_INT32S GuiLib_CosDeg(
   GuiConst_INT32S Angle);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Sqrt
// ===========
//
// Returns Square root of argument.
//
// Input:
// ------
// Argument
//
// Output:
// -------
// Square root of argument
//
extern GuiConst_INT32U GuiLib_Sqrt(
   GuiConst_INT32U X);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetRedRgbColor
// =====================
//
// Extracts red component from RGB color.
//
// Input:
// ------
// 24 bit RGB color
//
// Output:
// -------
// 8 bit red color component
//
extern GuiConst_INT8U GuiLib_GetRedRgbColor(
   GuiConst_INT32U RgbColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_SetRedRgbColor
// =====================
//
// Sets red component from RGB color.
//
// Input:
// ------
// 24 bit RGB color
// 8 bit red color component
//
// Output:
// -------
// 24 bit RGB color
//
extern GuiConst_INT32U GuiLib_SetRedRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U RedColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetGreenRgbColor
// =======================
//
// Extracts green component from RGB color.
//
// Input:
// ------
// 24 bit RGB color
//
// Output:
// -------
// 8 bit green color component
//
extern GuiConst_INT8U GuiLib_GetGreenRgbColor(
   GuiConst_INT32U RgbColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_SetGreenRgbColor
// =======================
//
// Sets green component from RGB color.
//
// Input:
// ------
// 24 bit RGB color
// 8 bit green color component
//
// Output:
// -------
// 24 bit RGB color
//
extern GuiConst_INT32U GuiLib_SetGreenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U GreenColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetBlueRgbColor
// ======================
//
// Extracts blue component from RGB color.
//
// Input:
// ------
// 24 bit RGB color
//
// Output:
// -------
// 8 bit blue color component
//
extern GuiConst_INT8U GuiLib_GetBlueRgbColor(
   GuiConst_INT32U RgbColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_SetBlueRgbColor
// ======================
//
// Sets blue component from RGB color.
//
// Input:
// ------
// 24 bit RGB color
// 8 bit blue color component
//
// Output:
// -------
// 24 bit RGB color
//
extern GuiConst_INT32U GuiLib_SetBlueRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT8U BlueColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_RgbToPixelColor
// ======================
//
// Translates from RGB color to proper pixel value for display controller
// color setup.
//
// Input:
// ------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_RgbToPixelColor(
   GuiConst_INT32U RgbColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Rgb888ToPixelColor
// =========================
//
// Translates from RGB color components to proper pixel value for display
// controller color setup.
//
// Input:
// ------
// Red color value
// Green color value
// Blue color value
//
// Output:
// -------
// Encoded pixel color value
//
GuiConst_INTCOLOR GuiLib_Rgb888ToPixelColor(
   GuiConst_INT8U Red, GuiConst_INT8U Green, GuiConst_INT8U Blue);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_PixelToRgbColor
// ======================
//
// Translates from pixel value for display controller color setup to RGB color.
//
// Input:
// ------
// Encoded pixel color value
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_PixelToRgbColor(
   GuiConst_INTCOLOR PixelColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_RgbColorToGrayScale
// ==========================
//
// Translates from RGB color to 0~255 gray scale value.
//
// Input:
// ------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
// Output:
// -------
// Gray scale value, 0~255
//
extern GuiConst_INT8U GuiLib_RgbColorToGrayScale(
   GuiConst_INT32U RgbColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Rgb888ColorToGrayScale
// =============================
//
// Translates from RGB color components to a 0~255 grayscale value.
//
// Input:
// ------
// Red color value
// Green color value
// Blue color value
//
// Output:
// -------
// Gray scale value, 0~255
//
GuiConst_INT8U GuiLib_Rgb888ColorToGrayScale(
   GuiConst_INT8U Red, GuiConst_INT8U Green, GuiConst_INT8U Blue);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GrayScaleToRgbColor
// ==========================
//
// Translates from 0~255 gray scale value to RGB color.
//
// Input:
// ------
// Gray scale value, 0~255
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_GrayScaleToRgbColor(
   GuiConst_INT8U GrayValue);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_PixelColorToGrayScale
// ============================
//
// Translates from pixel value for display controller color setup to 0~255 gray
// scale value.
//
// Input:
// ------
// Encoded pixel color value
//
// Output:
// -------
// Gray scale value, 0~255
//
extern GuiConst_INT8U GuiLib_PixelColorToGrayScale(
   GuiConst_INTCOLOR PixelColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GrayScaleToPixelColor
// ============================
//
// Translates from gray scale value to display controller color setup.
//
// Input:
// ------
// Gray scale value, 0~255
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_GrayScaleToPixelColor(
   GuiConst_INT8U GrayValue);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_BrightenRgbColor
// =======================
//
// Brightens RGB color 0~1000�, 1000� results in pure white.
//
// Input:
// ------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
// Brighten value, 0 ~ 1000�
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_BrightenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_BrightenPixelColor
// =========================
//
// Brightens color in display controller color setup 0~1000�, 1000� results in
// pure white.
//
// Input:
// ------
// Encoded pixel color value
// Brighten value, 0 ~ 1000�
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_BrightenPixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DarkenRgbColor
// =====================
//
// Darkens RGB color 0~1000�, 1000� results in pure black.
//
// Input:
// ------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
// Darken value, 0 ~ 1000�
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_DarkenRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DarkenPixelColor
// =======================
//
// Darkens color in display controller color setup 0~1000�, 1000� results in
// pure black.
//
// Input:
// ------
// Encoded pixel color value
// Darken value, 0 ~ 1000�
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_DarkenPixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_MiddleRgbColor
// =====================
//
// Gives middle color, between color 1 and color 2.
// Ratio in 0~1000�, 0� gives color 1, 1000� gives color 2.
//
// Input:
// ------
// RGB color 1 value
// RGB color 2 value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
// Color ratio, 0 ~ 1000�
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_MiddleRgbColor(
   GuiConst_INT32U RgbColor1,
   GuiConst_INT32U RgbColor2,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_MiddlePixelColor
// =======================
//
// Gives middle color in display controller color setup,
// between color 1 and color 2.
// Ratio in 0~1000�, 0� gives color 1, 1000� gives color 2.
//
// Input:
// ------
// Encoded pixel color 1 value
// Encoded pixel color 2 value
// Color ratio, 0 ~ 1000�
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_MiddlePixelColor(
   GuiConst_INTCOLOR PixelColor1,
   GuiConst_INTCOLOR PixelColor2,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DesaturateRgbColor
// =========================
//
// Removes saturation from color, turning it towards gray color.
// Ratio in 0~1000�, 0� gives color 1, 1000� gives gray.
//
// Input:
// ------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
// Desaturation ratio, 0 ~ 1000�
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_DesaturateRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DesaturatePixelColor
// ===========================
//
// Removes saturation from color, turning it towards gray color.
// Ratio in 0~1000�, 0� gives color 1, 1000� gives gray.
//
// Input:
// ------
// Encoded pixel color value
// Color ratio, 0 ~ 1000�
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_DesaturatePixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_AccentuateRgbColor
// =========================
//
// Accentuates RGB color. Colors with less than 50% mean gray are made darker,
// otherwise brighter. Amount in �, 0� gives no change, 1000� gives pure black
// or white.
//
// Input:
// ------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
// Accentuate value, 0 ~ 1000�
//
// Output:
// -------
// RGB color value
// (32 bit, 24 bits used, low byte = Red, middle byte = Green, high byte = Blue)
//
extern GuiConst_INT32U GuiLib_AccentuateRgbColor(
   GuiConst_INT32U RgbColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_AccentuatePixelColor
// ===========================
//
// Accentuates color in display controller color setup. Colors with less than
// 50% mean gray are made darker, otherwise brighter. Amount in �, 0� gives no
// change, 1000� gives pure black or white.
//
// Input:
// ------
// Encoded pixel color value
// Accentuate value, 0 ~ 1000�
//
// Output:
// -------
// Encoded pixel color value
//
extern GuiConst_INTCOLOR GuiLib_AccentuatePixelColor(
   GuiConst_INTCOLOR PixelColor,
   GuiConst_INT16U Amount);
// -----------------------------------------------------------------------------

// =============================================================================
// GuiLib_SetButtonDisabledColor
// =============================
//
// Override automatic generation of button disabled color with the specified
// pixel color value.
//
// Input:
// ------
// Encoded pixel color value
//
// Output:
// -------
// Encoded pixel color value
//
GuiConst_INTCOLOR GuiLib_SetButtonDisabledColor(GuiConst_INTCOLOR PixelColor);
// -----------------------------------------------------------------------------

#ifdef GuiConst_CLIPPING_SUPPORT_ON

// =============================================================================
// GuiLib_ResetClipping
// ====================
//
// Resets clipping.
// Drawing can be limited to a rectangular portion of the screen,
// this routine resets the clipping limits to the entire screen.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_ResetClipping(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_SetClipping
// ==================
//
// Sets clipping.
// Drawing can be limited to a rectangular portion of the screen,
// this routine sets the clipping limits expressed as two opposite
// corner coordinates.
//
// Input:
// ------
// Rectangle coordinates
//
// Output:
// -------
// None
//
extern void GuiLib_SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2);
// -----------------------------------------------------------------------------

#endif


// =============================================================================
// GuiLib_ResetDisplayRepaint
// ==========================
//
// Resets all repainting scan line markers.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_ResetDisplayRepaint(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_MarkDisplayBoxRepaint
// ============================
//
// Sets the repainting scan line markers, indicating that all pixels inside the
// specified rectangle must be repainted.
//
// Input:
// ------
// Rectangle coordinates
//
// Output:
// -------
// None
//
extern void GuiLib_MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ClearDisplay
// ===================
//
// Clears the screen.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_ClearDisplay(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Dot
// ==========
//
// Draws a single pixel.
//
// Input:
// ------
// Coordinates
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_Dot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetDot
// =============
//
// Returns the color of a single pixel.
//
// Input:
// ------
// Coordinates
//
// Output:
// -------
// Color
//
extern GuiConst_INTCOLOR GuiLib_GetDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Line
// ===========
//
// Draws a line.
//
// Input:
// ------
// Coordinates
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_Line(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_LinePattern
// ==================
//
// Draws a patterned line.
//
// Input:
// ------
// Coordinates
// Line pattern (8 pixel pattern)
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_LinePattern(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT8U LinePattern,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_HLine
// ============
//
// Draws a horizontal line.
//
// Input:
// ------
// Coordinates
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_HLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_VLine
// ============
//
// Draws a vertical line.
//
// Input:
// ------
// Coordinates
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_VLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Box
// ==========
//
// Draws a single pixel wide rectangle.
//
// Input:
// ------
// Coordinates
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_Box(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_FillBox
// ==============
//
// Draws a filled rectangle.
//
// Input:
// ------
// Coordinates
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_FillBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_BorderBox
// ================
//
// Draws a filled rectangle with single pixel border.
//
// Input:
// ------
// Coordinates
// Border and fill colors
//
// Output:
// -------
// None
//
extern void GuiLib_BorderBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR BorderColor,
   GuiConst_INTCOLOR FillColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_InvertBox
// ================
//
// Inverts a block.
//
// Input:
// ------
// Coordinates
//
// Output:
// -------
// None
//
extern void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Circle
// =============
//
// Draws a filled or framed circle with single pixel width border.
//
// Input:
// ------
// Center coordinate
// Radius
// Border color, GuiLib_NO_COLOR means same color as fill color
// Fill color, GuiLib_NO_COLOR means no filling
//
// Output:
// -------
// None
//
extern void GuiLib_Circle(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Ellipse
// ==============
//
// Draws a filled or framed ellipse with single pixel width border.
//
// Input:
// ------
// Center coordinate
// Horizontal and vertical radii
// Border color, GuiLib_NO_COLOR means same color as fill color
// Fill color, GuiLib_NO_COLOR means no filling
//
// Output:
// -------
// None
//
extern void GuiLib_Ellipse(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U Radius1,
   GuiConst_INT16U Radius2,
   GuiConst_INT32S BorderColor,
   GuiConst_INT32S FillColor);
// -----------------------------------------------------------------------------


#ifdef GuiConst_BITMAP_SUPPORT_ON

// =============================================================================
// GuiLib_ShowBitmap
// =================
//
// Displays a stored bitmap.
//
// Input:
// ------
// Bitmap index in GuiStruct_BitmapPtrList
// Coordinates for upper left corner
// Transparent background color, -1 means no transparency
//
// Output:
// -------
// None
//
extern void GuiLib_ShowBitmap(
   GuiConst_INT16U BitmapIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT32S TranspColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ShowBitmapAt
// ===================
//
// Displays a bitmap at a specific address.
// The data in the memory area must conform to the format explained in the
// easyGUI user manual.
//
// Input:
// ------
// Pointer to memory area
// Coordinates for upper left corner
// Transparent background color, -1 means no transparency
//
// Output:
// -------
// None
//
extern void GuiLib_ShowBitmapAt(
   GuiConst_INT8U * BitmapPtr,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT32S TranspColor);
// -----------------------------------------------------------------------------


// ============================================================================
// GuiLib_ShowBitmapArea
// =====================
//
// Displays part of a stored bitmap.
//
// Input:
// ------
// Bitmap index in GuiStruct_BitmapPtrList
// Starting coordinates for upper left corner of bitmap
// Absolute coordinates for upper left and lower right corner of displayed area
// Transparent background color, -1 means no transparency
//
// Output:
// -------
// None
//
extern void GuiLib_ShowBitmapArea(
   GuiConst_INT16U BitmapIndex,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S AX1,
   GuiConst_INT16S AY1,
   GuiConst_INT16S AX2,
   GuiConst_INT16S AY2,
   GuiConst_INT32S TranspColor);
// -----------------------------------------------------------------------------


// ============================================================================
// GuiLib_ShowBitmapAreaAt
// =======================
//
// Displays part of a bitmap at a specific address.
// The data in the memory area must conform to the format explained in the
// easyGUI user manual.
//
// Input:
// ------
// Pointer to memory area
// Starting coordinates for upper left corner of bitmap
// Absolute coordinates for upper left and lower right corner of displayed area
// Transparent background color, -1 means no transparency
//
// Output:
// -------
// None
//
extern void GuiLib_ShowBitmapAreaAt(
   GuiConst_INT8U * BitmapPtr,
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S AX1,
   GuiConst_INT16S AY1,
   GuiConst_INT16S AX2,
   GuiConst_INT16S AY2,
   GuiConst_INT32S TranspColor);
// -----------------------------------------------------------------------------

#endif


// =============================================================================
// GuiLib_DrawChar
// ===============
//
// Draws a single character on the display.
//
// Input:
// ------
// Coordinates
// Font index
// Character
// Color
//
// Output:
// -------
// None
//
extern void GuiLib_DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   GuiConst_TEXT Character,
   GuiConst_INTCOLOR Color);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DrawStr
// ==============
//
// Draws a text string on the display.
//
// Input:
// ------
// Coordinates
// Font index
// Text string
// Alignment
// Ps mode
// Transparent
// Underlining
// Back box size X (0 = none)
// Back box size Y1 (height above base line, 0 = default font box)
// Back box size Y2 (height below base line, 0 = default font box)
// Extra back box pixels
// Text color
// Background box color
//
// Output:
// -------
// None
//
extern void GuiLib_DrawStr(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   GuiConst_TEXT *String,
   GuiConst_INT8U Alignment,
   GuiConst_INT8U PsWriting,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U Underlining,
   GuiConst_INT16S BackBoxSizeX,
   GuiConst_INT16S BackBoxSizeY1,
   GuiConst_INT16S BackBoxSizeY2,
   GuiConst_INT8U BackBorderPixels,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_DrawVar
// ==============
//
// Draws a variable on the display.
//
// Input:
// ------
// Coordinates
// Font index
// Variable pointer reference
// Variable type - can be:
//                   GuiLib_VAR_BOOL
//                   GuiLib_VAR_UNSIGNED_CHAR
//                   GuiLib_VAR_SIGNED_CHAR
//                   GuiLib_VAR_UNSIGNED_INT
//                   GuiLib_VAR_SIGNED_INT
//                   GuiLib_VAR_UNSIGNED_LONG
//                   GuiLib_VAR_SIGNED_LONG
//                   GuiLib_VAR_FLOAT
//                   GuiLib_VAR_DOUBLE
//                   GuiLib_VAR_STRING
// Variable format - can be:
//                   GuiLib_FORMAT_DEC
//                   GuiLib_FORMAT_EXP
//                   GuiLib_FORMAT_HEX
//                   GuiLib_FORMAT_TIME_MMSS
//                   GuiLib_FORMAT_TIME_HHMM_24
//                   GuiLib_FORMAT_TIME_HHMMSS_24
//                   GuiLib_FORMAT_TIME_HHMM_12_ampm
//                   GuiLib_FORMAT_TIME_HHMMSS_12_ampm
//                   GuiLib_FORMAT_TIME_HHMM_12_AMPM
//                   GuiLib_FORMAT_TIME_HHMMSS_12_AMPM
// Variable format field width
// Variable format alignment - can be:
//                   GuiLib_FORMAT_ALIGNMENT_LEFT
//                   GuiLib_FORMAT_ALIGNMENT_CENTER
//                   GuiLib_FORMAT_ALIGNMENT_RIGHT
// Variable format decimals
// Variable format show sign (true or false)
// Variable format zero padding (true or false)
// Variable format trailing zeros (true or false)
// Variable format thousands separator (true or false)
// Alignment
// Ps mode
// Transparent
// Underlining
// Back box size X (0 = none)
// Back box size Y1 (height above base line, 0 = default font box)
// Back box size Y2 (height below base line, 0 = default font box)
// Extra back box pixels
// Text color
// Background box color
//
// Output:
// -------
// None
//
extern void GuiLib_DrawVar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16U FontNo,
   void *VarPtr,
   GuiConst_INT8U VarType,
   GuiConst_INT8U FormatterFormat,
   GuiConst_INT8U FormatterFieldWidth,
   GuiConst_INT8U FormatterAlignment,
   GuiConst_INT8U FormatterDecimals,
   GuiConst_INT8U FormatterShowSign,
   GuiConst_INT8U FormatterZeroPadding,
   GuiConst_INT8U FormatterTrailingZeros,
   GuiConst_INT8U FormatterThousandsSeparator,
   GuiConst_INT8U Alignment,
   GuiConst_INT8U PsWriting,
   GuiConst_INT8U Transparent,
   GuiConst_INT8U Underlining,
   GuiConst_INT16S BackBoxSizeX,
   GuiConst_INT16S BackBoxSizeY1,
   GuiConst_INT16S BackBoxSizeY2,
   GuiConst_INT8U BackBorderPixels,
   GuiConst_INTCOLOR ForeColor,
   GuiConst_INTCOLOR BackColor);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Init
// ===========
//
// Initialises the easyGUI library module.
// MUST be called at application startup, before using the easyGUI library.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_Init(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Clear
// ============
//
// Clears the screen, and resets the easyGUI system.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_Clear(void);
// -----------------------------------------------------------------------------


#ifdef GuiConst_CHARMODE_UNICODE

// =============================================================================
// GuiLib_UnicodeStrLen
// ====================
//
// Calculates length of Unicode string.
// This function is equivalent to the ANSI character mode strlen function.
//
// Input:
// ------
// Unicode string reference
//
// Output:
// -------
// Length in characters, excluding zero termination
//
extern GuiConst_INT16U GuiLib_UnicodeStrLen(
   GuiConst_TEXT *S);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_StrAnsiToUnicode
// =======================
//
// Converts ANSI string to Unicode string.
//
// Input:
// ------
// ANSI and Unicode string references
// Unicode string must have sufficient space
//
// Output:
// -------
// None
//
extern void GuiLib_StrAnsiToUnicode(
   GuiConst_TEXT *S2,
   GuiConst_CHAR *S1);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_UnicodeStrCmp
// ====================
//
// Compares two Unicode strings.
// This function is equivalent to the ANSI character mode strcmp function.
//
// Input:
// ------
// Unicode string references
//
// Output:
// -------
// <0: S1 is less than S2
// =0: S1 and S2 are equal
// >0: S1 is greater than S2
//
GuiConst_INT16S GuiLib_UnicodeStrCmp(
   GuiConst_TEXT *S1,
   GuiConst_TEXT *S2);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_UnicodeStrNCmp
// =====================
//
// Compares two Unicode strings, until the N'th character.
// This function is equivalent to the ANSI character mode strncmp function.
//
// Input:
// ------
// Unicode string references
// Character count to compare
//
// Output:
// -------
// <0: S1 is less than S2 inside the N characters
// =0: S1 and S2 are equal inside the N characters
// >0: S1 is greater than S2 inside the N characters
//
GuiConst_INT16S GuiLib_UnicodeStrNCmp(
   GuiConst_TEXT *S1,
   GuiConst_TEXT *S2,
   GuiConst_INT16U StrLen);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_UnicodeStrCpy
// ====================
//
// Copy from one Unicode string to another.
// This function is equivalent to the ANSI character mode strcpy function
//
// Input:
// ------
// S1: Unicode source string reference
// S2: Unicode destination string reference
// Unicode destination string must have sufficient space
//
// Output:
// -------
// None
//
void GuiLib_UnicodeStrCpy(
   GuiConst_TEXT *S2,
   GuiConst_TEXT *S1);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_UnicodeStrNCpy
// =====================
//
// Copy from one Unicode string to another. No more than source StrLen bytes are
// copied.
// Bytes following a source null byte are not copied.
// If needed target is padded with null bytes until reaching StrLen.
// This function is equivalent to the ANSI character mode strncpy function.
//
// Input:
// ------
// S1: Unicode source string reference
// S2: Unicode destination string reference
// StrLen: Number of characters to copy
// Unicode destination string must have sufficient space
//
// Output:
// -------
// None
//
void GuiLib_UnicodeStrNCpy(
   GuiConst_TEXT *S2,
   GuiConst_TEXT *S1,
   GuiConst_INT16U StrLen);
// -----------------------------------------------------------------------------

#endif


// =============================================================================
// GuiLib_ShowScreen
// =================
//
// Instructs structure drawing task to draw a complete structure.
//
// Input:
// ------
// Structure ID
// Active cursor field No. -
//    use GuiLib_NO_CURSOR if there is no cursor to show
// Maintain or erase old AutoReadraw fields -
//    use GuiLib_NO_RESET_AUTO_REDRAW or GuiLib_RESET_AUTO_REDRAW
//
// Output:
// -------
// None
//
extern void GuiLib_ShowScreen(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16S CursorFieldToShow,
   GuiConst_INT8U ResetAutoRedraw);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetTextLanguagePtr
// =========================
//
// Returns pointer to text in structure
// Language of selected text can be freely selected, no matter what language
// is active
//
// Input:
// ------
// Structure ID
// Text No. - index 0 is first text in structure, Item types other than Texts
// and Paragraph are ignored
//
// Output:
// -------
// Pointer to text based on structure, text No. and current language
// Returns Nil if no text was found
//
extern GuiConst_TEXT *GuiLib_GetTextLanguagePtr(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo,
   GuiConst_INT16S LanguageIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetTextPtr
// =================
//
// Returns pointer to text in structure
//
// Input:
// ------
// Structure ID
// Text No. - index 0 is first text in the structure, item types other than
// Text and Paragraph are ignored
//
// Output:
// -------
// Pointer to text based on structure, text No. and current language
// Returns Nil if no text was found
//
extern GuiConst_TEXT *GuiLib_GetTextPtr(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetTextWidth
// ===================
//
// Returns width of text in pixels
//
// Input:
// ------
// Pointer to text string
// Pointer to font
// PS writing mode - can be:
//   GuiLib_PS_OFF   No proportional writing
//   GuiLib_PS_ON    Normal proportional writing
//   GuiLib_PS_NUM   Numerical proportional writing
//
// Output:
// -------
// Width of text in pixels, returns zero if an error is encountered
//
extern GuiConst_INT16U GuiLib_GetTextWidth(
   GuiConst_TEXT *String,
   GuiLib_FontRecConstPtr Font,
   GuiConst_INT8U PsWriting);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GetCharCode
// ==================
//
// Returns ANSI or Unicode code of specific character in string
//
// Input:
// ------
// Structure ID
// Text No. - index 0 is first text in the structure, item types other than
// Text and Paragraph are ignored
// Character position
// Omit control codes, Control codes are soft and hard line breaks
//   0 - count all charactes
//   1 - omit control codes during counting
//
// Output:
// -------
// ANSI or Unicode code of character, returns zero if an error is encountered
//
extern GuiConst_TEXT GuiLib_GetCharCode(
   const GuiConst_INT16U StructureNdx,
   GuiConst_INT16U TextNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_GuiLib_ClearPositionCallbacks
// ====================================
//
// Resets all position callback function references - no callbacks will then
// happen, until GuiLib_SetPositionCallbackFunc has been called
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_ClearPositionCallbacks(void);


// =============================================================================
// GuiLib_SetPositionCallbackFunc
// ==============================
//
// Defines a callback function for a specific position callback item
// The callback function will be called each time a structure containing a
// position callback item with the same index number. The callback will contain
// the index number and the current X,Y display coordinates.
//
// Input:
// ------
// Position callback index No.
// Position callback function - function style is F(IndexNo, X, Y)
//
// Output:
// -------
// None
//
extern void GuiLib_SetPositionCallbackFunc(
   GuiConst_INT16U IndexNo,
   void (*PosCallbackFunc) (GuiConst_INT16U IndexNo,
                            GuiConst_INT16S X,
                            GuiConst_INT16S Y));


#ifdef GuiConst_BLINK_SUPPORT_ON

// =============================================================================
// GuiLib_GetBlinkingCharCode
// ==========================
//
// Returns ANSI or Unicode code of character in string marked as blinking field
//
// Input:
// ------
// Blinking item No.
// Character position
// Omit control codes, Control codes are soft and hard line breaks
//   0 - count all charactes
//   1 - omit control codes during counting
//
// Output:
// -------
// ANSI or Unicode code of character, returns zero if an error is encountered
//
extern GuiConst_TEXT GuiLib_GetBlinkingCharCode(
   GuiConst_INT16U BlinkFieldNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16U OmitCtrlCode);
// -----------------------------------------------------------------------------

#endif


// =============================================================================
// GuiLib_SetLanguage
// ==================
//
// Selects current language. Index zero is the reference language.
//
// Input:
// ------
// Language index
//
// Output:
// -------
// None
//
extern void GuiLib_SetLanguage(
   GuiConst_INT16S NewLanguage);
// -----------------------------------------------------------------------------


#ifdef GuiConst_CURSOR_SUPPORT_ON

// =============================================================================
// GuiLib_Cursor_Hide
// ==================
//
// Hides cursor field.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_Cursor_Hide(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_IsCursorFieldInUse
// =========================
//
// Reports if a cursor field is active.
//
// Input:
// ------
// Query cursor field No.
//
// Output:
// -------
// 0 = Cursor field not active
// 1 = Cursor field active
//
extern GuiConst_INT8U GuiLib_IsCursorFieldInUse(
   GuiConst_INT16S AskCursorFieldNo);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Cursor_Select
// ====================
//
// Makes requested cursor field active, redrawing both current and new cursor
// field.
//
// Input:
// ------
// New cursor field No.
// -1 hides cursor.
//
// Output:
// -------
// 0 = No change, requested cursor field is invalid
// 1 = Cursor moved // extern GuiConst_INT8U GuiLib_Cursor_Select(
//  GuiConst_INT16S NewCursorFieldNo);
//
extern GuiConst_INT8U GuiLib_Cursor_Select(
   GuiConst_INT16S NewCursorFieldNo);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Cursor_Down
// ==================
//
// Makes next cursor field active, redrawing both current and new cursor field.
//
// Input:
// ------
// None
//
// Output:
// -------
// 0 = Cursor at end of range
// 1 = Cursor moved
//
extern GuiConst_INT8U GuiLib_Cursor_Down(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Cursor_Up
// ================
//
// Makes previous cursor field active, redrawing both current and new cursor
// field.
//
// Input:
// ------
// None
//
// Output:
// -------
// 0 = Cursor at end of range
// 1 = Cursor moved
//
extern GuiConst_INT8U GuiLib_Cursor_Up(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Cursor_Home
// ==================
//
// Makes first cursor field active, redrawing both current and new cursor
// field.
//
// Input:
// ------
// None
//
// Output:
// -------
// 0 = No change, first cursor field already active
// 1 = Cursor moved
//
extern GuiConst_INT8U GuiLib_Cursor_Home(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_Cursor_End
// =================
//
// Makes last cursor field active, redrawing both current and new cursor
// field.
//
// Input:
// ------
// None
//
// Output:
// -------
// 0 = No change, last cursor field already active
// 1 = Cursor moved
//
extern GuiConst_INT8U GuiLib_Cursor_End(void);
// -----------------------------------------------------------------------------

#endif


#ifdef GuiConst_BLINK_SUPPORT_ON

// =============================================================================
// GuiLib_BlinkBoxStart
// ====================
//
// Sets parameters for blinking box function
//
// Input:
// ------
// X1, Y1, X2, Y2: Blinking rectangle
// Rate: Blinking rate, in multiples of refresh rate, valid range 0-255
//       Rate = 255 disables blinking, but makes an initial inverting of the
//       rectangle
//
// Output:
// -------
// None
//
extern void GuiLib_BlinkBoxStart(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT16S Rate);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_BlinkBoxStop
// ===================
//
// Stops blinking box function
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_BlinkBoxStop(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_BlinkBoxMarkedItem
// =========================
//
// Sets parameters for blinking box function based on marked blink text field
//
// Input:
// ------
// BlinkFieldNo: Index No. to marked blink text field in structure
// CharNo: Character number in text, first character is No. 1
//         CharNo = 0 means entire text
// Rate: Blinking rate, in multiples of refresh rate, valid range 0-255
//       Rate = 255 disables blinking, but makes an initial marking of the
//       character/text
//
// Output:
// -------
// None
//
extern void GuiLib_BlinkBoxMarkedItem(
   GuiConst_INT16U BlinkFieldNo,
   GuiConst_INT16U CharNo,
   GuiConst_INT16S Rate);
// -----------------------------------------------------------------------------

// =============================================================================
// GuiLib_BlinkBoxMarkedItemStop
// =============================
//
// Stops blinking box function based on marked blink text field
//
// Input:
// ------
// BlinkFieldNo: Index No. to marked blink text field in structure
//
// Output:
// -------
// None
//
extern void GuiLib_BlinkBoxMarkedItemStop(
   GuiConst_INT16U BlinkFieldNo);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_BlinkBoxMarkedItemUpdate
// ===============================
//
// Update parameters for blinking box function based on marked blink text field
//
// Input:
// ------
// BlinkFieldNo: Index No. to marked blink text field in structure
//
// Output:
// -------
// None
//
extern void GuiLib_BlinkBoxMarkedItemUpdate(
   GuiConst_INT16U BlinkFieldNo);

//------------------------------------------------------------------------------

#endif


// =============================================================================
// GuiLib_InvertBoxStart
// =====================
//
// Sets parameters for inverted box function
//
// Input:
// ------
// X1, Y1, X2, Y2: Inverted box rectangle
//
// Output:
// -------
// None
//
extern void GuiLib_InvertBoxStart(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_InvertBoxStop
// ====================
//
// Stops inverted box function
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_InvertBoxStop(void);
// -----------------------------------------------------------------------------


#ifdef GuiConst_ITEM_TOUCHAREA_INUSE

// =============================================================================
// GuiLib_TouchCheck
// =================
//
// Returns touch area No. corresponding to the supplied coordinates.
// If no touch area is found at coordinates -1 is returned.
// Touch coordinates are converted to display coordinates, if conversion
// parameters have been set with the GuiLib_TouchAdjustSet function.
//
// Input:
// ------
// X, Y: Touch position in touch interface coordinates
//
// Output:
// -------
// -1  = No touch area found
// >=0 = Touch area No.
//
//
extern GuiConst_INT32S GuiLib_TouchCheck(
   GuiConst_INT16S X,
   GuiConst_INT16S Y);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TouchGet
// ===============
//
// Performs the same action as GuiLib_TouchCheck, and additionally returns
// touch coordinates in display coordinates:
// Returns touch area No. corresponding to the supplied coordinates.
// If no touch area is found at coordinates -1 is returned.
// Touch coordinates are converted to display coordinates, if conversion
// parameters have been set with the GuiLib_TouchAdjustSet function.
// OBS: If no conversion parameters have been set with the GuiLib_TouchAdjustSet
// function the calculated touch coordinates in display coordinates will be
// TouchX = X, and TouchY = Y.
//
// Input:
// ------
// X, Y: Touch position in touch interface coordinates
// TouchX, TouchY: Addresses of the variables where touch coordinates
//                 converted to display coordinates will be stored
//
// Output:
// -------
// -1  = No touch area found
// >=0 = Touch area No.
//
//
extern GuiConst_INT32S GuiLib_TouchGet(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S* TouchX,
   GuiConst_INT16S* TouchY);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TouchAdjustReset
// =======================
//
// Resets touch coordinate conversion.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_TouchAdjustReset(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TouchAdjustSet
// =====================
//
// Sets one coordinate pair for touch coordinate conversion.
// Must be called two times, once for each of two diagonally opposed corners,
// or four times, once for each of the corners.
// The corner positions should be as close as possible to the physical display
// corners, as precision is lowered when going towards the display center.
//
// Input:
// ------
// XTrue, YTrue        : Position represented in display coordinates
// XMeasured, YMeasured: Position represented in touch interface coordinates
//
// Output:
// -------
// None
//
extern void GuiLib_TouchAdjustSet(
   GuiConst_INT16S XTrue,
   GuiConst_INT16S YTrue,
   GuiConst_INT16S XMeasured,
   GuiConst_INT16S YMeasured);
// -----------------------------------------------------------------------------

#endif


// =============================================================================
// GuiLib_Refresh
// ==============
//
// Refreshes variables and updates display.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_Refresh(void);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TestPattern
// ==================
//
// Makes test pattern for display control error diagnosis. See user manual for
// further info.
//
// Input:
// ------
// None
//
// Output:
// -------
// None
//
extern void GuiLib_TestPattern(void);
// -----------------------------------------------------------------------------


#ifdef GuiConst_ITEM_SCROLLBOX_INUSE

// =============================================================================
// GuiLib_ScrollBox_Init
// =====================
//
// Initializes a scroll box (new item type style).
//
// Input:
// ------
// Scroll box index
// DataFuncPtr: Address of scroll line call-back function of type
//    "void F(GuiConst_INT16S LineIndex)"
// NoOfLines: Total No. of lines in scroll box
// ActiveLine: Active scroll line, -1 means no active scroll line
//
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_Init(
   GuiConst_INT8U ScrollBoxIndex,
   void (*DataFuncPtr) (GuiConst_INT16S LineIndex),
   GuiConst_INT16S NoOfLines,
   GuiConst_INT16S ActiveLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_Redraw
// =======================
//
// Redraws dynamic parts of scroll box (new item type style).
// For complete redraw use GuiLib_ScrollBox_Init.
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_Redraw(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_RedrawLine
// ===========================
//
// Redraws single line of scroll box (new item type style).
// If line is not visible no action is performed.
//
// Input:
// ------
// Scroll box index
// Scroll line to redraw, index zero is first line in scroll box list
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_RedrawLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_Close
// ======================
//
// Closes a scroll box (new item type style).
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_Close(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_Down
// =====================
//
// Makes next scroll line active, and scrolls list if needed (new item type
// style).
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// 0 = No change, list already at bottom
// 1 = Active scroll line changed
//
extern GuiConst_INT8U GuiLib_ScrollBox_Down(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_Up
// ===================
//
// Makes previous scroll line active, and scrolls list if needed (new item type
// style).
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// 0 = No change, list already at top
// 1 = Active scroll line changed
//
extern GuiConst_INT8U GuiLib_ScrollBox_Up(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_Home
// =====================
//
// Makes first scroll line active, and scrolls list if needed (new item type
// style).
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// 0 = No change, list already at top
// 1 = Active scroll line changed
//
extern GuiConst_INT8U GuiLib_ScrollBox_Home(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_End
// ====================
//
// Makes last scroll line active, and scrolls list if needed (new item type
// style).
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// 0 = No change, list already at bottom
// 1 = Active scroll line changed
//
extern GuiConst_INT8U GuiLib_ScrollBox_End(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_To_Line
// ========================
//
// Makes specified scroll line active, and scrolls list if needed (new item type
// style).
//
// Input:
// ------
// Scroll box index
// New scroll line, -1 means no active scroll line
//
// Output:
// -------
// 0 = No change, list already at specified line
// 1 = Active scroll line changed
//
extern GuiConst_INT8U GuiLib_ScrollBox_To_Line(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S NewLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_SetLineMarker
// ==============================
//
// Sets scroll marker position and line count.
// Scroll marker index 0 (active scroll line) can only cover 1 scroll line.
// Display only changes after subsequent GuiLib_ScrollBox_Redraw call.
//
// Input:
// ------
// Scroll box index
// Scroll marker index
// Marker start line, -1 means no marker
// Marker line count (clipped to a maximum of 1 for marker index 0)
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_SetLineMarker(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLineMarkerIndex,
   GuiConst_INT16S StartLine,
   GuiConst_INT16U Size);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_GetActiveLine
// ==============================
//
// Returns topmost scroll marker line of selected scroll box / marker.
//
// Input:
// ------
// Scroll box index
// Scroll marker index (0 = active scroll line, >0 = secondary line markers)
//
// Output:
// -------
// Index of topmost scroll marker line
// -1 = Error in parameters
//
extern GuiConst_INT16S GuiLib_ScrollBox_GetActiveLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLineMarkerIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_GetActiveLineCount
// ===================================
//
// Returns number of marked lines in selected scroll box / marker.
//
// Input:
// ------
// Scroll box index
// Scroll marker index (0 = active scroll line, >0 = secondary line markers)
//
// Output:
// -------
// Count of marked scroll lines
// -1 = Error in parameters
// Active scroll line (scroll marker index = 0) will always return 1
//
extern GuiConst_INT16S GuiLib_ScrollBox_GetActiveLineCount(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16U ScrollLineMarkerIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_SetIndicator
// =============================
//
// Sets scroll indicator position.
// Display only changes after subsequent GuiLib_ScrollBox_Redraw call.
//
// Input:
// ------
// Scroll box index
// Indicator line, -1 means no indicator
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_SetIndicator(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S StartLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_SetTopLine
// ===========================
//
// Sets topmost visible scroll line of selected scroll box.
// Display only changes after subsequent GuiLib_ScrollBox_Redraw call.
// If simultaneously setting line marker position make sure top line setting is
// last action before GuiLib_ScrollBox_Redraw call.
//
// Input:
// ------
// Scroll box index
// Index of topmost visible scroll line
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_ScrollBox_SetTopLine(
   GuiConst_INT8U ScrollBoxIndex,
   GuiConst_INT16S TopLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_ScrollBox_GetTopLine
// ===========================
//
// Returns topmost visible scroll line of selected scroll box.
//
// Input:
// ------
// Scroll box index
//
// Output:
// -------
// Index of topmost visible scroll line
// -1  = Error in parameters
//
extern GuiConst_INT16S GuiLib_ScrollBox_GetTopLine(
   GuiConst_INT8U ScrollBoxIndex);
// -----------------------------------------------------------------------------

#endif


#ifdef GuiConst_ITEM_GRAPH_INUSE

// =============================================================================
// GuiLib_Graph_Close
// ==================
//
// Closes a graph, so that no futher actions can be accomplished with it.
// Memory assigned to datasets must be freed independently.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
GuiConst_INT8U GuiLib_Graph_Close(
   GuiConst_INT8U GraphIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_AddDataSet
// =======================
//
// Adds a data set to a graph. The data set must be created in the structure,
// but is not shown before this function is called.
// Memory must be assigned independently for the data set.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
// XAxisIndex: Index of X-axis to use.
// YAxisIndex: Index of Y-axis to use.
// DataPtr: Pointer to an array of data points, where each data point is a 32
//    bit signed X,Y entity of type GuiLib_GraphDataPoint.
// DataSize: Maximum number of data points possible in the data set.
// DataCount: Number of active data points in the data set.
// DataFirst: Index of first active data point (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_AddDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   GuiConst_INT8U XAxisIndex,
   GuiConst_INT8U YAxisIndex,
   GuiLib_GraphDataPoint *DataPtr,
   GuiConst_INT16U DataSize,
   GuiConst_INT16U DataCount,
   GuiConst_INT16U DataFirst);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_RemoveDataSet
// ==========================
//
// Removes a data set from a graph.
// Memory must be released independently for the data set.
// Nothing is drawn in this function.
//
//------------------------------------------------------------------------------
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_RemoveDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_AddDataPoint
// =========================
//
// Adds a data point to a data set. The data set must have sufficient space for
// the new data point.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
// DataPointX, DataPointY: The data point.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_AddDataPoint(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   GuiConst_INT32S DataPointX,
   GuiConst_INT32S DataPointY);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_ShowDataSet
// ========================
//
// Marks a data set as visible.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_ShowDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_HideDataSet
// ========================
//
// Marks a data set as invisible.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_HideDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_ShowXAxis
// ======================
//
// Marks an X-axis as visible.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of X axis in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_ShowXAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_HideXAxis
// ======================
//
// Marks an X-axis as invisible.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of X axis in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_HideXAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_ShowYAxis
// ======================
//
// Marks an Y-axis as visible.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of Y axis in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_ShowYAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_HideYAxis
// ======================
//
// Marks an Y-axis as invisible.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of Y axis in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_HideYAxis(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_SetXAxisRange
// ==========================
//
// Changes an X-axis range.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of X axis in the graph (index is zero based).
// MinValue, MaxValue: Limits of the X axis range.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_SetXAxisRange(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT32S MinValue,
   GuiConst_INT32S MaxValue);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_SetYAxisRange
// ==========================
//
// Changes an Y-axis range.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of Y axis in the graph (index is zero based).
// MinValue, MaxValue: Limits of the Y axis range.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_SetYAxisRange(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U AxisIndex,
   GuiConst_INT32S MinValue,
   GuiConst_INT32S MaxValue);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_ResetXAxisOrigin
// =============================
//
// Resets the X-axis origin to the original origin.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of X axis in the graph (index is zero based). Index -1 will
//            reset all defined X axes.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_ResetXAxisOrigin(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8S AxisIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_OffsetXAxisOrigin
// ==============================
//
// Adjusts the X-axis origin. Useful for dynamic graphs with moving X-axis.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of X axis in the graph (index is zero based). Index -1 will
//            offset all defined X axes. This only makes sense if all X axes
//            have identical scaling.
// Offset: Amount of movement of the X axis origin.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_OffsetXAxisOrigin(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8S AxisIndex,
   GuiConst_INT32S Offset);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_ResetYAxisOrigin
// =============================
//
// Resets the Y-axis origin to the original origin.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of Y axis in the graph (index is zero based). Index -1 will
//            reset all defined Y axes.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_ResetYAxisOrigin(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8S AxisIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_OffsetYAxisOrigin
// ==============================
//
// Adjusts the Y-axis origin. Useful for dynamic graphs with moving Y-axis.
// Nothing is drawn in this function.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// AxisIndex: Index of Y axis in the graph (index is zero based). Index -1 will
//            offset all defined Y axes. This only makes sense if all Y axes
//            have identical scaling.
// Offset: Amount of movement of the Y axis origin.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_OffsetYAxisOrigin(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8S AxisIndex,
   GuiConst_INT32S Offset);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_Redraw
// ===================
//
// Redraws the complete graph, including background, axes, and data sets.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_Redraw(
   GuiConst_INT8U GraphIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_DrawAxes
// =====================
//
// Redraws the graph, including background and axes, but excluding data sets.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_DrawAxes(
   GuiConst_INT8U GraphIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_DrawDataSet
// ========================
//
// Redraws a data set.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_DrawDataSet(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex);
//------------------------------------------------------------------------------


// =============================================================================
// GuiLib_Graph_DrawDataPoint
// ==========================
//
// Draws a single data point in a data set.
//
// Input:
// ------
// GraphIndex: Index of graph (index is zero based).
// DataSetIndex: Index of data set in the graph (index is zero based).
// DataIndex: Index of point in the data set (index is zero based).
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_Graph_DrawDataPoint(
   GuiConst_INT8U GraphIndex,
   GuiConst_INT8U DataSetIndex,
   GuiConst_INT16U DataIndex);
// -----------------------------------------------------------------------------

#endif


#ifdef GuiConst_ITEM_GRAPHICS_LAYER_FILTER_INUSE

// =============================================================================
// GuiLib_GraphicsFilter_Init
// ==========================
//
// Initializes a graphics filter call-back function.
//
// Input:
// ------
// GraphicsFilterIndex: Index of graphics filter (index is zero based).
// FilterFuncPtr: Pointer to user-defined call-back filter function.
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
//------------------------------------------------------------------------------
extern GuiConst_INT8U GuiLib_GraphicsFilter_Init(
   GuiConst_INT8U GraphicsFilterIndex,
   void (*FilterFuncPtr)
     (GuiConst_INT8U *DestAddress,
      GuiConst_INT16U DestLineSize,
      GuiConst_INT8U *SourceAddress,
      GuiConst_INT16U SourceLineSize,
      GuiConst_INT16U Width,
      GuiConst_INT16U Height,
      GuiConst_INT32S FilterPars[10]));
#endif


// =============================================================================
// GuiLib_TextBox_Scroll_Up
// ========================
//
// Scrolls text box contents one text line up.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Up(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Down
// ==========================
//
// Scrolls text box contents one text line down.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Down(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Home
// ==========================
//
// Scrolls text box contents to the top.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Home(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_End
// =========================
//
// Scrolls text box contents to the bottom.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_End(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_To_Line
// =============================
//
// Scrolls text box contents to a specific text line.
//
// Input:
// ------
// Text box index
// Text line
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_To_Line(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT16S NewLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Up_Pixel
// ==============================
//
// Scrolls text box contents one pixel position up.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Up_Pixel(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Down_Pixel
// ================================
//
// Scrolls text box contents one pixel position down.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Down_Pixel(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Home_Pixel
// ================================
//
// Scrolls text box contents to the top.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Home_Pixel(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_End_Pixel
// ===============================
//
// Scrolls text box contents to the bottom.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_End_Pixel(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_To_PixelLine
// ==================================
//
// Scrolls text box contents to a specific pixel position.
//
// Input:
// ------
// Text box index
// Pixel line
//
// Output:
// -------
// 0 = Error in parameters
// 1 = Ok
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_To_PixelLine(
   GuiConst_INT8U TextBoxIndex,
   GuiConst_INT16S NewPixelLine);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Get_Pos
// =============================
//
// Returns status of topmost visible text line of text box.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// GuiLib_TEXTBOX_SCROLL_ILLEGAL_NDX   Illegal text box index
// GuiLib_TEXTBOX_SCROLL_INSIDE_BLOCK  Text box scrolled mid way
// GuiLib_TEXTBOX_SCROLL_AT_HOME       Text box scrolled to the top
// GuiLib_TEXTBOX_SCROLL_AT_END        Text box scrolled to the bottom
// GuiLib_TEXTBOX_SCROLL_ABOVE_HOME    Text box scrolled above the top
// GuiLib_TEXTBOX_SCROLL_BELOW_END     Text box scrolled below the bottom
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Get_Pos(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_Get_Pos_Pixel
// ===================================
//
// Returns status of topmost visible pixel position of text box.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// GuiLib_TEXTBOX_SCROLL_ILLEGAL_NDX   Illegal text box index
// GuiLib_TEXTBOX_SCROLL_INSIDE_BLOCK  Text box scrolled mid way
// GuiLib_TEXTBOX_SCROLL_AT_HOME       Text box scrolled to the top
// GuiLib_TEXTBOX_SCROLL_AT_END        Text box scrolled to the bottom
// GuiLib_TEXTBOX_SCROLL_ABOVE_HOME    Text box scrolled above the top
// GuiLib_TEXTBOX_SCROLL_BELOW_END     Text box scrolled below the bottom
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_Get_Pos_Pixel(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


// =============================================================================
// GuiLib_TextBox_Scroll_FitsInside
// ================================
//
// Determines if a text fits completely inside a text box without scrolling.
//
// Input:
// ------
// Text box index
//
// Output:
// -------
// 0 = No
// 1 = Yes
//
extern GuiConst_INT8U GuiLib_TextBox_Scroll_FitsInside(
   GuiConst_INT8U TextBoxIndex);
// -----------------------------------------------------------------------------


#ifdef GuiConst_REMOTE_DATA
// =============================================================================
// GuiLib_RemoteCheck
// ==================
//
// Checks if binary remote data file has correct ID.
//
// Input:
// ------
// None
//
// Output:
// -------
// 0 = Illegal ID
// 1 = ID accepted
//
extern GuiConst_INT8U GuiLib_RemoteCheck(void);
// -----------------------------------------------------------------------------
#endif



#ifdef GuiConst_CODEVISION_COMPILER
#pragma used-
#endif
// -----------------------------------------------------------------------------


#endif

