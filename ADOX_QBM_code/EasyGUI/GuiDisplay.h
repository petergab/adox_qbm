/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*                       easyGUI display driver unit                        */
/*                               v6.0.14.001                                */
/*                                                                          */
/* ************************************************************************ */

#ifndef __GUIDISPLAY_H
#define __GUIDISPLAY_H

#include "GuiConst.h"
#ifndef GuiConst_CODEVISION_COMPILER
#include "GuiLib.h"
#endif

extern void GuiDisplay_Lock (void);
extern void GuiDisplay_Unlock (void);
extern void GuiDisplay_Init (void);
extern void GuiDisplay_Refresh (void);

#endif
