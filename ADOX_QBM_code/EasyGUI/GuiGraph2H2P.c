/* ************************************************************************ */
/*                                                                          */
/*                     (C)2004-2013 IBIS Solutions ApS                      */
/*                            sales@easyGUI.com                             */
/*                             www.easyGUI.com                              */
/*                                                                          */
/*           2 bit (4 shade grayscale) graphics primitives library          */
/*                      Horizontal display bytes layout                     */
/*                              2 color planes                              */
/*                               v6.0.8.004                                 */
/*                                                                          */
/*     GuiLib.c include file - do NOT reference it in your linker setup     */
/*                                                                          */
/* ************************************************************************ */

//------------------------------------------------------------------------------
GuiConst_INT8U
   GuiLib_DisplayBuf[2][GuiConst_BYTE_LINES][GuiConst_BYTES_PR_LINE];

#ifdef GuiConst_CLIPPING_SUPPORT_ON
GuiConst_INT8U ClippingXPattern[GuiConst_BYTES_PR_LINE];
#endif
const GuiConst_INT8U LinePatternLeft[8] =
#ifdef GuiConst_BIT_TOPLEFT
   {0xFF, 0xFE, 0xFC, 0xF8, 0xF0, 0xE0, 0xC0, 0x80};
#else
   {0xFF, 0x7F, 0x3F, 0x1F, 0x0F, 0x07, 0x03, 0x01};
#endif
const GuiConst_INT8U LinePatternRight[8] =
#ifdef GuiConst_BIT_TOPLEFT
  {0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0xFF};
#else
  {0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF};
#endif
const GuiConst_INT8U LinePatternPixel[8] =
#ifdef GuiConst_BIT_TOPLEFT
  {0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF, 0xBF, 0x7F};
#else
  {0x7F, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0xFE};
#endif
const GuiConst_INT8U ColorPattern0[8][4] = {
#ifdef GuiConst_BIT_TOPLEFT
  {0x00, 0x00, 0x01, 0x01},
  {0x00, 0x00, 0x02, 0x02},
  {0x00, 0x00, 0x04, 0x04},
  {0x00, 0x00, 0x08, 0x08},
  {0x00, 0x00, 0x10, 0x10},
  {0x00, 0x00, 0x20, 0x20},
  {0x00, 0x00, 0x40, 0x40},
  {0x00, 0x00, 0x80, 0x80}
#else
  {0x00, 0x00, 0x80, 0x80},
  {0x00, 0x00, 0x40, 0x40},
  {0x00, 0x00, 0x20, 0x20},
  {0x00, 0x00, 0x10, 0x10},
  {0x00, 0x00, 0x08, 0x08},
  {0x00, 0x00, 0x04, 0x04},
  {0x00, 0x00, 0x02, 0x02},
  {0x00, 0x00, 0x01, 0x01}
#endif
  };
const GuiConst_INT8U ColorPattern1[8][4] = {
#ifdef GuiConst_BIT_TOPLEFT
  {0x00, 0x01, 0x00, 0x01},
  {0x00, 0x02, 0x00, 0x02},
  {0x00, 0x04, 0x00, 0x04},
  {0x00, 0x08, 0x00, 0x08},
  {0x00, 0x10, 0x00, 0x10},
  {0x00, 0x20, 0x00, 0x20},
  {0x00, 0x40, 0x00, 0x40},
  {0x00, 0x80, 0x00, 0x80}
#else
  {0x00, 0x80, 0x00, 0x80},
  {0x00, 0x40, 0x00, 0x40},
  {0x00, 0x20, 0x00, 0x20},
  {0x00, 0x10, 0x00, 0x10},
  {0x00, 0x08, 0x00, 0x08},
  {0x00, 0x04, 0x00, 0x04},
  {0x00, 0x02, 0x00, 0x02},
  {0x00, 0x01, 0x00, 0x01}
#endif
  };
GuiConst_INT8U ColorBitPattern0[4] = {0x00, 0x00, 0xFF, 0xFF};
GuiConst_INT8U ColorBitPattern1[4] = {0x00, 0xFF, 0x00, 0xFF};

#ifdef GuiConst_CLIPPING_SUPPORT_ON
static GuiConst_INT8U ClippingXOff;
#endif

//==============================================================================

#ifdef GuiConst_CLIPPING_SUPPORT_ON
//------------------------------------------------------------------------------
static void SetClipping(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S C;
  GuiConst_INT16S ClippingMinXb;
  GuiConst_INT16S ClippingMaxXb;

  if (X1 < 0)
    X1 = 0;
  if (Y1 < 0)
    Y1 = 0;
  if (X2 > sgl.CurLayerWidth - 1)
    X2 = sgl.CurLayerWidth - 1;
  if (Y2 > sgl.CurLayerHeight - 1)
    Y2 = sgl.CurLayerHeight - 1;

  sgl.ClippingX1 = X1;
  sgl.ClippingY1 = Y1;
  sgl.ClippingX2 = X2;
  sgl.ClippingY2 = Y2;
  ClippingXOff = ((X1 == 0) && (X2 == sgl.CurLayerWidth - 1));

  if (!ClippingXOff)
  {
    ClippingMinXb = sgl.ClippingX1 / 8;
    ClippingMaxXb = sgl.ClippingX2 / 8;

    for (C = 0; C < ClippingMinXb; C++)
      ClippingXPattern[C] = 0x00;
    ClippingXPattern[ClippingMinXb] = LinePatternLeft[sgl.ClippingX1 % 8];
    for (C = ClippingMinXb + 1; C < ClippingMaxXb; C++)
      ClippingXPattern[C] = 0xFF;
    ClippingXPattern[ClippingMaxXb] = LinePatternRight[sgl.ClippingX2 % 8];
    for (C = ClippingMaxXb + 1; C < GuiConst_BYTES_PR_LINE; C++)
      ClippingXPattern[C] = 0x00;
  }
}
#endif

//------------------------------------------------------------------------------
static void MarkDisplayBoxRepaint(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  if (!sgl.BaseLayerDrawing)
    return;

  X1 = X1 / 8;
  X2 = X2 / 8;

  while (Y1 <= Y2)
  {
    if ((GuiLib_DisplayRepaint[Y1].ByteEnd == -1) ||
        (X1 < GuiLib_DisplayRepaint[Y1].ByteBegin))
      GuiLib_DisplayRepaint[Y1].ByteBegin = X1;
    if (X2 > GuiLib_DisplayRepaint[Y1].ByteEnd)
      GuiLib_DisplayRepaint[Y1].ByteEnd = X2;

    Y1++;
  }
}

//------------------------------------------------------------------------------
static void ClearDisplay(void)
{
  memset(sgl.CurLayerBufPtr, 0xFF, sgl.CurLayerBytes);
}

//------------------------------------------------------------------------------
static void MakeDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT8U *PixelPtr;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X, &Y, &X, &Y))
#endif
  {
    PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 8);
    *PixelPtr =
       ColorPattern0[X % 8][Color] | (*PixelPtr & LinePatternPixel[X % 8]);
    PixelPtr += (GuiConst_DISPLAY_BYTES / 2);
    *PixelPtr =
       ColorPattern1[X % 8][Color] | (*PixelPtr & LinePatternPixel[X % 8]);

    MarkDisplayBoxRepaint(X, Y, X, Y);
  }
}

//------------------------------------------------------------------------------
static GuiConst_INTCOLOR ReadDot(
   GuiConst_INT16S X,
   GuiConst_INT16S Y)
{
  GuiConst_INT8U Pix;
  GuiConst_INT8U *PixelPtr;

  if ((X < 0) || (X >= sgl.CurLayerWidth) ||
      (Y < 0) || (Y >= sgl.CurLayerHeight))
    return (0);
  else
  {
    PixelPtr = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + (X / 8);
#ifdef GuiConst_BIT_TOPLEFT
    if (*PixelPtr && (0x01 << (X % 8)) == 0)
#else
    if (*PixelPtr && (0x80 >> (X % 8)) == 0)
#endif
      Pix = 0;
    else
      Pix = 1;
    PixelPtr += (GuiConst_DISPLAY_BYTES / 2);
#ifdef GuiConst_BIT_TOPLEFT
    if (*PixelPtr && (0x01 << (X % 8)) == 0)
#else
    if (*PixelPtr && (0x80 >> (X % 8)) == 0)
#endif
      return (Pix);
    else
      return (2+Pix);
  }
}

//------------------------------------------------------------------------------
static void HorzLine(
   GuiConst_INT16S X1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S X, X1b, X2b, X1m, X2m;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  X1b = X1 / 8;
  X2b = X2 / 8;
  X1m = X1 % 8;
  X2m = X2 % 8;
  PixelPtr1 = sgl.CurLayerBufPtr + Y * sgl.CurLayerLineSize + X1b;
  PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);

  if (X1b == X2b)
  {
    *PixelPtr1 = (*PixelPtr1 &
       ~(LinePatternLeft[X1m] & LinePatternRight[X2m])) |
       (ColorBitPattern0[Color] &
       (LinePatternLeft[X1m] & LinePatternRight[X2m]));
    *PixelPtr2 = (*PixelPtr2 &
       ~(LinePatternLeft[X1m] & LinePatternRight[X2m])) |
       (ColorBitPattern1[Color] &
       (LinePatternLeft[X1m] & LinePatternRight[X2m]));
  }
  else
  {
    *PixelPtr1 =
       (*PixelPtr1 & ~LinePatternLeft[X1m]) |
       (ColorBitPattern0[Color] & LinePatternLeft[X1m]);
    *PixelPtr2 =
       (*PixelPtr2 & ~LinePatternLeft[X1m]) |
       (ColorBitPattern1[Color] & LinePatternLeft[X1m]);
    PixelPtr1++;
    PixelPtr2++;

    for (X = X1b + 1; X < X2b; X++)
    {
      *PixelPtr1 = ColorBitPattern0[Color];
      *PixelPtr2 = ColorBitPattern1[Color];
      PixelPtr1++;
      PixelPtr2++;
    }

    *PixelPtr1 =
       (*PixelPtr1 & ~LinePatternRight[X2m]) |
       (ColorBitPattern0[Color] & LinePatternRight[X2m]);
    *PixelPtr2 =
       (*PixelPtr2 & ~LinePatternRight[X2m]) |
       (ColorBitPattern1[Color] & LinePatternRight[X2m]);
  }
}

//------------------------------------------------------------------------------
static void VertLine(
   GuiConst_INT16S X,
   GuiConst_INT16S Y1,
   GuiConst_INT16S Y2,
   GuiConst_INTCOLOR Color)
{
  GuiConst_INT16S Xb, Xm;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  Xb = X / 8;
  Xm = X % 8;

  PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + Xb;
  PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
  while (Y1 <= Y2)
  {
    *PixelPtr1 =
       ColorPattern0[Xm][Color] | (*PixelPtr1 & LinePatternPixel[Xm]);
    *PixelPtr2 =
       ColorPattern1[Xm][Color] | (*PixelPtr2 & LinePatternPixel[Xm]);
    PixelPtr1 += sgl.CurLayerLineSize;
    PixelPtr2 += sgl.CurLayerLineSize;
    Y1++;
  }
}

//------------------------------------------------------------------------------
static void DrawChar(
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiLib_FontRecPtr Font,
#ifdef GuiConst_REMOTE_FONT_DATA
   GuiConst_INT32S CharNdx,
#else
   GuiConst_INT8U PrefixRom * CharPtr,
#endif
   GuiConst_INTCOLOR Color)
{
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *PixelData;
  GuiConst_INT8U * CharPtr;
#else
  GuiConst_INT8U PrefixRom *PixelData;
#endif
  GuiConst_INT8S BitOffset1, BitOffset2;
  GuiConst_INT8U PixelByte1Visible, PixelByte2Visible;
  GuiConst_INT8U PixelPattern1, PixelPattern2;
  GuiConst_INT16S N;
  GuiConst_INT8U YHeight;
  GuiConst_INT16S Bx;
  GuiConst_INT16S PY, Y2;
  GuiConst_INT8U PixelLineSize;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;
#ifndef GuiConst_FONT_UNCOMPRESSED
#ifdef GuiConst_REMOTE_FONT_DATA
  GuiConst_INT8U *LineCtrl;
#else
  GuiConst_INT8U PrefixRom *LineCtrl;
#endif
  GuiConst_INT8U LineCtrlByte;
  GuiConst_INT16S LineRepeat;
  GuiConst_INT16S M;
  GuiConst_INT8U Finished;
#endif
  GuiConst_INT8U ColorBit0;
  GuiConst_INT8U ColorBit1;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (sgl.ClippingTotal)
    return;
#endif

#ifdef GuiConst_REMOTE_FONT_DATA
  if (CharNdx != sgl.CurRemoteFont)
  {
    GuiLib_RemoteDataReadBlock(
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx + 1] -
       (GuiConst_INT32U PrefixRom)GuiFont_ChPtrList[CharNdx],
       sgl.GuiLib_RemoteFontBuffer);
    sgl.CurRemoteFont = CharNdx;
  }
  CharPtr = &sgl.GuiLib_RemoteFontBuffer[0];
#endif

  if ((*(CharPtr + GuiLib_CHR_XWIDTH_OFS) == 0) ||
      (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) == 0))
    return;

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST(Color);
  ColorBit0 = (Color & 0x02) >> 1;
  ColorBit1 = Color & 0x01;

  gl.Dummy1_8U = Font->LineSize;   // To avoid compiler warning
#ifdef GuiConst_FONT_UNCOMPRESSED
  PixelLineSize = Font->LineSize;
  #ifdef GuiConst_ROTATED_90DEGREE
  YHeight = Font->XSize;
  #else
  YHeight = Font->YSize;
  #endif
  PixelData = CharPtr + GuiLib_CHR_LINECTRL_OFS + 1;
#else
  #ifdef GuiConst_ROTATED_90DEGREE
  PixelLineSize = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  #else
  PixelLineSize = *(CharPtr + GuiLib_CHR_XWIDTH_OFS);
  YHeight = *(CharPtr + GuiLib_CHR_YHEIGHT_OFS);
  #endif
  LineCtrl = CharPtr + GuiLib_CHR_LINECTRL_OFS;
  N = (YHeight + 7) / 8;
  if (N == 0)
    N++;
  PixelData = LineCtrl + N;
  PixelLineSize = (PixelLineSize + 7) / 8;
#endif

#ifdef GuiConst_FONT_UNCOMPRESSED

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #else
  X -= Font->XSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #else
  Y -= Font->XSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->YSize - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= Font->XSize - 1;
    #else
  X -= Font->XSize - 1;
  Y -= Font->YSize - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= Font->XSize - 1;
    #else
  X -= Font->YSize - 1;
  Y -= Font->XSize - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= Font->YSize - 1;
    #endif
  #endif
#endif

#else

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_YTOP_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y -= (*(CharPtr + GuiLib_CHR_XWIDTH_OFS) +
        *(CharPtr + GuiLib_CHR_XLEFT_OFS) - 1);
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X += *(CharPtr + GuiLib_CHR_YTOP_OFS);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #else
  X -= (*(CharPtr + GuiLib_CHR_YHEIGHT_OFS) +
        *(CharPtr + GuiLib_CHR_YTOP_OFS) - 1);
  Y += *(CharPtr + GuiLib_CHR_XLEFT_OFS);
    #endif
  #endif
#endif

#endif

  BitOffset1 = X % 8;
  if (X < 0)
    BitOffset1 += 8;
  BitOffset2 = 8 - BitOffset1;

  PY = 0;
#ifndef GuiConst_FONT_UNCOMPRESSED
  LineCtrlByte = *LineCtrl;
  LineCtrlByte >>= 1;
  LineCtrl++;
#endif
  while (PY < YHeight)
  {
#ifndef GuiConst_FONT_UNCOMPRESSED
    LineRepeat = 0;
    do
    {
      LineRepeat++;
      Finished = (((LineCtrlByte & 0x01) == 0) || (PY >= YHeight - 1));

      PY++;
      if (PY % 8 == 7)
      {
        LineCtrlByte = *LineCtrl;
        LineCtrl++;
      }
      else
        LineCtrlByte >>= 1;
    }
    while (!Finished);
#endif

    Bx = X / 8;
    if (X < 0)
      Bx--;

    for (N = 0; N < PixelLineSize; N++)
    {
      if (*PixelData != 0)
      {
        if (!BitOffset1)
        {
          PixelByte1Visible = ((Bx >= 0) && (Bx < GuiConst_BYTES_PR_LINE));
          if (PixelByte1Visible)
          {
            PixelPattern1 = *PixelData;

#ifdef GuiConst_CLIPPING_SUPPORT_ON
            if (!ClippingXOff)
              PixelPattern1 = PixelPattern1 & ClippingXPattern[Bx];
#endif

            Y2 = Y;
            PixelPtr1 = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Bx;
            PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
#ifndef GuiConst_FONT_UNCOMPRESSED
            for (M = 0; M < LineRepeat; M++)
#endif
            {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
              if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
              {
                if (ColorBit0)
                  *PixelPtr1 |= PixelPattern1;
                else
                  *PixelPtr1 &= ~PixelPattern1;
                if (ColorBit1)
                  *PixelPtr2 |= PixelPattern1;
                else
                  *PixelPtr2 &= ~PixelPattern1;
              }
              PixelPtr1 += sgl.CurLayerLineSize;
              PixelPtr2 += sgl.CurLayerLineSize;
              Y2++;
            }
          }
        }
        else
        {
          PixelByte1Visible = ((Bx >= 0) && (Bx < GuiConst_BYTES_PR_LINE));
          PixelByte2Visible =
             ((Bx + 1 >= 0) && (Bx + 1 < GuiConst_BYTES_PR_LINE));
#ifdef GuiConst_BIT_TOPLEFT
          PixelPattern1 = *PixelData << BitOffset1;
          PixelPattern2 = *PixelData >> BitOffset2;
#else
          PixelPattern1 = *PixelData >> BitOffset1;
          PixelPattern2 = *PixelData << BitOffset2;
#endif

#ifdef GuiConst_CLIPPING_SUPPORT_ON
          if (!ClippingXOff)
          {
            PixelPattern1 = PixelPattern1 & ClippingXPattern[Bx];
            PixelPattern2 = PixelPattern2 & ClippingXPattern[Bx + 1];
          }
#endif

          Y2 = Y;
          PixelPtr1 = sgl.CurLayerBufPtr + Y2 * sgl.CurLayerLineSize + Bx;
          PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
#ifndef GuiConst_FONT_UNCOMPRESSED
          for (M = 0; M < LineRepeat; M++)
#endif
          {
#ifdef GuiConst_CLIPPING_SUPPORT_ON
            if ((Y2 >= sgl.ClippingY1) && (Y2 <= sgl.ClippingY2))
#endif
            {
              if (PixelByte1Visible)
              {
                if (ColorBit0)
                  *PixelPtr1 |= PixelPattern1;
                else
                  *PixelPtr1 &= ~PixelPattern1;
                if (ColorBit1)
                  *PixelPtr2 |= PixelPattern1;
                else
                  *PixelPtr2 &= ~PixelPattern1;
              }
              if (PixelByte2Visible)
              {
                if (ColorBit0)
                  *(PixelPtr1 + 1) |= PixelPattern2;
                else
                  *(PixelPtr1 + 1) &= ~PixelPattern2;
                if (ColorBit1)
                  *(PixelPtr2 + 1) |= PixelPattern2;
                else
                  *(PixelPtr2 + 1) &= ~PixelPattern2;
              }
            }
            PixelPtr1 += sgl.CurLayerLineSize;
            PixelPtr2 += sgl.CurLayerLineSize;
            Y2++;
          }
        }
      }

      PixelData++;
      Bx++;
    }

#ifdef GuiConst_FONT_UNCOMPRESSED
    PY++;
    Y++;
#else
    Y += LineRepeat;
#endif
  }
}

#ifdef GuiConst_BITMAP_SUPPORT_ON
//------------------------------------------------------------------------------
static void ShowBitmapArea(
#ifdef GuiConst_REMOTE_BITMAP_DATA
   GuiConst_INT8U * PixelDataPtr,
#else
   GuiConst_INT8U PrefixRom * PixelDataPtr,
#endif
   GuiConst_INT16S X,
   GuiConst_INT16S Y,
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2,
   GuiConst_INT32S TranspColor,
   GuiConst_INT8U BitmapType)
{
  GuiConst_INT16S SizeX;
  GuiConst_INT16S SizeY;
  GuiConst_INT16S ByteSizeX;
  GuiConst_INT8U PrefixRom *PixelDataPtr2;
  GuiConst_INT8U B;
  GuiConst_INT16S X1b, X2b, Xb;
  GuiConst_INT8S BitOffset1a;
  GuiConst_INT8U BitOffsetX1, BitOffsetX2;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  SizeX = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeX += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY = (GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  SizeY += 256*(GuiConst_INT16S)*PixelDataPtr;
  PixelDataPtr++;
  ByteSizeX = (SizeX + 7) / 8;

#ifdef GuiConst_ROTATED_90DEGREE
  sgl.BitmapWriteX2 = X + SizeY - 1;
  sgl.BitmapWriteY2 = Y + SizeX - 1;
#else
  sgl.BitmapWriteX2 = X + SizeX - 1;
  sgl.BitmapWriteY2 = Y + SizeY - 1;
#endif

  GuiLib_COORD_ADJUST(X, Y);
  GuiLib_COLOR_ADJUST_TRANSP(TranspColor);

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    GuiLib_COORD_ADJUST(X1, Y1);
    GuiLib_COORD_ADJUST(X2, Y2);
    OrderCoord(&X1, &X2);
    OrderCoord(&Y1, &Y2);
  }

#ifdef GuiConst_ROTATED_OFF
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_RIGHT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #else
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_UPSIDEDOWN
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifndef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #endif
  #else
    #ifdef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #endif
#endif
#ifdef GuiConst_ROTATED_90DEGREE_LEFT
  #ifdef GuiConst_MIRRORED_HORIZONTALLY
    #ifdef GuiConst_MIRRORED_VERTICALLY
  Y -= SizeY - 1;
    #else
  X -= SizeX - 1;
  Y -= SizeY - 1;
    #endif
  #else
    #ifndef GuiConst_MIRRORED_VERTICALLY
  X -= SizeX - 1;
    #endif
  #endif
#endif

  if (BitmapType == GuiLib_AREA_BITMAP)
  {
    if ((X1 > X + SizeX - 1) || (X2 < X) || (Y1 > Y + SizeY - 1) || (Y2 < Y))
      return;
    if (X1 < X)
      X1 = X;
    if (X2 > X + SizeX - 1)
      X2 = X + SizeX - 1;
    if (Y1 < Y)
      Y1 = Y;
    if (Y2 > Y + SizeY - 1)
      Y2 = Y + SizeY - 1;
  }
  else
  {
    X2 = X + SizeX - 1;
    Y2 = Y + SizeY - 1;

    OrderCoord(&X, &X2);
    OrderCoord(&Y, &Y2);

    X1 = X;
    Y1 = Y;
  }

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);   // Before changing Y1

    X1b = X1 / 8;
    X2b = X2 / 8;

    PixelDataPtr += 2 * (ByteSizeX * (Y1 - Y) + X1b - (X / 8));

    BitOffset1a = X % 8;
    if (BitOffset1a < 0)
      BitOffset1a += 8;
    BitOffsetX1 = X1 % 8;
    BitOffsetX2 = X2 % 8;

    while (Y1 <= Y2)
    {
      PixelDataPtr2 = PixelDataPtr;

      PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
      PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);
      if (X % 8 == 7)
      {
        if (X1b == X2b)
        {
          *PixelPtr1 =
             (*PixelPtr1 &
             ~(LinePatternLeft[BitOffsetX1] &
             LinePatternRight[BitOffsetX2])) |
             (*PixelDataPtr2 &
             (LinePatternLeft[BitOffsetX1] & LinePatternRight[BitOffsetX2]));
          PixelDataPtr2++;
          *PixelPtr2 =
             (*PixelPtr2 &
             ~(LinePatternLeft[BitOffsetX1] &
             LinePatternRight[BitOffsetX2])) |
             (*PixelDataPtr2 &
             (LinePatternLeft[BitOffsetX1] & LinePatternRight[BitOffsetX2]));
        }
        else
        {
          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternLeft[BitOffsetX1]) |
             (*PixelDataPtr2 & LinePatternLeft[BitOffsetX1]);
          PixelDataPtr2++;
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternLeft[BitOffsetX1]) |
             (*PixelDataPtr2 & LinePatternLeft[BitOffsetX1]);
          PixelDataPtr2++;
          PixelPtr1++;
          PixelPtr2++;

          for (Xb = X1b + 1; Xb < X2b; Xb++)
          {
            *PixelPtr1 = *PixelDataPtr2;
            PixelDataPtr2++;
            *PixelPtr2 = *PixelDataPtr2;
            PixelDataPtr2++;
            PixelPtr1++;
            PixelPtr2++;
          }

          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternRight[BitOffsetX2]) |
             (*PixelDataPtr2 & LinePatternRight[BitOffsetX2]);
          PixelDataPtr2++;
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternRight[BitOffsetX2]) |
             (*PixelDataPtr2 & LinePatternRight[BitOffsetX2]);
        }
      }
      else
      {
        if (X1b == X2b)
        {
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*PixelDataPtr2) & LinePatternRight[BitOffsetX1]) <<
             (BitOffsetX1);
#else
          B = (*PixelDataPtr2) >> (BitOffsetX1);
#endif
          *PixelPtr1 =
             (*PixelPtr1 &
             ~(LinePatternLeft[BitOffsetX1] &
             LinePatternRight[BitOffsetX2])) |
             (B & (LinePatternLeft[BitOffsetX1] &
             LinePatternRight[BitOffsetX2]));
          PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*PixelDataPtr2) & LinePatternRight[BitOffsetX1]) <<
             (BitOffsetX1);
#else
          B = (*PixelDataPtr2) >> (BitOffsetX1);
#endif
          *PixelPtr2 =
             (*PixelPtr2 &
             ~(LinePatternLeft[BitOffsetX1] &
             LinePatternRight[BitOffsetX2])) |
             (B & (LinePatternLeft[BitOffsetX1] &
             LinePatternRight[BitOffsetX2]));
        }
        else
        {
#ifdef GuiConst_BIT_TOPLEFT
          B = *PixelDataPtr2 << (BitOffsetX1);
#else
          B = *PixelDataPtr2 >> (BitOffsetX1);
#endif
          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternLeft[BitOffsetX1]) |
             (B & LinePatternLeft[BitOffsetX1]);
          PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
          B = *PixelDataPtr2 << (BitOffsetX1);
#else
          B = *PixelDataPtr2 >> (BitOffsetX1);
#endif
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternLeft[BitOffsetX1]) |
             (B & LinePatternLeft[BitOffsetX1]);
          PixelDataPtr2++;
          PixelPtr1++;
          PixelPtr2++;

          for (Xb = X1b + 1; Xb < X2b; Xb++)
          {
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 2)) >> (8 - (BitOffsetX1))) +
                (*PixelDataPtr2 << (BitOffsetX1));
#else
            B = ((*(PixelDataPtr2 - 2)) << (8 - (BitOffsetX1))) +
                (*PixelDataPtr2 >> (BitOffsetX1));
#endif
            *PixelPtr1 = B;
            PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
            B = ((*(PixelDataPtr2 - 2)) >> (8 - (BitOffsetX1))) +
                (*PixelDataPtr2 << (BitOffsetX1));
#else
            B = ((*(PixelDataPtr2 - 2)) << (8 - (BitOffsetX1))) +
                (*PixelDataPtr2 >> (BitOffsetX1));
#endif
            *PixelPtr2 = B;
            PixelDataPtr2++;
            PixelPtr1++;
            PixelPtr2++;
          }

#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 2)) >> (8 - (BitOffsetX1))) +
              (*PixelDataPtr2 << (BitOffsetX1));
#else
          B = ((*(PixelDataPtr2 - 2)) << (8 - (BitOffsetX1))) +
              (*PixelDataPtr2 >> (BitOffsetX1));
#endif
          *PixelPtr1 =
             (*PixelPtr1 & ~LinePatternRight[BitOffsetX2]) |
             (B & LinePatternRight[BitOffsetX2]);
          PixelDataPtr2++;
#ifdef GuiConst_BIT_TOPLEFT
          B = ((*(PixelDataPtr2 - 2)) >> (8 - (BitOffsetX1))) +
              (*PixelDataPtr2 << (BitOffsetX1));
#else
          B = ((*(PixelDataPtr2 - 2)) << (8 - (BitOffsetX1))) +
              (*PixelDataPtr2 >> (BitOffsetX1));
#endif
          *PixelPtr2 =
             (*PixelPtr2 & ~LinePatternRight[BitOffsetX2]) |
             (B & LinePatternRight[BitOffsetX2]);
        }
      }

      PixelDataPtr += 2 * ByteSizeX;
      Y1++;
    }
  }
}
#endif

//==============================================================================

//------------------------------------------------------------------------------
void GuiLib_InvertBox(
   GuiConst_INT16S X1,
   GuiConst_INT16S Y1,
   GuiConst_INT16S X2,
   GuiConst_INT16S Y2)
{
  GuiConst_INT16S X, X1b, X2b;
  GuiConst_INT8U *PixelPtr1, *PixelPtr2;

  GuiLib_COORD_ADJUST(X1, Y1);
  GuiLib_COORD_ADJUST(X2, Y2);

  OrderCoord(&X1, &X2);
  OrderCoord(&Y1, &Y2);

#ifdef GuiConst_CLIPPING_SUPPORT_ON
  if (CheckRect (&X1, &Y1, &X2, &Y2))
#endif
  {
    MarkDisplayBoxRepaint(X1, Y1, X2, Y2);
    while (Y1 <= Y2)
    {
      X1b = X1 / 8;
      X2b = X2 / 8;
      PixelPtr1 = sgl.CurLayerBufPtr + Y1 * sgl.CurLayerLineSize + X1b;
      PixelPtr2 = PixelPtr1 + (GuiConst_DISPLAY_BYTES / 2);

      if (X1b == X2b)
      {
        *PixelPtr1 ^= LinePatternLeft[X1 % 8] & LinePatternRight[X2 % 8];
        *PixelPtr2 ^= LinePatternLeft[X1 % 8] & LinePatternRight[X2 % 8];
      }
      else
      {
        *PixelPtr1 ^= LinePatternLeft[X1 % 8];
        *PixelPtr2 ^= LinePatternLeft[X1 % 8];
        PixelPtr1++;
        PixelPtr2++;

        for (X = X1b + 1; X < X2b; X++)
        {
          *PixelPtr1 = ~*PixelPtr1;
          *PixelPtr2 = ~*PixelPtr2;
          PixelPtr1++;
          PixelPtr2++;
        }

        *PixelPtr1 ^= LinePatternRight[X2 % 8];
        *PixelPtr2 ^= LinePatternRight[X2 % 8];
      }

      Y1++;
    }
  }
}

