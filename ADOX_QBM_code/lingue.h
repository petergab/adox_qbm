


 #define GUASTO_IN  0
 #define GUASTO_OUT 1
//testi nelle varie lingue
//                                  10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL1[] = " phase failure anomalia fasi Phasenfehler";
  char ID_MSG_GUASTO_AL1[] = {0,14,28,41};
//                                  10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL2[] = " low pressure bassa pressione Druck zu niedrig";
  char ID_MSG_GUASTO_AL2[] = {0,13,29,46};
//                                  10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL3[] = " motor failure motore Motor";
  char ID_MSG_GUASTO_AL3[] = {0,14,21,27};
//                                  10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL4[] = " vacuum-pump high T. alta temp motori VP hoch Motortemp.  VP";
  char ID_MSG_GUASTO_AL4[] = {0,20,40,60};
//                                  10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL5[] = " output pressure pressione uscita Ausgangdruck";
  char ID_MSG_GUASTO_AL5[] = {0,16,33,46};
//                                  10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL8[] = " inverter inverter inverter";
  char ID_MSG_GUASTO_AL8[] = {0,9,18,27};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL11[] = " high air temp. alta temperat. aria Zuluft-Temp. zu hoch";
  char ID_MSG_GUASTO_AL11[] = {0,15,35,56};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL12[] = " cabinet temperature temperatura cabinet cabinet temperatur";
  char ID_MSG_GUASTO_AL12[] = {0,20,40,59};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL20[] = " O2 out of the range O2 non in purezza O2 zu hoch";
  char ID_MSG_GUASTO_AL20[] = {0,20,38,49};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL21[] = " high air pressure alta press. aria Zuluft-Druck zu hoch";
  char ID_MSG_GUASTO_AL21[] = {0,18,35,46};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL22[] = " high air pressure alta press. aria Zuluft-Druck zu hoch";
  char ID_MSG_GUASTO_AL22[] = {0,18,35,46};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL24[] = " discharger scaricatore Entlader";
  char ID_MSG_GUASTO_AL24[] = {0,11,23,32};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_CAN[] = " CAN-BUS CAN_BUS CAN-BUS";
  char ID_MSG_GUASTO_CAN[] = {0,8,16,24};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL26[] = " Slave Slave Slave";
  char ID_MSG_GUASTO_AL26[] = {0,6,12,18};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL27[] = " PT-C sensor fault Guasto sonda PT-C PT-C Sensor Fehler";
  char ID_MSG_GUASTO_AL27[] = {0,18,36,55};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL28[] = " PT-V sensor fault Guasto sonda PT-V PT-V Sensor Fehler";
  char ID_MSG_GUASTO_AL28[] = {0,18,36,55};
//                                   10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160
//                          123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
  char MSG_GUASTO_AL29[] = " PT-N2 sensor fault Guasto sonda PT-N2 PT-N2 Sensor Fehler";
  char ID_MSG_GUASTO_AL29[] = {0,19,38,58};


  char MSG_CUT[] = " : cut : taglio";
  char ID_MSG_CUT[] = {0,6,15};
  char MSG_MISSING_DEV[] = " : no device or broken relays : dispositivo assente o guasto rele";
  char ID_MSG_MISSING_DEV[] = {0,29,65};
  char MSG_SHORTCIRCUIT[] = " : short circuit : corto circuito";
  char ID_MSG_SHORTCIRCUIT[] = {0,16,33};
  char MSG_NO_ALARMS[] = "No alarmsNessun allarme presente";
  char ID_MSG_NO_ALARMS[] = {0,9,32};
  char MSG_ALARMS_RESET[] = "Alarms reset Ureset allarmi U";
  char ID_MSG_ALARMS_RESET[] = {0,14,29};
  char MSG_ALARMS_DEL[] = "Alarms list deleted UCancellata lista allarmi U";
  char ID_MSG_ALARMS_DEL[] = {0,21,47};
  char MSG_EVENTS_DEL[] = "Events list deleted UCancellata lista eventi U";
  char ID_MSG_EVENTS_DEL[] = {0,21,46};
  char MSG_O2_LOW[] = "TH LOW O2 basso ";
  char ID_MSG_O2_LOW[] = {0,7,16};
  char MSG_O2_HIGH[] = "TH HIGH O2 alto ";
  char ID_MSG_O2_HIGH[] = {0,8,16};
  char MSG_ALL[] = "ALL TUTTE";
  char ID_MSG_ALL[] = {0,4,9};
  char MSG_IST[] = "hyst: ist: ";
  char ID_MSG_IST[] = {0,6,11};
  char MSG_SET[] = "set: set: ";
  char ID_MSG_SET[] = {0,5,10};
  char MSG_SYSTEM_ON[] = "system turned ONavvio sistema";
  char ID_MSG_SYSTEM_ON[] = {0,16,29};
  char MSG_NEW_CONFIG[] = "loaded new configuration caricata nuova configurazione ";
  char ID_MSG_NEW_CONFIG[] = {0,25,55};
  char MSG_LOGOUT[] = "logout user level logout utente livello ";
  char ID_MSG_LOGOUT[] = {0,18,40};
  char MSG_LOGIN[] = "login user level login utente livello ";
  char ID_MSG_LOGIN[] = {0,17,38};
  char MSG_DEACTIV[] = "deactiveted disattivata ";
  char ID_MSG_DEACTIV[] = {0,12,24};
  char MSG_ACTIV[] = "activeted attivata ";
  char ID_MSG_ACTIV[] = {0,10,19};
  char MSG_BUZZER[] = "buzzer cicalino ";
  char ID_MSG_BUZZER[] = {0,7,16};
  char MSG_FLUX[] = "flux flussostato ";
  char ID_MSG_FLUX[] = {0,5,17};
  char MSG_AUX_FLUX[] = "aux flux f.ausiliario ";
  char ID_MSG_AUX_FLUX[] = {0,9,22};
  char MSG_DISPLAY[] = "display display ";
  char ID_MSG_DISPLAY[] = {0,8,16};
  char MSG_VALVE[] = "valve valvola ";
  char ID_MSG_VALVE[] = {0,6,14};
  char MSG_AUX_VALVE[] = "aux valve v.ausiliaria ";
  char ID_MSG_AUX_VALVE[] = {0,10,23};
  char MSG_EXT_N2[] = "Ext. N2 supplyingresso N2 esterno";
  char ID_MSG_EXT_N2[] = {0,14,33};
  char MSG_PANEL[] = "panel pannello ";
  char ID_MSG_PANEL[] = {0,6,15};
  char MSG_LAMP[] = "lamp lamp. ";
  char ID_MSG_LAMP[] = {0,5,11};
  char MSG_SIREN[] = "siren sirena ";
  char ID_MSG_SIREN[] = {0,6,13};
  char MSG_O2_SENSOR[] = "sensor sensore ";
  char ID_MSG_O2_SENSOR[] = {0,7,15};
  char MSG_USER[] = "User Utente ";
  char ID_MSG_USER[] = {0,5,12};
  char MSG_ENABLED[] = " Enabled  abilitata ";
  char ID_MSG_ENABLED[] = {0,9,20};
  char MSG_DISABLED[] = " Disabled  disabilitata ";
  char ID_MSG_DISABLED[] = {0,10,24};
  char MSG_MACHINE[] = "Machine PSA/ADOX ";
  char ID_MSG_MACHINE[] = {0,8,17};
  char MSG_GUASTO_COMUNICAZIONE_ANYBUS[] = "Anybus communication fault guasto comunicazione Anybus ";
  char ID_MSG_GUASTO_COMUNICAZIONE_ANYBUS[] = {0,27,55};
  char MSG_GUASTO_COMUNICAZIONE_WEBSERVER[] = "Webserver communication fault guasto comunicazione Webserver ";
  char ID_MSG_GUASTO_COMUNICAZIONE_WEBSERVER[] = {0,30,61};
  char MSG_GUASTO_COMUNICAZIONE_DISPLAY[] = "communication fault display guasto comunicaz. display ";
  char ID_MSG_GUASTO_COMUNICAZIONE_DISPLAY[] = {0,28,54};
  char MSG_SD_SBM[] = "SBM-SD card not insertedMemoria SD-SBM assente";
  char ID_MSG_SD_SBM[] = {0,24,46};
  char MSG_RELE[] = "relays rele ";
  char ID_MSG_RELE[] = {0,7,12};
  char MSG_OPEN[] = " open aperto";
  char ID_MSG_OPEN[] = {0,5,12};
  char MSG_CORTO[] = " short-c corto-c";
  char ID_MSG_CORTO[] = {0,8,16};
  char MSG_INPUT[] = "input ingresso ";
  char ID_MSG_INPUT[] = {0,6,15};
  char MSG_OUTPUT[] = "output uscita ";
  char ID_MSG_OUTPUT[] = {0,7,14};
  char MSG_EXT_FAULT[] = "External faultguasto esterno";
  char ID_MSG_EXT_FAULT[] = {0,14,28};
  char MSG_PSA_ADOX[] = "Machine macchina ";
  char ID_MSG_PSA_ADOX[] = {0,8,17};
  char MSG_TEMP_SM[] = "Temperature fault machine room Guasto temperatura sala macchine ";
  char ID_MSG_TEMP_SM[] = {0,31,64};
  char MSG_BATTERY[] = "Battery Batteria ";
  char ID_MSG_BATTERY[] = {0,8,17};
  char MSG_GUASTO_RETE[] = " network fault guasto di rete";
  char ID_MSG_GUASTO_RETE[] = {0,14,29};
  char MSG_ALIMENTATORE[] = "power supply alimentatore ";
  char ID_MSG_ALIMENTATORE[] = {0,13,26};
  char MSG_FAULT[] = " fault  guasto ";
  char ID_MSG_FAULT[] = {0,7,15};
  char MSG_FAULT_ZONE[] = " fault zone  guasto zona ";
  char ID_MSG_FAULT_ZONE[] = {0,12,25};
  char MSG_SWITCH[] = "Switch finecorsa ";
  char ID_MSG_SWITCH[] = {0,7,17};
  char MSG_FLOW_FAULT[] = " N2 flow fault zone  guasto flusso N2 zona ";
  char ID_MSG_FLOW_FAULT[] = {0,20,43};
  char MSG_AUX_FLOW_FAULT[] = "Auxiliary N2 supply flow faultGuasto flusso N2 ausiliario";
  char ID_MSG_AUX_FLOW_FAULT[] = {0,30,57};
  char MSG_O2_MAG_20[] = "Oxygen difference > 2.0% SDifferenza O2 > 2.0% S";
  char ID_MSG_O2_MAG_20[] = {0,26,48};
  char MSG_O2_MAG_05[] = "Oxygen difference > 0.5% SDifferenza O2 > 0.5% S";
  char ID_MSG_O2_MAG_05[] = {0,26,48};
  char MSG_COMM_FAULT_SENSOR[] = " comm. fault sensor  guasto com. sensore ";
  char ID_MSG_COMM_FAULT_SENSOR[] = {0,20,41};
  char MSG_WARMUP[] = "Warm up fault sensor sensore in warmup ";
  char ID_MSG_WARMUP[] = {0,21,39};
  char MSG_ELAB_FAULT_SENSOR[] = "Elaboration fault sensor guasto elaborazione sensore ";
  char ID_MSG_ELAB_FAULT_SENSOR[] = {0,25,53};
  char MSG_GUASTO_IN_OUT[] = " <-- -->";
  char ID_MSG_GUASTO_IN_OUT[] = {0,4,8};
  char MSG_ZONA[] = " zone  zona ";
  char ID_MSG_ZONA[] = {0,6,12};
  char MSG_GUASTO_COMUNICAZIONE_SENSORE[] = "communication fault sensor Guasto comunicazione sensore ";
  char ID_MSG_GUASTO_COMUNICAZIONE_SENSORE[] = {0,27,56};
  char MSG_AL_ESTERNO[] = "External alarmAllarme esterno";
  char ID_MSG_AL_ESTERNO[] = {0,14,29};
  char MSG_AL_O2_ALTO[] = "Oxygen level over Livello ossigeno > ";
  char ID_MSG_AL_O2_ALTO[] = {0,18,37};
  char MSG_AL_O2_BASSO[] = "Oxygen level under Livello ossigeno < ";
  char ID_MSG_AL_O2_BASSO[] = {0,19,38};
  char MSG_CAN[] = "CAN CAN ";
  char ID_MSG_CAN[] = {0,4,8};
  char MSG_MODULO[] = "module modulo ";
  char ID_MSG_MODULO[] = {0,7,14};
  char MSG_GUASTO_COMUNICAZIONE[] = "communication faultguasto comunicazione";
  char ID_MSG_GUASTO_COMUNICAZIONE[] = {0,19,39};
  char MSG_GUASTO_COMUNICAZIONE_ADOX[] = "Adox communication faultGuasto comunicazione Adox";
  char ID_MSG_GUASTO_COMUNICAZIONE_ADOX[] = {0,24,49};
  char MSG_GUASTO_COMUNICAZIONE_CIE[] = "CIE communication faultGuasto comunicazione CIE";
  char ID_MSG_GUASTO_COMUNICAZIONE_CIE[] = {0,23,47};
