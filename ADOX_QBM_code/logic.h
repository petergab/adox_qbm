


#include "Drivers/lpc_types.h"

//la EEPROM; � divisa in 63 pagine (0-62), ognuna con 64 byte (0-63)
//#define E2_MAX_PAGE  4 // utilizzo le prime E2_MAX_PAGE/2 pagine per BYTE le restanti per INTERI
#define E2_MAX_BYTE 60 // i byte sarebbero 63, ne uso solo 60 tanto non me ne servono molti - per ora non lo uso
#define E2_MAX_PAGE  4 // utilizzo 4 pagine tutte di INTERI (sono 120 pi� di quelli della versione NewAdso)
#define E2_MAX_INT  30 // numero di interi per pagina
#define E2_PAGE_1    0
#define E2_PAGE_2    1
#define E2_PAGE_3    2
#define E2_PAGE_4    3


//E2prom - PAGE_1 - integer - analogiche inizio e fine scala impostabili
#define E2_SD_DATA_VALID       0
#define E2_1_INPUT1_IS         1
#define E2_1_INPUT1_FS         2
#define E2_1_INPUT2_IS         3
#define E2_1_INPUT2_FS         4
#define E2_1_INPUT3_IS         5
#define E2_1_INPUT3_FS         6
#define E2_1_INPUT4_IS         7
#define E2_1_INPUT4_FS         8
#define E2_1_INPUT5_IS         9
#define E2_1_INPUT5_FS        10
#define E2_1_INPUT6_IS        11
#define E2_1_INPUT6_FS        12
#define E2_1_INPUT7_IS        13
#define E2_1_INPUT7_FS        14
#define E2_1_INPUT8_IS        15
#define E2_1_INPUT8_FS        16
#define E2_1_INPUT9_IS        17
#define E2_1_INPUT9_FS        18
#define E2_1_INPUT10_IS       19
#define E2_1_INPUT10_FS       20


//E2prom - PAGE_2 - integer - parametri impostazioni lavoro Adox
#define E2_2_ADOX_TYPE         0 // a seconda del modello Adox N2 avr� fasi lavoro differenti e tipo valvole differenti
#define E2_2_MACHINE           1 // tipo Adox
#define E2_2_TA_VSAPSA         2
#define E2_2_TC_VSAPSA         3
#define E2_2_SCARICO           4
#define E2_2_SFASAMENTO        5
#define E2_2_TMR_RIT_START     6 //timer ritardo accensione Adox dopo ricezione comando
#define E2_2_TOO_HIGH_TEMP     7 //soglia allarme temperatura (cabinet) elevata che comporta arresto macchina
#define E2_2_TEMP_ALARM        8 //soglia allarme temperatura aria
#define E2_2_TMR_RIT_VACUUM    9
#define E2_2_ADOX_O2_SET      10
#define E2_2_ADOX_ISTERESI    11
#define E2_2_O2_HIGH_ALARM    12 
#define E2_2_POWER_USER       13 //abilita/disabilita l'utente per cambiare LOC/REM e START/STOP
#define E2_2_SOLO_MEDIA       14 //visualizzazione media e valore istantaneo O2
#define E2_2_MEDIAMOBILE      15
#define E2_2_TMR_B2_ERROR     16 //timer errore sonda B2 - se 0 non c'� ritardo
#define E2_2_PRESS_START_AL2  17 //pressione di alert per P1 (P-C) - se 0 � OFF non controlla le sonde
#define E2_2_PRESS_STOP_AL2   18 //pressione di stop per P1 (P-C)
#define E2_2_T3_R5_TEMP       19 //soglia attivazione ventilazione forzata per ridurre la temperatura
#define E2_2_HIGH_TEMP        20 //soglia allarme temperatura elevata
#define E2_2_VENTIL_TEMP      21 //soglia attivazione ventilazione forzata per ridurre la temperatura
#define E2_2_TMR_COMP_ON      22 //timer rel� compensazione ON
#define E2_2_TMR_COMP_OFF     23 //timer rel� compensazione OFF


//page 3 - integer - parametri comunicazioni seriali
#define E2_3_ID_CAN_1           0  //identificatore scheda nella rete CAN 
#define E2_3_ID_CAN_2           1  //identificatore scheda nella rete CAN 
#define E2_3_ID_CAN_PORT_SEL    2  //seleziona quale porta CAN � Master e quale Slave
#define E2_3_CAN_1_BAUDRATE     3  //imposta BTR0 per baudrate CAN 1
#define E2_3_CAN_2_BAUDRATE     4  //imposta BTR0 per baudrate CAN 2
#define E2_3_ID_RS485_1         5  //identificatore scheda nella rete RS485
#define E2_3_ID_RS485_2         6  //identificatore scheda nella rete RS485
#define E2_3_ID_RS485_PORT_SEL  7  //seleziona quale porta COM � Master e quale Slave
#define E2_3_UART1_BAUDRATE     8  //configurazione BAUDRATE seriale 1
#define E2_3_UART2_BAUDRATE     9  //configurazione BAUDRATE seriale 2


//page 4 - integer - analogiche valori ADC da calcolare in fase di taratura - preimpostati di default da "InitSpecial" 
#define E2_4_INPUT_1_ZEROADC   0
#define E2_4_INPUT_1_SPANADC   1
#define E2_4_INPUT_2_ZEROADC   2
#define E2_4_INPUT_2_SPANADC   3
#define E2_4_INPUT_3_ZEROADC   4
#define E2_4_INPUT_3_SPANADC   5
#define E2_4_INPUT_4_ZEROADC   6
#define E2_4_INPUT_4_SPANADC   7
#define E2_4_INPUT_5_ZEROADC   8
#define E2_4_INPUT_5_SPANADC   9
#define E2_4_INPUT_6_ZEROADC  10
#define E2_4_INPUT_6_SPANADC  11
#define E2_4_INPUT_7_ZEROADC  12
#define E2_4_INPUT_7_SPANADC  13
#define E2_4_INPUT_8_ZEROADC  14
#define E2_4_INPUT_8_SPANADC  15
#define E2_4_INPUT_9_ZEROADC  16
#define E2_4_INPUT_9_SPANADC  17
#define E2_4_INPUT_10_ZEROADC 18
#define E2_4_INPUT_10_SPANADC 19
#define E2_4_AVGINDEXMAX      20
#define E2_4_HALFDAY          21 // - solo da programma: ogni 12 ore salva su E2prom ore lavoro
#define E2_4_LOC_REM_STATUS   22 // - solo da programma: impostazione stato CMD_LOCAL_ON, CMD_LOCAL_OFF, CMD_REMOTE
#define E2_4_SERVICE          23
#define E2_4_NEXT_SERVICE     24


//pagine di EEPROM per la memorizzazione della configurazione
#define E2PROM_PAGE_OF_OTHER            50

//registri della pagina E2PROM_PAGE_OF_OTHER  (page 50)
#define REG_BOARD_NUMBER              0
#define REG_NEW_CONFIGURATION         1
#define REG_LANGUAGE                  2
#define REG_BACKLIGHT                 3

//ingressi analogici 0-1V, 4-20mA o PT100
#define NUMERO_SONDE   8

//ADOX ingressi analogici utilizzati:
// 2 PT100
// 3 trasduttori di pressione 4-20mA
// 1 flussimetro 4-20 mA (opzionale)
// se si vuole liberare un ingresso digitale si potrebbe sostituire il pressostato con un trasduttore di pressione 4-20mA
// rimane libero ancora un ingresso analogico

//canale sonde PT100 - da sistemare
#define SONDATEMP1   0
#define SONDATEMP2   1
#define SONDATEMP3   5
#define SONDATEMP4   2
#define SONDATEMP5   4
#define SONDATEMP6   3


//ingressi
#define DIG_IN_1     0x01
#define DIG_IN_2     0x02
#define DIG_IN_3     0x04
#define DIG_IN_4     0x08
#define DIG_IN_5     0x10
#define DIG_IN_6     0x20
#define DIG_IN_7     0x40
#define DIG_IN_8     0x80

//non ci sono e non servono, se servissero per macchine "speciali" utilizzeremo un modulino di I/O esterno
//#define DIG_IN_9     0x100
//#define DIG_IN_10    0x200
//#define DIG_IN_11    0x400



#define MAXRELE 6

#define ON   1
#define OFF  0

#define RUNADOX    0x14  // Pu� lavorare come Adox

#define RELEOFF          0x00
#define RELEON           0x01
#define RELESETON        0x02
#define RELESETOFF       0x04

#define CMD_REMOTE       0x00
#define CMD_LOCAL_OFF    0x01
#define CMD_LOCAL_ON     0x02


//definizione uscite ADOX rel� sulla scheda SBM
#define OUT_CC_ADOX      0  // R1 cc macchina - compressor START
#define OUT_1_ADOX       1  // R2 pompa da vuoto VP1 - PTC vacuum pump
#define OUT_ALARM        2  // R3 allarme generico - normalmente ON, si diseccita in caso di guasto (era R10 nella new Adso)
#define OUT_VENTIL_TEMP  3  // R4 ventilazione cabinet - CABINET FAN
#define OUT_R5_LIKE_R1   4  // R5 rel� che replica funzionamento di R1 - sacrificabile o sostituibile con contatto ausiliario su teleruttore
#define OUT_VTARATURA    5  // R6 valvola taratura (comune ai vari modelli)

// i seguenti rel� non sono disponibili su SBM
//#define OUT_R3_LIKE_R10  2  // R3 replica comportamento R10 per segnalare guasto alla macchina Master - non � mai stato usato
//#define OUT_RICIRCOLO    6  // R7:  temporizza il ricircolo per evitare depressione in cella - sacrificabile
//definizione uscite blocchi valvole - Adox N2 2016
//#define OUT_R8_V1A_V2V   7  // R8:  usato se comando valvole elettriche - quindi solo se usiamo valvole elettriche - modulo aggiuntivo
//#define OUT_R9_V2A_V1V   8  // R9:  usato se comando valvole elettriche - quindi solo se usiamo valvole elettriche


//stato valvole CANopen nelle varie fasi a seconda del modello di macchina impostato
//se imposto ADOX_N2_2016 // valvole FESTO DA
#define ADOX_N2_2016_PHASE_A1    0x00000405  // fase 1 assorbimento A1
#define ADOX_N2_2016_PHASE_C1    0x00000104  // fase 2 compensazione C1
#define ADOX_N2_2016_PHASE_S1    0x00000150  // fase 3 scarico S1
#define ADOX_N2_2016_PHASE_A2    0x00000150  // fase 4 assorbimento A2
#define ADOX_N2_2016_PHASE_C2    0x00000104  // fase 5 compensazione C2
#define ADOX_N2_2016_PHASE_S2    0x00000405  // fase 0 scarico S2

//definizione uscite blocchi valvole - Adox 200 Tyco
//se imposto ADOX_200_TYCO // valvole FESTO NC
#define ADOX_200_TYCO_PHASE_A1    0x000000C6  // fase 1 assorbimento A1
#define ADOX_200_TYCO_PHASE_C1    0x00000024  // fase 2 compensazione C1
#define ADOX_200_TYCO_PHASE_S1    0x000000A8  // fase 3 scarico S1
#define ADOX_200_TYCO_PHASE_A2    0x000000B8  // fase 4 assorbimento A2
#define ADOX_200_TYCO_PHASE_C2    0x00000024  // fase 5 compensazione C2
#define ADOX_200_TYCO_PHASE_S2    0x000000C4  // fase 0 scarico S2

//definizione uscite blocchi valvole - Adox 1500 (1300 - UNIVERGOMMA)
//se imposto ADOX_1300_UNIVERGOMMA // valvole FESTO doppio effetto
#define ADOX_1300_UNIVERGOMMA_PHASE_A1    0x00000045  // fase 1 assorbimento A1
#define ADOX_1300_UNIVERGOMMA_PHASE_C1    0x00000014  // fase 2 compensazione C1
#define ADOX_1300_UNIVERGOMMA_PHASE_S1    0x00000511  // fase 3 scarico S1
#define ADOX_1300_UNIVERGOMMA_PHASE_A2    0x00000111  // fase 4 assorbimento A2
#define ADOX_1300_UNIVERGOMMA_PHASE_C2    0x00000014  // fase 5 compensazione C2
#define ADOX_1300_UNIVERGOMMA_PHASE_S2    0x00000445  // fase 0 scarico S2

//definizione uscite blocchi valvole - Adox 1500 (1300)
//se imposto ADOX_1300_HYPOXIC // valvole PNEUMAX doppio effetto o FESTO NC
#define ADOX_1300_HYPOXIC_PHASE_A1    0x0000000B  // fase 1 assorbimento A1
#define ADOX_1300_HYPOXIC_PHASE_C1    0x00000006  // fase 2 compensazione C1
#define ADOX_1300_HYPOXIC_PHASE_S1    0x00000035  // fase 3 scarico S1
#define ADOX_1300_HYPOXIC_PHASE_A2    0x00000015  // fase 4 assorbimento A2
#define ADOX_1300_HYPOXIC_PHASE_C2    0x00000006  // fase 5 compensazione C2
#define ADOX_1300_HYPOXIC_PHASE_S2    0x0000002B  // fase 0 scarico S2*/

#define TMR_PRE_STOP   15  // 15" timer PRE STOP

#define DIM_O2_QUEUE   110  // dimensione coda per media O2

#define DIM_ADOX_TYPE   10  // dimensione array modelli Adox

#define HALFDAY    43200

#define SLAVE_WDOG_TMOUT    5  // minuti di TimeOut dello slave.
#define RIT_O2_ALARM  300   // 300 secondi

#define LOGIC_ABS(x) (  ((int32_t)(x)) < 0 ? (-(x)) : (x))

struct e2prom
{
  unsigned char E2_Byte[E2_MAX_BYTE];   //byte da EEPROM interna
  unsigned int  E2_Int[E2_MAX_INT];     //intero da EEPROM interna
};

struct analog_input {
	unsigned int Span; //Span canali ADC
};

struct digital_input {
	unsigned char Status;//DigIn.Status mi da lo stato degli ingressi digitali bit-wise
};
		
struct strele
{
  unsigned char Stato;                 // ON, OFF, ecc.
  unsigned char Port;                  // PORT corrispondente del micro al rel�
  unsigned char Pin;                   // PIN corrispondente del micro al rel�
  unsigned int SecondiToOn;            // Secondi mancanti all'accensione del rel�.
  unsigned long SecondiOn;     // Minuti che il rel� deve restare ancora acceso.
};

struct adox
{
	double PT100_1_Avg[100];
  unsigned char PT100_1_AvgIndex;
  unsigned char PT100_1_AvgBufferFull;
	double PT100_2_Avg[100];
  unsigned char PT100_2_AvgIndex;
  unsigned char PT100_2_AvgBufferFull;

  unsigned int Timer[2];                  
  unsigned char Ciclo[2];                  
  unsigned long ModuleValve[2];  
  unsigned long Output;  
  unsigned char Mode;  
  unsigned int Assorbimento;           
  unsigned int Compensazione;           
  unsigned int Scarico;  
  unsigned int Sfasamento;  
  unsigned int TmpDummy;  
  unsigned char RitVacuum;
  unsigned int Tmr_Stop;         //timer arresto
  unsigned int FlowAvg[250];
  unsigned char AvgIndex;
  unsigned char AvgBufferFull;
  unsigned long phaseA1[DIM_ADOX_TYPE],phaseC1[DIM_ADOX_TYPE],phaseS1[DIM_ADOX_TYPE],phaseA2[DIM_ADOX_TYPE],phaseC2[DIM_ADOX_TYPE],phaseS2[DIM_ADOX_TYPE];
  unsigned char Tmr_Comp;        // timer compensazione
  unsigned int Tmr_Start;        // timer ritardo partenza
//  unsigned char Ingressi;        // flag degli ingressi
  unsigned int SecondiOn;        // decimi di secondo assorbimento/compensazione acceso.
  unsigned int Allarme;          // Flag allarme Adox
  unsigned char CAN_cmd;         // comando remoto di start/stop via CAN da centrale
//  unsigned char CAN_Flag;        // flag comunicazione CAN
  unsigned int O2_Tmr_Err;        //timer allarme O2 impuro
  unsigned char O2_Fifo[DIM_O2_QUEUE];
  unsigned int O2_FifoPointer;
  unsigned char O2_Queue;
#ifdef SHOW_O2_PPM
  unsigned int SetO2;            // soglia di O2 alla quale l'Adox si deve arrestare
  unsigned int Isteresi;         // isteresi simmetrica rispetto al SET O2
#else
  unsigned char SetO2;           // soglia di O2 alla quale l'Adox si deve arrestare
  unsigned char Isteresi;        // isteresi simmetrica rispetto al SET O2
#endif
  unsigned char Reintegro;       // tempo di reintegro (PSA). N.B. mettere a zero se Adox N2-F
  unsigned char DoppioReintegro; // tempo di reintegro doppio (PSA) o reintegro in Stby per PSA 1/2.
  unsigned int CounterSec;       // contatore Adox secondi accensione.
  unsigned int Ore_Run;          // totale ore funzionamento (da moltiplic. per 12).
  unsigned int TmrService;       // contatore prossimo intervento
  unsigned char Stato;           // ASS_ON, CMP_ON, OFF
  unsigned char Standby;         // stato sequenza standby
  unsigned char Ingressi;        // flag degli ingressi
  unsigned char Tipo;            // modalit� funzionamento Adox / Adso o N2Gen / AdoxN2 o PSA
  unsigned char TmrFirstRun;     // contatore rirardo prima partenza
  unsigned char TmrRitAL2;       // contatore ritardo allarme AL2
  unsigned char TmrRitAL5;
  unsigned char Al11enabled;     // ritardo prima di considerare AL11 per PSA (in minuti)
  unsigned char AL2_Memo[7];     //array memoria allarme AL2
  unsigned int TmrFirstSincro;   // timer sincronizzazione inizio lavoro PSA in parallelo
  unsigned char Set_Pause;       // indica il raggiungimento del SET per VSA o PSA.
  unsigned char StatoSlave;
  unsigned int TmrStartUp;
  unsigned char TmrErrB1;        //timer ritardo errore sonde pressione
  unsigned char TmrErrB2;        //timer ritardo errore sonde pressione
  unsigned char TmrErrB3;        //timer ritardo errore sonde pressione
};


	
//definizione stato/comando flag Adox1500.StatoSlave
#define RUNNING       0x01  //Slave sta lavorando
#define CAN_FAULT     0x80  //allarme comunicazione
//define per Adox.CAN_Flag
#define CAN_REQ_STATUS     0x01  //richiesta dal Master dello stato (guasto/OK)


//definisce flag stato Adox1500
#define ADOX_OFF       0x01  // Adox OFF
#define ASS_CICLO1     0x02  // Adox ON in assorbimento ciclo 1
#define CMP_CICLO1     0x04  // Adox ON in compensazione ciclo 1
#define ASS_CICLO2     0x08  // Adox ON in assorbimento ciclo 2
#define CMP_CICLO2     0x10  // Adox ON in compensazione ciclo 2
#define ADOX_PWD       0x20  // Adox OFF per inserimento PWD tecnico
#define ADOX_TARATURA  0x40  // l'Adox � in fase di taratura
#define SET_RAGGIUNTO  0x80  // l'Adox ha raggiunto il set impostato

//define per flag Adox1500.Standby
#define STB_REQUEST    0x01  // richiesta standby
#define STB_START      0x02  // avvio sequenza standby
#define STB_READY      0x04  // sequenza preparazione standby

//definisce flag tipo Adox1500
#define TYPE_1_SELECTED  0x01  // selezionato modo 1
#define TYPE_1           0x02  // set=CO2 Adsorber else N2 generator
#define TYPE_2           0x04  // Adox N2 (F)
#define TYPE_3           0x08  // PSA NT (NTH se O2 in ppm) e SLIM
#define TYPE_4           0x10  // PSA 1/2
#define TYPE_5           0x20  // PSA NTF (Frutta)
#define TYPE_6           0x40  // PSA TSF - come NT ma con compressore -> esistono AL6 e AL7

//definizioni flag Adox1500.Mode
#define ADOX_START    0x01     //comando avvio macchina
#define ADOX_STOP     0x02     //comando arresto macchina
#define ADOX_RUNNING  0x04     //stato macchina: in funzione
#define ADOX_PRE_STOP 0x80     //comando arresto pompe

//definizioni allarmi
#define AL0      0
#define AL1      1
#define AL2      2
#define AL3      3
#define AL4      4
#define AL5      5
#define AL6      6
#define AL7      7
#define AL8      8
#define AL9      9
#define AL10    10
#define AL11    11
#define AL5PSA  12
#define AL12    13
//#define AL13    18
#define AL14    14
#define AL15    15
#define AL16    16
#define AL17    17
#define AL13    18
#define AL20    20
#define AL21    21
#define AL22    22
#define AL24    24
#define AL25    25
#define AL26    26

#define RIT_O2_ALARM  300   // 300 secondi
#define RITALARME      30
#define RIT_AL2        10   //10 secondi
#define RIT_AL5        30   //30 minuti
#define RIT_OFF_R1  36000   //1 ora (3600 * 10 decimi)


void Rele_Work(void);
void InitRele(void);
void SetMioRele(unsigned char comando, unsigned int sectowork, unsigned long secwork, unsigned char rele);
void Adox1500work (void);
void O2_Media (void);
void FlowAverage (void);
void Ricircolo (void);
unsigned char get_DigInput(void);
void InitSpecial (void);
void InitAdox (void);
void CheckIngressi(void);
void CheckTemp(void);
void CheckPress(void);

