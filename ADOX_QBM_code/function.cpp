// per SSP2
#define SSP2_SEL0_0		(LPC_GPIO1->CLR |= (1<<13)) // p1.13 -> low 
#define SSP2_SEL0_1		(LPC_GPIO1->SET |= (1<<13)) // p1.13 -> high 

#define SSP2_SEL1_0		(LPC_GPIO1->CLR |= (1<<14)) // p1.14 -> low 
#define SSP2_SEL1_1		(LPC_GPIO1->SET |= (1<<14)) // p1.14 -> high


// per SSP0
#define SSP0_CS_ON		(LPC_GPIO2->CLR |= (1<<23)) // p2.23 -> low    /* LPC4088 */
#define SSP0_CS_OFF		(LPC_GPIO2->SET |= (1<<23)) // p2.23 -> high   /* LPC4088 */

#define SSP0_SEL0_0		(LPC_GPIO1->CLR |= (1<<9)) // p1.9 -> low    /* LPC4088 */
#define SSP0_SEL0_1		(LPC_GPIO1->SET |= (1<<9)) // p1.9 -> high   /* LPC4088 */

#define SSP0_SEL1_0		(LPC_GPIO1->CLR |= (1<<10)) // p1.10 -> low    /* LPC4088 */
#define SSP0_SEL1_1		(LPC_GPIO1->SET |= (1<<10)) // p1.10 -> high   /* LPC4088 */


#define SSP0_PORT  (LPC_SSP0)   /* LPC4088 */
#define SSP0_CLOCK (100000)

#define SSP2_PORT  (LPC_SSP2)   /* LPC4088 */
#define SSP2_CLOCK (100000)


#define RTD 	1
#define VOLT	2
#define CURR 	3
