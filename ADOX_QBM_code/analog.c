
#include "Drivers/board.h"
#include "Drivers/lpc_pinsel.h"
#include "Drivers/lpc_gpio.h"
#include "Drivers/lpc_rtc.h"
#include "Drivers/lpc_ssp.h"
#include "EasyGui/GuiLib.h"//eliminare solo per debug


SSP_CFG_Type SSP_ConfigStruct;
		
struct timer_y TIM2;
struct timer_x TIM3;
struct timer_x TIM4;
struct timer_x TIM5;

unsigned int ADC_value[5][3] ;	// 3 canali per ogni dispositivo ADC ( che sono 4 sotto la SPI0 ) 
unsigned int ADC_value_ADC5[3] ;	// 2 canali per adc5

// variabili per gestire il valore mediato su 8 campioni ---------------------------------------------------			
unsigned int ADC_value_med[5][3] ;	// 3 canali per ogni dispositivo ADC ( che sono 4 sotto la SPI0 ) 
unsigned long int ADC_value_ADC5_med[3] ;	// 2 canali per adc5
			
unsigned int ADC_value_med_temp[5][3] ;	// 3 canali per ogni dispositivo ADC ( che sono 4 sotto la SPI0 ) 
								
unsigned char ADC_med_cnt[5][3] ;
unsigned char ADC_ADC5_med_cnt[3] ;

//new version
unsigned int ADC_value_ADC5_med_ref[3] ;	// 2 canali per adc5			
unsigned int count_ADC5_med_ref[3] ;
unsigned int ADC_value_ADC5_med_temp[3][16] ;		// 2 canali per adc5
unsigned char ADC_ADC5_med_cnt[3] ;


//			// --------------------------------------------------------------------------
//			// il riferimento per il filtro inventato da Antonio
//			unsigned int ADC_value_med_ref[5][3] ;	// 3 canali per ogni dispositivo ADC ( che sono 4 sotto la SPI0 ) 
//			unsigned int ADC_value_ADC5_med_ref[3] ;	// 3 canali per adc5			
//			
//			
//			unsigned int count_med_ref[5][3] ;	// 3 canali per ogni dispositivo ADC ( che sono 4 sotto la SPI0 ) 
//			unsigned int count_ADC5_med_ref[3] ;
//			// --------------------------------------------------------------------------

//-------------------------------------------------------------
unsigned int ADC_value_read_ADC5(unsigned char num_CH)
{
	return ADC_value_ADC5[num_CH] ;
}


//-------------------------------------------------------------
unsigned int ADC_value_read_ADC5_med(unsigned char num_CH)
{
  return ADC_value_ADC5_med[num_CH] ;
}	
	
	
//-------------------------------------------------------------
unsigned int ADC_value_read(unsigned char num_ADC, unsigned char num_CH)
{	
	return ADC_value[num_ADC][num_CH] ;
}		
	
	
//-------------------------------------------------------------
unsigned int ADC_value_read_med(unsigned char num_ADC, unsigned char num_CH)
{	
	return ADC_value_med[num_ADC][num_CH] ;
}		
	
	
	
	
	
//-------------------------------------------------------------
void ADC_conf_reg_ch_ADC5(unsigned char num_CH, unsigned char mode)
{
	unsigned char spi_data[3] ;	
	unsigned int i ;
		
	PINSEL_ConfigPin (1,  13, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<13), 1);				// pin in Output
	MySetPin(1,13,0) ;		
	

	PINSEL_ConfigPin (1,  14, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<14), 1);				// pin in Output
	MySetPin(1,14,0) ;

	PINSEL_ConfigPin (1, 8, 0);				// SSP2_CS
	GPIO_SetDir(1, (1<<8), 1);				// pin in output	
	MySetPin(1,8,0) ;			
		
		
	spi_data[0] = 0x10 ; 	// communication register to write into config register		
		
	switch ( mode )
		{
		case 1 :		// misura RTD	
		spi_data[1] = 0x14 ;	// Gain = 16, misura unipolare, external reference, AD channel 	
		spi_data[2] = 0x00 ;		
		break ;	
		
		
		case 2 :		// se misura in tensione 0..1V
		spi_data[1] = 0x11 ;	// Gain = 2, misura unipolare, fondo scala 1.25V, internal reference, AD channel 	
		// spi_data[1] = 0x10 ;	// Gain = 2, misura unipolare, fondo scala 1.25V, internal reference, AD channel 	

		spi_data[2] = 0x80 ;						
		break ;

		
		case 3 :	// se misura in corrente 0..20mA
		spi_data[1] = 0x10 ;	// Gain = 1, misura unipolare, fondo scala 2.5V, internal reference, AD channel 	
		spi_data[2] = 0x80 ;				
		break ;
		
		
		case 4 :	// ---------- se misura in tensione con guadagno elevato = 64 ( fondo scala 39,06mV )
			spi_data[1] = 0x1E ; //0x1F : Gain = 128 con BOOST //0x1E : Gain = 64 con BOOST // 0x16 : Gain = 64, senza BOOST; misura unipolare, fondo scala 2.5V, internal reference, AD channel 	
			spi_data[2] = 0x80 ;	// 0x80 unbuffered // 0x90 buffered (lo � di default anche senza imporlo se gain elevato)

			if ( num_CH == 0)
				spi_data[1] |= 0x40 ;		// V bias su AN1-
			else
				spi_data[1] |= 0x80 ;		// V bias su AN2-
		
		break ;				
		
		}
		  
		
	if ( num_CH <= 2)	
		spi_data[2] |= num_CH ;	
		
	for ( i=0;i<3;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
	
	TIM3.msec_timer	= 5 ;
	while ( TIM3.msec_timer != 0) ;
		
	// SSP0_CS_OFF ;	
	MySetPin(1,8,1) ;
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;	
		
	if ( mode == 1 ) // cio� se misura RTD 
		{			
			MySetPin(1,8,0) ;
		
			spi_data[0] = 0x28 ;	// scrivi su IO register
			if ( num_CH == 0)
				spi_data[1] = 0x0A ;	// entrambe le sorgemnti su IOUT1 da 210uA ciascuna per totale di 420uA su IOUT1
			else
				if ( num_CH == 1 ) 
					spi_data[1] = 0x0E ;	// entrambe le sorgemnti su IOUT2 da 210uA ciascuna per totale di 420uA su IOUT2
			
				
			for ( i=0;i<2;i++)
				SSP_SendData(SSP0_PORT, spi_data[i] ) ;
					
				
			TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;	
				
			// SSP0_CS_OFF ;	
			MySetPin(1,8,1) ;				
								
		}	// fine se misura RTD
	
		
		if ( mode == 2 )	// se misura in tensione 0..1V
			{
			MySetPin(1,8,0) ;
		
			spi_data[0] = 0x28 ;	// scrivi su IO register
			spi_data[1] = 0x00 ;

					
			for ( i=0;i<2;i++)
				SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
			TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;		
			
			// SSP0_CS_OFF ;						
			MySetPin(1,8,1) ;		
				
			}	// fine if misura V
		
	
		
		if ( mode == 3 )	// se misura in corrente 0..20mA
			{
			MySetPin(1,8,0) ;
		
			spi_data[0] = 0x28 ;	// scrivi su IO register
			spi_data[1] = 0x00 ;

					
			for ( i=0;i<2;i++)
				SSP_SendData(SSP0_PORT, spi_data[i] ) ;
			
			TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;		
			
			// SSP0_CS_OFF ;						
			MySetPin(1,8,1) ;		
		
			}	// fine if misura in corrente I
			
			
			
		if ( mode == 4 )	// se misura in tensione 0..17 o 39 mV
			{
			MySetPin(1,8,0) ;
		
			spi_data[0] = 0x28 ;	// scrivi su IO register
			spi_data[1] = 0x00 ;

					
			for ( i=0;i<2;i++)
				SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
			TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;		
			
			// SSP0_CS_OFF ;						
			MySetPin(1,8,1) ;		
				
			}	// fine if misura V			
			
			
			
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;		


}


//-------------------------------------------------------------
void ADC_conf_reg_ch(unsigned char num_ADC, unsigned char num_CH, unsigned char mode)
{
	unsigned char spi_data[3] ;	
	unsigned int i ;
		
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
		
	spi_data[0] = 0x10 ; 	// communication register to write into config register		
		
	switch ( mode )
		{
		case 1 :		// misura RTD	
		spi_data[1] = 0x10 ;	// Gain = 1, misura unipolare, external reference, AD channel ( era 14 per Gain=16 ma va fuori scala)	
		spi_data[2] = 0x00 ;	// se voglio leggere il canale 1 devo mettere 0x00 - se voglio leggere il canale 2 devo mettere 0x01	viene fatto passando alla funzione num_ch
		break ;	
		
		
		case 2 :		// se misura in tensione 0..1V
//	  spi_data[1] = 0x11 ;	// Gain = 2, misura unipolare, fondo scala 1.17V, internal reference, AD channel 	
//		spi_data[1] = 0x14 ;	// Gain = 16, misura unipolare, fondo scala 1.17V, internal reference, AD channel 	
#ifdef PROVA_mV
  	spi_data[1] = 0x5F ;	// 0x17 | 0x48 Gain = 128, misura unipolare, fondo scala 1.17V, internal reference, AD channel polarizzazione di A1- per leggere piccoli segnali	
#else
		spi_data[1] = 0x10 ;	// Gain = 1, misura unipolare, fondo scala 1.17V, internal reference, AD channel 	
#endif
		spi_data[2] = 0x80 ;	//metto REFSEL a 1 per riferimento interno					
		break ;

		case 3 :	// se misura in corrente 0..20mA
		spi_data[1] = 0x10 ;	// Gain = 1, misura unipolare, fondo scala 1.17V, internal reference, AD channel 	
		spi_data[2] = 0x80 ;				
		break ;
		
		}
		  
	
	if ( num_CH <= 2)
		spi_data[2] |= num_CH ;	
		
	for ( i=0;i<3;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
	// SSP0_CS_OFF ;	
	MySetPin(2,23,1) ;
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;	
		
	if ( mode == 1 ) // cio� se misura RTD
		{			
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
	
		spi_data[0] = 0x28 ;	// scrivi su IO register
		if ( num_CH == 0)
			spi_data[1] = 0x0A ;	// entrambe le sorgenti su IOUT1 da 210uA ciascuna per totale di 420uA su IOUT1 
		else
			if ( num_CH == 1 ) 
				spi_data[1] = 0x0E ;	// entrambe le sorgenti su IOUT2 da 210uA ciascuna per totale di 420uA su IOUT2 
		
			
		for ( i=0;i<2;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
				
			
		TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;	
			
		// SSP0_CS_OFF ;	
		MySetPin(2,23,1) ;				
			
					
		}	// fine se misura RTD
	
		
		if ( mode == 2 )	// se misura in tensione 0..1V
			{
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
	
		spi_data[0] = 0x28 ;	// scrivi su IO register
		spi_data[1] = 0x00 ;

				
		for ( i=0;i<2;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
	
		TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;		
		
		// SSP0_CS_OFF ;						
		MySetPin(2,23,1) ;		
		
		
		}	// fine if misura V
		
	
		
		if ( mode == 3 )	// se misura in corrente 0..20mA
			{
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
	
		spi_data[0] = 0x28 ;	// scrivi su IO register
		spi_data[1] = 0x00 ;

				
		for ( i=0;i<2;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
		TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;		
		
		// SSP0_CS_OFF ;						
		MySetPin(2,23,1) ;		
		
		}	// fine if misura in corrente I
			
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;		
		
}
	
	
	
	
	
	
//-------------------------------------------------------------
void ADC_set_mode_ADC5(void)
{
	unsigned char spi_data[3] ;	
	unsigned int i ;	
		
		
	PINSEL_ConfigPin (1,  13, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<13), 1);				// pin in Output
	MySetPin(1,13,0) ;		
	

	PINSEL_ConfigPin (1,  14, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<14), 1);				// pin in Output
	MySetPin(1,14,0) ;

	PINSEL_ConfigPin (1, 8, 0);				// SSP2_CS
	GPIO_SetDir(1, (1<<8), 1);				// pin in output	
	MySetPin(1,8,0) ;		
		
		
	spi_data[0] = 0x08 ; 	// communication register to write into mode register		
	spi_data[1] = 0x20 ;	// single conversion, 64KHz internal, 242Hz 8ms conversion time
//  spi_data[2] = 0x01 ;
//	spi_data[2] = 0x08 ;  // 19.6Hz 101ms conversion time - Antonio - aumentando i tempi di conversione la misura risulta pi� stabile
//	spi_data[2] = 0x0A ;  // 16.7Hz 120ms conversion time - Antonio - aumentando i tempi di conversione la misura risulta pi� stabile
//	spi_data[2] = 0x0B ;  // 12.5Hz 160ms conversion time - Antonio - aumentando i tempi di conversione la misura risulta pi� stabile
//	spi_data[2] = 0x0D ;  // 8.33Hz 240ms conversion time - Antonio - aumentando i tempi di conversione la misura risulta pi� stabile
	spi_data[2] = 0x0F ;  // 4.17Hz 480ms conversion time - Antonio - aumentando i tempi di conversione la misura risulta pi� stabile - risultati migliori!!!
		
	for ( i=0;i<3;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
		
	// SSP2_CS rilasciato	
  MySetPin(1,8,1) ;	

	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;			
				
}
	
	
	
	
	
	
//-------------------------------------------------------------
void ADC_set_mode(unsigned char num_ADC)
{
	unsigned char spi_data[3] ;	
	unsigned int i ;
		
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
		
	spi_data[0] = 0x08 ; 	// communication register to write into mode register		
//	spi_data[1] = 0x00 ;	// continuous conversion, 64KHz internal, 470Hz 4ms conversion time // Antonio
	spi_data[1] = 0x20 ;	// single conversion, 64KHz internal, 470Hz 4ms conversion time
#ifdef PROVA_mV
	// - Antonio: da mettere se si usa GAIN elevato e polarizzazione di AIN-
	spi_data[2] = 0x0F ;  // 4.17Hz 101ms conversion time - Antonio - aumentando i tempi di conversione la misura risulta pi� stabile, ma lo stesso non riesco a leggere sotto i 30mV
//  spi_data[2] = 0x07 ;  // 33.2Hz 60ms conversion time - Antonio
#else
	spi_data[2] = 0x01 ;  // 470Hz 4ms conversion time
#endif

		
	for ( i=0;i<3;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
		
	// SSP0_CS_OFF ;	
	// GPIO_SetValue(2, 23 ) ;	
  MySetPin(2,23,1) ;	

	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;		

}		
	
	
	
	
	
	
	
	
	
	
	
void ADC_default_ADC5(void)
{
	unsigned char spi_data[4] ;	
	unsigned int i ;
	
	PINSEL_ConfigPin (1,  13, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<13), 1);				// pin in Output
	MySetPin(1,13,0) ;		
	

	PINSEL_ConfigPin (1,  14, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<14), 1);				// pin in Output
	MySetPin(1,14,0) ;

	PINSEL_ConfigPin (1, 8, 0);				// SSP2_CS
	GPIO_SetDir(1, (1<<8), 1);				// pin in output	
	MySetPin(1,8,0) ;		
	
	
	spi_data[0] = 0xFF ; 	// communication register to write into mode register		
	spi_data[1] = 0xFF ;	// single conversion, 64KHz internal, 242Hz 8ms conversion time
  spi_data[2] = 0xFF ;
	spi_data[3] = 0xFF ;
		
	for ( i=0;i<4;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
	
  MySetPin(1,8,1) ;	

	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;		
	
}
	
	
	
	
	
	
	
	
//-------------------------------------------------------------
void ADC_default(unsigned char num_ADC)
{
	unsigned char spi_data[4] ;	
	unsigned int i ;
		
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
		
	// con quesa sequenza di bit a 1 consecutivi si riporta l'ADC in stato di default
	
	spi_data[0] = 0xFF ; 	// communication register to write into mode register		
	spi_data[1] = 0xFF ;	// single conversion, 64KHz internal, 242Hz 8ms conversion time
  spi_data[2] = 0xFF ;
	spi_data[3] = 0xFF ;
		
	for ( i=0;i<4;i++)
			SSP_SendData(SSP0_PORT, spi_data[i] ) ;
		
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
		
	// SSP0_CS_OFF ;	
	// GPIO_SetValue(2, 23 ) ;	
  MySetPin(2,23,1) ;	

	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;		

}		
	

	
	
//-------------------------------------------------------------
void ADC_read_ADC5(unsigned char num_CH, unsigned char mode)
{
	unsigned char spi_tdata[4] ;	
	unsigned char spi_rdata[4] ;	
		
	SSP_DATA_SETUP_Type sspCfg;	
		
	unsigned int i ;
	unsigned int ADC_result_value ;		
	uint32_t uilTemp ;		
		
		
	PINSEL_ConfigPin (1,  13, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<13), 1);				// pin in Output
	MySetPin(1,13,0) ;		
	

	PINSEL_ConfigPin (1,  14, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<14), 1);				// pin in Output
	MySetPin(1,14,0) ;

	PINSEL_ConfigPin (1, 8, 0);				// SSP2_CS
	GPIO_SetDir(1, (1<<8), 1);				// pin in output	
	MySetPin(1,8,0) ;
  
	spi_tdata[0] = 0x58 ;

  sspCfg.tx_data = spi_tdata;
  sspCfg.rx_data = spi_rdata;
  sspCfg.length  = 1; 

  TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
 
  SSP_ReadWrite (SSP0_PORT, &sspCfg, SSP_TRANSFER_POLLING);


	spi_tdata[0] = 0xFF ;	
	spi_tdata[1] = 0xFF ;	
	spi_rdata[0] = 0x00 ;	  
  spi_rdata[1] = 0x00 ;		
		
  sspCfg.tx_data = spi_tdata;
  sspCfg.rx_data = spi_rdata;
  sspCfg.length  = 2; 
  SSP_ReadWrite (SSP0_PORT, &sspCfg, SSP_TRANSFER_POLLING);


		
	TIM3.msec_timer	= 2 ;
	while ( TIM3.msec_timer != 0) ;		
		
	// SSP0_CS_OFF ;	
	// GPIO_SetValue(2, 23 ) ;
	MySetPin(1,8,1) ;
	
	ADC_result_value = spi_rdata[0] ;
	ADC_result_value = ADC_result_value << 8 ;	
	ADC_result_value = ADC_result_value + spi_rdata[1] ;	
	
		
	ADC_value_ADC5[num_CH]= ADC_result_value ;
	
//OLD VERSION	
//	ADC_value_ADC5_med_temp[num_CH] += ADC_result_value ;
//	ADC_ADC5_med_cnt[num_CH] += 1 ;
//	
//	if ( ADC_ADC5_med_cnt[num_CH] == ADC_CAMPIONI_ADC_5 ){
//		ADC_value_ADC5_med[num_CH] = 	(unsigned int)((float)(ADC_value_ADC5_med_temp[num_CH]) / ADC_CAMPIONI_ADC_5);
//		ADC_ADC5_med_cnt[num_CH] = 0 ;	
//		ADC_value_ADC5_med_temp[num_CH] = 0 ;	
//	}

//NEW VERSION come per Analizzatore
	// Filtro di Antonio con esclusione valori troppo discostanti dal precedente
	if (  ( ( LOGIC_ABS(((signed long)(ADC_value_ADC5_med_ref[num_CH])-(signed long)(ADC_result_value))) < 5000) || (count_ADC5_med_ref[num_CH] >= 3 ) ) && (ADC_value_ADC5[num_CH] != 0xFFFF) )
			{
			count_ADC5_med_ref[num_CH] = 0 ;	
				
			ADC_value_ADC5_med_temp[num_CH][ADC_ADC5_med_cnt[num_CH]] = ADC_result_value ;
			
			ADC_ADC5_med_cnt[num_CH] += 1 ; 
			if ( ADC_ADC5_med_cnt[num_CH] >= ADC_CAMPIONI_ADC_5)
				ADC_ADC5_med_cnt[num_CH] = 0 ;
			
			uilTemp = 0 ;
			for ( i=0;i<ADC_CAMPIONI_ADC_5;i++)
				{
				uilTemp = uilTemp + ADC_value_ADC5_med_temp[num_CH][i] ; 	
				}
			uilTemp = uilTemp / ADC_CAMPIONI_ADC_5 ;
			ADC_value_ADC5_med[num_CH] = (uint16_t)uilTemp ;			
			}
	else
		{
		count_ADC5_med_ref[num_CH] = count_ADC5_med_ref[num_CH] + 1 ;	
			
		}				
								
	
	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
}
	
	
	
//-------------------------------------------------------------
void ADC_read(unsigned char num_ADC, unsigned char num_CH, unsigned char mode)
{
	unsigned char spi_tdata[4] ;	
	unsigned char spi_rdata[4] ;
		
	SSP_DATA_SETUP_Type sspCfg;	
		
	unsigned int i ;
	unsigned int ADC_result_value ;
		
	// seleziona ADC e canale da misurare 	
//	ADC_conf_reg_ch(num_ADC, num_CH, mode )	;
		
		
		
		switch ( num_ADC ) {
			case 1 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;
			
			case 2 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_0 ;
					// GPIO_ClearValue(1, 10 ) ;
					MySetPin(1,10,0) ;
			
					// GPIO_ClearValue(2, 23 ) ;					
					// SSP0_CS_ON ;	
					MySetPin(2,23,0 ) ;			
			break ;	

			case 3 :
					// SSP0_SEL0_0 ;
					// GPIO_ClearValue(1, 9 ) ;
					MySetPin(1,9,0) ;
						
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
						
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;
					MySetPin(2,23,0 ) ;
			break ;

			case 4 :
					// SSP0_SEL0_1 ;
					// GPIO_SetValue(1, 9 ) ;
					MySetPin(1,9,1) ;
			
					// SSP0_SEL1_1 ;
					// GPIO_SetValue(1, 10 ) ;
					MySetPin(1,10,1) ;
			
					// SSP0_CS_ON ;	
					// GPIO_ClearValue(2, 23 ) ;	
					MySetPin(2,23,0 ) ;
			
			break ;
			
			case 5 :
			break ;			
		}
		
		
		

  

  
	spi_tdata[0] = 0x58 ;

  sspCfg.tx_data = spi_tdata;
  sspCfg.rx_data = spi_rdata;
  sspCfg.length  = 1; 

  TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
 
  SSP_ReadWrite (SSP0_PORT, &sspCfg, SSP_TRANSFER_POLLING);


	spi_tdata[0] = 0xFF ;	
	spi_tdata[1] = 0xFF ;	
	spi_rdata[0] = 0x00 ;	  
  spi_rdata[1] = 0x00 ;		
		
  sspCfg.tx_data = spi_tdata;
  sspCfg.rx_data = spi_rdata;
  sspCfg.length  = 2; 
  SSP_ReadWrite (SSP0_PORT, &sspCfg, SSP_TRANSFER_POLLING);

	
	// aspetta ingresso RDY � basso	
	/*	
	spi_data[0] = 0x58 ; 	// communication register to read into data register ADC conversion for selected channel 
		
	SSP_SendData(SSP0_PORT, spi_data[0] ) ;	

	TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;				
		
		
	// azzera il buffer per la ricezione del dato di conversione ADC
	spi_data[1] = 0x00 ;	  
  spi_data[2] = 0x00 ;

	SSP_SendData(SSP0_PORT, spi_data[1] ) ;		
	spi_data[1] = SSP_ReceiveData(SSP0_PORT) ;
	SSP_SendData(SSP0_PORT, spi_data[2] ) ;	
	spi_data[2] = SSP_ReceiveData(SSP0_PORT) ;	
	*/
		
		
	TIM3.msec_timer	= 2 ;
	while ( TIM3.msec_timer != 0) ;		
		
	// SSP0_CS_OFF ;	
	// GPIO_SetValue(2, 23 ) ;
	MySetPin(2,23,1) ;
	
	ADC_result_value = spi_rdata[0] ;
	ADC_result_value = ADC_result_value << 8 ;	
	ADC_result_value = ADC_result_value + spi_rdata[1] ;	
		
		
	ADC_value[num_ADC][num_CH] = ADC_result_value ;
	
	
	ADC_value_med_temp[num_ADC][num_CH] += ADC_result_value ;
	
	ADC_med_cnt[num_ADC][num_CH] += 1 ;
	
	if ( ADC_med_cnt[num_ADC][num_CH] == ADC_CAMPIONI )
		{
		ADC_value_med[num_ADC][num_CH] = ADC_value_med_temp[num_ADC][num_CH] / ADC_CAMPIONI ;
		ADC_value_med_temp[num_ADC][num_CH] = 0 ;
		ADC_med_cnt[num_ADC][num_CH] = 0 ;			
		}

		TIM3.msec_timer	= 1 ;
	while ( TIM3.msec_timer != 0) ;
		
}
	
	
	
//-------------------------------------------------------------
void ADC_CS_init_ADC5(void)
{
	unsigned int uiI, uiL ;
  		
	for ( uiI=0; uiI<3; uiI++ )
		for ( uiL=0; uiL<8; uiL++ )//new
				{
				ADC_value_ADC5_med[uiI] = 0 ;
				ADC_value_ADC5_med_temp[uiI][uiL]= 0 ;
				ADC_ADC5_med_cnt[uiI] = 0;
					
				ADC_value_ADC5_med_ref[uiI] = 0 ;
				count_ADC5_med_ref[uiI] = 0 ;	
					
				}			
//old version
//				{
//				ADC_value_ADC5_med[uiI] = 0 ;
//				ADC_value_ADC5_med_temp[uiI]= 0 ;
//				ADC_ADC5_med_cnt[uiI] = 0;
//				}				
				
		
	PINSEL_ConfigPin (1,  13, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<13), 1);				// pin in Output
	MySetPin(1,13,0) ;		
	

	PINSEL_ConfigPin (1,  14, 0);			// SSP2_SEL0
	GPIO_SetDir(1, (1<<14), 1);				// pin in Output
	MySetPin(1,14,0) ;

	PINSEL_ConfigPin (1, 8, 0);				// SSP2_CS
	GPIO_SetDir(1, (1<<8), 1);				// pin in output	
	MySetPin(1,8,1) ;	
											
}
	
	
//-------------------------------------------------------------
void ADC_SSP_init(void)
{		
	unsigned int uiI, uiL ;

  		
	for ( uiI=0; uiI<5; uiI++ )
			for ( uiL=0; uiL<3; uiL++)
				{
				ADC_value_med[uiI][uiL] = 0 ;
				ADC_value_med_temp[uiI][uiL] = 0 ;
				ADC_med_cnt[uiI][uiL] = 0;
				}														
				
	// provisiorio ma per inibire SSP2_CS che va ad interaginre con il 4� ADC 	
//	PINSEL_ConfigPin (1, 8, 0);	
//	GPIO_SetDir(1, 8, 1);	
//	GPIO_SetValue(1, 8 ) ;	
	// GPIO_ClearValue(1, 8 ) ;	
	// GPIO_SetValue(1, 8 ) ;
		
		
	// configura i pin per la SPI0 o SSPI0 	
	PINSEL_ConfigPin (2, 22, 2);				// SSP0_SCK
		
	// PINSEL_ConfigPin (2, 23, 2);			// SSP0_CS ma con PIN in output 	
	PINSEL_ConfigPin (2, 23, 0);	
	GPIO_SetDir(2, (1<<23), 1);					// pin in output	
	MySetPin(2,23,1) ;	
	
		
	PINSEL_ConfigPin (2, 26, 2);			// SSP0_MISO
	PINSEL_ConfigPin (2, 27, 2);			// SSP0_MOSI
		
		
	PINSEL_ConfigPin (1,  9, 0);			// SSP0_SEL0
	GPIO_SetDir(1, (1<<9), 1);							// pin in Output
	GPIO_SetValue(1, 9 ) ;
	MySetPin(1,9,0) ;	
	// GPIO_ClearValue(1, 9 ) ;
	// SSP0_SEL0_1 ;
		
		
		
	PINSEL_ConfigPin (1, 10, 0);			// SSP0_SEL1
	GPIO_SetDir(1, (1<<10) , 1);						// pin in Output	
	MySetPin(1,10,0) ;
	// GPIO_ClearValue(1, 10 ) ;
	// SSP0_SEL1_1 ;	
		
		
  // SSP0_CS_OFF;
	GPIO_SetValue(2, 23 ) ;
	MySetPin(2,23,0) ;

	

	// initialize SSP configuration structure to default
	SSP_ConfigStructInit(&SSP_ConfigStruct);

  // set clock rate
  SSP_ConfigStruct.ClockRate = SSP0_CLOCK;
  SSP_ConfigStruct.CPHA = SSP_CPHA_FIRST;
  SSP_ConfigStruct.CPOL = SSP_CPOL_HI;
	SSP_ConfigStruct.Databit = SSP_DATABIT_8;
	SSP_ConfigStruct.Mode = SSP_MASTER_MODE ;
  // SSP_ConfigStruct.FrameFormat = SSP_FRAME_SPI ; 		

	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(SSP0_PORT, &SSP_ConfigStruct);

	// Enable SSP peripheral
	SSP_Cmd(SSP0_PORT, ENABLE);	

}

