
#include "Drivers/board.h"
#include "Drivers/lpc_uart.h"
#include "Drivers/lpc_can.h"
#include "Drivers/lpc_pinsel.h"
#include "Drivers/lpc_rtc.h"
#include "EasyGui/GuiLib.h"//eliminare solo per debug

// external struct 
  // RTC
	extern RTC_TIME_Type RTCFullTime;
  extern struct adox Adox1500;
  extern struct e2prom E2prom[];
  extern struct strele StRele[];
  extern signed int sonde [];
  extern struct adox Adox1500;
  extern uint8_t LED_output;

  unsigned int CANcount[N_CANOPEN],CANcountSlave;
  unsigned char operational=0;

  unsigned char tmr_NMT_message;
	
 /*************************************************************************************************
  i file "communication" sono divisi in 4 settori:
	
  1. UART_3: RS485 Modbus Slave - COM 1 (A1-B1) nella serigrafia del dispositivo 
  2. UART_2: RS485 Modbus Master - COM 2 (A2-B2) nella serigrafia del dispositivo 
  3. CAN_1:  CANOpen Master
  4. CAN_2:  CANOpen Slave
	
	in ciascun settore si trovano le strutture, le variabili, le definizioni e le funzioni relative
	
 *************************************************************************************************/

//  1. 2. UART: configurazione di base delle 2 seriali - strutture, variabili

// **********************************************************************************************************
// RS485 - UART 2
	#define _LPC_UART_2           UART_2
	#define _UART_IRQ_2           UART2_IRQn
	#define UART_2_IRQHander      UART2_IRQHandler
	
	/* buffer size definition */
	#define UART_RING_BUFSIZE 256

	#define NUM_OF_WAITING      5

	/* Buf mask */
	#define __BUF_MASK (UART_RING_BUFSIZE-1)
	/* Check buf is full or not */
	#define __BUF_IS_FULL(head, tail) ((tail&__BUF_MASK)==((head+1)&__BUF_MASK))
	/* Check buf will be full in next receiving or not */
	#define __BUF_WILL_FULL(head, tail) ((tail&__BUF_MASK)==((head+2)&__BUF_MASK))
	/* Check buf is empty */
	#define __BUF_IS_EMPTY(head, tail) ((head&__BUF_MASK)==(tail&__BUF_MASK))
	/* Reset buf */
	#define __BUF_RESET(bufidx) (bufidx=0)
	#define __BUF_INCR(bufidx)  (bufidx=(bufidx+1)&__BUF_MASK)

	/** @brief UART Ring buffer structure */
	typedef struct
	{
			__IO uint32_t tx_head;                /*!< UART Tx ring buffer head index */
			__IO uint32_t tx_tail;                /*!< UART Tx ring buffer tail index */
			__IO uint32_t rx_head;                /*!< UART Rx ring buffer head index */
			__IO uint32_t rx_tail;                /*!< UART Rx ring buffer tail index */
			__IO uint8_t  tx[UART_RING_BUFSIZE];  /*!< UART Tx data ring buffer */
			__IO uint8_t  rx[UART_RING_BUFSIZE];  /*!< UART Rx data ring buffer */
	} UART_RING_BUFFER_T;
		
	/************************** PRIVATE VARIABLES *************************/
	// UART Ring buffer
	UART_RING_BUFFER_T rb2;

	// UART Configuration structure variable
	UART_CFG_Type UARTConfigStruct;
	// UART FIFO configuration Struct variable
	UART_FIFO_CFG_Type UARTFIFOConfigStruct;
	// RS485 configuration
	UART1_RS485_CTRLCFG_Type rs485cfg;
	// Temp. data
	uint32_t idx, retryCnt = 0;
	volatile uint32_t len;
	uint8_t buffer[10];
//	int32_t exit_flag, addr_toggle;
	volatile uint8_t rs485_rx_msg[8];
	uint8_t rs485_tx_msg[8];
	volatile unsigned int rs485_count = 0;
	volatile unsigned int valid_rs485_frame = 0;
	
	volatile unsigned int rs485_data_array[RX_BUFFER_RS485][7];
	volatile unsigned int rs485_buffer_count = 0;
	volatile unsigned int rs485_rx_timeout   = 0;

// **********************************************************************************************************
// RS485 - UART 3
	#define _LPC_UART_3           UART_3
	#define _UART_IRQ_3           UART3_IRQn
	#define UART_3_IRQHander      UART3_IRQHandler

	// UART Ring buffer
	UART_RING_BUFFER_T rb3;

// **********************************************************************************************************

	// per seriale UART verso le periferiche
	uint8_t rs485_rx_com_msg[100];	
	uint8_t rs485_rx_ndx = 0;
	uint8_t rs485_tx_com_msg[100];	
	uint8_t rs485_tx_ndx = 0;
  uint8_t PackFull;

  unsigned int Id_msg, Id_function, Id_Lenght, IdWritedReg, Set_data;
  unsigned int MsgSended;
  unsigned char RS485_Slave_Address; //indirizzo scheda per RS485 Slave
  unsigned char RS485_Master_Address; //indirizzo scheda per RS485 Master
  unsigned char Crc16_Low, Crc16_Hi;
  volatile unsigned int Ready2Send, BadFrame, TmrRxNextByte;
  unsigned char RxError=0;
	unsigned char crc_table[512];
	
	unsigned char RS485_Port_Sel; //seleziona la porta come Slave o come Master

  struct modbus Modbus;

/***********************************************************************
 * @brief		InitCrc16Table Modbus			
	 \param [in] 
 **********************************************************************/
void Modbus_InitCrc16Table (void){
	
	crc_table[0] = 0x0;
	crc_table[1] = 0xC1;
	crc_table[2] = 0x81;
	crc_table[3] = 0x40;
	crc_table[4] = 0x1;
	crc_table[5] = 0xC0;
	crc_table[6] = 0x80;
	crc_table[7] = 0x41;
	crc_table[8] = 0x1;
	crc_table[9] = 0xC0;
	crc_table[10] = 0x80;
	crc_table[11] = 0x41;
	crc_table[12] = 0x0;
	crc_table[13] = 0xC1;
	crc_table[14] = 0x81;
	crc_table[15] = 0x40;
	crc_table[16] = 0x1;
	crc_table[17] = 0xC0;
	crc_table[18] = 0x80;
	crc_table[19] = 0x41;
	crc_table[20] = 0x0;
	crc_table[21] = 0xC1;
	crc_table[22] = 0x81;
	crc_table[23] = 0x40;
	crc_table[24] = 0x0;
	crc_table[25] = 0xC1;
	crc_table[26] = 0x81;
	crc_table[27] = 0x40;
	crc_table[28] = 0x1;
	crc_table[29] = 0xC0;
	crc_table[30] = 0x80;
	crc_table[31] = 0x41;
	crc_table[32] = 0x1;
	crc_table[33] = 0xC0;
	crc_table[34] = 0x80;
	crc_table[35] = 0x41;
	crc_table[36] = 0x0;
	crc_table[37] = 0xC1;
	crc_table[38] = 0x81;
	crc_table[39] = 0x40;
	crc_table[40] = 0x0;
	crc_table[41] = 0xC1;
	crc_table[42] = 0x81;
	crc_table[43] = 0x40;
	crc_table[44] = 0x1;
	crc_table[45] = 0xC0;
	crc_table[46] = 0x80;
	crc_table[47] = 0x41;
	crc_table[48] = 0x0;
	crc_table[49] = 0xC1;
	crc_table[50] = 0x81;
	crc_table[51] = 0x40;
	crc_table[52] = 0x1;
	crc_table[53] = 0xC0;
	crc_table[54] = 0x80;
	crc_table[55] = 0x41;
	crc_table[56] = 0x1;
	crc_table[57] = 0xC0;
	crc_table[58] = 0x80;
	crc_table[59] = 0x41;
	crc_table[60] = 0x0;
	crc_table[61] = 0xC1;
	crc_table[62] = 0x81;
	crc_table[63] = 0x40;
	crc_table[64] = 0x1;
	crc_table[65] = 0xC0;
	crc_table[66] = 0x80;
	crc_table[67] = 0x41;
	crc_table[68] = 0x0;
	crc_table[69] = 0xC1;
	crc_table[70] = 0x81;
	crc_table[71] = 0x40;
	crc_table[72] = 0x0;
	crc_table[73] = 0xC1;
	crc_table[74] = 0x81;
	crc_table[75] = 0x40;
	crc_table[76] = 0x1;
	crc_table[77] = 0xC0;
	crc_table[78] = 0x80;
	crc_table[79] = 0x41;
	crc_table[80] = 0x0;
	crc_table[81] = 0xC1;
	crc_table[82] = 0x81;
	crc_table[83] = 0x40;
	crc_table[84] = 0x1;
	crc_table[85] = 0xC0;
	crc_table[86] = 0x80;
	crc_table[87] = 0x41;
	crc_table[88] = 0x1;
	crc_table[89] = 0xC0;
	crc_table[90] = 0x80;
	crc_table[91] = 0x41;
	crc_table[92] = 0x0;
	crc_table[93] = 0xC1;
	crc_table[94] = 0x81;
	crc_table[95] = 0x40;
	crc_table[96] = 0x0;
	crc_table[97] = 0xC1;
	crc_table[98] = 0x81;
	crc_table[99] = 0x40;
	crc_table[100] = 0x1;
	crc_table[101] = 0xC0;
	crc_table[102] = 0x80;
	crc_table[103] = 0x41;
	crc_table[104] = 0x1;
	crc_table[105] = 0xC0;
	crc_table[106] = 0x80;
	crc_table[107] = 0x41;
	crc_table[108] = 0x0;
	crc_table[109] = 0xC1;
	crc_table[110] = 0x81;
	crc_table[111] = 0x40;
	crc_table[112] = 0x1;
	crc_table[113] = 0xC0;
	crc_table[114] = 0x80;
	crc_table[115] = 0x41;
	crc_table[116] = 0x0;
	crc_table[117] = 0xC1;
	crc_table[118] = 0x81;
	crc_table[119] = 0x40;
	crc_table[120] = 0x0;
	crc_table[121] = 0xC1;
	crc_table[122] = 0x81;
	crc_table[123] = 0x40;
	crc_table[124] = 0x1;
	crc_table[125] = 0xC0;
	crc_table[126] = 0x80;
	crc_table[127] = 0x41;
	crc_table[128] = 0x1;
	crc_table[129] = 0xC0;
	crc_table[130] = 0x80;
	crc_table[131] = 0x41;
	crc_table[132] = 0x0;
	crc_table[133] = 0xC1;
	crc_table[134] = 0x81;
	crc_table[135] = 0x40;
	crc_table[136] = 0x0;
	crc_table[137] = 0xC1;
	crc_table[138] = 0x81;
	crc_table[139] = 0x40;
	crc_table[140] = 0x1;
	crc_table[141] = 0xC0;
	crc_table[142] = 0x80;
	crc_table[143] = 0x41;
	crc_table[144] = 0x0;
	crc_table[145] = 0xC1;
	crc_table[146] = 0x81;
	crc_table[147] = 0x40;
	crc_table[148] = 0x1;
	crc_table[149] = 0xC0;
	crc_table[150] = 0x80;
	crc_table[151] = 0x41;
	crc_table[152] = 0x1;
	crc_table[153] = 0xC0;
	crc_table[154] = 0x80;
	crc_table[155] = 0x41;
	crc_table[156] = 0x0;
	crc_table[157] = 0xC1;
	crc_table[158] = 0x81;
	crc_table[159] = 0x40;
	crc_table[160] = 0x0;
	crc_table[161] = 0xC1;
	crc_table[162] = 0x81;
	crc_table[163] = 0x40;
	crc_table[164] = 0x1;
	crc_table[165] = 0xC0;
	crc_table[166] = 0x80;
	crc_table[167] = 0x41;
	crc_table[168] = 0x1;
	crc_table[169] = 0xC0;
	crc_table[170] = 0x80;
	crc_table[171] = 0x41;
	crc_table[172] = 0x0;
	crc_table[173] = 0xC1;
	crc_table[174] = 0x81;
	crc_table[175] = 0x40;
	crc_table[176] = 0x1;
	crc_table[177] = 0xC0;
	crc_table[178] = 0x80;
	crc_table[179] = 0x41;
	crc_table[180] = 0x0;
	crc_table[181] = 0xC1;
	crc_table[182] = 0x81;
	crc_table[183] = 0x40;
	crc_table[184] = 0x0;
	crc_table[185] = 0xC1;
	crc_table[186] = 0x81;
	crc_table[187] = 0x40;
	crc_table[188] = 0x1;
	crc_table[189] = 0xC0;
	crc_table[190] = 0x80;
	crc_table[191] = 0x41;
	crc_table[192] = 0x0;
	crc_table[193] = 0xC1;
	crc_table[194] = 0x81;
	crc_table[195] = 0x40;
	crc_table[196] = 0x1;
	crc_table[197] = 0xC0;
	crc_table[198] = 0x80;
	crc_table[199] = 0x41;
	crc_table[200] = 0x1;
	crc_table[201] = 0xC0;
	crc_table[202] = 0x80;
	crc_table[203] = 0x41;
	crc_table[204] = 0x0;
	crc_table[205] = 0xC1;
	crc_table[206] = 0x81;
	crc_table[207] = 0x40;
	crc_table[208] = 0x1;
	crc_table[209] = 0xC0;
	crc_table[210] = 0x80;
	crc_table[211] = 0x41;
	crc_table[212] = 0x0;
	crc_table[213] = 0xC1;
	crc_table[214] = 0x81;
	crc_table[215] = 0x40;
	crc_table[216] = 0x0;
	crc_table[217] = 0xC1;
	crc_table[218] = 0x81;
	crc_table[219] = 0x40;
	crc_table[220] = 0x1;
	crc_table[221] = 0xC0;
	crc_table[222] = 0x80;
	crc_table[223] = 0x41;
	crc_table[224] = 0x1;
	crc_table[225] = 0xC0;
	crc_table[226] = 0x80;
	crc_table[227] = 0x41;
	crc_table[228] = 0x0;
	crc_table[229] = 0xC1;
	crc_table[230] = 0x81;
	crc_table[231] = 0x40;
	crc_table[232] = 0x0;
	crc_table[233] = 0xC1;
	crc_table[234] = 0x81;
	crc_table[235] = 0x40;
	crc_table[236] = 0x1;
	crc_table[237] = 0xC0;
	crc_table[238] = 0x80;
	crc_table[239] = 0x41;
	crc_table[240] = 0x0;
	crc_table[241] = 0xC1;
	crc_table[242] = 0x81;
	crc_table[243] = 0x40;
	crc_table[244] = 0x1;
	crc_table[245] = 0xC0;
	crc_table[246] = 0x80;
	crc_table[247] = 0x41;
	crc_table[248] = 0x1;
	crc_table[249] = 0xC0;
	crc_table[250] = 0x80;
	crc_table[251] = 0x41;
	crc_table[252] = 0x0;
	crc_table[253] = 0xC1;
	crc_table[254] = 0x81;
	crc_table[255] = 0x40;
	crc_table[256] = 0x0;
	crc_table[257] = 0xC0;
	crc_table[258] = 0xC1;
	crc_table[259] = 0x1;
	crc_table[260] = 0xC3;
	crc_table[261] = 0x3;
	crc_table[262] = 0x2;
	crc_table[263] = 0xC2;
	crc_table[264] = 0xC6;
	crc_table[265] = 0x6;
	crc_table[266] = 0x7;
	crc_table[267] = 0xC7;
	crc_table[268] = 0x5;
	crc_table[269] = 0xC5;
	crc_table[270] = 0xC4;
	crc_table[271] = 0x4;
	crc_table[272] = 0xCC;
	crc_table[273] = 0xC;
	crc_table[274] = 0xD;
	crc_table[275] = 0xCD;
	crc_table[276] = 0xF;
	crc_table[277] = 0xCF;
	crc_table[278] = 0xCE;
	crc_table[279] = 0xE;
	crc_table[280] = 0xA;
	crc_table[281] = 0xCA;
	crc_table[282] = 0xCB;
	crc_table[283] = 0xB;
	crc_table[284] = 0xC9;
	crc_table[285] = 0x9;
	crc_table[286] = 0x8;
	crc_table[287] = 0xC8;
	crc_table[288] = 0xD8;
	crc_table[289] = 0x18;
	crc_table[290] = 0x19;
	crc_table[291] = 0xD9;
	crc_table[292] = 0x1B;
	crc_table[293] = 0xDB;
	crc_table[294] = 0xDA;
	crc_table[295] = 0x1A;
	crc_table[296] = 0x1E;
	crc_table[297] = 0xDE;
	crc_table[298] = 0xDF;
	crc_table[299] = 0x1F;
	crc_table[300] = 0xDD;
	crc_table[301] = 0x1D;
	crc_table[302] = 0x1C;
	crc_table[303] = 0xDC;
	crc_table[304] = 0x14;
	crc_table[305] = 0xD4;
	crc_table[306] = 0xD5;
	crc_table[307] = 0x15;
	crc_table[308] = 0xD7;
	crc_table[309] = 0x17;
	crc_table[310] = 0x16;
	crc_table[311] = 0xD6;
	crc_table[312] = 0xD2;
	crc_table[313] = 0x12;
	crc_table[314] = 0x13;
	crc_table[315] = 0xD3;
	crc_table[316] = 0x11;
	crc_table[317] = 0xD1;
	crc_table[318] = 0xD0;
	crc_table[319] = 0x10;
	crc_table[320] = 0xF0;
	crc_table[321] = 0x30;
	crc_table[322] = 0x31;
	crc_table[323] = 0xF1;
	crc_table[324] = 0x33;
	crc_table[325] = 0xF3;
	crc_table[326] = 0xF2;
	crc_table[327] = 0x32;
	crc_table[328] = 0x36;
	crc_table[329] = 0xF6;
	crc_table[330] = 0xF7;
	crc_table[331] = 0x37;
	crc_table[332] = 0xF5;
	crc_table[333] = 0x35;
	crc_table[334] = 0x34;
	crc_table[335] = 0xF4;
	crc_table[336] = 0x3C;
	crc_table[337] = 0xFC;
	crc_table[338] = 0xFD;
	crc_table[339] = 0x3D;
	crc_table[340] = 0xFF;
	crc_table[341] = 0x3F;
	crc_table[342] = 0x3E;
	crc_table[343] = 0xFE;
	crc_table[344] = 0xFA;
	crc_table[345] = 0x3A;
	crc_table[346] = 0x3B;
	crc_table[347] = 0xFB;
	crc_table[348] = 0x39;
	crc_table[349] = 0xF9;
	crc_table[350] = 0xF8;
	crc_table[351] = 0x38;
	crc_table[352] = 0x28;
	crc_table[353] = 0xE8;
	crc_table[354] = 0xE9;
	crc_table[355] = 0x29;
	crc_table[356] = 0xEB;
	crc_table[357] = 0x2B;
	crc_table[358] = 0x2A;
	crc_table[359] = 0xEA;
	crc_table[360] = 0xEE;
	crc_table[361] = 0x2E;
	crc_table[362] = 0x2F;
	crc_table[363] = 0xEF;
	crc_table[364] = 0x2D;
	crc_table[365] = 0xED;
	crc_table[366] = 0xEC;
	crc_table[367] = 0x2C;
	crc_table[368] = 0xE4;
	crc_table[369] = 0x24;
	crc_table[370] = 0x25;
	crc_table[371] = 0xE5;
	crc_table[372] = 0x27;
	crc_table[373] = 0xE7;
	crc_table[374] = 0xE6;
	crc_table[375] = 0x26;
	crc_table[376] = 0x22;
	crc_table[377] = 0xE2;
	crc_table[378] = 0xE3;
	crc_table[379] = 0x23;
	crc_table[380] = 0xE1;
	crc_table[381] = 0x21;
	crc_table[382] = 0x20;
	crc_table[383] = 0xE0;
	crc_table[384] = 0xA0;
	crc_table[385] = 0x60;
	crc_table[386] = 0x61;
	crc_table[387] = 0xA1;
	crc_table[388] = 0x63;
	crc_table[389] = 0xA3;
	crc_table[390] = 0xA2;
	crc_table[391] = 0x62;
	crc_table[392] = 0x66;
	crc_table[393] = 0xA6;
	crc_table[394] = 0xA7;
	crc_table[395] = 0x67;
	crc_table[396] = 0xA5;
	crc_table[397] = 0x65;
	crc_table[398] = 0x64;
	crc_table[399] = 0xA4;
	crc_table[400] = 0x6C;
	crc_table[401] = 0xAC;
	crc_table[402] = 0xAD;
	crc_table[403] = 0x6D;
	crc_table[404] = 0xAF;
	crc_table[405] = 0x6F;
	crc_table[406] = 0x6E;
	crc_table[407] = 0xAE;
	crc_table[408] = 0xAA;
	crc_table[409] = 0x6A;
	crc_table[410] = 0x6B;
	crc_table[411] = 0xAB;
	crc_table[412] = 0x69;
	crc_table[413] = 0xA9;
	crc_table[414] = 0xA8;
	crc_table[415] = 0x68;
	crc_table[416] = 0x78;
	crc_table[417] = 0xB8;
	crc_table[418] = 0xB9;
	crc_table[419] = 0x79;
	crc_table[420] = 0xBB;
	crc_table[421] = 0x7B;
	crc_table[422] = 0x7A;
	crc_table[423] = 0xBA;
	crc_table[424] = 0xBE;
	crc_table[425] = 0x7E;
	crc_table[426] = 0x7F;
	crc_table[427] = 0xBF;
	crc_table[428] = 0x7D;
	crc_table[429] = 0xBD;
	crc_table[430] = 0xBC;
	crc_table[431] = 0x7C;
	crc_table[432] = 0xB4;
	crc_table[433] = 0x74;
	crc_table[434] = 0x75;
	crc_table[435] = 0xB5;
	crc_table[436] = 0x77;
	crc_table[437] = 0xB7;
	crc_table[438] = 0xB6;
	crc_table[439] = 0x76;
	crc_table[440] = 0x72;
	crc_table[441] = 0xB2;
	crc_table[442] = 0xB3;
	crc_table[443] = 0x73;
	crc_table[444] = 0xB1;
	crc_table[445] = 0x71;
	crc_table[446] = 0x70;
	crc_table[447] = 0xB0;
	crc_table[448] = 0x50;
	crc_table[449] = 0x90;
	crc_table[450] = 0x91;
	crc_table[451] = 0x51;
	crc_table[452] = 0x93;
	crc_table[453] = 0x53;
	crc_table[454] = 0x52;
	crc_table[455] = 0x92;
	crc_table[456] = 0x96;
	crc_table[457] = 0x56;
	crc_table[458] = 0x57;
	crc_table[459] = 0x97;
	crc_table[460] = 0x55;
	crc_table[461] = 0x95;
	crc_table[462] = 0x94;
	crc_table[463] = 0x54;
	crc_table[464] = 0x9C;
	crc_table[465] = 0x5C;
	crc_table[466] = 0x5D;
	crc_table[467] = 0x9D;
	crc_table[468] = 0x5F;
	crc_table[469] = 0x9F;
	crc_table[470] = 0x9E;
	crc_table[471] = 0x5E;
	crc_table[472] = 0x5A;
	crc_table[473] = 0x9A;
	crc_table[474] = 0x9B;
	crc_table[475] = 0x5B;
	crc_table[476] = 0x99;
	crc_table[477] = 0x59;
	crc_table[478] = 0x58;
	crc_table[479] = 0x98;
	crc_table[480] = 0x88;
	crc_table[481] = 0x48;
	crc_table[482] = 0x49;
	crc_table[483] = 0x89;
	crc_table[484] = 0x4B;
	crc_table[485] = 0x8B;
	crc_table[486] = 0x8A;
	crc_table[487] = 0x4A;
	crc_table[488] = 0x4E;
	crc_table[489] = 0x8E;
	crc_table[490] = 0x8F;
	crc_table[491] = 0x4F;
	crc_table[492] = 0x8D;
	crc_table[493] = 0x4D;
	crc_table[494] = 0x4C;
	crc_table[495] = 0x8C;
	crc_table[496] = 0x44;
	crc_table[497] = 0x84;
	crc_table[498] = 0x85;
	crc_table[499] = 0x45;
	crc_table[500] = 0x87;
	crc_table[501] = 0x47;
	crc_table[502] = 0x46;
	crc_table[503] = 0x86;
	crc_table[504] = 0x82;
	crc_table[505] = 0x42;
	crc_table[506] = 0x43;
	crc_table[507] = 0x83;
	crc_table[508] = 0x41;
	crc_table[509] = 0x81;
	crc_table[510] = 0x80;
	crc_table[511] = 0x40;
}


void UART_3_Initialization(void){
	
	/*
	 * Initialize UART3 pin connect
	 * P0.2: U2_TXD
	 * P0.3: U2_RXD
	 */
	PINSEL_ConfigPin(0,2,2);
	PINSEL_ConfigPin(0,3,2);

	//OE3: UART OE3 Output Enable for UART3
	PINSEL_ConfigPin(1, 30, 5); 

	/* Initialize UART Configuration parameter structure to default state:
	* Baudrate = 9600 bps
	* 8 data bit
	* 1 Stop bit
	* Parity: none
	* Note: Parity will be enabled later in UART_RS485Config() function.
	*/
	UART_ConfigStructInit(&UARTConfigStruct);
	UARTConfigStruct.Baud_rate = 9600;

	// Initialize UART3 peripheral with given to corresponding parameter
	UART_Init(_LPC_UART_3, &UARTConfigStruct);

	/* Initialize FIFOConfigStruct to default state:
	*               - FIFO_DMAMode = DISABLE
	*               - FIFO_Level = UART_FIFO_TRGLEV0
	*               - FIFO_ResetRxBuf = ENABLE
	*               - FIFO_ResetTxBuf = ENABLE
	*               - FIFO_State = ENABLE
	*/
	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

	// Initialize FIFO for UART0 peripheral
	UART_FIFOConfig(_LPC_UART_3, &UARTFIFOConfigStruct);

	// Configure RS485
	/*
	* - Auto Direction in Tx/Rx driving is enabled
	* - Direction control pin is set to DTR1
	* - Direction control pole is set to "1" that means direction pin
	* will drive to high state before transmit data.
	* - Multidrop mode is disable
	* - Auto detect address is disabled
	* - Receive state is enable
	*/
	rs485cfg.AutoDirCtrl_State = ENABLE;
	rs485cfg.DirCtrlPin = UART_RS485_DIRCTRL_DTR;
	rs485cfg.DirCtrlPol_Level = SET;
	rs485cfg.DelayValue = 50;
	rs485cfg.NormalMultiDropMode_State = DISABLE;
	rs485cfg.AutoAddrDetect_State = DISABLE;
	rs485cfg.MatchAddrValue = 0;
	rs485cfg.Rx_State = ENABLE;
	/* HO CAMBIATO LA PARITA !!! */
	UART_RS485Config(_LPC_UART_3, &rs485cfg);

	/* Enable UART Rx interrupt */
	UART_IntConfig(_LPC_UART_3, UART_INTCFG_RBR, ENABLE);

	/* Enable UART line status interrupt */
	UART_IntConfig(_LPC_UART_3, UART_INTCFG_RLS, ENABLE);


	// Priorities settings for UART RS485: here we use UART3 for RS485 communication
	// They should be changed if using another UART
	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(_UART_IRQ_3, ((0x01<<3)|0x01));

	/* Enable Interrupt for UART3 channel */
	NVIC_EnableIRQ(_UART_IRQ_3);

	// Enable UART Transmit
	UART_TxCmd(_LPC_UART_3, ENABLE);	
}



 /***********************************************************************
 * @brief		Modbus_check_crc16			
	 \param [in] 
 **********************************************************************/
void Modbus_check_crc16(volatile uint8_t* modbusframe,int Length){
  int i;
  int index;
  Crc16_Low = 0xFF;
  Crc16_Hi  = 0xFF;

  for (i = 0; i<Length; i++)  {
    index = Crc16_Hi ^ (unsigned char)modbusframe[i];
    Crc16_Hi = Crc16_Low ^ crc_table[index];
    Crc16_Low = crc_table[index + 256];
  }
}






void UART_Slave_IntReceive(void)
{
    uint8_t tmpc;
    uint32_t rLen;
		uint8_t i = 0;
		uint8_t valid_address = 0;
    uint8_t msgLen = 8;
    uint8_t exit_flag = 0;
	
    while(exit_flag==0){
        // Call UART read function in UART driver
  			if (RS485_Port_Sel==SLAVE_1_MASTER_2)
          rLen = UART_Receive(_LPC_UART_3, &tmpc, 1, NONE_BLOCKING);
		  	else if (RS485_Port_Sel==MASTER_1_SLAVE_2)
          rLen = UART_Receive(_LPC_UART_2, &tmpc, 1, NONE_BLOCKING);
        // If data received
        if (rLen){
            //azzera indice se i byte non arrivano in tempo
						if ( (rs485_rx_ndx) && (TmrRxNextByte==0) ){
							rs485_rx_ndx = 0;
							PackFull = 0;
							exit_flag = 1;
						}
						TmrRxNextByte = MAX_WAIT4BYTE;
      
						rs485_rx_com_msg[rs485_rx_ndx++] = tmpc;			// receive character 
//GuiVar_integer_1 = rs485_rx_com_msg[0];
//GuiVar_integer_2 = rs485_rx_com_msg[1];
//GuiVar_integer_3 = rs485_rx_com_msg[2];
//GuiVar_integer_4 = rs485_rx_com_msg[3];
//GuiVar_integer_5 = rs485_rx_com_msg[4];
//GuiVar_integer_6 = rs485_rx_com_msg[5];
//GuiVar_integer_7 = rs485_rx_com_msg[6];
//GuiVar_integer_8 = rs485_rx_com_msg[7];
						//controllo messaggio
						if ( ((rs485_rx_ndx==7)) && (rs485_rx_com_msg[0]==RS485_Slave_Address) && (rs485_rx_com_msg[1]==16) )
							msgLen = rs485_rx_com_msg[6]+9;
						if ( ((rs485_rx_ndx==msgLen)) && (rs485_rx_com_msg[0]==RS485_Slave_Address) && (rs485_rx_com_msg[1]==16) ){         
							Set_data = 256*(uint8_t)rs485_rx_com_msg[10]+(uint8_t)rs485_rx_com_msg[11];//eventuale valore ricevuto da master da modificare
							Modbus_check_crc16(&rs485_rx_com_msg[0],rs485_rx_ndx-2);//verifica il crc16 ricevuto
							if ( (rs485_rx_com_msg[msgLen-2]==Crc16_Hi) && (rs485_rx_com_msg[msgLen-1]==Crc16_Low) ){
								//se crc ok controlla il msg ricevuto
								Id_function = rs485_rx_com_msg[1];
								Id_msg = 256*(uint8_t)rs485_rx_com_msg[2]+(uint8_t)rs485_rx_com_msg[3];
								Id_Lenght = (rs485_rx_com_msg[6]<50)? rs485_rx_com_msg[6] :50;//al max 50 int
								IdWritedReg = 256*(uint8_t)rs485_rx_com_msg[4]+(uint8_t)rs485_rx_com_msg[5];
          
								Set_data = 256*(uint8_t)rs485_rx_com_msg[7]+(uint8_t)rs485_rx_com_msg[8];//eventuale valore ricevuto da master da modificare
								if (BadFrame==0){//se la linea � rimasta libera
									PackFull = 1;
//									Ready2Send = ANSWER_DELAY;//
								}
							}
							rs485_rx_ndx = 0;
							rs485_rx_com_msg[0] = 0;
					    exit_flag = 1;
						}
						else if ( (rs485_rx_ndx==8) && (rs485_rx_com_msg[0]==RS485_Slave_Address) && (rs485_rx_com_msg[1]!=16) ){
							msgLen = rs485_rx_ndx;
							Modbus_check_crc16(&rs485_rx_com_msg[0], rs485_rx_ndx-2);//verifica il crc16 ricevuto
							if ( (rs485_rx_com_msg[6]==Crc16_Hi) && (rs485_rx_com_msg[7]==Crc16_Low) ){
								//se crc ok controlla il msg ricevuto
								Id_function = rs485_rx_com_msg[1];
								Id_msg = 256*(uint8_t)rs485_rx_com_msg[2]+(uint8_t)rs485_rx_com_msg[3];
								if ( (Id_function==3) || (Id_function==6) || (Id_function==16) ){
									Id_Lenght = (rs485_rx_com_msg[5]<50)? rs485_rx_com_msg[5] :50;//al max 50 int
								}
								else{
									if (rs485_rx_com_msg[5]<9)
										Id_Lenght = 1;
									else if (rs485_rx_com_msg[5]<17)
										Id_Lenght = 2;
									else if (rs485_rx_com_msg[5]<25)
										Id_Lenght = 3;
									else if (rs485_rx_com_msg[5]<33)
										Id_Lenght = 4;
									else
										Id_Lenght = 5;
								}
								Set_data = 256*(uint8_t)rs485_rx_com_msg[4]+(uint8_t)rs485_rx_com_msg[5];//eventuale valore ricevuto da master da modificare
								if (BadFrame==0){//se la linea � rimasta libera
									PackFull = 1;
//									Ready2Send = ANSWER_DELAY;//
								}
							}
							rs485_rx_ndx = 0;
							rs485_rx_com_msg[0] = 0;
    					exit_flag = 1;
						}
						//messaggio da scartare
						else if ( (rs485_rx_ndx>=msgLen) || (rs485_rx_com_msg[0]!=RS485_Slave_Address) ){
							rs485_rx_ndx = 0;
							rs485_rx_com_msg[0] = 0;
							PackFull = 0;
							BadFrame = 5;//carica tempo di attesa linea libera
							exit_flag = 1;
						}	
        }
        // no more data
        else 
        {
					exit_flag = 1;
        }
    }
//////////	  // gestione messaggi Modbus in risposta al Master su seriale UART 3 - ora � nel main, da spostare qui se fosse necessario rispondere pi� in fretta
//////////		// per ora ho visto che va bene nel main e se messa qui la risposta a volte (1 su 15) � troppo veloce e il Master (MODSCAN32) mi perde il primo byte
//////////    if (PackFull==1)
//////////    {
//////////      PackFull = 0;
//////////      UART_3_Modbus_Slave_answer();
//////////    }	
}


/********************************************************************//**
 * @brief       UART receive function (ring buffer used)
 * @param[in]   None
 * @return      None
 *********************************************************************/
void UART_Master_IntReceive(void)
{
    uint8_t tmpc;
    uint32_t rLen;
		uint8_t i = 0;
		uint8_t valid_address = 0;
    uint8_t exit_flag = 0;
	
    while(exit_flag==0){
        // Call UART read function in UART driver
  			if (RS485_Port_Sel==SLAVE_1_MASTER_2)
          rLen = UART_Receive(_LPC_UART_2, &tmpc, 1, NONE_BLOCKING);
		  	else if (RS485_Port_Sel==MASTER_1_SLAVE_2)
          rLen = UART_Receive(_LPC_UART_3, &tmpc, 1, NONE_BLOCKING);
        // If data received
        if (rLen)
        {
            /* Check if buffer is more space
            * If no more space, remaining character will be trimmed out
            */
            if (!__BUF_IS_FULL(rb2.rx_head,rb2.rx_tail))
            {
                rb2.rx[rb2.rx_head] = tmpc;
                __BUF_INCR(rb2.rx_head);
            }

						// Fai spazio al nuovo byte
						for (i=0; i<6; i++) {
							rs485_rx_msg[0 + i] = rs485_rx_msg[1 + i];
						}					
						// Aggiungi il byte ricevuto nel buffer
						// nella posizione 7 perch� poi shifta
						rs485_rx_msg[6] = tmpc;

						// Controlla se l'indirizzo � di uno dei sensori 
						valid_address = 0;
						
						//qui era il codice per la ricezione del valore di O2 dai sensori ossido di zirconio - ci andr� il codice di ricezione da eventuale device slave collegato
//						for (i=1; i<(sys_sensors_num + 1); i++) {
//							if (i == rs485_rx_msg[0]) {
//								valid_address = 1;
//							}
//						}
//						
//						if (valid_address == 1) {
//								// Controlla se � un messaggio valido					
//								Modbus_check_crc16( &rs485_rx_msg[0], 5);
//								if ( (rs485_rx_msg[5] == Crc16_Hi ) && ((rs485_rx_msg[6] == Crc16_Low )) && (rs485_rx_msg[1] == 3)){ 	// controlla CRC e Controlla funzione
//  								sensors_array[rs485_rx_msg[0]].o2_value = rs485_rx_msg[4];
//	          			sensors_array[rs485_rx_msg[0]].o2_new_value = (sensors_array[rs485_rx_msg[0]].o2_value / 10);	
//				          sensors_array[rs485_rx_msg[0]].o2_value_from_sensors=sensors_array[rs485_rx_msg[0]].o2_new_value+sensors_array[rs485_rx_msg[0]].offset_o2_value;
//				          sensors_array[rs485_rx_msg[0]].o2_value = sensors_array[rs485_rx_msg[0]].o2_value_from_sensors;//sensors_array[temp_msg[0]].o2_value + sensors_array[temp_msg[0]].offset_o2_value;
//				
//          				sensors_array[rs485_rx_msg[0]].comm_timeout = SENSOR_COMM_TIMEOUT;
//								}
//						}
						
        }
        // no more data
        else 
        {
          exit_flag = 1;
        }
    }
}



/*********************************************************************//**
 * @brief       porta COM 1 UART3 interrupt handler sub-routine
 * @param[in]   None
 * @return      None
 **********************************************************************/
void UART_3_IRQHander(void)
{
    uint32_t intsrc, tmp, tmp1;

	  /* Determine the interrupt source */
    intsrc = UART_GetIntId(_LPC_UART_3);
    
    tmp = intsrc & UART_IIR_INTID_MASK;

    // Receive Line Status
    if (tmp == UART_IIR_INTID_RLS)
    {
        // Check line status
        tmp1 = UART_GetLineStatus(_LPC_UART_3);

        // Mask out the Receive Ready and Transmit Holding empty status
        tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE \
                                | UART_LSR_BI | UART_LSR_RXFE);

        // If any error exist
        if (tmp1) 
        {
//             UART_IntErr(tmp1);
        }
    }

    // Receive Data Available or Character time-out
    if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
    {		
			if (RS485_Port_Sel==SLAVE_1_MASTER_2)
        UART_Slave_IntReceive();
			else if (RS485_Port_Sel==MASTER_1_SLAVE_2)
        UART_Master_IntReceive();
    }
}


////spedisce messaggio Modbus in risposta al Master
///***********************************************************************
// * @brief		UART_Modbus_Slave_answer 
// *          r_w 0 = scrittura, 1 = lettura
// **********************************************************************/
void UART_Modbus_Slave_answer (void){
  uint32_t ore;
  unsigned long ore_e2;
  unsigned char tmp=0;
  unsigned char index;
  unsigned char internal_address;
  unsigned char valid_msg=0;
  uint32_t temp;
  uint32_t ResultCode, TrialResultCode, TrialResultCode2;

	rs485_tx_ndx = 0;
  
  rs485_tx_com_msg[rs485_tx_ndx++] = RS485_Slave_Address;
  
  //controlla se il messaggio � definito
  if (Id_function==READ_REGISTER)//lettura analog register
  {
    //spedisce valore letture analogiche: O2 - portata aria/azoto - pressione aria/azoto
    //spedisce allarmi: macchina + compressore + essicatore
    //spedisce stato funzionamento: ON,OFF,STBY
    //if ( (Id_msg>=MBUS_ANALOGICHE) && (Id_msg<=MBUS_COMANDO) )
    if ( (Id_msg>=MBUS_ANALOGICHE) && (Id_msg<=1200) )
    {
      valid_msg=1;
      
			// popola con dei valori per prove web server Nicola
//			Modbus.Array[MBVECTOR_O2] = 208;
//			Modbus.Array[MBVECTOR_Q_AIR] = 1400;
//			Modbus.Array[MBVECTOR_Q_N2] = 600;
//			Modbus.Array[MBVECTOR_P1] = 800;
//			Modbus.Array[MBVECTOR_P2] = 600;
//			Modbus.Array[MBVECTOR_T_AIR] = 253;
//			Modbus.Array[MBVECTOR_T_N2] = 242;
//			Modbus.Array[MBVECTOR_P3] = 550;
//			Modbus.Array[10] = 207;  //O2 medio
//			Modbus.Array[11] = 4510; //ore lavoro
//			Modbus.Array[MBVECTOR_T1] = 225;
//			Modbus.Array[MBVECTOR_T2] = 232;
//			Modbus.Array[MBVECTOR_T3] = 240;
//			Modbus.Array[MBVECTOR_T4] = 245;
//			Modbus.Array[MBVECTOR_T5] = 250;
//			Modbus.Array[MBVECTOR_T6] = 260;
//			Modbus.Array[MBVECTOR_FLOW_AVG] = 589;
      Modbus.Array[10] = Adox1500.O2_Fifo[0];
      Modbus.Array[11] = (unsigned int)(((unsigned long)Adox1500.Ore_Run * 12) + (Adox1500.CounterSec/3600));
      Modbus.Array[MBVECTOR_STATO] = Adox1500.Stato;//assegna lo stato di fuzionamento della macchina
      if (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_LOCAL_ON)
        Modbus.Array[MBVECTOR_STATO] |= 0x0100; //Adox ON (pu� essere in funzione o in standby)
      else
        Modbus.Array[MBVECTOR_STATO] &= ~0x0100; //Adox OFF
      if (Adox1500.Mode & ADOX_RUNNING)
        Modbus.Array[MBVECTOR_STATO] |= 0x0001; //Adox in marcia
      else
        Modbus.Array[MBVECTOR_STATO] &= ~0x0001; //Adox in arresto
      Modbus.Array[MBVECTOR_TIME_ASS] = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA];
      Modbus.Array[MBVECTOR_TIME_CMP] = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA];
      Modbus.Array[MBVECTOR_TIME_SCARICO] = Adox1500.Scarico;
      Modbus.Array[MBVECTOR_TIME_SFASAM] = Adox1500.Sfasamento;
      Modbus.Array[MBVECTOR_CMD_SETO2] = Adox1500.SetO2;
      Modbus.Array[MBVECTOR_CMD_SET_TIN] = E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM];
      Modbus.Array[MBVECTOR_O2_HIGH_AL] =  E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM];
      //25
			Modbus.Array[MBVECTOR_SECONDION] = Adox1500.SecondiOn;
      //30
			Modbus.Array[MBVECTOR_LED] = LED_output;
      Modbus.Array[MBVECTOR_RELE_1] = (StRele[OUT_CC_ADOX].Stato & RELEON)? 1: 0;
      Modbus.Array[MBVECTOR_RELE_2] = (StRele[OUT_1_ADOX].Stato & RELEON)? 1: 0;
      Modbus.Array[MBVECTOR_RELE_3] = (StRele[OUT_ALARM].Stato & RELEON)? 1: 0;
      Modbus.Array[MBVECTOR_RELE_4] = (StRele[OUT_VENTIL_TEMP].Stato & RELEON)? 1: 0;
      Modbus.Array[MBVECTOR_RELE_5] = (StRele[OUT_R5_LIKE_R1].Stato & RELEON)? 1: 0;
      Modbus.Array[MBVECTOR_RELE_6] = (StRele[OUT_VTARATURA].Stato & RELEON)? 1: 0;

      Modbus.Array[49] = Adox1500.Mode;
//max 49 ---------------------------------------      
      //sistemare con i giusti indici se necessario - pagina e registro
//      for (index=82; index<105; index++)
//        Modbus.Array[index] = E2prom[E2_PAGE_1].E2_Int[index-33];
            
      temp = MBUS_ANALOGICHE;
      internal_address = (unsigned char)(Id_msg-temp);
      rs485_tx_com_msg[rs485_tx_ndx++] = READ_REGISTER; //codice funzione (3-lettura 6-scrittura)
      rs485_tx_ndx++;
      for (index=internal_address; index<Id_Lenght+internal_address; index++)
      {
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[index]>>8) & 0xFF);        // O2
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[index] & 0xFF);             // O2
      }
      rs485_tx_com_msg[2] = rs485_tx_ndx-3; // numero byte trasmessi
    }
  }
  else if (Id_function==READ_COILS)//lettura coils
  {
    if ( (Id_msg>=MBUS_ANALOGICHE) && (Id_msg<=MBUS_COMANDO) )
    {
      valid_msg=1;
      temp = MBUS_ANALOGICHE;
      internal_address = (unsigned char)(Id_msg-temp);
      rs485_tx_com_msg[rs485_tx_ndx++] = READ_COILS; //codice funzione (3-lettura 6-scrittura)
      rs485_tx_ndx++;
      for (index=internal_address; index<Id_Lenght+internal_address; index++)
      {
        //if (Id_Lenght==1) a seconda byte alto o basso
        //rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_ALLARMI]>>8) & 0xFF);
        //rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_ALLARMI] & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = 43;             //solo una prova - non � implementato per un uso reale 
      }
      rs485_tx_com_msg[2] = Id_Lenght;//rs485_tx_ndx-3; // numero byte trasmessi
    }
  }
  else if (Id_function==WRITE_FUNCTION) //scrittura - comando
  {
    //spedisce conferma ricezione comando ON/OFF
    switch (Id_msg)
    {
      case MBUS_TIME_ASS:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_ASS] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_TIME_ASS]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_TIME_ASS] & 0xFF);
        Adox1500.Assorbimento = Modbus.Array[MBVECTOR_TIME_ASS];
        E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA] = Modbus.Array[MBVECTOR_TIME_ASS];
			  eeprom_write_word(E2_PAGE_2,E2_2_TA_VSAPSA*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA]);
        break;
        
      case MBUS_TIME_CMP:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_CMP] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_TIME_CMP]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_TIME_CMP] & 0xFF);
        Adox1500.Compensazione = Modbus.Array[MBVECTOR_TIME_CMP];
        E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA] = Modbus.Array[MBVECTOR_TIME_CMP];
			  eeprom_write_word(E2_PAGE_2,E2_2_TC_VSAPSA*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA]);
        break;
        
      case MBUS_TIME_SCARICO: //scarico per Adox 1500
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_SCARICO] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_TIME_SCARICO]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_TIME_SCARICO] & 0xFF);
        Adox1500.Scarico = Modbus.Array[MBVECTOR_TIME_SCARICO];
        E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO] = Adox1500.Scarico;
			  eeprom_write_word(E2_PAGE_2,E2_2_SCARICO*2,E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO]);
        break;
        
      case MBUS_TIME_SFASAM:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_SFASAM] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_TIME_SFASAM]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_TIME_SFASAM] & 0xFF);
        Adox1500.Sfasamento = Modbus.Array[MBVECTOR_TIME_SFASAM];
        E2prom[E2_PAGE_2].E2_Int[E2_2_SFASAMENTO] = Adox1500.Sfasamento;
			  eeprom_write_word(E2_PAGE_2,E2_2_SFASAMENTO*2,E2prom[E2_PAGE_2].E2_Int[E2_2_SFASAMENTO]);
        break;
        
      case MBUS_SETO2:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_CMD_SETO2] = (unsigned char)(Set_data);//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_CMD_SETO2]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_CMD_SETO2] & 0xFF);
        Adox1500.SetO2 = (unsigned char)(Modbus.Array[MBVECTOR_CMD_SETO2]);
        E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET] = (unsigned char)(Adox1500.SetO2);
			  eeprom_write_word(E2_PAGE_2,E2_2_ADOX_O2_SET*2,E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET]);
        break;
        
      case MBUS_SET_TIN://set sonda temperatura compressore
        valid_msg=SI;
        Modbus.Array[MBVECTOR_CMD_SET_TIN] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_CMD_SET_TIN]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_CMD_SET_TIN] & 0xFF);
        E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM] = Modbus.Array[MBVECTOR_CMD_SET_TIN];
			  eeprom_write_word(E2_PAGE_2,E2_2_TEMP_ALARM*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]);
        break;
        
      case MBUS_O2_HIGH_AL://set allarme ossigeno prodotto impuro
        valid_msg=SI;
        Modbus.Array[MBVECTOR_O2_HIGH_AL] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_O2_HIGH_AL]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_O2_HIGH_AL] & 0xFF);
        E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM] = Modbus.Array[MBVECTOR_O2_HIGH_AL];
			  eeprom_write_word(E2_PAGE_2,E2_2_O2_HIGH_ALARM*2,E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM]);
        break;
        
      case MBUS_T_ZERO:
        if ( (Set_data==34) || (Set_data==37)  || (Set_data==40)  || (Set_data==134)  || (Set_data==137)  || (Set_data==140) )
        {
          valid_msg=SI;
          rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
          //Set_data indica il primo PIN del morsetto relativo alla sonda PT100 da tarare
//          if (Set_data==34){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP1] = sonde[SONDATEMP1];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP1*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP1]);
//          }
//					else if (Set_data==37){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP2] = sonde[SONDATEMP2];
//	  			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP2*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP2]);
// 		  	  }
//				  else if (Set_data==40){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP3] = sonde[SONDATEMP3];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP3*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP3]);
//          }
//					else if (Set_data==134){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP4] = sonde[SONDATEMP4];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP4*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP4]);
//          }
//					else if (Set_data==137){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP5] = sonde[SONDATEMP5];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP5*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP5]);
//          }
//					else if (Set_data==140){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP6] = sonde[SONDATEMP6];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP6*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP6]);
//          }
					valid_msg=SI;
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Set_data>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Set_data & 0xFF);
        }
        break;
        
      case MBUS_AL_RESET:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_AL_RESET] = (unsigned char)(Set_data);//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_AL_RESET]>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_AL_RESET] & 0xFF);
        break;
        
      case MBUS_COMANDO:
        valid_msg=SI;
        //solo se la macchina � impostata in REM (controllo da remoto e non locale LOC) accetta il comando
        if (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_REMOTE)
        {
          Modbus.Array[MBVECTOR_COMANDO] = Set_data;//assegno il comando
          rs485_tx_com_msg[rs485_tx_ndx++] = WRITE_FUNCTION; //codice funzione (6-scrittura)
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Modbus.Array[MBVECTOR_COMANDO]>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Modbus.Array[MBVECTOR_COMANDO] & 0xFF);
//          TmrPushOnOff = 6;
          if (Modbus.Array[MBVECTOR_COMANDO])
            Adox1500.Mode = 0;
//          rtc_wrram((unsigned char)(Modbus.Array[MBVECTOR_COMANDO] & 0xFF),REG_RMT_CMD);
        }
        break;
    }
  }
  // multiple register
  else if (Id_function==MULTIPLE_WRITE_F) //scrittura multipla - comando
  {
    //spedisce conferma ricezione comando ON/OFF
    switch (Id_msg)
    {
      case MBUS_TIME_ASS:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_ASS] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        Adox1500.Assorbimento = Modbus.Array[MBVECTOR_TIME_ASS];
        E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA] = Modbus.Array[MBVECTOR_TIME_ASS];
			  eeprom_write_word(E2_PAGE_2,E2_2_TA_VSAPSA*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA]);
        break;
        
      case MBUS_TIME_CMP:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_CMP] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        Adox1500.Compensazione = Modbus.Array[MBVECTOR_TIME_CMP];
        E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA] = Modbus.Array[MBVECTOR_TIME_CMP];
			  eeprom_write_word(E2_PAGE_2,E2_2_TC_VSAPSA*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA]);
        break;
        
      case MBUS_TIME_SCARICO:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_SCARICO] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        Adox1500.Scarico = Modbus.Array[MBVECTOR_TIME_SCARICO];
        E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO] = Adox1500.Scarico;
			  eeprom_write_word(E2_PAGE_2,E2_2_SCARICO*2,E2prom[E2_PAGE_2].E2_Int[E2_2_SCARICO]);
        break;
        
      case MBUS_TIME_SFASAM:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_TIME_SFASAM] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        Adox1500.Sfasamento = Modbus.Array[MBVECTOR_TIME_SFASAM];
        E2prom[E2_PAGE_2].E2_Int[E2_2_SFASAMENTO] = Adox1500.Sfasamento;
			  eeprom_write_word(E2_PAGE_2,E2_2_SFASAMENTO*2,E2prom[E2_PAGE_2].E2_Int[E2_2_SFASAMENTO]);
        break;
        
      case MBUS_SETO2:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_CMD_SETO2] = (unsigned char)(Set_data);//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        Adox1500.SetO2 = (unsigned char)(Modbus.Array[MBVECTOR_CMD_SETO2]);
        E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET] = (unsigned char)(Adox1500.SetO2);
			  eeprom_write_word(E2_PAGE_2,E2_2_ADOX_O2_SET*2,E2prom[E2_PAGE_2].E2_Int[E2_2_ADOX_O2_SET]);
        break;
        
      case MBUS_SET_TIN://set sonda temperatura compressore
        valid_msg=SI;
        Modbus.Array[MBVECTOR_CMD_SET_TIN] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM] = Modbus.Array[MBVECTOR_CMD_SET_TIN];
			  eeprom_write_word(E2_PAGE_2,E2_2_TEMP_ALARM*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]);
        break;
        
      case MBUS_O2_HIGH_AL://set allarme ossigeno prodotto impuro
        valid_msg=SI;
        Modbus.Array[MBVECTOR_O2_HIGH_AL] = Set_data;//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (6-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM] = Modbus.Array[MBVECTOR_O2_HIGH_AL];
			  eeprom_write_word(E2_PAGE_2,E2_2_O2_HIGH_ALARM*2,E2prom[E2_PAGE_2].E2_Int[E2_2_O2_HIGH_ALARM]);
        break;
               
      case MBUS_T_ZERO:
        if ( (Set_data==34) || (Set_data==37)  || (Set_data==40)  || (Set_data==134)  || (Set_data==137)  || (Set_data==140) )
        {
          valid_msg=SI;
          rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
          //Set_data indica il primo PIN del morsetto relativo alla sonda PT100 da tarare
//          if (Set_data==34){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP1] = sonde[SONDATEMP1];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP1*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP1]);
//				  }
//				  else if (Set_data==37){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP2] = sonde[SONDATEMP2];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP2*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP2]);
//				  }
//          else if (Set_data==40){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP3] = sonde[SONDATEMP3];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP3*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP3]);
//				  }
//          else if (Set_data==134){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP4] = sonde[SONDATEMP4];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP4*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP4]);
//				  }
//          else if (Set_data==137){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP5] = sonde[SONDATEMP5];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP5*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP5]);
//				  }
//          else if (Set_data==140){
//            E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP6] = sonde[SONDATEMP6];
//    			  eeprom_write_word(E2_PAGE_3,E2_3_ADCZEROTEMP6*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ADCZEROTEMP6]);
//				  }
          valid_msg=SI;
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Set_data>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Set_data & 0xFF);
        }
        break;
        
      case MBUS_AL_RESET:
        valid_msg=SI;
        Modbus.Array[MBVECTOR_AL_RESET] = (unsigned char)(Set_data);//assegno il comando
        rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
        rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
        break;
        
      case MBUS_COMANDO:
        valid_msg=SI;
        //solo se la macchina � impostata in REM (controllo da remoto e non locale LOC) accetta il comando
        if (E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]==CMD_REMOTE)
        {
          Modbus.Array[MBVECTOR_COMANDO] = Set_data;//assegno il comando
          rs485_tx_com_msg[rs485_tx_ndx++] = Id_function; //codice funzione (16-scrittura)
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((Id_msg>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(Id_msg & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)((IdWritedReg>>8) & 0xFF);
          rs485_tx_com_msg[rs485_tx_ndx++] = (unsigned char)(IdWritedReg & 0xFF);
//          TmrPushOnOff = 6;
          if (Modbus.Array[MBVECTOR_COMANDO])
            Adox1500.Mode = 0;
//          rtc_wrram((unsigned char)(Modbus.Array[MBVECTOR_COMANDO] & 0xFF),REG_RMT_CMD);
        }
        break;
    }
  }
  //spedisce solo quando ha ricevuto da master un msg definito
  if (valid_msg==1)
  {
		//calcola crc16 del pacchetto
  	Modbus_check_crc16(&rs485_tx_com_msg[0], rs485_tx_ndx);
    //aggiunge i due byte di controllo
    rs485_tx_com_msg[rs485_tx_ndx++] = Crc16_Hi;
    rs485_tx_com_msg[rs485_tx_ndx] = Crc16_Low;
		if (RS485_Port_Sel==MASTER_1_SLAVE_2)
		  UART_RS485SendData(_LPC_UART_2, &rs485_tx_com_msg[0], rs485_tx_ndx+1);
		else if (RS485_Port_Sel==SLAVE_1_MASTER_2)
		  UART_RS485SendData(_LPC_UART_3, &rs485_tx_com_msg[0], rs485_tx_ndx+1);
  }
}


//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

//  2. UART_2: RS485 Modbus Master


void UART_2_Initialization(void){
	/* UART - RS485 */
	/*
	 * Initialize UART2 pin connect
	 * P4.22: U2_TXD
	 * P4.23: U2_RXD
	 */
	PINSEL_ConfigPin(4,22,2);
	PINSEL_ConfigPin(4,23,2);

	//OE2: UART OE2 Output Enable for UART2
	PINSEL_ConfigPin(1, 19, 6); 

	/* Initialize UART 2 Configuration parameter structure to default state:
	* Baudrate = 9600 bps
	* 8 data bit
	* 1 Stop bit
	* Parity: none
	* Note: Parity will be enabled later in UART_RS485Config() function.
	*/
	UART_ConfigStructInit(&UARTConfigStruct);
	UARTConfigStruct.Baud_rate = 9600;

	// Initialize UART2 peripheral with given to corresponding parameter
	UART_Init(_LPC_UART_2, &UARTConfigStruct);

	/* Initialize FIFOConfigStruct to default state:
	*               - FIFO_DMAMode = DISABLE
	*               - FIFO_Level = UART_FIFO_TRGLEV0
	*               - FIFO_ResetRxBuf = ENABLE
	*               - FIFO_ResetTxBuf = ENABLE
	*               - FIFO_State = ENABLE
	*/
	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

	// Initialize FIFO for UART0 peripheral
	UART_FIFOConfig(_LPC_UART_2, &UARTFIFOConfigStruct);

	// Configure RS485
	/*
	* - Auto Direction in Tx/Rx driving is enabled
	* - Direction control pin is set to DTR1
	* - Direction control pole is set to "1" that means direction pin
	* will drive to high state before transmit data.
	* - Multidrop mode is disable
	* - Auto detect address is disabled
	* - Receive state is enable
	*/
	rs485cfg.AutoDirCtrl_State = ENABLE;
	rs485cfg.DirCtrlPin = UART_RS485_DIRCTRL_DTR;
	rs485cfg.DirCtrlPol_Level = SET;
	rs485cfg.DelayValue = 50;
	rs485cfg.NormalMultiDropMode_State = DISABLE;
	rs485cfg.AutoAddrDetect_State = DISABLE;
	rs485cfg.MatchAddrValue = 0;
	rs485cfg.Rx_State = ENABLE;
	/* HO CAMBIATO LA PARITA !!! */
	UART_RS485Config(_LPC_UART_2, &rs485cfg);

	/* Enable UART Rx interrupt */
	UART_IntConfig(_LPC_UART_2, UART_INTCFG_RBR, ENABLE);

	/* Enable UART line status interrupt */
	UART_IntConfig(_LPC_UART_2, UART_INTCFG_RLS, ENABLE);


	// Priorities settings for UART RS485: here we use UART2 for RS485 communication
	// They should be changed if using another UART
	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(_UART_IRQ_2, ((0x01<<3)|0x01));

	/* Enable Interrupt for UART2 channel */
	NVIC_EnableIRQ(_UART_IRQ_2);

// Enable UART Transmit
	UART_TxCmd(_LPC_UART_2, ENABLE);	
	
}



/*********************************************************************//**
 * @brief       porta COM 2 UART2 interrupt handler sub-routine
 * @param[in]   None
 * @return      None
 **********************************************************************/
void UART_2_IRQHander(void)
{
    uint32_t intsrc, tmp, tmp1;

	/* Determine the interrupt source */
    intsrc = UART_GetIntId(_LPC_UART_2);
    
    tmp = intsrc & UART_IIR_INTID_MASK;

    // Receive Line Status
    if (tmp == UART_IIR_INTID_RLS)
    {
        // Check line status
        tmp1 = UART_GetLineStatus(_LPC_UART_2);

        // Mask out the Receive Ready and Transmit Holding empty status
        tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE \
                                | UART_LSR_BI | UART_LSR_RXFE);

        // If any error exist
        if (tmp1) 
        {
//             UART_IntErr(tmp1);
        }
    }

    // Receive Data Available or Character time-out
    if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
    {		
			if (RS485_Port_Sel==MASTER_1_SLAVE_2)
        UART_Slave_IntReceive();
			else if (RS485_Port_Sel==SLAVE_1_MASTER_2)
        UART_Master_IntReceive();
    }
}


 /***********************************************************************
 * @brief		UART_Modbus_Master_polling			
	 interroga i sensori (uno diverso ogni main)
 **********************************************************************/
void UART_Modbus_Master_polling(void){
  
//	// qui ci va il corpo della funzione che invia il messaggio in Master mode - era il msg per i sensori di O2 dalla CIE

//	unsigned char alarm_high;
//	unsigned char alarm_low;
//	
//  rs485_main_schedule = (rs485_main_schedule<RS485_MAIN_COUNT)? ++rs485_main_schedule: RS485_MAIN_COUNT;
//	
//	if (rs485_main_schedule == RS485_MAIN_COUNT){
//		if (rs485_imposta_soglie_allarme == 2)
//		{
//  	  // interroga un sensore ogni RS485_MAIN_COUNT cicli main (adesso � poco pi� di 1 secondo quindi)
//		  rs485_main_schedule = 0;
//  		sensor_polling_index++;
//		 
//      //  es:  01 06 07 D8 A0 8C 71 20
//		  rs485_tx_msg[0] = sensor_polling_index;		// Address
//		  rs485_tx_msg[1] = 0x06;									  // Function
//		  rs485_tx_msg[2] = 0x07;										// Address High
//		  rs485_tx_msg[3] = 0xD8;										// Address Low
//      
//			//N.B. se aggiungo un offset al dato che il sensore passa, lo devo togliere alla soglia che invio e viceversa
//			alarm_high = (unsigned char)((sensors_array[sensor_polling_index].th_high*10) - (sensors_array[sensor_polling_index].offset_o2_value*10))+1;		// soglia allarme ossigeno alt)o
//		  alarm_low = (unsigned char)((sensors_array[sensor_polling_index].th_low*10) - (sensors_array[sensor_polling_index].offset_o2_value*10))-1;		// soglia allarme ossigeno alt)o

//			if ( (sensors_array[sensor_polling_index].th_high==25) || (alarm_high>=250) || (alarm_high<20) )
//				alarm_high = 249;
//			rs485_tx_msg[4] = alarm_high;
//		  rs485_tx_msg[5] = alarm_low;
//		
//  		Modbus_check_crc16( &rs485_tx_msg[0], 6);
//	  	rs485_tx_msg[6] = Crc16_Hi;
//		  rs485_tx_msg[7] = Crc16_Low;
//		}
//    else
//		{		
//  	  // interroga un sensore ogni RS485_MAIN_COUNT cicli main (adesso � poco pi� di 1 secondo quindi)
//		  rs485_main_schedule = 0;
//  		sensor_polling_index++;
//		 
//		  rs485_tx_msg[0] = sensor_polling_index;		// Address
//		  rs485_tx_msg[1] = 0x03;									  // Function
//		  rs485_tx_msg[2] = 0x03;										// Address High
//		  rs485_tx_msg[3] = 0xEB;										// Address Low (235)
//		  rs485_tx_msg[4] = 0x00;										// N. of bytes High
//		  rs485_tx_msg[5] = 0x01;										// N. of bytes Low
//		
//  		Modbus_check_crc16( &rs485_tx_msg[0], 6);
//	  	rs485_tx_msg[6] = Crc16_Hi;
//		  rs485_tx_msg[7] = Crc16_Low;
//    }
//	  if (RS485_Port_Sel==MASTER_1_SLAVE_2)
//	  	UART_RS485SendData(_LPC_UART_3, &rs485_tx_msg[0], 8);//prova seconda seriale	
//		else if (RS485_Port_Sel==SLAVE_1_MASTER_2)
//		  UART_RS485SendData(_LPC_UART_2, &rs485_tx_msg[0], 8);//prova seconda seriale	
//		
//		if (sensor_polling_index == sys_sensors_num ) {
//			sensor_polling_index = 0;
//			rs485_imposta_soglie_allarme = (rs485_imposta_soglie_allarme==1)? 2: 0;
//		}
//	}
}



//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

//  3. CAN_1:  CANOpen Master


// CAN
	/** CAN variable definition **/
	CAN_MSG_Type TXMsg, RXMsg; // messages for test Bypass mode
	uint32_t CAN1RxCount, CAN1TxCount = 0;
	uint32_t CAN2RxCount, CAN2TxCount = 0;

	AF_SectionDef AFTable;
	FullCAN_Entry FullCAN_Table[1];
	SFF_Entry SFF_Table[1];
	SFF_GPR_Entry SFF_GPR_Table[1];
	EFF_Entry EFF_Table[1];
	EFF_GPR_Entry EFF_GPR_Table[1];

	volatile unsigned int can_data_array[RX_BUFFER_CAN][9];
	volatile unsigned int can_data_array2[RX_BUFFER_CAN][9];
	volatile unsigned int can_buffer_count = 0;
	volatile unsigned int can_buffer_count2 = 0;
	char new_message_can_1=0;
	char new_message_can_2=0;
  volatile unsigned int tmrCanReinit=0;
  volatile unsigned int CanBusFault=0;

	unsigned int can_polling_index 						 = 0;
	unsigned int can_init_polling_index 		   = 0;	


void CAN_BUS_Initialization(void){	
// CAN_1
    PINSEL_ConfigPin (0, 0, 1);
    PINSEL_ConfigPin (0, 1, 1);
// CAN_2
     PINSEL_ConfigPin (0, 4, 2);
     PINSEL_ConfigPin (0, 5, 2);

    //Initialize CAN1 & CAN2

// 	  CAN_Init(CAN_2, (uint32_t)(E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE])*1000);
//	  CAN_Init(CAN_1, (uint32_t)(E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE])*1000);
 	  CAN_Init(CAN_2, 125000);
	  CAN_Init(CAN_1, 125000);	
    //Enable Interrupt
    CAN_IRQCmd(CAN_2, CANINT_RIE, ENABLE);

    CAN_IRQCmd(CAN_1, CANINT_RIE, ENABLE);

    //Enable CAN Interrupt
    NVIC_EnableIRQ(CAN_IRQn);

		// Accetta tutti gli IDs !
		CAN_SetAFMode(CAN_ACC_BP);
		//CAN_SetAFMode(CAN_NORMAL);
}

/*********************************************************************//**
 * @brief       Init AF-Look Up Table Sections entry value
 *              We setup entries for 5 sections:
 *              - 6 entries for FullCAN Frame Format Section
 *              - 6 entries for Explicit Standard ID Frame Format Section
 *              - 6 entries for Group of Standard ID Frame Format Section
 *              - 6 entries for Explicit Extended ID Frame Format Section
 *              - 6 entries for Group of Extended ID Frame Format Section
 * @param[in]   none
 * @return      none
 **********************************************************************/
void CAN_SetupAFTable(void)
{

    FullCAN_Table[0].controller = CAN_1;
    FullCAN_Table[0].disable = MSG_DISABLE;
    FullCAN_Table[0].id_11 = 0x01;

    SFF_Table[0].controller = CAN_1;
    SFF_Table[0].disable = MSG_ENABLE;
    SFF_Table[0].id_11 = 0x08;

    SFF_GPR_Table[0].controller1 = SFF_GPR_Table[0].controller2 = CAN_1;
    SFF_GPR_Table[0].disable1 = SFF_GPR_Table[0].disable2 = MSG_ENABLE;
    SFF_GPR_Table[0].lowerID = 0x10;
    SFF_GPR_Table[0].upperID = 0x20;

    EFF_Table[0].controller = CAN_1;
    EFF_Table[0].ID_29 = (1 << 11);

    EFF_GPR_Table[0].controller1 = EFF_GPR_Table[0].controller2 = CAN_1;
    EFF_GPR_Table[0].lowerEID = (5 << 11);
    EFF_GPR_Table[0].upperEID = (6 << 11);

    AFTable.FullCAN_Sec = &FullCAN_Table[0];
    AFTable.FC_NumEntry = 0;

    AFTable.SFF_Sec = &SFF_Table[0];
    AFTable.SFF_NumEntry = 1;

    AFTable.SFF_GPR_Sec = &SFF_GPR_Table[0];
    AFTable.SFF_GPR_NumEntry = 0;

    AFTable.EFF_Sec = &EFF_Table[0];
    AFTable.EFF_NumEntry = 0;

    AFTable.EFF_GPR_Sec = &EFF_GPR_Table[0];
    AFTable.EFF_GPR_NumEntry = 0;
}





/*********************************************************************//**
 * @brief       Initialize transmit and receive message for Bypass operation
 * @param[in]   type (INIT_MODULES, SET_OUTPUTS )    (0,1)
 * @return      none
 **********************************************************************/
void CAN_1_InitMessage( unsigned int type){
	
	switch (type)
	{
		case INIT_DEVICES:
			TXMsg.id 			 = 0x00;
			TXMsg.len 		 = 0x02;
			TXMsg.type  	 = DATA_FRAME;
			TXMsg.format   = STD_ID_FORMAT;
	
		// DATA HIGH
			TXMsg.dataB[3] = 0x00;
			TXMsg.dataB[2] = 0x00;
			TXMsg.dataB[1] = 0x00;
			TXMsg.dataB[0] = 0x00;	

		// DATA LOW
			TXMsg.dataA[3] = 0x00; 
			TXMsg.dataA[2] = 0x00;
			TXMsg.dataA[1] = 0x00;
			TXMsg.dataA[0] = 0x01; //LSByte !
		
		break;
		
//		case SET_OUTPUTS:
//			TXMsg.id 			 = (0x600  + 2);
//			TXMsg.len 		 = 0x08;
//			TXMsg.type  	 = DATA_FRAME;
//			TXMsg.format   = STD_ID_FORMAT;
//		
//		// DATA HIGH
//			TXMsg.dataB[3] = 0x00;
//			TXMsg.dataB[2] = 0x00;
//			TXMsg.dataB[1] = 0x00;
// 			TXMsg.dataB[0] = 0x00;	 //modules_array[1].outputs;

//		// DATA LOW
//			TXMsg.dataA[3] = 0x01; 
//			TXMsg.dataA[2] = 0x62;
//			TXMsg.dataA[1] = 0x00;
//			TXMsg.dataA[0] = 0x2F; //LSByte !			
//				
//		break;
	}
	
	CAN_SendMsg(CAN_1, &TXMsg);
	delay_ms(50);

}


/********************************************************************//**
 * @brief  setup_can_for_modules
 *********************************************************************/
 void CAN_1_setup_devices(void){
	
	unsigned int i = 0;
	unsigned char guard_time_H, guard_time_L;
	unsigned char life_time_factor;
	
	delay_ms(20);

// Manda il messaggio OPERATIONAL
	CAN_1_InitMessage(INIT_DEVICES);

	  //imposto watch dog per portare le uscite a OFF se manca la comunicazione con il Master
//    for (i=0; i<numero_blocco_valvole; i++){
    for (i=0; i<1; i++){
			guard_time_H = 0x04;// 0x0400 = 1024 ms = circa 1 sec
			guard_time_L = 0x00;
			life_time_factor = 0x28;// 0x28 = 40 --> 40 x 1024 ms ~ 41 sec

		//-----------------------------------------------------------------------
		// Manda guard time (1024 ms)
		TXMsg.len 		 = 0x08;
		TXMsg.type  	 = DATA_FRAME;
		TXMsg.format   = STD_ID_FORMAT;			
		
		TXMsg.id 			 = (0x600 + i + 1);
		
		// DATA HIGH
		TXMsg.dataB[3] = 0x00;
		TXMsg.dataB[2] = 0x00;
		TXMsg.dataB[1] = guard_time_H;
		TXMsg.dataB[0] = guard_time_L;

		// DATA LOW
		TXMsg.dataA[3] = 0x00;
		TXMsg.dataA[2] = 0x10;
		TXMsg.dataA[1] = 0x0C;
		TXMsg.dataA[0] = 0x2B; //LSByte !
			
		CAN_SendMsg(CAN_1, &TXMsg);
		delay_ms(100);
			
		//-----------------------------------------------------------------------
		// Manda life time factor (imposto per 41 secondi)
		TXMsg.len 		 = 0x08;
		TXMsg.type  	 = DATA_FRAME;
		TXMsg.format   = STD_ID_FORMAT;			
		
		TXMsg.id 			 = (0x600 + i + 1);
		
		// DATA HIGH
		TXMsg.dataB[3] = 0x00;
		TXMsg.dataB[2] = 0x00;
		TXMsg.dataB[1] = 0x00;	
		TXMsg.dataB[0] = life_time_factor; 
			
		// DATA LOW
		TXMsg.dataA[3] = 0x00; 
		TXMsg.dataA[2] = 0x10;
		TXMsg.dataA[1] = 0x0D;
		TXMsg.dataA[0] = 0x2F; //LSByte !
			
		// manda prima il dataA
		CAN_SendMsg(CAN_1, &TXMsg);
		delay_ms(100);	
			
  }

//  for (i=0; i<numero_blocco_valvole; i++){
  for (i=0; i<1; i++){
		//-----------------------------------------------------------------------
		// Manda NMT (� quello periodico)
		TXMsg.len 		 = 0x00;
		TXMsg.type  	 = DATA_FRAME;
		TXMsg.format   = STD_ID_FORMAT;			
		
		TXMsg.id 			 = (0x700 +i + 1);
		
		// DATA HIGH
			TXMsg.dataB[3] = 0x00;
			TXMsg.dataB[2] = 0x00;
			TXMsg.dataB[1] = 0x00;	
			TXMsg.dataB[0] = 0x00;	

		// DATA LOW
			TXMsg.dataA[3] = 0x00; 
			TXMsg.dataA[2] = 0x00;
			TXMsg.dataA[1] = 0x00;
			TXMsg.dataA[0] = 0x00; //LSByte !
			
			CAN_SendMsg(CAN_1, &TXMsg);	
			delay_ms(200);
  }
}




/*********************************************************************//**
 * @brief       ReInit_CAN - reinizializza periodicamente i CAN per evitare blocco
 * param[in]    none
 * @return      none
 **********************************************************************/

void CAN_ReInit (void){
  
//  if (CanBusFault){
    // CAN_1)
    PINSEL_ConfigPin (0, 0, 1);
    PINSEL_ConfigPin (0, 1, 1);
    // CAN_2
    PINSEL_ConfigPin (0, 4, 2);
    PINSEL_ConfigPin (0, 5, 2);

    //Initialize CAN1 & CAN2
// 	  CAN_Init(CAN_2, (uint32_t)(E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_2_BAUDRATE])*1000);
//	  CAN_Init(CAN_1, (uint32_t)(E2prom[E2_PAGE_3].E2_Int[E2_3_CAN_1_BAUDRATE])*1000);

// 20201005  
// 	  CAN_Init(CAN_2, 125000);
//	  CAN_Init(CAN_1, 125000);
 	  CAN_Init(CAN_2, 20000);
	  CAN_Init(CAN_1, 20000);

  
    //Enable Interrupt
    CAN_IRQCmd(CAN_2, CANINT_RIE, ENABLE);
    CAN_IRQCmd(CAN_1, CANINT_RIE, ENABLE);
   
	  //Enable CAN Interrupt
    NVIC_EnableIRQ(CAN_IRQn);
   
	  // Accetta tutti gli IDs !
    CAN_SetAFMode(CAN_ACC_BP);
//	}
}



/*********************************************************************//**
 * @brief       CAN_IRQ Handler, control receive message operation
 * param[in]    none
 * @return      none
 **********************************************************************/
void CAN_IRQHandler()
{
    uint8_t IntStatus;
		uint8_t IntStatus2;
		uint8_t i ;
//     uint32_t data1;

    /* get interrupt status
     * Note that: Interrupt register CANICR will be reset after read.
     * So function "CAN_IntGetStatus" should be call only one time
     */
		 
		 
		 //CAN 1
    IntStatus = CAN_IntGetStatus(CAN_1);

    //check receive interrupt
    if((IntStatus >> 0) & 0x01)
    {
        CAN_ReceiveMsg(CAN_1, &RXMsg);

        CAN1RxCount++; //count success received message

				/* Ho ricevuto un messaggio valido ! */
				// fai spazio al nuovo arrivato nel buffer
				for (i=0; i<(RX_BUFFER_CAN - 1); i++ ){
					can_data_array[RX_BUFFER_CAN - 1 - i][0] = can_data_array[RX_BUFFER_CAN - 2 - i][0];
					can_data_array[RX_BUFFER_CAN - 1 - i][1] = can_data_array[RX_BUFFER_CAN - 2 - i][1];
					can_data_array[RX_BUFFER_CAN - 1 - i][2] = can_data_array[RX_BUFFER_CAN - 2 - i][2];
					can_data_array[RX_BUFFER_CAN - 1 - i][3] = can_data_array[RX_BUFFER_CAN - 2 - i][3];
					
					can_data_array[RX_BUFFER_CAN - 1 - i][4] = can_data_array[RX_BUFFER_CAN - 2 - i][4];
					can_data_array[RX_BUFFER_CAN - 1 - i][5] = can_data_array[RX_BUFFER_CAN - 2 - i][5];
					can_data_array[RX_BUFFER_CAN - 1 - i][6] = can_data_array[RX_BUFFER_CAN - 2 - i][6];
					can_data_array[RX_BUFFER_CAN - 1 - i][7] = can_data_array[RX_BUFFER_CAN - 2 - i][7];					

					can_data_array[RX_BUFFER_CAN - 1 - i][8] = can_data_array[RX_BUFFER_CAN - 2 - i][8];
				}
				
				// e inserisci il nuovo arrivato nel buffer
				can_data_array[0][0] = RXMsg.dataA[0]; // LSByte
				can_data_array[0][1] = RXMsg.dataA[1];
				can_data_array[0][2] = RXMsg.dataA[2];
				can_data_array[0][3] = RXMsg.dataA[3];

				can_data_array[0][4] = RXMsg.dataB[0]; // LSByte
				can_data_array[0][5] = RXMsg.dataB[1];
				can_data_array[0][6] = RXMsg.dataB[2];
				can_data_array[0][7] = RXMsg.dataB[3];
				
				can_data_array[0][8] = RXMsg.id ;
				
				// incrementa il contatore dei messaggi validi presenti nel buffer (se il buffer non � gi� pieno)
				if (can_buffer_count < (RX_BUFFER_CAN) ) {
					can_buffer_count++;
				}
			new_message_can_1=1;				
    }

//CAN 2
    
		
//     uint32_t data1;

    /* get interrupt status
     * Note that: Interrupt register CANICR will be reset after read.
     * So function "CAN_IntGetStatus" should be call only one time
     */
    IntStatus2 = CAN_IntGetStatus(CAN_2);

    //check receive interrupt
    if((IntStatus2 >> 0) & 0x01)
    {
        CAN_ReceiveMsg(CAN_2, &RXMsg);

        CAN2RxCount++; //count success received message

				/* Ho ricevuto un messaggio valido ! */
				// fai spazio al nuovo arrivato nel buffer
				for (i=0; i<(RX_BUFFER_CAN - 1); i++ ){
					can_data_array2[RX_BUFFER_CAN - 1 - i][0] = can_data_array2[RX_BUFFER_CAN - 2 - i][0];
					can_data_array2[RX_BUFFER_CAN - 1 - i][1] = can_data_array2[RX_BUFFER_CAN - 2 - i][1];
					can_data_array2[RX_BUFFER_CAN - 1 - i][2] = can_data_array2[RX_BUFFER_CAN - 2 - i][2];
					can_data_array2[RX_BUFFER_CAN - 1 - i][3] = can_data_array2[RX_BUFFER_CAN - 2 - i][3];
					
					can_data_array2[RX_BUFFER_CAN - 1 - i][4] = can_data_array2[RX_BUFFER_CAN - 2 - i][4];
					can_data_array2[RX_BUFFER_CAN - 1 - i][5] = can_data_array2[RX_BUFFER_CAN - 2 - i][5];
					can_data_array2[RX_BUFFER_CAN - 1 - i][6] = can_data_array2[RX_BUFFER_CAN - 2 - i][6];
					can_data_array2[RX_BUFFER_CAN - 1 - i][7] = can_data_array2[RX_BUFFER_CAN - 2 - i][7];					

					can_data_array2[RX_BUFFER_CAN - 1 - i][8] = can_data_array2[RX_BUFFER_CAN - 2 - i][8];
				}
				
				// e inserisci il nuovo arrivato nel buffer
				can_data_array2[0][0] = RXMsg.dataA[0]; // LSByte
				can_data_array2[0][1] = RXMsg.dataA[1];
				can_data_array2[0][2] = RXMsg.dataA[2];
				can_data_array2[0][3] = RXMsg.dataA[3];

				can_data_array2[0][4] = RXMsg.dataB[0]; // LSByte
				can_data_array2[0][5] = RXMsg.dataB[1];
				can_data_array2[0][6] = RXMsg.dataB[2];
				can_data_array2[0][7] = RXMsg.dataB[3];
				
				can_data_array2[0][8] = RXMsg.id ;
				
				// incrementa il contatore dei messaggi validi presenti nel buffer (se il buffer non � gi� pieno)
				if (can_buffer_count2 < (RX_BUFFER_CAN) ) {
					can_buffer_count2++;
				}
				new_message_can_2=1;			
    }
}



/********************************************************************//**
 * @brief       check_id
 *********************************************************************/
unsigned int CAN_check_id(unsigned int id, unsigned int type){
	
	unsigned int i 		= 0;
	unsigned int temp = 0;
	unsigned int result = 0;
	
	switch (type)
	{
		case OUTPUT_ID:
			id -= 0x580;
			if (id == 1){
				result = id;
			}
		break;
		
		case NMT_ID:
			id -= 0x700;
			if (id == 1){
				result = id;
			}
		break;
			
//		case EWON:
//			id -= 0x580;
//			// l'indirizzo dell'anybus � uguale al numero di moduli + 1 !
//			if (id == (sys_modules_num + 1)) {
//				result = 1;
//			} else {
//				result = 0;
//			}			
//		break;
//		
//		case FROM_CIE_ID:
//			id -= 0x600;
//			for (i=0; i<sys_modules_num; i++){
//				if (id == (modules_array[i].id + 1) ){
//					temp = id;
//				}
//			}
//			if (temp != 0) {
//				result = temp;
//			} else {
//				result = 0;
//			}			
//		break;
	}

	return(result);
}

/********************************************************************//**
 * @brief       read_can_1_buffer
 *********************************************************************/
void CAN_1_read_buffer(void){
		  char k;
			char offset_temp;
			unsigned int i = 0;
			uint8_t data_high[4];
			uint8_t data_low[4];
			unsigned int zone = 0;
			unsigned int temp_id = 0;
			unsigned int msg = 0;
			unsigned int display_id = 0;
			unsigned int temp = 0;
  unsigned int tmp_can_buffer_count;
	unsigned int initial_can_buffer_count;
	float temp_variable;

	if(new_message_can_1==1){
		new_message_can_1=0;

		initial_can_buffer_count = can_buffer_count;
		tmp_can_buffer_count = initial_can_buffer_count;
		// Leggi tutti messaggi	
		for (i=0; i<RX_BUFFER_CAN; i++ ){
  		if (tmp_can_buffer_count){
				data_low[0] = can_data_array[(tmp_can_buffer_count - 1)][0];	// LSByte
				data_low[1] = can_data_array[(tmp_can_buffer_count - 1)][1];	// 
				data_low[2] = can_data_array[(tmp_can_buffer_count - 1)][2];	// 
				data_low[3] = can_data_array[(tmp_can_buffer_count - 1)][3];	// 
					
				data_high[0] = can_data_array[(tmp_can_buffer_count - 1)][4];	// LSByte
				data_high[1] = can_data_array[(tmp_can_buffer_count - 1)][5];	// 
				data_high[2] = can_data_array[(tmp_can_buffer_count - 1)][6];	// 
				data_high[3] = can_data_array[(tmp_can_buffer_count - 1)][7];	// 
						
				temp_id 		 = can_data_array[(tmp_can_buffer_count - 1)][8]; // ID (standard, 11 bits)
						
				// decrementa il contatore dei messaggi disponibili nel buffer
				tmp_can_buffer_count = (tmp_can_buffer_count)? --tmp_can_buffer_count: 0 ;
				if (tmp_can_buffer_count==0){
					i = RX_BUFFER_CAN;//esce dal ciclo
					can_buffer_count -= initial_can_buffer_count;
				}
					
				//controlla se � la risposta dello slave al messaggio NMT
				msg = CAN_check_id(temp_id, NMT_ID);
				if (msg==1){
					// ATTENZIONE: commento questo per compatibilit� con valvole SMC - verificare se va bene con tutte e se � OK
//					if ( (data_low[0]==0x05) || (data_low[0]==0x85) ){
            CANcount[0] = 0;
//					}
				}	
//				//controlla se � la risposta dello slave al messaggio di impostazione uscite
//				msg = CAN_check_id(temp_id, OUTPUT_ID);
//				if (msg==1){
//					if (data_low[0]==0x60){
//            CANcount[0] = 0;
//					}
//				}
			}
		}
	}
}


/********************************************************************//**
 * @brief       read_can_2_buffer
 *********************************************************************/
void CAN_2_read_buffer(void){
	if(new_message_can_2==1){

		new_message_can_2=0;
	}
}

/********************************************************************//**
 * @brief       CAN_1_NMT_message
*********************************************************************/
void CAN_1_NMT_message (void){
	
	TXMsg.len 		 = 0x08;
	TXMsg.type  	 = REMOTE_FRAME;
	TXMsg.format   = STD_ID_FORMAT;			
				
	TXMsg.id 			 = (0x700 + 1);
				
	// DATA HIGH
	TXMsg.dataB[3] = 0x00;
	TXMsg.dataB[2] = 0x00;
	TXMsg.dataB[1] = 0x00;
	TXMsg.dataB[0] = 0x00;
			
	// DATA LOW
	TXMsg.dataA[3] = 0x00;
	TXMsg.dataA[2] = 0x00;
	TXMsg.dataA[1] = 0x00;
	TXMsg.dataA[0] = 0x00;
				
	CAN_SendMsg(CAN_1, &TXMsg);
}


/********************************************************************//**
 * @brief       CAN_1_polling
*********************************************************************/
void CAN_1_polling(void){

//  char communication_offset=0;
//  char i,j,k;
//  char offset_temp;
//  unsigned int num_item;
//	unsigned int temp 		 = 0;
//	unsigned int temp_high = 0;
	
  if (CANcount[0]>=50){
    CANcount[0] = 40;
    Adox1500.StatoSlave |= CAN_FAULT;
    Adox1500work();//per arresto macchina causa mancanza comunicazione con blocco valvole
    operational = 0;
  }
  //prova a reinizializzare il blocco valvole
  if ( (CANcount[0]>=30) && (CANcount[0]<40) ){
    CAN_ReInit();
	  CAN_1_InitMessage(INIT_DEVICES);
    operational = 1;
  }
  if ( ((operational==0) && (CANcount[0]==0)) || (CANcount[0]>=40) || (tmrCanReinit==0) ){
    tmrCanReinit = CAN_REINIT;
    CAN_ReInit();
    Adox1500.StatoSlave &= ~CAN_FAULT;
    CAN_1_setup_devices();
    operational = 1;
  }
  else if (operational==1){
//Adox1500.Output = LED_output;		//eliminare solo per prova
		TXMsg.len 		 = 0x08;
		TXMsg.type  	 = DATA_FRAME;
		TXMsg.format   = STD_ID_FORMAT;			
				
		TXMsg.id 			 = (0x200 + 1);
				
		// DATA HIGH
		TXMsg.dataB[3] = 0x00;
		TXMsg.dataB[2] = 0x00;
		TXMsg.dataB[1] = 0x00;
		TXMsg.dataB[0] = 0x00;
			
		// DATA LOW
		TXMsg.dataA[3] = (unsigned char)((Adox1500.Output>>24) & 0x000000FF);// MSB
		TXMsg.dataA[2] = (unsigned char)((Adox1500.Output>>16) & 0x000000FF);
		TXMsg.dataA[1] = (unsigned char)((Adox1500.Output>>8) & 0x000000FF);
		TXMsg.dataA[0] = (unsigned char)(Adox1500.Output & 0x000000FF);// LSB

		CAN_SendMsg(CAN_1, &TXMsg);

    ++CANcount[0];
////GuiVar_integer_4 = CANcount[0];
  }
}




void CAN_1_set_date(void){
	
	TXMsg.len 		 = 0x08;
	TXMsg.type  	 = DATA_FRAME;
	TXMsg.format   = STD_ID_FORMAT;
							
	TXMsg.id 			 = (0x06);
			
	// DATA HIGH
	TXMsg.dataB[3] = 255;//can_displays_polling_index_3;
	TXMsg.dataB[2] = O_TYPE_EXT_DISPLAY;
  TXMsg.dataB[1] = RTCFullTime.YEAR-2000;
	TXMsg.dataB[0] = RTCFullTime.MONTH;	
	// DATA LOW
	TXMsg.dataA[3] = RTCFullTime.DOM;
	TXMsg.dataA[2] = RTCFullTime.HOUR;
	TXMsg.dataA[1] = RTCFullTime.MIN;
	TXMsg.dataA[0] = RTCFullTime.SEC; //LSByte !
			
	CAN_SendMsg(CAN_1, &TXMsg);

}

