

#include "../logic.h"
#include "../communication.h"
#include "../analog.h"
#include "../init.h"


/* ########################## */
/* 		SYSTEM	CONFIGURATION	  */
/* ########################## */


// Nuovo Adox su SBM

// 2020.10.05 ver A1.10.000 : - Riportato gestione touch da PSA
//                            - Modificato gestione ADC 9 con due stati operativi differenti per init e lettura dato 
//                              (necesario per evitare attese troppo lunghe che fermano tutta la macchina...)
//                            

// ver 1.03 da fare: schermate impostazioni


// importo da:

// ADOX 100/200/800/1300/1500 ver 4.11 Adox N2 2016 le funzioni logiche di controllo e la comunicazione e le strutture
// cercare nei vari files "eliminare" usato solo per prove temporanee di alcune funzionalit�

// TO DO:
// - gestire guasti/eventi visualizzazione e salvataggio su SD
// - ingressi analogici 2 PT100, 3 trasduttori di pressione 4-20mA, 1 flussimetro (opzionale) 4-20mA - DONE
// - grafica impostazioni macchina
// - grafica stato funzionamento macchina (schermata principale)
// - grafica msg di diagnostica comunicazione con le altre periferiche (CAN-RS485)














//#define DEBUG_MODE //// ELIMINARE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


//#define ENABLE_WDOG

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

#define FIRMWARE_VERSION       1.03
#define BOARD_NUMBER            192

// 20201002
#define UPDATE_SCREEN_ON_PRESS // Aggiornamento screen su PRESSIONE tasto  NON in RILASCIO per velocizzare effetto Touch
// 20201002
#define READ_TOUCH_SCREEN    // Gestione Touch screen con lettura I2C separata dalla funzione di riconoscimneto area touch



////#define ANSWER_DELAY      0   //in decimi di sec risponde al master in un tempo compreso tra ANSWER_DELAY-1 e ANSWER_DELAY
//#define MAX_WAIT4BYTE     500   //us tempo massimo attesa byte successivo

// numero massimo di elementi; devo definire questi perch� non posso creare array con dimensione non definita
// sono i numeri massimi di elementi con cui si pu� configurare il sistema

#define Port_0 	0
#define Port_1 	1
#define Port_2 	2
#define Port_3 	3
#define Port_4 	4
#define Port_5 	5

#define RELE_1       1   //P5.1
#define RELE_2      15   //P4.15
#define RELE_3      16   //P4.16
#define RELE_4      17   //P4.17
#define RELE_5      18   //P4.18
#define RELE_6      19   //P4.19

#define P_O2_CMD     0   // CMD O2

#define P_E2PROM_WP  4   // abilita scrittura su E2PROM

#define P_PWM_LCD   18   // PWM backlight LCD

#define I2C2_SDA 						15		// P1.15 data
#define I2C2_SCL 						21		// P4.21 clock
#define TOUCH_RST           30    // P4.30 reset
#define TOUCH_INT           31    // P4.31 interrupt
#define TOUCHING (1-((GPIO_ReadValue(Port_4)>>TOUCH_INT)&1))

//#define CS_PRESS						13		// P3.13 ChipSelect sensore pressione (se low abilita il chip)
#define PRESS_MCLK					  20		// P4.20 I2C Pressure sensor master clock (adc interna)

// LED
#define uCS2_4                 2     // P5.02 chip select per driver LED   
#define LEDENABLE             14     // P0.14 abilita/disabilita uscita driver LED   

//// ADC_5
//#define uCS2_1                 3     // P5.03 chip select per ADC 5   

/* ########################## */
/* 		RTC REGISTER            */
/* ########################## */
// 5 registri BACK_UP in RTC ( 20 byte = 5 registri da 4 byte ciscuno (32bits ) )
#define RTC_REG_0 	        0
#define RTC_REG_1         	1
#define RTC_REG_2						2
#define RTC_REG_3						3
#define RTC_REG_4						4  // contatore reset scheda per disabilitare SD //2018.02.23

//SSP2 - SPI 2
#define SSP2_PORT  (LPC_SSP2)   /* LPC4088 */
//#define SSP2_CLOCK (10000000)
// SEL per SSP2
#define SSP2_SEL0_0		(LPC_GPIO1->CLR |= (1<<13)) // p1.13 -> low 
#define SSP2_SEL0_1		(LPC_GPIO1->SET |= (1<<13)) // p1.13 -> high 

#define SSP2_SEL1_0		(LPC_GPIO1->CLR |= (1<<14)) // p1.14 -> low 
#define SSP2_SEL1_1		(LPC_GPIO1->SET |= (1<<14)) // p1.14 -> high

#define SSP2_CS                 8     // P1.08 chip select per ADUM3154 di SSP2


//define per LED_output
#define LED_1               0x01
#define LED_2               0x02
#define LED_3               0x04
#define LED_4               0x08
#define LED_5               0x10
#define LED_6               0x20
#define LED_7               0x40
#define LED_8               0x80

//pesi molecolari
#define pm_o2		32.00
#define pm_n2		28.0134
#define pm_co2	44.0095
#define pm_ar		39.948
#define pm_h2o	18.01505
#define R_gas		8.314472	//costante universale dei gas
#define R_air		287.06 		//J/kgK
#define R_h2o		461.52 		//J/kgK

//O2 SENSORS
//con questi valori sembra meglio non ho le oscilaazioni di 3% che con i valori originali, ora in aria oscilla di max 0.4-0.5%
#define NPEAKS 					  10		// 20		numero di picchi per treno
#define TRENI 					  10		// 20	
#define SCARTI 						2			// 2 	vengono scartati i n=SCARTI pi� alti e pi� bassi
#define ADC_SAMPLE_TIME 	2.5		// 2.5	usec
#define TIMECORRECTION		0			// 2.5 usec
#define O2_AVG_MOBILE		 10			// numero campioni media mobile
#define AVG_START 3   // originale 2
#define AVG_STOP  22	// originale 20			// DMA_SIZE/2
#define NSAMPLES				20
#define DMA_SIZE					40		// 40	(con 40 ho oscillazioni del 2%)	numero di campioni per picco		/** DMA size of transfer */ /* DMA_SIZE non deve superare ca 80 campioni altrimenti la il trasferimento in matrice necesasita di un tempo superiore a mezzo periodo di eccitazione*/
#define PI				3.141592653590

#define DIM_CHART      50   //numero punti asse x (grafico)

//cercare e correggere: "//sistemare!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

//versione con define --->
#define CONSTANT_ZONES           10 
#define CONSTANT_SENSORS         50 
#define CONSTANT_SIRENS          10
#define CONSTANT_PANELS          10
#define CONSTANT_LAMPS           10
#define CONSTANT_VALVES          10
#define CONSTANT_EXT_DISPLAYS    30
#define CONSTANT_FLUXES          10
#define CONSTANT_MODULES         15
	#define MODULES_INPUT_NUM       8
	#define MODULES_OUTPUT_NUM      8
#define CONSTANT_PW_SUPPLY       15
#define CONSTANT_BATTERIES       15
#define CONSTANT_PSA             10
#define CONSTANT_THERM            2
#define CONSTANT_SWITCHES        10
#define CONSTANT_FANS             1
#define CONSTANT_DOORS            1


#define NUMBER_OF_ZONES            6
#define NUMBER_OF_SENSORS          6
#define NUMBER_OF_SIRENS           1 // compreso quella della centrale (in realt� 5, ma ciascuna CIE comanda la sua e le 3 esterne una per zona)
#define NUMBER_OF_LAMPS            1 // compreso quello della centrale (in realt� 5, ma ciascuna CIE comanda la sua e le 3 esterne una per zona)
#define NUMBER_OF_PANELS           0
#define NUMBER_OF_VALVES           3 // compresa quella ausiliaria che non c'�
#define NUMBER_OF_FLUXES           3 // compreso quello ausiliario che non c'�
#define NUMBER_OF_EXT_DISPLAYS     3
#define NUMBER_OF_LEDS             8 
#define NUMBER_OF_MODULES          6 // ciascuna CIE controlla da sola i suoi 2 e i 7 esterni comuni
#define NUMBER_OF_PW_SUPPLY        1 // compresi quelli delle centrali
#define NUMBER_OF_BATTERIES        1 // compresi quelli delle centrali
#define NUMBER_OF_PSA              1 // in realt� sono 4 Adox, ma ho cambiato le scritte
#define NUMBER_OF_BUZZER           1
#define NUMBER_OF_DOORS            0
#define NUMBER_OF_FANS             0
#define NUMBER_OF_ADOX             0
#define NUMBER_OF_SWITCHES         0

#define NUMBER_OF_OBJECTS (NUMBER_OF_ZONES + NUMBER_OF_SENSORS + NUMBER_OF_SIRENS + NUMBER_OF_LAMPS + NUMBER_OF_PANELS + NUMBER_OF_VALVES + NUMBER_OF_EXT_DISPLAYS + NUMBER_OF_FLUXES + NUMBER_OF_BUZZER)
#define ITEM_COLUMNS 5
#define MAX_ITEM          (NUMBER_OF_SENSORS + NUMBER_OF_SIRENS + NUMBER_OF_LAMPS +  NUMBER_OF_PANELS + NUMBER_OF_VALVES + NUMBER_OF_FLUXES + NUMBER_OF_EXT_DISPLAYS + NUMBER_OF_LEDS + NUMBER_OF_PW_SUPPLY + NUMBER_OF_BATTERIES + NUMBER_OF_PSA + NUMBER_OF_BUZZER + NUMBER_OF_DOORS + NUMBER_OF_FANS + NUMBER_OF_ADOX)
#define LEN3 12
#define LEN4 22



//versione con define --->
#define N_SENSORS_Z_1            2
#define N_SENSORS_Z_2            2
#define N_SENSORS_Z_3            2
#define N_SENSORS_Z_4            0
#define N_SENSORS_Z_5            0
#define N_SENSORS_Z_6            0
#define N_SENSORS_Z_7            0
#define N_SENSORS_Z_8            0

#define N_SIRENS_Z_1             0
#define N_SIRENS_Z_2             0
#define N_SIRENS_Z_3             0
#define N_SIRENS_Z_4             0
#define N_SIRENS_Z_5             0
#define N_SIRENS_Z_6             0
#define N_SIRENS_Z_7             0
#define N_SIRENS_Z_8             0

#define N_LAMPS_Z_1              0
#define N_LAMPS_Z_2              0
#define N_LAMPS_Z_3              0
#define N_LAMPS_Z_4              0
#define N_LAMPS_Z_5              0
#define N_LAMPS_Z_6              0
#define N_LAMPS_Z_7              0
#define N_LAMPS_Z_8              0

#define N_PANELS_Z_1             0
#define N_PANELS_Z_2             0
#define N_PANELS_Z_3             0
#define N_PANELS_Z_4             0
#define N_PANELS_Z_5             0
#define N_PANELS_Z_6             0
#define N_PANELS_Z_7             0
#define N_PANELS_Z_8             0

#define N_VALVES_Z_1             1
#define N_VALVES_Z_2             1
#define N_VALVES_Z_3             1
#define N_VALVES_Z_4             0
#define N_VALVES_Z_5             0
#define N_VALVES_Z_6             0
#define N_VALVES_Z_7             0
#define N_VALVES_Z_8             0

#define N_EXT_DISPLAYS_Z_1       1
#define N_EXT_DISPLAYS_Z_2       1
#define N_EXT_DISPLAYS_Z_3       1
#define N_EXT_DISPLAYS_Z_4       0
#define N_EXT_DISPLAYS_Z_5       0
#define N_EXT_DISPLAYS_Z_6       0
#define N_EXT_DISPLAYS_Z_7       0
#define N_EXT_DISPLAYS_Z_8       0

#define N_FLUXES_Z_1             1
#define N_FLUXES_Z_2             1
#define N_FLUXES_Z_3             1
#define N_FLUXES_Z_4             0
#define N_FLUXES_Z_5             0
#define N_FLUXES_Z_6             0
#define N_FLUXES_Z_7             0
#define N_FLUXES_Z_8             0
// <-- versione con define

// comune -->
/* ###################################### */
/* 		    	I/O DEFINITIONS         			*/
/* ###################################### */
// Moduli della centrale: 1 & 2
// Modulo stanze: 3
// Modulo sala macchine: 4
#define ACTIVE_HIGH							 1
#define ACTIVE_LOW 							 0
#define ON											 1
#define OFF				 							 0
#define NMT_ID  								 1
#define OUTPUT_ID	 							 2
//#define EWON     								 3
//#define FROM_CIE_ID              4
// <-- comune


//versione con define --->
/* OUTPUTS */
//-------------------------------------------------------------
// SIRENS
#ifdef MASTER_CIE
 #define SIREN_0_MODULE_1    		 2
#else
 #define SIREN_0_MODULE_1    		 4
#endif
#define SIREN_0_MODULE_1_OUT		 1
#define SIREN_0_MODULE_2    		 0
#define SIREN_0_MODULE_2_OUT		 0
#define SIREN_0_MODULE_3    		 0
#define SIREN_0_MODULE_3_OUT		 0
#define SIREN_0_LOGIC						 ACTIVE_HIGH

#define SIREN_1_MODULE_1    		 8
#define SIREN_1_MODULE_1_OUT		 3
#define SIREN_1_MODULE_2    		 0
#define SIREN_1_MODULE_2_OUT		 0
#define SIREN_1_MODULE_3    		 0
#define SIREN_1_MODULE_3_OUT		 0
#define SIREN_1_LOGIC						 ACTIVE_HIGH

#define SIREN_2_MODULE_1    		 9
#define SIREN_2_MODULE_1_OUT		 3
#define SIREN_2_MODULE_2    		 0
#define SIREN_2_MODULE_2_OUT		 0
#define SIREN_2_MODULE_3    		 0
#define SIREN_2_MODULE_3_OUT		 0
#define SIREN_2_LOGIC						 ACTIVE_HIGH

#define SIREN_3_MODULE_1    		10
#define SIREN_3_MODULE_1_OUT		 3
#define SIREN_3_MODULE_2    		 0
#define SIREN_3_MODULE_2_OUT		 0
#define SIREN_3_MODULE_3    		 0
#define SIREN_3_MODULE_3_OUT		 0
#define SIREN_3_LOGIC						 ACTIVE_HIGH

//-------------------------------------------------------------
// LAMPS
#ifdef MASTER_CIE
 #define LAMP_0_MODULE_1   		 2
#else
 #define LAMP_0_MODULE_1   		 4
#endif
#define LAMP_0_MODULE_1_OUT		 2
#define LAMP_0_MODULE_2    		 0
#define LAMP_0_MODULE_2_OUT		 0
#define LAMP_0_MODULE_3    		 0
#define LAMP_0_MODULE_3_OUT		 0
#define LAMP_0_LOGIC						 ACTIVE_HIGH

#define LAMP_1_MODULE_1    		 8
#define LAMP_1_MODULE_1_OUT		 2
#define LAMP_1_MODULE_2    		 0
#define LAMP_1_MODULE_2_OUT		 0
#define LAMP_1_LOGIC						 ACTIVE_HIGH

#define LAMP_2_MODULE_1    		 9
#define LAMP_2_MODULE_1_OUT		 2
#define LAMP_2_MODULE_2    		 0
#define LAMP_2_MODULE_2_OUT		 0
#define LAMP_2_LOGIC						 ACTIVE_HIGH

#define LAMP_3_MODULE_1    		10
#define LAMP_3_MODULE_1_OUT		 2
#define LAMP_3_MODULE_2    		 0
#define LAMP_3_MODULE_2_OUT		 0
#define LAMP_3_LOGIC						 ACTIVE_HIGH

//-------------------------------------------------------------
// PANELS
#define PANEL_1_MODULE_1    		 8
#define PANEL_1_MODULE_1_OUT		 1
#define PANEL_1_MODULE_2    		 0
#define PANEL_1_MODULE_2_OUT		 0
#define PANEL_1_LOGIC						 ACTIVE_HIGH

#define PANEL_2_MODULE_1    		 9
#define PANEL_2_MODULE_1_OUT		 1
#define PANEL_2_MODULE_2    		 0
#define PANEL_2_MODULE_2_OUT		 0
#define PANEL_2_LOGIC						 ACTIVE_HIGH

#define PANEL_3_MODULE_1    		10
#define PANEL_3_MODULE_1_OUT		 1
#define PANEL_3_MODULE_2    		 0
#define PANEL_3_MODULE_2_OUT		 0
#define PANEL_3_LOGIC						 ACTIVE_HIGH

//-------------------------------------------------------------
// VALVES
#define VALVE_0_MODULE_1    		 5 // non usata
#define VALVE_0_MODULE_1_OUT		 5 // non usata
#define VALVE_0_MODULE_2    		 0
#define VALVE_0_MODULE_2_OUT		 0
#define VALVE_0_LOGIC						 ACTIVE_HIGH

#define VALVE_1_MODULE_1    		 5
#define VALVE_1_MODULE_1_OUT		 1
#define VALVE_1_MODULE_2    		 0
#define VALVE_1_MODULE_2_OUT		 0
#define VALVE_1_LOGIC						 ACTIVE_HIGH

#define VALVE_2_MODULE_1    		 5
#define VALVE_2_MODULE_1_OUT		 2
#define VALVE_2_MODULE_2    		 0
#define VALVE_2_MODULE_2_OUT		 0
#define VALVE_2_LOGIC						 ACTIVE_HIGH

#define VALVE_3_MODULE_1    		 5
#define VALVE_3_MODULE_1_OUT		 3
#define VALVE_3_MODULE_2    		 0
#define VALVE_3_MODULE_2_OUT		 0
#define VALVE_3_LOGIC						 ACTIVE_HIGH

#define VALVE_4_MODULE_1    		 5
#define VALVE_4_MODULE_1_OUT		 4
#define VALVE_4_MODULE_2    		 0
#define VALVE_4_MODULE_2_OUT		 0
#define VALVE_4_LOGIC						 ACTIVE_HIGH

//-------------------------------------------------------------
// LEDS (CIE)
/* alarm */
#ifdef MASTER_CIE
 #define LED_0_MODULE_1    		 1
#else
 #define LED_0_MODULE_1    		 3
#endif
#define LED_0_MODULE_1_OUT		 1
#define LED_0_MODULE_2    		 0
#define LED_0_MODULE_2_OUT		 0
#define LED_0_LOGIC						 ACTIVE_HIGH

/* general faults */
#ifdef MASTER_CIE
 #define LED_1_MODULE_1    		 1
#else
 #define LED_1_MODULE_1    		 3
#endif
#define LED_1_MODULE_1_OUT		 2
#define LED_1_MODULE_2    		 0
#define LED_1_MODULE_2_OUT		 0
#define LED_1_LOGIC						 ACTIVE_HIGH

/* system faults */
#ifdef MASTER_CIE
 #define LED_2_MODULE_1    		 1
#else
 #define LED_2_MODULE_1    		 3
#endif
#define LED_2_MODULE_1_OUT		 3
#define LED_2_MODULE_2    		 0
#define LED_2_MODULE_2_OUT		 0
#define LED_2_LOGIC						 ACTIVE_HIGH

/* disablements */
#ifdef MASTER_CIE
 #define LED_3_MODULE_1    		 1
#else
 #define LED_3_MODULE_1    		 3
#endif
#define LED_3_MODULE_1_OUT		 5
#define LED_3_MODULE_2    		 0
#define LED_3_MODULE_2_OUT		 0
#define LED_3_LOGIC						 ACTIVE_HIGH

/* dev ctrl */
#ifdef MASTER_CIE
 #define LED_4_MODULE_1    		 1
#else
 #define LED_4_MODULE_1    		 3
#endif
#define LED_4_MODULE_1_OUT		 4
#define LED_4_MODULE_2    		 0
#define LED_4_MODULE_2_OUT		 0
#define LED_4_LOGIC						 ACTIVE_HIGH

/* energy faults */
#ifdef MASTER_CIE
 #define LED_5_MODULE_1    		 1
#else
 #define LED_5_MODULE_1    		 3
#endif
#define LED_5_MODULE_1_OUT		 6
#define LED_5_MODULE_2    		 0
#define LED_5_MODULE_2_OUT		 0
#define LED_5_LOGIC						 ACTIVE_HIGH

/* aux. N2 supply */
#ifdef MASTER_CIE
 #define LED_6_MODULE_1    		 1
#else
 #define LED_6_MODULE_1    		 3
#endif
#define LED_6_MODULE_1_OUT		 7
#define LED_6_MODULE_2    		 0
#define LED_6_MODULE_2_OUT		 0
#define LED_6_LOGIC						 ACTIVE_HIGH

/* Power OK */
#ifdef MASTER_CIE
 #define LED_7_MODULE_1    		 1
#else
 #define LED_7_MODULE_1    		 3
#endif
#define LED_7_MODULE_1_OUT		 8
#define LED_7_MODULE_2    		 0
#define LED_7_MODULE_2_OUT		 0
#define LED_7_LOGIC						 ACTIVE_HIGH

//-------------------------------------------------------------
// BUZZER (CIE)
#ifdef MASTER_CIE
 #define BUZZER_MODULE_1   		 2
#else
 #define BUZZER_MODULE_1   		 4
#endif
#define BUZZER_MODULE_1_OUT		 3
#define BUZZER_MODULE_2    		 0
#define BUZZER_MODULE_2_OUT		 0
#define BUZZER_LOGIC					 ACTIVE_HIGH

//-------------------------------------------------------------
// EXT. FAULT (CIE)
#ifdef MASTER_CIE
 #define EXT_FAULT_MODULE_1   		 2
#else
 #define EXT_FAULT_MODULE_1   		 4
#endif
#define EXT_FAULT_MODULE_1_OUT		 7
#define EXT_FAULT_MODULE_2    		 0
#define EXT_FAULT_MODULE_2_OUT		 0
#define EXT_FAULT_LOGIC					   ACTIVE_HIGH

//-------------------------------------------------------------
// EXT. ALARM (CIE)
#ifdef MASTER_CIE
 #define EXT_ALARM_MODULE_1   		 2
#else
 #define EXT_ALARM_MODULE_1   		 4
#endif
#define EXT_ALARM_MODULE_1_OUT		 8
#define EXT_ALARM_MODULE_2    		 0
#define EXT_ALARM_MODULE_2_OUT		 0
#define EXT_ALARM_LOGIC					   ACTIVE_HIGH

// PSA START                                            /// rivedere e inserire codice per comando PSA (vedere in LPG comando ADOX)
//#define PSA_0_START_MODULE_1    	   	 6
//#define PSA_0_START_MODULE_1_OUT		   1
//#define PSA_0_START_LOGIC 					 ACTIVE_HIGH

//#define PSA_1_START_MODULE_1    	   	 7
//#define PSA_1_START_MODULE_1_OUT		   1
//#define PSA_1_START_LOGIC 					 ACTIVE_HIGH

/* INPUTS */
//-------------------------------------------------------------
// FLUXES
#ifdef MASTER_CIE
 #define FLUX_0_MODULE_1   		 0
#else
 #define FLUX_0_MODULE_1   		 0
#endif
#define FLUX_0_MODULE_1_IN		 0
#define FLUX_0_LOGIC					 ACTIVE_HIGH

#define FLUX_1_MODULE_1    		 6
#define FLUX_1_MODULE_1_IN		 1
#define FLUX_1_LOGIC					 ACTIVE_HIGH

#define FLUX_2_MODULE_1    		 6
#define FLUX_2_MODULE_1_IN		 2
#define FLUX_2_LOGIC					 ACTIVE_HIGH

#define FLUX_3_MODULE_1    		 6
#define FLUX_3_MODULE_1_IN		 3
#define FLUX_3_LOGIC					 ACTIVE_HIGH

//-------------------------------------------------------------
// POSITION SWITCHES
#define SWITCH_1_MODULE   		 5
#define SWITCH_1_OFF		       1
#define SWITCH_1_ON					   2
#define SWITCH_1_LOGIC				 ACTIVE_HIGH

#define SWITCH_2_MODULE   		 5
#define SWITCH_2_OFF		       3
#define SWITCH_2_ON					   4
#define SWITCH_2_LOGIC				 ACTIVE_HIGH

#define SWITCH_3_MODULE   		 5
#define SWITCH_3_OFF		       5
#define SWITCH_3_ON					   6
#define SWITCH_3_LOGIC				 ACTIVE_HIGH

//-------------------------------------------------------------
// THERMOSTAT
#define THERMOSTAT_MODULE_1    		 6
#define THERMOSTAT_MODULE_1_IN		 8
#define THERMOSTAT_MODULE_2    		 7
#define THERMOSTAT_MODULE_2_IN		 8
#define THERMOSTAT_LOGIC					 ACTIVE_LOW // N/C


//-------------------------------------------------------------
// EXT. FAULT (IN)
#ifdef MASTER_CIE
 #define EXT_FAULT_IN_MODULE_1   		 2
#else
 #define EXT_FAULT_IN_MODULE_1   		 4
#endif
#define EXT_FAULT_IN_MODULE_1_IN		 7
#define EXT_FAULT_IN_LOGIC					 ACTIVE_HIGH // N/O

//-------------------------------------------------------------
// EXT. ALARM (IN)
#ifdef MASTER_CIE
 #define EXT_ALARM_IN_MODULE_1   		 2
#else
 #define EXT_ALARM_IN_MODULE_1   		 4
#endif
#define EXT_ALARM_IN_MODULE_1_IN		 8
#define EXT_ALARM_IN_LOGIC					 ACTIVE_HIGH

//-------------------------------------------------------------
// PSA
#define PSA_0_MODULE_1    		 6
#define PSA_0_MODULE_1_IN		   7
#define PSA_0_MODULE_1_OUT	   1
#define PSA_0_IN_LOGIC 				 ACTIVE_HIGH // N/O
#define PSA_0_OUT_LOGIC 			 ACTIVE_HIGH

#define PSA_1_MODULE_1    		 7
#define PSA_1_MODULE_1_IN		   7
#define PSA_1_MODULE_1_OUT	   1
#define PSA_1_IN_LOGIC 				 ACTIVE_HIGH // N/O
#define PSA_1_OUT_LOGIC 			 ACTIVE_HIGH

#define PSA_2_MODULE_1    		 8
#define PSA_2_MODULE_1_IN		   7
#define PSA_2_MODULE_1_OUT	   1
#define PSA_2_IN_LOGIC 				 ACTIVE_HIGH // N/O
#define PSA_2_OUT_LOGIC 			 ACTIVE_HIGH

#define PSA_3_MODULE_1    		 9
#define PSA_3_MODULE_1_IN		   7
#define PSA_3_MODULE_1_OUT	   1
#define PSA_3_IN_LOGIC 				 ACTIVE_HIGH // N/O
#define PSA_3_OUT_LOGIC 			 ACTIVE_HIGH

//-------------------------------------------------------------
// POWER SUPPLY BROKEN
#define PWR_BROKEN_0_MODULE_1   		 2
#define PWR_BROKEN_0_MODULE_1_IN		 1
#define PWR_BROKEN_0_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_0_S_MODULE_1   	 4
#define PWR_BROKEN_0_S_MODULE_1_IN	 1
#define PWR_BROKEN_0_S_LOGIC				 ACTIVE_HIGH

#define PWR_BROKEN_1_MODULE_1    		 8                 // aggiunti 4 alimentatori oltre a quello di ciascuna centrale - rivedere codice
#define PWR_BROKEN_1_MODULE_1_IN		 1
#define PWR_BROKEN_1_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_2_MODULE_1    		 9
#define PWR_BROKEN_2_MODULE_1_IN		 1
#define PWR_BROKEN_2_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_3_MODULE_1    		10
#define PWR_BROKEN_3_MODULE_1_IN		 1
#define PWR_BROKEN_3_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_4_MODULE_1    		 6
#define PWR_BROKEN_4_MODULE_1_IN		 4
#define PWR_BROKEN_4_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_5_MODULE_1    		 8                 // aggiunti 4 alimentatori oltre a quello di ciascuna centrale - rivedere codice
#define PWR_BROKEN_5_MODULE_1_IN		 1
#define PWR_BROKEN_5_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_6_MODULE_1    		 9
#define PWR_BROKEN_6_MODULE_1_IN		 1
#define PWR_BROKEN_6_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_7_MODULE_1    		10
#define PWR_BROKEN_7_MODULE_1_IN		 1
#define PWR_BROKEN_7_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_8_MODULE_1    		 6
#define PWR_BROKEN_8_MODULE_1_IN		 4
#define PWR_BROKEN_8_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_9_MODULE_1    		10
#define PWR_BROKEN_9_MODULE_1_IN		 1
#define PWR_BROKEN_9_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_10_MODULE_1    	 6
#define PWR_BROKEN_10_MODULE_1_IN		 4
#define PWR_BROKEN_10_LOGIC					 ACTIVE_HIGH

#define PWR_BROKEN_11_MODULE_1    	 6
#define PWR_BROKEN_11_MODULE_1_IN		 4
#define PWR_BROKEN_11_LOGIC					 ACTIVE_HIGH

//-------------------------------------------------------------
// POWER SUPPLY NETWORK FAULT
#define PWR_NET_0_MODULE_1   		   2
#define PWR_NET_0_MODULE_1_IN		   2
#define PWR_NET_0_LOGIC					   ACTIVE_HIGH

#define PWR_NET_0_S_MODULE_1   		 4
#define PWR_NET_0_S_MODULE_1_IN		 2
#define PWR_NET_0_S_LOGIC					 ACTIVE_HIGH

#define PWR_NET_1_MODULE_1    		 8
#define PWR_NET_1_MODULE_1_IN		   2
#define PWR_NET_1_LOGIC					   ACTIVE_HIGH

#define PWR_NET_2_MODULE_1    		 9
#define PWR_NET_2_MODULE_1_IN		   2
#define PWR_NET_2_LOGIC					   ACTIVE_HIGH

#define PWR_NET_3_MODULE_1    		10
#define PWR_NET_3_MODULE_1_IN		   2
#define PWR_NET_3_LOGIC					   ACTIVE_HIGH

#define PWR_NET_4_MODULE_1    		 6
#define PWR_NET_4_MODULE_1_IN		   5
#define PWR_NET_4_LOGIC					   ACTIVE_HIGH

#define PWR_NET_5_MODULE_1    		 8
#define PWR_NET_5_MODULE_1_IN		   2
#define PWR_NET_5_LOGIC					   ACTIVE_HIGH

#define PWR_NET_6_MODULE_1    		 9
#define PWR_NET_6_MODULE_1_IN		   2
#define PWR_NET_6_LOGIC					   ACTIVE_HIGH

#define PWR_NET_7_MODULE_1    		10
#define PWR_NET_7_MODULE_1_IN		   2
#define PWR_NET_7_LOGIC					   ACTIVE_HIGH

#define PWR_NET_8_MODULE_1    		 6
#define PWR_NET_8_MODULE_1_IN		   5
#define PWR_NET_8_LOGIC					   ACTIVE_HIGH

#define PWR_NET_9_MODULE_1    		 8
#define PWR_NET_9_MODULE_1_IN		   2
#define PWR_NET_9_LOGIC					   ACTIVE_HIGH

#define PWR_NET_10_MODULE_1    		 9
#define PWR_NET_10_MODULE_1_IN	   2
#define PWR_NET_10_LOGIC				   ACTIVE_HIGH

#define PWR_NET_11_MODULE_1    		10
#define PWR_NET_11_MODULE_1_IN	   2
#define PWR_NET_11_LOGIC				   ACTIVE_HIGH


//-------------------------------------------------------------
// BATTERY
#define BATTERY_0_MODULE_1   		   2
#define BATTERY_0_MODULE_1_IN		   3
#define BATTERY_0_LOGIC					   ACTIVE_HIGH

#define BATTERY_0_S_MODULE_1   		 4
#define BATTERY_0_S_MODULE_1_IN		 3
#define BATTERY_0_S_LOGIC			     ACTIVE_HIGH

#define BATTERY_1_MODULE_1    		 8                    // aggiunte 4 batterie oltre a quella di ciascuna centrale - rivedere codice
#define BATTERY_1_MODULE_1_IN		   3
#define BATTERY_1_LOGIC					   ACTIVE_HIGH

#define BATTERY_2_MODULE_1    		 9
#define BATTERY_2_MODULE_1_IN		   3
#define BATTERY_2_LOGIC					   ACTIVE_HIGH

#define BATTERY_3_MODULE_1    		10
#define BATTERY_3_MODULE_1_IN		   3
#define BATTERY_3_LOGIC					   ACTIVE_HIGH

#define BATTERY_4_MODULE_1    		 6
#define BATTERY_4_MODULE_1_IN		   6
#define BATTERY_4_LOGIC					   ACTIVE_HIGH

#define BATTERY_5_MODULE_1    		 8                    // aggiunte 4 batterie oltre a quella di ciascuna centrale - rivedere codice
#define BATTERY_5_MODULE_1_IN		   3
#define BATTERY_5_LOGIC					   ACTIVE_HIGH

#define BATTERY_6_MODULE_1    		 9
#define BATTERY_6_MODULE_1_IN		   3
#define BATTERY_6_LOGIC					   ACTIVE_HIGH

#define BATTERY_7_MODULE_1    		10
#define BATTERY_7_MODULE_1_IN		   3
#define BATTERY_7_LOGIC					   ACTIVE_HIGH

#define BATTERY_8_MODULE_1    		 6
#define BATTERY_8_MODULE_1_IN		   6
#define BATTERY_8_LOGIC					   ACTIVE_HIGH

#define BATTERY_9_MODULE_1    		 8                    // aggiunte 4 batterie oltre a quella di ciascuna centrale - rivedere codice
#define BATTERY_9_MODULE_1_IN		   3
#define BATTERY_9_LOGIC					   ACTIVE_HIGH

#define BATTERY_10_MODULE_1    		 9
#define BATTERY_10_MODULE_1_IN	   3
#define BATTERY_10_LOGIC				   ACTIVE_HIGH

#define BATTERY_11_MODULE_1    		 9
#define BATTERY_11_MODULE_1_IN	   3
#define BATTERY_11_LOGIC				   ACTIVE_HIGH

// special BADGE per disabilitazione zona --> per disattivare l'ingresso � alto
#define BADGE_0_MODULE_1    		 7
#define BADGE_0_MODULE_1_IN		   1
#define BADGE_0_LOGIC					   ACTIVE_HIGH // N/O

//aggiungere i position switch (ON/OFF) delle tre valvole


#define ENABLED_INPUT_MODULE_1    		 0x00
#define ENABLED_OUTPUT_MODULE_1	  	   0xFF
#define ENABLED_INPUT_MODULE_2    		 0xC7
#define ENABLED_OUTPUT_MODULE_2	  	   0xC7
#define ENABLED_INPUT_MODULE_3    		 0x00
#define ENABLED_OUTPUT_MODULE_3	  	   0xFF
#define ENABLED_INPUT_MODULE_4    		 0xC7
#define ENABLED_OUTPUT_MODULE_4	  	   0xC7
#define ENABLED_INPUT_MODULE_5    		 0x00
#define ENABLED_OUTPUT_MODULE_5	  	   0x00
#define ENABLED_INPUT_MODULE_6    		 0x00
#define ENABLED_OUTPUT_MODULE_6	  	   0x00
#define ENABLED_INPUT_MODULE_7    		 0x00
#define ENABLED_OUTPUT_MODULE_7	  	   0x00
#define ENABLED_INPUT_MODULE_8    		 0x00
#define ENABLED_OUTPUT_MODULE_8	  	   0x00
#define ENABLED_INPUT_MODULE_9    		 0x00
#define ENABLED_OUTPUT_MODULE_9	  	   0x00
#define ENABLED_INPUT_MODULE_10    		 0x00
#define ENABLED_OUTPUT_MODULE_10  	   0x00
#define ENABLED_INPUT_MODULE_11    		 0x00
#define ENABLED_OUTPUT_MODULE_11  	   0x00
// <-- versione con define


// versione elastica -->
////-------------------------------------------------------------
//// SIRENS
//#ifdef MASTER_CIE
// #define SIREN_0_MODULE_1    		 2
//#else
// #define SIREN_0_MODULE_1    		 4
//#endif
//#define SIREN_0_MODULE_1_OUT		 1
//#define SIREN_0_MODULE_2    		 0
//#define SIREN_0_MODULE_2_OUT		 0
//#define SIREN_0_MODULE_3    		 0
//#define SIREN_0_MODULE_3_OUT		 0
//#define SIREN_0_LOGIC						 ACTIVE_HIGH

////-------------------------------------------------------------
//// LAMPS
//#ifdef MASTER_CIE
// #define LAMP_0_MODULE_1   		 2
//#else
// #define LAMP_0_MODULE_1   		 4
//#endif
//#define LAMP_0_MODULE_1_OUT		 2
//#define LAMP_0_MODULE_2    		 0
//#define LAMP_0_MODULE_2_OUT		 0
//#define LAMP_0_MODULE_3    		 0
//#define LAMP_0_MODULE_3_OUT		 0
//#define LAMP_0_LOGIC						 ACTIVE_HIGH

//////-------------------------------------------------------------
//// BUZZER (CIE)
//#ifdef MASTER_CIE
//#define BUZZER_MODULE_1    		 2
//#else
//#define BUZZER_MODULE_1    		 4
//#endif
//#define BUZZER_MODULE_1_OUT		 3
//#define BUZZER_MODULE_2    		 0
//#define BUZZER_MODULE_2_OUT		 0
//#define BUZZER_LOGIC					 ACTIVE_HIGH

////-------------------------------------------------------------
//// EXT. FAULT (CIE)
//#ifdef MASTER_CIE
//#define EXT_FAULT_MODULE_1    		 2
//#else
//#define EXT_FAULT_MODULE_1    		 4
//#endif
//#define EXT_FAULT_MODULE_1_OUT		 7
//#define EXT_FAULT_MODULE_2    		 0
//#define EXT_FAULT_MODULE_2_OUT		 0
//#define EXT_FAULT_LOGIC					   ACTIVE_HIGH

////-------------------------------------------------------------
//// EXT. ALARM (CIE)
//#ifdef MASTER_CIE
//#define EXT_ALARM_MODULE_1    		 2
//#else
//#define EXT_ALARM_MODULE_1    		 4
//#endif
//#define EXT_ALARM_MODULE_1_OUT		 8	
//#define EXT_ALARM_MODULE_2    		 0
//#define EXT_ALARM_MODULE_2_OUT		 0
//#define EXT_ALARM_LOGIC					   ACTIVE_HIGH

/* INPUTS */
//-------------------------------------------------------------
// versione con define -->
// <-- versione con define

//-------------------------------------------------------------
// versione elastica -->
//// THERMOSTAT
//#define THERMOSTAT_MODULE_1    		 5
//#define THERMOSTAT_MODULE_1_IN		 8
//#define THERMOSTAT_LOGIC					 ACTIVE_LOW // N/C

////-------------------------------------------------------------
//// EXT. FAULT (IN)
//#ifdef MASTER_CIE
//#define EXT_FAULT_IN_MODULE_1    		 2
//#else
//#define EXT_FAULT_IN_MODULE_1    		 4
//#endif
//#define EXT_FAULT_IN_MODULE_1_IN		 7
//#define EXT_FAULT_IN_LOGIC					 ACTIVE_HIGH // N/O

////-------------------------------------------------------------
//// EXT. ALARM (IN)
//#ifdef MASTER_CIE
//#define EXT_ALARM_IN_MODULE_1    		 2
//#else
//#define EXT_ALARM_IN_MODULE_1    		 4
//#endif
//#define EXT_ALARM_IN_MODULE_1_IN		 8
//#define EXT_ALARM_IN_LOGIC					 ACTIVE_HIGH

////-------------------------------------------------------------
//// PSA

//#define PSA_0_MODULE_1    		 3
//#define PSA_0_MODULE_1_IN		   6
//#define PSA_0_LOGIC 					 ACTIVE_HIGH

////-------------------------------------------------------------
//// POWER SUPPLY BROKEN
//#define PWR_BROKEN_0_MODULE_1   		 1
//#define PWR_BROKEN_0_MODULE_1_IN		 1
//#define PWR_BROKEN_0_LOGIC					 ACTIVE_HIGH

//#define PWR_BROKEN_0_S_MODULE_1   	 3
//#define PWR_BROKEN_0_S_MODULE_1_IN	 1
//#define PWR_BROKEN_0_S_LOGIC				 ACTIVE_HIGH

//#define PWR_BROKEN_1_MODULE_1    		 0
//#define PWR_BROKEN_1_MODULE_1_IN		 0
//#define PWR_BROKEN_1_LOGIC					 ACTIVE_HIGH

//#define PWR_BROKEN_2_MODULE_1    		 0
//#define PWR_BROKEN_2_MODULE_1_IN		 0
//#define PWR_BROKEN_2_LOGIC					 ACTIVE_HIGH


////-------------------------------------------------------------

//// POWER SUPPLY NETWORK FAULT
//#define PWR_NET_0_MODULE_1   		   1
//#define PWR_NET_0_MODULE_1_IN		   2
//#define PWR_NET_0_LOGIC					   ACTIVE_HIGH

//#define PWR_NET_0_S_MODULE_1   		 3
//#define PWR_NET_0_S_MODULE_1_IN		 2
//#define PWR_NET_0_S_LOGIC					 ACTIVE_HIGH

//#define PWR_NET_1_MODULE_1    		 0
//#define PWR_NET_1_MODULE_1_IN		   0
//#define PWR_NET_1_LOGIC					   ACTIVE_HIGH

//#define PWR_NET_2_MODULE_1    		 0
//#define PWR_NET_2_MODULE_1_IN		   0
//#define PWR_NET_2_LOGIC					   ACTIVE_HIGH

////-------------------------------------------------------------


//// BATTERY
//#define BATTERY_0_MODULE_1   		   1
//#define BATTERY_0_MODULE_1_IN		   3
//#define BATTERY_0_LOGIC					   ACTIVE_HIGH

//#define BATTERY_0_S_MODULE_1   		 3
//#define BATTERY_0_S_MODULE_1_IN		 3
//#define BATTERY_0_S_LOGIC			     ACTIVE_HIGH
// <--versione elastica
//-------------------------------------------------------------


// comune --->
/* ###################################### */
/* 		    	ALL OBJECTS ARRAY       			*/
/* ###################################### */
// Indice del primo oggetto per ogni tipo di oggetto
#define FIRST_ZONE                 0
#define FIRST_SENSOR              (FIRST_ZONE   		 + NUMBER_OF_ZONES)
#define FIRST_SIREN               (FIRST_SENSOR 		 + NUMBER_OF_SENSORS)
#define FIRST_LAMP                (FIRST_SIREN  		 + NUMBER_OF_SIRENS)
#define FIRST_PANEL               (FIRST_LAMP   		 + NUMBER_OF_LAMPS)
#define FIRST_VALVE               (FIRST_PANEL  		 + NUMBER_OF_PANELS)
#define FIRST_EXT_DISPLAY         (FIRST_VALVE  		 + NUMBER_OF_VALVES)
#define FIRST_FLUX                (FIRST_EXT_DISPLAY + NUMBER_OF_EXT_DISPLAYS)

#define EVENTS_ARRAY_DIMENSIONS         50
#define ALARMS_ARRAY_DIMENSIONS         50
#define FAULTS_ARRAY_DIMENSIONS         50
#define DISABLEMENTS_ARRAY_DIMENSIONS   NUMBER_OF_OBJECTS
#define DEV_CTRL_ARRAY_DIMENSIONS       NUMBER_OF_OBJECTS

// poi ricordati di modificare l'inizializzazione nella funzione "init_variables"
// l� ci sono degli array stretttamente legati a queste definizioni

/* ###################################### */
/* 				     OBJECTS        			 			*/
/* ###################################### */

// types DEVICES CONTROL or DISABLEMENTS
#define O_DISABLEMENTS           1
#define O_DEV_CTRL               2

#define TYPES_OF_OBJECTS         9 // escluse le zone; uso questi tipi per puntare alla cella quindi � FLUX (= 8) + 1
#define O_TYPE_ZONE              1
#define O_TYPE_SENSOR            2
#define O_TYPE_SIREN             3
#define O_TYPE_LAMP              4
#define O_TYPE_PANEL             5
#define O_TYPE_VALVE             6
#define O_TYPE_EXT_DISPLAY       7
#define O_TYPE_FLUX              8
#define O_TYPE_BUZZER            9
#define O_TYPE_MODULE            10

// Specific types DEVICES CONTROL or DISABLEMENTS
#define O_TYPE_LAMP_CIE          0
#define O_TYPE_LAMP_ZONE         1
#define O_TYPE_VALVE_AUX         0
#define O_TYPE_VALVE_ZONE        1
#define O_TYPE_SIREN_CIE         0
#define O_TYPE_SIREN_ZONE        1
#define O_TYPE_FLUX_AUX          0
#define O_TYPE_FLUX_ZONE         1

/* ###################################### */
/* 				       LEDS         			 			*/
/* ###################################### */
#define LED_ALARM								 0
#define LED_GENERAL_FAULTS			 1
#define LED_SYSTEM_FAULTS				 2
#define LED_DISABLEMENTS				 3
#define LED_DEV_CTRL						 4
#define LED_AUX_N2_SUPPLY				 5
#define LED_ENERGY_FAULTS				 6
#define LED_POWER_SUPPLY_NETWORK_OK 7

/* ###################################### */
/* 				     COLORS         			 			*/
/* ###################################### */
#define DARK_RED     			 0xFFFF//0x109D
#define LIGHT_RED    			 0xFFFF//0x94DD
#define DARK_GREY    			 0xFFFF//0xB48C
#define LIGHT_GREY   			 0xFFFF//0xEED8
#define DARK_YELLOW  			 0xFFFF//0x167C
#define LIGHT_YELLOW 			 0xFFFF//0x76DC
#define DARK_BLUE  			   0xFFFF//0x59E3
#define DEEP_PURPLE 			 0xFFFF//0xFDD8
//#define WHITE        			 0xFFFF
#define DARK_GREEN   			 0xFFFF//0x2304
#define DARK_GREEN2  			 0xFFFF//0x7E0B
#define LIGHT_GREEN2 			 0xFFFF//0x9DD0
#define LIGHT_GREEN  			 0xFFFF//0xB634
#define LIGHT_BLUE   			 0xFFFF//0xEC03
#define LIGHT_ORANGE 			 0xFFFF//0x64FB
#define SUPER_LIGHT_ORANGE 0xFFFF//0xB69D
#define DARK_VIOLET        0xFFFF//0xC311
#define LIGHT_VIOLET       0xFFFF//0xC536

/* ######################################################## */
/* 	    RGB888 COLOR TABLE  - https://rgbcolorcode.com/     */
/* ######################################################## */

#define _TANGERINE_YELLOW		0xf8cc00	//ex ORANGE				0x067f
#define _CAPRI							0x00abfe	//ex CYAN					0xfd60
#define _CAMBRIDGE_BLUE			0xA7C7B0	//ex LIGHT_GREEN	0xb634
#define _LIGHT_SLATE_GREY		0x708895	//ex SLATEGRAY		0x944e
#define _NEON_GREEN					0x28ff28	//ex GREEN				0x27f5
#define _CADMIUM_YELLOW			0xfff400	//ex YELLOW				0x07bf
#define _WHITE							0xffffff	//ex WHITE				0xffff
#define _BLACK							0x000000	//ex BLACK				0x0000
#define _RED								0xff0000	//ex RED					0x001f
#define _MAJORELLE_BLUE			0x6060f0	//ex BLUE					0xf30c
#define _STRAW							0xe0d870	//ex LIGHT_YELLOW	0x76dc
#define _PRUSSIAN_BLUE			0x033c58	//ex DARK_BLUE		0x59e3
#define _LAVENDER_BLUE			0xC0BBF8	//ex LIGHT_BLUE		0xec03
#define _MAUVE							0xffffff	//ex DEEP_PURPLE	0xfdd8
#define _PASTEL_GREY				0xCFCFC4	//ex GREY					0xce59
#define _LIME_GREEN					0x32cd32

/* ######################################################## */
/* 	           RGB888 COLORS CONVERTED IN BGR565				    */
/* ######################################################## */

#define TANGERINE_YELLOW	(((_TANGERINE_YELLOW & 0xf80000) >> 19) | ((_TANGERINE_YELLOW & 0x00fc00) >> 5) | ((_TANGERINE_YELLOW & 0x0000f8) << 8))
#define CAPRI							(((_CAPRI & 0xf80000) >> 19) | ((_CAPRI & 0x00fc00) >> 5) | ((_CAPRI & 0x0000f8) << 8))
#define CAMBRIDGE_BLUE		(((_CAMBRIDGE_BLUE & 0xf80000) >> 19) | ((_CAMBRIDGE_BLUE & 0x00fc00) >> 5) | ((_CAMBRIDGE_BLUE & 0x0000f8) << 8))
#define LIGHT_SLATE_GREY	(((_LIGHT_SLATE_GREY & 0xf80000) >> 19) | ((_LIGHT_SLATE_GREY & 0x00fc00) >> 5) | ((_LIGHT_SLATE_GREY & 0x0000f8) << 8))
#define NEON_GREEN				(((_NEON_GREEN & 0xf80000) >> 19) | ((_NEON_GREEN & 0x00fc00) >> 5) | ((_NEON_GREEN & 0x0000f8) << 8))
#define CADMIUM_YELLOW		(((_CADMIUM_YELLOW	 & 0xf80000) >> 19) | ((_CADMIUM_YELLOW	 & 0x00fc00) >> 5) | ((_CADMIUM_YELLOW	 & 0x0000f8) << 8))
#define WHITE							(((_WHITE & 0xf80000) >> 19) | ((_WHITE & 0x00fc00) >> 5) | ((_WHITE & 0x0000f8) << 8))
#define BLACK							(((_BLACK & 0xf80000) >> 19) | ((_BLACK & 0x00fc00) >> 5) | ((_BLACK & 0x0000f8) << 8))
#define RED								(((_RED & 0xf80000) >> 19) | ((_RED & 0x00fc00) >> 5) | ((_RED & 0x0000f8) << 8))
#define MAJORELLE_BLUE		(((_MAJORELLE_BLUE	 & 0xf80000) >> 19) | ((_MAJORELLE_BLUE	 & 0x00fc00) >> 5) | ((_MAJORELLE_BLUE	 & 0x0000f8) << 8))
#define STRAW							(((_STRAW & 0xf80000) >> 19) | ((_STRAW & 0x00fc00) >> 5) | ((_STRAW & 0x0000f8) << 8))
#define PRUSSIAN_BLUE			(((_PRUSSIAN_BLUE & 0xf80000) >> 19) | ((_PRUSSIAN_BLUE & 0x00fc00) >> 5) | ((_PRUSSIAN_BLUE & 0x0000f8) << 8))
#define LAVENDER_BLUE			(((_LAVENDER_BLUE & 0xf80000) >> 19) | ((_LAVENDER_BLUE & 0x00fc00) >> 5) | ((_LAVENDER_BLUE & 0x0000f8) << 8))
#define MAUVE							(((_MAUVE & 0xf80000) >> 19) | ((_MAUVE & 0x00fc00) >> 5) | ((_MAUVE & 0x0000f8) << 8))
#define PASTEL_GREY				(((_PASTEL_GREY & 0xf80000) >> 19) | ((_PASTEL_GREY & 0x00fc00) >> 5) | ((_PASTEL_GREY & 0x0000f8) << 8))
#define LIME_GREEN				(((_LIME_GREEN & 0xf80000) >> 19) | ((_LIME_GREEN & 0x00fc00) >> 5) | ((_LIME_GREEN & 0x0000f8) << 8))

/* ###################################### */
/* 				     SCREENS         			 			*/
/* ###################################### */
#define UPSIDEDOWN									1
//1=upsidedown 0=diritto
#define SYSTEM_SCREEN               0
#define SET_SCREEN                  1
#define TH_SCREEN                   2
#define LOG_SCREEN                  3
#define OPTIONS_SCREEN              4
#define EVENTS_SCREEN               5
#define INFO_SCREEN                 6
#define CHARTS_SCREEN               7
#define FAULTS_SCREEN               8
#define DISABLEMENTS_SCREEN         9
#define DEV_CTRL_SCREEN            10
#define ALARMS_LIST_SCREEN         11
#define ZONE_X_SCREEN              12
#define DEV_CTRL_LIST_SCREEN       13
#define DISABLEMENTS_LIST_SCREEN   14

/* ###################################### */
/* 				     SYSTEM (other) 			 			*/
/* ###################################### */
#define CHANGE_TH                 1
#define CHANGE_SET                2
#define CHANGE_DISABLEMENT        1
#define CHANGE_DEV_CTRL           2
#define SORTED_BY_ZONES           1
#define SORTED_BY_ELEMENTS        2
#define SORT_BY_ZONES             1
#define SORT_BY_ELEMENTS          2

#define LINE_1          1
#define LINE_2          2
#define LINE_3          3
#define LINE_4          4
#define LINE_5          5

#define PRINT_ON_SD     1
#define SHOW_ON_DISPLAY 2

/* ###################################### */
/* 				     TIMINGS        			 			*/
/* ###################################### */
// 1 = 10 ms
#define DEF_SECONDO                 82   					// (1 sec)	  // non � molto preciso
#define MESSAGE_ON_SCREEN_TIME     100  					// (1 sec)	  // quanto rimane un messaggio prima di scomparire
#define RECTANGLE_BLINK_TIME        60   					// (600 ms)	  // velocit� del lampeggio del bordo schermata
#define SENSOR_COMM_TIMEOUT          6   	       	// (60 sec)	  // guasto di comunicazione dei sensori
#define MODULE_COMM_TIMEOUT          6   		      // (60 sec)   // guasto di comunicazione dei moduli
#define EXT_DISPLAY_COMM_TIMEOUT     6   		      // (60 sec)   // guasto di comunicazione dei display esterni
#define FLUX_TIMEOUT              (6 * 4)         // (4 min)    // tempo da attendere per guasto flussostati
#define IDLE_TIMEOUT							   6            // (60 sec)   // idle state dopo 60 secondi di inattivit�
#define LOGOUT_TIMEOUT            (6 * 2)         // (2 min)    // tempo dopo il qualce si auto-disconnette
#define TEST_LEDS_TIMEOUT            2            // (20 sec)   // tempo di test dei leds della centrale
// #define PWD_BLINK_TIME              25            // (700 ms)   // velocit� del lapeggio della password
#define PWD_BLINK_TIME              70            // (700 ms)   // velocit� del lapeggio della password
#define WDT_TIMEOUT     	  (5000000 * 2)		      // Watchodog time out in 10 seconds		
#define SAVE_O2_VALUES   					(6 * 5)	        // (5 minuti era 15 sec) frequenza con cui vengono salvati i valori di ossigeno sull'SD 
#define RS485_MAIN_COUNT			  		 3            // frequenza con cui vengono interrogati i sensori (in cicli main; 1 ciclo sono circa 180 ms)
#define IDLE_SCREEN_TIMEOUT          2            // (20 sec) tempo dopo il quale va nella schrmata di riposo, in assenza di allarmi e guasti
#define EWON_ROUTINE_TIMEOUT         2            // (20 sec) tempo da aspettare dall'accensione prima che sia popolata la memoria dell'Anybus per l'eWON
#define EWON_COMM_TIMEOUT         (6 * 2)         // (2  min)  // guasto di comunicazione eWON
#define WEBSERVER_COMM_TIMEOUT    (6 * 5)   			// (5  min)  // guasto di comunicazione Webserver
#define ADOX_COMM_TIMEOUT         (6 * 2)  				// (2  min)  // guasto di comunicazione MiniADOX
#define sense_touch_timer					  30
#define MODULE_CONFIG_TIMEOUT     (6 * 2) 				// (2  min)  // ricarica timer per riconfigurare moduli I/O
#define TMR_MAIN                     2            // (20ms)  // timer ciclo principale
#define REG_ERR_COUNT                4            // numero msg del error register contenente errori di comunicazione 
#define CIE_COMM_TIMEOUT             6   					// (60 sec)      // guasto di comunicazione tra le due centrali
#define SWITCH_TIMEOUT               6    			  // (60 sec) // tempo da attendere per guasto switches - quando spengo devo sommare anche il tempo che la valvola riceva il comando (riga 13555): switches_array[r-1].timeout = SWITCH_TIMEOUT+TMR_SWITCHOFF_VALVE;
#define TMR_SWITCHOFF_VALVE          6    			  // (1 min)  // timer ritardo chiusura valvole
#define CAN_REINIT                   6   		      // (60 sec)      // ogni tot reinizializza il CAN
#define TMR_LOW_BACKLIGHT         (300)           // (3 sec)       // alterna schermata spenta (2 secondi) con schermata poco retroilluminata (1 secondo)
#define TMR_USER_PRESS_BUTTON        6    			  // (60 sec)      // l'utente ha premuto un tasto, quindi aumenta la l'uminosit�

#define TMR_1_SECOND             (650)            // (1 sec)    // timer 1 secondo  impostare tra 650 e 700
#define TMR_10_SECONDS  (TMR_1_SECOND * 10)       // (10 sec)   // timer 10 secondi


/* ###################################### */
/* 				     EVENTS         			 			*/
/* ###################################### */

// Event types
#define E_TYPE_ALARM           1
#define E_TYPE_FAULT           2
#define E_TYPE_DISABLEMENT     3
#define E_TYPE_DEV_CTRL        4
#define E_TYPE_MISC            5


// Specific types ALARMS
#define E_S_TYPE_O2_LOW         1
#define E_S_TYPE_O2_HIGH        2
#define E_S_TYPE_EXT_ALARM      3

// Specific types DISABLEMENTS
// Specific types DEV CTRL

// Specific types MISC
#define E_S_TYPE_LOGIN          	1
#define E_S_TYPE_LOGOUT         	2
#define E_S_TYPE_SYSTEM_TURN_ON 	3
#define E_S_TYPE_SAVE_SET       	4
#define E_S_TYPE_SAVE_SET_ALL   	5
#define E_S_TYPE_SAVE_TH    			6
#define E_S_TYPE_SAVE_TH_ALL 			7
#define E_S_TYPE_DELETE_EVENTS  	8
#define E_S_TYPE_DELETE_ALARMS  	9
#define E_S_TYPE_RESET_ALARMS  	 10
#define E_S_TYPE_NEW_CONFIG    	 11
#define E_S_TYPE_INIT_E2PROM     12

#define ZONE_1                1

#define NUM_2_DIGITS          1
#define NUM_FLOAT             2
#define INPUT				          0
#define OUTPUT                1

/* ###################################### */
/* 				     RS485          			 			*/
/* ###################################### */
#define RX_BUFFER_RS485			10

/* ###################################### */
/* 				      CAN           			 			*/
/* ###################################### */
#define INIT_DEVICES					0
#define SET_OUTPUTS  					1
#define RX_BUFFER_CAN 			 20
#define CAN_1_baudrate 20000
//#define CAN_2_baudrate 20000


/* ###################################### */
/* 				     FAULTS         			 			*/
/* ###################################### */
// types FAULTS
#define F_TYPE_AL1_FAULT           1 //------> MBVECTOR_ALLARMI
#define F_TYPE_AL2_FAULT           2
#define F_TYPE_AL3_FAULT           3
#define F_TYPE_AL4_FAULT           4
#define F_TYPE_AL5_FAULT           5
//#define F_TYPE_AL6_FAULT           6
//#define F_TYPE_AL7_FAULT           7
#define F_TYPE_AL8_FAULT           8
#define F_TYPE_AL11_FAULT         11
#define F_TYPE_AL12_FAULT         12
#define F_TYPE_AL20_FAULT         20
#define F_TYPE_AL21_FAULT         21
#define F_TYPE_AL22_FAULT         22
#define F_TYPE_AL24_FAULT         23

#define F_TYPE_AL_CAN_FAULT       25 //------> MBVECTOR_ALLARMI_2
#define F_TYPE_AL26_SLAVE_FAULT   26
#define F_TYPE_AL27_SONDA_B1      27
#define F_TYPE_AL28_SONDA_B2      28
#define F_TYPE_AL29_SONDA_B3      29

#define F_TYPE_WEBSERVER          30
			#define F_TYPE_WEBSERVER_COMM_FAULT_CAN_1    1
			#define F_TYPE_WEBSERVER_COMM_FAULT_CAN_2    2
			
#define F_TYPE_SBM_SD_FAULT    10


/////////////////////////////////////////////////////////////////// old andranno eliminati
//old CIE
#define F_TYPE_SENSOR              1
			#define F_SENSOR_COMM_FAULT      1
			#define F_SENSOR_ELAB_FAULT      2
			#define F_SENSOR_SD_FAULT        3
			#define F_SENSOR_WARM_UP         4
      #define F_SENSOR_CAN1_FAULT      5
      #define F_SENSOR_CAN2_FAULT      6
#define F_TYPE_O2_DIFF             2
			#define F_TYPE_O2_DIFF_1         1
			#define F_TYPE_O2_DIFF_2         2
#define F_TYPE_FLUX                 3
			#define F_TYPE_AUX_FLUX          0
			#define F_TYPE_ZONE_FLUX         1
#define F_TYPE_PW_SUPPLY           4
			#define F_TYPE_PW_BROKEN         1
			#define F_TYPE_PW_NO_NET         2
			#define F_TYPE_PW_BATTERY        3
#define F_TYPE_THERMOSTAT      5
#define F_TYPE_PSA             6
			#define F_TYPE_PSA_BROKEN        1
#define F_TYPE_EXTERNAL        7
#define F_TYPE_MONITORING      8			
			#define F_TYPE_IN_CTO_CTO        1
			#define F_TYPE_IN_OPEN           2
			#define F_TYPE_IN_X              3
			#define F_TYPE_OUT_CTO_CTO       4
			#define F_TYPE_OUT_OPEN          5
			#define F_TYPE_OUT_X             6			
			#define F_TYPE_COMM_FAULT        7
			#define F_TYPE_COMM_FAULT_CAN_1  18 
			#define F_TYPE_COMM_FAULT_CAN_2  19     
#define F_TYPE_NETWORK          9
//#define F_TYPE_SBM_SD_FAULT    10
#define F_TYPE_EXT_DISPLAY     11
			#define F_TYPE_EXT_DISPLAY_COMM_FAULT_CAN_1   1
			#define F_TYPE_EXT_DISPLAY_COMM_FAULT_CAN_2   2
#define F_TYPE_EWON            12
			#define F_TYPE_EWON_COMM_FAULT          1
			#define F_DIGIN_CUT                    13
			#define F_DIGIN_SHORT_CIRCUIT          14
			#define F_RELE                         15
			#define F_TYPE_CONTROLL_PW_1           16
			#define F_TYPE_CONTROLL_PW_2           17
			#define F_TYPE_SWITCHES                20
			#define F_TYPE_THERMOSTAT_2            21
			#define F_TYPE_CIE                     22
			#define F_TYPE_CIE_COMM_FAULT          23
#define F_TYPE_ADOX                          24
			#define F_TYPE_ADOX_FAULT               1
			#define F_TYPE_ADOX_COMM_FAULT          2								
//#define F_TYPE_WEBSERVER                          25
//			#define F_TYPE_WEBSERVER_COMM_FAULT_CAN_1    1
//			#define F_TYPE_WEBSERVER_COMM_FAULT_CAN_2    2
																		
/* ###################################### */
/* 				        SD     	    			 			*/
/* ###################################### */
#define USE_SD  1
#define ACCAPO 	fputc('\r',GPS_file);	fputc('\n',GPS_file);
#define ALARMS_FILE     1
#define EVENTS_FILE     2
#define O2_VALUES_FILE  3
/* ###################################### */
/* 				     SDRAM   				 			      */
/* ###################################### */
#define BLS0 26
#define BLS1 27
#define BLS2 28
#define BLS3 29
#define Port_1 1
#define Port_2 2
#define Port_3 3
#define Port_4 4


/*
 * These timing parameters are based on the EMC clock
 * there is no way of ensuring what the EMC clock frequency is
 * without severely bloating the code
 * ENSURE THAT THE EMC clock is one of these values
 */
 
 
#ifndef __SDRAM_H
#define __SDRAM_H



#define SDRAM_SPEED_48 0
#define SDRAM_SPEED_50 1
#define SDRAM_SPEED_60 2
#define SDRAM_SPEED_72 3
#define SDRAM_SPEED_80 4

#define SDRAM_SPEED SDRAM_SPEED_60

#define SDRAM_CONFIG_32BIT
#define SDRAM_SIZE               0x80000

#define SDRAM_BASE               0xA0000000 /*CS0*/

// <-- comune


/* Initializes the SDRAM.
 *
 * The entire SDRAM will be made available to malloc per default.
 *
 * Note that this functions is called internally if malloc requests
 * memory from SDRAM and that hasn't been disabled with a call to
 * sdram_disableMallocSdram().
 *
 * @returns 0 on success, 1 on failure
 */
//int sdram_init();

/* Prevents malloc from using SDRAM.
 *
 * This function must be called before the first allocation that 
 * would have been in SDRAM. If a big allocation has already been
 * made then this call will do nothing as the SDRAM will have been
 * initialized and all SDRAM given to malloc.
 */
//void sdram_disableMallocSdram();


#ifndef MIN
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif
#define I2CDEV                        2
#define EEPROM_I2C_ADDR            0x54
#define I2C_EEPROM_TOTAL_SIZE    0xFFFF
#define EEPROM_I2C_PAGE_SIZE        128
#define ADDRESS_SIZE                  2

//int16_t i2c_eeprom_read(uint8_t* buf, uint16_t offset, uint16_t len);
//int16_t i2c_eeprom_write(uint8_t* buf, uint16_t offset, uint16_t len);


//digital input PIN
#define PORT_DIG_IN_1_TO_4      0
#define PORT_DIG_IN_5_TO_8      4
#define PIN_DIG_IN_1            6
#define PIN_DIG_IN_2            7
#define PIN_DIG_IN_3            8
#define PIN_DIG_IN_4            9
#define PIN_DIG_IN_5           24
#define PIN_DIG_IN_6           26 
#define PIN_DIG_IN_7           27
#define PIN_DIG_IN_8           28


#endif /* end __SDRAM_H */



#ifndef SI
  #define SI  1
#endif
#ifndef NO
  #define NO  0
#endif


#define ADC_PT100_FAULT_MAX 100     //numero massimo errore sonda (sonda staccata, prima di generare errore)
                                    // se si impostano le temperature a ZERO, il guasto non viene considerato

#define ADC_PT100_CAMPIONI   10     //numero campioni ADC su cui fare la media

#define ADC_CAMPIONI          1     //numero campioni ADC su cui fare la media
#define ADC_CAMPIONI_ADC_5    1     //numero campioni ADC 5 su cui fare la media

//N.B. Ho dovuto spostare le 2 PT100 su due ADC distinti, perch� sembra faccia casino nell'elaborazione
// su banco (ufficio) funziona correttamente anche con gli altri ingressi collegati
// sulla macchina invece (a fronte di du temperature sui 25-27�C ciascuna, se messe sullo stesso ADC leggo ciascuna circa 55�C (il doppio?)

//input_analog_1_acquisizione 4-20mA flussimetro
//#define INPUT_1_ADC        1 // ADC 1,2,3,4
//#define INPUT_1_CHANNEL    0 // 0 = canale 1; 1 = canale 2
//#define INPUT_1_MODE       3 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
#define INPUT_1_ADC        1 // ADC 1,2,3,4  
#define INPUT_1_CHANNEL    0 // 0 = canale 1; 1 = canale 2
#define INPUT_1_MODE       1 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_2_acquisizione 4-20mA
//#define INPUT_2_ADC        1 // ADC 1,2,3,4
//#define INPUT_2_CHANNEL    1 // 0 = canale 1; 1 = canale 2
//#define INPUT_2_MODE       3 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
#define INPUT_2_ADC        1 // ADC 1,2,3,4
#define INPUT_2_CHANNEL    1 // 0 = canale 1; 1 = canale 2
#define INPUT_2_MODE       1 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_3_acquisizione 4-20mA pressione
#define INPUT_3_ADC        2 // ADC 1,2,3,4
#define INPUT_3_CHANNEL    0 // 0 = canale 1; 1 = canale 2
#define INPUT_3_MODE       3 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_4_acquisizione 4-20mA pressione
#define INPUT_4_ADC        2 // ADC 1,2,3,4
#define INPUT_4_CHANNEL    1 // 0 = canale 1; 1 = canale 2
#define INPUT_4_MODE       3 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_5_acquisizione 4-20mA pressione
#define INPUT_5_ADC        3 // ADC 1,2,3,4
#define INPUT_5_CHANNEL    0 // 0 = canale 1; 1 = canale 2
#define INPUT_5_MODE       3 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_6_acquisizione 4-20mA
#define INPUT_6_ADC        3 // ADC 1,2,3,4
#define INPUT_6_CHANNEL    1 // 0 = canale 1; 1 = canale 2
#define INPUT_6_MODE       3 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_7_acquisizione PT100
#define INPUT_7_ADC        4 // ADC 1,2,3,4
#define INPUT_7_CHANNEL    0 // 0 = canale 1; 1 = canale 2
#define INPUT_7_MODE       1 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA
//input_analog_8_acquisizione PT100
#define INPUT_8_ADC        4 // ADC 1,2,3,4
#define INPUT_8_CHANNEL    1 // 0 = canale 1; 1 = canale 2
#define INPUT_8_MODE       1 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA

//input_analog_9_acquisizione ADC 5 canale 1 amplificato mV - O2
#define INPUT_9_ADC        5 // ADC 1,2,3,4
#define INPUT_9_CHANNEL    1 // 0 = canale 1; 1 = canale 2
#define INPUT_9_MODE       4 // 1 = PT100; 2 = 0-1V; 3 = 0-20mA; 4 = mV


void MySetPin(int PortNum, int PinNum, int value);
void delay_ms (unsigned int time_ms);
void eeprom_write_word (unsigned int page, unsigned int offset, unsigned int data);
unsigned int eeprom_read_word (unsigned int page, unsigned int offset);





