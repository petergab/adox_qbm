
 /*************************************************************************************************
  i file "communication" sono divisi in 4 settori:
	
  1. UART_3: RS485 Modbus Slave - COM 1 (A1-B1) nella serigrafia del dispositivo 
  2. UART_2: RS485 Modbus Master - COM 2 (A2-B2) nella serigrafia del dispositivo 
  3. CAN_1:  CANOpen Master - per le valvole Pneumax/Festo e per eventuali moduli I/O
  4. CAN_2:  CANOpen Slave  - per la CIE o altri dispositivi Master
	
	in ciascun settore si trovano le strutture, le variabili, le definizioni e le funzioni relative
	
 *************************************************************************************************/

//  1. UART_3: RS485 Modbus Slave

//Modbus define
//definizioni messaggi Modbus da Master a scheda Adso
#define MBUS_ANALOGICHE    1000   // richiede letture analogiche
#define MBUS_ALLARMI       1050   // richiede stato allarmi
#define MBUS_STATO         1075   // richiede stato funzionamento
#define MBUS_TIME_ASS      1076   // imposta tempo assorbimento (Adox.Assorbimento)
#define MBUS_TIME_CMP      1077   // imposta tempo compensazione (Adox.Compensazione)
#define MBUS_TIME_SCARICO  1078   // imposta tempo scarico (Adox.Scarico)
#define MBUS_TIME_SFASAM   1079   // imposta tempo sfasamento stby (Adox.Sfasamento)
#define MBUS_SETO2         1080   // imposta set lavoro PSA (Adox.SetO2)
#define MBUS_SET_TIN       1081   // imposta set massimo temperatura aria in ingresso (Adox.SetO2)
#define MBUS_O2_HIGH_AL    1082   // imposta set allarme ossigeno prodotto impuro

//#define MBUS_PD_PRESSMIN   1082 // set minimo pressione serbatoio reintegro in pulldown
//...............................    NTF
//#define MBUS_DELTA_PRESS   1099  //delta incremento/decremento set pressione

#define MBUS_T_ZERO        1147   // taratura zero sonde PT100
#define MBUS_AL_RESET      1148   // resetta gli allarmi PSA per permettere la ripartenza
#define MBUS_COMANDO       1149   // comanda ON/OFF del PSA

//definizioni indice array Rivoira
  //tra 0-49  letture analogiche
  //tra 50-74 ingressi digitali e allarmi
  //tra 75-85 stato funzionamento
  //a   99    comando ricevuto ON/OFF
#define MBVECTOR_O2               0    // lettura O2
#define MBVECTOR_Q_AIR            1    // portata aria in ingresso (nm3/h)
#define MBVECTOR_Q_N2             2    // portata N2 in uscita (nm3/h)
#define MBVECTOR_P1               3    // pressione (bar)
#define MBVECTOR_P2               4    // pressione (bar)
#define MBVECTOR_T_AIR            5    // temperatura aria in ingresso (�C)
#define MBVECTOR_T_N2             6    // temperatura N2 in mandata (�C)
#define MBVECTOR_P3               7    // pressione (bar)
#define MBVECTOR_DELTAP           8    // press diff IN/OUT gruppo filtri con PD on board
#define MBVECTOR_DELTAP_EXT       9    // press diff IN/OUT gruppo filtri con PD esterno
//usati O2_Fifo                  10    // 
//usati Ore_Run                  11    // 
#define MBVECTOR_T1              12    // sonda temperatura 1 (�C)
#define MBVECTOR_T2              13    // sonda temperatura 2 (�C)
#define MBVECTOR_T3              14    // sonda temperatura 3 (�C)
#define MBVECTOR_T4              15    // sonda temperatura 4 (�C)
#define MBVECTOR_T5              16    // sonda temperatura 5 (�C)
#define MBVECTOR_T6              17    // sonda temperatura 6 (�C)
#define MBVECTOR_FLOW_AVG        18    // portata media

#define MBVECTOR_SECONDION       25    // secondi ciclo rimanenti
#define MBVECTOR_LED             30    // cumulativo stato LED bit-wise
#define MBVECTOR_RELE_1          31    // stato rel�
#define MBVECTOR_RELE_2          32    // stato rel�
#define MBVECTOR_RELE_3          33    // stato rel�
#define MBVECTOR_RELE_4          34    // stato rel�
#define MBVECTOR_RELE_5          35    // stato rel�
#define MBVECTOR_RELE_6          36    // stato rel�
#define MBVECTOR_ALLARMI         50    // flag allarmi
#define MBVECTOR_ALLARMI_2       51
#define MBVECTOR_STATO           75    // flag stato di funzionamento
#define MBVECTOR_TIME_ASS        76    // imposta tempo assorbimento (Adox.Assorbimento)
#define MBVECTOR_TIME_CMP        77    // imposta tempo compensazione (Adox.Compensazione)
#define MBVECTOR_TIME_SCARICO    78   // imposta tempo scarico (Adox.Scarico)
#define MBVECTOR_TIME_SFASAM     79   // imposta tempo sfasamento stby (Adox.Sfasamento)
#define MBVECTOR_CMD_SETO2       80    // imposta set lavoro PSA (Adox.SetO2)
#define MBVECTOR_CMD_SET_TIN     81    // imposta set temperatura massima aria in ingresso PSA (Adox.Set_TIN)
#define MBVECTOR_O2_HIGH_AL      82    // imposta set allarme ossigeno prodotto impuro
#define MBVECTOR_AL_RESET       148    // resetta allarmi PSA
#define MBVECTOR_COMANDO        149    // comando remoto ON/OFF

//definizione allarmi da trasmettere
#define MB_ALLARMI_IN1   0x0001   // IN 1:  allarme pressione in linea - AL5
#define MB_ALLARMI_IN2   0x0002   // IN 2:  allarme pressione aria (pre allarme)
#define MB_ALLARMI_IN3   0x0004   // IN 3:  allarme pressione aria (bloccante)
//#define MB_ALLARMI_IN2   0x0002   // IN 2:  eliminato - allarme mancata depressione - deprimostato pompa 1 - AL10
//#define MB_ALLARMI_IN3   0x0004   // IN 3:  eliminato - allarme mancata depressione - deprimostato pompa 2 - AL10
#define MB_ALLARMI_IN4   0x0008   // IN 4:  allarme mancanza aria compressa - AL2
#define MB_ALLARMI_IN5   0x0010   // IN 5:  allarme rel� termico - AL3
#define MB_ALLARMI_IN6   0x0020   // IN 6:  guasto scaricatore
#define MB_ALLARMI_IN7   0x0040   // IN 7:  errore sequenza fasi - AL1
#define MB_ALLARMI_IN8   0x0080   // IN 8:  allarme inverter - AL13
#define MB_ALLARMI_IN9   0x0100   // IN 9:  eliminato - allarme alta temperatura pompa 1 - AL4
#define MB_ALLARMI_IN10  0x0200   // IN 10: eliminato - allarme alta temperatura pompa 2 - AL4
#define MB_ALLARMI_IN11  0x0400   // IN 11: allarme purezza O2 prodotto - AL20
#define MB_ALLARMI_IN12  0x0800   // IN 12: allarme alta temperatura cabinet - arresta la macchina
#define MB_ALLARMI_IN13  0x1000   // IN 13: alert alta temperatura cabinet
#define MB_ALLARMI_IN14  0x2000   // controllo  500h - AL6
#define MB_ALLARMI_IN15  0x4000   // controllo 5000h - AL7
#define MB_ALLARMI_IN16  0x8000   // alta temperatura ADOX

#define MB_ALLARMI_2_B1          0x0001   // allarme trasduttore pressione P1 (Pressione positiva)
#define MB_ALLARMI_2_B2          0x0002   // allarme trasduttore pressione P2 (Vacuum)
#define MB_ALLARMI_2_B3          0x0004   // allarme trasduttore pressione P3 (pressione N2)
#define MB_ALLARMI_2_CAN_VALVE   0x0040   // allarme comunicazione CAN con valvole FESTO
#define MB_ALLARMI_2_CAN         0x0080   // allarme comunicazione CAN con CIE
#define MB_ALLARMI_2_SLAVE       0x1000   // allarme comunicazione CAN con Adox Slave


#define READ_COILS       0x01   // codice funzione (1-lettura stati allarmi - bit) read coil status
#define READ_REGISTER    0x03   // codice funzione (3-lettura registri analogici) read holding register
#define WRITE_FUNCTION   0x06   // codice funzione (6-scrittura) preset single register
#define MULTIPLE_WRITE_F 0x10   // codice funzione scrittura multipla registri (0x10 = 16 - scrittura) preset multiple register

//#define ANSWER_DELAY      0   //in decimi di sec risponde al master in un tempo compreso tra ANSWER_DELAY-1 e ANSWER_DELAY
#define MAX_WAIT4BYTE     500   //us tempo massimo attesa byte successivo

  //Modbus - dati esterni da comunicare
  struct modbus
  {
    int Array[200];
    //tra 0-49     letture analogiche
    //tra 50-74    ingressi digitali e allarmi in bit
    //tra 75-85    stato funzionamento in bit
    //a   99       comando ricevuto ON/OFF
    //tra 150-174  ingressi digitali e allarmi in word
    //tra 175-199  stato funzionamento in word
    int Preview[2];
  };

#define MASTER_1_MASTER_2 0x01
#define MASTER_1_SLAVE_2  0x02
#define SLAVE_1_SLAVE_2   0x10
#define SLAVE_1_MASTER_2  0x20

void Modbus_InitCrc16Table(void);
void UART_2_Initialization(void);	
void UART_Modbus_Master_polling(void);
void UART_3_Initialization(void);
void UART_Modbus_Slave_answer(void);

	
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

//  3. CAN_1:  CANOpen Master
#define N_CANOPEN          1   //numero dispoisitivi CANOpen
#define TMR_NMT_MESSAGE    5

void CAN_BUS_Initialization(void);
void CAN_ReInit(void);
void CAN_1_read_buffer(void);
void CAN_1_polling(void);
void CAN_1_set_date(void);
void CAN_1_setup_devices(void);
void CAN_1_NMT_message (void);

//  4. CAN_2:  CANOpen Slave

void CAN_2_read_buffer(void);
