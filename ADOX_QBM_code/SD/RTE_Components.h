
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'File_Demo' 
 * Target:  'LPC4088 Flash' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "LPC407x_8x_177x_8x.h"

/*  ARM::CMSIS:RTOS:Keil RTX:4.82.0 */
#define RTE_CMSIS_RTOS                  /* CMSIS-RTOS */
        #define RTE_CMSIS_RTOS_RTX              /* CMSIS-RTOS Keil RTX */
/*  Keil.ARM Compiler::Compiler:I/O:File:File System:1.2.0 */
#define RTE_Compiler_IO_File            /* Compiler I/O: File */
          #define RTE_Compiler_IO_File_FS         /* Compiler I/O: File (File System) */
/*  Keil.ARM Compiler::Compiler:I/O:STDERR:ITM:1.2.0 */
#define RTE_Compiler_IO_STDERR          /* Compiler I/O: STDERR */
          #define RTE_Compiler_IO_STDERR_ITM      /* Compiler I/O: STDERR ITM */
/*  Keil.ARM Compiler::Compiler:I/O:STDIN:ITM:1.2.0 */
#define RTE_Compiler_IO_STDIN           /* Compiler I/O: STDIN */
          #define RTE_Compiler_IO_STDIN_ITM       /* Compiler I/O: STDIN ITM */
/*  Keil.ARM Compiler::Compiler:I/O:STDOUT:ITM:1.2.0 */
#define RTE_Compiler_IO_STDOUT          /* Compiler I/O: STDOUT */
          #define RTE_Compiler_IO_STDOUT_ITM      /* Compiler I/O: STDOUT ITM */
/*  Keil.ARM Compiler::Compiler:I/O:TTY:Breakpoint:1.2.0 */
#define RTE_Compiler_IO_TTY             /* Compiler I/O: TTY */
          #define RTE_Compiler_IO_TTY_BKPT        /* Compiler I/O: TTY Breakpoint */
/*  Keil.MDK-Pro::File System:CORE:LFN:6.12.0 */
#define RTE_FileSystem_Core             /* File System Core */
          #define RTE_FileSystem_LFN              /* File System with Long Filename support */
          #define RTE_FileSystem_Release          /* File System Release Version */
/*  Keil.MDK-Pro::File System:Drive:Memory Card:6.12.0 */
#define RTE_FileSystem_Drive_MC_0       /* File System Memory Card Drive 0 */

/*  Keil.MDK-Pro::File System:Drive:NAND:6.12.0 */
#define RTE_FileSystem_Drive_NAND_0     /* File System NAND Flash Drive 0 */

/*  Keil::CMSIS Driver:MCI:1.0 */
#define RTE_Drivers_MCI0                /* Driver MCI0 */
/*  Keil::CMSIS Driver:NAND:Memory Bus:1.1.0 */
#define RTE_Driver_NAND_MemoryBus       /* Driver NAND Flash on Memory Bus */


#endif /* RTE_COMPONENTS_H */
