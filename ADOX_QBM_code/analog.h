


#define ADC_NUM       1   //le prove le faccio segliendo sempre il primo canale dell'ADC_NUM (in questo caso il primo ADC)
#define PROVA_PT100       // mettere la resistenza 130ohm su JP5 - NO JP1
//#define PROVA_mA          // mettere tutti i jumper JP5 JP1
//#define PROVA_mV          // togliere i jumper JP1 JP5

#define SSP0_PORT  (LPC_SSP0)   /* LPC4088 */
#define SSP0_CLOCK (200000)

#define SSP2_PORT  (LPC_SSP2)   /* LPC4088 */
#define SSP2_CLOCK (200000)

struct timer_x {			
	unsigned int msec_timer;	
	unsigned char mode_action ;
};

struct timer_y {			
	unsigned int msec_timer;	
	unsigned char mode_action ;
	unsigned char switch_ADC;
};	


void ADC_SSP_init(void) ;							
void ADC_default(unsigned char num_ADC) ;		
void ADC_set_mode(unsigned char num_ADC) ;		
void ADC_conf_reg_ch(unsigned char num_ADC, unsigned char num_CH, unsigned char mode) ;		
void ADC_read(unsigned char num_ADC, unsigned char num_CH, unsigned char mode) ;		
unsigned int ADC_value_read(unsigned char num_ADC, unsigned char num_CH) ;
unsigned int ADC_value_read_med(unsigned char num_ADC, unsigned char num_CH) ;
				
void ADC_CS_init_ADC5(void) ;
void ADC_default_ADC5(void) ;
void ADC_set_mode_ADC5(void) ;
void ADC_conf_reg_ch_ADC5(unsigned char num_CH, unsigned char mode) ;
void ADC_read_ADC5(unsigned char num_CH, unsigned char mode) ;
unsigned int ADC_value_read_ADC5(unsigned char num_CH) ;
unsigned int ADC_value_read_ADC5_med(unsigned char num_CH) ;
