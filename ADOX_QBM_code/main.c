
/*************************************************************************************************************************************
* @file			main.c
* @brief		SBM Analizzatore - 
* @date			09 Novembre 2016
* @author		Antonio Goi
***************************************************************************************************************************************/
 
 
/***************************************************************************************************************************************/
																													/* INCLUDE */ 
/***************************************************************************************************************************************/

#include "Drivers/lpc_gpio.h"
#include "Drivers/lpc_clkpwr.h"
#include "Drivers/lpc_pinsel.h"
#include "Drivers/lpc_gpdma.h"
#include "Drivers/lpc_ssp.h"
#include "Drivers/lpc_timer.h"
#include "Drivers/lpc_i2c.h"
#include "Drivers/lpc_rtc.h"
#include "Drivers/lpc_types.h"
#include "Drivers/lpc_eeprom.h"
//#include "Drivers/lpc_uart.h"
#include "Drivers/lpc_can.h"
#include "Drivers/lpc_wwdt.h"
#include "Drivers/lpc_pwm.h"//prova PWM
#include "Drivers/lpc_adc.h"

#include "Drivers/board.h"
#include "Drivers/ea_lcd_board.h"
#include "Drivers/tsc2046_touch.h"
#include "EasyGui/GuiLib.h"
#include "lingue.h"

// SD lib.
// #include "../SD/RTL.h"
#include <File_Config.h>
#include "SD/MCI_LPC177x_8x.h"
#include <stdio.h>
#include <stdlib.h>
#include "stdint.h"
#include "math.h"




/***************************************************************************************************************************************/
																													/* REFERENCE */
/***************************************************************************************************************************************/
extern struct adox Adox1500;
extern struct analog_input AnalogInput[];


/***************************************************************************************************************************************/
																											/* PRIVATE DEFINTIONS */
/***************************************************************************************************************************************/

#define I2C_PORT (I2C_0)

// Vedi "board.h"

/***************************************************************************************************************************************/
																											/* PRIVATE VARIABLES */
/***************************************************************************************************************************************/
// GENERAL PURPOSE & SYSTEM

	extern unsigned int O2ZeroADC, O2SpanADC;
	extern unsigned int O2in;
	extern unsigned int O2Span;

  extern unsigned int ADC_value[5][3];
	unsigned int ADC_preview_value[5][3];
  unsigned char adc_counter_PT100_1;
  unsigned char adc_counter_PT100_2;
  unsigned char adc_counter_PT100_1_fault;
  unsigned char adc_counter_PT100_2_fault;

	// ADC
	uint32_t adc_value_int;
	uint32_t adc_internal[8] ;
	unsigned int adc_value_ext_temp ;

  unsigned char new_fault;  // da settare a 1 al verificarsi di un qualsiasi nuovo guasto per segnalarlo
	unsigned int tmp_hour = 25;
	unsigned int tmp_day;									 
	unsigned char daily_reset_counter = 0;
	unsigned char reset_after_new_configuration_load = 0;
  unsigned char tmrPT100=10;
	
	unsigned int buzzer_ON=0;
	unsigned char buzzer_enabled;
	unsigned int buzzer_modulation=0;

  unsigned int index_mem;
	
  uint8_t mem_w_data[EEPROM_I2C_PAGE_SIZE+2];
  uint8_t mem_r_data[EEPROM_I2C_PAGE_SIZE];

	//sensore MIS7300: pressione temperatura
	unsigned int MCLKs;
	char MCLK_i; // clock sensore pressione (timer 2)
	char C1_C10; // avvenuta lettura parametri sensore pressione
	int CCC[16]; // parametri sensore pressione
	char Press_FirstTime_and_invert_channel = 0;
	float temp_MIS7300=0;
	float Press_MIS7300=0;
	float altitude;
	int  altitude_offset;
	float pressure_altitude;
	float equivalent_altitude;
  // LED		
	uint8_t LED_output;			

	//sensore HDC1000: temperatura RH
	float temp_UR100CD;		
	float RH_UR100_CD;
	float	pvs_UR100CD;
	float temp_HDC1000;
	float RH_HDC1000, RH_HDC1000_percent;

  //optical O2 sensor
  int request_time=1000; //milliseconds
  float phi_avg[TRENI+1];
  double O2_avg[O2_AVG_MOBILE+1];
  float O2_phi_A;
  double O2_ist_part_A;
  double O2_medio;
  double O2_medio_avg;
  double O2_medio_span;
  double O2_medio_span_avg;
  char O2_command;
  float dtemp_dt;
  //calibration values
  double coeff_o2a[15]; 
  double o2a_correct=0;
  double o2_correct=0;
  float O2_medio_average[NSAMPLES];
  float O2_medio_span_average[NSAMPLES];
  int isCalibrated=0;
  int valore_Timer2, valore_Timer3;
  
	unsigned int counter_Timer1, counter_Timer2;
	
	struct timer1{
//		double touch;
//		double touch_timer;
		
		int main_trigger;
		int main_trigger_timer;
		int datalog_trigger;
		int datalog_trigger_timer;
		int first_touch;
		int first_touch_timer;
		int continuous_touch;
		int continuous_touch_timer;
		int	minimal_button_time;
		int minimal_button_timer;
		int tgs_sensor_measure;
		int tgs_sensor_measure_timer;
		int secondi;
		int secondi_timer;
		
		double msec_timer;
	} TIM1;
	
	struct timer9 {			
		double msec_timer;	
	} TIM9;
	
	struct timer10 {			
		double msec_timer;	
	} TIM10;		

	struct timer11 {			
		double msec_timer;	
	} TIM11;		
	
	//struttura per il riconoscimento di un guasto/errroe (serve per l'ACK)
	struct fault {			
		unsigned int Detected_1;	
		unsigned int Detected_2;	
	} Fault;		
	
  unsigned char touch_not_released = 0;

	char cambio_valori=0;
	// configurazione impianto
	// colonne: zona, item_type, ID, port
	char GPS[MAX_ITEM][ITEM_COLUMNS];
	char indice_visualizzazione_GPS=0;
	unsigned long max_item_used;
	unsigned char rs485_imposta_soglie_allarme = 2;
	unsigned char check_index_sensor=0, check_last_sensor=0;
	
  // questi poi saranno inizializzati in "init_variables"
	int sys_zones_num        		= 0;
	volatile unsigned int sys_sensors_num = 0;
	char sys_fans_num             = 0;
	char sys_doors_num            = 0;
  char sys_adox_num             = 0;
	char sys_sirens_num       		= 0;
	char sys_panels_num       		= 0;
	char sys_lamps_num        		= 0;
	char sys_valves_num       		= 0;
	char sys_ext_displays_num 		= 0;
	char sys_Anybus_num           = 0;
	char sys_fluxes_num       		= 0;
	char sys_modules_num      		= 0;
	char sys_power_supply_num 		= 0;
	char sys_batteries_num    		= 0;
	char sys_psa_num          		= 0;
	char sys_leds_num          		= 0;
	char occupation_ok;
	char sys_elements_num[CONSTANT_ZONES][TYPES_OF_OBJECTS];
	char sys_switches_num         = 0;
	char sys_thermostat_num       = 0;
	
	char sys_language             = 0;
	
	unsigned int temp = 0;
	float float_temp = 0;
	unsigned int output_off_state  = 0;	// attivazioni; se 1 : porta le uscite al loro stato iniziale
	unsigned int enable_aux_supply = 0; // se 1 devo attivare la valvola di immissione N2 ausiliaria
	unsigned int shut_up_siren     = 0; // 1 se premo il bottone "spegni sirena" dalla schermata principale   
	unsigned int shut_up_buzzer    = 0; // 1 se premo il bottone "spegni buzzer" dalla schermata principale   
	unsigned int leds_in_test      = 0; // 1 se ho abilitato il test dei led e della grafica dalla schermata principale
	unsigned int backlight = 0;
	unsigned char setbacklight = 0;
	unsigned int last_loaded_backlight;
	
	// Variabili legate al timer 2 (ogni 100 ms)
	volatile unsigned int message_timer_count  = 0;
	volatile unsigned int alarm_timer_count    = 0;
	volatile unsigned int idle_state           = 0;
	volatile unsigned int idle_screen_timeout  = 0;
	volatile unsigned int auto_logout          = 0;
	volatile unsigned int test_leds_timeout    = 0;
	volatile unsigned int print_o2_timeout     = SAVE_O2_VALUES;
	volatile unsigned int ewon_routine_timeout = 0;
	volatile unsigned int tmr_module_config    = 0;
	volatile unsigned int tmrCiclomain         = TMR_MAIN;
	
  // 20201005 - Per ingresso 9 
  volatile unsigned int tmr_ready_ADC5=0;
  volatile uint8_t sts_sample = 0; 
  
  
  volatile unsigned int tmrSecondo=0;	
  volatile unsigned int tmr_10_Minuti=0;
  volatile unsigned int resume_backlight=0;
  volatile unsigned int tmrLowBacklight=0;
  volatile unsigned int tmr_user_press_button=0;
  volatile unsigned int PowerNetworkFaults=0;
	volatile unsigned int Timer_repeat=0;
  
	unsigned char new_value_to_display = 0; // 18_06 cristian. Prova per attivare Aggiornamento Display dopo pressione tasto che esegue un cambiamento reale
  
	
	/* Transmission */
//	unsigned int sensor_polling_index 			   = 0;
//	unsigned int rs485_main_schedule  				 = 0;
//	unsigned int can_modules_out_polling_index = 0;
//	unsigned int can_modules_out_polling_index_2 = 0;
//	unsigned int can_modules_in_polling_index  = 0;
//	unsigned int can_displays_polling_index 	 = 0;
//	unsigned int can_displays_polling_index_2	 = 0;
	unsigned int index_ewon 									 = 0;
	unsigned int index_ewon_led 						   = 0;	
	unsigned int index_ewon_alarms_num 				 = 0;
	unsigned int index_ewon_faults_num 				 = 0;
	unsigned int index_ewon_disablements_num   = 0;
	unsigned int index_ewon_dev_ctrl_num 		   = 0;
	unsigned int index_ewon_others 						 = 0;
	unsigned int index_ewon_sensors 					 = 0;
	unsigned int index_ewon_zones 						 = 0;
	unsigned int index_ewon_power 						 = 0;
	unsigned int index_ewon_fluxes 						 = 0;
	unsigned int index_ewon_modules 					 = 0;
	unsigned int index_ewon_displays 					 = 0;
	unsigned int index_ewon_seconds            = 0;
	
	unsigned int tmr_startup, tmr_warm_up_alarm,tmr_warm_up_fault;
	unsigned int tmrChechSD,tmr_toggle1, tmr_toggle2, tmr_toggle3, tmr_toggle4, tmr_toggle5;
 
  char temp_string[55];//ATT verificare che non ci siano problemi con la var locale
  char * address = &temp_string[0];//ATT verificare che non ci siano problemi con la var locale

		/* Time & Date */
		struct Time {
			unsigned int day;
			unsigned int month;
			unsigned int year;
			
			unsigned int hour;
			unsigned int minutes;
			unsigned int seconds;
		};
		struct Time current_time;

	   /* Events */		
		struct Event
		{			
			struct Time time;						// momento in cui si � verificato l'evento		
			unsigned int type;					// Tipo di evento (FAULT, ALARM, DISABLEMENTS, DEV_CTRL, MISC)
			unsigned int type_2;				// Usato per descrivere i guasti (tipo di oggetto: sensore, flussostato ecc.)
			unsigned int specific_type;	// Tipo specifico (Fault type, Alarm type, Misc type, ecc.)
			unsigned int object_type;   // Tipo di oggetto (sirena, lampeggiante, ecc.)
			unsigned int id;            //per i fault di digin e output
			unsigned int id_1;
			unsigned int id_2;
			unsigned int user_level;		
			unsigned int user_id;				
			float float_value_1;		
			float float_value_2;
			unsigned int in_out; // indica, per esempio, se il guasto si verifica o va via
			
		};

	
   struct Object
		{
			unsigned int type;					// Tipo  (ZONE, SENSOR, SIREN...)
			unsigned int specific_type;	// Tipo specifico (es. valvola ausiliaria o normale, lamp della centrale o delle zone, ecc.)
			unsigned int zone;
			unsigned int id;
			
		};

		struct Fault
		{
			unsigned int type;					// Tipo  (SENSOR, FLUX)
			unsigned int specific_type;	// Tipo specifico (es. guasto comunicazione, sensore ecc.)
			unsigned int id;
			unsigned int zone;
			unsigned int digIN;
			unsigned int OUTput;
		};
		
		struct Event temp_event;
		struct Event events_array[EVENTS_ARRAY_DIMENSIONS];
		int index_events = 0;
		unsigned int events_array_dim = 0;
		
		/* System */
		int index_system = 0;
		unsigned int system_in_alarm = 0;
		
		/* Faults */
		int index_faults = 0;
		struct Fault faults_array[FAULTS_ARRAY_DIMENSIONS];
		unsigned int faults_number       = 0;
		unsigned int system_faults_number = 0;
		unsigned int energy_faults_number = 0;
		unsigned int faults_array_dim;
		struct Fault temp_fault;
		
		/* Alarms */
		int index_alarms = 0;
		struct Event alarms_array[ALARMS_ARRAY_DIMENSIONS];
		unsigned int alarms_number = 0;
		unsigned int alarms_array_dim;
		
		struct Object temp_object;
		
		/* Disablements */
		int index_disablements = 0;
		struct Object disablements_array[DISABLEMENTS_ARRAY_DIMENSIONS];
		unsigned int disablements_number = 0;
		unsigned int disablements_array_dim = 0;
		char disablements_change[NUMBER_OF_OBJECTS];
		char disablements_before_change[NUMBER_OF_OBJECTS];
		unsigned int sort_disablements = 0;
		
		/* Devices control */
		int index_dev_ctrl = 0;
		struct Object dev_ctrl_array[DEV_CTRL_ARRAY_DIMENSIONS];
		unsigned int dev_ctrl_number = 0;
		unsigned int dev_ctrl_array_dim = 0;
		char dev_ctrl_change[NUMBER_OF_OBJECTS];
		char dev_ctrl_before_change[NUMBER_OF_OBJECTS];
		unsigned int sort_dev_ctrl = 0;
		
		/* Zone x */
		int index_zone_x = 1;
		
		/* TH screen */
		int index_sensor = 0;			

		/* Password */
		struct User
		{
			volatile unsigned int level;					
			unsigned int id;	
			unsigned int operation;	
		};
		
		volatile unsigned int pwd_blink = 0;
		
		struct User current_user;
		struct User guest_user;  // serve per operazioni fatte da eWon o da display esterno
		unsigned int password[5];
		unsigned int pswd_index = 0;
		
/***************************************************************************************************************************************/
/* SYSTEM ELEMENTS - STRUCTS 	*/
/***************************************************************************************************************************************/
	struct cie 
		{
			unsigned int Master;
			volatile unsigned int CommTimeout;
			unsigned int Comm_fault;
			unsigned int Reply;
		};
		struct cie CIE;
		
		/* All objects of the project */
		// questa volta la posizione [0] � utilizzata!
		struct Object objects_array[NUMBER_OF_OBJECTS];
		struct Object sorted_objects_array[NUMBER_OF_OBJECTS];
		struct Object sorted_objects_array_2[NUMBER_OF_OBJECTS];
		
		int index_objects = 0;
		unsigned int objects_array_dim 				= NUMBER_OF_OBJECTS;
		unsigned int sorted_objects_array_dim = NUMBER_OF_OBJECTS;

		unsigned int ao_first_zone        = 0;
		unsigned int ao_first_sensor      = 0;
		unsigned int ao_first_siren       = 0;
		unsigned int ao_first_panel       = 0;
		unsigned int ao_first_lamp        = 0;
		unsigned int ao_first_valve       = 0;
		unsigned int ao_first_ext_display = 0;
		unsigned int ao_first_flux        = 0;
		unsigned int ao_first_buzzer      = 0;

		unsigned int zones_array_superuser_disabled[CONSTANT_ZONES+1];
		unsigned int sensors_array_superuser_disabled[CONSTANT_SENSORS+1];
		unsigned int displays_array_superuser_disabled[CONSTANT_EXT_DISPLAYS+1];
		unsigned int sirens_array_superuser_disabled[CONSTANT_SIRENS+1];
		unsigned int lamps_array_superuser_disabled[CONSTANT_LAMPS+1];
		unsigned int panels_array_superuser_disabled[CONSTANT_PANELS+1];
		unsigned int fluxes_array_superuser_disabled[CONSTANT_FLUXES+1];
		unsigned int valves_array_superuser_disabled[CONSTANT_VALVES];

/* ---------------------------------------------------------------------------------------------*/		
   	struct Zone
		{
			unsigned int n_sensors;					// numero di sensori di questa zona
			unsigned int n_sirens;					// numero di sirene di questa zona
			unsigned int n_lamps;					  // numero di lampeggianti di questa zona
			unsigned int n_panels;					// numero di panbnelli di questa zona
			unsigned int n_valves;					// numero di valvole di questa zona
			unsigned int n_ext_displays;		// numero di display esterni di questa zona
			unsigned int n_fluxes;					// numero di flussostati di questa zona
			
			unsigned int first_sensor;      // primo sensore della zona	(id)
			unsigned int first_siren;       // prima sirena  della zona	(id)
			unsigned int first_lamp;        // primo lmapeggiante della zona	(id)
			unsigned int first_panel;       // primo pannello della zona	(id)
			unsigned int first_valve;       // prima valvola della zona	(id)
			unsigned int first_ext_display; // primo display esterno della zona	(id)
			unsigned int first_flux;        // primo flussostato della zona	(id)
			
			float set;		                  // valore del set impostato per la zona
			float hyst;											// valore di isteresi impostato per la zona
			float th_high; 									// valore della soglia alta di allarme ossigeno
			float th_low;										// valore della soglia bassa di allarme ossigeno
			float o2_mean;									// valore di ossigeno mediato della zona (dai suoi n sensori)
			unsigned int in_alarm;					// 1 = in allarme; 0 = no
			unsigned int activated;					// attivazione/test
			unsigned int disabled;					// disabilitazione/esclusione
			unsigned int no_value;					// 1 = nessun valore di ossigeno valido (troppa differeneza fra sensori, ecc.)
			unsigned int diff_1;						// questo � 1 se almeno 1 sensore appartenente alla zona ha guasto diff_1_fault
			unsigned int diff_2;						// questo � 1 se almeno 1 sensore appartenente alla zona ha guasto diff_2_fault
			unsigned int disabled_from_display;						// disabilitazione da display
		};
		// la zona 1 � nella posizione [1] ecc, si salta la [0] (per comodit�)
		struct Zone zones_array[CONSTANT_ZONES+1];
		
		// zone selezionate per modificarne set e hyst
		// la zona 1 � nella posizione [1] ecc, si salta la [0] (per comodit�)
		unsigned int zones_change_set[CONSTANT_ZONES + 1];
		unsigned int zones_change_th[CONSTANT_ZONES + 1];
/* ---------------------------------------------------------------------------------------------*/	

		struct Door 
		{
			unsigned int on;
			unsigned int zone;
			unsigned int id;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;						
		};
		struct Door doors_array[CONSTANT_DOORS];


		struct Fan 
		{
			unsigned int on;
			unsigned int zone;
			unsigned int id;			
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;						
		};
		struct Fan fans_array[CONSTANT_FANS];

		struct Adox_fault
		{
			unsigned int on;
			unsigned int fault_detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct Adox_fault adox_fault_array[CONSTANT_PSA];

		struct Badge_Status
		{
			unsigned int on;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct Badge_Status badge_status_array[CONSTANT_ZONES+1];

		struct Adox_start
		{
			unsigned int on;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;			
			uint8_t logic;				
		};
		struct Adox_start adox_start_array[CONSTANT_PSA];

		
		struct Sensor
		{
			float th_high;		              // soglia di allarme alta impostata per il sensore
			float th_low;									  // soglia di allarme bassa impostata per il sensore
			float o2_value;
			float o2_new_value;
			float offset_o2_value;
			float offset_before_change;
			float o2_value_from_sensors;
			float new_offset_flag;
			unsigned int in_alarm;
			unsigned int in_alarm_low;      // 1 = allarme ossigeno basso !		
			unsigned int in_alarm_fake;			// 1 = prima era in allarme (per capire quando aggiungere un allarme... perch� il vero allarme "rosso" resta sulla centrale,
																			// ma se il valore di ossigeno supera la soglia, poi torna sotto e poi nuovamente sopra, devo avere 2 allarmi e invece se
																			// non lo uso non se ne accorge del secondo)
			unsigned int activated;
			unsigned int disabled;
			unsigned int zone;
			unsigned int sd_fault;
			unsigned int comm_fault;
			unsigned int elab_fault;
			unsigned int diff_1_fault;     // questo � il guasto che si verifica quando la differenza del valore di O2 fra il sensore e il successivo � compresa fra 0,5% e 2%
			unsigned int diff_2_fault;		 // questo � il guasto che si verifica quando la differenza del valore di O2 fra il sensore e il successivo � compresa >= 2%
			unsigned int warm_up;
			volatile unsigned int comm_timeout;
      char flag;
		};
		// il sensore 1 � nella posizione [1] ecc, si salta la [0] (per comodit�)
		struct Sensor sensors_array[CONSTANT_SENSORS + 1];

		// sensori selezionati per modificarne soglie alta e bassa
		// il sensore 1 � nella posizione [1] ecc, si salta la [0] (per comodit�)
		char sensors_change_th[CONSTANT_SENSORS + 1];
		char communication_offset_done=2;
		char communication_offset_recived=0;
		char starting_answer_offset_recived=0;
/* ---------------------------------------------------------------------------------------------*/	
		
		struct Display
		{
			unsigned int comm_fault;
			unsigned int activated;
			unsigned int disabled;
			unsigned int zone;
			unsigned int on;
			volatile unsigned int comm_timeout;
		};
		// il display 1 � nella posizione [1] ecc, si salta la [0] (per comodit�)
		struct Display displays_array[CONSTANT_EXT_DISPLAYS + 1];

/* ---------------------------------------------------------------------------------------------*/	
		struct Adox
		{
			volatile unsigned int comm_timeout;
			unsigned int comm_fault;
			char id;
			char input_cto_on[MODULES_INPUT_NUM + 1];
			char input_open_on[MODULES_INPUT_NUM + 1];
			char output_cto_on[MODULES_OUTPUT_NUM + 1];
			char output_open_on[MODULES_OUTPUT_NUM + 1];
			
			char input_cto_detected[MODULES_INPUT_NUM + 1];
			char input_open_detected[MODULES_INPUT_NUM + 1];		
			char output_cto_detected[MODULES_OUTPUT_NUM + 1];
			char output_open_detected[MODULES_OUTPUT_NUM + 1];

			uint8_t outputs;  // byte delle uscite   (1 uscita ogni bit)
			uint8_t inputs;		// byte degli ingressi (1 ingresso ogni bit)
		};
		struct Adox adox_array[CONSTANT_PSA];
/*--------------------------------------------------------------------------------------------------*/
		struct Module
		{
			volatile unsigned int comm_timeout;
			volatile unsigned int comm_timeout_can_1;
			volatile unsigned int comm_timeout_can_2;
			unsigned int comm_fault_rele;
			char id;
			char input_cto_on[MODULES_INPUT_NUM + 1];
			char input_open_on[MODULES_INPUT_NUM + 1];
			char output_cto_on[MODULES_OUTPUT_NUM + 1];
			char output_open_on[MODULES_OUTPUT_NUM + 1];
			
			char input_cto_detected[MODULES_INPUT_NUM + 1];
			char input_open_detected[MODULES_INPUT_NUM + 1];		
			char output_cto_detected[MODULES_OUTPUT_NUM + 1];
			char output_open_detected[MODULES_OUTPUT_NUM + 1];

			uint8_t outputs;  // byte delle uscite   (1 uscita ogni bit)
			uint8_t inputs;		// byte degli ingressi (1 ingresso ogni bit)
			
			unsigned int module_can_error_counter;		// contatore errori can di ogni modulo
			unsigned int enabled_input;		// input abilitati
			unsigned int enabled_output;	// output abilitati
			
			uint8_t controllo_uscite;
			uint8_t inputs_close;
			uint8_t inputs_cut;
			uint8_t inputs_short_circuit;
			uint8_t inputs_open;
			uint8_t ingressi_digitali_selezionati;
			uint8_t rele_selezionati;
			uint8_t port_digin_with_short_circuit;
			uint8_t port_digin_with_cut;
			uint8_t port_rele_with_fault;
			uint8_t variabile_temporanea_cc;
			uint8_t variabile_temporanea_cut;
			uint8_t variabile_temporanea_rele;
	    uint8_t variabile_temporanea_2;
	    uint8_t variabile_temporanea_3;
			uint8_t variabile_temporanea_4;
		  uint8_t comm_fault;
			uint8_t comm_fault_can_1;
			uint8_t comm_fault_can_2;
			uint8_t comm_fault_digin_cut;
			uint8_t comm_fault_digin_short_circuit;
			uint8_t flag_event_rele_with_fault;
			uint8_t flag_event_digin_with_short_circuit;
			uint8_t flag_event_digin_with_cut; 
			uint8_t power_supply_with_fault;
			uint8_t comm_fault_power_supply_1;
			uint8_t comm_fault_power_supply_2;
			uint8_t variabile_temporanea_5;
			uint8_t fault_power_supply_1;
			uint8_t fault_power_supply_2;
			uint8_t variabile_temporanea_pw_1;
			uint8_t variabile_temporanea_pw_2;
		};
		
		// Gli ingressi e le uscite sono numerati da 1 a 8 (o comunque MAX) per comodit�
		struct Module modules_array[CONSTANT_MODULES + 1];

/* ---------------------------------------------------------------------------------------------*/		
		struct Can_network_fault
		{
			unsigned int on;
			unsigned int detected;	
			unsigned int Module_Falult;		
		};
		struct Can_network_fault can_network_fault;

/* ---------------------------------------------------------------------------------------------*/

		struct Sd_card 
		{
			unsigned int fault;
			unsigned int detected;
		};
		struct Sd_card SBM_sd_card;

/* ---------------------------------------------------------------------------------------------*/

		struct Ewon 
		{
			unsigned int Anybus_Init_Request;//ver 2.08
			unsigned int comm_fault;
			volatile unsigned int comm_timeout;
		};
		struct Ewon ewon;
		
/***************************************************************************************************************************************/
	/* OUTPUTs 	*/
/***************************************************************************************************************************************/

		struct Siren
		{
			unsigned int on;
			unsigned int activated;
			unsigned int auto_activated;
			unsigned int disabled;
			unsigned int zone;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;
			uint8_t module_3;
			uint8_t n_output_3;
			uint8_t logic;
		};
		// la sirena 1 � nella posizione [1] ecc, la [0] � quella della centrale
		struct Siren sirens_array[CONSTANT_SIRENS+1]; //Antonio 04.02.14 BUG indice??? ho aggiunto +1 !!!!
		
/* ---------------------------------------------------------------------------------------------*/	
		
		struct Lamp
		{
			unsigned int on;
			unsigned int activated;
			unsigned int auto_activated;
			unsigned int disabled;
			unsigned int zone;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;
			uint8_t logic;			
		};
		// il lampeggiante 1 � nella posizione [1] ecc, la [0] � quella della centrale
		struct Lamp lamps_array[CONSTANT_LAMPS+1]; //Antonio 04.02.14 BUG indice??? ho aggiunto +1 !!!!

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Panel
		{
			unsigned int on;
			unsigned int activated;
			unsigned int auto_activated;
			unsigned int disabled;
			unsigned int zone;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;			
		};
		// il pannello 1 � nella posizione [1] ecc, si salta la [0] (per comodit�)
		struct Panel panels_array[CONSTANT_PANELS + 1];

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Valve
		{
			unsigned int on;
			unsigned int activated;
			unsigned int auto_activated;
			unsigned int disabled;
			unsigned int zone;
			unsigned int tmrOff;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;			
		};
		// la valvola 1 � nella posizione [1] ecc, la [0] � quella ausiliaria
		struct Valve valves_array[CONSTANT_VALVES];

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Led
		{
			unsigned int type;
			unsigned int on;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;			
		};
		struct Led leds_array[NUMBER_OF_LEDS+1];

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Buzzer
		{
			unsigned int on;
			unsigned int activated;
			unsigned int disabled;			
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;			
		};
		struct Buzzer cie_buzzer;

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Ext_fault_out
		{
			unsigned int on;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;			
		};
		struct Ext_fault_out ext_fault_out;

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Ext_alarm_out
		{
			unsigned int on;
			uint8_t module_1;
			uint8_t n_output_1;
			uint8_t module_2;
			uint8_t n_output_2;	
			uint8_t logic;			
		};
		struct Ext_alarm_out ext_alarm_out;
		
/***************************************************************************************************************************************/
	/* INTPUTs 	*/
/***************************************************************************************************************************************/
		
		struct Flux
		{
			unsigned int on;
			unsigned int activated;
			unsigned int disabled;
			unsigned int id;
			unsigned int zone;
			unsigned int detected;
			volatile unsigned int timeout;
			unsigned int tested;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;			
		};
		// il flussostato 1 � nella posizione [1] ecc, la [0] � quella ausiliaria
		struct Flux fluxes_array[CONSTANT_FLUXES + 1];

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Switch
		{
			unsigned int off;
			unsigned int on;
			unsigned int detected;
			volatile unsigned int timeout;
			uint8_t module_1;
			//uint8_t module_2;
			uint8_t n_input_1;
			uint8_t n_input_2;
			uint8_t logic;			
			uint8_t zone;
		};
		struct Switch switches_array[CONSTANT_SWITCHES];
		
/* ---------------------------------------------------------------------------------------------*/	
		
		struct Ext_alarm
		{
			unsigned int on;
			unsigned int detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct Ext_alarm ext_alarm;

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Ext_fault
		{
			unsigned int on;
			unsigned int detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct Ext_fault ext_fault;

/* ---------------------------------------------------------------------------------------------*/	
		
		struct Thermostat
		{
			unsigned int on;
			unsigned int detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t module_2;
			uint8_t n_input_2;
			uint8_t logic;				
		};
		struct Thermostat thermostat[CONSTANT_THERM];;

/* ---------------------------------------------------------------------------------------------*/
		
		struct PSA
		{
			unsigned int fault;
			unsigned int on;
			unsigned int fault_detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t n_output_1;
  		uint8_t module_2;
			uint8_t n_input_2;
			uint8_t n_output_2;
			uint8_t input_logic;				
			uint8_t output_logic;				
			unsigned int orelavoro;
			unsigned int secondi;
			uint8_t logic;				
		};
		struct PSA psa_array[CONSTANT_PSA];

/* ---------------------------------------------------------------------------------------------*/
		
		struct Power_supply // alimentatore
		{
			unsigned int on;
			unsigned int detected;
			unsigned int id;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct Power_supply power_supply_array[CONSTANT_PW_SUPPLY];

/* ---------------------------------------------------------------------------------------------*/
		
		struct Battery // alimentatore
		{
			unsigned int on;
			unsigned int detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct Battery battery_array[CONSTANT_BATTERIES];

/* ---------------------------------------------------------------------------------------------*/
		
		struct No_network // alimentatore
		{
			unsigned int on;
			unsigned int detected;
			uint8_t module_1;
			uint8_t n_input_1;
			uint8_t logic;				
		};
		struct No_network no_network_array[CONSTANT_PW_SUPPLY];

/* ---------------------------------------------------------------------------------------------*/

// **********************************************************************************************************		
// SDRAM
	static volatile uint32_t ringosccount[2] = {0,0};

// **********************************************************************************************************
// LCD
	int32_t dev_lcd = 0;
	
// **********************************************************************************************************
// TOUCH
  int32_t x = 0;
  int32_t y = 0;
  int32_t z = 0;
// 20201002
	uint8_t primo_tasto = TRUE;	  
	int touch_area = 0;
	unsigned int cornice = 0;	
	unsigned int touch_time = 0;

// **********************************************************************************************************
// SD 
		char dummy[100]; //metto questa per creare uno spazio vuoto, sospetto BUG overflow
		FILE *alarms_file;
		FILE *events_file;
		FILE *o2_values_file;
		FILE *GPS_file;
		FILE *test_file;
		char data_string[23];
		char data_string_name[23];
		char alarms_file_name[29];
		char events_file_name[29];
		char o2_values_file_name[35];
		char GPS_file_name[35];
		int use_sd = 0;
		char test_file_name[35];
	
// **********************************************************************************************************	
// RTC
	RTC_TIME_Type RTCFullTime;	

// **********************************************************************************************************
// EEPROM
	uint8_t read_buffer[2];
	uint8_t write_buffer[2];

// **********************************************************************************************************
// TIMER 2
	TIM_MATCHCFG_Type TIM_MatchConfigStruct ;
	TIM_TIMERCFG_Type TIM_ConfigStruct;

// SSP2
  //definisce struttura per SSP2
	SSP_CFG_Type SSP_ConfigStruct2;



	extern unsigned int rs485_rx_timeout;
	extern struct strele StRele[];
  extern struct modbus Modbus;
	extern struct digital_input DigIn;
  extern uint8_t PackFull;
  extern volatile unsigned int Ready2Send, BadFrame, TmrRxNextByte;
  extern unsigned int tmrCanReinit;
  extern unsigned int CanBusFault;
  extern unsigned char tmr_NMT_message;
  extern struct e2prom E2prom[];
  extern struct timer_y TIM2;
  extern struct timer_x TIM3;
  extern struct timer_x TIM4;
  extern struct timer_x TIM5;
	
/***************************************************************************************************************************************/		
																													/* PRIVATE FUNCTIONS */
/***************************************************************************************************************************************/

// **********************************************************************************************************





// **********************************************************************************************************
// 20201002
#ifdef READ_TOUCH_SCREEN 
  uint8_t Touch_enable=0;
  void touch_press(void);  
#endif //READ_TOUCH_SCREEN

  void touch_handle(void);  

/***********************************************************************
 * @brief		delay_ms
	 NB.: non � affatto precisa come misura del tempo...
 **********************************************************************/
 void delay_ms (unsigned int time_ms) {
	 
		unsigned int j = 0;
	 
		for (j = 0; j < (7000 * time_ms) ; j++);	 
 }
 
 /*********************************************************************//**
* Iso_PressurSensorInit
* @description configure pressure sensor
 **********************************************************************/
void Iso_PressurSensorInit()
{
	// I2C sensore pressione
	PINSEL_ConfigPin(Port_4, PRESS_MCLK	 , 0);													// clock
	GPIO_SetDir(Port_4, (1<< PRESS_MCLK ), GPIO_DIRECTION_OUTPUT);	
}

/*********************************************************************//**
* Iso_ADCInit
* @description configure ADC
 **********************************************************************/
void Iso_ADCInit()
{
	//Configuration for ADC :  ADC conversion rate = 400Khz
	ADC_Init(LPC_ADC, 400000);
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN1, DISABLE); // O2 ADC 0
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN2, DISABLE); // O2_1 ADC 2
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN3, DISABLE); // O2_2 ADC 3
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN4, DISABLE); //uEC_CO
	ADC_IntConfig( LPC_ADC, ADC_ADINTEN0, DISABLE); //uA0_CO
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_3, ENABLE);
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_2, ENABLE);
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_1, ENABLE);
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_4, ENABLE);
	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_0, ENABLE);
  ADC_StartCmd(LPC_ADC, ADC_START_NOW);	
	ADC_BurstCmd(LPC_ADC,ENABLE);
}

// PWM
// Time out definition - used in blocking mode in read/write function
#define TIME_OUT    10000

#define _USING_PWM_NO                   1

/************************** PRIVATE VARIABLES *************************/
__IO uint32_t match_cnt = 0;
__IO uint32_t long_duty = 0;

/************************** PRIVATE FUNCTIONS *************************/
/* Interrupt service routine */
#if (_USING_PWM_NO == 1)
void PWM1_IRQHandler(void);
#elif (_USING_PWM_NO == 0)
void PWM0_IRQHandler(void);
#endif

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief       GPDMA interrupt handler sub-routine
 * @param[in]   None
 * @return      None
 **********************************************************************/
#if (_USING_PWM_NO == 1)
void PWM1_IRQHandler(void)
#elif (_USING_PWM_NO == 0)
void PWM0_IRQHandler(void)
#endif
{
    /* Check whether if match flag for channel 0 is set or not */
    if (PWM_GetIntStatus(_USING_PWM_NO, PWM_INTSTAT_MR0))
    {
        match_cnt++;

        /* Clear the interrupt flag */
        PWM_ClearIntPending(_USING_PWM_NO, PWM_INTSTAT_MR0);
    }
}

/*-------------------------MAIN FUNCTION------------------------------*/

/***********************************************************************
 * @brief		I2CWrite
 **********************************************************************/
static Status I2CWrite(uint32_t addr, uint8_t* buf, uint32_t len){
  I2C_M_SETUP_Type i2cData;
        
	i2cData.sl_addr7bit = addr;
	i2cData.tx_data = buf;
	i2cData.tx_length = len;
	i2cData.rx_data = NULL;
	i2cData.rx_length = 0;
	i2cData.retransmissions_max = 3;
	
//	if (addr==0x70)
    return I2C_MasterTransferData(2, &i2cData, I2C_TRANSFER_POLLING);
//  else
//	  return I2C_MasterTransferData(I2C_PORT, &i2cData, I2C_TRANSFER_POLLING);
}


/***********************************************************************
 * @brief		I2CRead
 **********************************************************************/
static Status I2CRead(uint32_t addr, uint8_t* buf, uint32_t len){
  I2C_M_SETUP_Type i2cData;

	i2cData.sl_addr7bit = addr;
	i2cData.tx_data = NULL;
	i2cData.tx_length = 0;
	i2cData.rx_data = buf;
	i2cData.rx_length = len;
	i2cData.retransmissions_max = 3;

//	if (addr==0x70)
    return I2C_MasterTransferData(2, &i2cData, I2C_TRANSFER_POLLING);
//  else
//	  return I2C_MasterTransferData(I2C_PORT, &i2cData, I2C_TRANSFER_POLLING);
}


//-------------------------------------------------------------------------------------------------------------------------------------------------------------


uint8_t EEPROM_wrdat[ADDRESS_SIZE+EEPROM_I2C_PAGE_SIZE];
uint8_t EEPROM_rddat[EEPROM_I2C_PAGE_SIZE];

/* Transmit setup */
I2C_M_SETUP_Type txsetup;
/* Receive setup */
I2C_M_SETUP_Type rxsetup;

/************************** PRIVATE FUNCTIONS *************************/
int32_t i2c_EEPROM_Write(uint32_t mem_addr, uint8_t* buf, uint32_t len);
int32_t i2c_EEPROM_Read(uint32_t mem_addr, uint8_t* buf, uint32_t len);

/*-------------------------PRIVATE FUNCTIONS------------------------------*/
/*********************************************************************//**
 * @brief   Write a number of data byte into EEPROM EEPROM
 * @param[in]   None
 * @return  0: if success, otherwise (-1) returned.
 **********************************************************************/
//int32_t i2c_EEPROM_Write(void){
int32_t i2c_EEPROM_Write(uint32_t mem_addr, uint8_t* buf, uint32_t len){
	
	  len = (len < EEPROM_I2C_PAGE_SIZE)? len: EEPROM_I2C_PAGE_SIZE;
	  len +=2; //incrementa perch� i primi due byte sono l'indirizzo iniziale di scrittura
	  EEPROM_wrdat[0] = (mem_addr>>8) & 0x00FF;
	  EEPROM_wrdat[1] = mem_addr & 0x00FF;
  	
	  txsetup.sl_addr7bit = EEPROM_I2C_ADDR;
    txsetup.tx_data = buf;
    txsetup.tx_length = len;
    txsetup.rx_data = NULL;
    txsetup.rx_length = 0;
    txsetup.retransmissions_max = 3;

    if (I2C_MasterTransferData((en_I2C_unitId)I2CDEV, &txsetup, I2C_TRANSFER_POLLING) == SUCCESS){
			delay_ms(10);
        return (0);
    } else {
        return (-1);
    }
}


/*********************************************************************//**
 * @brief   Read a number of data byte from EEPROM EEPROM
 * @param[in]   None
 * @return  0: if success, otherwise (-1) returned.
 **********************************************************************/
int32_t i2c_EEPROM_Read(uint32_t mem_addr, uint8_t* buf, uint32_t len){

	  len = (len < EEPROM_I2C_PAGE_SIZE)? len: EEPROM_I2C_PAGE_SIZE;
	  EEPROM_wrdat[0] = (mem_addr>>8) & 0x00FF;
	  EEPROM_wrdat[1] = mem_addr & 0x00FF;
    
	  rxsetup.sl_addr7bit = EEPROM_I2C_ADDR;
    rxsetup.tx_data = EEPROM_wrdat; // Get address to read at writing address
    rxsetup.tx_length = ADDRESS_SIZE;
    rxsetup.rx_data = buf;
    rxsetup.rx_length = len;
    rxsetup.retransmissions_max = 3;

    if (I2C_MasterTransferData((en_I2C_unitId)I2CDEV, &rxsetup, I2C_TRANSFER_POLLING) == SUCCESS){
        return (0);
    } else {
        return (-1);
    }
}
//scrive un byte alla locazione mem_addr
void i2c_EEPROM_Write_BYTE (uint32_t mem_addr, uint8_t dato){
	
	EEPROM_wrdat[2] = dato;

	i2c_EEPROM_Write(mem_addr, EEPROM_wrdat, 3);
}

//scrive un intero due byte dalla locazione mem_addr
void i2c_EEPROM_Write_INTEGER (uint32_t mem_addr, uint16_t dato){

	EEPROM_wrdat[2] = (dato>>8) & 0x00FF;
  EEPROM_wrdat[3] = dato & 0x00FF;

	i2c_EEPROM_Write(mem_addr, EEPROM_wrdat, 4);
}

//scrive un intero long 4 byte dalla locazione mem_addr
void i2c_EEPROM_Write_LONG_INTEGER (uint32_t mem_addr, uint32_t dato){

	EEPROM_wrdat[2] = (dato>>24) & 0x000000FF;
  EEPROM_wrdat[3] = (dato>>16) & 0x000000FF;
	EEPROM_wrdat[4] = (dato>>8) & 0x000000FF;
  EEPROM_wrdat[5] = dato & 0x000000FF;

	i2c_EEPROM_Write(mem_addr, EEPROM_wrdat, 6);
}

//legge un byte
uint8_t i2c_EEPROM_Read_BYTE (uint32_t mem_addr){
	uint8_t eeprom_byte;
  
	i2c_EEPROM_Read (mem_addr, EEPROM_rddat, 1);
	eeprom_byte = EEPROM_rddat[0];
	return eeprom_byte;
}

//legge un intero = due byte
uint16_t i2c_EEPROM_Read_INTEGER (uint32_t mem_addr){
	uint16_t eeprom_integer;
  
	i2c_EEPROM_Read (mem_addr, EEPROM_rddat, 2);
	eeprom_integer = EEPROM_rddat[0];
	eeprom_integer = (eeprom_integer<<8) & 0xFF00;
	eeprom_integer += EEPROM_rddat[1];
	return eeprom_integer;
}

//legge un intero long = 4 byte
uint32_t i2c_EEPROM_Read_LONG_INTEGER (uint32_t mem_addr){
	uint32_t eeprom_long_integer;
  
	i2c_EEPROM_Read (mem_addr, EEPROM_rddat, 4);
	eeprom_long_integer = (uint32_t)(EEPROM_rddat[0])<<24 & 0xFF000000;
	eeprom_long_integer = eeprom_long_integer | ((uint32_t)(EEPROM_rddat[1])<<16 & 0x00FF0000);
	eeprom_long_integer = eeprom_long_integer | ((uint32_t)(EEPROM_rddat[2])<<8 & 0x0000FF00);
	eeprom_long_integer = eeprom_long_integer | ((uint32_t)(EEPROM_rddat[3]) & 0x000000FF);
	return eeprom_long_integer;
}

/*********************************************************************//**
* MySetPin
* @param portNum	#della porta 0-5
* @param PinNum		numero [dec] del pin da pilotare 0-31
* @param value		valore del pin pilotato 0-1
 **********************************************************************/
void MySetPin(int PortNum, int PinNum, int value){
	if (value==0){
		//GPIO_ClearValue(portNum,GPIO_ReadValue(portNum)&~(1<<PinNum));
		GPIO_ClearValue(PortNum,(1<<PinNum));
    } else {
    GPIO_SetValue(PortNum,GPIO_ReadValue(PortNum)|(1<<PinNum));
    }
}



/*********************************************************************//**
 * @brief       pwm_config: Main PWM program body
 * @param[in]   None
 * @return      None
 **********************************************************************/
void pwm_config(void)
{
    uint8_t pwmChannel, channelVal;
    PWM_TIMERCFG_Type PWMCfgDat;
    PWM_MATCHCFG_Type PWMMatchCfgDat;

    /* PWM block section -------------------------------------------- */
    /* Initialize PWM peripheral, timer mode
     * PWM prescale value = 1 (absolute value - tick value) */
    PWMCfgDat.PrescaleOption = PWM_TIMER_PRESCALE_TICKVAL;
    PWMCfgDat.PrescaleValue = 1;
    PWM_Init(_USING_PWM_NO, PWM_MODE_TIMER, (void *) &PWMCfgDat);

    // Initialize PWM pin connect
    PINSEL_ConfigPin (1, 18, 2);
//    return 0;



    /* Set match value for PWM match channel 0 = 256, update immediately */
    PWM_MatchUpdate(_USING_PWM_NO, 0, 256, PWM_MATCH_UPDATE_NOW);

    /* PWM Timer/Counter will be reset when channel 0 matching
     * Enable interrupt when match
     * no stop when match */
    PWMMatchCfgDat.IntOnMatch = ENABLE;
    PWMMatchCfgDat.MatchChannel = 0;
    PWMMatchCfgDat.ResetOnMatch = ENABLE;
    PWMMatchCfgDat.StopOnMatch = DISABLE;
    PWM_ConfigMatch(_USING_PWM_NO, &PWMMatchCfgDat);

    /* Configure each PWM channel: --------------------------------------------- */
    /* - Single edge
     * - PWM Duty on each PWM channel determined by
     * the match on channel 0 to the match of that match channel.
     * Example: PWM Duty on PWM channel 1 determined by
     * the match on channel 0 to the match of match channel 1.
     */

    /* Configure PWM channel edge option
     * Note: PWM Channel 1 is in single mode as default state and
     * can not be changed to double edge mode */
//    for (pwmChannel = 2; pwmChannel < 7; pwmChannel++)
//    {
//        PWM_ChannelConfig(_USING_PWM_NO, pwmChannel, PWM_CHANNEL_SINGLE_EDGE);
//    }

    /* Setting interrupt for PWM ---------------------------------------------- */
    /* Disable PWM interrupt */
#if (_USING_PWM_NO == 1)
    NVIC_DisableIRQ(PWM1_IRQn);

    /* preemption = 1, sub-priority = 1 */
    NVIC_SetPriority(PWM1_IRQn, ((0x01<<3)|0x01));
#elif (_USING_PWM_NO == 0)
    NVIC_DisableIRQ(PWM0_IRQn);

    /* preemption = 1, sub-priority = 1 */
    NVIC_SetPriority(PWM0_IRQn, ((0x01<<3)|0x01));
#endif

    /* Configure match value for each match channel */
    channelVal = 10;
//    for (pwmChannel = 1; pwmChannel < 2; pwmChannel++)
pwmChannel = 1;
    {
        /* Set up match value */
        PWM_MatchUpdate(_USING_PWM_NO, pwmChannel, channelVal, PWM_MATCH_UPDATE_NOW);

        /* Configure match option */
        PWMMatchCfgDat.IntOnMatch = DISABLE;
        PWMMatchCfgDat.MatchChannel = pwmChannel;
        PWMMatchCfgDat.ResetOnMatch = DISABLE;
        PWMMatchCfgDat.StopOnMatch = DISABLE;
        PWM_ConfigMatch(_USING_PWM_NO, &PWMMatchCfgDat);

        /* Enable PWM Channel Output */
        PWM_ChannelCmd(_USING_PWM_NO, pwmChannel, ENABLE);

        /* Increase match value by 10 */
        channelVal += 30;
    }

    /* Enable PWM interrupt */
#if (_USING_PWM_NO == 1)
    NVIC_EnableIRQ(PWM1_IRQn);
#elif (_USING_PWM_NO == 0)
    NVIC_EnableIRQ(PWM0_IRQn);
#endif

    /* Reset and Start counter */
    PWM_ResetCounter(_USING_PWM_NO);

    PWM_CounterCmd(_USING_PWM_NO, ENABLE);

    /* Start PWM now */
    PWM_Cmd(_USING_PWM_NO, ENABLE);

}


// attende un tempo pari a circa "msec" milli secondi
void wait_msec(float msec){
	TIM1.msec_timer=msec;
	while(TIM1.msec_timer>0);
};


///***********************************************************************
// * @brief		set_LED_output 
// *          imposta l'uscita per i LED
// *          
// **********************************************************************/
void set_LED_output (void){

  MySetPin(Port_1, SSP2_CS, 1); //CS abilitato
	SSP_SendData(SSP2_PORT, LED_output) ;

  MySetPin(Port_5, uCS2_4, 0); //N.B. prima si scrive su SSP e poi si abilita il chip
  MySetPin(Port_1, SSP2_CS, 0); //CS disabilitato
  wait_msec(5);
  MySetPin(Port_5, uCS2_4, 1); //CS disabilitato per non sporcare lo stato
}



unsigned int input_analog_1_cnt = 0 ;
unsigned int input_analog_2_cnt = 0 ;
unsigned int input_analog_3_cnt = 0 ;
unsigned int input_analog_4_cnt = 0 ;
unsigned int input_analog_5_cnt = 0 ;
unsigned int input_analog_6_cnt = 0 ;
unsigned int input_analog_7_cnt = 0 ;
unsigned int input_analog_8_cnt = 0 ;
unsigned int input_analog_9_cnt = 0 ;

//--------------------------------------------------------------------
void input_analog_1_acquisizione(void)
{
		
uint16_t	integer_3 ;
uint16_t	integer_4 ;
float 	float_2 ;
float 	float_8 ;	
	
unsigned int adc_value_istantaneo, adc_value_ext_local_temp;
float risultato;
	
	// inserito il richiamo alla funzione default pech� l'ADC altrimenti non � operativo da subito
	input_analog_1_cnt = (input_analog_1_cnt)?  --input_analog_1_cnt: 0;
	if ( input_analog_1_cnt == 0)
		{
		input_analog_1_cnt = 100 ;
		ADC_default(INPUT_1_ADC) ;//1
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
		
	ADC_conf_reg_ch(INPUT_1_ADC,INPUT_1_CHANNEL,INPUT_1_MODE) ;
	TIM3.msec_timer	= 1;//20 ;
			while ( TIM3.msec_timer != 0) ;				
											
	ADC_set_mode(INPUT_1_ADC) ;	//1
	TIM3.msec_timer	= 3;//32 ;
			while ( TIM3.msec_timer != 0) ;		
					
	ADC_read(INPUT_1_ADC, INPUT_1_CHANNEL, INPUT_1_MODE)	;
								
	adc_value_ext_local_temp = ADC_value_read_med(INPUT_1_ADC,INPUT_1_CHANNEL) ;
				
  risultato =	adc_value_ext_local_temp;

  //1.flussimetro N2 mandata 720 m3/h
//  if ( (risultato>12500) && (risultato<63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
  {
    risultato = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT1_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_1_ZEROADC]) * (float) AnalogInput[1].Span)/
                 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_1_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_1_ZEROADC]);
  }
//  else
//    risultato = SENSOR_FAULT;
  Modbus.Array[MBVECTOR_Q_AIR] = (int)(risultato);
  Modbus.Array[MBVECTOR_Q_AIR] = (Modbus.Array[MBVECTOR_Q_AIR]>0)? Modbus.Array[MBVECTOR_Q_AIR]: 0;

//	GuiVar_integer_1 = Modbus.Array[MBVECTOR_Q_AIR];	
	
	GuiVar_float_1 = risultato;
	
	TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;

	ADC_default(INPUT_1_ADC) ; 
}
	

/*
void input_analog_2_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
	
unsigned int adc_value_ext_local_temp;	
float risultato;
	
//	input_analog_2_cnt = input_analog_2_cnt - 1 ;
	input_analog_2_cnt = (input_analog_2_cnt)?  --input_analog_2_cnt: 0; //2018.02.12
	if ( input_analog_2_cnt == 0)
		{
		input_analog_2_cnt = 100 ;
		ADC_default(INPUT_2_ADC) ; 
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_2_ADC,INPUT_2_CHANNEL,INPUT_2_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_2_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_2_ADC, INPUT_2_CHANNEL, INPUT_2_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_2_ADC,INPUT_2_CHANNEL) ;

  risultato = adc_value_ext_local_temp;

	risultato = (int)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC]) * (float) AnalogInput[7].Span)/
							 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC]);
  
	GuiVar_float_7 = risultato;
  Modbus.Array[MBVECTOR_T2] = (int)(risultato*10);
		
	TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;
	
  ADC_default(INPUT_2_ADC) ; 

}		
*/
void input_analog_2_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
double tmpAvg;
unsigned char i, max_index;
unsigned int adc_value_ext_local_temp;	
float risultato;
	
	adc_value_ext_local_temp = 0;
	
//	input_analog_2_cnt = (input_analog_2_cnt)?  --input_analog_2_cnt: 0; //2018.02.12
//	if ( input_analog_2_cnt == 0)
//		{
//		input_analog_2_cnt = 100 ;
//		ADC_default(INPUT_2_ADC) ; 
//		}	
//	
//	TIM3.msec_timer	= 1;//10 ;
//			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_2_ADC,INPUT_2_CHANNEL,INPUT_2_MODE);	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_2_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_2_ADC, INPUT_2_CHANNEL, INPUT_2_MODE);		
	
	// Filtro di Antonio con esclusione valori troppo discostanti dal precedente
  if ( (ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL]<65500) && (ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL]>30000) ){
    adc_counter_PT100_2_fault = 0;
		//  if ( ((ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL]-ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL]) < 10) || ((ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL]-ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL]) < 10) )
		if (LOGIC_ABS(ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL]-ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL]) < 3000){//3000){
			adc_value_ext_local_temp = ADC_value_read_med(INPUT_2_ADC,INPUT_2_CHANNEL);
			ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL] = adc_value_ext_local_temp;
			adc_counter_PT100_2 = 0;
		}
		else{
			++adc_counter_PT100_2;
			adc_value_ext_local_temp = ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL];
		}
		
		if (adc_counter_PT100_2>10){//50
			adc_value_ext_local_temp = ADC_value_read_med(INPUT_2_ADC,INPUT_2_CHANNEL);
			ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL] = adc_value_ext_local_temp;
			adc_counter_PT100_2 = 0;
		}
	  if ( (Adox1500.PT100_2_AvgIndex==1) && (Adox1500.PT100_2_AvgBufferFull==0) ){
		  ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL] = ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL];
		  adc_value_ext_local_temp = ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL];
		}
  }
	else{
		adc_value_ext_local_temp = ADC_preview_value[INPUT_2_ADC][INPUT_2_CHANNEL];
    adc_counter_PT100_2_fault = (adc_counter_PT100_2_fault<ADC_PT100_FAULT_MAX)? ++adc_counter_PT100_2_fault: ADC_PT100_FAULT_MAX;
		if ( (E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]) && (adc_counter_PT100_2_fault>=ADC_PT100_FAULT_MAX) )
			adc_value_ext_local_temp = 65535;
	}
	
		GuiVar_integer_1 = (GuiConst_INT16U)ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL];
		
//	adc_value_ext_local_temp = ADC_value_read_med(INPUT_2_ADC,INPUT_2_CHANNEL);

  risultato = adc_value_ext_local_temp;

	risultato = (int)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC]) * (float) AnalogInput[7].Span)/
							 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC]);

//////////////////////////////////
  
  tmpAvg = 0;
  if ( (risultato>-10) && (risultato < 80) )//se non c'� errore sonda
  {
    Adox1500.PT100_2_Avg[Adox1500.PT100_2_AvgIndex] = risultato;
    if (Adox1500.PT100_2_AvgBufferFull==0)
      max_index = Adox1500.PT100_2_AvgIndex;
    else
      max_index = ADC_PT100_CAMPIONI;
      
    for (i=1; i<max_index+1; i++)
      tmpAvg += Adox1500.PT100_2_Avg[i];
    Adox1500.PT100_2_Avg[0] = tmpAvg/max_index;
    if (Adox1500.PT100_2_AvgIndex<ADC_PT100_CAMPIONI)
      ++Adox1500.PT100_2_AvgIndex;
    else
    {
      Adox1500.PT100_2_AvgIndex = 1;
      Adox1500.PT100_2_AvgBufferFull = 1;
    }
  }
	GuiVar_integer_3 = Adox1500.PT100_2_AvgIndex;		

//  else
//  {
//    Adox1500.PT100_1_AvgIndex = 1;
//    Adox1500.PT100_1_AvgBufferFull = 0;
//    Adox1500.PT100_1_Avg[0] = risultato;
//  }
	
//////////////////////////////////	
 
	GuiVar_float_7 = Adox1500.PT100_2_Avg[0];
  Modbus.Array[MBVECTOR_T2] = (int)(GuiVar_float_7*10);
		
	TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;
	
  ADC_default(INPUT_2_ADC) ; 

}		




void input_analog_3_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
	
unsigned int adc_value_ext_local_temp;	
float risultato;

//	input_analog_2_cnt = input_analog_2_cnt - 1 ;
	input_analog_3_cnt = (input_analog_3_cnt)?  --input_analog_3_cnt: 0;
	if ( input_analog_3_cnt == 0)
		{
		input_analog_3_cnt = 100 ;
		ADC_default(INPUT_3_ADC) ; 
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_3_ADC,INPUT_3_CHANNEL,INPUT_3_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_3_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_3_ADC, INPUT_3_CHANNEL, INPUT_3_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_3_ADC,INPUT_3_CHANNEL) ;

  risultato =	adc_value_ext_local_temp;

	Modbus.Array[MBVECTOR_ALLARMI_2] &= ~MB_ALLARMI_2_B1;
  if ( (Adox1500.TmrStartUp==0) && (Adox1500.TmrErrB1==0) ){		
		if ( (risultato<12000) || (risultato>63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
			Modbus.Array[MBVECTOR_ALLARMI_2] |= MB_ALLARMI_2_B1;
	}

  //3.pressostato P1
  if ( (risultato>12400) && (risultato<63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
  {
    risultato = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT3_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_3_ZEROADC]) * (float) AnalogInput[3].Span)/
                 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_3_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_3_ZEROADC]);
    Adox1500.TmrErrB1 = 30;	
  }
//  else
//    risultato = SENSOR_FAULT;
	
	
  Modbus.Array[MBVECTOR_P1] = (int)(risultato);
	
//	GuiVar_integer_3 = Modbus.Array[MBVECTOR_P1];	
		
	GuiVar_float_3 = risultato/100;

	TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;
}		
	

void input_analog_4_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
	
unsigned int adc_value_ext_local_temp;	
float risultato;
	
	input_analog_4_cnt = (input_analog_4_cnt)?  --input_analog_4_cnt: 0;
	if ( input_analog_4_cnt == 0)
		{
		input_analog_4_cnt = 100 ;
		ADC_default(INPUT_4_ADC) ; 
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_4_ADC,INPUT_4_CHANNEL,INPUT_4_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_4_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_4_ADC, INPUT_4_CHANNEL, INPUT_4_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_4_ADC,INPUT_4_CHANNEL) ;
		
  risultato =	adc_value_ext_local_temp;

	Modbus.Array[MBVECTOR_ALLARMI_2] &= ~MB_ALLARMI_2_B2;
  if ( (Adox1500.TmrStartUp==0) && (Adox1500.TmrErrB2==0) ){		
		if ( (risultato<12000) || (risultato>63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
			Modbus.Array[MBVECTOR_ALLARMI_2] |= MB_ALLARMI_2_B2;
	}
  //4.pressostato P2
  if ( (risultato>12400) && (risultato<63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
  {
    risultato = (int16_t)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT4_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_ZEROADC]) * (float) AnalogInput[4].Span)/
                 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_ZEROADC]);
    Adox1500.TmrErrB2 = 30;		
//		//ATTENZIONE: c'� un errore nel salvataggio o nel caricamento dalla EEPROM di valori negativi
//    risultato = -100 + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_ZEROADC]) * (float) 200)/
//                 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_4_ZEROADC]);
  }
//  else
//    risultato = SENSOR_FAULT;

  Modbus.Array[MBVECTOR_P2] = (int)(risultato);
//	if ( (Modbus.Array[MBVECTOR_P2]>=-100) && (Modbus.Array[MBVECTOR_P2]<=100) )
//    Adox.TmrB2_Err = (uchar)E2prom[E2_PAGE_3].E2_Int[E2_3_TMR_B2_ERROR]; //ricarica ritardo errore sonda
		
//	GuiVar_integer_4 = Modbus.Array[MBVECTOR_P2];
	GuiVar_float_4 = risultato/100;

	TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;
}		

void input_analog_5_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
float risultato;
	
unsigned int adc_value_ext_local_temp;	
	
	input_analog_5_cnt = (input_analog_5_cnt)?  --input_analog_5_cnt: 0;
	if ( input_analog_5_cnt == 0)
		{
		input_analog_5_cnt = 100 ;
		ADC_default(INPUT_5_ADC) ; 
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_5_ADC,INPUT_5_CHANNEL,INPUT_5_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_5_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_5_ADC, INPUT_5_CHANNEL, INPUT_5_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_5_ADC,INPUT_5_CHANNEL) ;

  risultato = adc_value_ext_local_temp;

//GuiVar_integer_5 = adc_value_ext_local_temp;
		
	Modbus.Array[MBVECTOR_ALLARMI_2] &= ~MB_ALLARMI_2_B3;
  if ( (Adox1500.TmrStartUp==0) && (Adox1500.TmrErrB3==0) ){		
		if ( (risultato<12000) || (risultato>63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
			Modbus.Array[MBVECTOR_ALLARMI_2] |= MB_ALLARMI_2_B3;
  }
		//5.pressostato P4
  if ( (risultato>12400) && (risultato<63000) )  //vedere quali sono i range limite dell'ADC corrispondenti a 4 e 20 mA e segnalare errore
  {
    risultato = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT5_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_5_ZEROADC]) * (float) AnalogInput[5].Span)/
                 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_5_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_5_ZEROADC]);
    Adox1500.TmrErrB3 = 30;		
  }
//  else
//    risultato = SENSOR_FAULT;

  Modbus.Array[MBVECTOR_P3] = (int)(risultato);
//	if ( (Modbus.Array[MBVECTOR_P2]>=-100) && (Modbus.Array[MBVECTOR_P2]<=100) )
//    Adox.TmrB2_Err = (uchar)E2prom[E2_PAGE_3].E2_Int[E2_3_TMR_B2_ERROR]; //ricarica ritardo errore sonda
		
	GuiVar_integer_5 = Modbus.Array[MBVECTOR_P3];
	GuiVar_float_5 = risultato/100;

		TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;
}		


void input_analog_6_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
float risultato;
	
unsigned int adc_value_ext_local_temp;	
	
	input_analog_6_cnt = (input_analog_6_cnt)?  --input_analog_6_cnt: 0;
	if ( input_analog_6_cnt == 0)
		{
		input_analog_6_cnt = 100 ;
		ADC_default(INPUT_6_ADC) ; 
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_6_ADC,INPUT_6_CHANNEL,INPUT_6_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_6_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_6_ADC, INPUT_6_CHANNEL, INPUT_6_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_6_ADC,INPUT_6_CHANNEL) ;
		
//	GuiVar_ADC_3_CH_2 = adc_value_ext_local_temp;	

		TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;
}		



void input_analog_7_acquisizione(void)
{
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
float risultato;
	
unsigned int adc_value_ext_local_temp;	
	
	input_analog_7_cnt = (input_analog_7_cnt)?  --input_analog_7_cnt: 0;
	if ( input_analog_7_cnt == 0)
		{
		input_analog_7_cnt = 100 ;
		ADC_default(INPUT_7_ADC) ; 
		}	
	
	TIM3.msec_timer	= 1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	
	ADC_conf_reg_ch(INPUT_7_ADC,INPUT_7_CHANNEL,INPUT_7_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_7_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_7_ADC, INPUT_7_CHANNEL, INPUT_7_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_7_ADC,INPUT_7_CHANNEL) ;

//  risultato = adc_value_ext_local_temp;

//	risultato = (int)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT7_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC]) * (float) AnalogInput[7].Span)/
//							 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_7_ZEROADC]);
//  
//	GuiVar_float_7 = risultato;
//  Modbus.Array[MBVECTOR_T2] = (int)(risultato*10);
	
  TIM3.msec_timer	= 1 ;
		while ( TIM3.msec_timer != 0) ;
		
	ADC_default(INPUT_7_ADC) ; 
}

/*
void input_analog_8_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
float risultato;
	
unsigned int adc_value_ext_local_temp;	
	
	input_analog_8_cnt = (input_analog_8_cnt)?  --input_analog_8_cnt: 0;
	if ( input_analog_8_cnt == 0){
		input_analog_8_cnt = 100 ;
		ADC_default(INPUT_8_ADC) ; 
  	TIM3.msec_timer	= 5;//1;//10 ;
			while ( TIM3.msec_timer != 0) ;	
	}	
	
	
	ADC_conf_reg_ch(INPUT_8_ADC,INPUT_8_CHANNEL,INPUT_8_MODE) ;	
	TIM3.msec_timer	= 20;//1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_8_ADC) ;	
	TIM3.msec_timer	= 20;//3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_8_ADC, INPUT_8_CHANNEL, INPUT_8_MODE)	;		

	adc_value_ext_local_temp = ADC_value_read_med(INPUT_8_ADC,INPUT_8_CHANNEL) ;
		
		GuiVar_integer_8 = adc_value_ext_local_temp;		
////		GuiVar_float_7 = (float)(adc_value_ext_local_temp)*37.82/(65390-46330); //risultato in ohm
//		GuiVar_float_8 = (float)(adc_value_ext_local_temp-46330)*(75+20)/(65390-46330)-20; //risultato in �C
//    Modbus.Array[MBVECTOR_T1] = (int)(GuiVar_float_8*10);
////		GuiVar_integer_8 = Modbus.Array[MBVECTOR_T1];		

  risultato = adc_value_ext_local_temp;

	risultato = (int)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_ZEROADC]) * (float) AnalogInput[8].Span)/
							 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_ZEROADC]);
  GuiVar_float_8 = risultato;
  Modbus.Array[MBVECTOR_T1] = (int)(risultato*10);

		TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;

	ADC_default(INPUT_8_ADC) ; 
}		
*/


void input_analog_8_acquisizione(void)
{	
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
float risultato;
double tmpAvg;
unsigned char i, max_index;
unsigned int adc_value_ext_local_temp;	
	
//	input_analog_8_cnt = (input_analog_8_cnt)?  --input_analog_8_cnt: 0;
//	if ( input_analog_8_cnt == 0){
//		input_analog_8_cnt = 100 ;
//		ADC_default(INPUT_8_ADC) ; 
//  	TIM3.msec_timer	= 5;//1;//10 ;
//			while ( TIM3.msec_timer != 0) ;	
//	}	
	
	adc_value_ext_local_temp = 0;
	
	ADC_conf_reg_ch(INPUT_8_ADC,INPUT_8_CHANNEL,INPUT_8_MODE) ;	
	TIM3.msec_timer	= 1;//20		 ;
	  while ( TIM3.msec_timer != 0) ;																			
						
	ADC_set_mode(INPUT_8_ADC) ;	
	TIM3.msec_timer	= 3;//32 ;
	  while ( TIM3.msec_timer != 0) ;
					
	ADC_read(INPUT_8_ADC, INPUT_8_CHANNEL, INPUT_8_MODE)	;		

// Filtro di Antonio con esclusione valori troppo discostanti dal precedente
  if ( (ADC_value[INPUT_8_ADC][INPUT_8_CHANNEL]<65500) && (ADC_value[INPUT_8_ADC][INPUT_8_CHANNEL]>30000) ){
		adc_counter_PT100_1_fault = 0;
		if (LOGIC_ABS(ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL]-ADC_value[INPUT_8_ADC][INPUT_8_CHANNEL]) < 3000){
			adc_value_ext_local_temp = ADC_value_read_med(INPUT_8_ADC,INPUT_8_CHANNEL);
			ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL] = adc_value_ext_local_temp;
			adc_counter_PT100_1 = 0;
		}
		else{
			++adc_counter_PT100_1;
			adc_value_ext_local_temp = ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL];
		}
		
		if (adc_counter_PT100_1>10){
			adc_value_ext_local_temp = ADC_value_read_med(INPUT_8_ADC,INPUT_8_CHANNEL);
			ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL] = adc_value_ext_local_temp;
			adc_counter_PT100_1 = 0;
		}	
	  if ( (Adox1500.PT100_1_AvgIndex==1) && (Adox1500.PT100_1_AvgBufferFull==0) ){
		  ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL] = ADC_value[INPUT_8_ADC][INPUT_8_CHANNEL];
		  adc_value_ext_local_temp = ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL];
		}
	}
	else{
		adc_value_ext_local_temp = ADC_preview_value[INPUT_8_ADC][INPUT_8_CHANNEL];
    adc_counter_PT100_1_fault = (adc_counter_PT100_1_fault<ADC_PT100_FAULT_MAX)? ++adc_counter_PT100_1_fault: ADC_PT100_FAULT_MAX;
		if ( (E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]) && (adc_counter_PT100_1_fault>=ADC_PT100_FAULT_MAX) )
			adc_value_ext_local_temp = 65535;
	}
		
//	adc_value_ext_local_temp = ADC_value_read_med(INPUT_8_ADC,INPUT_8_CHANNEL) ;
		
	GuiVar_integer_2 = (GuiConst_INT16U)ADC_value[INPUT_8_ADC][INPUT_8_CHANNEL];		

  risultato = adc_value_ext_local_temp;

	risultato = (int)E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT8_IS] + ((risultato - (float) E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_ZEROADC]) * (float) AnalogInput[8].Span)/
							 (float) (E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_SPANADC] - E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_8_ZEROADC]);
  
//////////////////////////////////
  
  tmpAvg = 0;
  
  if ( (risultato>-10) && (risultato < 80) )//se non c'� errore sonda
  {
    Adox1500.PT100_1_Avg[Adox1500.PT100_1_AvgIndex] = risultato;
    if (Adox1500.PT100_1_AvgBufferFull==0)
      max_index = Adox1500.PT100_1_AvgIndex;
    else
      max_index = ADC_PT100_CAMPIONI;
      
    for (i=1; i<max_index+1; i++)
      tmpAvg += Adox1500.PT100_1_Avg[i];
    Adox1500.PT100_1_Avg[0] = tmpAvg/max_index;
    if (Adox1500.PT100_1_AvgIndex<ADC_PT100_CAMPIONI)
      ++Adox1500.PT100_1_AvgIndex;
    else
    {
      Adox1500.PT100_1_AvgIndex = 1;
      Adox1500.PT100_1_AvgBufferFull = 1;
    }
  }
	GuiVar_integer_4 = Adox1500.PT100_1_AvgIndex;		

//  else
//  {
//    Adox1500.PT100_1_AvgIndex = 1;
//    Adox1500.PT100_1_AvgBufferFull = 0;
//    Adox1500.PT100_1_Avg[0] = risultato;
//  }
	
//////////////////////////////////	
	GuiVar_float_8 = Adox1500.PT100_1_Avg[0];
	
	
  Modbus.Array[MBVECTOR_T1] = (int)(GuiVar_float_8*10);

		TIM3.msec_timer	= 1 ;
			while ( TIM3.msec_timer != 0) ;

	ADC_default(INPUT_8_ADC) ; 
}		



// 20201005 MODIFICATO per gestione tempi lunghi di acquisizione

//void input_analog_9_acquisizione(void)
//{
//uint16_t	integer_1 ;
//uint16_t	integer_2 ;
//float 	float_1 ;
//float 	float_7 ;		
//	
//unsigned int adc_value_ext_local_temp;	
//
//	input_analog_9_cnt = (input_analog_9_cnt)?  --input_analog_9_cnt: 0;
//	if ( input_analog_9_cnt == 0)
//		{
//		input_analog_9_cnt = 100 ;
//		ADC_default_ADC5() ; 
//	
//		TIM3.msec_timer	= 10;//10 OK
//				while ( TIM3.msec_timer != 0) ;						
//		}	

//	ADC_conf_reg_ch_ADC5(INPUT_9_CHANNEL,INPUT_9_MODE);	
//		
//	TIM3.msec_timer	= 800;//500 OK; //sembra il tempo pi� importante
//	  while ( TIM3.msec_timer != 0) ;																			
//						
//	ADC_set_mode_ADC5();	
//		
//	TIM3.msec_timer	= 3;//500 OK; // sembra basti molto meno
//	  while ( TIM3.msec_timer != 0) ;
//					
//	ADC_read_ADC5(INPUT_9_CHANNEL, INPUT_9_MODE);		

//	TIM3.msec_timer	= 5;//500 OK; // sembra non serva a nulla
//	  while ( TIM3.msec_timer != 0);
//		
//	adc_value_ext_local_temp = ADC_value_read_ADC5_med(INPUT_9_CHANNEL);
//		
//	GuiVar_ADC_O2 = adc_value_ext_local_temp;
////	GuiVar_integer_6 = adc_value_ext_local_temp;

//	GuiVar_float_9 = (float)(adc_value_ext_local_temp-E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC])*((float)(AnalogInput[9].Span)/100)/(E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_SPANADC]-E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC]); //risultato in %
////  GuiVar_float_9 = (float)(adc_value_ext_local_temp-0)*(20.93)/(42500-0); //risultato in %
//	
// 	O2in = (unsigned int)(GuiVar_float_9*10);
//	
//	GuiVar_new_value_O2 = GuiVar_float_9;
//	
//		TIM3.msec_timer	= 1;
//			while ( TIM3.msec_timer != 0);
//}

// 20201005 MODIFICATO per gestione tempi lunghi di acquisizione
void input_analog_9_acquisizione(void)
{
uint16_t	integer_1 ;
uint16_t	integer_2 ;
float 	float_1 ;
float 	float_7 ;		
	
unsigned int adc_value_ext_local_temp;	
  
// sts_sample = Stato di campionamento:
//  0 = IDLE
//  1 = WAIT_AND_READ
  
  switch (sts_sample)
  {
    case 0:   // IDLE
      input_analog_9_cnt = (input_analog_9_cnt)?  --input_analog_9_cnt: 0;
      if ( input_analog_9_cnt == 0)
        {
        input_analog_9_cnt = 100 ;
        ADC_default_ADC5() ; 
      
        TIM3.msec_timer	= 10;//10 OK
            while ( TIM3.msec_timer != 0) ;						
        }	

      ADC_conf_reg_ch_ADC5(INPUT_9_CHANNEL,INPUT_9_MODE);	
      TIM3.msec_timer	= 3;//
        while ( TIM3.msec_timer != 0) ;
        
      tmr_ready_ADC5	= 800;//500 OK; //sembra il tempo pi� importante
      sts_sample++;
      break;
  
    case 1:   // WAIT_AND_READ
      if(!tmr_ready_ADC5)
      {
        sts_sample = 0;
        
        ADC_set_mode_ADC5();	
          
        TIM3.msec_timer	= 3;//500 OK; // sembra basti molto meno
          while ( TIM3.msec_timer != 0) ;
                
        ADC_read_ADC5(INPUT_9_CHANNEL, INPUT_9_MODE);		

        TIM3.msec_timer	= 5;//500 OK; // sembra non serva a nulla
          while ( TIM3.msec_timer != 0);
          
        adc_value_ext_local_temp = ADC_value_read_ADC5_med(INPUT_9_CHANNEL);
          
        GuiVar_ADC_O2 = adc_value_ext_local_temp;
      //	GuiVar_integer_6 = adc_value_ext_local_temp;

        GuiVar_float_9 = (float)(adc_value_ext_local_temp-E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC])*((float)(AnalogInput[9].Span)/100)/(E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_SPANADC]-E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC]); //risultato in %
      //  GuiVar_float_9 = (float)(adc_value_ext_local_temp-0)*(20.93)/(42500-0); //risultato in %
        
        O2in = (unsigned int)(GuiVar_float_9*10);
        
        GuiVar_new_value_O2 = GuiVar_float_9;
        
          TIM3.msec_timer	= 1;
            while ( TIM3.msec_timer != 0);
      }
      break;
      
    }

}


/***********************************************************************
 * @brief       Timer_1_second
 * @param[in]   None
 * @return      None
 **********************************************************************/
void Timer_1_second(void)
{
	unsigned int i = 0;
// 20201005  
//	if (tmrSecondo==0){
//    tmrSecondo = TMR_1_SECOND;
//    
  if (tmrSecondo!=RTCFullTime.SEC){
		tmrSecondo = RTCFullTime.SEC;
    
#ifndef DEBUG_MODE
    DigIn.Status = get_DigInput();
#else
DigIn.Status = (DIG_IN_1 + DIG_IN_2 + DIG_IN_3 + DIG_IN_4 + DIG_IN_7 + DIG_IN_8); //eliminare simulo gli ingressi corretti per lavorare
#endif
    
		Adox1500.TmrStartUp = (Adox1500.TmrStartUp)? --Adox1500.TmrStartUp: 0;

//    set_LED_output();
		O2_Media();
		FlowAverage();

    CheckIngressi();
		tmrPT100 = (tmrPT100)? --tmrPT100: 0;
    if (tmrPT100==0)
  		CheckTemp();

    CheckPress();
		
		if (Modbus.Array[MBVECTOR_AL_RESET])
		{
			if (Adox1500.Stato & ADOX_OFF)
			{
				InitAdox();//resetta gli allarmi
			}
			Modbus.Array[MBVECTOR_AL_RESET] = 0;
		}

    Rele_Work();
    Adox1500work();
    CAN_1_polling();
		
		if (Adox1500.Mode & ADOX_RUNNING){
			Adox1500.TmrErrB1 = (Adox1500.TmrErrB1)? --Adox1500.TmrErrB1: 0;
			Adox1500.TmrErrB2 = (Adox1500.TmrErrB2)? --Adox1500.TmrErrB2: 0;
			Adox1500.TmrErrB3 = (Adox1500.TmrErrB3)? --Adox1500.TmrErrB3: 0;
		}
		
		//ricostruisco ore,minuti e secondi funzionamento Adox
		GuiVar_ore_macchina = Adox1500.CounterSec/3600 + (Adox1500.Ore_Run * 12);                                       // ore
		GuiVar_minuti_macchina = (Adox1500.CounterSec - Adox1500.CounterSec/3600*3600)/60;                              // minuti
		GuiVar_secondi_macchina = ((Adox1500.CounterSec - Adox1500.CounterSec/3600*3600) - GuiVar_minuti_macchina*60);  // secondi

		
	}
}

/***********************************************************************
 * @brief       Timer_10_seconds
 * @param[in]   None
 * @return      None
 **********************************************************************/
void Timer_10_seconds(void)
{
	unsigned int i = 0;
	
	if (Timer_repeat==0){
		// Tempo per identificare lo stato "idle"
//		idle_state  = (idle_state)  ? --idle_state : 0;
			
		// Tempo per auto-disautenticarsi (2 min)
		auto_logout = (auto_logout) ? --auto_logout : 0;

		// Timeout per salavare i valori di O2 sull'SD
		print_o2_timeout = (print_o2_timeout) ? --print_o2_timeout : 0;					
			
		// Timeout per andare nella schrmata di riposo
		idle_screen_timeout = (idle_screen_timeout) ? --idle_screen_timeout : 0;				

		// Timer reinizializzazione CAN
    tmrCanReinit = (tmrCanReinit)? --tmrCanReinit : 0;

		Timer_repeat = TMR_10_SECONDS;
		
		
	}
}


// **********************************************************************************************************
/***********************************************************************
 * @brief       TIMER1 interrupt handler sub-routine
 * @param[in]   None
 * @return      None
 **********************************************************************/
void TIMER1_IRQHandler(void){
  if (TIM_GetIntStatus(LPC_TIM1, TIM_MR0_INT)== SET){//TIM_MR0_INT
	
		//press clock
		if (MCLKs){
			MCLK_i = (MCLK_i)? 0: 1;
		  if ( (MCLKs) && (MCLK_i) )
			  --MCLKs;

	    //GPIO_OutputValue(Port_4,(1<<PRESS_MCLK),MCLK_i);
		  MySetPin(Port_4, PRESS_MCLK, MCLK_i);
		}

		//Buzzer
		if (buzzer_enabled){
			if (buzzer_ON>buzzer_modulation){//150
				MySetPin(Port_1, 31, 1);
				buzzer_ON = (buzzer_ON)? --buzzer_ON : buzzer_modulation;//150
			}
			else{
				MySetPin(Port_1, 31, 0);
				if (buzzer_ON)
					--buzzer_ON;
				else{
//					if (buzzer_modulation==100){
						buzzer_modulation = 150;
					  buzzer_ON = 160;
//					}
//					else{
//						buzzer_modulation = 100;
//					  buzzer_ON = 130;
//					}
				}
			}
		}
		
		valore_Timer2 = (valore_Timer2)? --valore_Timer2: 0;
		
//		Ready2Send = (Ready2Send)? --Ready2Send: 0;
//		TmrRxNextByte = (TmrRxNextByte)? --TmrRxNextByte: 0;
		BadFrame = (BadFrame)? --BadFrame: 0;

		//se c'� un nuovo guasto
// 20201002 SPOSTATO       
//		if ( (new_fault) || (GuiVar_fault_back_color==0x07DF) ){
//			TIM11.msec_timer = (TIM11.msec_timer)?	--TIM11.msec_timer:	0;
//			if (TIM11.msec_timer==0){
//				TIM11.msec_timer = 500;
//				if (GuiVar_fault_back_color==0xFFFF)
//					GuiVar_fault_back_color = 0x07DF;   //2018.03.05 0x07DF - giallo
//				else
//					GuiVar_fault_back_color = 0xFFFF;
//			}
//		}

	}		//=== end msec
  TIM_ClearIntPending(LPC_TIM1, TIM_MR0_INT);	
}



/***********************************************************************
 * @brief       TIMER2 interrupt handler sub-routine
 * @param[in]   None
 * @return      None
 **********************************************************************/
void TIMER2_IRQHandler(void)
{
	
	unsigned int i = 0;
	
    if (TIM_GetIntStatus(LPC_TIM2, TIM_MR0_INT)== SET)
    {
			
		counter_Timer2 = (counter_Timer2<65535)? ++counter_Timer2: 0;

  	TIM2.msec_timer = (TIM2.msec_timer)?	--TIM2.msec_timer:	0; //millisecondo ???
  	
		TIM3.msec_timer = (TIM3.msec_timer)?	--TIM3.msec_timer:	0; //millisecondo ???

  	TIM4.msec_timer = (TIM4.msec_timer)?	--TIM4.msec_timer:	0; //millisecondo ???

  	TIM5.msec_timer = (TIM5.msec_timer)?	--TIM5.msec_timer:	0; //millisecondo ???

	  TIM9.msec_timer = (TIM9.msec_timer)?	--TIM9.msec_timer:	0;
			
	  TIM10.msec_timer = (TIM10.msec_timer)?	--TIM10.msec_timer:	0;

//  20201005  
    tmr_ready_ADC5 = (tmr_ready_ADC5)?	--tmr_ready_ADC5:	0;     
      
			// Timer 10 second1
      Timer_repeat = (Timer_repeat)? --Timer_repeat : 0;
      
			// Tempo di visualizzazione dei messaggi (es. User authenticated, ...)
      message_timer_count = (message_timer_count)? --message_timer_count : 0 ;
			
			// Tempo per il lampeggio del bordo esterno delle schermate (solo grafica)
			alarm_timer_count   = (alarm_timer_count)?   --alarm_timer_count   : 0 ;
						
			// Timeout per la ricezione di un byte, RS485 (20 ms)
			rs485_rx_timeout = (rs485_rx_timeout) ? --rs485_rx_timeout : 0;

			// Timeout per il lampeggio della password
			pwd_blink = (pwd_blink) ? --pwd_blink : 0;
			
			// Timer durata minima ciclo main
//      tmrCiclomain = (tmrCiclomain)? --tmrCiclomain : 0;
			
			// Timer secondo
//      tmrSecondo = (tmrSecondo)? --tmrSecondo : 0;
			
			// Timer alternanza luminosit� se lavoro a batteria
//      tmrLowBacklight = (tmrLowBacklight)? --tmrLowBacklight : 0;
			
	  	TIM1.msec_timer = (TIM1.msec_timer>0)?	--TIM1.msec_timer:	0; //millisecondo ???
      
// 20201005 
//		  valore_Timer3 = (valore_Timer3)? --valore_Timer3: 0;
      
// 20201005 SPOSTATO da timer 1      
			TIM11.msec_timer = (TIM11.msec_timer)?	--TIM11.msec_timer:	0;      
    }
    
    TIM_ClearIntPending(LPC_TIM2, TIM_MR0_INT);
}
/* Interrupt seervice routine */


/*-------------------------PRIVATE FUNCTIONS------------------------------*/




 /***********************************************************************
 * @brief		press_button	
	 \param [in] button
	 abilita (premuto) un bottone e disabilita gli altri
 **********************************************************************/
void press_button (unsigned int button) { 
		switch (button)
		{
			// DEFAULT
			case 0:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }
				index_system = 1;
			break;
			
			// SET
			case 1:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 1; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;
				
			// Bombola calibrazione
			case 2:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 1; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
			break;	

			// LOGIN / LOGOUT
			case 3:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 1; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// OPTIONS
			case 4:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 1; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// EVENTS
			case 5:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 1; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// INFO
			case 6:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 1; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// ALARMS
			case 7:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) { GuiVar_button_alarms = 1;}
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// FAULTS
			case 8:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 1; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// DISABLEMENTS
			case 9:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 1; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 0; }			
			break;		

			// DEVICES CONTROL
			case 10:
				if (GuiVar_button_set != 2) {	GuiVar_button_set = 0; }
				if (GuiVar_button_th != 2) {	GuiVar_button_th = 0; }
				if (GuiVar_button_log != 2) {	GuiVar_button_log = 0; }
				if (GuiVar_button_options != 2) {	GuiVar_button_options = 0; }
				if (GuiVar_button_events != 2) {	GuiVar_button_events = 0; }
				if (GuiVar_button_info != 2) {	GuiVar_button_info = 0; }
				if (GuiVar_button_alarms != 2) {	GuiVar_button_alarms = 0; }
				if (GuiVar_button_faults != 2) {	GuiVar_button_faults = 0; }
				if (GuiVar_button_disablements != 2) {	GuiVar_button_disablements = 0; }
				if (GuiVar_button_dev_ctrl != 2) {	GuiVar_button_dev_ctrl = 1; }			
			break;			
			
		}
	
}

 /***********************************************************************
 * @brief		handle_pwd_blink	
	 gestisce il lampeggio delle cifre dlla password
 **********************************************************************/
void handle_pwd_blink (void) { 
	
// 20201002 	
//	if ((current_user.level == 0 ) && (GuiVar_screen_select == LOG_SCREEN )) { 
	if (GuiVar_screen_select == LOG_SCREEN ) { 
		switch (pswd_index)
		{
			case 0:
				if (pwd_blink == 0) {
					if (GuiVar_pwd_char_1 == 0){
						GuiVar_pwd_char_1 = 2;
					} else {
						GuiVar_pwd_char_1 = 0;
					}
					pwd_blink = PWD_BLINK_TIME;
				}	
			break;
			
			case 1:
				if (pwd_blink == 0) {
					if (GuiVar_pwd_char_2 == 0){
						GuiVar_pwd_char_2 = 2;
					} else {
						GuiVar_pwd_char_2 = 0;
					}
					pwd_blink = PWD_BLINK_TIME;
				}					
			break;
			
			case 2:
				if (pwd_blink == 0) {
					if (GuiVar_pwd_char_3 == 0){
						GuiVar_pwd_char_3 = 2;
					} else {
						GuiVar_pwd_char_3 = 0;
					}
					pwd_blink = PWD_BLINK_TIME;
				}	
			break;
        
// 20201002
			case 3:
				if (pwd_blink == 0) {
					if (GuiVar_pwd_char_4 == 0){
						GuiVar_pwd_char_4 = 2;
					} else {
						GuiVar_pwd_char_4 = 0;
					}
					pwd_blink = PWD_BLINK_TIME;
				}	
			break;        
		}
	}	
}	

 /***********************************************************************
 * @brief		print_number			
	 \param [in] NUM_TYPE: 2_DIGIT; FLOAT
	 \param [in] start_index: punta alla prima casella libera dell'array
	 \param [in] address: puntatore alla stringa
	 \param [in] number: numero da mettere nella stringa (intero)
	 \param [in] number: numero da mettere nella stringa (float)
	 aggiunge alla stringa il numero indicato
 **********************************************************************/
unsigned char print_number (unsigned int type, unsigned int first, char * address, unsigned int int_data, float float_data) {
		
		unsigned int int_store = int_data;
	  unsigned char length = 0;
	
		switch (type)
		{
			case NUM_2_DIGITS:
				int_store = int_store / 10;
				address[first] = ('0' + int_store % 10);
				int_store = int_data;
				address[first + 1] = ('0' + int_store % 10);
			  length = 2;
 			break;
			
			case NUM_FLOAT:
				int_store = float_data / 10;
				address[first] = ('0' + int_store % 10);
				int_store = float_data;
				address[first + 1] = ('0' + int_store % 10);
			
				address[first + 2] = ('.');
			
				int_store = (float_data * 10 );
				address[first + 3] = ('0' + int_store % 10);
			  length = 4;
			break;
		}
		return length;
}




unsigned char Iso_Printf(char stringa[], char idstringa[], char startpoint)
{
  unsigned char index, length=0;

  for (index = idstringa[sys_language]; index<idstringa[sys_language+1]; index++){
	  ++length;
    temp_string[startpoint+index-idstringa[sys_language]] = stringa[index];
	}
	return length;
}


unsigned char Iso_Print_Symbol(char stringa[], char idstringa[], char pos, char startpoint)
{
  unsigned char index, length=0;

  for (index = idstringa[pos]; index<idstringa[pos+1]; index++){
	  ++length;
    temp_string[startpoint+index-idstringa[pos]] = stringa[index];
	}
	return length;
}



 /***********************************************************************
 * @brief		handle_event			
	 \param [in] struttura (temporanea) relativa all'evento da visualizzare o da salvare su SD
	 \param [in] linea su cui voglio visualizzare l'evento [ da 1 a 5 ]
	 visualizza un evento sulla linea indicata
	 action PRINT_ON_SD or SHOW_ON_DISPLAY
 **********************************************************************/
void handle_event (struct Event Temp, unsigned int line_or_filetype, unsigned char action) {
	
	unsigned int length = 0;
	unsigned int z = 0;
	unsigned int color = WHITE;
	
	for (z=0; z<55; z++) {
		temp_string[z] = ' ';
	}
  
	if (action==SHOW_ON_DISPLAY){
	  // Cancella quella linea !
  	if (line_or_filetype == LINE_1) {
			for (z=0; z<56; z++) {
				GuiVar_events_line_1_string_1[z] = ' ';
			}
	  }
		else if (line_or_filetype == LINE_2) {
			for (z=0; z<56; z++) {
				GuiVar_events_line_2_string_1[z] = ' ';
			}
	  }
		else if (line_or_filetype == LINE_3) {
			for (z=0; z<56; z++) {
				GuiVar_events_line_3_string_1[z] = ' ';
			}
  	}
		else if (line_or_filetype == LINE_4) {
			for (z=0; z<56; z++) {
				GuiVar_events_line_4_string_1[z] = ' ';
			}
	  }
		else if (line_or_filetype == LINE_5) {
			for (z=0; z<56; z++) {
				GuiVar_events_line_5_string_1[z] = ' ';
			}
	  }
  }
	
	switch (Temp.type)
	{
		/* ALARMS */
		case 1:
			switch (Temp.specific_type)
			{
				case 1: // Allarm O2 low 			
		      length += Iso_Printf(MSG_AL_O2_BASSO, ID_MSG_AL_O2_BASSO, 0);
    			length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_1);// valore O2
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
					temp_string[length++] = 'S';
	        length += print_number(NUM_2_DIGITS,length,address,Temp.id_1 ,0);// numero sensore				
					temp_string[length++] = ' ';
					temp_string[length++] = 'Z';
	        length += print_number(NUM_2_DIGITS,length,address,Temp.id_2 ,0);// numero zona
				break;
				
				case 2: // Allarm O2 high
		      length += Iso_Printf(MSG_AL_O2_ALTO, ID_MSG_AL_O2_ALTO, 0);
    			length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_1);// valore O2
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
					temp_string[length++] = 'S';
	        length += print_number(NUM_2_DIGITS,length,address,Temp.id_1 ,0);// numero sensore				
					temp_string[length++] = ' ';
					temp_string[length++] = 'Z';
	        length += print_number(NUM_2_DIGITS,length,address,Temp.id_2 ,0);// numero zona
				break;
				
				case 3: // External alarm
	        length = Iso_Printf(MSG_AL_ESTERNO, ID_MSG_AL_ESTERNO, 0);
				break;				
			}
	    if (action==SHOW_ON_DISPLAY)
			  color = LIGHT_RED;
		break;
		
		/* FAULTS */
		case E_TYPE_FAULT:
	    if (action==SHOW_ON_DISPLAY)
  			color = LIGHT_YELLOW;
		
	    switch (Temp.type_2)
			{
				case F_TYPE_SBM_SD_FAULT:
	        length += Iso_Printf(MSG_SD_SBM, ID_MSG_SD_SBM, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;

			  case F_TYPE_WEBSERVER:
					switch (Temp.specific_type)
					{
						case F_TYPE_WEBSERVER_COMM_FAULT_CAN_1: case F_TYPE_WEBSERVER_COMM_FAULT_CAN_2:
							length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
							temp_string[length++] = ('0' + Temp.specific_type);
					    temp_string[length++] = ' ';
	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_WEBSERVER, ID_MSG_GUASTO_COMUNICAZIONE_WEBSERVER, length);
				      if (Temp.in_out == 0 ){
    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	    }
				      else{
	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					    }
						break;
						
						} // Fine switch(specific_type)						
				break;

				case F_TYPE_AL1_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL1, ID_MSG_GUASTO_AL1, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL2_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL2, ID_MSG_GUASTO_AL2, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;

				case F_TYPE_AL3_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL3, ID_MSG_GUASTO_AL3, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL4_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL4, ID_MSG_GUASTO_AL4, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;

				case F_TYPE_AL5_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL5, ID_MSG_GUASTO_AL5, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL8_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL8, ID_MSG_GUASTO_AL8, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL11_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL11, ID_MSG_GUASTO_AL11, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL12_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL12, ID_MSG_GUASTO_AL12, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL20_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL20, ID_MSG_GUASTO_AL20, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL21_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL21, ID_MSG_GUASTO_AL21, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL22_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL22, ID_MSG_GUASTO_AL22, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL24_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL24, ID_MSG_GUASTO_AL24, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL_CAN_FAULT:
	        length += Iso_Printf(MSG_GUASTO_CAN, ID_MSG_GUASTO_CAN, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL26_SLAVE_FAULT:
	        length += Iso_Printf(MSG_GUASTO_AL26, ID_MSG_GUASTO_AL26, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL27_SONDA_B1:
	        length += Iso_Printf(MSG_GUASTO_AL27, ID_MSG_GUASTO_AL27, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL28_SONDA_B2:
	        length += Iso_Printf(MSG_GUASTO_AL28, ID_MSG_GUASTO_AL28, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
				case F_TYPE_AL29_SONDA_B3:
	        length += Iso_Printf(MSG_GUASTO_AL29, ID_MSG_GUASTO_AL29, 0);
				  if (Temp.in_out == 0 ){
    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
		    	}
				  else{
	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
					}
				break;
						
//				/* SENSOR FAULT */
//				case F_TYPE_SENSOR:
//					switch (Temp.specific_type )
//					{
//						case F_SENSOR_COMM_FAULT: 
//							length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_SENSORE, ID_MSG_GUASTO_COMUNICAZIONE_SENSORE, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;

//						case F_SENSOR_ELAB_FAULT: 
//							length += Iso_Printf(MSG_ELAB_FAULT_SENSOR, ID_MSG_ELAB_FAULT_SENSOR, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;
//						
//						case F_SENSOR_SD_FAULT: //non usato perch� non c'� SD su sensore
//						break;	

//						case F_SENSOR_WARM_UP: 
//							length += Iso_Printf(MSG_WARMUP, ID_MSG_WARMUP, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;

//						case F_SENSOR_CAN1_FAULT: 
//							length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
//							temp_string[length++] = '1';
//							length += Iso_Printf(MSG_COMM_FAULT_SENSOR, ID_MSG_COMM_FAULT_SENSOR, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;

//						case F_SENSOR_CAN2_FAULT: 
//							length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
//							temp_string[length++] = '2';
//							length += Iso_Printf(MSG_COMM_FAULT_SENSOR, ID_MSG_COMM_FAULT_SENSOR, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;
//					}			
//				break;
//				
//				case F_TYPE_O2_DIFF: 
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_O2_DIFF_1: 
//  						length += Iso_Printf(MSG_O2_MAG_05, ID_MSG_O2_MAG_05, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							temp_string[length++] = ' ';
//							temp_string[length++] = '&';
//							temp_string[length++] = ' ';
//							length += print_number(NUM_2_DIGITS,length,address,(Temp.id_1 + 1),0); // numero sensore					
//							temp_string[length++] = ' ';
//							temp_string[length++] = 'Z';
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;
//						
//						case F_TYPE_O2_DIFF_2: 
//  						length += Iso_Printf(MSG_O2_MAG_20, ID_MSG_O2_MAG_20, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore				
//							temp_string[length++] = ' ';
//							temp_string[length++] = '&';
//							temp_string[length++] = ' ';
//							length += print_number(NUM_2_DIGITS,length,address,(Temp.id_1 + 1),0); // numero sensore					
//							temp_string[length++] = ' ';
//							temp_string[length++] = 'Z';
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;
//					} // Fine switch(specific_type)				
//				break;
//				
//				case F_TYPE_FLUX:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_AUX_FLUX: 
//  						length += Iso_Printf(MSG_AUX_FLOW_FAULT, ID_MSG_AUX_FLOW_FAULT, 0);
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;
//						
//						case F_TYPE_ZONE_FLUX: 
//  						length += Iso_Printf(MSG_FLUX, ID_MSG_FLUX, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero flussotato				
//  						length += Iso_Printf(MSG_FAULT_ZONE, ID_MSG_FAULT_ZONE, 0);
//							length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//							if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//							} else {
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//							}
//						break;
//					} // Fine switch(specific_type)						
//				break;
//				
//				case F_TYPE_SWITCHES:
//  				length += Iso_Printf(MSG_SWITCH, ID_MSG_SWITCH, 0);
//					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero finecorsa				
//  				length += Iso_Printf(MSG_FAULT_ZONE, ID_MSG_FAULT_ZONE, 0);
//					length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona
//					if (Temp.in_out == 0 ){
//	            length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					} else {
//	            length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					}
//				break;				

//				case F_TYPE_PW_SUPPLY:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_PW_BROKEN: 
//  				    length += Iso_Printf(MSG_ALIMENTATORE, ID_MSG_ALIMENTATORE, 0);
//    					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero alimentatore				
//  				    length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
//					    if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					    }
//							else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case F_TYPE_PW_NO_NET: 
//  				    length += Iso_Printf(MSG_ALIMENTATORE, ID_MSG_ALIMENTATORE, 0);
//    					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero alimentatore				
//  				    length += Iso_Printf(MSG_GUASTO_RETE, ID_MSG_GUASTO_RETE, length);
//					    if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					    }
//							else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case F_TYPE_PW_BATTERY: 
//  				    length += Iso_Printf(MSG_BATTERY, ID_MSG_BATTERY, 0);
//    					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero alimentatore				
//  				    length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
//					    if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					    }
//							else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//					} // Fine switch(specific_type)						
//				break;
//				
//				case F_TYPE_THERMOSTAT:
//  				    length += Iso_Printf(MSG_TEMP_SM, ID_MSG_TEMP_SM, 0);
//							temp_string[length++] = ' ';
//							temp_string[length++] = '1';
//					    if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					    }
//							else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//							break;	
//							
//							case F_TYPE_THERMOSTAT_2:
//  				    length += Iso_Printf(MSG_TEMP_SM, ID_MSG_TEMP_SM, 0);
//							temp_string[length++] = ' ';
//							temp_string[length++] = '2';
//					    if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					    }
//							else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//							break;
//				
//				case F_TYPE_PSA:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_PSA_BROKEN: 
//  				    length += Iso_Printf(MSG_MACHINE, ID_MSG_MACHINE, 0);
//    					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero macchina				
//  				    length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
//					    if (Temp.in_out == 0 ){
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					    }
//							else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case 2: 

//						break;
//					} // Fine switch(specific_type)						
//				break;
//				
//				case F_TYPE_EXTERNAL:
//  				length += Iso_Printf(MSG_EXT_FAULT, ID_MSG_EXT_FAULT, 0);
//					if (Temp.in_out == 0 ){
//	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//					}
//					else{
//	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					}
//				break;
//				
//				case F_TYPE_MONITORING:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_IN_CTO_CTO: 
//	            length += Iso_Printf(MSG_INPUT, ID_MSG_INPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero ingresso
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_CORTO, ID_MSG_CORTO, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case F_TYPE_IN_OPEN: 
//	            length += Iso_Printf(MSG_INPUT, ID_MSG_INPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero ingresso
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_OPEN, ID_MSG_OPEN, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case F_TYPE_OUT_CTO_CTO: 
//	            length += Iso_Printf(MSG_OUTPUT, ID_MSG_OUTPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero rele
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_CORTO, ID_MSG_CORTO, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case F_TYPE_OUT_OPEN: 
//	            length += Iso_Printf(MSG_OUTPUT, ID_MSG_OUTPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero rele
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_OPEN, ID_MSG_OPEN, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;

//						case F_TYPE_COMM_FAULT: 
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE, ID_MSG_GUASTO_COMUNICAZIONE, 0);
//							temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero modulo
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//	
//			      case F_TYPE_COMM_FAULT_CAN_1: 
//	            length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
//							temp_string[length++] = '1';
//							temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE, ID_MSG_GUASTO_COMUNICAZIONE, length);
//							temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero modulo
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						case F_TYPE_COMM_FAULT_CAN_2: 
//	            length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
//							temp_string[length++] = '2';
//							temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE, ID_MSG_GUASTO_COMUNICAZIONE, length);
//							temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero modulo
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;

//            case F_TYPE_CONTROLL_PW_1:

//              temp_string[0] = 'P';
//							temp_string[1] = 'o';
//							temp_string[2] = 'w';
//							temp_string[3] = 'e';
//							temp_string[4] = 'r';
//							temp_string[5] = ' ';
//							print_number(NUM_2_DIGITS,6,address,1,0);
//							temp_string[8] = ' ';
//							temp_string[9] = 'm';
//							temp_string[10] = 'o';
//							temp_string[11] = 'd';
//							temp_string[12] = 'u';
//							temp_string[13] = 'l';
//							temp_string[14] = 'e';
//							temp_string[15] = ' ';
//							print_number(NUM_2_DIGITS,16,address,Temp.id_2,0);
//							temp_string[18] = ' ';
//							temp_string[19] = ':';
//							temp_string[20] = ' ';
//							temp_string[21] = 'F';
//							temp_string[22] = 'A';
//							temp_string[23] = 'U';
//							temp_string[24] = 'L';
//							temp_string[25] = 'T';
//							if (Temp.in_out == 0 ){
//								temp_string[26] = ' ';
//								temp_string[27] = '<';
//								temp_string[28] = '-';
//								temp_string[29] = '-';								
//							} else {
//								temp_string[26] = ' ';
//								temp_string[27] = '-';
//								temp_string[28] = '-';
//								temp_string[29] = '>';		
//							}									
//							length = 30;
//				   break;
//					 
//					 case F_TYPE_CONTROLL_PW_2:

//              temp_string[0] = 'P';
//							temp_string[1] = 'o';
//							temp_string[2] = 'w';
//							temp_string[3] = 'e';
//							temp_string[4] = 'r';
//							temp_string[5] = ' ';
//							print_number(NUM_2_DIGITS,6,address,2,0);
//							temp_string[8] = ' ';
//							temp_string[9] = 'm';
//							temp_string[10] = 'o';
//							temp_string[11] = 'd';
//							temp_string[12] = 'u';
//							temp_string[13] = 'l';
//							temp_string[14] = 'e';
//							temp_string[15] = ' ';
//							print_number(NUM_2_DIGITS,16,address,Temp.id_2,0);
//							temp_string[18] = ' ';
//							temp_string[19] = ':';
//							temp_string[20] = ' ';
//							temp_string[21] = 'F';
//							temp_string[22] = 'A';
//							temp_string[23] = 'U';
//							temp_string[24] = 'L';
//							temp_string[25] = 'T';
//							if (Temp.in_out == 0 ){
//								temp_string[26] = ' ';
//								temp_string[27] = '<';
//								temp_string[28] = '-';
//								temp_string[29] = '-';								
//							} else {
//								temp_string[26] = ' ';
//								temp_string[27] = '-';
//								temp_string[28] = '-';
//								temp_string[29] = '>';		
//							}									
//							length = 30;
//				   break;
//					
//						case F_DIGIN_CUT: 								
//	            length += Iso_Printf(MSG_INPUT, ID_MSG_INPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero ingresso
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_CUT, ID_MSG_CUT, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//				   break;
//				
//				   case F_DIGIN_SHORT_CIRCUIT:
//	            length += Iso_Printf(MSG_INPUT, ID_MSG_INPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero ingresso
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_CORTO, ID_MSG_CORTO, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//				   break; 
//					 
//				   case F_RELE:
//	            length += Iso_Printf(MSG_OUTPUT, ID_MSG_OUTPUT, 0);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero rele
//	            length += Iso_Printf(MSG_MODULO, ID_MSG_MODULO, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero modulo
//	            length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
//				    	if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    			}
//				    	else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//					  break;
//					} // Fine switch(specific_type)						
//				break;
//				
//				case F_TYPE_NETWORK:
//	        length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE, ID_MSG_GUASTO_COMUNICAZIONE, 0);
//				  if (Temp.in_out == 0 ){
//    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	}
//				  else{
//	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					}
//				break;

//				case F_TYPE_SBM_SD_FAULT:
//	        length += Iso_Printf(MSG_SD_SBM, ID_MSG_SD_SBM, 0);
//				  if (Temp.in_out == 0 ){
//    	      length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	}
//				  else{
//	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					}
//				break;

//				/* EXT DISPLAY FAULT */
//				case F_TYPE_EXT_DISPLAY:
//					switch (Temp.specific_type )
//					{
//						case F_TYPE_EXT_DISPLAY_COMM_FAULT_CAN_1: case F_TYPE_EXT_DISPLAY_COMM_FAULT_CAN_2:
//							length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
//							temp_string[length++] = ('0' + Temp.specific_type);
//					    temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_DISPLAY, ID_MSG_GUASTO_COMUNICAZIONE_DISPLAY, length);
//    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero display
//							temp_string[length++] = ' ';
//							temp_string[length++] = 'Z';						
//							length +=  print_number(NUM_2_DIGITS,length,address,Temp.id_2 ,0);// numero zona
//				      if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	    }
//				      else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//					}			
//				break;

//			  case F_TYPE_EWON:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_EWON_COMM_FAULT: 
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_ANYBUS, ID_MSG_GUASTO_COMUNICAZIONE_ANYBUS, 0);
//				      if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	    }
//				      else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						} // Fine switch(specific_type)						
//				break;

//			  case F_TYPE_WEBSERVER:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_WEBSERVER_COMM_FAULT_CAN_1: case F_TYPE_WEBSERVER_COMM_FAULT_CAN_2:
//							length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
//							temp_string[length++] = ('0' + Temp.specific_type);
//					    temp_string[length++] = ' ';
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_WEBSERVER, ID_MSG_GUASTO_COMUNICAZIONE_WEBSERVER, length);
//				      if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	    }
//				      else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//						} // Fine switch(specific_type)						
//				break;

//				case F_TYPE_ADOX:
//					switch (Temp.specific_type)
//					{
//						case F_TYPE_ADOX_FAULT: 
//	            length += Iso_Printf(MSG_MACHINE, ID_MSG_MACHINE, 0);
//	            length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
//				      if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	    }
//				      else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;

//						case F_TYPE_ADOX_COMM_FAULT: 
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_ADOX, ID_MSG_GUASTO_COMUNICAZIONE_ADOX, 0);
//				      if (Temp.in_out == 0 ){
//    	          length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	    }
//				      else{
//	              length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					    }
//						break;
//						
//					} // Fine switch(specific_type)						
//				break;
//						
//		    case F_TYPE_CIE:
//			    switch (Temp.specific_type)
//			    {
//				    case F_TYPE_CIE_COMM_FAULT: 
//	            length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_CIE, ID_MSG_GUASTO_COMUNICAZIONE_CIE, 0);
//				        if (Temp.in_out == 0 ){
//    	            length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_IN, length);
//		    	      }
//				        else{
//	                length += Iso_Print_Symbol(MSG_GUASTO_IN_OUT, ID_MSG_GUASTO_IN_OUT, GUASTO_OUT, length);
//					      }
//				    break;
//			    } // Fine switch(specific_type)						
//		    break;
			
		} // Fine switch(type)
		break; // fine FAULTS
		
		/* DISABLEMENTS */
		case 3:
	    if (action==SHOW_ON_DISPLAY)
			  color = LIGHT_VIOLET;
		
			switch (Temp.object_type)
			{
				case O_TYPE_ZONE:
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero zona					
					temp_string[length++] = ' ';
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
					}
					temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
			  	length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
	        if (action==SHOW_ON_DISPLAY)
					  color = DARK_VIOLET;
				break;
				
				case O_TYPE_SENSOR:
	        length += Iso_Printf(MSG_O2_SENSOR, ID_MSG_O2_SENSOR, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore					
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
					}
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
					temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
			  	length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
					
				case O_TYPE_SIREN:
					if (Temp.specific_type == 0) {
							/* SIREN CIE */
	            length += Iso_Printf(MSG_SIREN, ID_MSG_SIREN, 0);
  					  temp_string[length++] = 'C';
  					  temp_string[length++] = 'I';
  					  temp_string[length++] = 'E';
  					  temp_string[length++] = ' ';
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* SIREN ZONE X*/
	            length += Iso_Printf(MSG_SIREN, ID_MSG_SIREN, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sirena					
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;	

				case O_TYPE_LAMP:
					if (Temp.specific_type == 0) {
							/* LAMP CIE */
	            length += Iso_Printf(MSG_LAMP, ID_MSG_LAMP, 0);
  					  temp_string[length++] = 'C';
  					  temp_string[length++] = 'I';
  					  temp_string[length++] = 'E';
  					  temp_string[length++] = ' ';
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* LAMP ZONE X*/
	            length += Iso_Printf(MSG_LAMP, ID_MSG_LAMP, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero lampada					
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;

				case O_TYPE_PANEL:
	        length += Iso_Printf(MSG_PANEL, ID_MSG_PANEL, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero pannello					
				  if (Temp.in_out == 1){
        	  length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    }
  		  	else{
	          length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  }
    	    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        	length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				  temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
  				temp_string[length++] = '.';
	  		  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;

				case O_TYPE_VALVE:
					if (Temp.specific_type == 0) {
							/* Valve CIE */
	            length += Iso_Printf(MSG_AUX_VALVE, ID_MSG_AUX_VALVE, 0);
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* Valve ZONE X*/
	            length += Iso_Printf(MSG_VALVE, ID_MSG_VALVE, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero lampada					
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;	

				case O_TYPE_EXT_DISPLAY:
	        length += Iso_Printf(MSG_DISPLAY, ID_MSG_DISPLAY, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero display					
				  if (Temp.in_out == 1){
        	  length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    }
  		  	else{
	          length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  }
    	    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        	length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				  temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
  				temp_string[length++] = '.';
	  		  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;

				case O_TYPE_FLUX:
					if (Temp.specific_type == 0) {
							/* FLUX CIE */
	            length += Iso_Printf(MSG_AUX_FLUX, ID_MSG_AUX_FLUX, 0);
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* FLUX ZONE X*/
	            length += Iso_Printf(MSG_FLUX, ID_MSG_FLUX, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero lampada					
				      if (Temp.in_out == 1){
        	      length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    	  }
  		  		  else{
	              length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  	  }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;

				case O_TYPE_BUZZER:
	        length += Iso_Printf(MSG_BUZZER, ID_MSG_BUZZER, 0);
				  temp_string[length++] = 'C';
				  temp_string[length++] = 'I';
				  temp_string[length++] = 'E';
				  if (Temp.in_out == 1){
        	  length += Iso_Printf(MSG_DISABLED, ID_MSG_DISABLED, length);
	  	    }
  		  	else{
	          length += Iso_Printf(MSG_ENABLED, ID_MSG_ENABLED, length);
				  }
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
  				temp_string[length++] = '.';
	  		  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
			
			}
		break;
		
		/* DEVICES CONTROL */
		case 4:
	    if (action==SHOW_ON_DISPLAY)
			  color = LIGHT_GREEN2;
		
			switch (Temp.object_type)
			{
				case O_TYPE_ZONE:
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero zona					
					temp_string[length++] = ' ';
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					}
					temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
			  	length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
    	    if (action==SHOW_ON_DISPLAY)
		  			color = DARK_GREEN2;
				break;
				
				case O_TYPE_SENSOR:
	        length += Iso_Printf(MSG_O2_SENSOR, ID_MSG_O2_SENSOR, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sensore					
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					}
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
					temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
			  	length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
					
				case O_TYPE_SIREN:
					if (Temp.specific_type == 0) {
							/* SIREN CIE */
	            length += Iso_Printf(MSG_SIREN, ID_MSG_SIREN, 0);
  					  temp_string[length++] = 'C';
  					  temp_string[length++] = 'I';
  					  temp_string[length++] = 'E';
  					  temp_string[length++] = ' ';
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}
					else{
    					/* SIREN ZONE X*/
	            length += Iso_Printf(MSG_SIREN, ID_MSG_SIREN, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero sirena					
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;	

				case O_TYPE_LAMP:
					if (Temp.specific_type == 0) {
							/* LAMP CIE */
	            length += Iso_Printf(MSG_LAMP, ID_MSG_LAMP, 0);
  					  temp_string[length++] = 'C';
  					  temp_string[length++] = 'I';
  					  temp_string[length++] = 'E';
  					  temp_string[length++] = ' ';
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* LAMP ZONE X*/
	            length += Iso_Printf(MSG_LAMP, ID_MSG_LAMP, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero lampada					
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;

				case O_TYPE_PANEL:
	        length += Iso_Printf(MSG_PANEL, ID_MSG_PANEL, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero pannello					
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					}
    	    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        	length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				  temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
  				temp_string[length++] = '.';
	  		  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;

				case O_TYPE_VALVE:
					if (Temp.specific_type == 0) {
							/* Valve CIE */
	            length += Iso_Printf(MSG_AUX_VALVE, ID_MSG_AUX_VALVE, 0);
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		    	    }
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* Valve ZONE X*/
	            length += Iso_Printf(MSG_VALVE, ID_MSG_VALVE, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero lampada					
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;	

				case O_TYPE_EXT_DISPLAY:
	        length += Iso_Printf(MSG_DISPLAY, ID_MSG_DISPLAY, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero display					
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					}
    	    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        	length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				  temp_string[length++] = ' ';
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
  				temp_string[length++] = '.';
	  		  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;

				case O_TYPE_FLUX:
					if (Temp.specific_type == 0) {
							/* FLUX CIE */
	            length += Iso_Printf(MSG_AUX_FLUX, ID_MSG_AUX_FLUX, 0);
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					} 
					else{
					    /* FLUX ZONE X*/
	            length += Iso_Printf(MSG_FLUX, ID_MSG_FLUX, 0);
    			    length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);// numero lampada					
				      if (Temp.in_out == 1){
    	          length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		         	}
				      else{
	              length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					    }
    	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
        			length += print_number(NUM_2_DIGITS,length,address,Temp.id_2,0);// numero zona					
				    	temp_string[length++] = ' ';
    	        length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					    temp_string[length++] = ('0' + Temp.user_level);
  					  temp_string[length++] = '.';
	  		  	  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
					}						
				break;

				case O_TYPE_BUZZER:
	        length += Iso_Printf(MSG_BUZZER, ID_MSG_BUZZER, 0);
				  temp_string[length++] = 'C';
				  temp_string[length++] = 'I';
				  temp_string[length++] = 'E';
				  if (Temp.in_out == 1){
    	      length += Iso_Printf(MSG_ACTIV, ID_MSG_ACTIV, length);
		    	}
				  else{
	          length += Iso_Printf(MSG_DEACTIV, ID_MSG_DEACTIV, length);
					}
    	    length += Iso_Printf(MSG_USER, ID_MSG_USER, length);
					temp_string[length++] = ('0' + Temp.user_level);
  				temp_string[length++] = '.';
	  		  length +=  print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
					
			}
		break;
		
		/* MISC */
		case 5:
			switch (Temp.specific_type)
			{
				case E_S_TYPE_LOGIN: // "Login"
	        length += Iso_Printf(MSG_LOGIN, ID_MSG_LOGIN, 0);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
				
				case E_S_TYPE_LOGOUT: // "Logout"
	        length += Iso_Printf(MSG_LOGOUT, ID_MSG_LOGOUT, 0);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
				
				case E_S_TYPE_NEW_CONFIG: 
	        length += Iso_Printf(MSG_NEW_CONFIG, ID_MSG_NEW_CONFIG, 0);
				break;
				
				case E_S_TYPE_SYSTEM_TURN_ON: 
	        length += Iso_Printf(MSG_SYSTEM_ON, ID_MSG_SYSTEM_ON, 0);
				break;	

				case E_S_TYPE_SAVE_SET: 
	        length += Iso_Printf(MSG_SET, ID_MSG_SET, 0);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_1);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_IST, ID_MSG_IST, length);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_2);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
					temp_string[length++] = 'Z';
					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);
					temp_string[length++] = ' ';
					temp_string[length++] = 'U';
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
	
				case E_S_TYPE_SAVE_SET_ALL: 
	        length += Iso_Printf(MSG_SET, ID_MSG_SET, 0);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_1);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_IST, ID_MSG_IST, length);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_2);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_ALL, ID_MSG_ALL, length);
					temp_string[length++] = ' ';
					temp_string[length++] = 'U';
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;	
				
				case E_S_TYPE_SAVE_TH: 
	        length += Iso_Printf(MSG_O2_HIGH, ID_MSG_O2_HIGH, 0);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_1);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_O2_LOW, ID_MSG_O2_LOW, length);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_2);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
					temp_string[length++] = 'Z';
					length += print_number(NUM_2_DIGITS,length,address,Temp.id_1,0);
					temp_string[length++] = ' ';
					temp_string[length++] = 'U';
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;	

				case E_S_TYPE_SAVE_TH_ALL: 
	        length += Iso_Printf(MSG_O2_HIGH, ID_MSG_O2_HIGH, 0);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_1);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_O2_LOW, ID_MSG_O2_LOW, length);
					length += print_number(NUM_FLOAT,length,address,0,Temp.float_value_2);
					temp_string[length++] = '%';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_ALL, ID_MSG_ALL, length);
					temp_string[length++] = ' ';
					temp_string[length++] = 'U';
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;

				case E_S_TYPE_DELETE_EVENTS: 
	        length += Iso_Printf(MSG_EVENTS_DEL, ID_MSG_EVENTS_DEL, 0);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;	

				case E_S_TYPE_DELETE_ALARMS: 
	        length += Iso_Printf(MSG_ALARMS_DEL, ID_MSG_ALARMS_DEL, 0);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;

				case E_S_TYPE_RESET_ALARMS: 
	        length += Iso_Printf(MSG_ALARMS_RESET, ID_MSG_ALARMS_RESET, 0);
					temp_string[length++] = ('0' + Temp.user_level);
					temp_string[length++] = '.';
					length += print_number(NUM_2_DIGITS,length,address,Temp.user_id,0);
				break;
				
			} // Fine switch(specific_type)
	    if (action==SHOW_ON_DISPLAY)
  			color = WHITE;
		break;
			
	} // Fine switch(type)
	
	if (action==SHOW_ON_DISPLAY){
	  if (line_or_filetype == LINE_1) {
			GuiVar_ev_1_day     = Temp.time.day;
			GuiVar_ev_1_month   = Temp.time.month;
			GuiVar_ev_1_year    = Temp.time.year;
			GuiVar_ev_1_hour    = Temp.time.hour;
			GuiVar_ev_1_minutes = Temp.time.minutes;
			GuiVar_ev_1_seconds = Temp.time.seconds;
		
			for (z=0; z<length; z++) {
				GuiVar_events_line_1_string_1[z] = temp_string[z];
			}
			GuiVar_line_1_bkg_color = color;
	  }
	  if (line_or_filetype == LINE_2) {
			GuiVar_ev_2_day     = Temp.time.day;
			GuiVar_ev_2_month   = Temp.time.month;
			GuiVar_ev_2_year    = Temp.time.year;
			GuiVar_ev_2_hour    = Temp.time.hour;
			GuiVar_ev_2_minutes = Temp.time.minutes;
			GuiVar_ev_2_seconds = Temp.time.seconds;
		
			for (z=0; z<length; z++) {
				GuiVar_events_line_2_string_1[z] = temp_string[z];
			}
			GuiVar_line_2_bkg_color = color;
	  }		
	  if (line_or_filetype == LINE_3) {
			GuiVar_ev_3_day     = Temp.time.day;
			GuiVar_ev_3_month   = Temp.time.month;
			GuiVar_ev_3_year    = Temp.time.year;
			GuiVar_ev_3_hour    = Temp.time.hour;
			GuiVar_ev_3_minutes = Temp.time.minutes;
			GuiVar_ev_3_seconds = Temp.time.seconds;
		
			for (z=0; z<length; z++) {
				GuiVar_events_line_3_string_1[z] = temp_string[z];
			}
			GuiVar_line_3_bkg_color = color;
	  }		
	  if (line_or_filetype == LINE_4) {
			GuiVar_ev_4_day     = Temp.time.day;
			GuiVar_ev_4_month   = Temp.time.month;
			GuiVar_ev_4_year    = Temp.time.year;
			GuiVar_ev_4_hour    = Temp.time.hour;
			GuiVar_ev_4_minutes = Temp.time.minutes;
			GuiVar_ev_4_seconds = Temp.time.seconds;
		
			for (z=0; z<length; z++) {
				GuiVar_events_line_4_string_1[z] = temp_string[z];
			}
			GuiVar_line_4_bkg_color = color;
	  }		
	  if (line_or_filetype == LINE_5) {
			GuiVar_ev_5_day     = Temp.time.day;
			GuiVar_ev_5_month   = Temp.time.month;
			GuiVar_ev_5_year    = Temp.time.year;
			GuiVar_ev_5_hour    = Temp.time.hour;
			GuiVar_ev_5_minutes = Temp.time.minutes;
			GuiVar_ev_5_seconds = Temp.time.seconds;
		
			for (z=0; z<length; z++) {
				GuiVar_events_line_5_string_1[z] = temp_string[z];
			}
			GuiVar_line_5_bkg_color = color;
	  }
	}
  else if (action==PRINT_ON_SD){
	  switch (line_or_filetype)
	  {
		  case ALARMS_FILE:
				alarms_file = fopen(alarms_file_name, "a");
				for(z=0; z<length; z++){
					fputc(temp_string[z],alarms_file);
				}
				fputc('\r',alarms_file);
				fputc('\r',alarms_file);
				fputc('\n',alarms_file);
				fputc('\n',alarms_file);
				fclose(alarms_file);		
		  break;
		
	  	case EVENTS_FILE:
				events_file = fopen(events_file_name, "a");
				for(z=0; z<length; z++){
					fputc(temp_string[z],events_file);
				}
				fputc('\r',events_file);
				fputc('\r',events_file);
				fputc('\n',events_file);
				fputc('\n',events_file);
				fclose(events_file);			
  		break;
		
	  	case O2_VALUES_FILE:
		
		  break;
	  }
	}
}


/***********************************************************************
 * @brief		print_data_on_sd			
 **********************************************************************/
void print_data_on_sd (struct Time data, unsigned int file_type) {
	
	unsigned int int_store   = 0;
	unsigned int t           = 0;
	
	data_string[0] = ' ';
	
	// print day
	int_store = data.day;
	int_store = int_store / 10;
	data_string[1] = ('0' + int_store % 10);
	int_store = data.day;
	data_string[2] = ('0' + int_store % 10);
	data_string[3] = '/';
	
	// print month
	int_store = data.month;
	int_store = int_store / 10;
	data_string[4] = ('0' + int_store % 10);
	int_store = data.month;
	data_string[5] = ('0' + int_store % 10);
	data_string[6] = '/';
	
	// print year
	int_store = data.year;
	int_store = int_store / 1000;
	data_string[7] = ('0' + int_store % 10);
	int_store = data.year;
	int_store = int_store / 100;
	data_string[8] = ('0' + int_store % 10);
	int_store = data.year;
	int_store = int_store / 10;
	data_string[9] = ('0' + int_store % 10);	
	int_store = data.year;
	data_string[10] = ('0' + int_store % 10);
	
	data_string[11] = ' ';
	
	// print hour
	int_store = data.hour ;
	int_store = int_store / 10;
	data_string[12] = ('0' + int_store % 10);
	int_store = data.hour;
	data_string[13] = ('0' + int_store % 10);
	data_string[14] = ':';	
	
	// print minutes
	int_store = data.minutes ;
	int_store = int_store / 10;
	data_string[15] = ('0' + int_store % 10);
	int_store = data.minutes;
	data_string[16] = ('0' + int_store % 10);
	data_string[17] = ':';		
	
	// print seconds
	int_store = data.seconds ;
	int_store = int_store / 10;
	data_string[18] = ('0' + int_store % 10);
	int_store = data.seconds;
	data_string[19] = ('0' + int_store % 10);
	data_string[20] = ' ';
	
	data_string[21] = ' ';
	
	switch (file_type)
	{
		case ALARMS_FILE:
			alarms_file = fopen(alarms_file_name, "a");
// 			fputs(data_string, alarms_file);
			for (t=0; t<22; t++){
				fputc(data_string[t],alarms_file);
			}
			fclose(alarms_file);	
				
		break;
		
		case EVENTS_FILE:
			events_file = fopen(events_file_name, "a");
// 			fputs(data_string, events_file);
			for (t=0; t<22; t++){
				fputc(data_string[t],events_file);
			}		
			fclose(events_file);				
		break;
		
		case O2_VALUES_FILE:
			o2_values_file = fopen(o2_values_file_name, "a");
// 			fputs(data_string, o2_values_file);
			for (t=0; t<22; t++){
				fputc(data_string[t],o2_values_file);
			}		
			fclose(o2_values_file);			
		break;
	}
}

 /***********************************************************************
 * @brief		print_o2_values			
 **********************************************************************/
void print_o2_values (void) {
	
	unsigned int i = 0;
	unsigned int r = 0;
	unsigned int t = 0;
	unsigned int int_store = 0;
	char zone_temp_string[16];
	char sensor_temp_string[18];
	
	float o2_value = 0;
	
	if (print_o2_timeout == 0) {
			print_o2_timeout = SAVE_O2_VALUES;

			/* STAMPA DATA */
			print_data_on_sd(current_time, O2_VALUES_FILE);
			o2_values_file = fopen(o2_values_file_name, "a");
		
			// Per ogni zona
			for(r=1; r< (sys_zones_num + 1); r++ ){
				
				for(t=0; t<17; t++){
					zone_temp_string[t] = ' ';
				}
					
				o2_value = zones_array[r].o2_mean;
				
				fputc(' ', o2_values_file);
				fputc(' ', o2_values_file);			
				zone_temp_string[0] = 'Z';
				zone_temp_string[1] = 'O';
				zone_temp_string[2] = 'N';
				zone_temp_string[3] = 'E';
				zone_temp_string[4] = ' ';
				
				// print ID
				int_store = r ;
				int_store = int_store / 10;
				zone_temp_string[5] = ('0' + int_store % 10);			
				int_store = r ;
				zone_temp_string[6] = ('0' + int_store % 10);
				zone_temp_string[7] = ' ';

				if ( zones_array[r].no_value == 0	){
				// print O2 value
					int_store = o2_value / 10;
					zone_temp_string[8] = ('0' + int_store % 10);
					int_store = o2_value;
					zone_temp_string[9] = ('0' + int_store % 10);	
					zone_temp_string[10] = ('.');
					int_store = o2_value * 10;
					zone_temp_string[11] = ('0' + int_store % 10);
//					int_store = o2_value * 100;
//					zone_temp_string[12] = ('0' + int_store % 10);	
					zone_temp_string[12] = ' ';
					zone_temp_string[13] = '%';
					zone_temp_string[14] = ' ';
				} else {
					zone_temp_string[8] = '-';
					zone_temp_string[9] = '-';
					zone_temp_string[10] = '-';
					zone_temp_string[11] = '-';
					zone_temp_string[12] = ' ';
					zone_temp_string[13] = ' ';
					zone_temp_string[14] = ' ';								
				}
								
// 				fputs(zone_temp_string, o2_values_file);
				for(t=0; t<15; t++) {
					fputc(zone_temp_string[t], o2_values_file);
				}
				
				for(i = zones_array[r].first_sensor ; i< (zones_array[r].first_sensor + zones_array[r].n_sensors ); i++ ){
					
						o2_value = sensors_array[i].o2_value;
					
						for(t=0; t<19; t++){
							sensor_temp_string[t] = ' ';
						}
						
						sensor_temp_string[0] = 'S';
						sensor_temp_string[1] = 'e';
						sensor_temp_string[2] = 'n';
						sensor_temp_string[3] = 's';
						sensor_temp_string[4] = 'o';
						sensor_temp_string[5] = 'r';
						sensor_temp_string[6] = ' ';
						// print ID
							int_store = i ;
							int_store = int_store / 10;
							sensor_temp_string[7] = ('0' + int_store % 10);	
							int_store = i ;
							sensor_temp_string[8] = ('0' + int_store % 10);
							sensor_temp_string[9] = ' ';

							if ( sensors_array[i].comm_fault == 0	){
									// print O2 value
										int_store = o2_value / 10;
										sensor_temp_string[10] = ('0' + int_store % 10);
										int_store = o2_value;
										sensor_temp_string[11] = ('0' + int_store % 10);	
										sensor_temp_string[12] = ('.');
										int_store = o2_value * 10;
										sensor_temp_string[13] = ('0' + int_store % 10);
//										int_store = o2_value * 100;
//										sensor_temp_string[14] = ('0' + int_store % 10);	
										sensor_temp_string[14] = ' ';
										sensor_temp_string[15] = '%';	
										sensor_temp_string[16] = ' ';									
							} else {
										sensor_temp_string[10] = '-';
										sensor_temp_string[11] = '-';
										sensor_temp_string[12] = '-';
										sensor_temp_string[13] = '-';
										sensor_temp_string[14] = ' ';
										sensor_temp_string[15] = ' ';
										sensor_temp_string[16] = ' ';									
							}
							
// 							fputs(sensor_temp_string, o2_values_file);
								for(t=0; t<17; t++) {
									fputc(sensor_temp_string[t], o2_values_file);
								}							
							fputc(' ',o2_values_file);
							
				}// fine per tutti i sensori
				
				fputc(' ',o2_values_file);
				fputc(' ',o2_values_file);
				fputc(' ',o2_values_file);
				
			}// Fine per ogni zona
			
			fputc('\r',o2_values_file);
			fputc('\r',o2_values_file);
			fputc('\n',o2_values_file);
			fputc('\n',o2_values_file);
			fclose(o2_values_file);		
	}		
			
}

 /***********************************************************************
 * @brief		add_event			
	 \param struttura (temporanea) relativa al nuovo evento
	 aggiunge un evento alla lista degli eventi
 **********************************************************************/
void add_event (struct Event Temp) { 
	
	unsigned int i = 0;
 
	// fai spazio per il nuovo arrivato; butta via il pi� vecchio
	for (i=1; i< (EVENTS_ARRAY_DIMENSIONS); i++) {
		
		// copia tutti i campi
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].float_value_1 = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].float_value_1;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].float_value_2 = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].float_value_2;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].id_1          = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].id_1;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].id_2          = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].id_2;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].specific_type = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].specific_type;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].time          = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].time;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].type          = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].type;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].type_2        = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].type_2;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].user_level    = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].user_level;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].user_id       = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].user_id;
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].in_out        = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].in_out;	
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].object_type   = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].object_type;	
		events_array[(EVENTS_ARRAY_DIMENSIONS - i)].id            = events_array[(EVENTS_ARRAY_DIMENSIONS - i - 1)].id;	
		
                
	}	

	// ECCO QUELLO NUOVO
	events_array[0].float_value_1 = Temp.float_value_1;
	events_array[0].float_value_2 = Temp.float_value_2;
	events_array[0].id_1          = Temp.id_1;
	events_array[0].id_2          = Temp.id_2;
	events_array[0].specific_type = Temp.specific_type;
	events_array[0].time          = Temp.time;
	events_array[0].type          = Temp.type;
	events_array[0].type_2        = Temp.type_2;
	events_array[0].user_level    = Temp.user_level;
	events_array[0].user_id       = Temp.user_id;
	events_array[0].in_out        = Temp.in_out;
	events_array[0].object_type   = Temp.object_type;
	events_array[0].id            = Temp.id;
	

	
	if (events_array_dim < EVENTS_ARRAY_DIMENSIONS){
		events_array_dim++;
	}

	#ifdef USE_SD
		if (use_sd == 1){
			// Gi� che sei qua, aggiungi l'evento nel file dell'SD !
			print_data_on_sd(events_array[0].time, EVENTS_FILE);
			handle_event(events_array[0], EVENTS_FILE, PRINT_ON_SD);
		}
	#endif
}

 /***********************************************************************
 * @brief		handle_rectangle_blink	
	 gestisce il lampeggio del rettangolo
 **********************************************************************/
void handle_rectangle_blink (void) { 
	
	/* Background variable rectangle color */
	if (test_leds_timeout == 0) {
		switch (GuiVar_screen_select)
		{
			case CHARTS_SCREEN:
			case ALARMS_LIST_SCREEN:
				if (alarms_number == 0){
					if ((faults_number != 0) || (disablements_number != 0) || (dev_ctrl_number != 0) ){
						
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREY) {
									GuiVar_var_rectangle_color = DARK_YELLOW;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREY;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
						
					} else {
						GuiVar_var_rectangle_color = LIGHT_GREY;	
					}
				} else {
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREY) {
									GuiVar_var_rectangle_color = LIGHT_RED;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREY;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
				}
			break;
			
			case DISABLEMENTS_SCREEN:
			case DEV_CTRL_SCREEN:
			case DISABLEMENTS_LIST_SCREEN:
			case DEV_CTRL_LIST_SCREEN:
			case FAULTS_SCREEN:
				if (alarms_number == 0){
					if ((faults_number != 0) || (disablements_number != 0) || (dev_ctrl_number != 0) ){
						
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREY) {
									GuiVar_var_rectangle_color = LIGHT_YELLOW;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREY;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
						
					} else {
						GuiVar_var_rectangle_color = LIGHT_GREY;	
					}
				} else {
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREY) {
									GuiVar_var_rectangle_color = DARK_RED;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREY;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
				}
			break;

			case SYSTEM_SCREEN:
			case INFO_SCREEN:
			case ZONE_X_SCREEN:
				if (alarms_number == 0){
					if ((faults_number != 0) || (disablements_number != 0) || (dev_ctrl_number != 0) ){
						
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == WHITE) {
									GuiVar_var_rectangle_color = DARK_YELLOW;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = WHITE;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
						
					} else {
						GuiVar_var_rectangle_color = WHITE;	
					}
				} else {
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == WHITE) {
									GuiVar_var_rectangle_color = DARK_RED;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = WHITE;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
				}			
			break;

			case SET_SCREEN:
			case TH_SCREEN:
			case OPTIONS_SCREEN:
			case LOG_SCREEN:
				if (alarms_number == 0){
					if ((faults_number != 0) || (disablements_number != 0) || (dev_ctrl_number != 0) ){
						
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREEN) {
									GuiVar_var_rectangle_color = DARK_YELLOW;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREEN;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
						
					} else {
						GuiVar_var_rectangle_color = LIGHT_GREEN;	
					}
				} else {
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREEN) {
									GuiVar_var_rectangle_color = DARK_RED;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREEN;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
				}			
			break;

			case EVENTS_SCREEN:
				if (alarms_number == 0){
					if ((faults_number != 0) || (disablements_number != 0) || (dev_ctrl_number != 0) ){
						
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == DEEP_PURPLE) {
									GuiVar_var_rectangle_color = DARK_YELLOW;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = DEEP_PURPLE;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
						
					} else {
						GuiVar_var_rectangle_color = DEEP_PURPLE;	
					}
				} else {
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == DEEP_PURPLE) {
									GuiVar_var_rectangle_color = DARK_RED;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = DEEP_PURPLE;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
				}			
			break;
				
			default:
				if (alarms_number == 0){
					if ((faults_number != 0) || (disablements_number != 0) || (dev_ctrl_number != 0) ){
						
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREY) {
									GuiVar_var_rectangle_color = DARK_YELLOW;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREY;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
						
					} else {
						GuiVar_var_rectangle_color = LIGHT_GREY;	
					}
				} else {
						if (alarm_timer_count == 0) {
								if (GuiVar_var_rectangle_color == LIGHT_GREY) {
									GuiVar_var_rectangle_color = DARK_RED;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								} else {
									GuiVar_var_rectangle_color = LIGHT_GREY;
									alarm_timer_count = RECTANGLE_BLINK_TIME;
								}
						}	
				}			
			break;
		}	
	}
}

 /***********************************************************************
 * @brief		zone_objects_count_disablement			
	 \param [in] zone: zona della quale devo controllare
	 \param [in] tipo di cambiamento (abilitazione o disabilitazione)
	 indica se tutti gli oggetti appartenenti ad una zona sono selezionati
 **********************************************************************/
unsigned int all_zone_objects_selected (unsigned int zone, unsigned int type) { 
	unsigned int i = 0;
	unsigned int count = 0;
	unsigned int result  = 0;
	
	switch (type)
	{
		case CHANGE_DISABLEMENT:
			for (i=0; i< NUMBER_OF_OBJECTS; i++) {
				if ((objects_array[i].zone == zone) && (disablements_change[i] == 0 ) ) {
					count++;
				}
			}
			if (count == 0) {
				result = 1;
			} else {
				result = 0;			
			}
		break;
		
		case CHANGE_DEV_CTRL:
			for (i=0; i< NUMBER_OF_OBJECTS; i++) {
				if ((objects_array[i].zone == zone) && (dev_ctrl_change[i] == 0 ) ) {
					count++;
				}
			}
			if (count == 0) {
				result = 1;
			} else {
				result = 0;			
			}			
		break;
		
	}

	 return(result);
}

 /***********************************************************************
 * @brief		add_alarm_event			
	 \param struttura (temporanea) relativa al nuovo evento
	 aggiunge un evento "allarme" alla lista degli allarmi
 **********************************************************************/
void add_alarm_event (struct Event Temp) { 
	
	unsigned int i = 0;
	
	// fai spazio per il nuovo arrivato; butta via il pi� vecchio
	for (i=1; i< (ALARMS_ARRAY_DIMENSIONS); i++) {
		
		// copia tutti i campi
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].float_value_1 = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].float_value_1;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].float_value_2 = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].float_value_2;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].id_1          = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].id_1;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].id_2          = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].id_2;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].specific_type = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].specific_type;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].time          = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].time;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].type          = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].type;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].user_level    = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].user_level;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].user_id       = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].user_id;
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].in_out        = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].in_out;	
		alarms_array[(ALARMS_ARRAY_DIMENSIONS - i)].object_type   = alarms_array[(ALARMS_ARRAY_DIMENSIONS - i - 1)].object_type;	
	}	

	// ECCO QUELLO NUOVO
	alarms_array[0].float_value_1 = Temp.float_value_1;
	alarms_array[0].float_value_2 = Temp.float_value_2;
	alarms_array[0].id_1          = Temp.id_1;
	alarms_array[0].id_2          = Temp.id_2;
	alarms_array[0].specific_type = Temp.specific_type;
	alarms_array[0].time          = Temp.time;
	alarms_array[0].type          = Temp.type;
	alarms_array[0].user_level    = Temp.user_level;
	alarms_array[0].user_id       = Temp.user_id;
	alarms_array[0].in_out        = Temp.in_out;
	alarms_array[0].object_type   = Temp.object_type;
	
	if (alarms_array_dim < ALARMS_ARRAY_DIMENSIONS){
		alarms_array_dim++;
	}

}

 /***********************************************************************
 * @brief		reset_alarms			
	resetta gli allarmi (numero di allarmi diventa 0)
 **********************************************************************/
void reset_alarms (void) { 
	unsigned int i = 0;
	
	alarms_number = 0;
	
	// Il sistema non � pi� in allarme
	system_in_alarm = 0;
	
	// Le zone non sono pi� in allarme
	for (i=0; i< (sys_zones_num + 1); i++) {
		zones_array[i].in_alarm = 0;
	}
	
	// I sensori non sono pi� in allarme
	for (i=0; i< (sys_sensors_num + 1); i++) {
		sensors_array[i].in_alarm      = 0;
		sensors_array[i].in_alarm_low  = 0;
		sensors_array[i].in_alarm_fake = 0;
	}	

	// Crea il relativo evento
	temp_event.time          = current_time;
	temp_event.type          = E_TYPE_MISC;
	temp_event.specific_type = E_S_TYPE_RESET_ALARMS;
	temp_event.user_level    = current_user.level;
	temp_event.user_id       = current_user.id;
	add_event(temp_event);
	
	// Spegni i lampeggianti
	for(i=0; i< sys_lamps_num; i++){
		lamps_array[i].on = 0;
	}	
	
	// Spegni le sirene
	for(i=0; i< sys_sirens_num; i++){
		sirens_array[i].on = 0;
	}	
}

 /***********************************************************************
 * @brief		add_fault			
	 \param struttura (temporanea) relativa al nuovo guasto
	 aggiunge un evento alla lista degli eventi
 **********************************************************************/
void add_fault (struct Fault Temp) { 
	
	unsigned int i = 0;
	
	// fai spazio per il nuovo arrivato; butta via il pi� vecchio
	for (i=1; i< (FAULTS_ARRAY_DIMENSIONS); i++) {
		
		// copia tutti i campi
		faults_array[(FAULTS_ARRAY_DIMENSIONS - i)].type          = faults_array[(FAULTS_ARRAY_DIMENSIONS - i - 1)].type;
		faults_array[(FAULTS_ARRAY_DIMENSIONS - i)].specific_type = faults_array[(FAULTS_ARRAY_DIMENSIONS - i - 1)].specific_type;
		faults_array[(FAULTS_ARRAY_DIMENSIONS - i)].id            = faults_array[(FAULTS_ARRAY_DIMENSIONS - i - 1)].id;
		faults_array[(FAULTS_ARRAY_DIMENSIONS - i)].zone          = faults_array[(FAULTS_ARRAY_DIMENSIONS - i - 1)].zone;
		faults_array[(FAULTS_ARRAY_DIMENSIONS - i)].digIN          = faults_array[(FAULTS_ARRAY_DIMENSIONS - i - 1)].digIN;
		faults_array[(FAULTS_ARRAY_DIMENSIONS - i)].OUTput          = faults_array[(FAULTS_ARRAY_DIMENSIONS - i - 1)].OUTput;
		

	}	

	// ECCO QUELLO NUOVO
	faults_array[0].zone          = Temp.zone;
	faults_array[0].id            = Temp.id;
	faults_array[0].specific_type = Temp.specific_type;
	faults_array[0].type          = Temp.type;
	faults_array[0].digIN         = Temp.digIN;
	faults_array[0].OUTput        = Temp.OUTput;
	

	
	
	if (faults_array_dim < FAULTS_ARRAY_DIMENSIONS){
		faults_array_dim++;
	}
	faults_number++;
	}
//fine D'Ippolito
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
 /***********************************************************************
 * @brief		add_object			
	 \param struttura (temporanea) relativa al nuovo oggetto
	 \param type: DISABLEMENTS or ACTIVATIONS
	 aggiunge un oggetto alla lista degli oggetti disabilitazioni o attivazioni
 **********************************************************************/
void add_object (struct Object Temp, unsigned int type) { 
	
	unsigned int i = 0;
	
	switch (type)
	{
		case O_DISABLEMENTS:
			// fai spazio per il nuovo arrivato; butta via il pi� vecchio
			for (i=1; i< (DISABLEMENTS_ARRAY_DIMENSIONS); i++) {
				
				// copia tutti i campi
				disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i)].type          = disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i - 1)].type;
				disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i)].specific_type = disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i - 1)].specific_type;
				disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i)].id            = disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i - 1)].id;
				disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i)].zone          = disablements_array[(DISABLEMENTS_ARRAY_DIMENSIONS - i - 1)].zone;
			}	

			// ECCO QUELLO NUOVO
			disablements_array[0].zone          = Temp.zone;
			disablements_array[0].id            = Temp.id;
			disablements_array[0].specific_type = Temp.specific_type;
			disablements_array[0].type          = Temp.type;
			
			if (disablements_array_dim < DISABLEMENTS_ARRAY_DIMENSIONS){
				disablements_array_dim++;
			}
		break;
		
		case O_DEV_CTRL:
			// fai spazio per il nuovo arrivato; butta via il pi� vecchio
			for (i=1; i< (DEV_CTRL_ARRAY_DIMENSIONS); i++) {
				
				// copia tutti i campi
				dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i)].type          = dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i - 1)].type;
				dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i)].specific_type = dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i - 1)].specific_type;
				dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i)].id            = dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i - 1)].id;
				dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i)].zone          = dev_ctrl_array[(DEV_CTRL_ARRAY_DIMENSIONS - i - 1)].zone;
			}	

			// ECCO QUELLO NUOVO
			dev_ctrl_array[0].zone          = Temp.zone;
			dev_ctrl_array[0].id            = Temp.id;
			dev_ctrl_array[0].specific_type = Temp.specific_type;
			dev_ctrl_array[0].type          = Temp.type;
			
			if (dev_ctrl_array_dim < DEV_CTRL_ARRAY_DIMENSIONS){
				dev_ctrl_array_dim++;
			}
		break;
	}
	
}

 /***********************************************************************
 * @brief		look_for_item	
	 \param [in] in quale array cerco l'elemento ?: SORTED_BY_ELEMENTS, SORTED_BY_ZONES
 **********************************************************************/
unsigned int look_for_item (struct Object item, unsigned int array) {
	
	unsigned int i   = 0;
	unsigned int pos = 0;
	
	switch (array)
	{
		case SORTED_BY_ELEMENTS:
			for (i=0; i<NUMBER_OF_OBJECTS; i++) {
				if ( (item.id            == sorted_objects_array_2[i].id)            &&
						 (item.type          == sorted_objects_array_2[i].type )         &&
						 (item.specific_type == sorted_objects_array_2[i].specific_type) &&
						 (item.zone          == sorted_objects_array_2[i].zone) 
					 ){
				 pos = i;
				}	
			}			
		break;
		
		case SORTED_BY_ZONES:
			for (i=0; i<NUMBER_OF_OBJECTS; i++) {
				if ( (item.id            == sorted_objects_array[i].id)            &&
						 (item.type          == sorted_objects_array[i].type )         &&
						 (item.specific_type == sorted_objects_array[i].specific_type) &&
						 (item.zone          == sorted_objects_array[i].zone) 
					 ){
				 pos = i;
				}	
			}
		break;
	}
	return(pos);
}

 /***********************************************************************
 * @brief		switch_array_order	
	 \param [in] Tipo di operazione: SORT_BY_ZONES o SORT_BY_ELEMENTS
	 \param [in] Schermata dove devo ordinare: DISABLEMENTS_LIST_SCREEN o DEV_CTRL_SCREEN
 **********************************************************************/
void switch_array_order (unsigned int operation, unsigned int screen){

	unsigned int i = 0;
	char temp_array[NUMBER_OF_OBJECTS];
	unsigned int pos = 0;
	
	if (screen == DEV_CTRL_LIST_SCREEN){
	/* DEV_CTRL LIST SCREEN */	
		
			switch (operation)
			{
				case SORT_BY_ZONES:
					// Devo mettere in ordine di zone dev_ctrl_before_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array_2[i], SORTED_BY_ZONES);
						temp_array[pos] = dev_ctrl_before_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						dev_ctrl_before_change[i] = temp_array[i];
					}			
					// Devo mettere in ordine di zone dev_ctrl_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array_2[i], SORTED_BY_ZONES);
						temp_array[pos] = dev_ctrl_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						dev_ctrl_change[i] = temp_array[i];
					}							
				break;
				
				case SORT_BY_ELEMENTS:
					// Devo mettere in ordine di elementi dev_ctrl_before_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array[i], SORTED_BY_ELEMENTS);
						temp_array[pos] = dev_ctrl_before_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						dev_ctrl_before_change[i] = temp_array[i];
					}			
					// Devo mettere in ordine di zone dev_ctrl_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array[i], SORTED_BY_ELEMENTS);
						temp_array[pos] = dev_ctrl_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						dev_ctrl_change[i] = temp_array[i];
					}				
				break;
			}
	} else {
	/* DISABLEMENTS LIST SCREEN */	
			switch (operation)
			{
				case SORT_BY_ZONES:
					// Devo mettere in ordine di zone dev_ctrl_before_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array_2[i], SORTED_BY_ZONES);
						temp_array[pos] = disablements_before_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						disablements_before_change[i] = temp_array[i];
					}			
					// Devo mettere in ordine di zone dev_ctrl_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array_2[i], SORTED_BY_ZONES);
						temp_array[pos] = disablements_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						disablements_change[i] = temp_array[i];
					}							
				break;
				
				case SORT_BY_ELEMENTS:
					// Devo mettere in ordine di elementi disablements_before_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array[i], SORTED_BY_ELEMENTS);
						temp_array[pos] = disablements_before_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						disablements_before_change[i] = temp_array[i];
					}			
					// Devo mettere in ordine di zone disablements_change
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						pos = look_for_item(sorted_objects_array[i], SORTED_BY_ELEMENTS);
						temp_array[pos] = disablements_change[i];
					}
					for (i=0; i<NUMBER_OF_OBJECTS; i++) {
						disablements_change[i] = temp_array[i];
					}				
				break;
			}		
	}
}



 /***********************************************************************
 * @brief		init_screen			
	 inizializza le varie schermate
	 NB.! chiamare la funzione "GuiLib_ShowScreen" quando si cambia schermata
				� cosa buona e giusta. Pi� che altro bisogna farlo ogni tanto 
				altrimenti le rilevazione delle aree attiva sballa un po' !
 **********************************************************************/
void init_screen (unsigned int type) { 
	
	unsigned int i = 0;
	
	GuiVar_message = 0;
	
	switch (type)
	{
		case 0: // System
			GuiVar_default_line_1_color = DARK_BLUE;
			GuiVar_default_line_2_color = DARK_BLUE;
			GuiVar_default_line_3_color = DARK_BLUE;
			index_system = 1;
			GuiVar_var_rectangle_color = WHITE;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 1: // SET
//			GuiVar_checkbox_set_all = 1;							
			GuiVar_checkbox_set_all = 0;							
			for (i=1; i < (sys_zones_num + 1); i++) {
				zones_change_set[i] = 0;
			}
//			GuiVar_set_set     = 15;
//			GuiVar_set_hyst    = 0.1;	
			GuiVar_set_set     = zones_array[1].set;
			GuiVar_set_hyst    = zones_array[1].hyst;	
			
			GuiVar_set_zone_id = 1;
			
			GuiVar_set_text_color  = DARK_BLUE;
			GuiVar_hyst_text_color = DARK_BLUE;
			GuiVar_var_rectangle_color = LIGHT_GREEN;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;

		case 2: // CALIBRATION SCREEN
  		if (current_user.level>1){
				GuiLib_ShowScreen(GuiStruct_calibration_Def,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);   
				GuiVar_selected_bottle=1;
				GuiVar_O2_bottle = E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS];
        if (!(Adox1500.Stato & ADOX_TARATURA)){
          Adox1500.Stato |= ADOX_TARATURA;
          SetMioRele(OFF, 0, 0, OUT_VTARATURA); // chiude valvola per taratura
				}
			}
		break;
		
		case 3: // LOG
			// Azzera la password
			GuiVar_pwd_char_1 = 0;
			GuiVar_pwd_char_2 = 0;
			GuiVar_pwd_char_3 = 0;	
			pswd_index = 0;
			for (i=0; i < 4; i++) {
					password[i] = 0;
			}		
			GuiVar_var_rectangle_color = LIGHT_GREEN;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 4: // OPTIONS
			// Init. displayed variables
			GuiVar_set_day     = GuiVar_day;
			GuiVar_set_month   = GuiVar_month;
			GuiVar_set_year    = GuiVar_year;
			GuiVar_set_hour    = GuiVar_hour;
			GuiVar_set_minutes = GuiVar_minutes;
		
			GuiVar_set_backlight = backlight;	

			GuiVar_day_color     = DARK_BLUE;
			GuiVar_month_color   = DARK_BLUE;
			GuiVar_year_color    = DARK_BLUE;
			GuiVar_hour_color    = DARK_BLUE;
			GuiVar_minutes_color = DARK_BLUE;	

			if (events_array_dim == 0){
				GuiVar_del_events_button = 2;
			}	else {
				GuiVar_del_events_button = 0;
			}
			GuiVar_var_rectangle_color = LIGHT_GREEN;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 5: // EVENTS
			index_events = 0;
			GuiVar_var_rectangle_color = DEEP_PURPLE;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 6: // INFO
			GuiVar_var_rectangle_color = WHITE;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 7: // ALARMS
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 8: // FAULTS
			index_faults = 0;
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 9: // DISABLEMENTS
			index_disablements = 0;
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 10: // DEV CTRL
			index_dev_ctrl = 0;
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 11: // ALARMS LIST
			index_alarms = 0;
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 12: // ZONE_X
			// vedi i 3 bottoni delle zone, poich� dipende dalla zona appunto, era pi� comodo l�
			// sono le aree 26, 27 e 28
		break;
		
		case 13: // DEV_CTRL LIST
			index_objects = 0;
			GuiVar_radio_flux_1 = 1;
			GuiVar_radio_flux_2 = 1;
			GuiVar_radio_flux_3 = 1;
			GuiVar_radio_flux_4 = 1;
			GuiVar_radio_flux_5 = 1;
			for(i=0; i<sys_fluxes_num; i++){
				fluxes_array[i].tested = 0;
			}
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
		
		case 14: // DISABLEMENTS LIST
			index_objects = 0;
			GuiVar_var_rectangle_color = LIGHT_GREY;
			GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
		break;
	}	
}	

 /***********************************************************************
 * @brief		handle_messages	
	 gestisce i messaggi (pi� che altro i tempi... li nasconde dopo 1 secondo per esempio)
 **********************************************************************/
void handle_messages (void) { 
	if ( (message_timer_count == 0) && ( 
                                    	 ( GuiVar_message == 1) ||
																			 ( GuiVar_message == 2) ||
																			 ( GuiVar_message == 4) ||
																			 ( GuiVar_message == 6) ||
	                                     ( GuiVar_message == 8) ||
																			 ( GuiVar_message == 11) ||
	                                     ( GuiVar_message == 12) ||
	                                     ( GuiVar_message == 13)
																		)   ){
																				 
    if (GuiVar_message==13)
      reset_after_new_configuration_load = 0xAB;			
																				 														 
		GuiVar_message = 0;
																			
//		if (alarms_number != 0) {
//			if (GuiVar_screen_select != CHARTS_SCREEN) {
//				GuiVar_screen_select = CHARTS_SCREEN;
//				init_screen(CHARTS_SCREEN);
//				press_button(7);
//			}
//		} else {
			if (GuiVar_screen_select != SYSTEM_SCREEN) {
				GuiVar_screen_select = SYSTEM_SCREEN;
				init_screen(SYSTEM_SCREEN);
				press_button(0);
			}
//		}

	}
}
char tabella[LEN4][LEN3];
/******************************************************************************************************
* @brief D'Ippolito.
riempie la matrice 21*12 per scrittura
*****************************************************************************************************/
void scrittura_tabella(){
	int j=0, i=0;
	
	for(i=0;i<LEN4;i++){
		for(j=0;j<LEN3;j++){
			tabella[i][j]=0;
		}
	}
	tabella[0][0]= 's';
	tabella[0][1]= 'e';
	tabella[0][2]= 'n';
	tabella[0][3]= 's';
	tabella[0][4]= 'o';
	tabella[0][5]= 'r';
	
	tabella[1][0]= 'd';
	tabella[1][1]= 'i';
	tabella[1][2]= 's';
	tabella[1][3]= 'p';
	tabella[1][4]= 'l';
	tabella[1][5]= 'a';
	tabella[1][6]= 'y';
	
	tabella[2][0]= 'a';
	tabella[2][1]= 'n';
	tabella[2][2]= 'y';
	tabella[2][3]= 'b';
	tabella[2][4]= 'u';
	tabella[2][5]= 's';
	
	tabella[3][0]= 'a';
	tabella[3][1]= 'd';
	tabella[3][2]= 'o';
	tabella[3][3]= 'x';
	
	tabella[4][0]= 'p';
	tabella[4][1]= 's';
	tabella[4][2]= 'a';
	
	tabella[5][0]= 'v';
	tabella[5][1]= 'a';
	tabella[5][2]= 'l';
	tabella[5][3]= 'v';
	tabella[5][4]= 'e';
	
	tabella[6][0]= 'f';
	tabella[6][1]= 'l';
	tabella[6][2]= 'o';
	tabella[6][3]= 'w';
	tabella[6][4]= 'm';
	tabella[6][5]= 'e';
	tabella[6][6]= 't';
	tabella[6][7]= 'e';
	tabella[6][8]= 'r';
	
	tabella[7][0]= 's';
	tabella[7][1]= 'i';
	tabella[7][2]= 'r';
	tabella[7][3]= 'e';
	tabella[7][4]= 'n';
	
	tabella[8][0]= 'm';
	tabella[8][1]= 'a';
	tabella[8][2]= 'i';
	tabella[8][3]= 'n';
	tabella[8][4]= ' ';
	tabella[8][5]= 's';
	tabella[8][6]= 'i';
	tabella[8][7]= 'r';
	tabella[8][8]= 'e';
	tabella[8][9]= 'n';
	
	tabella[9][0]= 'l';
	tabella[9][1]= 'i';
	tabella[9][2]= 'g';
	tabella[9][3]= 'h';
	tabella[9][4]= 't';
	
	tabella[10][0]= 'd';
	tabella[10][1]= 'o';
	tabella[10][2]= 'o';
	tabella[10][3]= 'r';
	
	tabella[11][0]= 'f';
	tabella[11][1]= 'a';
	tabella[11][2]= 'n';
	
	tabella[12][0]= 'g';
	tabella[12][1]= 'e';
	tabella[12][2]= 'n';
	tabella[12][3]= '.';
	tabella[12][4]= 'a';
	tabella[12][5]= 'l';
	tabella[12][6]= 'a';
	tabella[12][7]= 'r';
	tabella[12][8]= 'm';
	tabella[12][9]= ' ';
	tabella[12][10]= 'i';
	tabella[12][11]= 'n';
	
	tabella[13][0]= 'c';
	tabella[13][1]= 'c';
	tabella[13][2]= ' ';
	tabella[13][3]= 'g';
	tabella[13][4]= 'e';
	tabella[13][5]= 'n';
	
	tabella[14][0]= 'a';
	tabella[14][1]= 'l';
	tabella[14][2]= 'm';
	tabella[14][3]= ' ';
	tabella[14][4]= 'g';
	tabella[14][5]= 'e';
	tabella[14][6]= 'n';
	
  tabella[15][0]= 'p';
	tabella[15][1]= 'a';
	tabella[15][2]= 'n';
	tabella[15][3]= 'e';
	tabella[15][4]= 'l'; 
	
	tabella[16][0]= 'p';
	tabella[16][1]= 'o';
	tabella[16][2]= 'w';
	tabella[16][3]= 'e';
	tabella[16][4]= 'r'; 
	
	tabella[17][0]= 'b';
	tabella[17][1]= 'a';
	tabella[17][2]= 't';
	tabella[17][3]= 't';
	tabella[17][4]= 'e';
	tabella[17][5]= 'r';
	tabella[17][6]= 'y';
	
	tabella[18][0]= 'l';
	tabella[18][1]= 'e';
	tabella[18][2]= 'd';
	
	tabella[19][0]= 's';
	tabella[19][1]= 'w';
	tabella[19][2]= 'i';
	tabella[19][3]= 't';
	tabella[19][4]= 'c';
	tabella[19][5]= 'h';
	tabella[19][6]= 'e';
	tabella[19][7]= 's';
	
	tabella[20][0]= 't';
	tabella[20][1]= 'h';
	tabella[20][2]= 'e';
	tabella[20][3]= 'r';
	tabella[20][4]= 'm';
	tabella[20][5]= 'o';
	tabella[20][6]= 's';
	tabella[20][7]= 't';
	tabella[20][8]= 'a';
	tabella[20][9]= 't';
	
	
	for(i=0;i<21;i++){
		GuiVar_gps_item_type_name[i]=tabella[GuiVar_gps_item_type][i];
	}
}
 /***********************************************************************
 * @brief		handle_users	
	 gestisce gli utenti e le loro abilitazioni
	 disconnette automaticamente dopo 2 minuti
 **********************************************************************/
void handle_users (void) { 
	
	/* ok, adesso abilita o disabilita i bottoni in funzione del tipo di autenticazione */
	// main screen
	// IF LEVEL 1 USER
	if (current_user.level < 2) {
			//GuiVar_button_events  = 2;
			//GuiVar_button_options = 2;
//			GuiVar_no_buzzer_button 	 = 2;
//			GuiVar_no_siren_button  	 = 2;
			GuiVar_reset_alarms_button = 2;		
	}
	
	switch (GuiVar_screen_select)
	{
		case SET_SCREEN:
			if (current_user.level < 3) {
				GuiVar_set_dec_set  = 2;
				GuiVar_set_inc_set  = 2;
				GuiVar_set_dec_hyst = 2;
				GuiVar_set_inc_hyst = 2;
			}
			
//			if (current_user.level < 3) {
//				GuiVar_button_save  = 2;
//			}	else {
				if (GuiVar_button_save != 1) {
					GuiVar_button_save = 0;
				}
//			}	
		break;
		
		case TH_SCREEN:
			if (current_user.level < 3) {
				GuiVar_th_dec_high  = 2;
				GuiVar_th_inc_high  = 2;
				GuiVar_th_dec_low   = 2;
				GuiVar_th_inc_low   = 2;		
			}	

			if (current_user.level < 3) {
				GuiVar_button_save  = 2;
			}	else {
				if (GuiVar_button_save != 1) {
					GuiVar_button_save = 0;
				}
			}
		break;
		
		case LOG_SCREEN:
			// Se sei gi� autenticato, non puoi mettere un'altra password
//			if ( (current_user.level == 2) || (current_user.level == 3) ){
//				GuiVar_keyboard_1 = 2;
//				GuiVar_keyboard_2 = 2;
//				GuiVar_keyboard_3 = 2;
//				GuiVar_keyboard_4 = 2;
//				GuiVar_keyboard_5 = 2;
//				GuiVar_keyboard_6 = 2;
//				GuiVar_keyboard_7 = 2;
//				GuiVar_keyboard_8 = 2;
//				GuiVar_keyboard_9 = 2;
//				GuiVar_keyboard_0 = 2;
//			}
		break;

		case OPTIONS_SCREEN:
//			if (current_user.level < 2) {
//				GuiVar_button_save  = 2;
//			}	else {
				if (GuiVar_button_save != 1) {
					GuiVar_button_save = 0;
				}
//			}
		break;
			
		case CHARTS_SCREEN:
			// abilitazione bottone per cancellare lista allarmi
			if ( current_user.level < 3 ) {
				GuiVar_del_events_button = 2;	
			}
		break;
			
		case DISABLEMENTS_SCREEN:
			if ( current_user.level < 2 ) {
				GuiVar_config_button = 2;	
			} else {
				if (GuiVar_config_button != 1) {
					GuiVar_config_button = 0;
				}
			}					
		break;
			
		case DEV_CTRL_SCREEN:
			if ( current_user.level < 2 ) {//Antonio era 3
				GuiVar_config_button = 2;	
			}					

			if ( current_user.level < 3 ) {
				GuiVar_output_off = 2;	
			} else {
				if (GuiVar_output_off != 1) {
					GuiVar_output_off = 0;
				}
			}					
		break;
			
		case DISABLEMENTS_LIST_SCREEN:
			if (current_user.level < 2) {
				GuiVar_button_save  = 2;
			}	else {
				if (GuiVar_button_save != 1) {
					GuiVar_button_save = 0;
				}
			}			
		break;
			
		case DEV_CTRL_LIST_SCREEN:
			if (current_user.level < 2) {//Antonio era 3
				GuiVar_button_save  = 2;
			}	else {
				if (GuiVar_button_save != 1) {
					GuiVar_button_save = 0;
				}
			}				
		break;

		case ALARMS_LIST_SCREEN:
			if (current_user.level < 3) {
				GuiVar_del_events_button = 2;	
			}	else {
				if (( alarms_array_dim == 0 ) || ( alarms_number != 0 ) ) {
					GuiVar_del_events_button = 2;	
				} else {
					if (GuiVar_del_events_button != 1) {
						GuiVar_del_events_button = 0;
					}					
				}	
			}				
		break;					
	}
	
	/* ********************************************************************* */
	// Controlla se � ora di auto disconnettersi
	if  ((auto_logout == 0) && (current_user.level != 0 ) ) {

		GuiVar_message = 2;
		message_timer_count = MESSAGE_ON_SCREEN_TIME;
		
		GuiVar_keyboard_1 = 0;
		GuiVar_keyboard_2 = 0;
		GuiVar_keyboard_3 = 0;
		GuiVar_keyboard_4 = 0;
		GuiVar_keyboard_5 = 0;
		GuiVar_keyboard_6 = 0;
		GuiVar_keyboard_7 = 0;
		GuiVar_keyboard_8 = 0;
		GuiVar_keyboard_9 = 0;
		GuiVar_keyboard_0 = 0;
		
		GuiVar_logout_button = 2;
		GuiVar_reset_values  = 0;
		
		temp_event.time          = current_time;
		temp_event.type          = E_TYPE_MISC;
		temp_event.specific_type = E_S_TYPE_LOGOUT;
		temp_event.user_level    = current_user.level;
		temp_event.user_id       = current_user.id;
		add_event(temp_event);	
		
		current_user.level = 0;
		current_user.id   = 0;

		if (alarms_number != 0) {
			if (GuiVar_screen_select != CHARTS_SCREEN) {
				GuiVar_screen_select = CHARTS_SCREEN;
				init_screen(CHARTS_SCREEN);
				press_button(7);
			}
		} else {
			if (GuiVar_screen_select != SYSTEM_SCREEN) {
				GuiVar_screen_select = SYSTEM_SCREEN;
				init_screen(SYSTEM_SCREEN);
				press_button(0);
			}
		}
		
		
	}
	
}

 /***********************************************************************
 * @brief		handle_backlight	
	 gestisce la retroilluminazione
 **********************************************************************/
void handle_backlight (void) { 

	// la retroilluminazione deve sempre esere quella impostata dall'utente
	// anche se si smanetta nella schermata delle opzioni
	// non se si � nella schermata "idle"
	if ( (backlight != last_loaded_backlight) && (GuiVar_screen_select != 4) && (GuiLib_CurStructureNdx != GuiStruct_Idle_0)) {
		ea_lcdb_ctrl_backlightContrast(backlight);
		last_loaded_backlight = backlight;
// 20201005
		PWM_MatchUpdate(_USING_PWM_NO, 1, 20-last_loaded_backlight, PWM_MATCH_UPDATE_NOW);    
    
	}

// 20201005
//#if 0  
      // condizione di mancanza alimentazione rete (vado a batteria) per ridurre al minimo il consumo riduco la retro illuminazione
      // alternandola tra 0 e 20. Se l'utente preme ritorna al valore impostato per un minuto
    #ifdef REDUNDANCE
     #ifdef	MASTER_CIE 
      if (no_network_array[0].on==1) //0 per CIE Master, 1 per CIE Slave
     #else
      if (no_network_array[1].on==1) //0 per CIE Master, 1 per CIE Slave
     #endif
    #else
      if (no_network_array[0].on==1) //0 per CIE Master
    #endif
      {
        resume_backlight = 1;
        if (tmr_user_press_button)
          ea_lcdb_ctrl_backlightContrast(backlight);
        else
        {
          if (tmrLowBacklight<100)
          {
            ea_lcdb_ctrl_backlightContrast(20);
          } 
          else
          {
            ea_lcdb_ctrl_backlightContrast(0);
          }
          if (tmrLowBacklight==0)
            tmrLowBacklight = TMR_LOW_BACKLIGHT;
        }
      }
      else if (resume_backlight==1)
      {
        resume_backlight = 0;
        ea_lcdb_ctrl_backlightContrast(backlight);
      }
// #endif      
}


 /***********************************************************************
 * @brief		no_sensors_selected	
	 \param [in] Tipo di operazione:
		CHANGE_TH: selezionati per cambiarne le soglie di allarme alte e basse
	 determina se nessuna sensore � stato selezionato per fare qualche operazione
 **********************************************************************/
unsigned int no_sensors_selected (unsigned int type) { 

	unsigned int no_sensors_selected = 1;
	unsigned int i = 0;
	
	// Controlla se tutte le zone sono selezionate per cambiare set
	if (type == CHANGE_TH) {
		for (i= 1; i < (sys_sensors_num + 1); i++) {
			if (sensors_change_th[i] == 1) {
				no_sensors_selected = 0;
			}
		}
	}
	
	return(no_sensors_selected);
}

 /***********************************************************************
 * @brief		no_zones_selected	
	 \param [in] Tipo di operazione:
		CHANGE_SET: selezionati per cambiarne il set
	 determina se nessuna zona � stata selezionata per fare qualche operazione
 **********************************************************************/
unsigned int no_zones_selected (unsigned int type) { 

	unsigned int no_zones_selected = 1;
	unsigned int i = 0;
	
	// Controlla se tutte le zone sono selezionate per cambiare set
	if (type == CHANGE_SET) {
		for (i= 1; i < (sys_zones_num + 1); i++) {
			if (zones_change_set[i] == 1) {
				no_zones_selected = 0;
			}
		}
	}
	
	return(no_zones_selected);
}

 /***********************************************************************
 * @brief		eeprom_write_byte			
	 scrive un byte nella EEPROM; � divisa in 63 pagine, ognuna con 64 byte
	 \param [in] page: pagine (0 - 62)
	 \param [in] offset: (0 - 63)
	 \param [in] data (byte)
 **********************************************************************/
void eeprom_write_byte (unsigned int page, unsigned int offset, unsigned int data) {
	write_buffer[0] = data;
	EEPROM_Write(offset, page, (void*)write_buffer,MODE_8_BIT, 1);
}

 /***********************************************************************
 * @brief		eeprom_read_byte			
	 legge un byte dalla EEPROM; � divisa in 63 pagine, ognuna con 64 byte
	 \param [in] page: pagine (0 - 62)
	 \param [in] offset: (0 - 63)
 **********************************************************************/
unsigned int  eeprom_read_byte (unsigned int page, unsigned int offset) {
	EEPROM_Read(offset, page,  (void*)read_buffer,MODE_8_BIT,1);
	return(read_buffer[0]);
}

 /***********************************************************************
 * @brief		eeprom_write_word			
	 scrive una word nella EEPROM; � divisa in 63 pagine, ognuna con 64 byte
	 \param [in] page: pagine (0 - 62)
	 \param [in] offset: (0 - 63)
	 \param [in] data (word)
 **********************************************************************/
void eeprom_write_word (unsigned int page, unsigned int offset, unsigned int data) {
	
	// NB: write_buffer � fatto di bytes !!!
	unsigned int temp = 0;
	
	write_buffer[1] = (data & 0x00FF);
	temp = (data & 0xFF00);
	write_buffer[0] = (temp >> 8);
	EEPROM_Write(offset, page, (void*)write_buffer,MODE_8_BIT, 2);
}

 /***********************************************************************
 * @brief		eeprom_read_word			
	 legge una word dalla EEPROM; � divisa in 63 pagine, ognuna con 64 byte
	 \param [in] page: pagine (0 - 62)
	 \param [in] offset: (0 - 63)
 **********************************************************************/
unsigned int  eeprom_read_word (unsigned int page, unsigned int offset) {
	EEPROM_Read(offset, page,  (void*)read_buffer,MODE_8_BIT,2);
	return( (read_buffer[0] << 8) + read_buffer[1] );
}






 /***********************************************************************
 * @brief		rtc_get_time			
	 legge la data e l'ora dall'RTC e aggiorna le variabili di easyGUI (ma serve il refresh...)
 **********************************************************************/
void rtc_get_time (void) { 
	RTC_GetFullTime (LPC_RTC, &RTCFullTime);
	GuiVar_year    = RTCFullTime.YEAR;
	GuiVar_month   = RTCFullTime.MONTH;
	GuiVar_day     = RTCFullTime.DOM;
	GuiVar_hour    = RTCFullTime.HOUR;
	GuiVar_minutes = RTCFullTime.MIN;
	GuiVar_seconds = RTCFullTime.SEC;	

	current_time.year    = RTCFullTime.YEAR;
	current_time.month   = RTCFullTime.MONTH;
	current_time.day     = RTCFullTime.DOM;
	current_time.hour    = RTCFullTime.HOUR;
	current_time.minutes = RTCFullTime.MIN;
	current_time.seconds = RTCFullTime.SEC;		
	
}

 /***********************************************************************
 * @brief		rtc_set_time			
	 imposta la data e l'ora dell'RTC
	 \param [in] year
	 \param [in] month
	 \param [in] dom (day of month)
	 \param [in] hour
	 \param [in] minutes
	 \param [in] seconds
 **********************************************************************/
void rtc_set_time (unsigned int year,
									 unsigned int month,
									 unsigned int dom,
									 unsigned int hour,
									 unsigned int minutes,
									 unsigned int seconds) { 
										 
 	/* Set current time for RTC */
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_SECOND, seconds);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MINUTE, minutes);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_HOUR, hour);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MONTH, month);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_YEAR, year);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH, dom);
}




/***********************************************************************
 * @brief		password_check			
	 controlla la password e procedi con l'autenticazione; poi agisce sulla
	 disabilitazine/riabilitazione dei bottoni anche !
 **********************************************************************/
void password_check (void) {
	
	unsigned char init_e2prom = 0;
	
	// inizializza E2PROM con impostazioni di default - Isolcell
  if (current_user.level==3){
		// pwd:0321 - setta parametri di default senza toccare la calibrazione
	  if ((password[1] == 0) && (password[2] == 3) && (password[3] == 2) && (password[4] == 1)){
			GuiVar_message       = 11;
			message_timer_count  = MESSAGE_ON_SCREEN_TIME;
			init_e2prom = 1;
		  // Aggiungi il relativo evento
		 	temp_event.time          = current_time;
			temp_event.type          = E_TYPE_MISC;
			temp_event.specific_type = E_S_TYPE_INIT_E2PROM;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;			
			add_event(temp_event);	
		  InitSpecial();
	  }
  }
  if (init_e2prom==0){
	  // Controlla se utente di livello 2 (5436;6465;1694)
	  if (
			((password[1] == 5) && (password[2] == 4) && (password[3] == 3) && (password[4] == 6)) ||
			((password[1] == 6) && (password[2] == 4) && (password[3] == 6) && (password[4] == 5)) ||
			((password[1] == 1) && (password[2] == 6) && (password[3] == 9) && (password[4] == 4))
//			((password[1] == 1) && (password[2] == 6) && (password[3] == 9) && (password[4] == 4)) ||
//			((password[1] == 8) && (password[2] == 1) && (password[3] == 7) && (password[4] == 7)) ||
//			((password[1] == 1) && (password[2] == 9) && (password[3] == 5) && (password[4] == 5)) ||
//			((password[1] == 3) && (password[2] == 3) && (password[3] == 7) && (password[4] == 7)) ||
//			((password[1] == 7) && (password[2] == 1) && (password[3] == 3) && (password[4] == 3)) ||
//			((password[1] == 2) && (password[2] == 8) && (password[3] == 7) && (password[4] == 7)) ||
//			((password[1] == 3) && (password[2] == 7) && (password[3] == 6) && (password[4] == 6)) ||
//			((password[1] == 5) && (password[2] == 1) && (password[3] == 5) && (password[4] == 5)) 
		  ) {
				/* LEVEL 2 USER AUTHENTICATED !!! */
				GuiVar_message       = 1;
				message_timer_count  = MESSAGE_ON_SCREEN_TIME;
				current_user.level   = 2;
				
				if ((password[1] == 5) && (password[2] == 4) && (password[3] == 3) && (password[4] == 6)  ) {	current_user.id      = 0;	}
				if ((password[1] == 6) && (password[2] == 4) && (password[3] == 6) && (password[4] == 5)  ) {	current_user.id      = 1;	}
				if ((password[1] == 1) && (password[2] == 6) && (password[3] == 9) && (password[4] == 4)  ) {	current_user.id      = 2;	}
//				if ((password[1] == 1) && (password[2] == 6) && (password[3] == 9) && (password[4] == 4)  ) {	current_user.id      = 3;	}
//				if ((password[1] == 8) && (password[2] == 1) && (password[3] == 7) && (password[4] == 7)  ) {	current_user.id      = 4;	}
//				if ((password[1] == 1) && (password[2] == 9) && (password[3] == 5) && (password[4] == 5)  ) {	current_user.id      = 5;	}
//				if ((password[1] == 3) && (password[2] == 3) && (password[3] == 7) && (password[4] == 7)  ) {	current_user.id      = 6;	}
//				if ((password[1] == 7) && (password[2] == 1) && (password[3] == 3) && (password[4] == 3)  ) {	current_user.id      = 7;	}
//				if ((password[1] == 2) && (password[2] == 8) && (password[3] == 7) && (password[4] == 7)  ) {	current_user.id      = 8;	}
//				if ((password[1] == 3) && (password[2] == 7) && (password[3] == 6) && (password[4] == 6)  ) {	current_user.id      = 9;	}
//				if ((password[1] == 5) && (password[2] == 1) && (password[3] == 5) && (password[4] == 5)  ) {	current_user.id      = 10;	}
				
				// Abilita i bottoni che erano disabilitati
				GuiVar_logout_button  		 = 0;
				GuiVar_button_events  		 = 0;
				GuiVar_button_options 		 = 0;
				GuiVar_config_button       = 0;
				GuiVar_no_buzzer_button 	 = 0;
				GuiVar_no_siren_button  	 = 0;
				GuiVar_reset_alarms_button = 0;	
				
				// Aggiungi il relativo evento
				temp_event.time          = current_time;
				temp_event.type          = E_TYPE_MISC;
				temp_event.specific_type = E_S_TYPE_LOGIN;
				temp_event.user_level    = current_user.level;
				temp_event.user_id       = current_user.id;			
				add_event(temp_event);				
			} else

	  // Controlla se utente di livello 3 (5868;4725;2490)
	  if ( 
			((password[1] == 5) && (password[2] == 8) && (password[3] == 6) && (password[4] == 8)) ||
			((password[1] == 4) && (password[2] == 7) && (password[3] == 2) && (password[4] == 5)) ||
			((password[1] == 2) && (password[2] == 4) && (password[3] == 9) && (password[4] == 0))
//			((password[1] == 7) && (password[2] == 6) && (password[3] == 9) && (password[4] == 9)) ||
//			((password[1] == 4) && (password[2] == 8) && (password[3] == 2) && (password[4] == 2)) ||
//			((password[1] == 8) && (password[2] == 4) && (password[3] == 7) && (password[4] == 7)) ||
//			((password[1] == 3) && (password[2] == 1) && (password[3] == 3) && (password[4] == 3)) ||
//			((password[1] == 1) && (password[2] == 7) && (password[3] == 8) && (password[4] == 8)) ||
//			((password[1] == 2) && (password[2] == 9) && (password[3] == 7) && (password[4] == 7)) ||
//			((password[1] == 9) && (password[2] == 6) && (password[3] == 5) && (password[4] == 5)) ||
//			((password[1] == 3) && (password[2] == 1) && (password[3] == 4) && (password[4] == 4)) 
		  ) {
				/* LEVEL 3 USER AUTHENTICATED !!! */
				GuiVar_message = 1;
				message_timer_count = MESSAGE_ON_SCREEN_TIME;
				current_user.level = 3;
				current_user.operation = 0;
        GuiVar_extra_info_index = 1;
				
				if ((password[1] == 5) && (password[2] == 8) && (password[3] == 6) && (password[4] == 8)  ) {	current_user.id      = 0;	}
				if ((password[1] == 4) && (password[2] == 7) && (password[3] == 2) && (password[4] == 5)  ) {	current_user.id      = 1;	}
				if ((password[1] == 2) && (password[2] == 4) && (password[3] == 9) && (password[4] == 0)  ) {	current_user.id      = 2;	}
//				if ((password[1] == 7) && (password[2] == 6) && (password[3] == 9) && (password[4] == 9)  ) {	current_user.id      = 3;	}
//				if ((password[1] == 4) && (password[2] == 8) && (password[3] == 2) && (password[4] == 2)  ) {	current_user.id      = 4;	}
//				if ((password[1] == 8) && (password[2] == 4) && (password[3] == 7) && (password[4] == 7)  ) {	current_user.id      = 5;	}
//				if ((password[1] == 3) && (password[2] == 1) && (password[3] == 3) && (password[4] == 3)  ) {	current_user.id      = 6;	}
//				if ((password[1] == 1) && (password[2] == 7) && (password[3] == 8) && (password[4] == 8)  ) {	current_user.id      = 7;	}
//				if ((password[1] == 2) && (password[2] == 9) && (password[3] == 7) && (password[4] == 7)  ) {	current_user.id      = 8;	}
//				if ((password[1] == 9) && (password[2] == 6) && (password[3] == 5) && (password[4] == 5)  ) {	current_user.id      = 9;	}
//				if ((password[1] == 3) && (password[2] == 1) && (password[3] == 4) && (password[4] == 4)  ) {	current_user.id      = 10;	}
					
				// Abilita i bottoni che erano disabilitati
				GuiVar_logout_button  	   = 0;
				GuiVar_button_events  	   = 0;
				GuiVar_button_options 	   = 0;
				GuiVar_set_dec_set    	   = 0;
				GuiVar_set_inc_set    	   = 0;
				GuiVar_set_dec_hyst   	   = 0;
				GuiVar_set_inc_hyst   	   = 0;
				GuiVar_button_save    	   = 0;
				GuiVar_del_events_button   = 0;
				GuiVar_config_button       = 0;
				GuiVar_config_button     	 = 0;
				GuiVar_no_buzzer_button 	 = 0;
				GuiVar_no_siren_button  	 = 0;
				GuiVar_reset_alarms_button = 0;	
				GuiVar_reset_values        = 1;
					
				// Aggiungi il relativo evento
				temp_event.time          = current_time;
				temp_event.type          = E_TYPE_MISC;
				temp_event.specific_type = E_S_TYPE_LOGIN;
				temp_event.user_level    = current_user.level;
				temp_event.user_id       = current_user.id;			
				add_event(temp_event);					
			} 
			else {
				// Password sbagliata
				GuiVar_message = 6;
				message_timer_count = MESSAGE_ON_SCREEN_TIME;			
			}
		}
				
	  GuiVar_pwd_char_1 = 0;
	  GuiVar_pwd_char_2 = 0;
	  GuiVar_pwd_char_3 = 0;
	  GuiVar_pwd_char_4 = 0;
}

	


/***********************************************************************
 * @brief		touch_initial			
	 \param [in] area attiva premuta
	 codice da eseguire alla pressione di un'area attiva (prima inattiva)
 **********************************************************************/
void touch_initial (unsigned int touch_area) { 
	
		// La variabile "cornice" identifica l'area attiva premuta
		cornice = touch_area;

		idle_state  = IDLE_TIMEOUT;
		auto_logout = LOGOUT_TIMEOUT;
	
		if ( (GuiVar_message == 0) || (touch_area == 22) || (touch_area == 23) || (touch_area == 61)) {

// 20201002
		new_value_to_display = 1;			
      
		switch (touch_area)
		{
// 20201002      
			default:
        new_value_to_display = 0;
      break;      
      
			// PRESS SCREEN SET
			case 1:
 // 20201002                                    
#ifdef UPDATE_SCREEN_ON_PRESS          
				if (GuiVar_button_set != 2) {
					if (GuiVar_screen_select == 1) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(1);
							GuiVar_screen_select = 1;
						  if (current_user.level > 1){
						    GuiVar_index_show_radio = 1;
                GuiVar_selected_option = 0;
							}						
						  else{
								GuiVar_index_show_radio = 0;
                GuiVar_selected_option = 7;
							}						
						  GuiVar_index_set_config = 0;						
							GuiVar_set_page_1_val_1 = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA]/10;
							GuiVar_set_page_1_val_2 = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA]/10;
							GuiVar_set_page_1_val_3 = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM];
							GuiVar_set_page_1_val_4 = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START];
							GuiVar_set_page_1_val_5 = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1];
						  GuiVar_set_page_1_val_6 = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1];					
  					  if (GuiVar_set_page_1_val_6==0)
								GuiVar_index_id_rete_CAN = 0;
  					  else if (GuiVar_set_page_1_val_6<29)
								GuiVar_index_id_rete_CAN = 1;
							else
								GuiVar_index_id_rete_CAN = 2;
							GuiVar_set_page_2_val_1 = E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]/10;
							GuiVar_set_page_2_val_2 = E2prom[E2_PAGE_2].E2_Int[E2_2_TOO_HIGH_TEMP]/10;
							GuiVar_set_page_2_val_3 = E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP]/10;
							if (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]>=20){
								GuiVar_T_ventilazione_index = 2;
							  GuiVar_set_page_2_val_4 = E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]/10;
							}
							else{
								if (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]==0){
  								GuiVar_T_ventilazione_index = 0;
									GuiVar_set_page_2_val_4 = 0;
								}
								else{
  								GuiVar_T_ventilazione_index = 1;
									GuiVar_set_page_2_val_4 = 1;
								}
							}
							GuiVar_set_page_2_val_5 = E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2];
						  GuiVar_set_page_2_val_6 = E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2];				
							
							init_screen(SET_SCREEN);
					}					
				}
#else
				if (GuiVar_button_set != 2) {
						press_button(1);
				}	       
#endif 
			break;
			
			// PRESS CALIBRATION BOTTLE
			case 2:
				if (GuiVar_button_th != 2) {
						press_button(2);
// 20201002
#ifdef UPDATE_SCREEN_ON_PRESS            
				if (GuiVar_button_th != 2) {
					if (GuiVar_screen_select == 2) {
							press_button(SYSTEM_SCREEN);
							GuiVar_screen_select = SYSTEM_SCREEN;
							init_screen(SYSTEM_SCREEN);
					}
					
					else if(current_user.level > 1) {
							press_button(2);							
							GuiVar_screen_select = 2;
							init_screen(2);
					}
				}
#endif              
				}
			break;			
			
			// PRESS SCREEN LOGIN/LOGOUT 		
			case 3: 
// 20201002                            
#ifdef UPDATE_SCREEN_ON_PRESS         
				if (GuiVar_button_log != 2) {
					if (GuiVar_screen_select == 3) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(3);
							GuiVar_screen_select = 3;
							init_screen(LOG_SCREEN);
					}	
				}		
#else
				if (GuiVar_button_log != 2) {
						press_button(3);
				}					      
#endif        
			break;					
			
			// PRESS SCREEN OPTIONS 
			case 4: 
// 20201002                            
#ifdef UPDATE_SCREEN_ON_PRESS          
				if (GuiVar_button_options != 2) {
					if (GuiVar_screen_select == 4) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							/* LOAD OPTIONS SCREEN*/
							press_button(4);
							GuiVar_screen_select = 4;
							init_screen(OPTIONS_SCREEN);
					}		
				}	
#else
				if (GuiVar_button_options != 2) {
						press_button(4);
				}					
#endif           
			break;
			
			// PRESS SCREEN EVENTS 
			case 5: 
// 20201002          
#ifdef UPDATE_SCREEN_ON_PRESS
				if (GuiVar_button_events != 2) {
					if (GuiVar_screen_select == 5) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(5);
							GuiVar_screen_select = 5;
							init_screen(EVENTS_SCREEN);
							new_fault = 0;
					}	
				}	      
#else      
				if (GuiVar_button_events != 2) {
						press_button(5);
				}					
#endif      
			break;		

			// PRESS SCREEN INFO 
			case 6: 
// 20201002
#ifdef UPDATE_SCREEN_ON_PRESS 
// 20201005
//      
//      VERIFICARE
//      VERIFICARE
//      VERIFICARE
//      VERIFICARE
//      
//				if (GuiVar_button_info != 2) {
//						press_button(6);
//						ewon.Anybus_Init_Request = 0;//ver 2.08
//					  index_ewon = 0;
//						GuiVar_OK = 0;//ver 2.08
//				}					
      
				if(current_user.level >= 3){//Sebastian prima diceva che la calibrazione dei sensori doveva essere a livello 3, poi mi ha chiesto di metterla a livello 2 poi di nuovo a livello 3 (2017.08.30)
				  GuiVar_screen_select = 0;
					press_button(0);
					init_screen(SYSTEM_SCREEN);
					daily_reset_counter = 0;
					//salva su RAM tamponata del RTC il nuovo valore 
					RTC_WriteGPREG(LPC_RTC,0,daily_reset_counter);
		    }
				else {
					if (GuiVar_screen_select == 6) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					 } else {
							press_button(6);
							GuiVar_screen_select = 6;
							init_screen(INFO_SCREEN);
					 }
				}      
#else      
				if (GuiVar_button_info != 2) {
						press_button(6);
						ewon.Anybus_Init_Request = 0;//ver 2.08
					  index_ewon = 0;
						GuiVar_OK = 0;//ver 2.08
				}					
#endif        
			break;	

			// PRESS SCREEN ALARMS 
			case 7: 
				if (GuiVar_button_alarms != 2) {
						press_button(7);
				}
			break;	

			// PRESS SCREEN FAULTS 
			case 8: 
				if (GuiVar_button_faults != 2) {
						press_button(8);
				}					
			break;	

			// PRESS SCREEN DISABLEMENTS
			case 9: 
				if (GuiVar_button_disablements != 2) {
						press_button(9);
				}					
			break;	

			// PRESS SCREEN DEVICES CTRL
			case 10: 
				if (GuiVar_button_disablements != 2) {
						press_button(10);				
				}
			break;		

			// PRESS DEC SET_DAY
			case 11:
				if (GuiVar_dec_day_button != 2) {
						GuiVar_dec_day_button = 1;
					
						GuiVar_day_color = LIGHT_BLUE;
					
						if (GuiVar_set_day <= 1) {
							GuiVar_set_day = 31;
						} else {
							GuiVar_set_day -= 1;
						}
				}
			break;
			
			// PRESS INC SET_DAY
			case 12:
				if (GuiVar_inc_day_button != 2) {
						GuiVar_inc_day_button = 1;	

						GuiVar_day_color = LIGHT_BLUE;
					
						if (GuiVar_set_day >= 31) {
							GuiVar_set_day = 1;
						} else {
							GuiVar_set_day += 1;
						}
				}
			break;		

			// PRESS DEC SET_MONTH
			case 13:
				if (GuiVar_dec_month_button != 2) {
						GuiVar_dec_month_button = 1;		

						GuiVar_month_color = LIGHT_BLUE;
					
						if (GuiVar_set_month <= 1) {
							GuiVar_set_month = 12;
						} else {
							GuiVar_set_month -= 1;
						}
				}
			break;
			
			// PRESS INC SET_MONTH
			case 14:
				if (GuiVar_inc_month_button != 2) {
						GuiVar_inc_month_button = 1;		

						GuiVar_month_color = LIGHT_BLUE;
					
						if (GuiVar_set_month >= 12) {
							GuiVar_set_month = 1;
						} else {
							GuiVar_set_month += 1;
						}
				}
			break;	

			// PRESS DEC SET_YEAR
			case 15:
				if (GuiVar_dec_year_button != 2) {
						GuiVar_dec_year_button = 1;				

						GuiVar_year_color = LIGHT_BLUE;
					
						if (GuiVar_set_year > 2200) {
							GuiVar_set_year = 2000;
						}
						
						if (GuiVar_set_year <= 2000) {
							GuiVar_set_year = 2100;
						} else {
							GuiVar_set_year -= 1;
						}
				}
			break;
			
			// PRESS INC SET_YEAR
			case 16:
				if (GuiVar_inc_year_button != 2) {
						GuiVar_inc_year_button = 1;			

						GuiVar_year_color = LIGHT_BLUE;
					
						if (GuiVar_set_year >= 2100) {
							GuiVar_set_year = 2000;
						} else {
							GuiVar_set_year += 1;
						}
				}
			break;			

			// PRESS DEC SET_HOUR
			case 17:
				if (GuiVar_dec_hour_button != 2) {
						GuiVar_dec_hour_button = 1;

						GuiVar_hour_color = LIGHT_BLUE;
					
						if (GuiVar_set_hour <= 0) {
							GuiVar_set_hour = 23;
						} else {
							GuiVar_set_hour -= 1;
						}
				}
			break;
			
			// PRESS INC SET_HOUR
			case 18:
				if (GuiVar_inc_hour_button != 2) {
						GuiVar_inc_hour_button = 1;			

						GuiVar_hour_color = LIGHT_BLUE;
					
						if (GuiVar_set_hour >= 23) {
							GuiVar_set_hour = 0;
						} else {
							GuiVar_set_hour += 1;
						}
				}
			break;		

			// PRESS DEC SET_MINUTES
			case 19:
				if (GuiVar_dec_minutes_button != 2) {
						GuiVar_dec_minutes_button = 1;	

						GuiVar_minutes_color = LIGHT_BLUE;
					
						if (GuiVar_set_minutes <= 0) {
							GuiVar_set_minutes = 59;
						} else {
							GuiVar_set_minutes -= 1;
						}
				}
			break;
			
			// PRESS INC SET_MINUTES
			case 20:
				if (GuiVar_inc_minutes_button != 2) {
						GuiVar_inc_minutes_button = 1;			

						GuiVar_minutes_color = LIGHT_BLUE;
					
						if (GuiVar_set_minutes >= 59) {
							GuiVar_set_minutes = 0;
						} else {
							GuiVar_set_minutes += 1;
						}
				}
			break;		

			// PRESS SAVE
			case 21:
				if (GuiVar_button_save != 2) {
						GuiVar_button_save = 1;		
				}
			break;

			// PRESS YES button
			case 22:
				if (GuiVar_yes_button != 2) {
						GuiVar_yes_button = 1;		
				}
			break;

			// PRESS NO button
			case 23:
				if (GuiVar_no_button != 2) {
						GuiVar_no_button = 1;		
				}
			break;
				
			// PRESS DEC BACKLIGHT
			case 24: 
				if (GuiVar_dec_backlight_button != 2) {
						GuiVar_dec_backlight_button = 1;	

						GuiVar_backlight_color = LIGHT_BLUE;
					
						GuiVar_set_backlight--;
						if(GuiVar_set_backlight <= 0) { GuiVar_set_backlight = 0;}
						ea_lcdb_ctrl_backlightContrast(GuiVar_set_backlight);
						last_loaded_backlight = GuiVar_set_backlight;
				}
			break;	

			// PRESS INC BACKLIGHT
			case 25: 
				if (GuiVar_inc_backlight_button != 2) {
						GuiVar_inc_backlight_button = 1;			

						GuiVar_backlight_color = LIGHT_BLUE;
					
						if(GuiVar_set_backlight <= 0) { GuiVar_set_backlight = 0;}
						GuiVar_set_backlight++;
						if(GuiVar_set_backlight >= 100) { GuiVar_set_backlight = 100;}
						ea_lcdb_ctrl_backlightContrast(GuiVar_set_backlight);
						last_loaded_backlight = GuiVar_set_backlight;
				}
			break;			

			// PRESS DEFAULT "ZONE 1" BUTTON
			case 26: 
					if (GuiVar_default_button_1 != 2){ 
						GuiVar_default_button_1 = 1;
						GuiVar_default_line_1_color = LIGHT_BLUE;
					}
			break;		

			// PRESS DEFAULT "ZONE 2" BUTTON
			case 27: 
					if (GuiVar_default_button_2 != 2){ 
						GuiVar_default_button_2 = 1;
						GuiVar_default_line_2_color = LIGHT_BLUE;
					}
			break;	

			// PRESS DEFAULT "ZONE 3" BUTTON
			case 28: 
					if (GuiVar_default_button_3 != 2){ 
						GuiVar_default_button_3 = 1;
						GuiVar_default_line_3_color = LIGHT_BLUE;
					}
			break;						
					
			// PRESS BACK BUTTON
			case 29: 
					if (GuiVar_button_back != 2){ 
						GuiVar_button_back = 1;
					}
			break;	

			// PRESS DEC ZONES (schermata SET)
			case 31: 
					if (GuiVar_set_zone_dec_button != 2){ 	
							GuiVar_set_zone_dec_button = 1;						
					}					
			break;	

			// PRESS INC ZONES (schermata SET)
			case 33: 
					if (GuiVar_set_zone_inc_button != 2){ 	
							GuiVar_set_zone_inc_button = 1;						
					}					
			break;	

			// PRESS DEC SET value BUTTON (schermata SET)
			case 36: 
					if (GuiVar_set_dec_set != 2){ 	
							GuiVar_set_dec_set = 1;	

							GuiVar_set_text_color  = LIGHT_BLUE;
						
							GuiVar_set_set -= 0.1;
							if (GuiVar_set_set <= 0) {
									GuiVar_set_set = 0;
									GuiVar_set_dec_set = 2;
							}
							GuiVar_set_inc_set = 0;
					}					
			break;	

			// PRESS INC SET value BUTTON (schermata SET)
			case 37: 
					if (GuiVar_set_inc_set != 2){ 	
							GuiVar_set_inc_set = 1;	

							GuiVar_set_text_color  = LIGHT_BLUE;
						
							GuiVar_set_set += 0.1;
							if (GuiVar_set_set >= 25) {
									GuiVar_set_set = 25;
									GuiVar_set_inc_set = 2;
							}
							GuiVar_set_dec_set = 0;
					}					
			break;	

			// PRESS DEC HYST value BUTTON (schermata SET)
			case 38: 
					if (GuiVar_set_dec_hyst != 2){ 	
							GuiVar_set_dec_hyst = 1;	

							GuiVar_hyst_text_color  = LIGHT_BLUE;
						
							GuiVar_set_hyst -= 0.1;
							if (GuiVar_set_hyst <= 0) {
									GuiVar_set_hyst = 0;
									GuiVar_set_dec_hyst = 2;
							}
							GuiVar_set_inc_hyst = 0;
					}					
			break;	

			// PRESS INC HYST value BUTTON (schermata SET)
			case 39: 
					if (GuiVar_set_inc_hyst != 2){ 	
							GuiVar_set_inc_hyst = 1;	

							GuiVar_hyst_text_color  = LIGHT_BLUE;
						
							GuiVar_set_hyst += 0.1;
							if (GuiVar_set_hyst >= 0.5) {
									GuiVar_set_hyst = 0.5;
									GuiVar_set_inc_hyst = 2;
							}
							GuiVar_set_dec_hyst = 0;
					}					
			break;	

			// PRESS DEC ZONES (schermata TH)
			case 41: 
					if (GuiVar_th_zone_dec_button != 2){ 	
							GuiVar_th_zone_dec_button = 1;						
					}					
			break;	

			// PRESS INC ZONES (schermata TH)
			case 43: 
					if (GuiVar_th_zone_inc_button != 2){ 	
							GuiVar_th_zone_inc_button = 1;						
					}					
			break;	

			// PRESS DEC TH HIGH value BUTTON (schermata TH)
			case 46: 
					if (GuiVar_th_dec_high != 2){ 	
							GuiVar_th_dec_high = 1;	

							GuiVar_th_high_color  = LIGHT_BLUE;
						
							GuiVar_th_new_high -= 0.1;
							if (GuiVar_th_new_high <= GuiVar_th_dec_low) {
									GuiVar_th_new_high = GuiVar_th_dec_low+0.1;
									GuiVar_th_dec_high = 2;
							}
							GuiVar_th_inc_high = 0;
//							if (GuiVar_th_new_high <= 0) {
//									GuiVar_th_new_high = 0;
//									GuiVar_th_dec_high = 2;
//							}
//							GuiVar_th_inc_high = 0;
					}					
			break;	

			// PRESS INC TH HIGH value BUTTON (schermata TH)
			case 47: 
					if (GuiVar_th_inc_high != 2){ 	
							GuiVar_th_inc_high = 1;	

							GuiVar_th_high_color  = LIGHT_BLUE;
						
							GuiVar_th_new_high += 0.1;
							if (GuiVar_th_new_high >= 25) {
									GuiVar_th_new_high = 25;
									GuiVar_th_inc_high = 2;
							}
							GuiVar_th_dec_high = 0;
					}					
			break;	

			// PRESS DEC TH LOW value BUTTON (schermata TH)
			case 48: 
					if (GuiVar_th_dec_low != 2){ 	
							GuiVar_th_dec_low = 1;	

							GuiVar_th_low_color  = LIGHT_BLUE;
						
							GuiVar_th_new_low -= 0.1;
							if (GuiVar_th_new_low <= 0) {
									GuiVar_th_new_low = 0;
									GuiVar_th_dec_low = 2;
							}
							GuiVar_th_inc_low = 0;
					}					
			break;	

			// PRESS INC TH LOW value BUTTON (schermata TH)
			case 49: 
					if (GuiVar_th_inc_low != 2){ 	
							GuiVar_th_inc_low = 1;	

							GuiVar_th_low_color  = LIGHT_BLUE;
						
							GuiVar_th_new_low += 0.1;
							if (GuiVar_th_new_low >= GuiVar_th_new_high) {
									GuiVar_th_new_low = GuiVar_th_new_high-0.1;
									GuiVar_th_inc_low = 2;
							}
							GuiVar_th_dec_low = 0;
//							if (GuiVar_th_new_low >= 25) {
//									GuiVar_th_new_low = 25;
//									GuiVar_th_inc_low = 2;
//							}
//							GuiVar_th_dec_low = 0;
					}					
			break;	

			// PRESS keyboard 1
			case 50: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_1 != 2) {
						
						GuiVar_keyboard_1 = 1;
						pswd_index++;
						password[pswd_index] = 1;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
					}           
#else
					if (GuiVar_keyboard_1 != 2){ 	
							GuiVar_keyboard_1 = 1;          
					}					
#endif           
			break;

			// PRESS keyboard 2
			case 51: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_2 != 2) {
						
						GuiVar_keyboard_2 = 1;
						
						pswd_index++;
						password[pswd_index] = 2;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}											
					}	      
#else             
					if (GuiVar_keyboard_2 != 2){ 	
							GuiVar_keyboard_2 = 1;
					}					
#endif            
			break;

			// PRESS keyboard 3
			case 52: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS  
          if (GuiVar_keyboard_3 != 2) {
						
						GuiVar_keyboard_3 = 1;
						
						pswd_index++;
						password[pswd_index] = 3;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}												
					}
#else             
					if (GuiVar_keyboard_3 != 2){ 	
							GuiVar_keyboard_3 = 1;	
					}					
#endif            
			break;

			// PRESS keyboard 4
			case 53: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_4 != 2) {
						
						GuiVar_keyboard_4 = 1;
						
						pswd_index++;
						password[pswd_index] = 4;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}            
#else
					if (GuiVar_keyboard_4 != 2){ 	
							GuiVar_keyboard_4 = 1;	
					}					
#endif            
			break;

			// PRESS keyboard 5
			case 54: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_5 != 2) {
						
						GuiVar_keyboard_5 = 1;
						
						pswd_index++;
						password[pswd_index] = 5;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}            
#else
					if (GuiVar_keyboard_5 != 2){ 	
							GuiVar_keyboard_5 = 1;	
            
					}					
#endif            
			break;

			// PRESS keyboard 6
			case 55: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_6 != 2) {
						
						GuiVar_keyboard_6 = 1;
						
						pswd_index++;
						password[pswd_index] = 6;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}            
#else
					if (GuiVar_keyboard_6 != 2){ 	
							GuiVar_keyboard_6 = 1;	

					}					
#endif                  
			break;

			// PRESS keyboard 7
			case 56: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_7 != 2) {
						
						GuiVar_keyboard_7 = 1;
						
						pswd_index++;
						password[pswd_index] = 7;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}            
#else
					if (GuiVar_keyboard_7 != 2){ 	
							GuiVar_keyboard_7 = 1;						
      }
#endif                  
      
			break;

			// PRESS keyboard 8
			case 57: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_8 != 2) {
						
						GuiVar_keyboard_8 = 1;
						
						pswd_index++;
						password[pswd_index] = 8;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}	            
#else

					if (GuiVar_keyboard_8 != 2){ 	
							GuiVar_keyboard_8 = 1;	

					}					
#endif                      
			break;

			// PRESS keyboard 9
			case 58: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_9 != 2) {
						
						GuiVar_keyboard_9 = 1;
						
						pswd_index++;
						password[pswd_index] = 9;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}		
#else
					if (GuiVar_keyboard_9 != 2){ 	
							GuiVar_keyboard_9 = 1;	
            
					}					
#endif            
			break;

			// PRESS keyboard 0
			case 59: 
// 20201002         
#ifdef UPDATE_SCREEN_ON_PRESS
					if (GuiVar_keyboard_0 != 2) {
						
						GuiVar_keyboard_0 = 1;
						
						pswd_index++;
						password[pswd_index] = 0;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
							
					}						            
#else
					if (GuiVar_keyboard_0 != 2){ 	
							GuiVar_keyboard_0 = 1;	

					}					
#endif                      
			break;			

			// PRESS LOGOUT button
			case 60:
					if (GuiVar_logout_button != 2) {						
						GuiVar_logout_button = 1;				
					}						
			break;				

			// PRESS OK button
			case 61:
				GuiVar_ok_button = 1;
				if (GuiVar_message == 13){
					reset_after_new_configuration_load = 0xAB;
					message_timer_count = MESSAGE_ON_SCREEN_TIME*10;
				}
			break;	

			// PRESS UP button
			case 62:
				if (GuiVar_up_button != 2) {

					switch (GuiVar_screen_select)
					{
						case 0: // SYSTEM
							index_system--;
						break;
						
						case 5: // EVENTS
							index_events--;
						break;

						case 8: // FAULTS
							index_faults--;
						break;

						case 9: // DISABLEMENTS
							index_disablements--;
						break;

						case 10: // DEV_CTRL
							index_dev_ctrl--;
						break;
						
						case 11: // ALARMS LIST
							index_alarms--;
						break;

						case 12: // ZONE X
							index_zone_x-= 1;
						break;
						
						case 13: // OBJECTS LIST (Dev ctrl list)
						case 14: // OBJECTS LIST (Disablements list)
							index_objects--;
						break;
					}
					GuiVar_up_button = 1;					
				}	
			break;	

			// PRESS DOWN button
			case 63:
				if (GuiVar_down_button != 2) {
					
					switch (GuiVar_screen_select)
					{
						case 0: // SYSTEM
							index_system++;
						break;
						
						case 5: // EVENTS
							index_events++;
						break;

						case 8: // FAULTS
							index_faults++;
						break;

						case 9: // DISABLEMENTS
							index_disablements++;
						break;

						case 10: // DEV_CTRL
							index_dev_ctrl++;
						break;
						
						case 11: // ALARMS LIST
							index_alarms++;
						break;

						case 12: // ZONE X
							index_zone_x+= 1;
						break;
												
						case 13: // OBJECTS LIST (Dev ctrl list)
						case 14: // OBJECTS LIST (Disablements list)
							index_objects++;
						break;						
					}
					GuiVar_down_button = 1;					
				}	
			break;				
				
			// PRESS DEL events button
			case 64:
				if (GuiVar_button_delete_back_color == 0xFFFF) {//2018.02.23				
					GuiVar_del_events_button = 1;					
				}	
			break;	
				
			// PRESS ALARMS LIST button
			case 65:
				if (GuiVar_list_button != 2) {
					
					GuiVar_list_button = 1;					
				}	
			break;	
				
			// PRESS CONFIG DEV CTRL button
			case 66:
				if (GuiVar_config_button != 2) {
					GuiVar_config_button = 1;					
				}	
			break;	

			// PRESS TH DEC sens button
			case 72:
// 				if (GuiVar_th_dec_sens != 2) {
// 					GuiVar_th_dec_sens = 1;
// 					index_sensor -= 2;
// 				}	
			break;

			// PRESS TH INC sens button
			case 73:
// 				if (GuiVar_th_inc_sens != 2) {
// 					GuiVar_th_inc_sens = 1;
// 					index_sensor += 2;
// 				}	
			break;		

			// PRESS OUTPUT OFF (DEV_CTRL)
			case 74:
				if (GuiVar_output_off != 2) {
					GuiVar_output_off = 1;
				}	
			break;		

			// PRESS SORT (DEV CTRL)
			case 75:
				if (GuiVar_sort_dev_ctrl != 2) {
						GuiVar_sort_dev_ctrl = 1;					
				}
			break;

			// PRESS SORT (DISABLEMENTS)
			case 76:
				if (GuiVar_sort_disablements != 2) {
						GuiVar_sort_disablements = 1;					
				}
			break;	

			// PRESS RESET ALARMS BUTTON
			case 77:
				if (GuiVar_reset_alarms_button != 2) {
						GuiVar_reset_alarms_button = 1;					
				}
			break;
				
			// PRESS "IDLE" SCREEN
			case 78:
					GuiLib_CurStructureNdx = GuiStruct_Main_0;
					GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW); 
			break;	

			// PRESS NO BUZZER BUTTON
			case 79:
				if (GuiVar_no_buzzer_button != 2) {
						GuiVar_no_buzzer_button = 1;					
				}
			break;

			// PRESS NO SIREN BUTTON
			case 80:
				if (GuiVar_no_siren_button != 2) {
						GuiVar_no_siren_button = 1;					
				}
			break;

			// PRESS TEST LEDS BUTTON
		
				case 81:
				if (GuiVar_test_leds_button != 2) {						
					if (CIE.Master){ 	
					  GuiVar_test_leds_button = 1;
						leds_in_test = 1;
						GuiVar_test_screen=0;// schermate colorate
						test_leds_timeout = TEST_LEDS_TIMEOUT;
					}
				}
			break;	

			// PRESS reset values BUTTON
			case 87:
				if (GuiVar_reset_values == 1){
					GuiVar_reset_values_button = 1;
				}
			break;	

			// PRESS DEC lingue
			case 400: 
				if (GuiVar_dec_language_button != 2) {
						GuiVar_dec_language_button = 1;	
					  sys_language = (sys_language)? --sys_language: 1;
					  GuiVar_sys_language = sys_language;
					  GuiLib_SetLanguage(sys_language); // poi non deve essere usato cos� - cambiare la scritta italiano, english in altro modo
            init_screen(OPTIONS_SCREEN);
				}
			break;	

			// PRESS INC lingue
			case 401: 
				if (GuiVar_inc_language_button != 2) {
						GuiVar_inc_language_button = 1;			
					  sys_language = (sys_language<1)? ++sys_language: 0;
					  GuiVar_sys_language = sys_language;
					  GuiLib_SetLanguage(sys_language); // poi non deve essere usato cos�
            init_screen(OPTIONS_SCREEN);
				}
			break;			
				
			// load settings BUTTON
			case 422:
				if (GuiVar_settings_button == 0){
					GuiVar_settings_button = 1;
				}
			break;			

			case 424://ver 2.08
				if (GuiVar_Anybus_button == 0){
					GuiVar_Anybus_button = 1;
				}
			break;			

			case 538: case 539: case 540: case 541: case 542: case 543:
				GuiVar_selected_option = touch_area-538;
			break;		

			case 544:
				GuiVar_index_indice_pagina = (GuiVar_index_indice_pagina<1)? ++GuiVar_index_indice_pagina: 0;
				GuiVar_index_set_config = (GuiVar_index_set_config<1)? ++GuiVar_index_set_config: 0;
			break;		
			
				
//********************************
				
				
               
			
						
//***********************************************************************************************************************		
		} // Fine switch touch area
	}
}



void init_sys_io (void) { 

	unsigned int i;

	// ----------------------------------------------------------------------------------
}



 /***********************************************************************
 * @brief		init_variables			
	 inizializza le variabili di sistema
 **********************************************************************/
void init_variables (void) { 
	
	unsigned int i = 0;
	unsigned int r = 0;
	
	/* Init System */
	/* Init System */
	GuiVar_firmware_version = FIRMWARE_VERSION;
	GuiVar_board_number 		= eeprom_read_byte (E2PROM_PAGE_OF_OTHER, REG_BOARD_NUMBER);
	
	// ATTENZIONE !!!
	// il numero massimo di zone � limitato da CONSTANT_ZONES in "board.h" !
	// e anche tutti gli altri elementi
	
	sys_language          = eeprom_read_word (E2PROM_PAGE_OF_OTHER, REG_LANGUAGE*2);
	GuiVar_sys_language   = sys_language;
	GuiLib_SetLanguage(sys_language);
	
	events_array_dim       = 0;
	alarms_array_dim       = 0;
	faults_array_dim       = 0;
	disablements_array_dim = 0;
	dev_ctrl_array_dim     = 0;
	
	/* Init user / password */
	current_user.level   = 0;
	current_user.id      = 0;
	GuiVar_logout_button = 2;
	
	/* Init schermata iniziale */
	init_screen(SYSTEM_SCREEN);
	GuiVar_bar_index = -1;
	
	/*Init CRC table*/
	Modbus_InitCrc16Table();
	
	/* Transmission */
//	sensor_polling_index 				  = 0;
//	can_polling_index 					  = 0;
//	can_modules_out_polling_index = 0;

  /* Dev CTRL */
	GuiVar_radio_flux_1           = 1;
	GuiVar_radio_flux_2           = 1;
	GuiVar_radio_flux_3           = 1;
	GuiVar_radio_flux_4           = 1;
	GuiVar_radio_flux_5           = 1;
	//GuiVar_radio_flux_6           = 1;
}



/***********************************************************************
 * @brief		touch_continued		
	 \param [in] area attiva premuta
	 codice da eseguire alla pressione continua di un'area attiva (gi� premuta in precedenza)
 **********************************************************************/
void touch_continued (unsigned int touch_area) { 
	   float max_val, min_val;
	   char i;
		// incrementa il tempo (da quando ho cominciato a premere)
		touch_time++; 
	 
		if ( (GuiVar_message == 0) || (touch_area == 22) || (touch_area == 23) || (touch_area == 61)) {

// 20201002
    new_value_to_display = 1;
			
		switch (touch_area)
		{
// 20201002      
			default:
				new_value_to_display = 0;
			break;      
      
			case 1:
				new_value_to_display = 0; // 20201002
			break;

			case 2:
				new_value_to_display = 0; // 20201002
			break;		

			case 3:
				new_value_to_display = 0; // 20201002
			break;

			case 4:
				new_value_to_display = 0; // 20201002
			break;	

			case 5:
				new_value_to_display = 0; // 20201002
			break;

			case 6:
				new_value_to_display = 0; // 20201002
			break;	

			case 10: 		
				new_value_to_display = 0; // 20201002
			break;					
			
			// CONT_PRESS DEC SET_DAY
			case 11:	
				if (GuiVar_dec_day_button != 2) {
			
						if (GuiVar_set_day < 1) {
							GuiVar_set_day = 31;
						} else {
								if (touch_time <= 10){
									GuiVar_set_day -= 1;
								} else {
									GuiVar_set_day -= 3;
								}
						}
						if (GuiVar_set_day < 1) {
								GuiVar_set_day = 31;
						}
						
				}
			break;
			
			// CONT_PRESS INC SET_DAY
			case 12:
				if (GuiVar_inc_day_button != 2) {
							
						if (GuiVar_set_day > 31) {
							GuiVar_set_day = 1;
						} else {
								if (touch_time <= 10){
									GuiVar_set_day += 1;
								} else {
									GuiVar_set_day += 3;
								}
						}
						if (GuiVar_set_day > 31) {
							GuiVar_set_day = 1;
						}				
				}
			break;	

			// CONT_PRESS DEC SET_MONTH
			case 13:				
				if (GuiVar_dec_month_button != 2) {
							
						if (GuiVar_set_month < 1) {
							GuiVar_set_month = 12;
						} else {
								if (touch_time <= 10){
									GuiVar_set_month -= 1;
								} else {
									GuiVar_set_month -= 3;
								}
						}
						if (GuiVar_set_month < 1) {
								GuiVar_set_month = 12;
						}
				}
			break;
			
			// CONT_PRESS INC SET_MONTH
			case 14:
				if (GuiVar_inc_month_button != 2) {
											
						if (GuiVar_set_month > 12) {
							GuiVar_set_month = 1;
						} else {
								if (touch_time <= 10){
									GuiVar_set_month += 1;
								} else {
									GuiVar_set_month += 3;
								}
						}
						if (GuiVar_set_month > 12) {
							GuiVar_set_month = 1;
						}		
				}						
			break;	

			// CONT_PRESS DEC SET_YEAR
			case 15:		
				if (GuiVar_dec_year_button != 2) {
											
						if (GuiVar_set_year <= 2000) {
							GuiVar_set_year = 2100;
						} else {
								if (touch_time <= 10){
									GuiVar_set_year -= 1;
								} else {
									GuiVar_set_year -= 3;
								}
						}
						if (GuiVar_set_year < 2000) {
								GuiVar_set_year = 2100;
						}
				}
			break;
			
			// CONT_PRESS INC SET_YEAR
			case 16:	
				if (GuiVar_inc_year_button != 2) {
															
						if (GuiVar_set_year >= 2100) {
							GuiVar_set_year = 2000;
						} else {
								if (touch_time <= 10){
									GuiVar_set_year += 1;
								} else {
									GuiVar_set_year += 3;
								}
						}
						if (GuiVar_set_year > 2100) {
							GuiVar_set_year = 2000;
						}		
				}						
			break;	

			// CONT_PRESS DEC SET_HOUR
			case 17:	
				if (GuiVar_dec_hour_button != 2) {
															
						if (GuiVar_set_hour <= 0) {
							GuiVar_set_hour = 23;
						} else {
								if (touch_time <= 10){
									GuiVar_set_hour -= 1;
								} else {
									GuiVar_set_hour -= 3;
								}
						}
						if (GuiVar_set_hour <= 0) {
								GuiVar_set_hour = 0;
						}
				}
			break;
			
			// CONT_PRESS INC SET_HOUR
			case 18:
				if (GuiVar_inc_hour_button != 2) {
														
						if (GuiVar_set_hour >= 23) {
							GuiVar_set_hour = 0;
						} else {
								if (touch_time <= 10){
									GuiVar_set_hour += 1;
								} else {
									GuiVar_set_hour += 3;
								}
						}
						if (GuiVar_set_hour > 23) {
							GuiVar_set_hour = 0;
						}			
			  }				
			break;	

			// CONT_PRESS DEC SET_MINUTES
			case 19:				
				if (GuiVar_dec_minutes_button != 2) {
														
						if (GuiVar_set_minutes <= 0) {
							GuiVar_set_minutes = 59;
						} else {
								if (touch_time <= 10){
									GuiVar_set_minutes -= 1;
								} else {
									GuiVar_set_minutes -= 3;
								}
						}
						if (GuiVar_set_minutes <= 0) {
								GuiVar_set_minutes = 0;
						}
				}
			break;
			
			// CONT_PRESS INC SET_MINUTES
			case 20:
				if (GuiVar_inc_minutes_button != 2) {
																		
						if (GuiVar_set_minutes >= 59) {
							GuiVar_set_minutes = 0;
						} else {
								if (touch_time <= 10){
									GuiVar_set_minutes += 1;
								} else {
									GuiVar_set_minutes += 3;
								}
						}
						if (GuiVar_set_minutes > 59) {
							GuiVar_set_minutes = 0;
						}			
				}						
			break;		

			// CONT_PRESS DEC BACKLIGHT
			case 24: 
				if (GuiVar_dec_backlight_button != 2) {
																		
						if (touch_time <= 10) {
							GuiVar_set_backlight--;
						} else {
							GuiVar_set_backlight -= 5;
						}
						if(GuiVar_set_backlight <= 0) { GuiVar_set_backlight = 0;}
						ea_lcdb_ctrl_backlightContrast(GuiVar_set_backlight);
						last_loaded_backlight = GuiVar_set_backlight;
				}
			break;	

			// CONT_PRESS INC BACKLIGHT
			case 25: 
				if (GuiVar_inc_backlight_button != 2) {
																						
						if (touch_time <= 10) {
							GuiVar_set_backlight++;
						} else {
							GuiVar_set_backlight += 5;
						}
						if(GuiVar_set_backlight >= 100) { GuiVar_set_backlight = 100;}
						ea_lcdb_ctrl_backlightContrast(GuiVar_set_backlight);
						last_loaded_backlight = GuiVar_set_backlight;
				}
			break;				

			// CONT_PRESS DEC SET value BUTTON (schermata SET)
			case 36: 
					if (GuiVar_set_dec_set != 2){ 	
						
							if (touch_time <= 10) {
								GuiVar_set_set -= 0.1;
							} else {
								GuiVar_set_set -= 1;
							}
							
							if (GuiVar_set_set <= 0) {
									GuiVar_set_set = 0;
									GuiVar_set_dec_set = 2;
							}
							GuiVar_set_inc_set = 0;
					}					
			break;				

			// CONT_PRESS INC SET value BUTTON (schermata SET)
			case 37: 
					if (GuiVar_set_inc_set != 2){ 		

							if (touch_time <= 10) {
								GuiVar_set_set += 0.1;
							} else {
								GuiVar_set_set += 1;
							}

							if (GuiVar_set_set >= 25) {
									GuiVar_set_set = 25;
									GuiVar_set_inc_set = 2;
							}
							GuiVar_set_dec_set = 0;
							
//							if (GuiVar_set_set >= 25) {
//									GuiVar_set_set = 25;
//									GuiVar_set_inc_set = 2;
//							}
//							GuiVar_set_dec_set = 0;
					}					
			break;		

			// CONT_PRESS DEC HYST value BUTTON (schermata SET)
			case 38: 
					if (GuiVar_set_dec_hyst != 2){ 	
						
								GuiVar_set_hyst -= 0.1;
			
							if (GuiVar_set_hyst <= 0) {
									GuiVar_set_hyst = 0;
									GuiVar_set_dec_hyst = 2;
							}
							GuiVar_set_inc_hyst = 0;
					}					
			break;				

			// CONT_PRESS INC HYST value BUTTON (schermata SET)
			case 39: 
					if (GuiVar_set_inc_hyst != 2){ 		

								GuiVar_set_hyst += 0.1;
							
							if (GuiVar_set_hyst >= 0.5) {
									GuiVar_set_hyst = 0.5;
									GuiVar_set_inc_hyst = 2;
							}
							GuiVar_set_dec_hyst = 0;
					}					
			break;

			// CONT_PRESS DEC TH HIGH value BUTTON (schermata TH)
			case 46: 
					if (GuiVar_th_dec_high != 2){ 	
						
							if (touch_time <= 10) {
								GuiVar_th_new_high -= 0.1;
							} else {
								GuiVar_th_new_high -= 1;
							}
							
							if (GuiVar_th_new_high <= GuiVar_th_new_low) {
									GuiVar_th_new_high = GuiVar_th_new_low+0.1;
									GuiVar_th_dec_high = 2;
							}
							GuiVar_th_inc_high = 0;

//							if (GuiVar_th_new_high <= 0) {
//									GuiVar_th_new_high = 0;
//									GuiVar_th_dec_high = 2;
//							}
//							GuiVar_th_inc_high = 0;
					}					
			break;				

			// CONT_PRESS INC TH HIGH value BUTTON (schermata TH)
			case 47: 
					if (GuiVar_th_inc_high != 2){ 		

							if (touch_time <= 10) {
								GuiVar_th_new_high += 0.1;
							} else {
								GuiVar_th_new_high += 1;
							}
							
							if (GuiVar_th_new_high >= 25) {
									GuiVar_th_new_high = 25;
									GuiVar_th_inc_high = 2;
							}
							GuiVar_th_dec_high = 0;
					}					
			break;		

			// CONT_PRESS DEC TH LOW value BUTTON (schermata TH)
			case 48: 
					if (GuiVar_th_dec_low != 2){ 	
						
							if (touch_time <= 10) {
								GuiVar_th_new_low -= 0.1;
							} else {
								GuiVar_th_new_low -= 1;
							}
							
							if (GuiVar_th_new_low <= 0) {
									GuiVar_th_new_low = 0;
									GuiVar_th_dec_low = 2;
							}
							GuiVar_th_inc_low = 0;
					}					
			break;				

			// CONT_PRESS INC TH LOW value BUTTON (schermata TH)
			case 49: 
					if (GuiVar_th_inc_low != 2){ 		

							if (touch_time <= 10) {
								GuiVar_th_new_low += 0.1;
							} else {
								GuiVar_th_new_low += 1;
							}

							if (GuiVar_th_new_low >= GuiVar_th_new_high) {
									GuiVar_th_new_low = GuiVar_th_new_high-0.1;
									GuiVar_th_inc_low = 2;
							}
							GuiVar_th_dec_low = 0;
							
//							if (GuiVar_th_new_low >= 25) {
//									GuiVar_th_new_low = 25;
//									GuiVar_th_inc_low = 2;
//							}
//							GuiVar_th_dec_low = 0;
					}					
			break;		

			// CONT_PRESS UP button
			case 62:
				if (GuiVar_up_button != 2) {
					switch (GuiVar_screen_select)
					{
						case 0: // SYSTEM
							if (touch_time <= 10) {
								index_system -= 1;
							} else {
								index_system -= 3;
							}
						break;
							
						case 5: // EVENTS
							if (touch_time <= 10) {
								index_events -= 1;
							} else {
								index_events -= 3;
							}
						break;

						case 8: // FAULTS
							if (touch_time <= 10) {
								index_faults -= 1;
							} else {
								index_faults -= 3;
							}
						break;

						case 9: // DISABLEMENTS
							if (touch_time <= 10) {
								index_disablements -= 1;
							} else {
								index_disablements -= 3;
							}
						break;

						case 10: // DEVICES CONTROL
							if (touch_time <= 10) {
								index_dev_ctrl -= 1;
							} else {
								index_dev_ctrl -= 3;
							}
						break;
							
						case 11: // ALARMS LIST
							if (touch_time <= 10) {
								index_alarms -= 1;
							} else {
								index_alarms -= 3;
							}
						break;

						case 12: // ZONE_X
							if (touch_time <= 10) {
								index_zone_x -= 1;//2;
							} else {
								index_zone_x -= 1;//4;
							}
						break;
							
						case 13: // OBJECTS LIST (Dev ctrl list)							
						case 14: // OBJECTS LIST (Disablements list)
							if (touch_time <= 10) {
								index_objects -= 1;
							} else {
								index_objects -= 3;
							}
						break;
					}	
       }					
			break;	

			// CONT_PRESS DOWN button
			case 63:
				if (GuiVar_down_button != 2) {
					switch (GuiVar_screen_select)
					{
						case 0: // SYSTEM
							if (touch_time <= 10) {
								index_system += 1;
							} else {
								index_system += 3;
							}
						break;
							
						case 5: // EVENTS
							if (touch_time <= 10) {
								index_events += 1;
							} else {
								index_events += 3;
							}
						break;

						case 8: // FAULTS
							if (touch_time <= 10) {
								index_faults += 1;
							} else {
								index_faults += 3;
							}
						break;

						case 9: // DISABLEMENTS
							if (touch_time <= 10) {
								index_disablements += 1;
							} else {
								index_disablements += 3;
							}
						break;

						case 10: // DEVICES CONTROL
							if (touch_time <= 10) {
								index_dev_ctrl += 1;
							} else {
								index_dev_ctrl += 3;
							}
						break;
							
						case 11: // ALARMS LIST
							if (touch_time <= 10) {
								index_alarms += 1;
							} else {
								index_alarms += 3;
							}
						break;

						case 12: // ZONE_X
							if (touch_time <= 10) {
								index_zone_x += 1; //2;
							} else {
								index_zone_x += 1; //4;
							}
						break;
							
						case 13: // OBJECTS LIST (Dev ctrl list)							
						case 14: // OBJECTS LIST (Disablements list)
							if (touch_time <= 10) {
								index_objects += 1;
							} else {
								index_objects += 3;
							}
						break;
					}	
				}
			break;

			// CONT_PRESS TH DEC sens button
			case 72:
// 				if (GuiVar_th_dec_sens != 2) {
// 						if (touch_time <= 10) {
// 							index_sensor -= 2;
// 						} else {
// 							index_sensor -= 4;
// 						}
// 				}	
			break;

			// CONT_PRESS TH INC sens button
			case 73:
// 				if (GuiVar_th_inc_sens != 2) {
// 						if (touch_time <= 10) {
// 							index_sensor += 2;
// 						} else {
// 							index_sensor += 4;
// 						}
// 				}	
			break;
			
								
					
				case 414:
					if (GuiVar_selected_bottle>1){ 
					  GuiVar_selected_bottle--;
					}
					if (GuiVar_selected_bottle==1)
				    GuiVar_O2_bottle = (float)(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS])/100;
					else
				    GuiVar_O2_bottle = (float)(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS])/100;
				break;		
				
				case 415:	
					if (GuiVar_selected_bottle<2){ 
					  GuiVar_selected_bottle++;
					}	
					if (GuiVar_selected_bottle==1)
				    GuiVar_O2_bottle = (float)(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS])/100;
					else
				    GuiVar_O2_bottle = (float)(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS])/100;
				break;		

				case 416: 
					if ( (touch_time <= 10) || (GuiVar_O2_bottle>=24.5) ){
  					if (GuiVar_O2_bottle<25)
    					GuiVar_O2_bottle += 0.01;
					}
  				else {
    					GuiVar_O2_bottle += 0.10;
					}
				break;		

				case 417: 
					if ( (touch_time <= 10) || (GuiVar_O2_bottle<=0.5) ){
  					if (GuiVar_O2_bottle>0)
    					GuiVar_O2_bottle -= 0.01;
						GuiVar_O2_bottle = (GuiVar_O2_bottle<0)? 0: GuiVar_O2_bottle;// necessario perch� easygui fa casino e mi va anche in negativo fino a -0.01
					}
					else{
    				GuiVar_O2_bottle -= 0.10;
					}
				break;		

			// PRESS exit
			case 420: 
				Adox1500.Stato &= ~ADOX_TARATURA;
				SetMioRele(ON, 0, 0, OUT_VTARATURA); // chiude valvola per taratura
				if (GuiVar_exit != 2){ 	
						GuiVar_exit = 1;	
				}					
			break;
				
			// PRESS store_O2
			case 421:
    			if ( (GuiVar_store_O2 != 2) && (current_user.level >= 2) )
							GuiVar_store_O2 = 1;	
        else{
    			if ( (GuiVar_store_O2 != 2) && (current_user.level >= 3) )
							GuiVar_store_O2 = 1;	
				}						
				break;
					
			// calib. BUTTON
			case 423:
				if (GuiVar_calibration_button == 0){
          if (current_user.level>1)
					  GuiVar_calibration_button = 1;
				}
			break;			

							// PRESS store_bottle_set
			case 426: 
					if (GuiVar_store_bottle_set != 2){ 	
							GuiVar_store_bottle_set = 1;	
					}					
			break;

   				//  touch area "Item Settings" della schermata 15				
					 case 422: 
						 if (current_user.level >= 3)
						   current_user.operation = 2;//inizializza la eeprom con i parametri letti da SD
					 break;
									
					 case 424: //ver 2.08
  					 //spedisce_messaggi_configurazione_Anybus
					   if (current_user.level >= 3){
               ewon.Anybus_Init_Request = 1;
  					   index_ewon = 0;
             }							 
					 break;

          // incremento variabile
					case 537:
						switch (GuiVar_selected_option)
						{
							case 0:
								if (GuiVar_index_set_config==0){// tempo assorbimento
									max_val = 120;
									if (touch_time <= 10) {
											GuiVar_set_page_1_val_1 = (GuiVar_set_page_1_val_1<max_val)? ++GuiVar_set_page_1_val_1 :max_val;
									} else {
										GuiVar_set_page_1_val_1 = (GuiVar_set_page_1_val_1<max_val-10)? GuiVar_set_page_1_val_1+10 :max_val;
									}
  							}
								else if (GuiVar_index_set_config==1){
									max_val = 80;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_1 = (GuiVar_set_page_2_val_1<max_val)? ++GuiVar_set_page_2_val_1 :max_val;
									} else {
										GuiVar_set_page_2_val_1 = (GuiVar_set_page_2_val_1<max_val-10)? GuiVar_set_page_2_val_1+10 :max_val;
									}
  							}
//								else if (GuiVar_index_set_config==2){// ritardo vacuum
//									max_val = 1200;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_1 = (GuiVar_set_page_3_val_1<max_val)? ++GuiVar_set_page_3_val_1 :max_val;
//									} else {
//										GuiVar_set_page_3_val_1 = (GuiVar_set_page_3_val_1<max_val-10)? GuiVar_set_page_3_val_1+10 :max_val;
//									}
//									if (GuiVar_set_page_3_val_1)
//								    GuiVar_index_Pr_1 = 1;
//									else
//								    GuiVar_index_Pr_1 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// timer errore sonda B2
//									max_val = 90;
//									if (touch_time <= 10) {
//											GuiVar_set_page_4_val_1 = (GuiVar_set_page_4_val_1<max_val)? ++GuiVar_set_page_4_val_1 :max_val;
//									} else {
//										GuiVar_set_page_4_val_1 = (GuiVar_set_page_4_val_1<max_val-10)? GuiVar_set_page_4_val_1+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer assorbimento
//									max_val = 900;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_1 = (GuiVar_set_page_5_val_1<max_val)? ++GuiVar_set_page_5_val_1 :max_val;
//									} else {
//										GuiVar_set_page_5_val_1 = (GuiVar_set_page_5_val_1<max_val-10)? GuiVar_set_page_5_val_1+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// inizio scala canale 1
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_1 = (GuiVar_set_page_6_val_1<max_val)? ++GuiVar_set_page_6_val_1 :max_val;
//									} else {
//										GuiVar_set_page_6_val_1 = (GuiVar_set_page_6_val_1<max_val-10)? GuiVar_set_page_6_val_1+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// inizio scala canale 4
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_1 = (GuiVar_set_page_7_val_1<max_val)? ++GuiVar_set_page_7_val_1 :max_val;
//									} else {
//										GuiVar_set_page_7_val_1 = (GuiVar_set_page_7_val_1<max_val-10)? GuiVar_set_page_7_val_1+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==7){// secondi/numero campioni media O2
//									max_val = 250;
//									if (touch_time <= 10) {
//											GuiVar_set_page_8_val_1 = (GuiVar_set_page_8_val_1<max_val)? ++GuiVar_set_page_8_val_1 :max_val;
//									} else {
//										GuiVar_set_page_8_val_1 = (GuiVar_set_page_8_val_1<max_val-10)? GuiVar_set_page_8_val_1+10 :max_val;
//									}
//  							}				
							  break;
								
							case 1: 
								if (GuiVar_index_set_config==0){// compensazione
									max_val = 120;
									if (touch_time <= 10) {
											GuiVar_set_page_1_val_2 = (GuiVar_set_page_1_val_2<max_val)? ++GuiVar_set_page_1_val_2 :max_val;
									} else {
										GuiVar_set_page_1_val_2 = (GuiVar_set_page_1_val_2<max_val-10)? GuiVar_set_page_1_val_2+10 :max_val;
									}
							  }
								else if (GuiVar_index_set_config==1){//
									max_val = 80;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_2 = (GuiVar_set_page_2_val_2<max_val)? ++GuiVar_set_page_2_val_2 :max_val;
									} else {
										GuiVar_set_page_2_val_2 = (GuiVar_set_page_2_val_2<max_val-10)? GuiVar_set_page_2_val_2+10 :max_val;
									}
  							}
//								else if (GuiVar_index_set_config==2){// pressione Air OK
//									max_val = 1200;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_2 = (GuiVar_set_page_3_val_2<max_val)? ++GuiVar_set_page_3_val_2 :max_val;
//									} else {
//										GuiVar_set_page_3_val_2 = (GuiVar_set_page_3_val_2<max_val-10)? GuiVar_set_page_3_val_2+10 :max_val;
//									}
//									if (GuiVar_set_page_3_val_2)
//								    GuiVar_index_Pr_2 = 1;
//									else
//								    GuiVar_index_Pr_2 = 0;  							}
//								else if (GuiVar_index_set_config==3){// timer errore sonda B4
//									max_val = 90;
//									if (touch_time <= 10) {
//											GuiVar_set_page_4_val_2 = (GuiVar_set_page_4_val_2<max_val)? ++GuiVar_set_page_4_val_2 :max_val;
//									} else {
//										GuiVar_set_page_4_val_2 = (GuiVar_set_page_4_val_2<max_val-10)? GuiVar_set_page_4_val_2+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer assorbimento iniziale
//									max_val = 900;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_2 = (GuiVar_set_page_5_val_2<max_val)? ++GuiVar_set_page_5_val_2 :max_val;
//									} else {
//										GuiVar_set_page_5_val_2 = (GuiVar_set_page_5_val_2<max_val-10)? GuiVar_set_page_5_val_2+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// fine scala canale 1
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_2 = (GuiVar_set_page_6_val_2<max_val)? ++GuiVar_set_page_6_val_2 :max_val;
//									} else {
//										GuiVar_set_page_6_val_2 = (GuiVar_set_page_6_val_2<max_val-10)? GuiVar_set_page_6_val_2+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// fine scala canale 4
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_2 = (GuiVar_set_page_7_val_2<max_val)? ++GuiVar_set_page_7_val_2 :max_val;
//									} else {
//										GuiVar_set_page_7_val_2 = (GuiVar_set_page_7_val_2<max_val-10)? GuiVar_set_page_7_val_2+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==7){// timer warm-up sensore O2 zirconio ppm
//									max_val = 900;
//									if (touch_time <= 10) {
//											GuiVar_set_page_8_val_2 = (GuiVar_set_page_8_val_2<max_val)? ++GuiVar_set_page_8_val_2 :max_val;
//									} else {
//										GuiVar_set_page_8_val_2 = (GuiVar_set_page_8_val_2<max_val-10)? GuiVar_set_page_8_val_2+10 :max_val;
//									}
//  							}
								break;
								
							case 2:
								if (GuiVar_index_set_config==0){// ritardo vacuum
									max_val = 30;
									if (touch_time <= 10) {
										GuiVar_set_page_1_val_3 = (GuiVar_set_page_1_val_3<max_val)? ++GuiVar_set_page_1_val_3 :max_val;
									} else {
										GuiVar_set_page_1_val_3 = (GuiVar_set_page_1_val_3<max_val-10)? GuiVar_set_page_1_val_3+10 :max_val;
									}
  							}
								else if (GuiVar_index_set_config==1){// pressione massima OUT 2
									max_val = 80;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_3 = (GuiVar_set_page_2_val_3<max_val)? ++GuiVar_set_page_2_val_3 :max_val;
									} else {
										GuiVar_set_page_2_val_3 = (GuiVar_set_page_2_val_3<max_val-10)? GuiVar_set_page_2_val_3+10 :max_val;
									}
  							}
//								else if (GuiVar_index_set_config==2){// pressione di processo min
//									max_val = 1200;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_3 = (GuiVar_set_page_3_val_3<max_val)? ++GuiVar_set_page_3_val_3 :max_val;
//									} else {
//										GuiVar_set_page_3_val_3 = (GuiVar_set_page_3_val_3<max_val-10)? GuiVar_set_page_3_val_3+10 :max_val;
//									}
//									if (GuiVar_set_page_3_val_3)
//								    GuiVar_index_Pr_3 = 1;
//									else
//								    GuiVar_index_Pr_3 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// numero blocchi valvole
//									max_val = 4;
//									GuiVar_set_page_4_val_3 = (GuiVar_set_page_4_val_3<max_val)? ++GuiVar_set_page_4_val_3 :max_val;
//  							}
//								else if (GuiVar_index_set_config==4){// timer compensazione
//									max_val = 100;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_3 = (GuiVar_set_page_5_val_3<max_val)? ++GuiVar_set_page_5_val_3 :max_val;
//									} else {
//										GuiVar_set_page_5_val_3 = (GuiVar_set_page_5_val_3<max_val-3)? GuiVar_set_page_5_val_3+3 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// inizio scala canale 2
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_3 = (GuiVar_set_page_6_val_3<max_val)? ++GuiVar_set_page_6_val_3 :max_val;
//									} else {
//										GuiVar_set_page_6_val_3 = (GuiVar_set_page_6_val_3<max_val-10)? GuiVar_set_page_6_val_3+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// inizio scala canale 5
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_3 = (GuiVar_set_page_7_val_3<max_val)? ++GuiVar_set_page_7_val_3 :max_val;
//									} else {
//										GuiVar_set_page_7_val_3 = (GuiVar_set_page_7_val_3<max_val-10)? GuiVar_set_page_7_val_3+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==7){// tempo massimo scarico (in minuti)
//									max_val = 60;
//									if (touch_time <= 10) {
//											GuiVar_set_page_8_val_3 = (GuiVar_set_page_8_val_3<max_val)? ++GuiVar_set_page_8_val_3 :max_val;
//									} else {
//										GuiVar_set_page_8_val_3 = (GuiVar_set_page_8_val_3<max_val-10)? GuiVar_set_page_8_val_3+10 :max_val;
//									}
//  							}
							  break;
								
							case 3:
								if (GuiVar_index_set_config==0){// ritardo vacuum
									max_val = 10;
									GuiVar_set_page_1_val_4 = (GuiVar_set_page_1_val_4<max_val)? ++GuiVar_set_page_1_val_4 :max_val;
  							}
								else if (GuiVar_index_set_config==1){// pressione minima OUT 2
									max_val = 70;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_4 = (GuiVar_set_page_2_val_4<max_val)? ++GuiVar_set_page_2_val_4 :max_val;
									} else {
										GuiVar_set_page_2_val_4 = (GuiVar_set_page_2_val_4<max_val-10)? GuiVar_set_page_2_val_4+10 :max_val;
									}
									if (GuiVar_set_page_2_val_4>=2){
										GuiVar_T_ventilazione_index = 2;
									}
									else{
										if (GuiVar_set_page_2_val_4==0){
											GuiVar_T_ventilazione_index = 0;
										}
										else{
											GuiVar_T_ventilazione_index = 1;
										}
									}									
  							}
//								else if (GuiVar_index_set_config==2){// pressione di processo OK
//									max_val = 1200;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_4 = (GuiVar_set_page_3_val_4<max_val)? ++GuiVar_set_page_3_val_4 :max_val;
//									} else {
//										GuiVar_set_page_3_val_4 = (GuiVar_set_page_3_val_4<max_val-10)? GuiVar_set_page_3_val_4+10 :max_val;
//									}
//									if (GuiVar_set_page_3_val_4)
//								    GuiVar_index_Pr_4 = 1;
//									else
//								    GuiVar_index_Pr_4 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// numero moduli
//									max_val = 4;
//									GuiVar_set_page_4_val_4 = (GuiVar_set_page_4_val_4<max_val)? ++GuiVar_set_page_4_val_4 :max_val;
//  							}
//								else if (GuiVar_index_set_config==4){// timer standby 1
//									max_val = 900;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_4 = (GuiVar_set_page_5_val_4<max_val)? ++GuiVar_set_page_5_val_4 :max_val;
//									} else {
//										GuiVar_set_page_5_val_4 = (GuiVar_set_page_5_val_4<max_val-10)? GuiVar_set_page_5_val_4+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// fine scala canale 2
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_4 = (GuiVar_set_page_6_val_4<max_val)? ++GuiVar_set_page_6_val_4 :max_val;
//									} else {
//										GuiVar_set_page_6_val_4 = (GuiVar_set_page_6_val_4<max_val-10)? GuiVar_set_page_6_val_4+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// fine scala canale 5
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_4 = (GuiVar_set_page_7_val_4<max_val)? ++GuiVar_set_page_7_val_4 :max_val;
//									} else {
//										GuiVar_set_page_7_val_4 = (GuiVar_set_page_7_val_4<max_val-10)? GuiVar_set_page_7_val_4+10 :max_val;
//									}
//  							}
							  break;
								
							case 4: 
								if (GuiVar_index_set_config==0){// indirizzo modbus
									max_val = 127;
									if (touch_time <= 10) {
										GuiVar_set_page_1_val_5 = (GuiVar_set_page_1_val_5<max_val)? ++GuiVar_set_page_1_val_5 :max_val;
									} else {
										GuiVar_set_page_1_val_5 = (GuiVar_set_page_1_val_5<max_val-3)? GuiVar_set_page_1_val_5+3 :max_val;
									}
  							}
								else if (GuiVar_index_set_config==1){
									max_val = 200;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_5 = (GuiVar_set_page_2_val_5<max_val)? ++GuiVar_set_page_2_val_5 :max_val;
									} else {
										GuiVar_set_page_2_val_5 = (GuiVar_set_page_2_val_5<max_val-10)? GuiVar_set_page_2_val_5+10 :max_val;
									}
  							}
//								else if ( (GuiVar_index_set_config==2) && (current_user.level>2) ){
//									max_val = 65000;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_5 = (GuiVar_set_page_3_val_5<max_val)? ++GuiVar_set_page_3_val_5 :max_val;
//									} else {
//										GuiVar_set_page_3_val_5 = (GuiVar_set_page_3_val_5<max_val-10)? GuiVar_set_page_3_val_5+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer standby 2
//									max_val = 900;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_5 = (GuiVar_set_page_5_val_5<max_val)? ++GuiVar_set_page_5_val_5 :max_val;
//									} else {
//										GuiVar_set_page_5_val_5 = (GuiVar_set_page_5_val_5<max_val-10)? GuiVar_set_page_5_val_5+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// inizio scala canale 3
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_5 = (GuiVar_set_page_6_val_5<max_val)? ++GuiVar_set_page_6_val_5 :max_val;
//									} else {
//										GuiVar_set_page_6_val_5 = (GuiVar_set_page_6_val_5<max_val-10)? GuiVar_set_page_6_val_5+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// inizio scala canale 6
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_5 = (GuiVar_set_page_7_val_5<max_val)? ++GuiVar_set_page_7_val_5 :max_val;
//									} else {
//										GuiVar_set_page_7_val_5 = (GuiVar_set_page_7_val_5<max_val-10)? GuiVar_set_page_7_val_5+10 :max_val;
//									}
//  							}
							  break;
								
							case 5:
								if (GuiVar_index_set_config==0){// // indirizzo CAN
									max_val = 29;
									if (touch_time <= 10) {
										GuiVar_set_page_1_val_6 = (GuiVar_set_page_1_val_6<max_val)? ++GuiVar_set_page_1_val_6 :max_val;
									} else {
										GuiVar_set_page_1_val_6 = (GuiVar_set_page_1_val_6<max_val-3)? GuiVar_set_page_1_val_6+3 :max_val;
									}
									if (GuiVar_set_page_1_val_6==0)
										GuiVar_index_id_rete_CAN = 0;
									else if (GuiVar_set_page_1_val_6<29)
										GuiVar_index_id_rete_CAN = 1;
									else
										GuiVar_index_id_rete_CAN = 2;									
  							}
								else if (GuiVar_index_set_config==1){//
									max_val = 65000;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_6 = (GuiVar_set_page_2_val_6<max_val)? ++GuiVar_set_page_2_val_6 :max_val;
									} else {
										GuiVar_set_page_2_val_6 = (GuiVar_set_page_2_val_6<max_val-10)? GuiVar_set_page_2_val_6+10 :max_val;
									}
  							}
//								else if ( (GuiVar_index_set_config==2) && (current_user.level>2) ){// ore lavoro OUT 2
//									max_val = 65000;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_6 = (GuiVar_set_page_3_val_6<max_val)? ++GuiVar_set_page_3_val_6 :max_val;
//									} else {
//										GuiVar_set_page_3_val_6 = (GuiVar_set_page_3_val_6<max_val-10)? GuiVar_set_page_3_val_6+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer avvio PSA
//									max_val = 500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_6 = (GuiVar_set_page_5_val_6<max_val)? ++GuiVar_set_page_5_val_6 :max_val;
//									} else {
//										GuiVar_set_page_5_val_6 = (GuiVar_set_page_5_val_6<max_val-10)? GuiVar_set_page_5_val_6+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// fine scala canale 3
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_6 = (GuiVar_set_page_6_val_6<max_val)? ++GuiVar_set_page_6_val_6 :max_val;
//									} else {
//										GuiVar_set_page_6_val_6 = (GuiVar_set_page_6_val_6<max_val-10)? GuiVar_set_page_6_val_6+10 :max_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// fine scala canale 6
//									max_val = 2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_6 = (GuiVar_set_page_7_val_6<max_val)? ++GuiVar_set_page_7_val_6 :max_val;
//									} else {
//										GuiVar_set_page_7_val_6 = (GuiVar_set_page_7_val_6<max_val-10)? GuiVar_set_page_7_val_6+10 :max_val;
//									}
//  							}
							  break;
								
							default:
								break;
						}
					break;			
						
					// decremento variabile
					case 536:
						switch (GuiVar_selected_option)
						{
							case 0: 
								if (GuiVar_index_set_config==0){// assorbimento
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_1_val_1 = (GuiVar_set_page_1_val_1>min_val)? --GuiVar_set_page_1_val_1 :min_val;
									} else {
										GuiVar_set_page_1_val_1 = (GuiVar_set_page_1_val_1>min_val+10)? GuiVar_set_page_1_val_1-10 :min_val;
									}
  							}
								else if (GuiVar_index_set_config==1){//
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_1 = (GuiVar_set_page_2_val_1>min_val)? --GuiVar_set_page_2_val_1 :min_val;
									} else {
										GuiVar_set_page_2_val_1 = (GuiVar_set_page_2_val_1>min_val+10)? GuiVar_set_page_2_val_1-10 :min_val;
									}
  							}
//								else if (GuiVar_index_set_config==2){// pressione Air min
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_1 = (GuiVar_set_page_3_val_1>min_val)? --GuiVar_set_page_3_val_1 :min_val;
//									} else {
//										GuiVar_set_page_3_val_1 = (GuiVar_set_page_3_val_1>min_val+10)? GuiVar_set_page_3_val_1-10 :min_val;
//									}
//									if (GuiVar_set_page_3_val_1)
//								    GuiVar_index_Pr_1 = 1;
//									else
//								    GuiVar_index_Pr_1 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// timer errore sonda B2
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_4_val_1 = (GuiVar_set_page_4_val_1>min_val)? --GuiVar_set_page_4_val_1 :min_val;
//									} else {
//										GuiVar_set_page_4_val_1 = (GuiVar_set_page_4_val_1>min_val+10)? GuiVar_set_page_4_val_1-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer assorbimento
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_1 = (GuiVar_set_page_5_val_1>min_val)? --GuiVar_set_page_5_val_1 :min_val;
//									} else {
//										GuiVar_set_page_5_val_1 = (GuiVar_set_page_5_val_1>min_val+10)? GuiVar_set_page_5_val_1-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// inizio scala canale 1
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_1 = (GuiVar_set_page_6_val_1>min_val)? --GuiVar_set_page_6_val_1 :min_val;
//									} else {
//										GuiVar_set_page_6_val_1 = (GuiVar_set_page_6_val_1>min_val+10)? GuiVar_set_page_6_val_1-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// inizio scala canale 4
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_1 = (GuiVar_set_page_7_val_1>min_val)? --GuiVar_set_page_7_val_1 :min_val;
//									} else {
//										GuiVar_set_page_7_val_1 = (GuiVar_set_page_7_val_1>min_val+10)? GuiVar_set_page_7_val_1-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==7){// secondi/numero campioni media O2
//									min_val = 1;
//									if (touch_time <= 10) {
//											GuiVar_set_page_8_val_1 = (GuiVar_set_page_8_val_1>min_val)? --GuiVar_set_page_8_val_1 :min_val;
//									} else {
//										GuiVar_set_page_8_val_1 = (GuiVar_set_page_8_val_1>min_val+10)? GuiVar_set_page_8_val_1-10 :min_val;
//									}
//  							}
							  break;
								
							case 1: 
								if (GuiVar_index_set_config==0){// compensazione
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_1_val_2 = (GuiVar_set_page_1_val_2>min_val)? --GuiVar_set_page_1_val_2 :min_val;
									} else {
										GuiVar_set_page_1_val_2 = (GuiVar_set_page_1_val_2>min_val+10)? GuiVar_set_page_1_val_2-10 :min_val;
									}
  							}
								else if (GuiVar_index_set_config==1){
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_2 = (GuiVar_set_page_2_val_2>min_val)? --GuiVar_set_page_2_val_2 :min_val;
									} else {
										GuiVar_set_page_2_val_2 = (GuiVar_set_page_2_val_2>min_val+10)? GuiVar_set_page_2_val_2-10 :min_val;
									}
  							}
//								else if (GuiVar_index_set_config==2){// pressione Air OK
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_2 = (GuiVar_set_page_3_val_2>min_val)? --GuiVar_set_page_3_val_2 :min_val;
//									} else {
//										GuiVar_set_page_3_val_2 = (GuiVar_set_page_3_val_2>min_val+10)? GuiVar_set_page_3_val_2-10 :min_val;
//									}
//									if (GuiVar_set_page_3_val_2)
//								    GuiVar_index_Pr_2 = 1;
//									else
//								    GuiVar_index_Pr_2 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// timer errore sonda B4
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_4_val_2 = (GuiVar_set_page_4_val_2>min_val)? --GuiVar_set_page_4_val_2 :min_val;
//									} else {
//										GuiVar_set_page_4_val_2 = (GuiVar_set_page_4_val_2>min_val+10)? GuiVar_set_page_4_val_2-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer assorbimento iniziale
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_2 = (GuiVar_set_page_5_val_2>min_val)? --GuiVar_set_page_5_val_2 :min_val;
//									} else {
//										GuiVar_set_page_5_val_2 = (GuiVar_set_page_5_val_2>min_val+10)? GuiVar_set_page_5_val_2-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// fine scala canale 1
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_2 = (GuiVar_set_page_6_val_2>min_val)? --GuiVar_set_page_6_val_2 :min_val;
//									} else {
//										GuiVar_set_page_6_val_2 = (GuiVar_set_page_6_val_2>min_val+10)? GuiVar_set_page_6_val_2-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// fine scala canale 4
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_2 = (GuiVar_set_page_7_val_2>min_val)? --GuiVar_set_page_7_val_2 :min_val;
//									} else {
//										GuiVar_set_page_7_val_2 = (GuiVar_set_page_7_val_2>min_val+10)? GuiVar_set_page_7_val_2-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==7){// timer warm-up sensore O2 zirconio ppm
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_8_val_2 = (GuiVar_set_page_8_val_2>min_val)? --GuiVar_set_page_8_val_2 :min_val;
//									} else {
//										GuiVar_set_page_8_val_2 = (GuiVar_set_page_8_val_2>min_val+10)? GuiVar_set_page_8_val_2-10 :min_val;
//									}
//  							}
							  break;
								
							case 2:
								if (GuiVar_index_set_config==0){// ritardo vacuum
									min_val = 0;
									if (touch_time <= 10) {
										GuiVar_set_page_1_val_3 = (GuiVar_set_page_1_val_3>min_val)? --GuiVar_set_page_1_val_3 :min_val;
									} else {
										GuiVar_set_page_1_val_3 = (GuiVar_set_page_1_val_3>min_val+10)? GuiVar_set_page_1_val_3-10 :min_val;
									}
  							}
								else if (GuiVar_index_set_config==1){
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_3 = (GuiVar_set_page_2_val_3>min_val)? --GuiVar_set_page_2_val_3 :min_val;
									} else {
										GuiVar_set_page_2_val_3 = (GuiVar_set_page_2_val_3>min_val+10)? GuiVar_set_page_2_val_3-10 :min_val;
									}
  							}
//								else if (GuiVar_index_set_config==2){// pressione di processo min
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_3 = (GuiVar_set_page_3_val_3>min_val)? --GuiVar_set_page_3_val_3 :min_val;
//									} else {
//										GuiVar_set_page_3_val_3 = (GuiVar_set_page_3_val_3>min_val+10)? GuiVar_set_page_3_val_3-10 :min_val;
//									}
//									if (GuiVar_set_page_3_val_3)
//								    GuiVar_index_Pr_3 = 1;
//									else
//								    GuiVar_index_Pr_3 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// numero blocchi valvole
//									min_val = 1;
//									GuiVar_set_page_4_val_3 = (GuiVar_set_page_4_val_3>min_val)? --GuiVar_set_page_4_val_3 :min_val;
//  							}
//								else if (GuiVar_index_set_config==4){// timer compensazione
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_3 = (GuiVar_set_page_5_val_3>min_val)? --GuiVar_set_page_5_val_3 :min_val;
//									} else {
//										GuiVar_set_page_5_val_3 = (GuiVar_set_page_5_val_3>min_val+3)? GuiVar_set_page_5_val_3-3 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// inizio scala canale 2
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_3 = (GuiVar_set_page_6_val_3>min_val)? --GuiVar_set_page_6_val_3 :min_val;
//									} else {
//										GuiVar_set_page_6_val_3 = (GuiVar_set_page_6_val_3>min_val+10)? GuiVar_set_page_6_val_3-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// inizio scala canale 5
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_3 = (GuiVar_set_page_7_val_3>min_val)? --GuiVar_set_page_7_val_3 :min_val;
//									} else {
//										GuiVar_set_page_7_val_3 = (GuiVar_set_page_7_val_3>min_val+10)? GuiVar_set_page_7_val_3-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==7){// tempo massimo scarico (in minuti)
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_8_val_3 = (GuiVar_set_page_8_val_3>min_val)? --GuiVar_set_page_8_val_3 :min_val;
//									} else {
//										GuiVar_set_page_8_val_3 = (GuiVar_set_page_8_val_3>min_val+10)? GuiVar_set_page_8_val_3-10 :min_val;
//									}
//  							}
							  break;
								
							case 3:
								if (GuiVar_index_set_config==0){// ritardo restart
									min_val = 0;
									GuiVar_set_page_1_val_4 = (GuiVar_set_page_1_val_4>min_val)? --GuiVar_set_page_1_val_4 :min_val;
  							}
								else if (GuiVar_index_set_config==1){
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_4 = (GuiVar_set_page_2_val_4>min_val)? --GuiVar_set_page_2_val_4 :min_val;
									} else {
										GuiVar_set_page_2_val_4 = (GuiVar_set_page_2_val_4>min_val+10)? GuiVar_set_page_2_val_4-10 :min_val;
									}
 									if (GuiVar_set_page_2_val_4>=2){
										GuiVar_T_ventilazione_index = 2;
									}
									else{
										if (GuiVar_set_page_2_val_4==0){
											GuiVar_T_ventilazione_index = 0;
										}
										else{
											GuiVar_T_ventilazione_index = 1;
										}
									}									
  							}

//								else if (GuiVar_index_set_config==2){// pressione di processo OK
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_4 = (GuiVar_set_page_3_val_4>min_val)? --GuiVar_set_page_3_val_4 :min_val;
//									} else {
//										GuiVar_set_page_3_val_4 = (GuiVar_set_page_3_val_4>min_val+10)? GuiVar_set_page_3_val_4-10 :min_val;
//									}
//									if (GuiVar_set_page_3_val_4)
//								    GuiVar_index_Pr_4 = 1;
//									else
//								    GuiVar_index_Pr_4 = 0;
//  							}
//								else if (GuiVar_index_set_config==3){// numero moduli
//									min_val = 1;
//									GuiVar_set_page_4_val_4 = (GuiVar_set_page_4_val_4>min_val)? --GuiVar_set_page_4_val_4 :min_val;
//									if (GuiVar_set_page_4_val_4>2)
//  									GuiVar_Modulo_4_abilitato = 0;
//									else if (GuiVar_set_page_4_val_4>1)
//  									GuiVar_Modulo_3_abilitato = 0;
//									else
//  									GuiVar_Modulo_2_abilitato = 0;
//  							}
//								else if (GuiVar_index_set_config==4){// timer standby 1
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_4 = (GuiVar_set_page_5_val_4>min_val)? --GuiVar_set_page_5_val_4 :min_val;
//									} else {
//										GuiVar_set_page_5_val_4 = (GuiVar_set_page_5_val_4>min_val+10)? GuiVar_set_page_5_val_4-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// fine scala canale 2
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_4 = (GuiVar_set_page_6_val_4>min_val)? --GuiVar_set_page_6_val_4 :min_val;
//									} else {
//										GuiVar_set_page_6_val_4 = (GuiVar_set_page_6_val_4>min_val+10)? GuiVar_set_page_6_val_4-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// fine scala canale 5
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_4 = (GuiVar_set_page_7_val_4>min_val)? --GuiVar_set_page_7_val_4 :min_val;
//									} else {
//										GuiVar_set_page_7_val_4 = (GuiVar_set_page_7_val_4>min_val+10)? GuiVar_set_page_7_val_4-10 :min_val;
//									}
//  							}
							  break;
								
							case 4: 
								if (GuiVar_index_set_config==0){// indirizzo modbus
									min_val = 1;
									if (touch_time <= 10) {
										GuiVar_set_page_1_val_5 = (GuiVar_set_page_1_val_5>min_val)? --GuiVar_set_page_1_val_5 :min_val;
									} else {
										GuiVar_set_page_1_val_5 = (GuiVar_set_page_1_val_5>min_val+3)? GuiVar_set_page_1_val_5-3 :min_val;
									}
  							}
								else if (GuiVar_index_set_config==1){
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_5 = (GuiVar_set_page_2_val_5>min_val)? --GuiVar_set_page_2_val_5 :min_val;
									} else {
										GuiVar_set_page_2_val_5 = (GuiVar_set_page_2_val_5>min_val+10)? GuiVar_set_page_2_val_5-10 :min_val;
									}
  							}
//								else if ( (GuiVar_index_set_config==2) && (current_user.level>2) ){// ore lavoro OUT 1
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_5 = (GuiVar_set_page_3_val_5>min_val)? --GuiVar_set_page_3_val_5 :min_val;
//									} else {
//										GuiVar_set_page_3_val_5 = (GuiVar_set_page_3_val_5>min_val+10)? GuiVar_set_page_3_val_5-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer standby 2
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_5 = (GuiVar_set_page_5_val_5>min_val)? --GuiVar_set_page_5_val_5 :min_val;
//									} else {
//										GuiVar_set_page_5_val_5 = (GuiVar_set_page_5_val_5>min_val+10)? GuiVar_set_page_5_val_5-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// inizio scala canale 3
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_5 = (GuiVar_set_page_6_val_5>min_val)? --GuiVar_set_page_6_val_5 :min_val;
//									} else {
//										GuiVar_set_page_6_val_5 = (GuiVar_set_page_6_val_5>min_val+10)? GuiVar_set_page_6_val_5-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// inizio scala canale 6
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_5 = (GuiVar_set_page_7_val_5>min_val)? --GuiVar_set_page_7_val_5 :min_val;
//									} else {
//										GuiVar_set_page_7_val_5 = (GuiVar_set_page_7_val_5>min_val+10)? GuiVar_set_page_7_val_5-10 :min_val;
//									}
//  							}
							  break;
								
							case 5:
								if (GuiVar_index_set_config==0){// indirizzo CAN
									min_val = 0;
									if (touch_time <= 10) {
										GuiVar_set_page_1_val_6 = (GuiVar_set_page_1_val_6>min_val)? --GuiVar_set_page_1_val_6 :min_val;
									} else {
										GuiVar_set_page_1_val_6 = (GuiVar_set_page_1_val_6>min_val+3)? GuiVar_set_page_1_val_6-3 :min_val;
									}
									if (GuiVar_set_page_1_val_6==0)
										GuiVar_index_id_rete_CAN = 0;
									else if (GuiVar_set_page_1_val_6<29)
										GuiVar_index_id_rete_CAN = 1;
									else
										GuiVar_index_id_rete_CAN = 2;																		
  							}
								else if (GuiVar_index_set_config==1){// 
									min_val = 0;
									if (touch_time <= 10) {
											GuiVar_set_page_2_val_6 = (GuiVar_set_page_2_val_6>min_val)? --GuiVar_set_page_2_val_6 :min_val;
									} else {
										GuiVar_set_page_2_val_6 = (GuiVar_set_page_2_val_6>min_val+10)? GuiVar_set_page_2_val_6-10 :min_val;
									}
  							}
//								else if ( (GuiVar_index_set_config==2) && (current_user.level>2) ){// ore lavoro OUT 2
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_3_val_6 = (GuiVar_set_page_3_val_6>min_val)? --GuiVar_set_page_3_val_6 :min_val;
//									} else {
//										GuiVar_set_page_3_val_6 = (GuiVar_set_page_3_val_6>min_val+10)? GuiVar_set_page_3_val_6-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==4){// timer avvio PSA
//									min_val = 0;
//									if (touch_time <= 10) {
//											GuiVar_set_page_5_val_6 = (GuiVar_set_page_5_val_6>min_val)? --GuiVar_set_page_5_val_6 :min_val;
//									} else {
//										GuiVar_set_page_5_val_6 = (GuiVar_set_page_5_val_6>min_val+10)? GuiVar_set_page_5_val_6-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==5){// fine scala canale 3
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_6_val_6 = (GuiVar_set_page_6_val_6>min_val)? --GuiVar_set_page_6_val_6 :min_val;
//									} else {
//										GuiVar_set_page_6_val_6 = (GuiVar_set_page_6_val_6>min_val+10)? GuiVar_set_page_6_val_6-10 :min_val;
//									}
//  							}
//								else if (GuiVar_index_set_config==6){// fine scala canale 6
//									min_val = -2500;
//									if (touch_time <= 10) {
//											GuiVar_set_page_7_val_6 = (GuiVar_set_page_7_val_6>min_val)? --GuiVar_set_page_7_val_6 :min_val;
//									} else {
//										GuiVar_set_page_7_val_6 = (GuiVar_set_page_7_val_6>min_val+10)? GuiVar_set_page_7_val_6-10 :min_val;
//									}
//  							}
							  break;
								
							default:
								break;
						}
						break;	
						
		} // Fine switch touch area
	}
}
/***********************************************************************
 * @brief		check_ADOX_leds			
	 controlla l'accensione e lo spegnimento dei leds della macchina
 **********************************************************************/
void check_ADOX_leds (void) {
		
}




/***********************************************************************
 * @brief		touch_release	
	 \param [in] area attiva rilasciata
	 codice da eseguire al rilascio di un'area attiva (gi� premuta in precedenza)
 **********************************************************************/
void touch_release (unsigned int touch_area) { 
		
		unsigned int i = 0;

		// questa serve per capire da quanto si stava premendo
		touch_time = 0;
		
		// questa � per identificare cosa era gi� premuto
		cornice = 0;
	
		if ( (GuiVar_message == 0) || (touch_area == 22) || (touch_area == 23) || (touch_area == 61)) {
//		  tmr_user_press_button = TMR_USER_PRESS_BUTTON;	
// 20201002      
    new_value_to_display = 1;	
			
		switch (touch_area)
		{
// 20201002
			default:
        new_value_to_display = 0;
			break;
			      
			// RELEASE SCREEN CHARTS
			case 0: 
					if (GuiVar_screen_select == 7) {
//							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					}
			break;	

			// RELEASE SCREEN SET
			case 1:
// 20201002                            
#ifndef UPDATE_SCREEN_ON_PRESS          
				if (GuiVar_button_set != 2) {
					if (GuiVar_screen_select == 1) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(1);
							GuiVar_screen_select = 1;
						  if (current_user.level > 1){
						    GuiVar_index_show_radio = 1;
                GuiVar_selected_option = 0;
							}						
						  else{
								GuiVar_index_show_radio = 0;
                GuiVar_selected_option = 7;
							}						
						  GuiVar_index_set_config = 0;						
							GuiVar_set_page_1_val_1 = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA]/10;
							GuiVar_set_page_1_val_2 = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA]/10;
							GuiVar_set_page_1_val_3 = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM];
							GuiVar_set_page_1_val_4 = E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START];
							GuiVar_set_page_1_val_5 = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1];
						  GuiVar_set_page_1_val_6 = E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1];					
  					  if (GuiVar_set_page_1_val_6==0)
								GuiVar_index_id_rete_CAN = 0;
  					  else if (GuiVar_set_page_1_val_6<29)
								GuiVar_index_id_rete_CAN = 1;
							else
								GuiVar_index_id_rete_CAN = 2;
							GuiVar_set_page_2_val_1 = E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]/10;
							GuiVar_set_page_2_val_2 = E2prom[E2_PAGE_2].E2_Int[E2_2_TOO_HIGH_TEMP]/10;
							GuiVar_set_page_2_val_3 = E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP]/10;
							if (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]>=20){
								GuiVar_T_ventilazione_index = 2;
							  GuiVar_set_page_2_val_4 = E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]/10;
							}
							else{
								if (E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]==0){
  								GuiVar_T_ventilazione_index = 0;
									GuiVar_set_page_2_val_4 = 0;
								}
								else{
  								GuiVar_T_ventilazione_index = 1;
									GuiVar_set_page_2_val_4 = 1;
								}
							}
							GuiVar_set_page_2_val_5 = E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2];
						  GuiVar_set_page_2_val_6 = E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2];				
							
							init_screen(SET_SCREEN);
					}					
				}
#endif         
			break;
			 
			// RELEASE SCREEN CALIBRATION 
			case 2:  //ver 1.05
// 20200831                            
#ifndef UPDATE_SCREEN_ON_PRESS            
				if (GuiVar_button_th != 2) {
					if (GuiVar_screen_select == 2) {
							press_button(SYSTEM_SCREEN);
							GuiVar_screen_select = SYSTEM_SCREEN;
							init_screen(SYSTEM_SCREEN);
					}
					
					else if(current_user.level > 1) {
							press_button(2);							
							GuiVar_screen_select = 2;
							init_screen(2);
					}
				}
#endif      
			break;			
			
			// RELEASE SCREEN LOGIN/LOGOUT 		
			case 3: 
// 20201002                            
#ifndef UPDATE_SCREEN_ON_PRESS         
				if (GuiVar_button_log != 2) {
					if (GuiVar_screen_select == 3) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(3);
							GuiVar_screen_select = 3;
							init_screen(LOG_SCREEN);
					}	
				}		
#endif			
			break;					
			
			// RELEASE SCREEN OPTIONS 
			case 4: 
// 20201002                            
#ifndef UPDATE_SCREEN_ON_PRESS          
				if (GuiVar_button_options != 2) {
					if (GuiVar_screen_select == 4) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							/* LOAD OPTIONS SCREEN*/
							press_button(4);
							GuiVar_screen_select = 4;
							init_screen(OPTIONS_SCREEN);
					}		
				}	
#endif    
			break;
			
			// RELEASE SCREEN EVENTS 
			case 5: 
// 20201002                            
#ifndef UPDATE_SCREEN_ON_PRESS         
				if (GuiVar_button_events != 2) {
					if (GuiVar_screen_select == 5) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(5);
							GuiVar_screen_select = 5;
							init_screen(EVENTS_SCREEN);
							new_fault = 0;
					}	
				}					
#endif    
			break;		

			// RELEASE SCREEN INFO 
			case 6:
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS           
				if(current_user.level >= 3){//Sebastian prima diceva che la calibrazione dei sensori doveva essere a livello 3, poi mi ha chiesto di metterla a livello 2 poi di nuovo a livello 3 (2017.08.30)
				  GuiVar_screen_select = 15;
					init_screen(SYSTEM_SCREEN);
					daily_reset_counter = 0;
					//salva su RAM tamponata del RTC il nuovo valore 
					RTC_WriteGPREG(LPC_RTC,0,daily_reset_counter);
		    }
				else {
					if (GuiVar_screen_select == 6) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					 } else {
							press_button(6);
							GuiVar_screen_select = 6;
							init_screen(INFO_SCREEN);
					 }
				}
#endif      
			break;	

			// RELEASE SCREEN CHARTS 
			case 7: 
//				if (GuiVar_button_alarms != 2) {
//					if ((GuiVar_screen_select == 7) || (GuiVar_screen_select == 11)) {
//							press_button(0);
//							GuiVar_screen_select = 0;
//							init_screen(SYSTEM_SCREEN);
//					} else {
//							press_button(7);
							GuiVar_screen_select = 7;
							init_screen(CHARTS_SCREEN);
//					}		
//				}
			break;	

			// RELEASE SCREEN FAULTS 
			case 8: 
				if (GuiVar_button_faults != 2) {
					if (GuiVar_screen_select == 8) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(8);
							GuiVar_screen_select = 8;
							init_screen(FAULTS_SCREEN);
					}	
				}					
			break;	

			// RELEASE SCREEN DISABLEMENTS
			case 9: 
				if ((GuiVar_button_disablements != 2) ) {
					if ( (GuiVar_screen_select == 9) || (GuiVar_screen_select == 14) ) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(9);
							GuiVar_screen_select = 9;
							init_screen(DISABLEMENTS_SCREEN);
					}	
				}					
			break;	

			// RELEASE SCREEN DEVICES CTRL
			case 10: 
				if (GuiVar_button_dev_ctrl != 2) {
					if ((GuiVar_screen_select == 10) || (GuiVar_screen_select == 13) ) {
							press_button(0);
							GuiVar_screen_select = 0;
							init_screen(SYSTEM_SCREEN);
					} else {
							press_button(10);
							GuiVar_screen_select = 10;
							init_screen(DEV_CTRL_SCREEN);
					}	
				}					
			break;	

//			// RELEASE DEC SET_DAY
//			case 11:
//						GuiVar_day_color = DARK_BLUE;
//			break;
//			
//			// RELEASE INC SET_DAY
//			case 12:
//						GuiVar_day_color = DARK_BLUE;
//			break;		

//			// RELEASE DEC SET_MONTH
//			case 13:
//						GuiVar_month_color = DARK_BLUE;
//			break;
//			
//			// RELEASE INC SET_MONTH
//			case 14:
//						GuiVar_month_color = DARK_BLUE;
//			break;	

//			// RELEASE DEC SET_YEAR
//			case 15:
//						GuiVar_year_color = DARK_BLUE;
//			break;
//			
//			// RELEASE INC SET_YEAR
//			case 16:
//						GuiVar_year_color = DARK_BLUE;
//			break;			

//			// RELEASE DEC SET_HOUR
//			case 17:
//						GuiVar_hour_color = DARK_BLUE;
//			break;
//			
//			// RELEASE INC SET_HOUR
//			case 18:
//						GuiVar_hour_color = DARK_BLUE;
//			break;		

//			// RELEASE DEC SET_MINUTES
//			case 19:
//						GuiVar_minutes_color = DARK_BLUE;
//			break;
//			
//			// RELEASE INC SET_MINUTES
//			case 20:
//						GuiVar_minutes_color = DARK_BLUE;
//			break;	
				
			// RELEASE SAVE
			case 21: 
				// MESSAGE "SAVE ?"
				if (GuiVar_button_save != 2) {
						GuiVar_button_save = 0;
					
						switch (GuiVar_screen_select)
						{
							case 1: // SET		
	              if (current_user.level>1){//2018.02.22
								  GuiVar_message = 3; // Save changes ?
								}
								break;
								
							case 2: // THRESHOLDS
							case 13: // DEV_CTRL List
							case 14: // DISABLEMENTS List
								GuiVar_message = 3; // Save changes ?
							break;
							
							case 4: // OPTIONS
								// Save Date & Time
								rtc_set_time ( GuiVar_set_year, GuiVar_set_month, GuiVar_set_day, GuiVar_set_hour, GuiVar_set_minutes, 0);
								
								// Save backlight
								if (backlight!=GuiVar_set_backlight)
								  setbacklight = 1;
								
								eeprom_write_word(E2PROM_PAGE_OF_OTHER,REG_LANGUAGE*2,GuiVar_sys_language);
								backlight = GuiVar_set_backlight;
								eeprom_write_word(E2PROM_PAGE_OF_OTHER,REG_BACKLIGHT*2,backlight);
								
								GuiVar_message = 0;
//								if (alarms_number != 0) {
//									if (GuiVar_screen_select != ALARMS_SCREEN) {
//										GuiVar_screen_select = ALARMS_SCREEN;
//										init_screen(ALARMS_SCREEN);
//										press_button(7);
//									}
//								} else {
									if (GuiVar_screen_select != SYSTEM_SCREEN) {
										GuiVar_screen_select = SYSTEM_SCREEN;
										init_screen(SYSTEM_SCREEN);
										press_button(0);
									}
//								}	
							break;							
						}
				}
			break;			

			// RELEASE YES (SAVE)
			case 22: 
				if (GuiVar_yes_button != 2) {
					GuiVar_yes_button = 0;
					if (GuiVar_message != 10)
							switch (GuiVar_screen_select){
							{
								/* SAVE SET */
								case SET_SCREEN:
									switch (GuiVar_index_set_config){
								    case 0://pagina 1
											E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA] = (unsigned int)GuiVar_set_page_1_val_1*10;
										  eeprom_write_word(E2_PAGE_2,E2_2_TA_VSAPSA*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA]);
											Adox1500.Assorbimento = E2prom[E2_PAGE_2].E2_Int[E2_2_TA_VSAPSA];
											E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA] = (unsigned int)GuiVar_set_page_1_val_2*10;
										  eeprom_write_word(E2_PAGE_2,E2_2_TC_VSAPSA*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA]);								
											Adox1500.Compensazione = E2prom[E2_PAGE_2].E2_Int[E2_2_TC_VSAPSA];
										  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM] = GuiVar_set_page_1_val_3;
										  eeprom_write_word(E2_PAGE_2,E2_2_TMR_RIT_VACUUM*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_VACUUM]);
										  E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START] = GuiVar_set_page_1_val_4;
										  eeprom_write_word(E2_PAGE_2,E2_2_TMR_RIT_START*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TMR_RIT_START]);
											E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1] = GuiVar_set_page_1_val_5;
										  eeprom_write_word(E2_PAGE_3,E2_3_ID_RS485_1*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_1]);
											E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_2] = GuiVar_set_page_1_val_5;										
										  eeprom_write_word(E2_PAGE_3,E2_3_ID_RS485_2*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ID_RS485_2]);
											E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1] = GuiVar_set_page_1_val_6;
										  eeprom_write_word(E2_PAGE_3,E2_3_ID_CAN_1*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_1]);
											E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_2] = GuiVar_set_page_1_val_6;
										  eeprom_write_word(E2_PAGE_3,E2_3_ID_CAN_2*2,E2prom[E2_PAGE_3].E2_Int[E2_3_ID_CAN_2]);
										break;
										
								    case 1://pagina 2
											E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM] = GuiVar_set_page_2_val_1*10; //temperatura aria compressa - allarme T2 aria
											eeprom_write_word(E2_PAGE_2,E2_2_TEMP_ALARM*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TEMP_ALARM]);
											E2prom[E2_PAGE_2].E2_Int[E2_2_TOO_HIGH_TEMP] = GuiVar_set_page_2_val_2*10; //temperatura blocco macchina - T1 blocco/stop
											eeprom_write_word(E2_PAGE_2,E2_2_TOO_HIGH_TEMP*2,E2prom[E2_PAGE_2].E2_Int[E2_2_TOO_HIGH_TEMP]);
											E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP] = GuiVar_set_page_2_val_3*10; //alert temperatura cabinet - T1 avviso/alert
											eeprom_write_word(E2_PAGE_2,E2_2_HIGH_TEMP*2,E2prom[E2_PAGE_2].E2_Int[E2_2_HIGH_TEMP]);
										  if (GuiVar_set_page_2_val_4<2)
											  E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP] = GuiVar_set_page_2_val_4; //OFF or R-1											
											else
												E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP] = GuiVar_set_page_2_val_4*10; //temperatura avvio ventilazione											
											eeprom_write_word(E2_PAGE_2,E2_2_VENTIL_TEMP*2,E2prom[E2_PAGE_2].E2_Int[E2_2_VENTIL_TEMP]);
											E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2] = GuiVar_set_page_2_val_5; //Press. stop
											eeprom_write_word(E2_PAGE_2,E2_2_PRESS_START_AL2*2,E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_START_AL2]);
											E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2] = GuiVar_set_page_2_val_6;  //Press. start
											eeprom_write_word(E2_PAGE_2,E2_2_PRESS_STOP_AL2*2,E2prom[E2_PAGE_2].E2_Int[E2_2_PRESS_STOP_AL2]);
                      break;
										
								    default:
                      break;
									}
									GuiVar_message = 0;
									GuiVar_index_indice_pagina = 0;
									GuiVar_index_set_config = 0;

								  if (GuiVar_screen_select != SYSTEM_SCREEN) {
										GuiVar_screen_select = SYSTEM_SCREEN;
										init_screen(SYSTEM_SCREEN);
										press_button(0);
									}
								break;
								
								/* SAVE THRESHOLDS */
								case TH_SCREEN:
										// Se nessun sensore � selezionato cosa salvi a fare? manda messaggio val�
										if (no_sensors_selected(CHANGE_TH) == 1) {
												GuiVar_message = 4;
												message_timer_count = MESSAGE_ON_SCREEN_TIME;
										} else {
										    // invece se qualcosa � stato selezionato, salva
												for (i = 1; i < (sys_sensors_num + 1); i++ ){
													if (sensors_change_th[i] == 1) {
														// devo fare questo ritocco altrimenti easyGUI arrotonda come vuole, ma pensa te
														sensors_array[i].th_high  = (GuiVar_th_new_high  + 0.001);
														sensors_array[i].th_low   = (GuiVar_th_new_low   + 0.001);
														
														// Salva in modo permanente (EEPROM)
														temp = (sensors_array[i].th_high * 10);
														eeprom_write_byte(3,i, temp);
														temp = (sensors_array[i].th_low * 10);
														eeprom_write_byte(4,i, temp);
													}
												}
												
												for (i = 1; i < (sys_zones_num + 1); i++ ){
													if (zones_change_th[i] == 1) {
														// devo fare questo ritocco altrimenti easyGUI arrotonda come vuole, ma pensa te
														zones_array[i].th_high  = (GuiVar_th_new_high  + 0.001);
														zones_array[i].th_low   = (GuiVar_th_new_low   + 0.001);
														
				
													}
												}
												
												// Se ho cambiato i valori per tute le zone, crea un evento unico
												GuiVar_message = 0;
												if (alarms_number != 0) {
													if (GuiVar_screen_select != CHARTS_SCREEN) {
														GuiVar_screen_select = CHARTS_SCREEN;
														init_screen(CHARTS_SCREEN);
														press_button(7);
													}
												} else {
													if (GuiVar_screen_select != SYSTEM_SCREEN) {
														GuiVar_screen_select = SYSTEM_SCREEN;
														init_screen(SYSTEM_SCREEN);
														press_button(0);
													}
												}
											rs485_imposta_soglie_allarme = 1;
										}
								break;
														
								/* LOGOUT (yes)*/
								case LOG_SCREEN:
										GuiVar_message = 2;
										message_timer_count = MESSAGE_ON_SCREEN_TIME;
								
										GuiVar_keyboard_1 = 0;
										GuiVar_keyboard_2 = 0;
										GuiVar_keyboard_3 = 0;
										GuiVar_keyboard_4 = 0;
										GuiVar_keyboard_5 = 0;
										GuiVar_keyboard_6 = 0;
										GuiVar_keyboard_7 = 0;
										GuiVar_keyboard_8 = 0;
										GuiVar_keyboard_9 = 0;
										GuiVar_keyboard_0 = 0;
										
										GuiVar_logout_button = 2;
										GuiVar_reset_values  = 0;
								
										temp_event.time          = current_time;
										temp_event.type          = E_TYPE_MISC;
										temp_event.specific_type = E_S_TYPE_LOGOUT;
										temp_event.user_level    = current_user.level;
										temp_event.user_id       = current_user.id;
										add_event(temp_event);	
										
										current_user.level = 0;
										current_user.id   = 0;
								break;	
								
								/* SAVE OPTIONS */
								case OPTIONS_SCREEN:
										// Save Date & Time
										rtc_set_time ( GuiVar_set_year, GuiVar_set_month, GuiVar_set_day, GuiVar_set_hour, GuiVar_set_minutes, 0);
								
										// Save backlight
								    if (backlight!=GuiVar_set_backlight)
											setbacklight = 1;
										backlight = GuiVar_set_backlight;
										eeprom_write_word(E2PROM_PAGE_OF_OTHER,REG_BACKLIGHT*2,backlight);
								
										GuiVar_message = 0;
										if (alarms_number != 0) {
											if (GuiVar_screen_select != CHARTS_SCREEN) {
												GuiVar_screen_select = CHARTS_SCREEN;
												init_screen(CHARTS_SCREEN);
												press_button(7);
											}
										} else {
											if (GuiVar_screen_select != SYSTEM_SCREEN) {
												GuiVar_screen_select = SYSTEM_SCREEN;
												init_screen(SYSTEM_SCREEN);
												press_button(0);
											}
										}	
								break;
						
								/* DELETE EVENTS */
								case EVENTS_SCREEN:
									index_events = 0;
									events_array_dim = 0;
								
									for (i=0; i< EVENTS_ARRAY_DIMENSIONS; i++){
										events_array[i].float_value_1 = 0;
										events_array[i].float_value_2 = 0;
										events_array[i].id_1 					= 0;
										events_array[i].id_2 					= 0;
										events_array[i].specific_type = 0;
										events_array[i].time.day 			= 0;
										events_array[i].time.hour 		= 0;
										events_array[i].time.minutes 	= 0;
										events_array[i].time.month 		= 0;
										events_array[i].time.seconds  = 0;
										events_array[i].time.year 		= 0;
										events_array[i].type 					= 0;
										events_array[i].user_id 			= 0;
										events_array[i].user_level 		= 0;
										events_array[i].in_out    		= 0;
										events_array[i].object_type		= 0;
									}
									GuiVar_message = 0;
									GuiVar_line_1_bkg_color = WHITE;
									GuiVar_line_2_bkg_color = WHITE;
									GuiVar_line_3_bkg_color = WHITE;
									GuiVar_line_4_bkg_color = WHITE;
									GuiVar_line_5_bkg_color = WHITE;
									
									// Crea il relativo evento
									temp_event.time          = current_time;
									temp_event.type          = E_TYPE_MISC;
									temp_event.specific_type = E_S_TYPE_DELETE_EVENTS; 				
									temp_event.user_level    = current_user.level;						 // user_level
									temp_event.user_id       = current_user.id;								 // user_id
									add_event(temp_event);								
								break;
			
								/* DELETE ALARMS LIST */
								case ALARMS_LIST_SCREEN:
									index_alarms = 0;
									alarms_array_dim = 0;
								
									for (i=0; i< ALARMS_ARRAY_DIMENSIONS; i++){
										alarms_array[i].float_value_1 = 0;
										alarms_array[i].float_value_2 = 0;
										alarms_array[i].id_1 					= 0;
										alarms_array[i].id_2 					= 0;
										alarms_array[i].specific_type = 0;
										alarms_array[i].time.day 			= 0;
										alarms_array[i].time.hour 		= 0;
										alarms_array[i].time.minutes 	= 0;
										alarms_array[i].time.month 		= 0;
										alarms_array[i].time.seconds  = 0;
										alarms_array[i].time.year 		= 0;
										alarms_array[i].type 					= 0;
										alarms_array[i].user_id 			= 0;
										alarms_array[i].user_level 		= 0;
										alarms_array[i].in_out    		= 0;
										alarms_array[i].object_type		= 0;
									}
									GuiVar_message = 0;
									GuiVar_line_1_bkg_color = WHITE;
									GuiVar_line_2_bkg_color = WHITE;
									GuiVar_line_3_bkg_color = WHITE;
									GuiVar_line_4_bkg_color = WHITE;
									GuiVar_line_5_bkg_color = WHITE;
									
									// Crea il relativo evento
									temp_event.time          = current_time;
									temp_event.type          = E_TYPE_MISC;
									temp_event.specific_type = E_S_TYPE_DELETE_ALARMS; 				
									temp_event.user_level    = current_user.level;						 // user_level
									temp_event.user_id       = current_user.id;								 // user_id
									add_event(temp_event);								
								break;
								
								/* SAVE DEVICES CTRL (activations) */
								case DEV_CTRL_LIST_SCREEN:
									GuiVar_message = 0;
									if (alarms_number != 0) {
										if (GuiVar_screen_select != CHARTS_SCREEN) {
											GuiVar_screen_select = CHARTS_SCREEN;
											init_screen(CHARTS_SCREEN);
											press_button(7);
										}
									} else {
										if (GuiVar_screen_select != SYSTEM_SCREEN) {
											GuiVar_screen_select = SYSTEM_SCREEN;
											init_screen(SYSTEM_SCREEN);
											press_button(0);
										}
									}									
								break;
								
								/* SAVE DISABLEMENTS */
								case DISABLEMENTS_LIST_SCREEN:
									GuiVar_message = 0;
									if (alarms_number != 0) {
										if (GuiVar_screen_select != CHARTS_SCREEN) {
											GuiVar_screen_select = CHARTS_SCREEN;
											init_screen(CHARTS_SCREEN);
											press_button(7);
										}
									} else {
										if (GuiVar_screen_select != SYSTEM_SCREEN) {
											GuiVar_screen_select = SYSTEM_SCREEN;
											init_screen(SYSTEM_SCREEN);
											press_button(0);
										}
									}	
								break;
									
							}
					} else {
					/* RESET ALARMS / MAIN SCREEN */
						reset_alarms();
						GuiVar_message = 0;
						if (alarms_number != 0) {
							if (GuiVar_screen_select != CHARTS_SCREEN) {
								GuiVar_screen_select = CHARTS_SCREEN;
								init_screen(CHARTS_SCREEN);
								press_button(7);
							}
						} else {
							if (GuiVar_screen_select != SYSTEM_SCREEN) {
								GuiVar_screen_select = SYSTEM_SCREEN;
								init_screen(SYSTEM_SCREEN);
								press_button(0);
							}
						}	
					}		
				}	
			break;		
     
			// RELEASE NO (SAVE)
			case 23: 
				if (GuiVar_no_button != 2) {
				   GuiVar_no_button = 0;				
					 GuiVar_message = 0;
						if (alarms_number != 0) {
							if (GuiVar_screen_select != CHARTS_SCREEN) {
								GuiVar_screen_select = CHARTS_SCREEN;
								init_screen(CHARTS_SCREEN);
								press_button(7);
							}
						} else {
							if (GuiVar_screen_select != SYSTEM_SCREEN) {
								GuiVar_screen_select = SYSTEM_SCREEN;
								init_screen(SYSTEM_SCREEN);
								press_button(0);
							}
						}	
				 }
			break;					

			// RELEASE DEC BACKLIGHT
			case 24: 
						GuiVar_backlight_color = DARK_BLUE;
			break;	

			// RELEASE INC BACKLIGHT
			case 25: 
						GuiVar_backlight_color = DARK_BLUE;
			break;
				
			// RELEASE DEFAULT "ZONE 1" BUTTON
			case 26: 
					if (GuiVar_default_button_1 != 2){ 
						GuiVar_default_button_1 = 0;
						GuiVar_screen_select    = 12;
						GuiVar_zone_id          = GuiVar_default_line_1;
						index_zone_x = zones_array[GuiVar_default_line_1].first_sensor;
						GuiVar_button_back = 0;
						GuiVar_default_line_1_color = DARK_BLUE;
						GuiVar_var_rectangle_color = WHITE;
						GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
					}
			break;		

			// RELEASE DEFAULT "ZONE 2" BUTTON
			case 27: 
					if (GuiVar_default_button_2 != 2){ 
						GuiVar_default_button_2 = 0;
						GuiVar_screen_select = 12;
						GuiVar_zone_id = GuiVar_default_line_2;
						index_zone_x = zones_array[GuiVar_default_line_2].first_sensor;
						GuiVar_button_back = 0;
						GuiVar_default_line_2_color = DARK_BLUE;
						GuiVar_var_rectangle_color = WHITE;
						GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
					}
			break;	

			// RELEASE DEFAULT "ZONE 3" BUTTON
			case 28: 
					if (GuiVar_default_button_3 != 2){ 
						GuiVar_default_button_3 = 0;
						GuiVar_screen_select = 12;
						GuiVar_zone_id = GuiVar_default_line_3;
						index_zone_x = zones_array[GuiVar_default_line_3].first_sensor;
						GuiVar_button_back = 0;
						GuiVar_default_line_3_color = DARK_BLUE;
						GuiVar_var_rectangle_color = WHITE;
						GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
					}
			break;			
					
			// RELEASE BACK BUTTON
			case 29: 
					if (GuiVar_button_back != 2){ 
						GuiVar_button_back = 0;
						switch (GuiVar_screen_select)
						{
							case 12: // Zone X
								GuiVar_screen_select = 0;
								index_system = 1;
								init_screen(SYSTEM_SCREEN);
							break;
							
							case 11: // ALARMS (LIST)
								GuiVar_screen_select = 7;
								init_screen(CHARTS_SCREEN);
							break;
							
							case 13: // DEV CTRL (LIST)
								GuiVar_screen_select = 10;
								GuiVar_radio_flux_1 = 1;
								GuiVar_radio_flux_2 = 1;
								GuiVar_radio_flux_3 = 1;
								GuiVar_radio_flux_4 = 1;
								GuiVar_radio_flux_5 = 1;
								init_screen(DEV_CTRL_SCREEN);
							break;
							
							case 14: // DISABLEMENTS (LIST)
								GuiVar_screen_select = 9;
								init_screen(DISABLEMENTS_SCREEN);
							break;
						}						
					}					
			break;	

			// RELEASE checkbox "ALL" schermata SET
			case 30: 
					if (GuiVar_checkbox_set_all == 0){
						// TUTTI SELEZIONATI !
						GuiVar_checkbox_set_all = 1;
						
						for (i=1; i < (sys_zones_num + 1); i++) {
							zones_change_set[i] = 1;
						}
					} else {
						// NESSUNO SELEZIONATO !
						GuiVar_checkbox_set_all = 0;
						
						for (i=1; i < (sys_zones_num + 1); i++) {
							zones_change_set[i] = 0;
						}
						GuiVar_set_checkbox_zone = 0;
						GuiVar_set_checkbox_sensor_1 = 0;
						GuiVar_set_checkbox_sensor_2 = 0;		

						GuiVar_set_zone_id = 1;
						GuiVar_set_sensor_id_1 = 1;
						GuiVar_set_sensor_id_2 = 2;
						GuiVar_set_zone_dec_button = 2;
						GuiVar_set_zone_inc_button = 0;
				    GuiVar_set_set  = zones_array[GuiVar_set_zone_id].set;//Antonio
				    GuiVar_set_hyst = zones_array[GuiVar_set_zone_id].hyst;	
					}					
			break;				

			// RELEASE DEC ZONES BUTTON (schermata SET)
			case 31: 
					if (GuiVar_set_zone_dec_button != 2){ 
						
							GuiVar_set_zone_dec_button = 0;
						
							if (GuiVar_set_zone_id > 1 ) {
								GuiVar_set_zone_id--;
				        GuiVar_set_set  = zones_array[GuiVar_set_zone_id].set;//Antonio
				        GuiVar_set_hyst = zones_array[GuiVar_set_zone_id].hyst;	
							}

							GuiVar_set_zone_inc_button = 0;
					}
			break;	

			// RELEASE checkbox "ZONE X" schermata SET
			case 32: 
					if (GuiVar_set_checkbox_zone == 0){
						// TUTTI la zona SELEZIONATA !
						GuiVar_set_checkbox_zone = 1;
						GuiVar_set_checkbox_sensor_1 = 1;
						GuiVar_set_checkbox_sensor_2 = 1;
						
						zones_change_set[GuiVar_set_zone_id] = 1;
						
					} else {
						// NESSUN sensore della zona SELEZIONATO !
						GuiVar_set_checkbox_zone = 0;
						GuiVar_set_checkbox_sensor_1 = 0;
						GuiVar_set_checkbox_sensor_2 = 0;	

						zones_change_set[GuiVar_set_zone_id] = 0;						
					}	
					
			break;	
					
			// RELEASE INC ZONES BUTTON (schermata SET)
			case 33: 
					if (GuiVar_set_zone_inc_button != 2){ 
						
							GuiVar_set_zone_inc_button = 0;
						
							if (GuiVar_set_zone_id < sys_zones_num ) {
								GuiVar_set_zone_id++;
				        GuiVar_set_set  = zones_array[GuiVar_set_zone_id].set;//Antonio
				        GuiVar_set_hyst = zones_array[GuiVar_set_zone_id].hyst;	
							}

							GuiVar_set_zone_dec_button = 0;							
					}					
			break;

			// RELEASE CHECKBOX SENSOR 1 (schermata SET)
			case 34: 
						
			break;
			
			// RELEASE CHECKBOX SENSOR 2 (schermata SET)
			case 35: 
			
			break;	

			// RELEASE DEC set value BUTTON (schermata SET)
			case 36:
					if (GuiVar_set_set != 0) {
						GuiVar_set_dec_set = 0;			
					}		
						
					GuiVar_set_text_color  = DARK_BLUE;
			break;

			// RELEASE INC set value BUTTON (schermata SET)
			case 37:
					if (GuiVar_set_set != 25) {
						GuiVar_set_inc_set = 0;			
					}
						
					GuiVar_set_text_color  = DARK_BLUE;
			break;	

			// RELEASE DEC hyst value BUTTON (schermata SET)
			case 38:
					if (GuiVar_set_hyst != 0) {
						GuiVar_set_dec_hyst = 0;			
					}			

					GuiVar_hyst_text_color  = DARK_BLUE;
			break;

			// RELEASE INC hyst value BUTTON (schermata SET)
			case 39:
					if (GuiVar_set_hyst != 0.5) {
						GuiVar_set_inc_hyst = 0;			
					}
						
					GuiVar_hyst_text_color  = DARK_BLUE;
			break;						
		
			// RELEASE checkbox "ALL" schermata TH
			case 40: 
					if (GuiVar_checkbox_th_all == 0){
						// TUTTI SELEZIONATI !
						GuiVar_checkbox_th_all = 1;
						
						for (i=1; i < (sys_sensors_num + 1); i++) {
							sensors_change_th[i] = 1;
						}
						for (i=1; i < (sys_zones_num + 1); i++) {
							zones_change_th[i] = 1;
						}						
					} else {
						// NESSUNO SELEZIONATO !
						GuiVar_checkbox_th_all = 0;
						
						for (i=1; i < (sys_sensors_num + 1); i++) {
							sensors_change_th[i] = 0;
						}
						for (i=1; i < (sys_zones_num + 1); i++) {
							zones_change_th[i] = 0;
						}				
						GuiVar_th_checkbox_zone = 0;
						GuiVar_th_checkbox_sensor_1 = 0;
						GuiVar_th_checkbox_sensor_2 = 0;		

						GuiVar_th_zone_id = 1;
						GuiVar_th_sensor_id_1 = 1;
						GuiVar_th_sensor_id_2 = 2;
						GuiVar_th_zone_dec_button = 2;
						GuiVar_th_zone_inc_button = 0;
			      GuiVar_th_new_high = sensors_array[GuiVar_th_sensor_id_1].th_high;//Antonio
			      GuiVar_th_new_low  = sensors_array[GuiVar_th_sensor_id_1].th_low;	
					}					
			break;				

			// RELEASE DEC ZONES BUTTON (schermata TH)
			case 41: 
					if (GuiVar_th_zone_dec_button != 2){ 
						
							GuiVar_th_zone_dec_button = 0;
						
							if (GuiVar_th_zone_id > 1 ) {
								GuiVar_th_zone_id--; 
			          GuiVar_th_new_high = sensors_array[GuiVar_th_sensor_id_1-1].th_high;//Antonio
			          GuiVar_th_new_low  = sensors_array[GuiVar_th_sensor_id_1-1].th_low;	
							}
							index_sensor = (zones_array[GuiVar_th_zone_id].first_sensor);
							GuiVar_th_zone_inc_button = 0;							
					}					
			break;	

			// RELEASE checkbox "ZONE X" schermata TH
			case 42: 
					if (GuiVar_th_checkbox_zone == 0){
						// TUTTA la zona SELEZIONATA !
						GuiVar_th_checkbox_zone = 1;
						GuiVar_th_checkbox_sensor_1 = 1;
						GuiVar_th_checkbox_sensor_2 = 1;

						// Seleziona tutti i sensori di quella zona
						for (i=zones_array[GuiVar_th_zone_id].first_sensor; i< ( zones_array[GuiVar_th_zone_id].first_sensor + zones_array[GuiVar_th_zone_id].n_sensors) ; i++) {
							sensors_change_th[i] = 1;
						} 						
						zones_change_th[GuiVar_th_zone_id] = 1;
						
					} else {
						// NESSUN sensore della zona SELEZIONATO !
						GuiVar_th_checkbox_zone = 0;
						GuiVar_th_checkbox_sensor_1 = 0;
						GuiVar_th_checkbox_sensor_2 = 0;	

						// Deseleziona tutti i sensori di quella zona
						for (i=zones_array[GuiVar_th_zone_id].first_sensor; i< ( zones_array[GuiVar_th_zone_id].first_sensor + zones_array[GuiVar_th_zone_id].n_sensors) ; i++) {
							sensors_change_th[i] = 0;
						} 	
						zones_change_th[GuiVar_th_zone_id] = 0;
					}	

			break;	
					
			// RELEASE INC ZONES BUTTON (schermata TH)
			case 43: 
					if (GuiVar_th_zone_inc_button != 2){ 
						
							GuiVar_th_zone_inc_button = 0;
						
							if (GuiVar_th_zone_id < sys_zones_num ) {
								GuiVar_th_zone_id++;
			          GuiVar_th_new_high = sensors_array[GuiVar_th_zone_id*sys_elements_num[GuiVar_th_zone_id][O_TYPE_SENSOR]].th_high;//Antonio
			          GuiVar_th_new_low  = sensors_array[GuiVar_th_zone_id*sys_elements_num[GuiVar_th_zone_id][O_TYPE_SENSOR]].th_low;	
							}
							index_sensor = (zones_array[GuiVar_th_zone_id].first_sensor);
							
							GuiVar_th_zone_dec_button = 0;							
					}					
			break;

			// RELEASE CHECKBOX SENSOR 1 (schermata TH)
			case 44: 
// 				
// 					if (GuiVar_th_checkbox_sensor_1 == 0) {
// 							GuiVar_th_checkbox_sensor_1 = 1;					
// 							// "index_sensor" dovrebbe gi� puntare al primo sensore della zona
// 							// dipende da "GuiVar_th_zone_id"; l'indice � impostato quando carico la pagina
// 							sensors_change_th[index_sensor] = 1;						
// 					} else {
// 							GuiVar_th_checkbox_sensor_1 = 0;						
// 							// "index_sensor" dovrebbe gi� puntare al primo sensore della zona
// 							// dipende da "GuiVar_th_zone_id"; l'indice � impostato quando carico la pagina
// 							sensors_change_th[index_sensor] = 0;						
// 					}
// 					
// 					// Se tutti i sensori di quella zona selezionati, checkboz_all checked								
// 					if ( all_zone_sensors_selected(GuiVar_th_zone_id, CHANGE_TH) == 1 ) {
// 							GuiVar_th_checkbox_zone = 1;
// 					} else {
// 							GuiVar_th_checkbox_zone = 0;
// 					}		
// 					
// 					if (all_sensors_selected(CHANGE_TH) == 1) {
// 						GuiVar_checkbox_th_all = 1;
// 					}							
			break;
			
			// RELEASE CHECKBOX SENSOR 2 (schermata TH)
			case 45: 
// 					if (GuiVar_th_checkbox_sensor_2 == 0) {
// 							GuiVar_th_checkbox_sensor_2 = 1;
// 						
// 							// "index_sensor" dovrebbe gi� puntare al primo sensore della zona
// 							// dipende da "GuiVar_th_zone_id"; l'indice � impostato quando carico la pagina
// 							sensors_change_th[index_sensor +1] = 1;						
// 					} else {
// 							GuiVar_th_checkbox_sensor_2 = 0;
// 							// "index_sensor" dovrebbe gi� puntare al primo sensore della zona
// 							// dipende da "GuiVar_th_zone_id"; l'indice � impostato quando carico la pagina
// 							sensors_change_th[index_sensor +1] = 0;							
// 					}
// 					
// 					// Se tutti i sensori di quella zona selezionati, checkboz_all checked								
// 					if ( all_zone_sensors_selected(GuiVar_th_zone_id, CHANGE_TH) == 1 ) {
// 							GuiVar_th_checkbox_zone = 1;
// 					} else {
// 							GuiVar_th_checkbox_zone = 0;
// 					}		
// 					
// 					if (all_sensors_selected(CHANGE_TH) == 1) {
// 						GuiVar_checkbox_th_all = 1;
// 					}					
			break;		

			// RELEASE DEC th high value BUTTON (schermata TH)
			case 46:
					if (GuiVar_th_new_high != 0) {
						GuiVar_th_dec_high = 0;
            GuiVar_th_new_high = (GuiVar_th_new_high>GuiVar_th_new_low)? GuiVar_th_new_high: GuiVar_th_new_low+0.1;						
					}					

					GuiVar_th_high_color  = DARK_BLUE;
			break;


			// RELEASE INC th high value BUTTON (schermata TH)
			case 47:
					if (GuiVar_th_new_high != 25) {
						GuiVar_th_inc_high = 0;			
					}					
					
					GuiVar_th_high_color  = DARK_BLUE;
			break;	

			// RELEASE DEC th low value BUTTON (schermata TH)
			case 48:
					if (GuiVar_th_new_low != 0) {
						GuiVar_th_dec_low = 0;			
					}
						
					GuiVar_th_low_color  = DARK_BLUE;
			break;

			// RELEASE INC th low value BUTTON (schermata TH)
			case 49:
					if (GuiVar_th_new_low != 0.5) {
						GuiVar_th_inc_low = 0;			
					}
          GuiVar_th_new_low = (GuiVar_th_new_low<GuiVar_th_new_high)? GuiVar_th_new_low: GuiVar_th_new_high-0.1;						

					GuiVar_th_low_color  = DARK_BLUE;
			break;	
					
			// RELEASE Password button 1
			case 50:
// 20201002
					if (GuiVar_keyboard_1 != 2) {						
						GuiVar_keyboard_1 = 0;      
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 1;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif            
 				 }
			break;	

			// RELEASE Password button 2
			case 51:
					if (GuiVar_keyboard_2 != 2) {
						GuiVar_keyboard_2 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 2;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}											
#endif  
					}	
			break;	

			// RELEASE Password button 3
			case 52:
					if (GuiVar_keyboard_3 != 2) {
						GuiVar_keyboard_3 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 3;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}												
#endif  
					}							
			break;	

			// RELEASE Password button 4
			case 53:
					if (GuiVar_keyboard_4 != 2) {
						GuiVar_keyboard_4 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 4;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
					}							
			break;	

			// RELEASE Password button 5
			case 54:
					if (GuiVar_keyboard_5 != 2) {
						GuiVar_keyboard_5 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 5;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
					}						
			break;	

			// RELEASE Password button 6
			case 55:
					if (GuiVar_keyboard_6 != 2) {
						GuiVar_keyboard_6 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 6;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
					}					
			break;	

			// RELEASE Password button 7
			case 56:
					if (GuiVar_keyboard_7 != 2) {
						GuiVar_keyboard_7 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 7;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
					}					
			break;	

			// RELEASE Password button 8
			case 57:
					if (GuiVar_keyboard_8 != 2) {
						GuiVar_keyboard_8 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 8;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
							
					}								
			break;	

			// RELEASE Password button 9
			case 58:
					if (GuiVar_keyboard_9 != 2) {
						GuiVar_keyboard_9 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 9;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
					}							
			break;	

			// RELEASE Password button 0
			case 59:
					if (GuiVar_keyboard_0 != 2) {
						GuiVar_keyboard_0 = 0;
// 20201002
#ifndef UPDATE_SCREEN_ON_PRESS						
						pswd_index++;
						password[pswd_index] = 0;
						
						if (pswd_index == 1) {GuiVar_pwd_char_1 = 1;}
						if (pswd_index == 2) {GuiVar_pwd_char_2 = 1;}
						if (pswd_index == 3) {GuiVar_pwd_char_3 = 1;}
						if (pswd_index == 4) {
							// Ho inserito tutte le cifre della password
							GuiVar_pwd_char_4 = 1;
							pswd_index = 0;
							password_check();
						}						
#endif  
					}						            
			break;	

			// RELEASE LOGOUT button
			case 60://2018.02.22
				if (current_user.level>1) {
					GuiVar_message = 5;	
					GuiVar_logout_button = 0;					
				}						
			break;		
			
			// RELEASE OK button
			case 61:
				GuiVar_ok_button = 0;
				GuiVar_message   = 0;
			break;
			
			// RELEASE UP button
			case 62:
				if (GuiVar_button_delete_back_color == 0xFFFF) {
//					if (GuiVar_up_button != 2) {
					GuiVar_up_button = 0;					
				}			
			break;	

			// RELEASE DOWN button
			case 63:
				if (GuiVar_button_delete_back_color == 0xFFFF) {
//					if (GuiVar_down_button != 2) {
					GuiVar_down_button = 0;					
				}			
			break;

			// RELEASE DEL events button
			case 64:
				if ( (current_user.level >= 2 ) && (events_array_dim != 0) ) {
//  			if (GuiVar_del_events_button != 2) {
					
					switch (GuiVar_screen_select)
					{
						case EVENTS_SCREEN: 
							GuiVar_message = 7;
						break;
						
						case ALARMS_LIST_SCREEN: 
							GuiVar_message = 7;
						break;
					}
					GuiVar_del_events_button = 0;					
				}	
			break;	

			// RELEASE ALARMS LIST button
			case 65:
				if (GuiVar_list_button != 2) {
					GuiVar_screen_select = 11;
					init_screen(ALARMS_LIST_SCREEN);

					GuiVar_list_button = 0;					
				}	
			break;	

			// RELEASE CONFIG DEV CTRL button
			case 66:
				if (GuiVar_config_button != 2) {
					switch (GuiVar_screen_select)
					{
						case 9: // Disablements
							GuiVar_screen_select = 14;
							init_screen(DISABLEMENTS_LIST_SCREEN);
						break;
						
						case 10: // Devices control
							GuiVar_screen_select = 13;
							init_screen(DEV_CTRL_LIST_SCREEN);
						break;
					}					
					
					GuiVar_config_button = 0;					
				}	
			break;	

			// RELEASE DISABL. & DEV_CTRL CheckBox 1
			case 67:
				/* DISABLEMENTS */
				if (GuiVar_screen_select == 14){
						if (GuiVar_line_1_ck == 0) {
							// se � un oggetto di una zona in allarme ...
							if ( ( (objects_array[GuiVar_line_index_1 - 1].zone != 0) && (zones_array[objects_array[GuiVar_line_index_1 - 1].zone].in_alarm == 1 ) )  ||
							// ...o una zona in allarme 
							( (objects_array[GuiVar_line_index_1 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_1 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_1 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_1 - 1].type != O_TYPE_ZONE ) )
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_1_ck = 1;
								disablements_change[GuiVar_line_index_1 - 1] = 1;		
							}
						}	else {
							GuiVar_line_1_ck = 0;
							disablements_change[GuiVar_line_index_1 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_1 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_1 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										disablements_change[i] = GuiVar_line_1_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if ( (objects_array[GuiVar_line_index_1 - 1].zone) != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_1_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_1 - 1].zone), CHANGE_DISABLEMENT) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_disablements == 0){ 
													disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_1 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_1 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													disablements_change[temp] = 1;
												}	
										}
								}
								if (GuiVar_line_1_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										if (sort_disablements == 0){ 
											disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_1 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_1 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											disablements_change[temp] = 0;
										}									
								}
						}
						
				}

				/* DEV CTRL */
				if (GuiVar_screen_select == 13){
						if (GuiVar_line_1_ck == 0) {
							// se � una zona in allarme ...
							if ( ( (objects_array[GuiVar_line_index_1 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_1 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_1 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_1 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_1 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_1 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_1_ck = 1;
								dev_ctrl_change[GuiVar_line_index_1 - 1] = 1;		
							}
						}	else {
							GuiVar_line_1_ck = 0;
							dev_ctrl_change[GuiVar_line_index_1 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_1 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_1 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										dev_ctrl_change[i] = GuiVar_line_1_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if ( (objects_array[GuiVar_line_index_1 - 1].zone) != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_1_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_1 - 1].zone), CHANGE_DEV_CTRL) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_dev_ctrl == 0){ 
													dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_1 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_1 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													dev_ctrl_change[temp] = 1;
												}		
										}
								}
								if (GuiVar_line_1_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_dev_ctrl == 0){ 
											dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_1 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_1 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											dev_ctrl_change[temp] = 0;
										}									
								}
						}						
				} 				
			break;	

			// RELEASE DISABL. & DEV_CTRL CheckBox 2
			case 68:
				/* DISABLEMENTS */
				if (GuiVar_screen_select == 14){
						if (GuiVar_line_2_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_2 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_2 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_2 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_2 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_2 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_2 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_2_ck = 1;
								disablements_change[GuiVar_line_index_2 - 1] = 1;		
							}	
						}	else {
							GuiVar_line_2_ck = 0;
							disablements_change[GuiVar_line_index_2 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_2 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_2 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										disablements_change[i] = GuiVar_line_2_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_2 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_2_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_2 - 1].zone), CHANGE_DISABLEMENT) == 1 ) {
												// tutti selezionati ! seleziona la zona
												if (sort_disablements == 0){ 
													disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_2 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_2 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													disablements_change[temp] = 1;
												}	
										}
								} 
								if (GuiVar_line_2_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										if (sort_disablements == 0){ 
											disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_2 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_2 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											disablements_change[temp] = 0;
										}									
								}
						}				
				 }
				 
				/* DEVICES CTRL */
				if (GuiVar_screen_select == 13){
						if (GuiVar_line_2_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_2 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_2 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_2 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_2 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_2 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_2 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_2_ck = 1;
								dev_ctrl_change[GuiVar_line_index_2 - 1] = 1;		
							}
						}	else {
							GuiVar_line_2_ck = 0;
							dev_ctrl_change[GuiVar_line_index_2 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_2 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_2 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										dev_ctrl_change[i] = GuiVar_line_2_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_2 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_2_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_2 - 1].zone), CHANGE_DEV_CTRL) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_dev_ctrl == 0){ 
													dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_2 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_2 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													dev_ctrl_change[temp] = 1;
												}		
										}
								} 
								if (GuiVar_line_2_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_dev_ctrl == 0){ 
											dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_2 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_2 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											dev_ctrl_change[temp] = 0;
										}									
								}
						}							
				 }
			break;	

			// RELEASE DISABL. & DEV_CTRL CheckBox 3
			case 69:
				/* DISABLEMENTS */
				if (GuiVar_screen_select == 14){
						if (GuiVar_line_3_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_3 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_3 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_3 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_3 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_3 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_3 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_3_ck = 1;
								disablements_change[GuiVar_line_index_3 - 1] = 1;		
							}
						}	else {
							GuiVar_line_3_ck = 0;
							disablements_change[GuiVar_line_index_3 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_3 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_3 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										disablements_change[i] = GuiVar_line_3_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_3 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_3_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_3 - 1].zone), CHANGE_DISABLEMENT) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_disablements == 0){ 
													disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_3 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_3 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													disablements_change[temp] = 1;
												}	
										}
								} 
								if (GuiVar_line_3_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_disablements == 0){ 
											disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_3 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_3 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											disablements_change[temp] = 0;
										}									
								}
						}					
				}

				/* DEVICES CTRL */
				if (GuiVar_screen_select == 13){
						if (GuiVar_line_3_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_3 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_3 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_3 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_3 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_3 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_3 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_3_ck = 1;
								dev_ctrl_change[GuiVar_line_index_3 - 1] = 1;		
							}	
						}	else {
							GuiVar_line_3_ck = 0;
							dev_ctrl_change[GuiVar_line_index_3 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_3 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_3 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										dev_ctrl_change[i] = GuiVar_line_3_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_3 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_3_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_3 - 1].zone), CHANGE_DEV_CTRL) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_dev_ctrl == 0){ 
													dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_3 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_3 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													dev_ctrl_change[temp] = 1;
												}		
										}
								} 
								if (GuiVar_line_3_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_dev_ctrl == 0){ 
											dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_3 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_3 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											dev_ctrl_change[temp] = 0;
										}										
								}
						}							
				}					
			break;	

			// RELEASE DISABL. & DEV_CTRL CheckBox 4
			case 70:
				/* DISABLEMENTS */
				if (GuiVar_screen_select == 14){
						if (GuiVar_line_4_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_4 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_4 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_4 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_4 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_4 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_4 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_4_ck = 1;
								disablements_change[GuiVar_line_index_4 - 1] = 1;		
							}
						}	else {
							GuiVar_line_4_ck = 0;
							disablements_change[GuiVar_line_index_4 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_4 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_4 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										disablements_change[i] = GuiVar_line_4_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_4 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_4_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_4 - 1].zone), CHANGE_DISABLEMENT) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_disablements == 0){ 
													disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_4 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_4 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													disablements_change[temp] = 1;
												}	
										}
								} 
								if (GuiVar_line_4_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_disablements == 0){ 
											disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_4 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_4 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											disablements_change[temp] = 0;
										}									
								}
						}						
				}
				
				/* DEVICES CTRL */
				if (GuiVar_screen_select == 13){
						if (GuiVar_line_4_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_4 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_4 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_4 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_4 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_4 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_4 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_4_ck = 1;
								dev_ctrl_change[GuiVar_line_index_4 - 1] = 1;		
							}
						}	else {
							GuiVar_line_4_ck = 0;
							dev_ctrl_change[GuiVar_line_index_4 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_4 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_4 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										dev_ctrl_change[i] = GuiVar_line_4_ck;
									}	
								}
						}
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_4 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_4_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_4 - 1].zone), CHANGE_DEV_CTRL) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_dev_ctrl == 0){ 
													dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_4 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_4 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													dev_ctrl_change[temp] = 1;
												}		
										}
								} 
								if (GuiVar_line_4_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_dev_ctrl == 0){ 
											dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_4 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_4 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											dev_ctrl_change[temp] = 0;
										}										
								}
						}	
				}
			break;

			// RELEASE DISABL. & DEV_CTRL CheckBox 5
			case 71:
				/* DISABLEMENTS */
				if (GuiVar_screen_select == 14){
						if (GuiVar_line_5_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_5 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_5 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_5 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_5 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_5 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_5 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_5_ck = 1;
								disablements_change[GuiVar_line_index_5 - 1] = 1;		
							}	
						}	else {
							GuiVar_line_5_ck = 0;
							disablements_change[GuiVar_line_index_5 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_5 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_5 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										disablements_change[i] = GuiVar_line_5_ck;
									}	
								}
						}	
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_5 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_5_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_5 - 1].zone), CHANGE_DISABLEMENT) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_disablements == 0){ 
													disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_5 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_5 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													disablements_change[temp] = 1;
												}	
										}
								} 
								if (GuiVar_line_5_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_disablements == 0){ 
											disablements_change[FIRST_ZONE + (objects_array[GuiVar_line_index_5 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_5 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											disablements_change[temp] = 0;
										}								
								}
						}						
				}
				
				/* DEVICES CTRL */
				if (GuiVar_screen_select == 13){
						if (GuiVar_line_5_ck == 0) {
							if ( ( (objects_array[GuiVar_line_index_5 - 1].zone != 0)  && (zones_array[objects_array[GuiVar_line_index_5 - 1].zone].in_alarm == 1 ) )  ||
							// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_5 - 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[GuiVar_line_index_5 - 1].id].in_alarm == 1 ) ) ||
							// o un oggetto della centrale e la centrale � in allarme, non puoi selezionarlo !	
							( (objects_array[GuiVar_line_index_5 - 1].zone == 0 ) && (system_in_alarm == 1) && (objects_array[GuiVar_line_index_5 - 1].type != O_TYPE_ZONE ))							
							){
								GuiVar_message = 8;
								message_timer_count = MESSAGE_ON_SCREEN_TIME;
							} else {
								GuiVar_line_5_ck = 1;
								dev_ctrl_change[GuiVar_line_index_5 - 1] = 1;		
							}	
						}	else {
							GuiVar_line_5_ck = 0;
							dev_ctrl_change[GuiVar_line_index_5 - 1] = 0;	
						}
						
						// se ho abilitato e disabilitato una zona, devo aggoirnare anche tutti gli oggeti che gli appartengono
						if (objects_array[GuiVar_line_index_5 - 1].type == O_TYPE_ZONE){
								for (i=0; i<NUMBER_OF_OBJECTS; i++) {
									if ( (objects_array[i].zone == objects_array[GuiVar_line_index_5 - 1].id) && (objects_array[i].type != O_TYPE_ZONE) ){
										dev_ctrl_change[i] = GuiVar_line_5_ck;
									}	
								}
						}	
						
						// se ho disabilitato o abilitato un oggetto appartenente ad una zona, devo controllare che il checkbox di quella zona sia aggiornato coerentemente
						if (objects_array[GuiVar_line_index_5 - 1].zone != 0){
								// se ho selezionato e tutti gli elementi della zona sono selezionati, anche il checkbox della zona deve esserlo
								if (GuiVar_line_5_ck == 1){
										// conta il numero di oggeti di questa zona abilitati
										if ( all_zone_objects_selected((objects_array[GuiVar_line_index_5 - 1].zone), CHANGE_DEV_CTRL) == 1 ) {
												// tutti selezionati ! seleziona la zona
												// per� la posizione della zona dipende da come sono ordinati...
												if (sort_dev_ctrl == 0){ 
													dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_5 - 1].zone) - 1 ] = 1;
												}else {	
													temp_object.type = O_TYPE_ZONE;
													temp_object.id = objects_array[GuiVar_line_index_5 - 1].zone;
													temp_object.specific_type = 0;
													temp_object.zone = 0;
													temp = look_for_item(temp_object, SORTED_BY_ZONES);
													dev_ctrl_change[temp] = 1;
												}		
										}
								} 
								if (GuiVar_line_5_ck == 0){
								// se ho deselezionato, anche il checkbox della zona deve esserlo
										//  deseleziona la zona
										// per� la posizione della zona dipende da come sono ordinati...
										if (sort_dev_ctrl == 0){ 
											dev_ctrl_change[FIRST_ZONE + (objects_array[GuiVar_line_index_5 - 1].zone) - 1 ] = 0;
										}else {	
											temp_object.type = O_TYPE_ZONE;
											temp_object.id = objects_array[GuiVar_line_index_5 - 1].zone;
											temp_object.specific_type = 0;
											temp_object.zone = 0;
											temp = look_for_item(temp_object, SORTED_BY_ZONES);
											dev_ctrl_change[temp] = 0;
										}										
								}
						}							
				}
			break;	

			// RELEASE OUTPUT OFF (DEV CTRL)
			case 74:
				if (GuiVar_output_off != 2) {
					if (output_off_state == 0) {
						GuiVar_output_off = 1;
						output_off_state = 1;
						
					} else {
						GuiVar_output_off = 0;		
						output_off_state = 0;
					}
					eeprom_write_byte(0,3,output_off_state);
				}
			break;

			// RELEASE SORT (DEV CTRL)
			case 75:
				if (GuiVar_sort_dev_ctrl != 2) {
					
					if (sort_dev_ctrl == 0) {
						// Carica l'array in ordine di zone
						sort_dev_ctrl = 1;
						for (i=0; i<NUMBER_OF_OBJECTS; i++) {
							objects_array[i].id 					 = sorted_objects_array[i].id;
							objects_array[i].specific_type = sorted_objects_array[i].specific_type;
							objects_array[i].type 				 = sorted_objects_array[i].type;
							objects_array[i].zone					 = sorted_objects_array[i].zone;
						}
						index_objects = 0; 
						switch_array_order(SORTED_BY_ZONES, DEV_CTRL_LIST_SCREEN);
					} else {	
						// Carica l'array in ordine di elementi
						sort_dev_ctrl = 0;
						for (i=0; i<NUMBER_OF_OBJECTS; i++) {
							objects_array[i].id 					 = sorted_objects_array_2[i].id;
							objects_array[i].specific_type = sorted_objects_array_2[i].specific_type;
							objects_array[i].type 				 = sorted_objects_array_2[i].type;
							objects_array[i].zone					 = sorted_objects_array_2[i].zone;
						}
						index_objects = 0;	
						switch_array_order(SORTED_BY_ELEMENTS, DEV_CTRL_LIST_SCREEN);
					}
					GuiVar_sort_dev_ctrl = 0;
				}
			break;

			// RELEASE SORT (DISABLEMENTS)
			case 76:
				if (GuiVar_sort_disablements != 2) {
					
					if (sort_disablements == 0) {
						// Carica l'array in ordine di zone
						sort_disablements = 1;
						for (i=0; i<NUMBER_OF_OBJECTS; i++) {
							objects_array[i].id 					 = sorted_objects_array[i].id;
							objects_array[i].specific_type = sorted_objects_array[i].specific_type;
							objects_array[i].type 				 = sorted_objects_array[i].type;
							objects_array[i].zone					 = sorted_objects_array[i].zone;
						}
						index_objects = 0;
						switch_array_order(SORTED_BY_ZONES, DISABLEMENTS_LIST_SCREEN);
					} else {	
						// Carica l'array in ordine di elementi
						sort_disablements = 0;
						for (i=0; i<NUMBER_OF_OBJECTS; i++) {
							objects_array[i].id 					 = sorted_objects_array_2[i].id;
							objects_array[i].specific_type = sorted_objects_array_2[i].specific_type;
							objects_array[i].type 				 = sorted_objects_array_2[i].type;
							objects_array[i].zone					 = sorted_objects_array_2[i].zone;
						}
						index_objects = 0;	
						switch_array_order(SORTED_BY_ELEMENTS, DISABLEMENTS_LIST_SCREEN);
					}
					GuiVar_sort_disablements = 0;
				}
			break;	

			// RELEASE RESET ALARMS BUTTON
			case 77:
				if (GuiVar_reset_alarms_button != 2) {
						GuiVar_message = 10;
						GuiVar_reset_alarms_button = 0;					
				}
			break;

			// RELEASE NO BUZZER BUTTON
			case 79:
//				if (GuiVar_no_buzzer_button != 2) {
//						GuiVar_no_buzzer_button = 0;
//						shut_up_buzzer = 1;
//				}
// da utilizzare se si vuole la riattivazione del buzzer				
				if (GuiVar_no_buzzer_button != 2) {
						GuiVar_no_buzzer_button = 0;
					shut_up_buzzer = (shut_up_buzzer)? 0: 1;
				}
			break;

			// RELEASE NO SIREN BUTTON
			case 80:
//				if (GuiVar_no_siren_button != 2) {
//						GuiVar_no_siren_button = 0;	
//						shut_up_siren = 1;
//				}
// da utilizzare se si vuole la riattivazione dell'allarme				
				if (GuiVar_no_siren_button != 2) {
						GuiVar_no_siren_button = 0;	
					  if (shut_up_siren==0)
						  shut_up_siren = 1;
						else {
							shut_up_siren = 0;
  						GuiVar_no_siren_button = 1;
						}
				}
			break;

			// RELEASE TEST LEDS BUTTON
			case 81:
				if (GuiVar_test_leds_button != 2) {
			
				}
			break;		

			// RELEASE Flux 1 BUTTON
			case 82:
// 				GuiVar_radio_flux_1 = 1;
					fluxes_array[dev_ctrl_array[index_dev_ctrl ].id ].tested = 0;
			break;	

			// RELEASE Flux 2 BUTTON
			case 83:
// 				GuiVar_radio_flux_2 = 1;
					fluxes_array[dev_ctrl_array[index_dev_ctrl + 1].id ].tested = 0;
			break;	

			// RELEASE Flux 3 BUTTON
			case 84:
// 				GuiVar_radio_flux_3 = 1;
					fluxes_array[dev_ctrl_array[index_dev_ctrl + 2].id ].tested = 0;
			break;	

			// RELEASE Flux 4 BUTTON
			case 85:
// 				GuiVar_radio_flux_4 = 1;
				fluxes_array[dev_ctrl_array[index_dev_ctrl + 3].id ].tested = 0;
			break;	

			// RELEASE Flux 5 BUTTON
			case 86:
// 				GuiVar_radio_flux_5 = 1;
				fluxes_array[dev_ctrl_array[index_dev_ctrl + 4].id ].tested = 0;
			break;			

			// RELEASE reset values BUTTON
			case 87:
				if (GuiVar_reset_values == 1){
					GuiVar_reset_values_button = 0;			
					eeprom_write_byte(0,0,1);
				}
			break;			
			 
			// RELEASE exit BUTTON 
			case 420:  
				Adox1500.Stato &= ~ADOX_TARATURA;
				SetMioRele(ON, 0, 0, OUT_VTARATURA); // chiude valvola per taratura
				GuiLib_ShowScreen(GuiStruct_Main_0,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);			
			  if ( GuiVar_screen_select == 2 ){
					press_button(0) ;
					GuiVar_screen_select = 0 ;	
          GuiVar_num_cella_analisi = 1; 
					init_screen(SYSTEM_SCREEN) ;	
//          if (GuiVar_autocal_index==0){
//	          if ( calib_from_C0146_or_Interno == 1 ){		
//							MySetPin(4,CMD_VALV_3VIE,0) ;		
//						}	
//						else{
//							RS485_COM.set_output_c0146(0,C0146_CMD_VALV_3VIE);	
//						}
//					}
				}
				break;

			// RELEASE store O2 BUTTON
			case 421:			
				if (GuiVar_selected_bottle==1){
				  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS] = (unsigned int)(GuiVar_O2_bottle*100);
					eeprom_write_word(E2_PAGE_1,E2_1_INPUT9_IS*2,E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS]);
					E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC] = GuiVar_ADC_O2;
					eeprom_write_word(E2_PAGE_4,E2_4_INPUT_9_ZEROADC*2,E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_ZEROADC]);
				}
				else{
				  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS] = (unsigned int)(GuiVar_O2_bottle*100);
					eeprom_write_word(E2_PAGE_1,E2_1_INPUT9_FS*2,E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS]);
					E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_SPANADC] = GuiVar_ADC_O2;
					eeprom_write_word(E2_PAGE_4,E2_4_INPUT_9_SPANADC*2,E2prom[E2_PAGE_4].E2_Int[E2_4_INPUT_9_SPANADC]);
			  }
        AnalogInput[9].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS]);
			  break;		
				
			// RELEASE PARAMETRI/CALIBRAZIONE BUTTON
			case 423:  
				
        if (current_user.level>1){
				  GuiLib_ShowScreen(GuiStruct_calibration_Def,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);   
  				GuiVar_selected_bottle=1;
			    GuiVar_O2_bottle = (float)(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS])/100;
				}					
				break;

							// RELEASE store set bottle BUTTON
			case 426:
				if (GuiVar_selected_bottle==1){
				  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS] = (unsigned int)(GuiVar_O2_bottle*100);
					eeprom_write_word(E2_PAGE_1,E2_1_INPUT9_IS*2,E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS]);
				}
				else{
				  E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS] = (unsigned int)(GuiVar_O2_bottle*100);
					eeprom_write_word(E2_PAGE_1,E2_1_INPUT9_FS*2,E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS]);
			  }
        AnalogInput[9].Span = LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_FS]) - LOGIC_ABS(E2prom[E2_PAGE_1].E2_Int[E2_1_INPUT9_IS]);
				break;			


			case 200:
				 if (current_user.level>1){
  				 GuiVar_ON_OFF = 0;
				   GuiVar_Adox_Cmd = 0;
           E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS] = GuiVar_Adox_Cmd;
    			 eeprom_write_word(E2_PAGE_4,E2_4_LOC_REM_STATUS*2,E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]);
				 }
				 break;

			case 201:
				 if (current_user.level>1){
  				 GuiVar_ON_OFF = 0;
				   GuiVar_Adox_Cmd = 1;
           E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS] = GuiVar_Adox_Cmd;
    			 eeprom_write_word(E2_PAGE_4,E2_4_LOC_REM_STATUS*2,E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]);
				 }
				 break;

			case 202:  
				 if (current_user.level>1){
 				   GuiVar_ON_OFF = 1;
					 Modbus.Array[MBVECTOR_COMANDO] = 0;
				   GuiVar_Adox_Cmd = 2;
           E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS] = GuiVar_Adox_Cmd;
    			 eeprom_write_word(E2_PAGE_4,E2_4_LOC_REM_STATUS*2,E2prom[E2_PAGE_4].E2_Int[E2_4_LOC_REM_STATUS]);
				 }
				 break;
				 
			case 203:
				if (GuiVar_Adox_Cmd==2){
					if (GuiVar_ON_OFF==1){
						GuiVar_ON_OFF = 2;
						Modbus.Array[MBVECTOR_COMANDO] = 1;
						//salva su RAM tamponata del RTC il nuovo valore 
//						RTC_WriteGPREG(LPC_RTC,REG_RMT_CMD,Modbus.Array[MBVECTOR_COMANDO]);
					}
					else{
						GuiVar_ON_OFF = 1;
						Modbus.Array[MBVECTOR_COMANDO] = 0;
						//salva su RAM tamponata del RTC il nuovo valore 
//						RTC_WriteGPREG(LPC_RTC,REG_RMT_CMD,Modbus.Array[MBVECTOR_COMANDO]);
					}
				}
			  break;
				 
		}		// Fine switch touch area
	}			
}

/***********************************************************************
 * @brief		deactivate_buttons			
	 disattiva tutti i bottoni (graficamente... devono apparire non premuti)
 **********************************************************************/
void deactivate_buttons (void) { 

	if (GuiVar_store_bottle_set != 2){ GuiVar_store_bottle_set = 0;	}			
	if (GuiVar_exit != 2){ GuiVar_exit = 0;	}		

	if (GuiVar_default_button_1 != 2) {
		GuiVar_default_button_1 = 0;
	}
	if (GuiVar_default_button_2 != 2) {
		GuiVar_default_button_2 = 0;
	}
	if (GuiVar_default_button_3 != 2) {
		GuiVar_default_button_3 = 0;
	}	

	if (GuiVar_default_button_1 != 1) {
		GuiVar_default_line_1_color = DARK_BLUE;	
	}
	if (GuiVar_default_button_2 != 1) {
		GuiVar_default_line_2_color = DARK_BLUE;	
	}
	if (GuiVar_default_button_3 != 1) {
		GuiVar_default_line_3_color = DARK_BLUE;	
	}	

	if (GuiVar_button_back != 2){ 
		GuiVar_button_back = 0;
	}			
	
	if (GuiVar_button_save != 2) {
			GuiVar_button_save = 0;		
	}

	// Schermata Default 
	if ( (GuiVar_button_set != 2) && (GuiVar_screen_select != 1) ) {
			GuiVar_button_set = 0;		
	}		
	if ( (GuiVar_button_th != 2) && (GuiVar_screen_select != 2) ) {
			GuiVar_button_th = 0;		
	}	
	if ( (GuiVar_button_log != 2) && (GuiVar_screen_select != 3) ) {
			GuiVar_button_log = 0;		
	}	
	if ( (GuiVar_button_options != 2) && (GuiVar_screen_select != 4) ) {
			GuiVar_button_options = 0;		
	}	
	if ( (GuiVar_button_events != 2) && (GuiVar_screen_select != 5) ) {
			GuiVar_button_events = 0;		
	}	
	if ( (GuiVar_button_info != 2) && (GuiVar_screen_select != 6) ) {
			GuiVar_button_info = 0;		
	}	
	if ( (GuiVar_button_alarms != 2) && (GuiVar_screen_select != 7) && (GuiVar_screen_select != 11)) {
			GuiVar_button_alarms = 0;		
	}		
	if ( (GuiVar_button_faults != 2) && (GuiVar_screen_select != 8) ) {
			GuiVar_button_faults = 0;		
	}
	if ( (GuiVar_button_disablements != 2) && (GuiVar_screen_select != 9) && (GuiVar_screen_select != 14) ) {
			GuiVar_button_disablements = 0;		
	}	
	if ( (GuiVar_button_dev_ctrl != 2) && (GuiVar_screen_select != 10) && (GuiVar_screen_select != 13)) {
			GuiVar_button_dev_ctrl = 0;		
	}		

	// Schermata SET
	if (GuiVar_set_zone_dec_button != 2){ 	
			GuiVar_set_zone_dec_button = 0;						
	}			
	if (GuiVar_set_zone_inc_button != 2){ 	
			GuiVar_set_zone_inc_button = 0;						
	}		
	if (GuiVar_set_inc_set != 2){ GuiVar_set_inc_set = 0;	}		
	if (GuiVar_set_dec_set != 2){	GuiVar_set_dec_set = 0;	}			
	if (GuiVar_set_inc_hyst != 2){ GuiVar_set_inc_hyst = 0;	}		
	if (GuiVar_set_dec_hyst != 2){ GuiVar_set_dec_hyst = 0;	}			
	
	GuiVar_set_text_color  = DARK_BLUE;
	GuiVar_hyst_text_color = DARK_BLUE;
	
	// Schermata TH
	if (GuiVar_th_zone_dec_button != 2){ GuiVar_th_zone_dec_button = 0; }			
	if (GuiVar_th_zone_inc_button != 2){ GuiVar_th_zone_inc_button = 0; }		
	if (GuiVar_th_dec_high != 2){ GuiVar_th_dec_high = 0;	}		
	if (GuiVar_th_inc_high != 2){	GuiVar_th_inc_high = 0;	}			
	if (GuiVar_th_dec_low != 2){ GuiVar_th_dec_low = 0;	}		
	if (GuiVar_th_inc_low != 2){ GuiVar_th_inc_low = 0;	}	
	if (GuiVar_th_inc_sens != 2){ GuiVar_th_inc_sens = 0;	}	
	if (GuiVar_th_dec_sens != 2){ GuiVar_th_dec_sens = 0;	}	
	
	GuiVar_th_high_color  = DARK_BLUE;
	GuiVar_th_low_color   = DARK_BLUE;
	
	// Schermata PASSWORD
	if (GuiVar_keyboard_0 != 2){ GuiVar_keyboard_0 = 0;	}			
	if (GuiVar_keyboard_1 != 2){ GuiVar_keyboard_1 = 0;	}			
	if (GuiVar_keyboard_2 != 2){ GuiVar_keyboard_2 = 0;	}			
	if (GuiVar_keyboard_3 != 2){ GuiVar_keyboard_3 = 0;	}			
	if (GuiVar_keyboard_4 != 2){ GuiVar_keyboard_4 = 0;	}			
	if (GuiVar_keyboard_5 != 2){ GuiVar_keyboard_5 = 0;	}			
	if (GuiVar_keyboard_6 != 2){ GuiVar_keyboard_6 = 0;	}			
	if (GuiVar_keyboard_7 != 2){ GuiVar_keyboard_7 = 0;	}			
	if (GuiVar_keyboard_8 != 2){ GuiVar_keyboard_8 = 0;	}			
	if (GuiVar_keyboard_9 != 2){ GuiVar_keyboard_9 = 0;	}			
	if (GuiVar_logout_button != 2) {GuiVar_logout_button = 0;}				
			
	// Schermata OPTIONS
	if (GuiVar_dec_day_button       != 2){ GuiVar_dec_day_button       = 0;	}			
	if (GuiVar_dec_month_button     != 2){ GuiVar_dec_month_button     = 0;	}			
	if (GuiVar_dec_year_button      != 2){ GuiVar_dec_year_button      = 0;	}			
	if (GuiVar_dec_hour_button      != 2){ GuiVar_dec_hour_button      = 0;	}			
	if (GuiVar_dec_minutes_button   != 2){ GuiVar_dec_minutes_button   = 0;	}			
	if (GuiVar_dec_backlight_button != 2){ GuiVar_dec_backlight_button = 0;	}			
	if (GuiVar_dec_language_button  != 2){ GuiVar_dec_language_button  = 0;	}		
	
	GuiVar_day_color       = DARK_BLUE;
	GuiVar_month_color     = DARK_BLUE;
	GuiVar_year_color      = DARK_BLUE;
	GuiVar_hour_color      = DARK_BLUE;
	GuiVar_minutes_color   = DARK_BLUE;
	GuiVar_backlight_color = DARK_BLUE;
	
	if (GuiVar_inc_day_button       != 2){ GuiVar_inc_day_button       = 0;	}			
	if (GuiVar_inc_month_button     != 2){ GuiVar_inc_month_button     = 0;	}			
	if (GuiVar_inc_year_button      != 2){ GuiVar_inc_year_button      = 0;	}			
	if (GuiVar_inc_hour_button      != 2){ GuiVar_inc_hour_button      = 0;	}			
	if (GuiVar_inc_minutes_button   != 2){ GuiVar_inc_minutes_button   = 0;	}			
	if (GuiVar_inc_backlight_button != 2){ GuiVar_inc_backlight_button = 0;	}			
	if (GuiVar_inc_language_button  != 2){ GuiVar_inc_language_button  = 0;	}

	// Schermata INFO
	if (GuiVar_reset_values_button   != 2) {GuiVar_reset_values_button = 0;   }
	
	// Schermata ALARMS
	if (GuiVar_list_button   != 2) {GuiVar_list_button = 0;   }
	
  // Schermata DEVICES CONTROL
	if (GuiVar_config_button != 2) {GuiVar_config_button = 0;	}
	if (GuiVar_output_off    != 2) {GuiVar_output_off = output_off_state;}
	if (GuiVar_sort_dev_ctrl != 2) {GuiVar_sort_dev_ctrl = 0;}

  // Schermata DISABLEMENTS
	if (GuiVar_sort_disablements != 2) {GuiVar_sort_disablements = 0;}
	
	// MESSAGEs
	if (GuiVar_ok_button       != 2){ GuiVar_ok_button       = 0;	}		
	if (GuiVar_no_button       != 2){ GuiVar_no_button       = 0;	}		
	if (GuiVar_yes_button      != 2){ GuiVar_yes_button      = 0;	}		

	// Tante schermate...
	if (GuiVar_up_button   != 2) {GuiVar_up_button = 0;	  }		
	if (GuiVar_down_button != 2) {GuiVar_down_button = 0;	}	
	if (GuiVar_del_events_button != 2) { GuiVar_del_events_button = 0;}	
	
	if (GuiVar_reset_alarms_button != 2) {GuiVar_reset_alarms_button = 0;}
	if (GuiVar_no_buzzer_button    != 2) {GuiVar_no_buzzer_button    = 0;}
	if (GuiVar_no_siren_button     != 2) {GuiVar_no_siren_button     = 0;}
	if (GuiVar_test_leds_button    != 2) {
		if (GuiVar_test_leds_button != 1){
			GuiVar_test_leds_button    = 0;
		}
	}
  if (GuiVar_calibration_button != 2){ GuiVar_calibration_button = 0;	}			
	//disabilita button di calibrazione se utente di livello 1 o 2
	if (current_user.level<3)	
		GuiVar_calibration_button = 2;
	else
		GuiVar_calibration_button = 0;
  
  if (GuiVar_Anybus_button != 2){ GuiVar_Anybus_button = 0;	}//ver 2.08		
	//disabilita button di configurazione Anybus se utente di livello 1 o 2
	if (current_user.level<3)	
		GuiVar_Anybus_button = 2;
	else
		GuiVar_Anybus_button = 0;

  if (GuiVar_settings_button != 2){ GuiVar_settings_button = 0;	}//ver 2.08		
	//disabilita tasto impostazioni impianto se utente di livello 1 o 2
	if (current_user.level<3)	
		GuiVar_settings_button = 2;
	else
		GuiVar_settings_button = 0;

	if (GuiVar_store_O2 != 2){ GuiVar_store_O2 = 0;	}			

}

/******************************************************************************
 * @brief   i2c0_pinConfig
 *   PIN configuration for I2C0
 *****************************************************************************/
void i2c0_pinConfig(void) 
{
 	// Config Pin for I2C_SDA and I2C_SCL of I2C0
	PINSEL_ConfigPin (0, 27, 1);
	PINSEL_ConfigPin (0, 28, 1);
 }
 
/***********************************************************************
 * @brief		init_peripherals			
	 inizializzazione di tutte le periferiche del micro
 **********************************************************************/
void init_peripherals (void) { 
	/* INIZIALIZZAZIONE */
	// NB!: parte dell'inizializzzione � gi� stata fatta in system_LPC407x_8x_177x_8c.c
	// (per necessit� della memoria esterna SDRAM...)
	// e cio� GPIO clk, SDRAM e Timer	0						

	// --------------------------------------------------------------------------------------------------------
  /* Initialize I2C_0 per NFC */
  I2C_Init(I2C_PORT, 100000);
  I2C_Cmd(I2C_PORT, 78, ENABLE);
	i2c0_pinConfig();
	
	// --------------------------------------------------------------------------------------------------------
	  //  Timer 1

  	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	  TIM_ConfigStruct.PrescaleValue  = 1;//100
  	TIM_MatchConfigStruct.MatchChannel = 0;// use channel 1, MR1
	  TIM_MatchConfigStruct.IntOnMatch   = TRUE;// Enable interrupt when MR0 matches the value in TC register
  	TIM_MatchConfigStruct.ResetOnMatch = TRUE;	//Enable reset on MR0: TIMER will reset if MR0 matches it
	  TIM_MatchConfigStruct.StopOnMatch  = FALSE;	//Stop on MR0 if MR0 matches it
  	TIM_MatchConfigStruct.ExtMatchOutputType =TIM_EXTMATCH_TOGGLE;	//Toggle MR0.0 pin if MR0 matches it
	  // Set Match value, count value of 10000 (10000 * 100uS = 1000000us = 1s --> 1 Hz)
  	TIM_MatchConfigStruct.MatchValue   = 14;  // cos� � 1 ms
	  TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &TIM_ConfigStruct);// Set configuration for Tim_config and Tim_MatchConfig
	  TIM_ConfigMatch(LPC_TIM1, &TIM_MatchConfigStruct);
	  NVIC_SetPriority(TIMER1_IRQn, ((0x01<<3)|0x01));/* preemption = 1, sub-priority = 1 */
	  NVIC_EnableIRQ(TIMER1_IRQn);/* Enable interrupt for timer 1 */
	  TIM_Cmd(LPC_TIM1, ENABLE);// To start timer

    /* Timer 2 (Match interrupt; Timer 0 � gi� usato...) */
	  // Initialize timer 0, prescale count time of 100uS
  	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	  TIM_ConfigStruct.PrescaleValue  = 100;
    // use channel 2, MR2
    TIM_MatchConfigStruct.MatchChannel = 0;
    // Enable interrupt when MR0 matches the value in TC register
    TIM_MatchConfigStruct.IntOnMatch   = TRUE;
    //Enable reset on MR0: TIMER will reset if MR0 matches it
    TIM_MatchConfigStruct.ResetOnMatch = TRUE;
    //Stop on MR0 if MR0 matches it
    TIM_MatchConfigStruct.StopOnMatch  = FALSE;
    //Toggle MR0.0 pin if MR0 matches it
//     TIM_MatchConfigStruct.ExtMatchOutputType =TIM_EXTMATCH_TOGGLE;
    // Set Match value, count value of 10 (10 * 100uS = 1000us = 1ms --> 1 KHz)
    TIM_MatchConfigStruct.MatchValue   = 10;  // cos� � 1 ms

    // Set configuration for Tim_config and Tim_MatchConfig
    TIM_Init(LPC_TIM2, TIM_TIMER_MODE, &TIM_ConfigStruct);
    TIM_ConfigMatch(LPC_TIM2, &TIM_MatchConfigStruct);

    /* preemption = 1, sub-priority = 1 */
    NVIC_SetPriority(TIMER2_IRQn, ((0x01<<3)|0x01));

    /* Enable interrupt for timer 0 */
    NVIC_EnableIRQ(TIMER2_IRQn);

    // To start timer
    TIM_Cmd(LPC_TIM2, ENABLE);
		
	// --------------------------------------------------------------------------------------------------------
	/* LCD */
  ea_lcdb_open(NULL, NULL, &dev_lcd);

	// --------------------------------------------------------------------------------------------------------
	/* easyGUI */
	// per fare 1 ms servono circa 7000 nop del micro
	// ci sarebbe l'opzione da settare in GuiDisplay.c che adesso � a 100 ma per ora visto che va la lascio cos�
	GuiLib_Init();
	// la variabile Color in easyGUI, per 5 6 5 � di 16 bit:
	// si prendono i bit + significativi 5 6 5 e si invertono R e B cio� BGR anzich� RGB
	
	// --------------------------------------------------------------------------------------------------------
	/* Touch Controller */
	touch_init();  
	// IMPOSTAZIONE COORDINATE TOUCH
	// Qui metto d'accordo le coordinate del TSC con quelle di EasyGui (display); 
	// prima metto (x,y) del display e poi le altre;
	// Devo mettere almeno un'area touch nel progetto easyGUI perch� funzioni
	
	GuiLib_TouchAdjustReset(); 
//	GuiLib_TouchAdjustSet (   0,    0,  108,    270);
//	GuiLib_TouchAdjustSet ( 800,    0,  4030,   207);
//	GuiLib_TouchAdjustSet (   0,   480, 157,   3904);
//	GuiLib_TouchAdjustSet ( 800,   480, 4034,  3912);

  GuiLib_TouchAdjustSet (   0,    0,  0,    0);
	GuiLib_TouchAdjustSet ( 800,    0,  4096,   0);
	GuiLib_TouchAdjustSet (   0,   480, 0,   4096);
	GuiLib_TouchAdjustSet ( 800,   480, 4096,  4096);

	// --------------------------------------------------------------------------------------------------------
	/* SD Card */
#ifdef USE_SD                                         
	temp = finit(NULL);
	if (temp != 0) {
		use_sd = 0;
		SBM_sd_card.fault = 1;
	} else {
		use_sd = 2;
	}
#endif
	// --------------------------------------------------------------------------------------------------------
	/* RTC */
	RTC_Init(LPC_RTC);
	// Disable RTC interrupt
	NVIC_DisableIRQ(RTC_IRQn);
	// preemption = 1, sub-priority = 1 
	NVIC_SetPriority(RTC_IRQn, ((0x01<<3)|0x01));
	// Enable rtc (starts increase the tick counter and second counter register)
	RTC_ResetClockTickCounter(LPC_RTC);
	RTC_Cmd(LPC_RTC, ENABLE);
	RTC_CalibCounterCmd(LPC_RTC, DISABLE);
	
	// --------------------------------------------------------------------------------------------------------
	/* EEPROM */
	EEPROM_Init();
	
	//LCD backlight
	PINSEL_ConfigPin(4,16,0);
	GPIO_SetDir(4, 16, 1);
	PINSEL_ConfigPin(1,18,0);
	GPIO_SetDir(1, 18, 1);
  GPIO_OutputValue(1, 18, 1);	
	// --------------------------------------------------------------------------------------------------------
	
//	UART_2_Initialization();
//	
//	UART_3_Initialization();


  // --------------------------------------------------------------------------------------------------------
	/* CAN */
  CAN_BUS_Initialization();	
		
		// --------------------------------------------------------------------------------------------------------
		/* WATCHDOG */
    // Initialize WDT, IRC OSC, interrupt mode, timeout = 5000000us = 5s
#ifdef ENABLE_WDOG    
    WWDT_Init(WDT_TIMEOUT);

    WWDT_Enable(ENABLE);

    WWDT_SetMode(WWDT_RESET_MODE, ENABLE);
    
    // Start watchdog with timeout given
    WWDT_Start(WDT_TIMEOUT);		
#endif    

    PINSEL_ConfigPin(Port_5, P_O2_CMD , 0);
    GPIO_SetDir(Port_5, (1<< P_O2_CMD ), GPIO_DIRECTION_OUTPUT);

    PINSEL_ConfigPin(Port_5, P_E2PROM_WP , 0);
    GPIO_SetDir(Port_5, (1<< P_E2PROM_WP ), GPIO_DIRECTION_OUTPUT);

    PINSEL_ConfigPin(Port_5, RELE_1 , 0);
    GPIO_SetDir(Port_5, (1<< RELE_1 ), GPIO_DIRECTION_OUTPUT);
    PINSEL_ConfigPin(Port_4, RELE_2 , 0);
    GPIO_SetDir(Port_4, (1<< RELE_2 ), GPIO_DIRECTION_OUTPUT);
    PINSEL_ConfigPin(Port_4, RELE_3 , 0);
    GPIO_SetDir(Port_4, (1<< RELE_3 ), GPIO_DIRECTION_OUTPUT);
    PINSEL_ConfigPin(Port_4, RELE_4 , 0);
    GPIO_SetDir(Port_4, (1<< RELE_4 ), GPIO_DIRECTION_OUTPUT);
    PINSEL_ConfigPin(Port_4, RELE_5 , 0);
    GPIO_SetDir(Port_4, (1<< RELE_5 ), GPIO_DIRECTION_OUTPUT);
    PINSEL_ConfigPin(Port_4, RELE_6 , 0);
    GPIO_SetDir(Port_4, (1<< RELE_6 ), GPIO_DIRECTION_OUTPUT);
    
		//PWM
		PINSEL_ConfigPin(Port_1, P_PWM_LCD , 2);
		
		//I2C_2 - gestione touch controller MIS-730 HDC ed E2PROM esterna
//    LPC_SC->PCONP     |= 0x04000000;
	  PINSEL_ConfigPin(Port_1, I2C2_SDA		 , 3); // P1.15 data
	  PINSEL_ConfigPin(Port_4, I2C2_SCL		 , 2); // P4.21 clock
		PINSEL_SetOpenDrainMode(Port_1, I2C2_SDA, ENABLE);
    PINSEL_SetOpenDrainMode(Port_4, I2C2_SCL, ENABLE);
//	  PINSEL_SetI2CMode(Port_1, I2C2_SDA, PINSEL_I2CMODE_FAST_STANDARD);
//  	PINSEL_SetI2CFilter(Port_1, I2C2_SDA, ENABLE);		

    PINSEL_ConfigPin(Port_4, TOUCH_RST , 0);
    GPIO_SetDir(Port_4, (1<< TOUCH_RST ), GPIO_DIRECTION_OUTPUT);
    PINSEL_ConfigPin(Port_4, TOUCH_INT , 0);
    GPIO_SetDir(Port_4, (1<< TOUCH_INT ), GPIO_DIRECTION_INPUT);
  	/* Initialize I2C_2 */
    I2C_Init(2, 100000);//100000 per MIS7300 ?
    I2C_Cmd(2, I2C_MASTER_MODE, ENABLE);
	  Iso_PressurSensorInit();

    Iso_ADCInit();

    //Ingressi digitali
		PINSEL_ConfigPin(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_1, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_2, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_3, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_4, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_5, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_6, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_7, 0);
		PINSEL_ConfigPin(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_8, 0);
		// Pin Direction : 
		GPIO_SetDir(PORT_DIG_IN_1_TO_4, (1<< (PIN_DIG_IN_1)),  GPIO_DIRECTION_INPUT);
		GPIO_SetDir(PORT_DIG_IN_1_TO_4, (1<< (PIN_DIG_IN_2)),  GPIO_DIRECTION_INPUT);
		GPIO_SetDir(PORT_DIG_IN_1_TO_4, (1<< (PIN_DIG_IN_3)),  GPIO_DIRECTION_INPUT);
		GPIO_SetDir(PORT_DIG_IN_1_TO_4, (1<< (PIN_DIG_IN_4)),  GPIO_DIRECTION_INPUT);
		GPIO_SetDir(PORT_DIG_IN_5_TO_8, (1<< (PIN_DIG_IN_5)),  GPIO_DIRECTION_INPUT);
		GPIO_SetDir(PORT_DIG_IN_5_TO_8, (1<< (PIN_DIG_IN_6)),  GPIO_DIRECTION_INPUT);	
		GPIO_SetDir(PORT_DIG_IN_5_TO_8, (1<< (PIN_DIG_IN_7)),  GPIO_DIRECTION_INPUT);
		GPIO_SetDir(PORT_DIG_IN_5_TO_8, (1<< (PIN_DIG_IN_8)),  GPIO_DIRECTION_INPUT);	
		// OUTPUT ENABLE (active low)
		PINSEL_SetPinMode(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_1, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_2, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_3, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_1_TO_4, PIN_DIG_IN_4, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_5, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_6, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_7, PINSEL_BASICMODE_PULLDOWN);
		PINSEL_SetPinMode(PORT_DIG_IN_5_TO_8, PIN_DIG_IN_8, PINSEL_BASICMODE_PULLDOWN);
		
		// LED
		PINSEL_ConfigPin(Port_5, uCS2_4, 0); // chip select per driver LED
    GPIO_SetDir(Port_5, (1<< uCS2_4 ), GPIO_DIRECTION_OUTPUT);
		PINSEL_ConfigPin(Port_0, LEDENABLE, 0); // abilita/disabilita uscita driver LED
    GPIO_SetDir(Port_0, (1<< LEDENABLE ), GPIO_DIRECTION_OUTPUT);

    //SSP2
  	// configura i pin per la SSP2 	
	  PINSEL_ConfigPin (Port_1, 0, 4);			// SSP2_SCK	
	  PINSEL_ConfigPin (Port_1, 4, 4);			// SSP2_MISO
	  PINSEL_ConfigPin (Port_1, 1, 4);			// SSP2_MOSI

	  // initialize SSP configuration structure to default
		
	  SSP_ConfigStructInit(&SSP_ConfigStruct2);
    // set clock rate
    SSP_ConfigStruct2.ClockRate = SSP2_CLOCK;
    SSP_ConfigStruct2.CPHA = SSP_CPHA_FIRST;
    SSP_ConfigStruct2.CPOL = SSP_CPOL_HI;
	  SSP_ConfigStruct2.Databit = SSP_DATABIT_8;
	  SSP_ConfigStruct2.Mode = SSP_MASTER_MODE ;
    // SSP_ConfigStruct.FrameFormat = SSP_FRAME_SPI ; 		

	  // Initialize SSP peripheral with parameter given in structure above
	  SSP_Init(SSP2_PORT, &SSP_ConfigStruct2);

	  // Enable SSP peripheral
	  SSP_Cmd(SSP2_PORT, ENABLE);	

    PINSEL_ConfigPin(Port_1, SSP2_CS, 0); // chip select per ADUM3154 di SSP2
    GPIO_SetDir(Port_1, (1<< SSP2_CS ), GPIO_DIRECTION_OUTPUT);

    ADC_SSP_init();

    //BUZZER
    PINSEL_ConfigPin(Port_1, 31 , 0);
    GPIO_SetDir(Port_1, (1<< 31 ), GPIO_DIRECTION_OUTPUT);

		// ----------------------------------------------------------------------------------------------------------
}

 /***********************************************************************
 * @brief		init_from_eeprom	
   legge i dati iniziali dalla eeprom
 **********************************************************************/
void init_from_eeprom (void) { 

}





 /***********************************************************************
 * @brief		init_from_eeprom	
   legge i dati iniziali dalla eeprom
 **********************************************************************/
void init_from_eeprom_old (void) { 
	
	unsigned int i = 0;

	/* ******************************************************** */
	/* 								PRIMA ACCENSIONE !!! 											*/
	/* ******************************************************** */
	
	// Leggi valore "First time"; se � la prima volta che ti accendi, scrivi i valori di default
	if (eeprom_read_byte(0,0) == 1)
		{
			// scrivi illuminazione
			eeprom_write_word(E2PROM_PAGE_OF_OTHER,REG_BACKLIGHT*2,20);
			
			// scrivi valori set e isteresi
			for (i = 1; i < (sys_zones_num + 1); i++ ){
					zones_array[i].set  = ( 16.0);
					zones_array[i].hyst = ( 0.1 );
					// Salva in modo permanente (EEPROM)
					temp = (zones_array[i].set * 10);
					eeprom_write_byte(1,i, temp);
				  temp = (zones_array[i].hyst * 10);
					eeprom_write_byte(2,i, temp);
  				eeprom_write_byte(5,i,0);  // Disabled
					eeprom_write_byte(12,i,0); // Activated
			}
			
			// scrivi valori thresholds high & low
			for (i = 1; i < (sys_sensors_num + 1); i++ ){
					sensors_array[i].th_high  = ( 21.0);
					sensors_array[i].th_low   = ( 12.0 );
					// Salva in modo permanente (EEPROM)
					temp = (sensors_array[i].th_high * 10);
					eeprom_write_byte(3,i, temp);
					temp = (sensors_array[i].th_low * 10);
					eeprom_write_byte(4,i, temp);
					eeprom_write_byte(6,i,0);  // Disabled
					eeprom_write_byte(13,i,0); // Activated
			}

			for (i=0; i< (sys_sirens_num); i++ ){
				eeprom_write_byte(7,i,0);  // Disabled
				eeprom_write_byte(14,i,0); // Activated
			}
			
			for (i=0; i< (sys_lamps_num ); i++ ){
				eeprom_write_byte(8,i,0);  // Disabled
				eeprom_write_byte(15,i,0); // Activated
			}
			
			for (i=1; i< (sys_panels_num + 1); i++ ){
				eeprom_write_byte(9,i,0);  // Disabled
				eeprom_write_byte(16,i,0); // Activated
			}
			
			for (i=0; i< (sys_valves_num); i++ ){
				eeprom_write_byte(10,i,0); // Disabled
				eeprom_write_byte(17,i,0); // Activated
			}
			
			for (i=1; i< (sys_ext_displays_num + 1); i++ ){
				eeprom_write_byte(11,i,0); // Disabled
				eeprom_write_byte(18,i,0); // Activated
			}	

			for (i=0; i< (sys_fluxes_num); i++ ){
				eeprom_write_byte(19,i,0); // Disabled
				eeprom_write_byte(20,i,0); // Activated
			}	
			
			// Buzzer
			eeprom_write_byte(22,0,0); // Disabled
			eeprom_write_byte(21,0,0); // Activated
			
			// Ricordati che la prima volta � andata
			eeprom_write_byte(0,0,0);
		
			// Scrivi "impostazione uscite allo stato iniziale" disabilitato
			eeprom_write_byte(0,3,0);
	}
	
	/* ******************************************************** */
	/* 								FINE PRIMA ACCENSIONE 										*/
	/* ******************************************************** */
	
	/* OPTIONS */
	// Leggi valore di retroilluminazione e applicalo
	backlight = 20;//eeprom_read_byte(0,1);
	setbacklight = 1;
	if (backlight<5)
		backlight = 20;
	eeprom_write_word(E2PROM_PAGE_OF_OTHER,REG_BACKLIGHT*2,backlight);
	
	last_loaded_backlight = backlight;
	ea_lcdb_ctrl_backlightContrast(backlight);
	
//	/* SET & HYST */
	for (i = 1; i < (sys_zones_num + 1); i++ ){
			float_temp = eeprom_read_byte(1,i); 
			zones_array[i].set  = ( float_temp / 10);
			float_temp = eeprom_read_byte(2,i);
			zones_array[i].hyst = (  float_temp / 10 );
	}	
	
	/* THRESHOLDS */
	for (i = 1; i < (sys_sensors_num + 1); i++ ){
			float_temp = eeprom_read_byte(3,i); 
			sensors_array[i].th_high = ( float_temp / 10);
			float_temp = eeprom_read_byte(4,i);
			sensors_array[i].th_low  = ( float_temp / 10 );
	}	
	
	/* DISABLEMENTS */
	for (i=1; i< (sys_zones_num + 1); i++ ){
		zones_array[i].disabled = eeprom_read_byte(5,i);
		zones_array_superuser_disabled[i] = zones_array[i].disabled;
		zones_array[i].disabled_from_display = eeprom_read_byte(23,i);
	}
	
	for (i=1; i< (sys_sensors_num + 1); i++ ){
		sensors_array[i].disabled = eeprom_read_byte(6,i);
		sensors_array_superuser_disabled[i] = sensors_array[i].disabled;
	}
	
	for (i=0; i< (sys_sirens_num); i++ ){
		sirens_array[i].disabled = eeprom_read_byte(7,i);
		sirens_array_superuser_disabled[i] = sirens_array[i].disabled;
	}
	
	for (i=0; i< (sys_lamps_num ); i++ ){
		lamps_array[i].disabled = eeprom_read_byte(8,i);
		lamps_array_superuser_disabled[i] = lamps_array[i].disabled;
	}
	
	for (i=1; i< (sys_panels_num + 1); i++ ){
		panels_array[i].disabled = eeprom_read_byte(9,i);
		panels_array_superuser_disabled[i] = panels_array[i].disabled;
	}
	
	for (i=0; i< (sys_valves_num); i++ ){
		valves_array[i].disabled = eeprom_read_byte(10,i);
		valves_array_superuser_disabled[i] = valves_array[i].disabled;
	}
	
	for (i=1; i< (sys_ext_displays_num + 1); i++ ){
		displays_array[i].disabled = eeprom_read_byte(11,i);
		displays_array_superuser_disabled[i] = displays_array[i].disabled;
	}	
	
	for (i=0; i< (sys_fluxes_num); i++ ){
		fluxes_array[i].disabled = eeprom_read_byte(19,i);
		fluxes_array_superuser_disabled[i] = fluxes_array[i].disabled;
	}
	
	cie_buzzer.disabled = eeprom_read_byte(22,0);
	
	/* DEVICES CONTROL */
	// impostazione uscite (per test)
	output_off_state = eeprom_read_byte(0,3);
	
	for (i=1; i< (sys_zones_num + 1); i++ ){
		zones_array[i].activated = eeprom_read_byte(12,i);
	}
	
	for (i=1; i< (sys_sensors_num + 1); i++ ){
		sensors_array[i].activated = eeprom_read_byte(13,i);
	}
	
	for (i=0; i< (sys_sirens_num); i++ ){
		sirens_array[i].activated = eeprom_read_byte(14,i);
	}
	
	for (i=0; i< (sys_lamps_num ); i++ ){
		lamps_array[i].activated = eeprom_read_byte(15,i);
	}
	
	for (i=1; i< (sys_panels_num + 1); i++ ){
		panels_array[i].activated = eeprom_read_byte(16,i);
	}
	
	for (i=0; i< (sys_valves_num); i++ ){
		valves_array[i].activated = eeprom_read_byte(17,i);
	}
	
	for (i=1; i< (sys_ext_displays_num + 1); i++ ){
		displays_array[i].activated = eeprom_read_byte(18,i);
	}	

	for (i=0; i< (sys_fluxes_num); i++ ){
		fluxes_array[i].activated = eeprom_read_byte(20,i);
	}
	
	cie_buzzer.activated = eeprom_read_byte(21,0);
	
	psa_array[0].orelavoro = eeprom_read_word(30,0);
	psa_array[1].orelavoro = eeprom_read_word(30,4);
	psa_array[0].secondi = eeprom_read_word(30,8);
	psa_array[1].secondi = eeprom_read_word(30,12);
	
}




// 20201002

#ifdef READ_TOUCH_SCREEN 
unsigned int i2c_x, i2c_y,i2c_z;  

void touch_press(void) {
  uint8_t i2c2_data[20];
  uint8_t i2c2_buf[20]; 
  uint32_t i2c_x_int, i2c_y_int;

  memset(&i2c2_data,0,7);
  
	// lettura coordinate tasto premuto
	i2c2_buf[0] = 0;
	I2CWrite(0x38,i2c2_buf,1);			
	I2CRead(0x38,i2c2_data,7);

  //legge posizioni x1 e x2 da i2c2
  i2c_y = ((i2c2_data[3] & 0x0F)*256+i2c2_data[4]);
  i2c_x = ((i2c2_data[5] & 0x0F)*256+i2c2_data[6]);
  i2c_z = (i2c2_data[2] & 0x0F);

}
#endif //READ_TOUCH_SCREEN




 /***********************************************************************
 * @brief		touch_handle			
	 legge touch, gestisce le aree touch e esegue le rispettive funzioni
 **********************************************************************/
 uint32_t last_i2c_x, last_i2c_y, last_i2c_z;
 
void touch_handle(void) {
	unsigned int tempx, tempy;
  
  uint8_t i2c2_data[20];
  uint8_t i2c2_buf[20]; 
  uint16_t x=0;
  uint16_t y=0;
  uint16_t z=0;
  uint32_t i2c_x_int, i2c_y_int;
#ifndef READ_TOUCH_SCREEN   
	unsigned int i2c_x, i2c_y;
#endif  //READ_TOUCH_SCREEN
	

#ifndef READ_TOUCH_SCREEN 
  memset(&i2c2_data,0,7);
  
  // lettura coordinate tasto premuto
	i2c2_buf[0] = 0;
	I2CWrite(0x38,i2c2_buf,1);			
	I2CRead(0x38,i2c2_data,7);

  //legge posizioni x1 e x2 da i2c2
  i2c_y = ((i2c2_data[3] & 0x0F)*256+i2c2_data[4]);
  i2c_x = ((i2c2_data[5] & 0x0F)*256+i2c2_data[6]);
  z = (i2c2_data[2] & 0x0F);

#else
  z = i2c_z ;
#endif //READ_TOUCH_SCREEN

  if (z)
	{	// Tasto PREMUTO  
		if ( primo_tasto == 1)
			{	// rilevato "tasto premuto" all'accensione ---> NON VALIDO: NON GESTIRE!!!
				primo_tasto++;
				last_i2c_x = i2c_x; // Valore tasto premuto
				last_i2c_y = i2c_y;			
				return;
		}
		if ( primo_tasto == 2)
		{	// Attesa nuovo tasto dopo accensione
			if ((last_i2c_x != i2c_x) && (last_i2c_y != i2c_y))
				{ // Valore tasto cambiato ---> VALIDO:  GESTIRE!!!
					TIM10.msec_timer = 50;	// timer di guardia
					last_i2c_x = i2c_x; 		// ultimo tasto premuto
					last_i2c_y = i2c_y;			
					last_i2c_z = z;	
					primo_tasto = FALSE;		// Sblocco per tasti successivi
			}
			else // Valore tasto NON cambiato ---> NON VALIDO: NON GESTIRE!!!
				return;
		}      
	}
	else					
	{ // Tasto NON premuto    
		primo_tasto = FALSE;
		if (TIM10.msec_timer==0)
		{
			if (cornice)
			{
				touch_release(cornice);        
				touch_not_released = 0;
				cornice = 0;
				deactivate_buttons();          
			}
		}		
		return;
	}
// 20200330
  
	// Tasto premuto: Gestione Touch Area		    
		//converte la risoluzione del touch controller nella risoluzione della easygui (4096x4096)
		x = (unsigned int)4096*(960-i2c_x)/960;//960 sono i punti che conta il touch sull'asse orizzontale
    y = (unsigned int)4096*(620-i2c_y)/620;//620 sono i punti che conta il touch sull'asse verticale

	 
	if (UPSIDEDOWN==0){
		if (y != 0) {
			tempy = y;
		} else {
			tempy = 0;
		}
		tempx = x;
	}else{
		if (y != 0) {
			tempy = 4096 - y;
		} else {
			tempy = 0;
		}
		tempx = 4096 - x;
	}
	
	// passo le coordinate x e y per l'individuazione dell'area selezionata
	touch_area = GuiLib_TouchCheck(tempx,tempy);
	
	if (touch_area == -1) {      
  /* Not pressing anything OR pressing inactive area */
    if ( z == 0) {			
    /* Not pressing anything */
      if (cornice == 0) {
          // Non avevo premuto niente neanche prima								
          // questo lo metto per sicurezza ma in realt� non servirebbe...
          deactivate_buttons();	
      } else {
        if (TIM10.msec_timer==0){
          touch_release(cornice);
          touch_not_released = 0;
        }
      }
    } else {
      /* Pressing inactive area */
        if (cornice) 
        {	
          touch_release(cornice);
          cornice = 0;					
          deactivate_buttons();
        }							
     }
  } else {
  /* Pressing active area */
    if ((touch_area) && (touch_area != cornice)) {

// 20200805      
      if (cornice)
      {
//        touch_release(cornice); 
        deactivate_buttons();      
      }
      
      touch_initial(touch_area);
      touch_time=0;
    } else {
      if (touch_time<10)
        touch_time++;       
      touch_continued(touch_area);        
    }
  }
}

 /***********************************************************************
 * @brief		get_string_data		
 **********************************************************************/
void get_file_names (struct Time data) {

	unsigned int i = 0;
	unsigned int int_store   = 0;
	
	//--------------------------------------------------------------------------------
	// get data 
	
	data_string_name[0] = '_';
	
	// get year
	int_store = data.year;
	int_store = int_store / 1000;
	data_string_name[1] = ('0' + int_store % 10);
	int_store = data.year;
	int_store = int_store / 100;
	data_string_name[2] = ('0' + int_store % 10);
	int_store = data.year;
	int_store = int_store / 10;
	data_string_name[3] = ('0' + int_store % 10);	
	int_store = data.year;
	data_string_name[4] = ('0' + int_store % 10);
	data_string_name[5] = '.';
	
	// get month
	int_store = data.month;
	int_store = int_store / 10;
	data_string_name[6] = ('0' + int_store % 10);
	int_store = data.month;
	data_string_name[7] = ('0' + int_store % 10);
	data_string_name[8] = '.';
	
	// get day
	int_store = data.day;
	int_store = int_store / 10;
	data_string_name[9] = ('0' + int_store % 10);
	int_store = data.day;
	data_string_name[10] = ('0' + int_store % 10);
	
	//--------------------------------------------------------------------------------
	// alarms file 
	
	alarms_file_name[0] = 'A';
	alarms_file_name[1] = 'L';
	alarms_file_name[2] = 'A';
	alarms_file_name[3] = 'R';
	alarms_file_name[4] = 'M';
	alarms_file_name[5] = 'S';
	alarms_file_name[6] = 92; // '\'
	alarms_file_name[7] = 'a';
	alarms_file_name[8] = 'l';
	alarms_file_name[9] = 'a';
	alarms_file_name[10] = 'r';
	alarms_file_name[11] = 'm';
	alarms_file_name[12] = 's';
	
	for (i=0; i< 11; i++){
		alarms_file_name[13 + i] = data_string_name[i];
	}
	
	alarms_file_name[24] = '.';
	alarms_file_name[25] = 't';
	alarms_file_name[26] = 'x';
	alarms_file_name[27] = 't';
	alarms_file_name[28] = '\0';
	
	//--------------------------------------------------------------------------------
	// events file

	events_file_name[0] = 'E';
	events_file_name[1] = 'V';
	events_file_name[2] = 'E';
	events_file_name[3] = 'N';
	events_file_name[4] = 'T';
	events_file_name[5] = 'S';
	events_file_name[6] = 92;
	events_file_name[7] = 'e';
	events_file_name[8] = 'v';
	events_file_name[9] = 'e';
	events_file_name[10] = 'n';
	events_file_name[11] = 't';
	events_file_name[12] = 's';
	
	for (i=0; i< 11; i++){
		events_file_name[13 + i] = data_string_name[i];
	}
	
	events_file_name[24] = '.';
	events_file_name[25] = 't';
	events_file_name[26] = 'x';
	events_file_name[27] = 't';
	events_file_name[28] = '\0';
	
// 	//--------------------------------------------------------------------------------
// 	// o2_values file

	o2_values_file_name[0] = 'O';
	o2_values_file_name[1] = '2';
	o2_values_file_name[2] = '_';
	o2_values_file_name[3] = 'V';
	o2_values_file_name[4] = 'A';
	o2_values_file_name[5] = 'L';
	o2_values_file_name[6] = 'U';
	o2_values_file_name[7] = 'E';
	o2_values_file_name[8] = 'S';
	o2_values_file_name[9] = 92; // '\'
	o2_values_file_name[10] = 'O';
	o2_values_file_name[11] = '2';
	o2_values_file_name[12] = '_';
	o2_values_file_name[13] = 'v';
	o2_values_file_name[14] = 'a';
	o2_values_file_name[15] = 'l';
	o2_values_file_name[16] = 'u';
	o2_values_file_name[17] = 'e';
	o2_values_file_name[18] = 's';
	
	for (i=0; i< 11; i++){
		o2_values_file_name[19 + i] = data_string_name[i];
	}
	
	o2_values_file_name[30] = '.';
	o2_values_file_name[31] = 't';
	o2_values_file_name[32] = 'x';
	o2_values_file_name[33] = 't';
	o2_values_file_name[34] = '\0';
	
}

 /***********************************************************************
 * @brief		check_idle_state			
	 controlla se sono passati 15 secondi dall'ultima volta che � stato premuto qualcosa
 **********************************************************************/
void check_idle_state (void) {
	
	if (idle_state == 0) {
	// IDLE STATE
//		if (
//			  (alarms_number 				== 0) &&
//				(faults_number 				== 0) &&
//				(disablements_number  == 0) &&
//				(dev_ctrl_number      == 0) &&
//				(idle_screen_timeout  == 0)
//		 ) {
		if (idle_screen_timeout  == 0){
			if (GuiLib_CurStructureNdx != GuiStruct_Main_0) {
				GuiLib_CurStructureNdx = GuiStruct_Main_0;
				GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
				GuiVar_message = 0;
			}
//			if (GuiLib_CurStructureNdx == GuiStruct_Main_0)
//				idle_screen_timeout = 1;
			ea_lcdb_ctrl_backlightContrast(1);
			last_loaded_backlight = 1;
		} 
//		else {
//				if (GuiLib_CurStructureNdx != GuiStruct_Main_0) {
//					GuiLib_CurStructureNdx = GuiStruct_Main_0;
//					GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
//				}
//				
//				if (alarms_number != 0) {
//					if (GuiVar_screen_select != CHARTS_SCREEN) {
//						GuiVar_screen_select = CHARTS_SCREEN;
//						init_screen(CHARTS_SCREEN);
//						press_button(7);
//					}
//				} else {
//					if (GuiVar_screen_select != SYSTEM_SCREEN) {
//						GuiVar_screen_select = SYSTEM_SCREEN;
//						init_screen(SYSTEM_SCREEN);
//						press_button(0);
//					}
//				}				
//		}

	}
}
	
 /***********************************************************************
 * @brief		clear_line			
	 \param [in] linea che si vuole cancellare e indicare come "vuota" [ da 1 a 5 ]
	 cancella una linea (quando associata a nessun evento)
 **********************************************************************/
void clear_line (unsigned int line) {
	
	unsigned int s = 0;
	
	// Cancella quella linea !
	if (line == LINE_1) {
			for (s=0; s<56; s++) {
				GuiVar_events_line_1_string_1[s] = ' ';
			}
			GuiVar_flux_line_1  = 0;
			GuiVar_ev_1_day     = 100;
			GuiVar_ev_1_month   = 100;
			GuiVar_ev_1_year    = 10000;
			GuiVar_ev_1_hour    = 100;
			GuiVar_ev_1_minutes = 100;
			GuiVar_ev_1_seconds = 100;
			
			GuiVar_line_1_bkg_color = WHITE;
	}				
	if (line == LINE_2) {
			for (s=0; s<56; s++) {
				GuiVar_events_line_2_string_1[s] = ' ';
			}
			GuiVar_flux_line_2  = 0;
			GuiVar_ev_2_day     = 100;
			GuiVar_ev_2_month   = 100;
			GuiVar_ev_2_year    = 10000;
			GuiVar_ev_2_hour    = 100;
			GuiVar_ev_2_minutes = 100;
			GuiVar_ev_2_seconds = 100;
			
			GuiVar_line_2_bkg_color = WHITE;
	}		
	if (line == LINE_3) {
			for (s=0; s<56; s++) {
				GuiVar_events_line_3_string_1[s] = ' ';
			}
			GuiVar_flux_line_3  = 0;
			GuiVar_ev_3_day     = 100;
			GuiVar_ev_3_month   = 100;
			GuiVar_ev_3_year    = 10000;
			GuiVar_ev_3_hour    = 100;
			GuiVar_ev_3_minutes = 100;
			GuiVar_ev_3_seconds = 100;
			
			GuiVar_line_3_bkg_color = WHITE;
	}		
	if (line == LINE_4) {
			for (s=0; s<56; s++) {
				GuiVar_events_line_4_string_1[s] = ' ';
			}
			GuiVar_flux_line_4  = 0;
			GuiVar_ev_4_day     = 100;
			GuiVar_ev_4_month   = 100;
			GuiVar_ev_4_year    = 10000;
			GuiVar_ev_4_hour    = 100;
			GuiVar_ev_4_minutes = 100;
			GuiVar_ev_4_seconds = 100;
			
			GuiVar_line_4_bkg_color = WHITE;
	}	
	if (line == LINE_5) {
			for (s=0; s<56; s++) {
				GuiVar_events_line_5_string_1[s] = ' ';
			}
			GuiVar_flux_line_5  = 0;
			GuiVar_ev_5_day     = 100;
			GuiVar_ev_5_month   = 100;
			GuiVar_ev_5_year    = 10000;
			GuiVar_ev_5_hour    = 100;
			GuiVar_ev_5_minutes = 100;
			GuiVar_ev_5_seconds = 100;
			
			GuiVar_line_5_bkg_color = WHITE;
	}	
	
}	


 /***********************************************************************
 * @brief		check_alarms			
	 controlla se ci sono sensori o zone in allarme
 **********************************************************************/
void check_alarms (void) {
}

 /***********************************************************************
 * @brief		check_disablements			
	 controlla quali elementi sono disailitati e li aggiunge alla lista
 **********************************************************************/
void check_disablements (void) {
}

 /***********************************************************************
 * @brief		check_dev_ctrl			
	 controlla quali elementi sono attivati e li aggiunge alla lista
 **********************************************************************/
void check_dev_ctrl (void) {
}

 


void show_fault (struct Fault Temp, unsigned int line) {
		
	unsigned int length = 0;
	unsigned int z = 0;
	
	// Cancella quella linea !
	if (line == LINE_1) {
			for (z=0; z<55; z++) {
				GuiVar_events_line_1_string_1[z] = ' ';
			}
	}	else			
	if (line == LINE_2) {
			for (z=0; z<55; z++) {
				GuiVar_events_line_2_string_1[z] = ' ';
			}
	}	else	
	if (line == LINE_3) {
			for (z=0; z<55; z++) {
				GuiVar_events_line_3_string_1[z] = ' ';
			}
	}	else	
	if (line == LINE_4) {
			for (z=0; z<55; z++) {
				GuiVar_events_line_4_string_1[z] = ' ';
			}
	}	else	
	if (line == LINE_5) {
			for (z=0; z<55; z++) {
				GuiVar_events_line_5_string_1[z] = ' ';
			}
	}	
	
	switch (Temp.type)
	{
		/* SENSOR FAULT */
		case F_TYPE_SENSOR:
			switch (Temp.specific_type)
			{
				case F_SENSOR_COMM_FAULT: 
	        length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_SENSORE, ID_MSG_GUASTO_COMUNICAZIONE_SENSORE, 0);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
  			break;

	  		case F_SENSOR_CAN1_FAULT: 
		      length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
					temp_string[length++] = '1';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_SENSORE, ID_MSG_GUASTO_COMUNICAZIONE_SENSORE, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
        break;

	  		case F_SENSOR_CAN2_FAULT: 
		      length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
					temp_string[length++] = '2';
					temp_string[length++] = ' ';
	        length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_SENSORE, ID_MSG_GUASTO_COMUNICAZIONE_SENSORE, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
        break;
					
				case F_SENSOR_ELAB_FAULT: 
		      length += Iso_Printf(MSG_ELAB_FAULT_SENSOR, ID_MSG_ELAB_FAULT_SENSOR, 0);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
	        length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
  			break;
				
				case F_SENSOR_SD_FAULT: 
				break;	

				case F_SENSOR_WARM_UP: 
		      length += Iso_Printf(MSG_WARMUP, ID_MSG_WARMUP, 0);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
	        length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
			}			
		break;
		
		case F_TYPE_O2_DIFF: 
			switch (Temp.specific_type)
			{
				case F_TYPE_O2_DIFF_1: 
		      length += Iso_Printf(MSG_O2_MAG_05, ID_MSG_O2_MAG_05, 0);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
					temp_string[length++] = ' ';
					temp_string[length++] = '&';
					temp_string[length++] = ' ';
    		  length += print_number(NUM_2_DIGITS,length,address,(Temp.id + 1),0);// numero
					temp_string[length++] = ' ';
					temp_string[length++] = 'Z';
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
				
				case F_TYPE_O2_DIFF_2: 
  				length += Iso_Printf(MSG_O2_MAG_20, ID_MSG_O2_MAG_20, 0);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero
					temp_string[length++] = ' ';
					temp_string[length++] = '&';
					temp_string[length++] = ' ';
    		  length += print_number(NUM_2_DIGITS,length,address,(Temp.id + 1),0);// numero
					temp_string[length++] = ' ';
					temp_string[length++] = 'Z';
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
			} // Fine switch(specific_type)				
		break;
		
		case F_TYPE_FLUX:
			switch (Temp.specific_type)
			{
				case F_TYPE_AUX_FLUX: 
		      length += Iso_Printf(MSG_AUX_FLOW_FAULT, ID_MSG_AUX_FLOW_FAULT, 0);
				break;
				
				case F_TYPE_ZONE_FLUX: 
		      length += Iso_Printf(MSG_FLUX, ID_MSG_FLUX, 0);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero flux
	        length += Iso_Printf(MSG_FAULT_ZONE, ID_MSG_FAULT_ZONE, length);
    		  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
			} // Fine switch(specific_type)						
		break;
		
		case F_TYPE_SWITCHES:
		    length += Iso_Printf(MSG_SWITCH, ID_MSG_SWITCH, 0);
    		length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero switch
	      length += Iso_Printf(MSG_FAULT_ZONE, ID_MSG_FAULT_ZONE, length);
    		length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
		break;		
		
		case F_TYPE_PW_SUPPLY:
			switch (Temp.specific_type)
			{
				case F_TYPE_PW_BROKEN: 
		      length += Iso_Printf(MSG_ALIMENTATORE, ID_MSG_ALIMENTATORE, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero rele
	        length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
				break;
				
				case F_TYPE_PW_NO_NET: 
		      length += Iso_Printf(MSG_ALIMENTATORE, ID_MSG_ALIMENTATORE, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero rele
	        length += Iso_Printf(MSG_GUASTO_RETE, ID_MSG_GUASTO_RETE, length);
				break;
				
				case F_TYPE_PW_BATTERY: 
		      length += Iso_Printf(MSG_BATTERY, ID_MSG_BATTERY, 0);
    			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero rele
	        length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
				break;
				
			} // Fine switch(specific_type)						
		break;
		
		case F_TYPE_THERMOSTAT:
  				    length += Iso_Printf(MSG_TEMP_SM, ID_MSG_TEMP_SM, 0);
							temp_string[length++] = ' ';
							temp_string[length++] = '1';
							temp_string[length++] = ' ';
							temp_string[length++] = '<';				
							temp_string[length++] = ' ';
							temp_string[length++] = '5';
							temp_string[length++] = ' ';
							temp_string[length++] = 176; // �
							temp_string[length++] = 'C';
							temp_string[length++] = ' ';
							temp_string[length++] = '/';
							temp_string[length++] = ' ';
							temp_string[length++] = '>';				
							temp_string[length++] = ' ';
							temp_string[length++] = '4';
							temp_string[length++] = '5';
							temp_string[length++] = ' ';
							temp_string[length++] = 176; // �
							temp_string[length++] = 'C';
				break;
							
				
				case F_TYPE_THERMOSTAT_2:
  				    length += Iso_Printf(MSG_TEMP_SM, ID_MSG_TEMP_SM, 0);
							temp_string[length++] = ' ';
							temp_string[length++] = '2';
							temp_string[length++] = ' ';
							temp_string[length++] = '<';				
							temp_string[length++] = ' ';
							temp_string[length++] = '5';
							temp_string[length++] = ' ';
							temp_string[length++] = 176; // �
							temp_string[length++] = 'C';
							temp_string[length++] = ' ';
							temp_string[length++] = '/';
							temp_string[length++] = ' ';
							temp_string[length++] = '>';				
							temp_string[length++] = ' ';
							temp_string[length++] = '4';
							temp_string[length++] = '5';
							temp_string[length++] = ' ';
							temp_string[length++] = 176; // �
							temp_string[length++] = 'C';
				break;
				
		case F_TYPE_PSA:
			switch (Temp.specific_type)
			{
				case F_TYPE_PSA_BROKEN:
	        length += Iso_Printf(MSG_MACHINE, ID_MSG_MACHINE, 0);
	        length += Iso_Printf(MSG_FAULT, ID_MSG_FAULT, length);
				break;
				
				case 2: 

				break;
			} // Fine switch(specific_type)						
		break;
		
		case F_TYPE_EXTERNAL:
	    length += Iso_Printf(MSG_EXT_FAULT, ID_MSG_EXT_FAULT, 0);
		break;
		
		
			case F_TYPE_NETWORK:
	      length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE, ID_MSG_GUASTO_COMUNICAZIONE, 0);
			break;

			case F_TYPE_SBM_SD_FAULT:
	      length += Iso_Printf(MSG_SD_SBM, ID_MSG_SD_SBM, 0);
			break;

			/* DISPLAY FAULT */
			case F_TYPE_EXT_DISPLAY:
				switch (Temp.specific_type)
				{
					case F_TYPE_EXT_DISPLAY_COMM_FAULT_CAN_1: case F_TYPE_EXT_DISPLAY_COMM_FAULT_CAN_2:
						length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
						temp_string[length++] = ('0' + Temp.specific_type);
					  temp_string[length++] = ' ';
	          length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_DISPLAY, ID_MSG_GUASTO_COMUNICAZIONE_DISPLAY, length);
					  length += print_number(NUM_2_DIGITS,length,address,Temp.id,0); // numero display
	          length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
					  length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0); // numero zona
				  break;
				
			  }			
		    break;

			case F_TYPE_WEBSERVER:
			  switch (Temp.specific_type)
				{
					case F_TYPE_WEBSERVER_COMM_FAULT_CAN_1: case F_TYPE_WEBSERVER_COMM_FAULT_CAN_2:
						length += Iso_Printf(MSG_CAN, ID_MSG_CAN, 0);
						temp_string[length++] = ('0' + Temp.specific_type);
				    temp_string[length++] = ' ';
	          length += Iso_Printf(MSG_GUASTO_COMUNICAZIONE_WEBSERVER, ID_MSG_GUASTO_COMUNICAZIONE_WEBSERVER, length);
					break;
					
				} // Fine switch(specific_type)						
				break;
				
		case F_TYPE_EWON:
			switch (Temp.specific_type)
			{
				case F_TYPE_EWON_COMM_FAULT:
	        length = Iso_Printf(MSG_GUASTO_COMUNICAZIONE_ANYBUS, ID_MSG_GUASTO_COMUNICAZIONE_ANYBUS, 0);
				break;
				
			}// Fine switch(specific_type)						
		break;

		case F_TYPE_ADOX:
			switch (Temp.specific_type)
			{
					case F_TYPE_ADOX_FAULT: 
						temp_string[0] = 'M';
						temp_string[1] = 'i';
						temp_string[2] = 'n';
						temp_string[3] = 'i';							
						temp_string[4] = 'A';
						temp_string[5] = 'd';
						temp_string[6] = 'o';
						temp_string[7] = 'x';
						temp_string[8] = ' ';
						temp_string[9] = 'f';
						temp_string[10] = 'a';
						temp_string[11] = 'u';
						temp_string[12] = 'l';
						temp_string[13] = 't';
						temp_string[14] = ' ';
						length = 15;
					break;
					
					case F_TYPE_ADOX_COMM_FAULT: 
	          length = Iso_Printf(MSG_GUASTO_COMUNICAZIONE_ADOX, ID_MSG_GUASTO_COMUNICAZIONE_ADOX, 0);
					break;						
				
			} // Fine switch(specific_type)						
		break;
			
		case F_TYPE_CIE:
			switch (Temp.specific_type)
			{
				case F_TYPE_CIE_COMM_FAULT: 
	        length = Iso_Printf(MSG_GUASTO_COMUNICAZIONE_CIE, ID_MSG_GUASTO_COMUNICAZIONE_CIE, 0);
				break;
				
			} // Fine switch(specific_type)						
		break;
			
	} // Fine switch(type) 					
	     
	if (line == LINE_1) {	
			for (z=0; z<length; z++) {
				GuiVar_events_line_1_string_1[z] = temp_string[z];
			}
	}				
	if (line == LINE_2) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_2_string_1[z] = temp_string[z];
			}
	}		
	if (line == LINE_3) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_3_string_1[z] = temp_string[z];
			}
	}		
	if (line == LINE_4) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_4_string_1[z] = temp_string[z];
			}
	}		
	if (line == LINE_5) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_5_string_1[z] = temp_string[z];
			}
	}			

}

 /***********************************************************************
 * @brief	show_object			
	 \param [in] struttura (temporanea) relativa all'oggetto da visualizzare
	 \param [in] linea su cui voglio visualizzare l'oggetto [ da 1 a 5 ]
	 visualizza un oggetto sulla linea indicata
 **********************************************************************/
void show_object (struct Object Temp, unsigned int line) {
		
//	char temp_string[45];
//	char * address = &temp_string[0];
	unsigned int length = 0;
	unsigned int z = 0;
	
	// Cancella quella linea !
	if (line == LINE_1) {
			clear_line(LINE_1);
	}	else			
	if (line == LINE_2) {
			clear_line(LINE_2);
	}	else	
	if (line == LINE_3) {
			clear_line(LINE_3);
	}	else	
	if (line == LINE_4) {
			clear_line(LINE_4);
	}	else	
	if (line == LINE_5) {
			clear_line(LINE_5);
	}	
	
	switch (Temp.type)
	{
		case O_TYPE_ZONE:
			length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, 0);
			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero zona				
		break;
		
		case O_TYPE_SENSOR:
			temp_string[length++] = ' ';
			temp_string[length++] = '-';
			temp_string[length++] = ' ';			
			length += Iso_Printf(MSG_O2_SENSOR, ID_MSG_O2_SENSOR, length);
			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sensore				
			length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
			length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
		break;
		
		case O_TYPE_SIREN:
			switch (Temp.specific_type)
			{
				case O_TYPE_SIREN_CIE: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_SIREN, ID_MSG_SIREN, length);
			    temp_string[length++] = 'C';
			    temp_string[length++] = 'I';
			    temp_string[length++] = 'E';			
				break;
				
				case O_TYPE_SIREN_ZONE: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_SIREN, ID_MSG_SIREN, length);
			    length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sirena				
			    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
			    length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
			}		
		break;
		
		case O_TYPE_LAMP:
			switch (Temp.specific_type)
			{
				case O_TYPE_LAMP_CIE: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_LAMP, ID_MSG_LAMP, length);
			    temp_string[length++] = 'C';
			    temp_string[length++] = 'I';
			    temp_string[length++] = 'E';			
				break;
				
				case O_TYPE_LAMP_ZONE: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_LAMP, ID_MSG_LAMP, length);
			    length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sirena				
			    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
			    length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
			}				
		break;
		
		case O_TYPE_PANEL:
			temp_string[length++] = ' ';
			temp_string[length++] = '-';
			temp_string[length++] = ' ';			
			length += Iso_Printf(MSG_PANEL, ID_MSG_PANEL, length);
			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sirena				
			length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
			if (Temp.zone==0) //sistemare!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				Temp.zone++;
			length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
		break;

		case O_TYPE_VALVE:
			switch (Temp.specific_type)
			{
				case O_TYPE_VALVE_AUX: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_AUX_VALVE, ID_MSG_AUX_VALVE, length);
					temp_string[length++] = '(';
			    length += Iso_Printf(MSG_EXT_N2, ID_MSG_EXT_N2, length);
					temp_string[length++] = ')';
				break;
				
				case O_TYPE_VALVE_ZONE: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_VALVE, ID_MSG_VALVE, length);
			    length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sirena				
			    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
				  if (Temp.zone==0) //sistemare!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						Temp.zone++;
			    length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
			}				
		break;

		case O_TYPE_EXT_DISPLAY:
			temp_string[length++] = ' ';
			temp_string[length++] = '-';
			temp_string[length++] = ' ';			
			length += Iso_Printf(MSG_DISPLAY, ID_MSG_DISPLAY, length);
			length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sirena				
			length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
			length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
		break;

		case O_TYPE_FLUX:
			switch (Temp.specific_type)
			{
				case O_TYPE_FLUX_AUX: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_AUX_FLUX, ID_MSG_AUX_FLUX, length);
					temp_string[length++] = '(';
			    length += Iso_Printf(MSG_EXT_N2, ID_MSG_EXT_N2, length);
					temp_string[length++] = ')';
				break;
				
				case O_TYPE_FLUX_ZONE: 
			    temp_string[length++] = ' ';
			    temp_string[length++] = '-';
			    temp_string[length++] = ' ';			
			    length += Iso_Printf(MSG_VALVE, ID_MSG_VALVE, length);
			    length += print_number(NUM_2_DIGITS,length,address,Temp.id,0);// numero sirena				
			    length += Iso_Printf(MSG_ZONA, ID_MSG_ZONA, length);
				  if (Temp.zone==0) //sistemare!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						Temp.zone++;
			    length += print_number(NUM_2_DIGITS,length,address,Temp.zone,0);// numero zona
				break;
						
			}					
		break;

		case O_TYPE_BUZZER: 
			temp_string[length++] = ' ';
			temp_string[length++] = '-';
			temp_string[length++] = ' ';			
			length += Iso_Printf(MSG_BUZZER, ID_MSG_BUZZER, length);
			temp_string[length++] = 'C';
			temp_string[length++] = 'I';
			temp_string[length++] = 'E';			
		break;
				
	} // Fine switch(type)
	
	if (line == LINE_1) {	
			for (z=0; z<length; z++) {
				GuiVar_events_line_1_string_1[z] = temp_string[z];
			}
	}				
	if (line == LINE_2) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_2_string_1[z] = temp_string[z];
			}
	}		
	if (line == LINE_3) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_3_string_1[z] = temp_string[z];
			}
	}		
	if (line == LINE_4) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_4_string_1[z] = temp_string[z];
			}
	}		
	if (line == LINE_5) {
			for (z=0; z<length; z++) {
				GuiVar_events_line_5_string_1[z] = temp_string[z];
			}
	}			

}



 

 /***********************************************************************
 * @brief		init_sd_files			
 **********************************************************************/
void init_sd_files (void) { 
	
	alarms_file = fopen(alarms_file_name, "a");
	fputs("ALARMS LIST:", alarms_file);
	fputc('\r',alarms_file);
	fputc('\r',alarms_file);
	fputc('\n',alarms_file);
	fputc('\n',alarms_file);	
	fclose(alarms_file);
	
	events_file = fopen(events_file_name, "a");
	fputs("EVENTS LIST:", events_file);
	fputc('\r',events_file);
	fputc('\r',events_file);
	fputc('\n',events_file);
	fputc('\n',events_file);	
	fclose(events_file);

	o2_values_file = fopen(o2_values_file_name, "a");
	fputs("O2 VALUES:", o2_values_file);
	fputc('\r',alarms_file);
	fputc('\r',alarms_file);
	fputc('\n',alarms_file);
	fputc('\n',alarms_file);	
	fclose(o2_values_file);
}

 


 
 /***********************************************************************
 * @brief		update_graphics_variables			
	 aggiorna alcune variabili della grafica in tempo reale (bottoni disabilitati, ecc.)
 **********************************************************************/
void update_graphics_variables (void) {
	
		unsigned int i = 0;
	
		/* MAIN */
	
		/* BOTTOM BUTTONS BAR*/
		if (test_leds_timeout == 0) {
				// ALARM Button color
				if (alarms_number != 0) {
					if (GuiVar_button_alarms == 1) {
							GuiVar_alarm_color = LIGHT_RED;
					} else {
							GuiVar_alarm_color = DARK_RED;
					}			
				} else {
					if (GuiVar_button_alarms == 1) {
							GuiVar_alarm_color = LIGHT_GREY;
					} else {
							GuiVar_alarm_color = DARK_GREY;
					}		
				}

				// FAULTS Button color
				if (faults_number != 0) {
					if (GuiVar_button_faults == 1) {
							GuiVar_faults_color = LIGHT_YELLOW;
					} else {
							GuiVar_faults_color = DARK_YELLOW;
					}			
				} else {
					if (GuiVar_button_faults == 1) {
							GuiVar_faults_color = LIGHT_GREY;
					} else {
							GuiVar_faults_color = DARK_GREY;
					}		
				}

				// DISABLEMENTS Button color
				if (disablements_number != 0) {
					if (GuiVar_button_disablements == 1) {
							GuiVar_disablements_color = LIGHT_YELLOW;
					} else {
							GuiVar_disablements_color = DARK_YELLOW;
					}			
				} else {
					if (GuiVar_button_disablements == 1) {
							GuiVar_disablements_color = LIGHT_GREY;
					} else {
							GuiVar_disablements_color = DARK_GREY;
					}		
				}		

				// DEVICES CONTROL Button color
				if (dev_ctrl_number != 0) {
					if (GuiVar_button_dev_ctrl == 1) {
							GuiVar_dev_ctrl_color = LIGHT_YELLOW;
					} else {
							GuiVar_dev_ctrl_color = DARK_YELLOW;
					}			
				} else {
					if (GuiVar_button_dev_ctrl == 1) {
							GuiVar_dev_ctrl_color = LIGHT_GREY;
					} else {
							GuiVar_dev_ctrl_color = DARK_GREY;
					}		
				}			
		}
		
		/* TEST DEI COLORI DELLA GRAFICA !!! */
		if (test_leds_timeout > 0){
			GuiVar_alarm_color  			= DARK_RED;
			GuiVar_faults_color 		  = DARK_YELLOW;
			GuiVar_disablements_color = DARK_YELLOW;
			GuiVar_dev_ctrl_color		  = DARK_YELLOW;						
		}
		
		/* LEFT BUTTONS BAR*/
		
		// Set button color
		if (GuiVar_button_set == 1) { 
				GuiVar_set_color = LIGHT_GREEN; 
		} else {
				if (GuiVar_button_set != 2) {
					GuiVar_set_color = DARK_BLUE; 
				}
		}
		// Th button color
		if (GuiVar_button_th == 1) { 
				GuiVar_th_color = LIGHT_GREEN; 
		} else {
				if (GuiVar_button_th != 2) {
					GuiVar_th_color = DARK_BLUE; 
				}
		}
		// Log button color
		if (GuiVar_button_log == 1) { 
				GuiVar_log_color = LIGHT_GREEN; 
		} else {
				if (GuiVar_button_log != 2) {
					GuiVar_log_color = DARK_BLUE; 
				}
		}
		// Options button color
		if (GuiVar_button_options == 1) { 
				GuiVar_options_color = LIGHT_GREEN; 
		} else {
				if (GuiVar_button_options != 2) {
					GuiVar_options_color = DARK_BLUE; 
				}
		}
		// Events button color
		if (GuiVar_button_events == 1) { 
				GuiVar_events_color = DEEP_PURPLE; 
		} else {
				if (GuiVar_button_events != 2) {
					GuiVar_events_color = DARK_BLUE; 
				}
		}
		// Info button color
		if (GuiVar_button_info == 1) { 
				GuiVar_info_color = WHITE; 
		} else {
				if (GuiVar_button_info != 2) {
					GuiVar_info_color = DARK_BLUE; 
				}
		}
			
		GuiVar_alarms_number 			 = alarms_number;
		GuiVar_faults_number 			 = faults_number;
		GuiVar_disablements_number = disablements_number;
		GuiVar_dev_ctrl_number 		 = dev_ctrl_number;
		
		
// 20201002
// Aggiornamento Colore Tasto EVENTI    
//    
    if (TIM11.msec_timer==0)
    {
      TIM11.msec_timer = 1000;
      if ( (new_fault) || (GuiVar_fault_back_color==0x07DF) )
      { 
        if (GuiVar_fault_back_color==0xFFFF)
          GuiVar_fault_back_color = 0x07DF;   //2018.03.05 0x07DF - giallo
        else
          GuiVar_fault_back_color = 0xFFFF;
      }
    }     
    
    
		// User symbol
		GuiVar_user    = current_user.level;
		GuiVar_user_id = current_user.id;
		
		switch (GuiVar_screen_select)
		{
			
			/* SYSTEM SCREEN */
			//******************************************************************************************************************************************
			case SYSTEM_SCREEN:
				// Id delle zone
				GuiVar_default_line_1 = index_system;
				GuiVar_default_line_2 = (index_system + 1);
				GuiVar_default_line_3 = (index_system + 2);
			
				// valori di ossigeno delle zone
				if (zones_array[index_system].no_value == 1 ) {
					GuiVar_default_value_1 = 10000;
				} else {
					GuiVar_default_value_1 = zones_array[index_system].o2_mean;
				}

				if (zones_array[index_system + 1].no_value == 1 ) {
					GuiVar_default_value_2 = 10000;
				} else {
					GuiVar_default_value_2 = zones_array[(index_system + 1)].o2_mean;
				}

				if (zones_array[index_system + 2].no_value == 1 ) {
					GuiVar_default_value_3 = 10000;
				} else {
					GuiVar_default_value_3 = zones_array[index_system + 2].o2_mean;
				}
				
				GuiVar_system_line_1_color = WHITE;
				
				// Valore di ossigeno "giallo" se ci sono guasti nella zona
				if ((zones_array[index_system].diff_1 == 1) || (zones_array[index_system].diff_2 == 1) ) {
					GuiVar_system_line_1_color = LIGHT_YELLOW;
				}
				// Valore di ossigeno "viola" se zona disabilitata
				if (zones_array[index_system ].disabled == 1 ) {
					GuiVar_system_line_1_color = LIGHT_VIOLET;
				}
				// Valore di ossigeno "verde" se zona attivata
				if (zones_array[index_system ].activated == 1 ) {
					GuiVar_system_line_1_color = LIGHT_GREEN2;
				}
				
				
				GuiVar_system_line_2_color = WHITE;
				
				// Valore di ossigeno "giallo" se ci sono guasti nella zona
				if ((zones_array[index_system + 1].diff_1 == 1) || (zones_array[index_system + 1].diff_2 == 1) ) {
					GuiVar_system_line_2_color = LIGHT_YELLOW;
				}				
				// Valore di ossigeno "viola" se zona disabilitata
				if (zones_array[index_system + 1].disabled == 1 ) {
					GuiVar_system_line_2_color = LIGHT_VIOLET;
				}
				// Valore di ossigeno "verde" se zona attivata
				if (zones_array[index_system + 1].activated == 1 ) {
					GuiVar_system_line_2_color = LIGHT_GREEN2;
				}
				
				
				GuiVar_system_line_3_color = WHITE;
				
				if ((zones_array[index_system + 2].diff_1 == 1) || (zones_array[index_system + 2].diff_2 == 1) ) {
					GuiVar_system_line_3_color = LIGHT_YELLOW;
				}
				// Valore di ossigeno "viola" se zona disabilitata
				if (zones_array[index_system + 2].disabled == 1 ) {
					GuiVar_system_line_3_color = LIGHT_VIOLET;
				}
				// Valore di ossigeno "verde" se zona attivata
				if (zones_array[index_system + 2].activated == 1 ) {
					GuiVar_system_line_3_color = LIGHT_GREEN2;
				}
				
				
				// bottoni allarmi delle zone e colore dei valori (rosso se allarme)
				if (zones_array[index_system].in_alarm == 1) {
						GuiVar_default_alarm_1 = 0;
				} else {
						GuiVar_default_alarm_1 = 1;						
				}

				if (zones_array[(index_system + 1)].in_alarm == 1) {
						GuiVar_default_alarm_2 = 0;
				} else {
						GuiVar_default_alarm_2 = 1;
				}

				if (zones_array[(index_system + 2)].in_alarm == 1) {
						GuiVar_default_alarm_3 = 0;
				} else {
						GuiVar_default_alarm_3 = 1;
				}

				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_system < 1) {
					index_system = 1;
				}	
				
				if (index_system > 1) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				if (sys_zones_num > (index_system + 2)) {
					GuiVar_down_button = 0;
				} else {
					GuiVar_down_button = 2;
					index_system = sys_zones_num - 2;
				}				
			break;
			
			/* SET */
			//******************************************************************************************************************************************
			case SET_SCREEN:
				// Carica valori di set e isteresi di quella zona
				GuiVar_set_old  = zones_array[GuiVar_set_zone_id].set;
				GuiVar_hyst_old = zones_array[GuiVar_set_zone_id].hyst;	
				
				// Ricordati se questa zona era gi� selezionata o no
				if (zones_change_set[GuiVar_set_zone_id] == 1)  {
						GuiVar_set_checkbox_zone = 1;
				} else {
						GuiVar_set_checkbox_zone = 0;
				}			
				
				// Non posso pi� decrementare zona se sono arrivato all'estremit�
				if (GuiVar_set_zone_id == 1) {
						GuiVar_set_zone_dec_button = 2;
				}	

				// Non posso pi� incrementare zona se sono arrivato all'estremit�
				if (GuiVar_set_zone_id == sys_zones_num) {
						GuiVar_set_zone_inc_button = 2;
				}				
			break;

			/* THRESHOLDS */
			//******************************************************************************************************************************************
			case TH_SCREEN:
				GuiVar_th_sensor_id_1 = (index_sensor);
				GuiVar_th_sensor_id_2 = (index_sensor + 1);
				
				// Carica i valori salvati della prima zona
				GuiVar_saved_th_high_1  = sensors_array[GuiVar_th_sensor_id_1].th_high;
				GuiVar_saved_th_high_2  = sensors_array[GuiVar_th_sensor_id_2].th_high;						
				GuiVar_saved_th_low_1   = sensors_array[GuiVar_th_sensor_id_1].th_low;
				GuiVar_saved_th_low_2   = sensors_array[GuiVar_th_sensor_id_2].th_low;	
			
				// Checkbox 1 checked ?
				if (sensors_change_th[index_sensor] == 1) {
					GuiVar_th_checkbox_sensor_1 = 1;
				} else {
					GuiVar_th_checkbox_sensor_1 = 0;
				}
				// Checkbox 2 checked ?
				if (sensors_change_th[index_sensor + 1] == 1) {
					GuiVar_th_checkbox_sensor_2 = 1;
				} else {
					GuiVar_th_checkbox_sensor_2 = 0;
				}		

				// Non posso decrementare ancora se sei arrivato all'estremit�
				if (GuiVar_th_zone_id == 1) {
						GuiVar_th_zone_dec_button = 2;
				}		
				// Non posso incrementare ancora se ho raggiunto l'estremit�
				if (GuiVar_th_zone_id == sys_zones_num) {
						GuiVar_th_zone_inc_button = 2;
				}

				// abilitazione bottoni per scorrimento orizzontale, inc e dec sensori
				if (index_sensor < zones_array[GuiVar_th_zone_id].first_sensor) {
					index_sensor = zones_array[GuiVar_th_zone_id].first_sensor;
				}	
				
				if ( (index_sensor > zones_array[GuiVar_th_zone_id].first_sensor) ) {
					if(GuiVar_th_dec_sens != 1) {
						GuiVar_th_dec_sens = 0;
					}
				} else {
					GuiVar_th_dec_sens = 2;
				}
				
				if ( (index_sensor + 1) < (zones_array[GuiVar_th_zone_id].first_sensor + zones_array[GuiVar_th_zone_id].n_sensors - 1) ) {
					if(GuiVar_th_inc_sens != 1) {
						GuiVar_th_inc_sens = 0;
					}
				} else {
					GuiVar_th_inc_sens = 2;
					index_sensor = (zones_array[GuiVar_th_zone_id].first_sensor + zones_array[GuiVar_th_zone_id].n_sensors - 2);
				}					
			break;
				
			/* AUTHENTICATION */
			//******************************************************************************************************************************************
			case LOG_SCREEN:
				// vedi anche "handle_users" per la gestione dei bottoni
// 20201002      
        handle_pwd_blink();       // Aggiornamento lampeggio Password  
			break;

			/* OPTIONS */
			//******************************************************************************************************************************************
			case OPTIONS_SCREEN:
				// per il momento niente lingua alternativa, sorry
//				GuiVar_dec_language_button = 2;
//				GuiVar_inc_language_button = 2;
			break;

			/* EVENTS */
			//******************************************************************************************************************************************
			case EVENTS_SCREEN:
				// abilitazione bottone per cancellare lista allarmi
				if ( (current_user.level >= 2 ) && (events_array_dim != 0) ) {
					if (GuiVar_del_events_button != 1) {
						GuiVar_del_events_button = 0;
		        GuiVar_button_delete_back_color = 0xFFFF; //2018.02.23
					}
				} 
				else {
//					GuiVar_del_events_button = 2;
		      GuiVar_button_delete_back_color = 0xBEDB;//0xBEDC; //2018.02.23
				}
				
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_events < 0) {
					index_events = 0;
				}	
				
				if (index_events > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
		        GuiVar_button_up_back_color = 0xFFFF; //2018.02.23
					}
				} else {
//					GuiVar_up_button = 2;
		      GuiVar_button_up_back_color = 0xBEDB;//0xBEDC; //2018.02.23
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (events_array_dim > 5) && (index_events >= (events_array_dim - 5) ) ) {
					index_events = (events_array_dim - 5);
//				  GuiVar_down_button = 2;
		      GuiVar_button_down_back_color = 0xBEDB;//0xBEDC; //2018.02.23
					
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
 		        GuiVar_button_down_back_color = 0xFFFF; //2018.02.23
					}
				}
				
				if (events_array_dim < 6) {
//				  GuiVar_down_button = 2;
		      GuiVar_button_down_back_color = 0xBEDB;//0xBEDC; //2018.02.23
					index_events = 0;
				}
				
				// Show indexes
				GuiVar_line_index_1 = (index_events + 1);	
				GuiVar_line_index_2 = (index_events + 2);	
				GuiVar_line_index_3 = (index_events + 3);	
				GuiVar_line_index_4 = (index_events + 4);	
				GuiVar_line_index_5 = (index_events + 5);	
			
				// Show events on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( events_array_dim >= (index_events + i + 1) ){
					  handle_event(events_array[index_events + i], (i + 1), SHOW_ON_DISPLAY);
					}	else 	{
							clear_line(i + 1);
							switch (i + 1)
							{
								case 1:
									GuiVar_line_1_bkg_color = WHITE;
								break;
								case 2:
									GuiVar_line_2_bkg_color = WHITE;
								break;
								case 3:
									GuiVar_line_3_bkg_color = WHITE;
								break;
								case 4:
									GuiVar_line_4_bkg_color = WHITE;
								break;
								case 5:
									GuiVar_line_5_bkg_color = WHITE;
								break;
							}
					}
				}
			break;
			
			/* ALARMS */
			//******************************************************************************************************************************************
			case CHARTS_SCREEN:
			
				// se non ci sono allarmi
				if (alarms_number == 0) {
					clear_line(1);
					clear_line(2);					
					GuiVar_events_line_1_string_1[0] = GuiVar_events_line_2_string_1[0] = 'N';
					GuiVar_events_line_1_string_1[1] = GuiVar_events_line_2_string_1[1] = 'o';
					GuiVar_events_line_1_string_1[2] = GuiVar_events_line_2_string_1[2] = ' ';
					GuiVar_events_line_1_string_1[3] = GuiVar_events_line_2_string_1[3] = 'a';
					GuiVar_events_line_1_string_1[4] = GuiVar_events_line_2_string_1[4] = 'l';
					GuiVar_events_line_1_string_1[5] = GuiVar_events_line_2_string_1[5] = 'a';
					GuiVar_events_line_1_string_1[6] = GuiVar_events_line_2_string_1[6] = 'r';
					GuiVar_events_line_1_string_1[7] = GuiVar_events_line_2_string_1[7] = 'm';
					GuiVar_events_line_1_string_1[8] = GuiVar_events_line_2_string_1[8] = 's';
				} 
				
				// bottone lista allarmi
				if (alarms_array_dim == 0) {
					GuiVar_list_button = 2;
				} else {
					if (GuiVar_list_button != 1) {
						GuiVar_list_button = 0;
					}
				}
			break;
			
			/* FAULTS */
			//******************************************************************************************************************************************
			case FAULTS_SCREEN:
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_faults < 0) {
					index_faults = 0;
				}	
				
				if (index_faults > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (faults_array_dim > 5) && (index_faults >= (faults_array_dim - 5) ) ) {
					index_faults = (faults_array_dim - 5);
				  GuiVar_down_button = 2;
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				}		
				
				if (faults_array_dim < 6) {
				  GuiVar_down_button = 2;
					index_faults = 0;
				} 
				
				// Show indexes
				GuiVar_line_index_1 = (index_faults + 1);	
				GuiVar_line_index_2 = (index_faults + 2);	
				GuiVar_line_index_3 = (index_faults + 3);	
				GuiVar_line_index_4 = (index_faults + 4);	
				GuiVar_line_index_5 = (index_faults + 5);	
			
				// Show faults on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( faults_array_dim >= (index_faults + i + 1) ){
							show_fault(faults_array[index_faults + i], (i + 1));
						
							switch (i + 1)
							{
								case LINE_1:
									GuiVar_line_1_bkg_color = LIGHT_YELLOW;
								break;
								
								case LINE_2:
									GuiVar_line_2_bkg_color = LIGHT_YELLOW;
								break;
								
								case LINE_3:
									GuiVar_line_3_bkg_color = LIGHT_YELLOW;
								break;
								
								case LINE_4:
									GuiVar_line_4_bkg_color = LIGHT_YELLOW;
								break;
								
								case LINE_5:
									GuiVar_line_5_bkg_color = LIGHT_YELLOW;
								break;								
							}
					}	else 	{
							clear_line(i + 1);
					}
				}				
			break;

			/* DISABLEMENTS */
			//******************************************************************************************************************************************
			case DISABLEMENTS_SCREEN:
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_disablements < 0) {
					index_disablements = 0;
				}	
				
				if (index_disablements > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (disablements_array_dim > 5) && (index_disablements >= (disablements_array_dim - 5) ) ) {
					index_disablements = (disablements_array_dim - 5);
				  GuiVar_down_button = 2;
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				}		
				
				if (disablements_array_dim < 6) {
				  GuiVar_down_button = 2;
					index_disablements = 0;
				} 
				
				// Show indexes
				GuiVar_line_index_1 = (index_disablements + 1);	
				GuiVar_line_index_2 = (index_disablements + 2);	
				GuiVar_line_index_3 = (index_disablements + 3);	
				GuiVar_line_index_4 = (index_disablements + 4);	
				GuiVar_line_index_5 = (index_disablements + 5);	
			
				// Show faults on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( disablements_array_dim >= (index_disablements + i + 1) ){
							show_object(disablements_array[index_disablements + i], (i + 1));
						
							switch (i + 1)
							{
								case 1:
									if (disablements_array[index_disablements + i].type == O_TYPE_ZONE ) {
										GuiVar_line_1_bkg_color = DARK_VIOLET; 
									}	else {
										GuiVar_line_1_bkg_color = LIGHT_VIOLET; 
									}											
								break;
								
								case 2:
									if (disablements_array[index_disablements + i].type == O_TYPE_ZONE ) {
										GuiVar_line_2_bkg_color = DARK_VIOLET; 
									}	else {
										GuiVar_line_2_bkg_color = LIGHT_VIOLET; 
									}																
								break;

								case 3:
									if (disablements_array[index_disablements + i].type == O_TYPE_ZONE ) {
										GuiVar_line_3_bkg_color = DARK_VIOLET; 
									}	else {
										GuiVar_line_3_bkg_color = LIGHT_VIOLET; 
									}																
								break;
								
								case 4:
									if (disablements_array[index_disablements + i].type == O_TYPE_ZONE ) {
										GuiVar_line_4_bkg_color = DARK_VIOLET; 
									}	else {
										GuiVar_line_4_bkg_color = LIGHT_VIOLET; 
									}																
								break;		

								case 5:
									if (disablements_array[index_disablements + i].type == O_TYPE_ZONE ) {
										GuiVar_line_5_bkg_color = DARK_VIOLET; 
									}	else {
										GuiVar_line_5_bkg_color = LIGHT_VIOLET; 
									}																
								break;								
							}						
							
					}	else 	{
							clear_line(i + 1);
					}
				}				
			break;

			/* DEVICES CONTROL */
			//******************************************************************************************************************************************
			case DEV_CTRL_SCREEN:
				
				// colore del bottone "outputs off"
				if (output_off_state == 0) {
					GuiVar_outputs_off_color = DARK_BLUE;
				} else {
					GuiVar_outputs_off_color = LIGHT_BLUE;
				}
				
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_dev_ctrl < 0) {
					index_dev_ctrl = 0;
				}	

				if (index_dev_ctrl > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (dev_ctrl_array_dim > 5) && (index_dev_ctrl >= (dev_ctrl_array_dim - 5) ) ) {
					index_dev_ctrl = (dev_ctrl_array_dim - 5);
				  GuiVar_down_button = 2;
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				}		
				
				if (dev_ctrl_array_dim < 6) {
				  GuiVar_down_button = 2;
					index_dev_ctrl = 0;
				} 
				
				// Show indexes
				GuiVar_line_index_1 = (index_dev_ctrl + 1);	
				GuiVar_line_index_2 = (index_dev_ctrl + 2);	
				GuiVar_line_index_3 = (index_dev_ctrl + 3);	
				GuiVar_line_index_4 = (index_dev_ctrl + 4);	
				GuiVar_line_index_5 = (index_dev_ctrl + 5);	
			
				// Show objects on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( dev_ctrl_array_dim >= (index_dev_ctrl + i + 1) ){
							show_object(dev_ctrl_array[index_dev_ctrl + i], (i + 1));
						
							switch (i + 1)
							{
								case 1:
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_ZONE ) {
										GuiVar_line_1_bkg_color = DARK_GREEN2; 
									}	else {
										GuiVar_line_1_bkg_color = LIGHT_GREEN2; 
									}
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_FLUX ) {
										GuiVar_flux_line_1 = 1;
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].on == 1){
											fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested = 1;
										}
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested == 1){
											GuiVar_radio_flux_1 = 0;
										} else {
											GuiVar_radio_flux_1 = 1;
										}	
									} else {
										GuiVar_flux_line_1 = 0;
									}										
								break;
								
								case 2:
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_ZONE ) {
										GuiVar_line_2_bkg_color = DARK_GREEN2; 
									}	else {
										GuiVar_line_2_bkg_color = LIGHT_GREEN2; 
									}
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_FLUX ) {
										GuiVar_flux_line_2 = 1;
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].on == 1){
											fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested = 1;
										}										
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested == 1){
											GuiVar_radio_flux_2 = 0;
										}	else {
											GuiVar_radio_flux_2 = 1;
										}	
									} else {
										GuiVar_flux_line_2 = 0;
									}														
								break;
								
								case 3:
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_ZONE ) {
										GuiVar_line_3_bkg_color = DARK_GREEN2; 
									}	else {
										GuiVar_line_3_bkg_color = LIGHT_GREEN2; 
									}
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_FLUX ) {
										GuiVar_flux_line_3 = 1;
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].on == 1){
											fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested = 1;
										}										
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested == 1){
											GuiVar_radio_flux_3 = 0;
										} else {
											GuiVar_radio_flux_3 = 1;
										}		
									} else {
										GuiVar_flux_line_3 = 0;
									}													
								break;

								case 4:
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_ZONE ) {
										GuiVar_line_4_bkg_color = DARK_GREEN2; 
									}	else {
										GuiVar_line_4_bkg_color = LIGHT_GREEN2; 
									}
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_FLUX ) {
										GuiVar_flux_line_4 = 1;
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].on == 1){
											fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested = 1;
										}										
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested == 1){
											GuiVar_radio_flux_4 = 0;
										}	else {
											GuiVar_radio_flux_4 = 1;
										}	
									} else {
										GuiVar_flux_line_4 = 0;
									}												
								break;

								case 5:
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_ZONE ) {
										GuiVar_line_5_bkg_color = DARK_GREEN2; 
									}	else {
										GuiVar_line_5_bkg_color = LIGHT_GREEN2; 
									}
									if (dev_ctrl_array[index_dev_ctrl + i].type == O_TYPE_FLUX ) {
										GuiVar_flux_line_5 = 1;
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].on == 1){
											fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested = 1;
										}										
										if (fluxes_array[dev_ctrl_array[index_dev_ctrl + i].id ].tested == 1){
											GuiVar_radio_flux_5 = 0;
										}	else {
											GuiVar_radio_flux_5 = 1;
										}	
									} else {
										GuiVar_flux_line_5 = 0;
									}														
								break;								
							}		
							
					}	else 	{
							clear_line(i + 1);
					}
				}

				// Bottone per la disabilitazione delle uscite (per test)
				if (GuiVar_output_off != 2) {
					if (output_off_state == 1){
						GuiVar_output_off = 1;
					}
				}
			break;
				
			/* ALARMS (LIST) */
			//******************************************************************************************************************************************
			case ALARMS_LIST_SCREEN:
// 				// abilitazione bottone per cancellare lista allarmi
// 				if (( alarms_array_dim == 0 ) || ( alarms_number != 0 ) ) {
// 					GuiVar_del_events_button = 2;	
// 				}
			
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_alarms < 0) {
					index_alarms = 0;
				}	
				
				if (index_alarms > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (alarms_array_dim > 5) && (index_alarms >= (alarms_array_dim - 5) ) ) {
					index_alarms = (alarms_array_dim - 5);
				  GuiVar_down_button = 2;
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				}		
				
				if (alarms_array_dim < 6) {
				  GuiVar_down_button = 2;
					index_alarms = 0;
				} 
		
				// Show indexes
				GuiVar_line_index_1 = (index_alarms + 1);	
				GuiVar_line_index_2 = (index_alarms + 2);	
				GuiVar_line_index_3 = (index_alarms + 3);	
				GuiVar_line_index_4 = (index_alarms + 4);	
				GuiVar_line_index_5 = (index_alarms + 5);	
			
				// Show events on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( alarms_array_dim >= (index_alarms + i + 1) ){
					    handle_event(alarms_array[index_alarms + i], (i + 1), SHOW_ON_DISPLAY);
							switch (i + 1)
							{
								case 1:
									if ( (index_alarms + i) < alarms_number ){
										GuiVar_line_1_bkg_color = LIGHT_RED; 		
									} else {
										GuiVar_line_1_bkg_color = WHITE; 
									}	
								break;
								
								case 2:
									if ( (index_alarms + i) < alarms_number ){
										GuiVar_line_2_bkg_color = LIGHT_RED;
									} else {
										GuiVar_line_2_bkg_color = WHITE; 
									}	
								break;

								case 3:
									if ( (index_alarms + i) < alarms_number ){
										GuiVar_line_3_bkg_color = LIGHT_RED;	
									}	else {
										GuiVar_line_3_bkg_color = WHITE; 
									}	
								break;
								
								case 4:
									if ( (index_alarms + i) < alarms_number ){
										GuiVar_line_4_bkg_color = LIGHT_RED;
									} else {
										GuiVar_line_4_bkg_color = WHITE; 
									}	
								break;		

								case 5:
									if ( (index_alarms + i) < alarms_number ){
										GuiVar_line_5_bkg_color = LIGHT_RED;
									} else {
										GuiVar_line_5_bkg_color = WHITE; 
									}	
								break;								
							}						
					}	else 	{
							clear_line(i + 1);
					}
				}			
			break;
				
			/* ZONE X */
			//******************************************************************************************************************************************
			case ZONE_X_SCREEN:
				// valori di ossigeno dei sensori
				if (sensors_array[index_zone_x].comm_fault == 0 ){
					GuiVar_sensor_value_1 = sensors_array[index_zone_x].o2_value;
				} else {
					GuiVar_sensor_value_1 = 10000;
				}
				
				if (sensors_array[index_zone_x + 1].comm_fault == 0 ){
					GuiVar_sensor_value_2 = sensors_array[index_zone_x + 1].o2_value;
				} else {
					GuiVar_sensor_value_2 = 10000;
				}

				GuiVar_sensor_id_1 = index_zone_x;
				GuiVar_sensor_id_2 = index_zone_x + 1;	
			
				GuiVar_zone_x_line_1_color = WHITE;
			
				// valore "giallo" se guasto relativo al valore di ossigeno
				if (
//     				 (sensors_array[index_zone_x].diff_1_fault == 1 ) ||
// 						 (sensors_array[index_zone_x].diff_2_fault == 1 ) ||
					sensors_array[index_zone_x].elab_fault == 1 ) {
					GuiVar_zone_x_line_1_color = LIGHT_YELLOW;
				}
				// Valore di ossigeno "viola" se zona disabilitata
				if (sensors_array[index_zone_x].disabled == 1 ) {
					GuiVar_zone_x_line_1_color = LIGHT_VIOLET;
				}
				// Valore di ossigeno "verde" se zona attivata
				if (sensors_array[index_zone_x].activated == 1 ) {
					GuiVar_zone_x_line_1_color = LIGHT_GREEN2;
				}
				
				GuiVar_zone_x_line_2_color = WHITE;
				
				if ( 
// 					   (sensors_array[index_zone_x + 1].diff_1_fault == 1 ) ||
// 						 (sensors_array[index_zone_x + 1].diff_2_fault == 1 ) ||
						sensors_array[index_zone_x + 1].elab_fault == 1  ) {
						GuiVar_zone_x_line_2_color = LIGHT_YELLOW;
				}
				// Valore di ossigeno "viola" se zona disabilitata
				if (sensors_array[index_zone_x + 1].disabled == 1 ) {
					GuiVar_zone_x_line_2_color = LIGHT_VIOLET;
				}
				// Valore di ossigeno "verde" se zona attivata
				if (sensors_array[index_zone_x + 1].activated == 1 ) {
					GuiVar_zone_x_line_2_color = LIGHT_GREEN2;
				}
				
				// bottoni allarmi dei sensori
				if (sensors_array[index_zone_x].in_alarm == 1) {
						GuiVar_sensor_alarm_1 = 0;						
				} else {
						GuiVar_sensor_alarm_1 = 1;
				}			
				if (sensors_array[(index_zone_x + 1)].in_alarm == 1) {
						GuiVar_sensor_alarm_2 = 0;
				} else {
						GuiVar_sensor_alarm_2 = 1;
				}		

				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_zone_x < 0) {
					index_zone_x = 0;
				}	
				
				if ( (index_zone_x > zones_array[GuiVar_zone_id].first_sensor) ) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				if ( (index_zone_x + 1) < (zones_array[GuiVar_zone_id].first_sensor + zones_array[GuiVar_zone_id].n_sensors - 1) ) {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				} else {
					GuiVar_down_button = 2;
					index_zone_x = (zones_array[GuiVar_zone_id].first_sensor + zones_array[GuiVar_zone_id].n_sensors - 2);
				}					
			break;

			/* DEVICES CONTROL (LIST) */
			//******************************************************************************************************************************************
			case DEV_CTRL_LIST_SCREEN:
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_objects < 0) {
					index_objects = 0;
				}
				
				if (index_objects > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (objects_array_dim > 5) && (index_objects >= (objects_array_dim - 5) ) ) {
					index_objects = (objects_array_dim - 5);
				  GuiVar_down_button = 2;
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				}
				
				if (objects_array_dim < 6) {
				  GuiVar_down_button = 2;
					index_objects = 0;
				}
				
				// Show indexes
				GuiVar_line_index_1 = (index_objects + 1);	
				GuiVar_line_index_2 = (index_objects + 2);	
				GuiVar_line_index_3 = (index_objects + 3);	
				GuiVar_line_index_4 = (index_objects + 4);	
				GuiVar_line_index_5 = (index_objects + 5);	
			
				// Show objects on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( objects_array_dim >= (index_objects + i + 1) ){
							show_object(objects_array[index_objects + i], (i + 1));
						
							// Update checkboxes according to disablements !
							switch (i+ 1)
							{
								case 1:
									GuiVar_line_1_ck = dev_ctrl_change[index_objects];
									if (dev_ctrl_change[index_objects] == 1) {
										GuiVar_line_1_bkg_color = LIGHT_GREEN2; 										
										if (objects_array[index_objects].type == O_TYPE_ZONE ) {
											GuiVar_line_1_bkg_color = DARK_GREEN2; 
										}										
									}	else {
										GuiVar_line_1_bkg_color = WHITE; 
									}
									
									
									// se � un oggetto di una zona in allarme...
									if ( ( (objects_array[index_objects].zone != 0)  && (zones_array[objects_array[index_objects].zone].in_alarm == 1 ) )  ||
									// ...o una zona in allarme, linea di colore rosso !	
									( (objects_array[index_objects].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects].id].in_alarm == 1 ) )
									){
										GuiVar_line_1_bkg_color = DARK_RED;
									}

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects].type != O_TYPE_ZONE )) {
										GuiVar_line_1_bkg_color = DARK_RED;
									}
								
								break;
									
								case 2:
									GuiVar_line_2_ck = dev_ctrl_change[index_objects + 1];
									if (dev_ctrl_change[index_objects + 1] == 1) {
										GuiVar_line_2_bkg_color = LIGHT_GREEN2; 
										if (objects_array[index_objects + 1].type == O_TYPE_ZONE ) {
											GuiVar_line_2_bkg_color = DARK_GREEN2; 
										}										
									}	else {
										GuiVar_line_2_bkg_color = WHITE; 
									}
									
									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 1].zone != 0)  && (zones_array[objects_array[index_objects + 1].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 1].id].in_alarm == 1 ) )
									){
										GuiVar_line_2_bkg_color = DARK_RED;
									}	

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 1].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 1].type != O_TYPE_ZONE )) {
										GuiVar_line_2_bkg_color = DARK_RED;
									}									
								break;
								
								case 3:
									GuiVar_line_3_ck = dev_ctrl_change[index_objects + 2];
									if (dev_ctrl_change[index_objects + 2] == 1) {
										GuiVar_line_3_bkg_color = LIGHT_GREEN2; 
										if (objects_array[index_objects + 2].type == O_TYPE_ZONE ) {
											GuiVar_line_3_bkg_color = DARK_GREEN2; 
										}										
									}	else {
										GuiVar_line_3_bkg_color = WHITE; 
									}

									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 2].zone != 0)  && (zones_array[objects_array[index_objects + 2].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 2].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 2].id].in_alarm == 1 ) )
									){
										GuiVar_line_3_bkg_color = DARK_RED;
									}

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 2].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 2].type != O_TYPE_ZONE )) {
										GuiVar_line_3_bkg_color = DARK_RED;
									}									
								break;
								
								case 4:
									GuiVar_line_4_ck = dev_ctrl_change[index_objects + 3];
									if (dev_ctrl_change[index_objects + 3] == 1) {
										GuiVar_line_4_bkg_color = LIGHT_GREEN2; 
										if (objects_array[index_objects + 3].type == O_TYPE_ZONE ) {
											GuiVar_line_4_bkg_color = DARK_GREEN2; 
										}										
									}	else {
										GuiVar_line_4_bkg_color = WHITE; 
									}

									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 3].zone != 0)  && (zones_array[objects_array[index_objects + 3].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 3].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 3].id].in_alarm == 1 ) )
									){
										GuiVar_line_4_bkg_color = DARK_RED;
									}	

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 3].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 3].type != O_TYPE_ZONE )) {
										GuiVar_line_4_bkg_color = DARK_RED;
									}									
								break;	

								case 5:
									GuiVar_line_5_ck = dev_ctrl_change[index_objects + 4];
									if (dev_ctrl_change[index_objects + 4] == 1) {
										GuiVar_line_5_bkg_color = LIGHT_GREEN2;
										if (objects_array[index_objects + 4].type == O_TYPE_ZONE ) {
											GuiVar_line_5_bkg_color = DARK_GREEN2; 
										}										
									}	else {
										GuiVar_line_5_bkg_color = WHITE; 
									}

									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 4].zone != 0)  && (zones_array[objects_array[index_objects + 4].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 4].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 4].id].in_alarm == 1 ) )
									){
										GuiVar_line_5_bkg_color = DARK_RED;
									}

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 4].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 4].type != O_TYPE_ZONE )) {
										GuiVar_line_5_bkg_color = DARK_RED;
									}									
								break;									
							}							
					}	else 	{
							clear_line(i + 1);
					}
				}				
			break;

			/* DISABLEMENTS (LIST) */
			//******************************************************************************************************************************************
			case DISABLEMENTS_LIST_SCREEN:			
				// abilitazione bottoni per scorrimento vericale, su e gi�
				if (index_objects < 0) {
					index_objects = 0;
				}	
				
				if (index_objects > 0) {
					if(GuiVar_up_button != 1) {
						GuiVar_up_button = 0;
					}
				} else {
					GuiVar_up_button = 2;
				}
				
				// se l'array � gi� pieno, punta all'ultimo possibile
				if ( (objects_array_dim > 5) && (index_objects >= (objects_array_dim - 5) ) ) {
					index_objects = (objects_array_dim - 5);
				  GuiVar_down_button = 2;
				} else {
					if(GuiVar_down_button != 1) {
						GuiVar_down_button = 0;
					}
				}		
				
				if (objects_array_dim < 6) {
				  GuiVar_down_button = 2;
					index_objects = 0;
				} 
				
				// Show indexes
				GuiVar_line_index_1 = (index_objects + 1);	
				GuiVar_line_index_2 = (index_objects + 2);	
				GuiVar_line_index_3 = (index_objects + 3);	
				GuiVar_line_index_4 = (index_objects + 4);	
				GuiVar_line_index_5 = (index_objects + 5);	
			
				// Show objects on the list, according to index of the list !!
				for (i=0; i<5; i++) {
					if ( objects_array_dim >= (index_objects + i + 1) ){
							show_object(objects_array[index_objects + i], (i + 1));
						
							// Update checkboxes according to devices control !
							switch (i+ 1)
							{
								case 1:
									GuiVar_line_1_ck = disablements_change[index_objects];
									if (disablements_change[index_objects] == 1) {
										GuiVar_line_1_bkg_color = LIGHT_VIOLET; 
										if (objects_array[index_objects].type == O_TYPE_ZONE ) {
											GuiVar_line_1_bkg_color = DARK_VIOLET; 
										}
										
									}	else {
										GuiVar_line_1_bkg_color = WHITE; 
									}
									
									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects].zone != 0)  && (zones_array[objects_array[index_objects].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects].id].in_alarm == 1 ) )
									){
										GuiVar_line_1_bkg_color = DARK_RED;
									}		

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects].type != O_TYPE_ZONE )) {
										GuiVar_line_1_bkg_color = DARK_RED;
									}									
								break;
									
								case 2:
									GuiVar_line_2_ck = disablements_change[index_objects + 1];
									if (disablements_change[index_objects + 1] == 1) {
										GuiVar_line_2_bkg_color = LIGHT_VIOLET; 
										if (objects_array[index_objects + 1].type == O_TYPE_ZONE ) {
											GuiVar_line_2_bkg_color = DARK_VIOLET; 
										}
										
									}	else {
										GuiVar_line_2_bkg_color = WHITE; 
									}
									
									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 1].zone != 0)  && (zones_array[objects_array[index_objects + 1].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 1].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 1].id].in_alarm == 1 ) )
									){
										GuiVar_line_2_bkg_color = DARK_RED;
									}

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 1].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 1].type != O_TYPE_ZONE )) {
										GuiVar_line_2_bkg_color = DARK_RED;
									}									
								break;
								
								case 3:
									GuiVar_line_3_ck = disablements_change[index_objects + 2];
									if (disablements_change[index_objects + 2] == 1) {
										GuiVar_line_3_bkg_color = LIGHT_VIOLET; 
										if (objects_array[index_objects + 2].type == O_TYPE_ZONE ) {
											GuiVar_line_3_bkg_color = DARK_VIOLET; 
										}
										
									}	else {
										GuiVar_line_3_bkg_color = WHITE; 
									}
									
									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 2].zone != 0)  && (zones_array[objects_array[index_objects + 2].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 2].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 2].id].in_alarm == 1 ) )
									){
										GuiVar_line_3_bkg_color = DARK_RED;
									}

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 2].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 2].type != O_TYPE_ZONE )) {
										GuiVar_line_3_bkg_color = DARK_RED;
									}									
								break;
								
								case 4:
									GuiVar_line_4_ck = disablements_change[index_objects + 3];
									if (disablements_change[index_objects + 3] == 1) {
										GuiVar_line_4_bkg_color = LIGHT_VIOLET; 
										if (objects_array[index_objects + 3].type == O_TYPE_ZONE ) {
											GuiVar_line_4_bkg_color = DARK_VIOLET; 
										}
										
									}	else {
										GuiVar_line_4_bkg_color = WHITE; 
									}
									
									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 3].zone != 0)  && (zones_array[objects_array[index_objects + 3].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 3].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 3].id].in_alarm == 1 ) )
									){
										GuiVar_line_4_bkg_color = DARK_RED;
									}	

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 3].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 3].type != O_TYPE_ZONE )) {
										GuiVar_line_4_bkg_color = DARK_RED;
									}									
								break;	

								case 5:
									GuiVar_line_5_ck = disablements_change[index_objects + 4];
									if (disablements_change[index_objects + 4] == 1) {
										GuiVar_line_5_bkg_color = LIGHT_VIOLET; 
										if (objects_array[index_objects + 4].type == O_TYPE_ZONE ) {
											GuiVar_line_5_bkg_color = DARK_VIOLET; 
										}
										
									}	else {
										GuiVar_line_5_bkg_color = WHITE; 
									}
									
									// se � una zona in allarme ...
									if ( ( (objects_array[index_objects + 4].zone != 0)  && (zones_array[objects_array[index_objects + 4].zone].in_alarm == 1 ) )  ||
									// ...o un oggetto di una zona in allarme, non puoi selezionarlo !	
									( (objects_array[index_objects + 4].type == O_TYPE_ZONE)  && (zones_array[objects_array[index_objects + 4].id].in_alarm == 1 ) )
									){
										GuiVar_line_5_bkg_color = DARK_RED;
									}	

									// se � un oggetto della centrale e la centrale � in allarme, linea di colore rosso !
									if ( (objects_array[index_objects + 4].zone == 0) && (system_in_alarm == 1) && (objects_array[index_objects + 4].type != O_TYPE_ZONE )) {
										GuiVar_line_5_bkg_color = DARK_RED;
									}									
								break;									
							}	
					}	else 	{
							clear_line(i + 1);
					}
				}
			break;
				
		}
}


/***********************************************************************
 * @brief		check_faults			
	 controlla se ci sono guasti
	 ATTENZIONE !!! alcuni guasti sono identificati in altre funzioni
	 questa funzione deve essere eseguita prima di queste altre poich�
	 azzera il conteggio dei guasti all'inizio !!!!
 **********************************************************************/
void check_faults (void) {

	unsigned int i = 0;
	
	//----------------------------------------------------------------------------------------
	// CIE SD CARD FAULT
	
	if (SBM_sd_card.fault == 1)  {
		
		if (SBM_sd_card.detected == 0) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_SBM_SD_FAULT;//guasto scheda SD
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		SBM_sd_card.detected = 1;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (SBM_sd_card.detected == 1) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_SBM_SD_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		SBM_sd_card.detected = 0;
	}

//da fare per tutti i guasti e preparare i testi descrittivi	
	if (Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_CAN_VALVE)  {
		
		if (!(Fault.Detected_2 & MB_ALLARMI_2_CAN_VALVE)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL_CAN_FAULT; //guasto comunicazione blocco valvole
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_2 |= MB_ALLARMI_2_CAN_VALVE;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_2 & MB_ALLARMI_2_CAN_VALVE) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL_CAN_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_2 &= ~MB_ALLARMI_2_CAN_VALVE;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_SLAVE)  {
		
		if (!(Fault.Detected_2 & MB_ALLARMI_2_SLAVE)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL26_SLAVE_FAULT; //guasto comunicazione con modulo slave
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_2 |= MB_ALLARMI_2_SLAVE;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_2 & MB_ALLARMI_2_SLAVE) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL26_SLAVE_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_2 &= ~MB_ALLARMI_2_SLAVE;
	}

	if (Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_B1)  {
		
		if (!(Fault.Detected_2 & MB_ALLARMI_2_B1)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL27_SONDA_B1; //guasto sonda pressione B1
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_2 |= MB_ALLARMI_2_B1;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_2 & MB_ALLARMI_2_B1) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL27_SONDA_B1;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_2 &= ~MB_ALLARMI_2_B1;
	}

	if (Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_B2)  {
		
		if (!(Fault.Detected_2 & MB_ALLARMI_2_B2)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL28_SONDA_B2; //guasto sonda pressione B2
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_2 |= MB_ALLARMI_2_B2;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_2 & MB_ALLARMI_2_B2) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL28_SONDA_B2;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_2 &= ~MB_ALLARMI_2_B2;
	}

	if (Modbus.Array[MBVECTOR_ALLARMI_2] & MB_ALLARMI_2_B3)  {
		
		if (!(Fault.Detected_2 & MB_ALLARMI_2_B3)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL29_SONDA_B3; //guasto sonda pressione B3
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_2 |= MB_ALLARMI_2_B3;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_2 & MB_ALLARMI_2_B3) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL29_SONDA_B3;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_2 &= ~MB_ALLARMI_2_B3;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN1)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN1)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL5_FAULT; //pressione in linea
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN1;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN1) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL5_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN1;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN6)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN6)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL24_FAULT; //guasto scaricatore
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN6;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN6) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL24_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN6;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN9)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN9)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL4_FAULT; //alta temperatura pompa
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN9;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN9) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL4_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN9;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN4)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN4)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL2_FAULT; //mancanza aria compressa
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN4;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN4) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL2_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN4;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN5)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN5)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL3_FAULT; //rel� termico
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN5;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN5) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL3_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN5;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN8)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN8)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL8_FAULT; //guasto inverter
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN8;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN8) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL8_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN8;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN7)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN7)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL1_FAULT; //errore sequenza fasi alimentazion
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN7;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN7) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL1_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN7;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN11)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN11)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL20_FAULT; //O2 impuro
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN11;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN11) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL20_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN11;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN2)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN2)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL21_FAULT; //pressione
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN2;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN2) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL21_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN2;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN3)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN3)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL22_FAULT; //pressione
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN3;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN3) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL22_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN3;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN16)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN16)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL11_FAULT; //temperatura aria
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN16;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN16) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL11_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN16;
	}
	
	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN13)  {
		
		if (!(Fault.Detected_1 & MB_ALLARMI_IN13)) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL12_FAULT; //temperatura cabinet
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.in_out = INPUT;
			add_event(temp_event);
			new_fault = 1;
		}
		Fault.Detected_1 |= MB_ALLARMI_IN13;
	
	}else {
		
		// se prima non c'era segnala nuovo evento
		if (Fault.Detected_1 & MB_ALLARMI_IN13) {
			temp_event.time          = current_time;
			temp_event.type          = E_TYPE_FAULT;
			temp_event.type_2        = F_TYPE_AL12_FAULT;
			temp_event.user_level    = current_user.level;
			temp_event.user_id       = current_user.id;
			temp_event.id_1 = i;
			temp_event.in_out = OUTPUT;
			add_event(temp_event);
		}
		Fault.Detected_1 &= ~MB_ALLARMI_IN13;
	}
	
//	if (Modbus.Array[MBVECTOR_ALLARMI] & MB_ALLARMI_IN12)  {
//		
//		if (!(Fault.Detected_1 & MB_ALLARMI_IN12)) {
//			temp_event.time          = current_time;
//			temp_event.type          = E_TYPE_FAULT;
//			temp_event.type_2        = F_TYPE_AL11_FAULT; //temperatura cabinet
//			temp_event.user_level    = current_user.level;
//			temp_event.user_id       = current_user.id;
//			temp_event.in_out = INPUT;
//			add_event(temp_event);
//			new_fault = 1;
//		}
//		Fault.Detected_1 |= MB_ALLARMI_IN12;
//	
//	}else {
//		
//		// se prima non c'era segnala nuovo evento
//		if (Fault.Detected_1 & MB_ALLARMI_IN12) {
//			temp_event.time          = current_time;
//			temp_event.type          = E_TYPE_FAULT;
//			temp_event.type_2        = F_TYPE_AL11_FAULT;
//			temp_event.user_level    = current_user.level;
//			temp_event.user_id       = current_user.id;
//			temp_event.id_1 = i;
//			temp_event.in_out = OUTPUT;
//			add_event(temp_event);
//		}
//		Fault.Detected_1 &= ~MB_ALLARMI_IN12;
//	}
	
} // fine check faults


 /***********************************************************************
 * @brief		wartchdog_handle			
	 gestisce il watchdog (5 secondi)
 **********************************************************************/
void watchdog_handle(void){
		if (WWDT_GetStatus(WWDT_TIMEOUT_FLAG)){ // Clear WDT TimeOut
				WWDT_ClrTimeOutFlag();
		} 	
		WWDT_Feed();
}


//inizializza timeout errori di comunicazione con le periferiche collegate
void init_comm_err (void){
	unsigned int i;
	
}




// legge MIS-7300 con I2C_2
void read_PT(void){
	
	uint8_t address=0x76;
	char C10_i;
	uint8_t buff_r[3], buff_w[2], buff_r2[2];
	uint16_t D1=0,D2=0,C1=0,C2=0,C3=0,C4=0,C5=0,C6=0,C7=0,C8=0,C9=0,C10=0;
//	float D1=0,D2=0,C1=0,C2=0,C3=0,C4=0,C5=0,C6=0,C7=0,C8=0,C9=0,C10=0;
	float off_, sens, dT_;//,pausa;
	//buff_r[0] = buff_r[1] = buff_r[2] = 0;
	//buff_r2[0] = buff_r2[1] = 0;
	 
	if(C1_C10==0){
		C1_C10++;
		for(C10_i=0;C10_i<17;C10_i++){
		  CCC[C10_i]=0;
		}
		// read OTP Memory C1-C10
		buff_w[0]=0x0E;		buff_w[1]=0x20;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[0]=buff_r[1];	CCC[0]=(CCC[0]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x28;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[1]=buff_r[1];	CCC[1]=(CCC[1]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x30;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[2]=buff_r[1];	CCC[2]=(CCC[2]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x38;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[3]=buff_r[1];	CCC[3]=(CCC[3]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x40;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[4]=buff_r[1];	CCC[4]=(CCC[4]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x48;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[5]=buff_r[1];	CCC[5]=(CCC[5]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x50;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[6]=buff_r[1];	CCC[6]=(CCC[6]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x58;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[7]=buff_r[1];	CCC[7]=(CCC[7]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x60;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[8]=buff_r[1];	CCC[8]=(CCC[8]<<8)|(buff_r[2]);
		buff_w[0]=0x0E;		buff_w[1]=0x68;		I2CWrite(address,buff_w,2);		I2CRead(address,buff_r,3);	CCC[9]=buff_r[1];	CCC[9]=(CCC[9]<<8)|(buff_r[2]);
		wait_msec(40);
	}

	//read D1 D2
//	if(C1_C10==0){
  	buff_w[0]=0x0F;	
	  buff_w[1]=0x51;	
	  I2CWrite(address,buff_w,2);
	
	  MCLKs = 1150;
	  while (MCLKs);

  	I2CRead(address,buff_r2,2);	
	  CCC[10]=buff_r2[0];	
	  CCC[10]=(CCC[10]<<8)|(buff_r2[1]);

//  	//ASPETTO MILLISECONDI
//	  wait_msec(60);

  	buff_w[0]=0x0F;	
	  buff_w[1]=0x21;	
	  I2CWrite(address,buff_w,2);
	  MCLKs = 1150;
	  while (MCLKs);

    I2CRead(address,buff_r2,2);	
	  CCC[11]=buff_r2[0];	
	  CCC[11]=(CCC[11]<<8)|(buff_r2[1]);

//  	//ASPETTO MILLISECONDI
//		wait_msec(60);
	
	C1=CCC[0];
	C2=CCC[1];
	C3=CCC[2];
	C4=CCC[3];
	C5=CCC[4];
	C6=CCC[5];
	C7=CCC[6];
	C8=CCC[7];
	C9=CCC[8];
	C10=CCC[9];
	
	if (Press_FirstTime_and_invert_channel==0){
	  D1=(float)CCC[11]; // press
	  D2=(float)CCC[10]; // temp
  }
	else{
	  D1=(float)CCC[10]; // press
	  D2=(float)CCC[11]; // temp
  }

	// 0x0F40 command 
	buff_w[0]=0x0F;
	buff_w[1]=0x40;
	I2CWrite(address, buff_w, 2);
	// reset 
	buff_w[0]=0xAA;
	buff_w[1]=0xAA;
	I2CWrite(address, buff_w, 2);
	
	// calculate temperature
	dT_=D2-C1;
	temp_MIS7300=((C2-1000)/100)+(dT_*(C4+30000)/pow(10,7))+(C3*dT_*dT_/pow(10,10));

// calculate pressure
	sens=(dT_*dT_*(C5-20000)/pow(10,12))-(C6*dT_/pow(10,9))+(C7/1000);
	off_=(dT_*dT_*(C8-10000)/pow(10,9))+(dT_*(C9-30000)/pow(10,5))+(C10);
	Press_MIS7300=1000-((off_-D1)/sens);
  
	//ATT! se il chip � sbagliato bisogna invertire
	if ( ((Press_MIS7300<300) || (temp_MIS7300>80)) && (Press_FirstTime_and_invert_channel==0) ) {
		Press_FirstTime_and_invert_channel = 1;
	}

//GuiVar_float_7 = Press_MIS7300;
//GuiVar_float_8 = temp_MIS7300;

}


//legge HDC1000 con I2C_2
/***********************************************************************
 * @brief		I2CReadRH
 **********************************************************************/
    double pvs, ma, Tdew, pvs1, pvs2;		
		void read_RHT(void){
					uint8_t address=(0x40);
					uint8_t buff_r[4], buff_w[4];
			
				buff_r[0]=0x00;
				buff_r[1]=0x00;
				buff_r[2]=0x00;
				buff_r[3]=0x00;

////prova lettura registro Manufacturer ID
//buff_r[0]=0;
//buff_r[1]=0;			
//buff_w[0]=0xFE;
//I2CWrite((address),buff_w,1);
//wait_msec(20);
//I2CRead(((address)),buff_r,2);
//GuiVar_integer_4 = buff_r[0]*256 + buff_r[1];
			
//  			wait_msec(50);
				
				//scrittura registro configurazione
				buff_w[0]=0x02;
				buff_w[1]=0x10;
				buff_w[2]=0x00;

				I2CWrite((address),buff_w,3);
//				wait_msec(10);
        buff_w[0]=0x00;
				I2CWrite((address),buff_w,1);
		//		buff_r[1]=0x41;
    	    //se il segnale DRDY � basso, posso leggere le misure di T e RH
//				if((GPIO_ReadValue((Port_2)>>21)&1)==1)
//				  while(	(GPIO_ReadValue(Port_2)>>21)&1	);
				wait_msec(20);
				I2CRead(((address)),buff_r,4);

        temp_HDC1000=((  (((  ((float)(buff_r[0])*256)+((float)(buff_r[1]))  )*16500)/65536))-4000)/100;
				RH_HDC1000=((((float)(buff_r[2])*256)+((float)(buff_r[3])))*10000/65536)/100;
        RH_HDC1000_percent = RH_HDC1000;



				// pressione_taratura di vapore saturo - temp_HDC1000 
				pvs1=exp(65.81-(7066.27/(temp_HDC1000+273.15))-(5.976*(log(temp_HDC1000+273.15))));
				// pressione_taratura di vapore saturo - temp_HDC1000 
				pvs2=exp(65.81-(7066.27/(temp_MIS7300+273.15))-(5.976*(log(temp_MIS7300+273.15))));
				// umidit� assoluta - g_h2o / kg_air_dry
				ma=(R_air/R_h2o)*(pvs1/(Press_MIS7300*100-pvs1))*RH_HDC1000/100*1000;

				RH_HDC1000=(ma*(R_h2o/R_air)*(Press_MIS7300*100-pvs2)/pvs2)*100/1000;

//GuiVar_float_1 = (float)(temp_HDC1000);
//GuiVar_float_2 = (float)(RH_HDC1000);

}





/***********************************************************************
 * @brief		read phi value by sensor 1-3
 **********************************************************************/
float read_phi(void)
{
	//VARIABLES
	GPDMA_Channel_CFG_Type 	GPDMACfg;	
	uint16_t adc_lux[DMA_SIZE][NPEAKS];
	uint16_t i;
	uint16_t j;
	uint16_t p;
	uint16_t peak;
	uint16_t treno_i;
	uint16_t O2_command_load;
	uint16_t adc_lux_min;
	uint32_t adc_lux32[DMA_SIZE];
	uint32_t dark;
	float adc_lux_avg[DMA_SIZE];
	float tao[TRENI];
	float phi[TRENI];
	float adc_lux_max;
	double med;
	int phi_max_i;
	int phi_min_i;
	float phi_max;
	float phi_min;
	int peak_i;
	float O2_phi;
	int value_i;
	
	//inibisco l'interrupt del CAN
	//Disable Interrupt
	CAN_IRQCmd(CAN_2, CANINT_RIE, DISABLE);
	CAN_IRQCmd(CAN_1, CANINT_RIE, DISABLE);
	//Disable CAN Interrupt
	NVIC_DisableIRQ(CAN_IRQn);

	O2_command_load=5;	// 0-10 duty cycle LED [10uS] ->	5
	
	//READ PHI
	/* Setting GPDMA interrupt */  
	NVIC_DisableIRQ(DMA_IRQn);  
	NVIC_SetPriority(DMA_IRQn, ((0x01 << 3) | 0x01));  
	NVIC_EnableIRQ(DMA_IRQn);  
	/* Setting ADC interrupt, ADC Interrupt must be disable in DMA mode */  
	NVIC_DisableIRQ(ADC_IRQn);
	ADC_Init(LPC_ADC, 400000);												// ADC conversion rate = 400KHz
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_0,ENABLE);	    //sensor A	// LPC_ADC_BURST_MULTI
	GPDMA_Init(); 																		/* Initialize GPDMA controller */
	GPDMACfg.ChannelNum = 0;										    	// channel 2 (channel 0 with highest priority)
	GPDMACfg.SrcMemAddr = 0;													// Source memory - unused
	GPDMACfg.DstMemAddr = (uint32_t)&adc_lux32[0];  	// Destination memory
	GPDMACfg.TransferSize  = DMA_SIZE;							  // Transfer size
	GPDMACfg.TransferWidth = 0;												// Transfer width - unused
	GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M; 	// Transfer type
	GPDMACfg.SrcConn = GPDMA_CONN_ADC;								// Source connection
	GPDMACfg.DstConn = 0;															// Destination connection - unused
	GPDMACfg.DMALLI  = 0;															// Linker List Item - unused
	GPDMA_Setup(&GPDMACfg);
	
	
	ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_0,ENABLE);	// LPC_ADC_BURST_MULTI
	
	// LED OFF
	GPIO_OutputValue(Port_5,(1<<P_O2_CMD),0);
	
	
	for(treno_i=0;treno_i<TRENI;treno_i++)
	{
//		ADC_BurstCmd(LPC_ADC, ENABLE);
		
		for(peak_i=1;peak_i<NPEAKS+1;peak_i++)
		{
			//accendo LED ON			
			GPIO_OutputValue(Port_5,(1<<P_O2_CMD),1);
						
			//campiono i valori del ciclo precedente
			for(i=0;i<DMA_SIZE;i++){
				//while (ADC_DR_DONE_FLAG==0);
				adc_lux[i][peak_i-1]=ADC_DR_RESULT(adc_lux32[i]);	// converto il risultato del ciclo precedente
			}
			
			GPDMA_ChannelCmd(GPDMACfg.ChannelNum, ENABLE);									// Enable GPDMA ADC channel
			
			//spengo LED OFF
			GPIO_OutputValue(Port_5,(1<<P_O2_CMD),0);
			
			//aspetto
			valore_Timer2 = O2_command_load;
			while(valore_Timer2>0){}
					
			GPDMA_ChannelCmd(GPDMACfg.ChannelNum, DISABLE);									// Disable GPDMA channel		
			NVIC_DisableIRQ(DMA_IRQn);
			GPDMA_Setup(&GPDMACfg);

		}	//FINE PICCHI
		
		ADC_BurstCmd(	LPC_ADC, DISABLE);
		
		//CALCOLO LA MEDIA
		for(i=0;i<DMA_SIZE;i++){
			//adc_lux_avg[i]=0;
			med=0;
			for(p=0;p<NPEAKS;p++){
				med += adc_lux[i][p]; //adc_lux_avg[i]+adc_lux[i][p]
			}
			adc_lux_avg[i]=(med/NPEAKS);	
		}
		
		//TROVO IL VALORE MINIMO
		adc_lux_min=4096;
		for(i=1;i<DMA_SIZE;i++){
			if(adc_lux_min>=adc_lux_avg[i]){
				adc_lux_min=adc_lux_avg[i];
			}
		}
		dark=adc_lux_min;
		
		//TOLGO IL VALORE MINIMO DAI VALORI COME FOSSE UN OFFSET
		for(i=0;i<DMA_SIZE;i++)
		{
			adc_lux_avg[i]=adc_lux_avg[i]-dark;
		}
		
		//ELIMINO I PRIMI VALORI
		adc_lux_avg[0]=0;
		
		//TROVO IL VALORE MASSIMO
		adc_lux_max=0;
		for(i=1;i<DMA_SIZE/4;i++)
		{
			if(adc_lux_max<=adc_lux_avg[i]){
				adc_lux_max=adc_lux_avg[i];
			}
		}
		
		//CALCOLO IL VALORE tau
		for(i=1;i<DMA_SIZE;i++)
		{
			if(adc_lux_max/adc_lux_avg[i]>0){
				tao[i]=((i*ADC_SAMPLE_TIME-TIMECORRECTION)/(logf(adc_lux_max/adc_lux_avg[i])));
			}else{
				tao[i]=0;
			}
		}
		
		//CALCOLO LA phi
		for(i=1;i<DMA_SIZE;i++)
		{
			tao[i] = (tao[i]>0)? tao[i]:0;
			//			      2 PI  Hz		tao
			phi[i]=((atan(2*PI*5000*tao[i]/1000000))*180/PI);
		}
		
		phi_avg[treno_i]=0;
		for(i=AVG_START;i<AVG_STOP;i++){
			phi_avg[treno_i] += phi[i];
		}
		phi_avg[treno_i] = phi_avg[treno_i]/(AVG_STOP-AVG_START);
		

	} //FINE TRENI
	
	//SPENGO ADC
	ADC_DeInit(LPC_ADC);
	
//	// CALCOLO LA MEDIA DELLA PHI
	for(j=0;j<SCARTI;j++){	
		phi_max=0;
		phi_min=4096;//900;
		// elimini i n=SCARTI VALORI MINIMO
		for(i=1;i<TRENI;i++){
			if(phi_min>phi_avg[i] & phi_avg[i]>0){ // identifico il minimo
				phi_min=phi_avg[i];
				phi_min_i=i;
			}
		}
		phi_avg[phi_min_i]=0;
		// elimini i n=SCARTI VALORI MASSIMI
		for(i=1;i<TRENI;i++){
			if(phi_max<phi_avg[i] & phi_avg[i]>0){	// identifico il massimo
				phi_max=phi_avg[i];
				phi_max_i=i;
			}
		}
		phi_avg[phi_max_i]=0;
	}	// end of FOR SCARTI

	O2_phi=0;
	for(i=0;i<TRENI;i++){
		O2_phi += phi_avg[i];
	}
  O2_phi=O2_phi/(TRENI-2*SCARTI);

	//riabilito l'interrupt del CAN
	//Enable Interrupt
	CAN_IRQCmd(CAN_2, CANINT_RIE, ENABLE);
	CAN_IRQCmd(CAN_1, CANINT_RIE, ENABLE);
	//Enable CAN Interrupt
	NVIC_EnableIRQ(CAN_IRQn);
	
	return O2_phi;
}


int n_index=0;
int ncampioni=0;
void readO2(void)
{
	int i;
	float tmp1;
	float tmp2;
	float phi; 

	//flashing time
	phi = read_phi();
	
  O2_phi_A=phi;

//	calc_O2(phi);

//	calibrateO2Span();
	
	//salvo i valori per la media
	O2_medio_average[n_index]=O2_medio;
  O2_medio_span_average[n_index]=O2_medio_span;
	
	if(n_index<NSAMPLES-1)
	{
		n_index++;
	}
	else
	{
		n_index=0;
		ncampioni=1;
	}
	
	tmp1=0;
	tmp2=0;
	
	if(ncampioni==1)
	{
		for(i=0;i<NSAMPLES;i++)
		{
			tmp1 = tmp1 + O2_medio_average[i];
			tmp2 = tmp2 + O2_medio_span_average[i];
		}
		O2_medio_avg = tmp1/NSAMPLES;
		O2_medio_span_avg = tmp2/NSAMPLES;
	}
	else
	{
		for(i=0;i<n_index;i++)
		{
			tmp1 = tmp1 + O2_medio_average[i];
			tmp2 = tmp2 + O2_medio_span_average[i];
		}
		O2_medio_avg = tmp1/n_index;
		O2_medio_span_avg = tmp2/n_index;
	}


}
/*
//legge e scrive un byte alla volta
int EEpromTest (void)
{
    //scrive byte
    int res;
    char buf[3];
    int mem_addr = 0x00;//0x11;
    char data = 0x55;

    buf[0] = (0xff00 & mem_addr)>>8;        // Write Address High byte set
    buf[1] = (0x00ff & mem_addr);           // Write Address Low  byte set
    buf[2] = data;

    res = i2c2.write(0xA8, buf, sizeof(buf), false);
    if(res==0)
    {
        uart2.printf("Successo! Scrittura EEprom Byte. \n");
    }
    else
    {
        uart2.printf("Fallito! Scrittura EEprom Byte. %d.\n", res);
    }
    //lettura byte
    //spedisce indirizzo
    res = i2c2.write(0xA8, buf, sizeof(buf)-1, true);
    //legge byte
    res = i2c2.read(0xA8, &data, 1, false);

    uart2.printf("letto da EEprom Byte. %d.\n", data);

    if (data ==0x55 ) return 0;

    return res;
}
*/

static void eepromDelay(void)
{
  volatile int i = 0;
  for (i = 0; i <0x20000; i++);
}

/******************************************************************************
 *
 * Description:
 *    Read from the EEPROM
 *
 * Params:
 *   [in] buf - read buffer
 *   [in] offset - offset to start to read from
 *   [in] len - number of bytes to read
 *
 * Returns:
 *   number of read bytes or -1 in case of an error
 *
 *****************************************************************************/
int16_t i2c_eeprom_read(uint8_t* buf, uint16_t offset, uint16_t len)
{
  uint8_t addr = 0;
  int i = 0;
  uint8_t off[2];

  if (len > I2C_EEPROM_TOTAL_SIZE || offset+len > I2C_EEPROM_TOTAL_SIZE) {
    return -1;
  }

  addr = EEPROM_I2C_ADDR;
  off[0] = ((offset >> 8) & 0xff);
  off[1] = (offset & 0xff);

  I2CWrite(0xA8, off, 2);
  //for ( i = 0; i < 0x2000; i++);
  I2CRead(0xA9, buf, 1);

//  I2CWrite((addr << 1), off, 2);
//  for ( i = 0; i < 0x2000; i++);
//  I2CRead((addr << 1)+1, buf, len);
//  I2CWrite((addr << 1), off, 2);
//  for ( i = 0; i < 0x2000; i++);
//  I2CRead((addr << 1)+1, buf, len);

  return len;

}

/******************************************************************************
 *
 * Description:
 *    Write to the EEPROM
 *
 * Params:
 *   [in] buf - data to write
 *   [in] offset - offset to start to write to
 *   [in] len - number of bytes to write
 *
 * Returns:
 *   number of written bytes or -1 in case of an error
 *
 *****************************************************************************/
int16_t i2c_eeprom_write(uint8_t* buf, uint16_t offset, uint16_t len)
{
  uint8_t addr = 0;
  int16_t written = 0;
  uint16_t wLen = 0;
  uint16_t off = offset;
  uint8_t tmp[EEPROM_I2C_PAGE_SIZE+2];
	unsigned int provadebug;

  if (len > I2C_EEPROM_TOTAL_SIZE || offset+len > I2C_EEPROM_TOTAL_SIZE) {
    return -1;
  }

  addr = EEPROM_I2C_ADDR;

    tmp[0] = 0;        // Write Address High byte set
    tmp[1] = 0;           // Write Address Low  byte set
    tmp[2] = 0x55;

//  wLen = EEPROM_I2C_PAGE_SIZE - (off % EEPROM_I2C_PAGE_SIZE);
//  wLen = MIN(wLen, len);

//  while (len) {
//    tmp[0] = ((off >> 8) & 0xff);
//    tmp[1] = (off & 0xff);
//    memcpy(&tmp[2], (void*)&buf[written], wLen);
//    provadebug = I2CWrite((addr << 1), tmp, wLen+2);

//    /* delay to wait for a write cycle */
//    eepromDelay();

//    len     -= wLen;
//    written += wLen;
//    off     += wLen;

//    wLen = MIN(EEPROM_I2C_PAGE_SIZE, len);
//  }

  return written;
}


/////***********************************************************************
//// * @brief		get_DigInput 
//// *          legge lo stato degli ingressi digitali
//// *          N.B. restituisce 1 se l'ingresso sente un contatto chiuso, 0 se aperto
//// **********************************************************************/
//unsigned char get_DigInput(void){
// unsigned char result=0;
//	
//  result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_1 & 1) << 0;
//  result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_2 & 1) << 1;
// 	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_3 & 1) << 2;
//	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_1_TO_4)>>PIN_DIG_IN_4 & 1) << 3;
//	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_5 & 1) << 4;
//	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_6 & 1) << 5;
//	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_7 & 1) << 6;
//	result |= (unsigned char)(GPIO_ReadValue(PORT_DIG_IN_5_TO_8)>>PIN_DIG_IN_8 & 1) << 7;
//	
//	return ~result;//faccio il complemento a 1 per invertire
//}







/***********************************************************************
 * @brief		read data from SD
 **********************************************************************/
int* caricaConfigurazione(int nline)
{
	char line[255];
	int *returned_values;
	int values[30];
	int n=1;
  FILE *datalog_file;
	
#ifdef USE_SD                                         
	if (use_sd == 1){
		finit(NULL);
		
		//leggo da sd
		datalog_file = fopen("ADOX_config.txt", "r");
		if(datalog_file){
			//leggo tutta la linea
			while(fgets(line, sizeof(line), datalog_file) != NULL)
		  {
				if(n==nline)
				{
//					sscanf(line,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",&values[0],&values[1],&values[2],&values[3],&values[4],&values[5],&values[6],&values[7],&values[8],&values[9],&values[10],&values[11],&values[12],&values[13],&values[14],&values[15],&values[16],&values[17],&values[18],&values[19],&values[20],&values[21],&values[22],&values[23],&values[24],&values[25],&values[26],&values[27],&values[28],&values[29],&values[30],&values[31],&values[32],&values[33],&values[34],&values[35],&values[36],&values[37],&values[38],&values[39],&values[40],&values[41],&values[42],&values[43],&values[44],&values[45],&values[46],&values[47],&values[48],&values[49],&values[50],&values[51],&values[52],&values[53],&values[54],&values[55],&values[56],&values[57],&values[58],&values[59],&values[60],&values[61],&values[62],&values[63]);
					sscanf(line,"%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",&values[0],&values[1],&values[2],&values[3],&values[4],&values[5],&values[6],&values[7],&values[8],&values[9],&values[10],&values[11],&values[12],&values[13],&values[14],&values[15],&values[16],&values[17],&values[18],&values[19],&values[20],&values[21],&values[22],&values[23],&values[24],&values[25],&values[26],&values[27],&values[28],&values[29]);
				}
				n++;
		  }
			fclose(datalog_file);
		}
		else
			values[0] = -1;
	}
	else
		values[0] = -1;
	
	returned_values = values;
	
	return returned_values;
#endif
}



int init_eeprom (void){

  int *values;
	int page_index, int_index, i, a;
	
	for(page_index=0; page_index<E2_MAX_PAGE-1; page_index++){
	  values = caricaConfigurazione(page_index+1);
		if ( (values[0]==43690) || (page_index) ){//0xAAAA
	    for(int_index=0; int_index<E2_MAX_INT; int_index++){
  		  eeprom_write_word (page_index, int_index*2, values[int_index]);
      }
		}
		else
			page_index = E2_MAX_PAGE;
	}
	
	return values[0];

}



/***************************************************************************************************************************************/
/*--------------------------------------------------------------------------------------------------------------------------------------
																													Main Programd
*--------------------------------------------------------------------------------------------------------------------------------------*/
/***************************************************************************************************************************************/
extern unsigned char RS485_Slave_Address;


int main (void) {
	unsigned int tmrCanInitmsg, pwm_LCD, pwm_rele, dummy, var_done=0;
	int j=0;
  int test=0;
	int value2plot[DIM_CHART];
	int value2plot2[DIM_CHART];
	int plotminX,plotmaxX;
	unsigned int lastplotindex=0;
	unsigned int canalePT100 = 0;
  unsigned int tmr_check_input_mV = 0;

	
  GuiLib_GraphDataPoint GraphDataLine[50];
  GuiLib_GraphDataPoint GraphDataLine2[50];


//	U32 temp_SD;
	delay_ms(100);	// per sicurezza aspettare che si stabilizzino tensioni etc.
	
	init_peripherals();
	
  delay_ms(200);
	rtc_get_time();	
	
	//all'accensione setta la scheda come Master o Slave a seconda del define, in seguito in funzione della comunicazione con l'altra CIE 
	CIE.Reply = 0;
  CIE.Master = 1;

#ifdef DEBUG_MODE
  GuiVar_Titolo = 3;
#else
  GuiVar_Titolo = CIE.Master;
#endif
	
//	GuiLib_CurStructureNdx = GuiStruct_Main_0;
	GuiLib_CurStructureNdx = GuiStruct_Idle_0;
	GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
	GuiLib_Refresh();
	
/////////////////*	InitSpecial(); // chiamata con password */
	
	init_variables();
	init_from_eeprom();
//	init_sys_io();
	
	GuiVar_theme_color_1 = BLACK;
	GuiVar_theme_color_2 = MAJORELLE_BLUE;
	GuiVar_theme_color_3 = RED;
	
	rtc_get_time();

	tmp_day = RTCFullTime.DOM;

	if (CIE.Master)
		CAN_1_setup_devices();
  else
	  GuiVar_test_leds_button = 1;
	
	tmr_warm_up_fault	= 300; //attende un po' prima di controllare eventuali guasti
  tmr_warm_up_alarm	= 6000;
	tmr_startup = 150;
  tmr_toggle1	= 0;	
  tmr_toggle2	= 300;	
  tmr_toggle3	= 0;	
	tmr_toggle5 = 0;
  CAN_1_read_buffer();
	CAN_2_read_buffer();
	
	init_comm_err();
	tmrCanReinit = CAN_REINIT;
 
//	GuiVar_test_screen = 0;
//	while (GuiVar_test_screen<20){
//		++GuiVar_test_screen;
//		delay_ms(200);
//	  GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
//  	GuiLib_Refresh();
//  }
		/* Loop Forever */
	/* per ora (senza trasmissione) dura 176 ms*/
  //pwm
	pwm_config();
	
  MySetPin(Port_4, TOUCH_RST, 1);
	
//---------------------- eliminare --------------------------------
//GuiVar_default_room_1 = 1;
//-----------------------------------------------------------------
//inizializzazione grafico
	GuiLib_ShowScreen (GuiLib_CurStructureNdx,GuiLib_NO_CURSOR,GuiLib_RESET_AUTO_REDRAW);
	GuiLib_Graph_AddDataSet(0, 0, 0, 0, GraphDataLine, 50,0,0);
	GuiLib_Graph_AddDataSet(0, 1, 0, 0, GraphDataLine2, 50,0,0);
	GuiLib_Graph_AddDataPoint(0, 0, 0, 0);
	GuiLib_Graph_AddDataPoint(0, 1, 0, 0);
	GuiLib_Graph_Redraw(0);
//------------------------------------------------------------

//prova scruittura 5 byte a partire dall'indirizzo 60000	
//EEPROM_wrdat[2] = 121;
//EEPROM_wrdat[3] = 222;
//EEPROM_wrdat[4] = 33;
//EEPROM_wrdat[5] = 4;
//EEPROM_wrdat[6] = 5;
//i2c_EEPROM_Write(60000,EEPROM_wrdat,5);


//EEPROM_wrdat[2] = 1;
//EEPROM_wrdat[3] = 2;
//i2c_EEPROM_Write(60000,EEPROM_wrdat,2);
//prova scruittura di un intero a partire dall'indirizzo 60000	
//i2c_EEPROM_Write_INTEGER (60000, 25346);

////prova scruittura di un intero long a partire dall'indirizzo 10	
	// LED OFF
//GPIO_OutputValue(Port_5,(1<<P_E2PROM_WP),0); //abilito scrittura su E2PROM
//i2c_EEPROM_Write_LONG_INTEGER (10, 1234567890); //0x499602D2

//prove ADC ////////////////////////////////////////////////////////////////////////////////////////
TIM3.msec_timer	= 10 ;
while ( TIM3.msec_timer != 0) ;

ADC_default(ADC_NUM) ;
ADC_default_ADC5() ;

TIM3.msec_timer	= 10 ;
while ( TIM3.msec_timer != 0) ;

// solo temporaneamente qui <-------------------- !!! 
//#ifdef PROVA_PT100
//ADC_conf_reg_ch(ADC_NUM,0,1) ;	//PT100
//#endif
//#ifdef PROVA_mA
//ADC_conf_reg_ch(ADC_NUM,0,3) ;	//corrente
//#endif
//#ifdef PROVA_mV
//ADC_conf_reg_ch(ADC_NUM,0,2) ;	//tensione
//#endif

//TIM3.msec_timer	= 6 ;
//while ( TIM3.msec_timer != 0) ;		

/////////////////////////////////////////////////////////////////////////////////////////////////////

  MySetPin(Port_0, LEDENABLE, 1); // abilita l'uscita per i LED
  MySetPin(Port_5, uCS2_4, 1);    // CS disabilitato per non sporcare lo stato

  InitRele();

//prova scrittura RAM tamponata del RTC ho a dispozizione 5 registri da 32 bit (0-4) in tutto 20 byte
//RTC_WriteGPREG(LPC_RTC,0,65123);
  
	InitAdox();
	
	CAN_ReInit(); //richiama l'inizializzazione per impostare il baudrate come da E2PROM
	
  GuiVar_fault_back_color = 0xFFFF; //2018.03.05
	
	reset_after_new_configuration_load = 0;

  // gestisce il problema di scrittura sulla SD evitandone l'accesso in caso di errori ripetuti - contando i reset della scheda nella giornata
  daily_reset_counter = RTC_ReadGPREG (LPC_RTC,RTC_REG_4);
	if (daily_reset_counter<10)
		++daily_reset_counter;
 	//salva su RAM tamponata del RTC il nuovo valore 
  RTC_WriteGPREG(LPC_RTC,RTC_REG_4,daily_reset_counter);

	if (daily_reset_counter==10){
		use_sd = 0;
		SBM_sd_card.fault = 1;
	}

  Adox1500.TmrStartUp = 60;
	tmrPT100=10;

  GuiVar_fault_back_color = 0xFFFF;

	Adox1500.PT100_1_AvgIndex = 1;
	Adox1500.PT100_1_AvgBufferFull = 0;
	adc_counter_PT100_1 = 0;
	Adox1500.PT100_2_AvgIndex = 1;
	Adox1500.PT100_2_AvgBufferFull = 0;
	adc_counter_PT100_2 = 0;
	
  GuiVar_extra_info_index = 0;
	
	UART_2_Initialization();
	
	UART_3_Initialization();
		
// 20201002
	primo_tasto = TRUE;  
#ifdef READ_TOUCH_SCREEN         
   touch_press();
#endif  //READ_TOUCH_SCREEN   
	touch_handle();
	if ((new_value_to_display) || (cornice))
		touch_release(cornice);  
  
	while(1){
	// =====================

		// Interrogazione ingressi analogici con ADC esterni
		if ( TIM2.msec_timer == 0 ){
			
			//salta i canali non usati
			if ( (TIM2.switch_ADC==0) || (TIM2.switch_ADC==5) || (TIM2.switch_ADC==6) )	
				++TIM2.switch_ADC;
			
			switch (TIM2.switch_ADC){
				case 0:        
					input_analog_1_acquisizione();
					break;
				case 1:
					input_analog_2_acquisizione();
					break;
				case 2:
					input_analog_3_acquisizione();
					break;
				case 3:
					input_analog_4_acquisizione();
					break;
				case 4:
					input_analog_5_acquisizione();
					break;
				case 5:
					input_analog_6_acquisizione();
					break;
				case 6:
					input_analog_7_acquisizione();
					break;
				case 7:
					input_analog_8_acquisizione();
					break;
				case 8:
					input_analog_9_acquisizione(); // 20201005 MODIFICATO per gestione tempi lunghi di acquisizione
					break;
			}
      GuiVar_debug_integer_1 = ADC_value[INPUT_2_ADC][INPUT_2_CHANNEL];			
      GuiVar_debug_integer_2 = ADC_value[INPUT_8_ADC][INPUT_8_CHANNEL];
      //GuiVar_extra_info_index = 1;			
			
			//controlla canale mV solo una volta al secondo, perch� � lento (poi da sistemare i wait in modo da fare altro, mentre si attende il campionamento)
			if (tmr_check_input_mV<4){
				tmr_check_input_mV++;
			  TIM2.switch_ADC = (TIM2.switch_ADC<7)? ++TIM2.switch_ADC: 0;
			}
			else{
			  TIM2.switch_ADC = (TIM2.switch_ADC<8)? ++TIM2.switch_ADC: 0;
				if (TIM2.switch_ADC==8)
					tmr_check_input_mV = 0;
			}
			
			TIM2.msec_timer = 200 ;	
		}
		
//		if ( TIM2.msec_timer == 0 )
//				{
//#ifdef PROVA_PT100
//ADC_conf_reg_ch(ADC_NUM,canalePT100,1) ;	//PT100
//TIM3.msec_timer	= 8 ;
//while ( TIM3.msec_timer != 0) ;		
//#endif
//#ifdef PROVA_mA
//ADC_conf_reg_ch(ADC_NUM,0,3) ;	//corrente
//#endif
//#ifdef PROVA_mV
//ADC_conf_reg_ch(ADC_NUM,0,2) ;	//tensione
//#endif
////				adc_internal[1] = adc_value_int ;	 //non serve a niente !!!
//										
//				ADC_set_mode(ADC_NUM) ;	
//				TIM3.msec_timer	= 12 ;
//						while ( TIM3.msec_timer != 0) ;
//					
//				ADC_read(ADC_NUM, canalePT100, 1)	;					
////				ADC_read(ADC_NUM, 0, 1)	;		

//				TIM3.msec_timer	= 20 ;
//						while ( TIM3.msec_timer != 0) ;
//					
//#ifdef PROVA_mV
//        //era commentato - Antonio: da mettere se si usa GAIN elevato e polarizzazione di AIN-
//				TIM3.msec_timer	= 120 ; //ed era 1
//					 while ( TIM3.msec_timer != 0) ;
//#endif

//if (canalePT100==0){
////				adc_value_ext_temp = ADC_value_read(ADC_NUM,0) ;	
//				adc_value_ext_temp = ADC_value_read(ADC_NUM,canalePT100) ;	
//				
//				GuiVar_integer_3 = adc_value_ext_temp ;			

////				adc_value_ext_temp = ADC_value_read_med(ADC_NUM,0) ;	
//				adc_value_ext_temp = ADC_value_read_med(ADC_NUM,canalePT100) ;	
//				
//				GuiVar_integer_4 = adc_value_ext_temp ;	
//}
//else{
//				adc_value_ext_temp = ADC_value_read(ADC_NUM,canalePT100) ;			
//				GuiVar_integer_5 = adc_value_ext_temp ;			
//				adc_value_ext_temp = ADC_value_read_med(ADC_NUM,canalePT100) ;				
//				GuiVar_integer_6 = adc_value_ext_temp ;	
//}

//#ifdef PROVA_PT100
//        //PT100
//if (canalePT100==0){
//        GuiVar_float_3 = (float)(adc_value_ext_temp)*37.82/(65390-46330); //risultato in ohm
//        GuiVar_float_4 = (float)(adc_value_ext_temp-46330)*(75+20)/(65390-46330)-20; //risultato in �C
//}
//else{
//        GuiVar_float_5 = (float)(adc_value_ext_temp)*37.82/(65390-46330); //risultato in ohm
//        GuiVar_float_6 = (float)(adc_value_ext_temp-46330)*(75+20)/(65390-46330)-20; //risultato in �C
//}
//canalePT100 = (canalePT100)? 0: 1;
//#endif

//#ifdef PROVA_mA
//        //corrente 4-20mA - esempio trasduttore pressione 0 - 16.00 bar
//        GuiVar_float_4 = (float)(adc_value_ext_temp)*(20-4)/(62416-12475); //risultato in mA
//        GuiVar_float_5 = (float)(adc_value_ext_temp-12475)*(20-4)/(62416-12475); //risultato in bar
//#endif

//#ifdef PROVA_mV
//        //tensione 0-13mV - esempio KE-25 0 - 20.93% O2
//        GuiVar_float_4 = (float)(adc_value_ext_temp)*(13-0)/(60200-0); //risultato in mV
//        GuiVar_float_5 = (float)(adc_value_ext_temp-0)*(20.93)/(60200-0); //risultato in %
//#endif

////				TIM2.mode_action++ ;	
//					
//				TIM2.msec_timer = 100 ;	
//		}	
				
	  // gestione messaggi Modbus in risposta al Master su seriale UART 3
    if (PackFull==1)
    {
      PackFull = 0;
      UART_Modbus_Slave_answer();
    }	
		if (setbacklight){
			setbacklight = 0;
			PWM_MatchUpdate(_USING_PWM_NO, 1, 20-backlight, PWM_MATCH_UPDATE_NOW);
		}
		
//esempio prova rel� ------------------------ eliminare ---------------------
//GuiVar_default_room_1 = index_system;
#ifdef USE_SD  
 	 if ( (use_sd==1) || ((tmrChechSD==0) && (use_sd==0) && (daily_reset_counter<10)) )
 	 {
		 tmrChechSD = 500;
	   temp = finit(NULL);
  	 if (temp != 0) {
		   use_sd = 0;
  	   SBM_sd_card.fault = 1;
	   } else {
			 SBM_sd_card.fault = 0;
		   use_sd = 1;
	   }
   }
#endif
//	 timeout_fault_sensors(GuiVar_number_sensor);
		
	 if (tmrChechSD)
	   --tmrChechSD;
	 if (tmr_startup)
	   --tmr_startup;
	 if (tmr_warm_up_alarm)
		 --tmr_warm_up_alarm;
	 if (tmr_warm_up_fault)
	   --tmr_warm_up_fault;

//-----------------------------------------------------------
// SYSTEM FUNCTIONS (watchdog, SD when turn on)
//-----------------------------------------------------------
		watchdog_handle();
	  
	  if (tmr_toggle2)
			--tmr_toggle2;
		if (tmr_toggle2==0)
			tmr_toggle3=1;
		if (tmr_toggle2 == 50){
#ifdef USE_SD
		  if (use_sd==2){ 			
			  get_file_names(current_time);          
			  init_sd_files(); 
				use_sd=1;
	    }
#endif
			// EVENT: System turned on //
	    temp_event.time          = current_time;
	    temp_event.type          = E_TYPE_MISC;
	    temp_event.specific_type = E_S_TYPE_SYSTEM_TURN_ON;
	    add_event(temp_event);
		}

// 20201002
//    if ( (TIM9.msec_timer==0) || ((TOUCHING) && (touch_not_released==0)) ) {
//			if (TOUCHING)
//			  touch_not_released = 1;
//	    TIM9.msec_timer = 80;//150;
//	    touch_handle();
//    }	

    if (Touch_enable)
    {  
#ifdef READ_TOUCH_SCREEN         
        touch_press();
#endif  //READ_TOUCH_SCREEN      
        touch_handle();  
    }
    
    if (new_value_to_display == 1)
    {
      update_graphics_variables(); 
      GuiLib_Refresh();
      new_value_to_display = 3;   // Disabilita Refresh nella prossima scansione Timer
    }
    
//-----------------------------------------------------------
// SYSTEM FUNCTIONS (touch controller, rtc, sd)
//-----------------------------------------------------------
  	if (tmr_toggle1==5){
      ++tmr_toggle1;			
//			touch_handle();   
			rtc_get_time();	
      if ( (current_time.year>2100)||(current_time.year<2017) )
		    rtc_set_time (2017, 5, 15, 12, 30, 0);
	  	
			if ( (tmp_hour!=RTCFullTime.HOUR) && (RTCFullTime.SEC==0) ){
				tmp_hour = RTCFullTime.HOUR;
				//alla mezzanotte resetta il contatore dei reset scheda per provare a riscrivere su SD
				if (tmp_day != RTCFullTime.DOM){
					daily_reset_counter = 0;
					//salva su RAM tamponata del RTC il nuovo valore 
					RTC_WriteGPREG(LPC_RTC,RTC_REG_4,daily_reset_counter);
					tmp_day = RTCFullTime.DOM;
				}
			}

				//ogni ora sincronizza l'orologio dei display e della CIE Slave
  		if ( (tmp_hour!=RTCFullTime.HOUR) && (RTCFullTime.SEC==0) ){
        tmp_hour = RTCFullTime.HOUR;
		  	CAN_1_set_date();
	    }
		}
		else if (tmr_toggle1==6){
      tmr_toggle1 = 0;				
			#ifdef USE_SD                                         
				if (use_sd == 1){
			    get_file_names(current_time);
					print_o2_values();		
				}
			#endif
			}
		//-----------------------------------------------------------
		// LOGIC FUNCTIONS		
		//-----------------------------------------------------------		
		else if ( (tmr_startup==0) && (tmr_toggle1==0) ){
      ++tmr_toggle1;	
			handle_users();
			if (tmr_warm_up_fault==0)
		    check_faults();
    }
		//-----------------------------------------------------------
    // SYSTEM FUNCTIONS (sd, init function)
		//-----------------------------------------------------------
		else if (tmr_toggle1==1){
      Timer_1_second();//operazioni da fare una volta al secondo
      ++tmr_toggle1;
// 20201005      
      ++tmr_toggle1;  // Saltare       tmr_toggle1 = 2

		  tmr_toggle5 = 0;
			if (current_user.operation==2){
				current_user.operation = 0;
				if (init_eeprom()>=0){
					// inizializzazione corretta
					eeprom_write_byte (E2PROM_PAGE_OF_OTHER, REG_NEW_CONFIGURATION, 0xFF);
					GuiVar_screen_select = 0;
					init_screen(SYSTEM_SCREEN);
					GuiVar_message = 13;
					message_timer_count = MESSAGE_ON_SCREEN_TIME*10;
				}
				else{
					// manca il file o non � corretto
					GuiVar_screen_select = 0;
					init_screen(SYSTEM_SCREEN);
					GuiVar_message = 12;
					message_timer_count = MESSAGE_ON_SCREEN_TIME*10;
				}
			}
		}
		//-----------------------------------------------------------
		// OUTPUT RELATED FUNCTIONS
		//-----------------------------------------------------------
		else if (tmr_toggle1==2){
      ++tmr_toggle1;			
			 if (CIE.Master){
//			  if(valore_Timer3==0){
//          read_PT(); //lettura MIS-7300
//          readO2();
//					valore_Timer3 = 1000;
//				}
        Timer_10_seconds();//operazioni da fare ogni 10 secondi
		  }
		}
		//-----------------------------------------------------------
		// GRAPHICs FUNCTIONS
		//-----------------------------------------------------------
		else if (tmr_toggle1==3){
      ++tmr_toggle1;
      
// 20201002			
//			GuiLib_Refresh();
//			update_graphics_variables();
//			handle_rectangle_blink();
      
// 202010002      spostato in "update_graphics_variables()"      
//			handle_pwd_blink();
      
			handle_backlight();
			handle_messages();
      
// 20201002      
			if (new_value_to_display != 3)   // Se Refresh  non � gi� stato fatto prima
      {
        if (Touch_enable)
        {  
#ifdef READ_TOUCH_SCREEN         
          touch_press();
#endif  //READ_TOUCH_SCREEN      
          touch_handle();  
        }
        
        update_graphics_variables();  
        GuiLib_Refresh();
        Touch_enable = TRUE;
      }
      else
        new_value_to_display = 0;      

      
			check_idle_state();
      read_RHT();
			if (tmr_NMT_message==0)
        CAN_1_NMT_message();
			tmr_NMT_message = (tmr_NMT_message)? --tmr_NMT_message: TMR_NMT_MESSAGE;
			if (reset_after_new_configuration_load==0xAB){
				WWDT_Start(5000);//accorcia il timeout per il reset
				while(1);//loop infinito per resettare e ricaricare configurazione da eeprom
			}
    }
		//-----------------------------------------------------------
		// TRANSMISSION RELATED FUNCTIONS- send RS485, receive CAN
		//-----------------------------------------------------------
		else if (tmr_toggle1==4){
      ++tmr_toggle1;			
	  	UART_Modbus_Master_polling();
			CAN_1_read_buffer();
			CAN_2_read_buffer();
		}
		
		//forza la durata del ciclo ad essere sempre uguale
    // while (tmrCiclomain);
    
//		GuiVar_integer_1 = Modbus.Array[MBVECTOR_ALLARMI_2];
//    GuiVar_integer_2 = Modbus.Array[MBVECTOR_ALLARMI];
		
//		tmrCiclomain=TMR_MAIN;

    // se sono in calibrazione non posso uscire da AUTH
		if (Adox1500.Stato & ADOX_TARATURA)
			auto_logout = LOGOUT_TIMEOUT;
	  // --------------------------------------------------------------------------------------------------------

	} // Fine while(1)
} // Fine "main"


